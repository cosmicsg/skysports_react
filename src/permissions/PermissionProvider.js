import React,{forwardRef} from "react";
import { connect } from "react-redux";

//
// PermissionProvider is HOC(Higher Order Component) which is used to getting the App Permissions
//
const PermissionProvider = (WrappedComponent) => {
  //
  // Provider component returns the permissions
  //
  class Provider extends React.PureComponent {

    //
    // Below method which is used to read the permissions from redux store.
    //
    getAppPermissions = () => {
      let tempPermission = [];
      try {
        let pProps = this.props;
        if (
          this.validate(pProps) &&
          pProps.permissionState.userPermissions
        ) {
          tempPermission = this.convertArray(
            pProps.permissionState.userPermissions
          );
        }
      } catch (ex) {
        //If getting exceptions while reading permission then return with empty array
        throw ex;
      }
      return tempPermission;
    };

     //
    // Below method which is used to read the user's role from redux store.
    //
    getAppRoles = () => {
      let tempRoles = [];
      try {
        let pProps = this.props;
        if (this.validate(pProps) && pProps.permissionState.userRoles) {
          tempRoles = this.convertArray(pProps.permissionState.userRoles);
        }
      } catch (ex) {
        //If getting exceptions while reading permission then return with empty array
        throw ex;
      }
      return tempRoles;
    };
    //
    // validate given value
    //
    validate = (val) => {
      if (val && val.permissionState) return true;
      return false;
    };
  
    //
    // below function will checks whether given value is array or not, if not then return value with array
    //
    convertArray = (val) => {
      return Array.isArray(val) ? val : [val];
    };

    //
    // Render wrapped component if it is valid otherwise will throws an error
    //
    render() {
      //Keep old props and return with wrapped component
      const {forwardedRef, ...rest } = this.props;
      
      if (WrappedComponent)
        return (
          <WrappedComponent
            {...rest}
            ref={forwardedRef}
            getAppPermissions={this.getAppPermissions}
            getAppRoles={this.getAppRoles}
          />
        );
      else throw "Please provide valid component to get the app permissions";
    }
  }

  //
  // below method is used to read the redux states from reducer for getting the permissions.
  //
  const mapPropsToState = (state) => ({
    permissionState: state.userReducer,
  });

  const ConnectedMyComponent = connect(
    mapPropsToState
  )(Provider);

  return React.forwardRef((props,ref)=>{
   return <ConnectedMyComponent {...props} forwardedRef={ref}/>
  })
};

//
// here is exporting component as default

export default PermissionProvider;
