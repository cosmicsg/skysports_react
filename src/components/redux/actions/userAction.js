// here add actions, which is like a function and it will return the action type
// action is the only way to interact with store

import * as ActionType from "./actionContants"

// Authenticate user
export const authenticateUser = (IsAuthenticate) => {
	return {
		type: ActionType.AUTHENTICATE_USER,
		payload: IsAuthenticate,
	}
}
export const setUsername = (username) => {
	return {
		type: ActionType.SET_USERNAME,
		payload: username,
	}
}
// example store the user token
export const addUserToken = (userToken) => {
	return {
		type: ActionType.ADD_USER_TOKEN,
		payload: userToken,
	}
}
// example store the user id
export const addUserId = (userId = 0) => {
	return {
		type: ActionType.ADD_USER_ID,
		payload: userId,
	}
}

export const addName = (name) => {
	return {
		type: ActionType.SET_NAME,
		payload: name,
	}
}

export const addToken = (token) => {
	return {
		type: ActionType.SET_TOKEN,
		payload: token,
	}
}

export const addRefreshToken = (refreshToken) => {
	return {
		type: ActionType.SET_REFRESH_TOKEN,
		payload: refreshToken,
	}
}

export const addPermission = (permissions) => {
	return {
		type: ActionType.SET_PERMISSION,
		payload: permissions,
	}
}

export const addFullName = (fullName) => {
	return {
		type: ActionType.USER_FULL_NAME,
		payload: fullName,
	}
}

export const addEmail = (email) => {
	return {
		type: ActionType.USER_EMAIL,
		payload: email,
	}
}

export const addUserProfilePhoto = (userProfilePhoto) => {
	return {
		type: ActionType.USER_PROFILE_PHOTO,
		payload: userProfilePhoto,
	}
}

export const setPersoanlCompleted = (persoanlCompleted) => {
	return {
		type: ActionType.PROFILE_PERSONAL_COMPLETED,
		payload: persoanlCompleted,
	}
}

export const setContactCompleted = (ContactCompleted) => {
	return {
		type: ActionType.PROFILE_CONTACT_COMPLETED,
		payload: ContactCompleted,
	}
}

export const setLoadingCompleted = (LoadingCompleted) => {
	return {
		type: ActionType.LOADING_COMPLETED,
		payload: LoadingCompleted,
	}
}
export const addUserPofileId = (UserProfileId = 0) => {
	
	return {
		type: ActionType.ADD_USER_PROFILE_ID,
		payload: UserProfileId,
	}
}
export const setUserType = (UserType ) => {
	
	return {
		type: ActionType.ADD_USER_TYPE,
		payload: UserType,
	}
}
export const setInvitedUserID=(InvitedUserID)=>{
	return{
		type: ActionType.SET_INVITED_USER_ID,
		payload: InvitedUserID
	}
}