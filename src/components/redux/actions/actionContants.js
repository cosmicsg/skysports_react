// here add action name

export const ADD_USER_TOKEN = "ADD_USER_TOKEN"
export const ADD_USER_ID = "ADD_USER_ID"
export const AUTHENTICATE_USER = "AUTHENTICATE_USER"
export const CHANGE_PASSWORD_URL = "change-password"
export const SET_USERNAME = "SET_USERNAME"
export const SET_NAME = "SET_NAME"
export const SET_TOKEN = "SET_TOKEN"
export const SET_REFRESH_TOKEN = "SET_REFRESH_TOKEN"
export const SET_PERMISSION = "SET_PERMISSIONS"
export const USER_FULL_NAME = "USER_FULL_NAME"
export const USER_EMAIL = "USER_EMAIL"
export const USER_PROFILE_PHOTO = "USER_PROFILE_PHOTO"
export const PROFILE_PERSONAL_COMPLETED = "PROFILE_PERSONAL_COMPLETED"
export const PROFILE_CONTACT_COMPLETED = "PROFILE_CONTACT_COMPLETED"
export const LOADING_COMPLETED = "LOADING_COMPLETED"
export const ADD_USER_PROFILE_ID = "ADD_USER_PROFILE_ID"
export const ADD_USER_TYPE = "ADD_USER_TYPE"
export const SET_INVITED_USER_ID="SET_INVITED_USER_ID"