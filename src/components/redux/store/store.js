import {createStore} from 'redux';
import {rootReducer} from '../reducers/rootReducer';

//creating redux store
const store = createStore(rootReducer);

// store.dispatch({
//     type:"ADD_USER_ID",
//     payload:111
// })
export default store;