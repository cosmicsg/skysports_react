// here added reducer to handle redux store activity
import * as ActionType from "../actions/actionContants";
//import { Permissions } from '../../../message/permissionConstant';
import { Permissions, Roles } from "../../../permissions/AppPermissions";

// here added redux store state variable
const initialStates = {
  userToken: "",
  userId: "",
  userProfileID: "",
  UserType: "",
  userList: [],
  IsAuthenticated: false,
  username: "",
  Name: "",
  token: "",
  refreshToken: "",
  FullName: "",
  UserProfilePhoto: "",
  EmailAddress: "",
  ProfilePersonalCompleted: false,
  ProfileContactCompleted: false,
  LoadingCompleted: false,
  userPermissions: [],
  userRoles: [],
  InvitedUserID: 0,
};

// sample reducer to store user token
export const userReducer = (state = initialStates, action) => {
  switch (action.type) {
    case ActionType.AUTHENTICATE_USER:
      return {
        ...state,
        IsAuthenticated: action.payload,
      };
    case ActionType.SET_USERNAME:
      return {
        ...state,
        username: action.payload,
      };

    case ActionType.SET_NAME:
      return {
        ...state,
        Name: action.payload,
      };
    case ActionType.ADD_USER_TOKEN:
      return {
        ...state,
        userToken: action.payload,
      };

    case ActionType.SET_TOKEN:
      return {
        ...state,
        token: action.payload,
      };

    case ActionType.SET_REFRESH_TOKEN:
      return {
        ...state,
        refreshToken: action.payload,
      };

    case ActionType.SET_PERMISSION:
      return {
        ...state,
        userPermissions: action.payload,
      };

    case ActionType.ADD_USER_ID:
      return {
        ...state, // it will keep old state values
        userId: action.payload,
        //userList:[...state.userList, action.payload],// this will keep old array values and new upcoming values
        //userList: [action.payload], // this will keep only new upcoming values
      };

    case ActionType.USER_FULL_NAME:
      return {
        ...state,
        FullName: action.payload,
      };

    case ActionType.USER_EMAIL:
      return {
        ...state,
        EmailAddress: action.payload,
      };

    case ActionType.USER_PROFILE_PHOTO:
      return {
        ...state,
        UserProfilePhoto: action.payload,
      };

    case ActionType.PROFILE_PERSONAL_COMPLETED:
      return {
        ...state,
        ProfilePersonalCompleted: action.payload,
      };

    case ActionType.PROFILE_CONTACT_COMPLETED:
      return {
        ...state,
        ProfileContactCompleted: action.payload,
      };

    case ActionType.LOADING_COMPLETED:
      return {
        ...state,
        LoadingCompleted: action.payload,
      };

    case ActionType.ADD_USER_PROFILE_ID:
      return {
        ...state,
        userProfileID: action.payload,
      };

    case ActionType.ADD_USER_TYPE:
      return {
        ...state,
        UserType: action.payload,
      };
      
    case ActionType.SET_INVITED_USER_ID:
      return {
        ...state,
        InvitedUserID: parseInt(action.payload),
      };

    default: {
      return state;
    }
  }
};
// sample reducer to store user id
//export const addUserId = (state = initialStates, action) =>{
//    switch(action.type){
//        case ADD_USER_ID : return{
//            ...state, // it will keep old state values
//            userId:action.payload,
//            //userList:[...state.userList, action.payload],// this will keep old array values and new upcoming values
//            userList:[action.payload]// this will keep only new upcoming values
//        }
//
//        default: return state
//    }
//}
// export const addUserId = (state = initialStates, action) =>{

//     switch(action.type){
//         case ADD_USER_ID : return{
//             ...state,
//             //userId:action.payload,
//             userId: action.payload

//         }
//         default: return state
//     }
// }
