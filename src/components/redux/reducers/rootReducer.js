// it is used when we have multiple reducer, because store accept single reducer only
// so that we combined all the reducer with single rootReducer.
// here assign reducer inside the combined reducer
import {combineReducers} from 'redux';
import { userReducer} from './userReducer';

export const rootReducer = combineReducers({
    //syntax is [key:reducer name]. here key is the short name of the reducer name, 
    //we can give any short name related to reducer
    userReducer:userReducer
    //addUser:addUserId
});

