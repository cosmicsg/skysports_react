import axios from "axios";
import history from "../../routing/history";
import { notification } from "antd";
import { ROUTES } from "../../routing/routeConstants";

const requestClient = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
  responseType: "stream",
  // timeout: 30000,
});

//show Notification properties
export const showNotification = (message, type) => {
  notification[type]({
    message: message,
  });
};

//HttpRequest
const httpRequest = async function (options) {
  //await setInterceptor();
  // console.error(options.data)
  const onSuccess = function (response) {
    return response.data;
  };

  const onError = function (error) {
    return Promise.reject(
      error.response || error.message || error.config || error.request
    );
  };

  return requestClient(options).then(onSuccess).catch(onError);
};

//API Request class
class APIRequest {
  //Post service method
  static async getPostService(url, inputData) {
    var token = sessionStorage.getItem("AUTH_TOKEN");
    const headerContent = token
      ? {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          Authorization: `Bearer ${token}`,
        }
      : {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        };
    const response = await httpRequest({
      data: inputData,
      method: "post",
      headers: headerContent,
      url: url,
    });
    return response;
  }

  //GET service method
  static async getGetService(url) {
    var token = sessionStorage.getItem("AUTH_TOKEN");
    const headerContent = token
      ? {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          Authorization: `Bearer ${token}`,
        }
      : {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        };
    const response = await httpRequest({
      method: "GET",
      headers: headerContent,
      url: url,
    });
    return response;
  }

  //Put service method
  static async getPutService(url, inputData) {
    var token = sessionStorage.getItem("AUTH_TOKEN");
    const headerContent = token
      ? { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
      : { "Content-Type": "application/json" };
    const response = await httpRequest({
      data: inputData,
      method: "PUT",
      headers: headerContent,
      url: url,
    });
    return response;
  }

  //Delete service method
  static async getDeleteService(url) {
    var token = sessionStorage.getItem("AUTH_TOKEN");
    const headerContent = token
      ? {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          Authorization: `Bearer ${token}`,
        }
      : {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        };
    const response = await httpRequest({
      method: "DELETE",
      headers: headerContent,
      url: url,
    });
    return response;
  }
}

let isRefreshing = false;

/*  It will automatically gets executed whenever an axios getting the incoming request.
    It is used when token getting expired, and it will gets the new access token by using 
    the refresh token and re-calling the failed request.
*/
requestClient.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const {
      config,
      response: { status },
    } = error;
    const originalReq = config;
    if (status === 401) {
      if (!isRefreshing) {
        // isRefreshing = true;
        let refreshToken = sessionStorage.getItem("REFRESH_TOKEN");
        let accessToken = sessionStorage.getItem("AUTH_TOKEN");
        if (
          refreshToken !== null &&
          refreshToken !== "null" &&
          refreshToken !== undefined &&
          accessToken !== null &&
          accessToken !== "null" &&
          accessToken !== undefined
        ) {
          return fetch(
            process.env.REACT_APP_BASE_URL + "api/account/refresh-token",
            {
              method: "POST",
              mode: "cors",
              cache: "no-cache",
              credentials: "same-origin",
              headers: {
                "Content-Type": "application/json",
                Device: "device",
              },
              redirect: "follow",
              referrer: "no-referrer",
              body: JSON.stringify({
                RefreshToken: refreshToken,
                AccessToken: accessToken,
              }),
            }
          )
            .then((res) => res.json())
            .then((res) => {
              isRefreshing = false;
              if (res.IsValidToken) {
                
                //error.response.config.headers['Authorization'] = `Bearer ${res.Token}`;
                sessionStorage.clear();
                sessionStorage.setItem("REFRESH_TOKEN", res.RefreshToken);
                sessionStorage.setItem("AUTH_TOKEN", res.Token);
                originalReq.headers["Authorization"] = `Bearer ${res.Token}`;
                //calling original request
                return requestClient.request(originalReq);
              } else {
                //If token gets expired then clear session storage and redirect to login page
                sessionStorage.clear();
                history.replace(ROUTES.LOGIN);
              }
            })
            .catch((error) => {});
        } else {
          //If there is no token then clear session storage and redirect to login page
          sessionStorage.clear();
          history.replace(ROUTES.LOGIN);
        }
      }
    }
    return Promise.reject(error);
  }
);

export { httpRequest, APIRequest };
