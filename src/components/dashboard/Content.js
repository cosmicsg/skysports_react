import React from "react"
import { Layout } from "antd"
import { Switch, withRouter, Route } from "react-router-dom"
import dashBoard from "../../pages/dashboard/Dashboard.js"
import viewProfile from "../../pages/profile/MyProfile.js"
import ManageUserTabMenu from "../../pages/admin/create-users/CreateUser" //   CreateUsers/CreateUserTabMenu"
import ManageRoleList from "../../pages/admin/manage-access-control/UserRoleList" //manageroles&permission/ManageRoleList"
import ManageUserList from "../../pages/admin/manage-users//ManageUsers" //manageUserInvite/ManageUserInviteList"
import ManageGames from "../../pages/games/game-setup/CreateGames"
import ManageSportsLevel from "../../pages/games/ManageSportsLevel/ManageSportsLevel"
import ManageSports from "../../pages/games/manage-sport/ManageSport"
import ManageCalandarView from "../../pages/games/ScheduleSports/CalandarView"
import { ROUTES } from "../../routing/routeConstants"
import TrackResults from "../../pages/games/Track-Results/TrackResults"
import TrackFiles from "../../pages/games/games-track-files/TrackFiles"
import FourOhFour from "../../404/404.js"
import CreateHost from "../../pages/games/manage-host-country-activity/HostCountryActivityList"
import GameTrackInviteStatusList from "../../pages/games/game-track-invite-status/GameTrackInviteStatusList"
import ProgramList from "../../pages/training/manage-program//ProgramList"
import ManageSchedule from "../../pages/training/manage-schedules/ManageSchedules"
import TrackAthleteAttendance from "../../pages/training/track-athlete-attendance/TrackAthleteAttendance"
import TrainingAthleteWellness from "../../pages/training/athlete-wellness/AthleteWellness"
import ManageFood from "../../pages/diet/manage-food/ManageFood.js"
import Mater from "../../pages/athlete/my-schedules/MainSchedules"
import MaterDiet from "../../pages/athlete/diet-chart-view/MasterDietChartView"
import DietChart from "../../pages/diet/diet-chart/DietChart.js"
import DepartmentList from "../../pages/department/DepartmentList";
import TrainingPerformance from "../../pages/athlete/training-performance/TrainingPerformance";
import TrackAthletePerformance from "../../pages/coach/track-athlete-performance/TrackAthletePerformance";
const { Content } = Layout

class contentArea extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			isAuthenticated: true,
		}
	}
	componentDidUpdate() {
		window.scrollTo(0, 0)
	}
	render() {
		return (
			<div className='bg-white h-100 sidebar-scroll-overflow-auto padding-body-3' style={{}}>
				<Content>
					<Switch>
						<Route exact path={ROUTES.DASHBOARD} component={dashBoard} />
						<Route exact path={ROUTES.PROFILE} component={viewProfile} />
						<Route exact path={ROUTES.CREATE_USER} component={ManageUserTabMenu} />
						<Route exact path={ROUTES.MANAGE_ROLES} component={ManageRoleList} />
						<Route exact path={ROUTES.MANAGE_USER} component={ManageUserList} />
						<Route exact path={ROUTES.MANAGE_GAMES} component={ManageGames} />
						<Route exact path={ROUTES.SCHEDULE_SPORTS} component={ManageCalandarView} />
						<Route exact path={ROUTES.MANAGE_SPORTS_LEVEL} component={ManageSportsLevel} />
						<Route exact path={ROUTES.MANAGE_SPORT} component={ManageSports} />
						<Route exact path={ROUTES.HOST_COUNTRY_ACTIVITY} component={CreateHost} />
						<Route exact path={ROUTES.DEPARTMENT_SETUP} component={DepartmentList} />
						<Route
							exact
							path={ROUTES.TRACK_INVITE_USER_STATUS}
							component={GameTrackInviteStatusList}
						/>
						<Route exact path={ROUTES.GAMES_TRACK_RESULT} component={TrackResults} />
						<Route exact path={ROUTES.GAMES_TRACK_FILES} component={TrackFiles} />
						<Route exact path={ROUTES.MANAGE_PROGRAM} component={ProgramList} />
						<Route exact path={ROUTES.MANAGE_SCHEDULES} component={ManageSchedule} />
						<Route
							exact
							path={ROUTES.TRACK_ATHLETE_ATTENDANCE}
							component={TrackAthleteAttendance}
						/>
						<Route exact path={ROUTES.ATHLETE_WELLNESS} component={TrainingAthleteWellness} />
						<Route exact path={ROUTES.DIET_MANAGE_FOOD} component={ManageFood} />
						<Route exact path={ROUTES.DIET_MANAGE_CHART} component={DietChart} />
						<Route exact path={ROUTES.MY_SCHEDULES} component={Mater} />
						<Route exact path={ROUTES.DIET_CHART_VIEW} component={MaterDiet} />
						<Route exact path={ROUTES.TRAINING_PERFORMANCE} component={TrainingPerformance} />
						<Route exact path={ROUTES.TRACK_ATHLETE_PERFORMANCE} component={TrackAthletePerformance} />
						<Route component={FourOhFour} />
					</Switch>
				</Content>
			</div>
		)
	}
}

export default withRouter(contentArea)
