import React from "react"
import { withRouter, Link } from "react-router-dom"
import { Layout, Menu } from "antd"
import {
	faUser,
	faUsers,
	faChartPie,
	faClipboard,
	faClipboardList,
	faClipboardCheck,
	faBuilding,
	faChalkboardTeacher,
	faTasks,
	faClock,
	faUserClock,
	faSimCard,
	faCalendarAlt,
	faChartBar,
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons"
import AppLogo from "../../content/images/GoldLogo.png"
import AppLogoSmall from "../../content/images/GoldLogoSmall.png"
import UserIcon from "../../content/images/user.png"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { APIRequest } from "../../components/api-manager/apiRequest" //"components/redux/actions/userAction"
import { connect } from "react-redux"
import { ROUTES } from "../../routing/routeConstants"
import { Permissions as Permission } from "../../permissions/AppPermissions"
import * as ReduxAction from "../../components/redux/actions/userAction" //"components/redux/actions/userAction"
// import DietMntLogo from "../../content//Icons/DietMntLogo.svg"
import DietMantNewLogo from "../../content/Icons/DietMantNewLogo.svg"
import PermissionProvider from "../../permissions/PermissionProvider"

const { Sider } = Layout
const { SubMenu } = Menu

class SideMenu extends React.Component {
	constructor(props) {
		super(props)
		let userPermissions = props.getAppPermissions()
		this.state = {
			collapsed: false,
			FullName: "",
			UserProfilePhoto: "",
			Email: "",
			LoginDefaultKey: "1",
			permissions: userPermissions,
			ProfilePersonalCompleted: false,
			ProfileContactCompleted: false,
		}
		this.fetch = this.fetch.bind(this)
		this.fetchUserInfo = this.fetchUserInfo.bind(this)
		this.handleOnClick = this.handleOnClick.bind(this)
	}
	toggle = () => {
		this.setState({
			collapsed: !this.state.collapsed,
		})
	}
	handleOnClick = (e) => {
		sessionStorage.setItem("SelectedKey", e.key)
	}
	async fetch(state) {
		
		const fetchURL = APIUrl.GET_PROFILE_PHOTO
		let thisSelf = this
		APIRequest.getGetService(fetchURL)
			.then((response) => {
				if (response.Succeeded) {
					thisSelf.setState({
						UserProfilePhoto: response.Data.FileData,
					})
				}
				if (
					!sessionStorage.getItem("PersonalCompleted") === "true" ||
					!sessionStorage.getItem("ContactCompleted") === "true"
				) {
					this.setState({
						...this.state,
						LoginDefaultKey: "2",
					})
				} else {
					this.setState({
						...this.state,
						LoginDefaultKey: "1",
					})
				}
			})
			.catch((error) => {})
	}
	async fetchUserInfo(state) {
		
		const fetchURL = APIUrl.GET_MY_PROFILE_INFO
		let thisSelf = this
		APIRequest.getGetService(fetchURL)
			.then((response) => {
				if (response.Succeeded) {
					thisSelf.setState({
						FullName: response.Data.FullName,
						Roles: response.Data.Roles,
						Email: response.Data.Email,
					})
				} else {
				}
			})
			.catch((error) => {
				thisSelf.setState({ loading: false })
			})
	}
	componentDidMount() {
		this.fetch(this.state)
		this.fetchUserInfo(this.state)
	}
	render() {
		const RefreshDefaultKey = sessionStorage.getItem("SelectedKey")
		let UserTypes = this.props.user.UserType
		return (
			<Layout style={{ minHeight: "100vh" }}>
				<Sider
					trigger={null}
					collapsible
					collapsed={this.state.collapsed}
					className='sidebar-menu-items-width'
				>
					{React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
						className: "trigger",
						onClick: this.toggle,
					})}
					{this.state.collapsed ? (
						<div className='' style={{ marginBottom: "2rem" }}>
							<img
								alt='Logo'
								className=''
								style={{ margin: "1rem 1rem 1rem 0.5rem" }}
								src={AppLogoSmall}
							></img>
						</div>
					) : (
						<div className='' style={{ textAlign: "center", marginBottom: "4.5rem" }}>
							<img alt='Logo' className='' style={{ height: "120px" }} src={AppLogo}></img>
						</div>
					)}

					{this.state.collapsed ? (
						<div>
							{this.props.user.UserProfilePhoto != null || this.state.UserProfilePhoto != null ? (
								<div className='photo-left'>
									<img
										alt='Profile Photo'
										className='photo photo-userprofile bg-white'
										style={{
											width: "50px",
											height: "50px",
											textAlign: "center",
											marginTop: "-1.5rem",
											marginLeft: "15px",
											marginBottom: "1rem",
										}}
										src={
											this.props.user.UserProfilePhoto
												? this.props.user.UserProfilePhoto
												: this.state.UserProfilePhoto
										}
									></img>
								</div>
							) : (
								<div className='photo-left'>
									<img
										alt='Profile Photo'
										className='photo photo-userprofile bg-white'
										style={{
											width: "50px",
											height: "50px",
											textAlign: "center",
											marginTop: "-1.5rem",
											marginLeft: "15px",
											marginBottom: "1rem",
										}}
										src={UserIcon}
									></img>
								</div>
							)}
						</div>
					) : (
						<div className='profile-left sidebar-avatar-layout'>
							{this.state.UserProfilePhoto != null ? (
								<div className='photo-left'>
									<img
										alt='Profile Photo'
										className='photo photo-userprofile bg-white'
										src={
											this.props.user.UserProfilePhoto
												? this.props.user.UserProfilePhoto
												: this.state.UserProfilePhoto
										}
									></img>
								</div>
							) : (
								<div className='photo-left'>
									<img
										className='photo photo-userprofile bg-white'
										src={UserIcon}
										alt='Profile Photo'
									></img>
								</div>
							)}
							<span
								className='user-profile-head-font word-wrap-slideBar'
								title={this.props.user.FullName ? this.props.user.FullName : this.state.FullName}
							>
								{this.props.user.FullName ? this.props.user.FullName : this.state.FullName}
							</span>
							<span
								className='user-profile-head-font word-wrap-slideBar'
								title={
									this.props.user.EmailAddress ? this.props.user.EmailAddress : this.state.Email
								}
							>
								{this.props.user.EmailAddress ? this.props.user.EmailAddress : this.state.Email}
							</span>
						</div>
					)}
					<div className='slider-sidemenu scrollbar'>
						<Menu
							className='default-backgroundColor slider-sidemenu scrollbar'
							defaultSelectedKeys={
								RefreshDefaultKey !== null
									? [RefreshDefaultKey.toString()]
									: [this.state.LoginDefaultKey]
							}
							mode='inline'
							theme='dark'
						>
							<Menu.Item
								key='1'
								icon={
									<i>
										<FontAwesomeIcon icon={faChartPie} />
									</i>
								}
								disabled={this.state.LoginDefaultKey === "2" ? true : false}
								onClick={this.handleOnClick}
							>
								DashBoard
								<Link to={ROUTES.DASHBOARD} />
							</Menu.Item>
							<Menu.Item
								key='2'
								icon={
									<i>
										<FontAwesomeIcon icon={faUser} />
									</i>
								}
								onClick={this.handleOnClick}
							>
								View Profile
								<Link to={ROUTES.PROFILE} />
							</Menu.Item>
							{this.state.permissions.includes(Permission.CU_VIEW) ||
							this.state.permissions.includes(Permission.C_COMMITTEE_U_VIEW) ||
							this.state.permissions.includes(Permission.C_ASSOCIATION_U_VIEW) ||
							this.state.permissions.includes(Permission.C_CLUB_U_VIEW) ||
							this.state.permissions.includes(Permission.C_ATHLETE_U_VIEW) ||
							this.state.permissions.includes(Permission.C_COACH_U_VIEW) ||
							this.state.permissions.includes(Permission.C_COUNCIL_U_VIEW) ||
							this.state.permissions.includes(Permission.M_A_C_VIEW) ||
							this.state.permissions.includes(Permission.M_U_VIEW) ? (
								<>
									<SubMenu
										key='3'
										icon={
											<i>
												<FontAwesomeIcon icon={faUsers} />
											</i>
										}
										title='  Administration'
										className='default-backgroundColor'
										onClick={this.handleOnClick}
									>
										{this.state.permissions.includes(Permission.CU_VIEW) ||
										this.state.permissions.includes(Permission.C_COMMITTEE_U_VIEW) ||
										this.state.permissions.includes(Permission.C_ASSOCIATION_U_VIEW) ||
										this.state.permissions.includes(Permission.C_CLUB_U_VIEW) ||
										this.state.permissions.includes(Permission.C_ATHLETE_U_VIEW) ||
										this.state.permissions.includes(Permission.C_COACH_U_VIEW) ||
										this.state.permissions.includes(Permission.C_COUNCIL_U_VIEW) ? (
											<Menu.Item key='4' onClick={this.handleOnClick}>
												<Link to={ROUTES.CREATE_USER} />
												Create Users{" "}
											</Menu.Item>
										) : null}
										{this.state.permissions.includes(Permission.M_U_VIEW) ? (
											<Menu.Item key='5' onClick={this.handleOnClick}>
												<Link to={ROUTES.MANAGE_USER} />
												Manage Users
											</Menu.Item>
										) : null}
										{this.state.permissions.includes(Permission.M_A_C_VIEW) ? (
											<Menu.Item key='6' onClick={this.handleOnClick}>
												<Link to={ROUTES.MANAGE_ROLES} />
												Manage Access Control{""}
											</Menu.Item>
										) : null}
									</SubMenu>
								</>
							) : null}
							{/* <Menu.Item
								key='7'
								icon={
									<i>
										<FontAwesomeIcon icon={faClipboardList} />
									</i>
								}
								style={
									!this.props.user.ProfilePersonalCompleted ||
									!this.props.user.ProfileContactCompleted
										? { display: "none" }
										: ""
								}
								onClick={this.handleOnClick}
							>
								Coach Management
							</Menu.Item>

							<Menu.Item
								key='8'
								icon={
									<i>
										<FontAwesomeIcon icon={faClipboard} />
									</i>
								}
								style={
									!this.props.user.ProfilePersonalCompleted ||
									!this.props.user.ProfileContactCompleted
										? { display: "none" }
										: ""
								}
								onClick={this.handleOnClick}
							>
								Athlete Management
							</Menu.Item> */}
							{UserTypes == "Athlete" && this.state.permissions.includes(Permission.M_S_VIEW) ? (
								<>
									<Menu.Item
										key='25'
										icon={
											<i>
												<FontAwesomeIcon icon={faCalendarAlt} />
											</i>
										}
										title='My Schedule'
										className='default-backgroundColor'
									>
										<Link to={ROUTES.MY_SCHEDULES} />
										My Schedule{" "}
									</Menu.Item>
								</>
							) : null}
							{UserTypes == "Athlete" &&
							this.state.permissions.includes(Permission.TRAINING_PERFORMANCE_VIEW) ? (
								<>
									<Menu.Item
										key='30'
										icon={
											<i>
												<FontAwesomeIcon icon={faChartBar} />
											</i>
										}
										title='Training Performance'
										className='default-backgroundColor'
									>
										<Link to={ROUTES.TRAINING_PERFORMANCE} />
										Training Performance{" "}
									</Menu.Item>
								</>
							) : null}
							{UserTypes == "Athlete" &&
							this.state.permissions.includes(Permission.ATHLETE_DIET_CHART_VIEW) ? (
								<>
									<Menu.Item
										key='28'
										icon={
											<i>
												<FontAwesomeIcon icon={faSimCard} />
											</i>
										}
										title='Diet Chart View'
										className='default-backgroundColor'
									>
										<Link to={ROUTES.DIET_CHART_VIEW} />
										Diet Chart View{" "}
									</Menu.Item>
								</>
							) : null}
							{this.state.permissions.includes(Permission.GAME_SETUP_VIEW) ||
							this.state.permissions.includes(Permission.G_T_R_VIEW) ||
							this.state.permissions.includes(Permission.G_T_F_VIEW) ||
							this.state.permissions.includes(Permission.G_S_S_VIEW) ||
							this.state.permissions.includes(Permission.TRACK_HOST_COUNTRY_VIEW) ||
							this.state.permissions.includes(Permission.HOST_COUNTRY_VIEW) ? (
								<>
									<SubMenu
										key='9'
										icon={
											<i>
												<FontAwesomeIcon icon={faClipboardCheck} />
											</i>
										}
										title='Game Management'
										className='default-backgroundColor'
										onClick={this.handleOnClick}
									>
										{this.state.permissions.includes(Permission.GAME_SETUP_VIEW) ? (
											<Menu.Item key='10' onClick={this.handleOnClick}>
												<Link to={ROUTES.MANAGE_GAMES} />
												Game Setup{" "}
											</Menu.Item>
										) : null}
										{this.state.permissions.includes(Permission.G_S_S_VIEW) ? (
											<Menu.Item key='11' onClick={this.handleOnClick}>
												<Link to={ROUTES.SCHEDULE_SPORTS} />
												Schedule Sports{" "}
											</Menu.Item>
										) : null}
										{this.state.permissions.includes(Permission.HOST_COUNTRY_VIEW) ? (
											<Menu.Item key='12' onClick={this.handleOnClick}>
												<Link to={ROUTES.HOST_COUNTRY_ACTIVITY} />
												Host Country Activities{" "}
											</Menu.Item>
										) : null}
										{this.state.permissions.includes(Permission.TRACK_HOST_COUNTRY_VIEW) ? (
											<Menu.Item key='13' onClick={this.handleOnClick}>
												<Link to={ROUTES.TRACK_INVITE_USER_STATUS} />
												Track Invites{" "}
											</Menu.Item>
										) : null}
										{this.state.permissions.includes(Permission.G_T_R_VIEW) ? (
											<Menu.Item key='14' onClick={this.handleOnClick}>
												<Link to={ROUTES.GAMES_TRACK_RESULT} />
												Track Results{" "}
											</Menu.Item>
										) : null}
										{this.state.permissions.includes(Permission.G_T_F_VIEW) ? (
											<Menu.Item key='15' onClick={this.handleOnClick}>
												<Link to={ROUTES.GAMES_TRACK_FILES} />
												Uploaded File Status{" "}
											</Menu.Item>
										) : null}
									</SubMenu>
								</>
							) : null}
							{this.state.permissions.includes(Permission.G_M_S_VIEW) ? (
								<SubMenu
									key='16'
									icon={
										<i>
											<FontAwesomeIcon icon={faTasks} />
										</i>
									}
									title='Sport Management'
									className='default-backgroundColor'
									onClick={this.handleOnClick}
								>
									<Menu.Item key='17' onClick={this.handleOnClick}>
										<Link to={ROUTES.MANAGE_SPORT} />
										Manage Sport{" "}
									</Menu.Item>
								</SubMenu>
							) : null}
							{this.state.permissions.includes(Permission.DEPARTMENT_MANAGE_SETUP) ? (
								<SubMenu
									key='18'
									icon={
										<i>
											<FontAwesomeIcon icon={faBuilding} />
										</i>
									}
									title='Department Setup'
									className='default-backgroundColor'
									onClick={this.handleOnClick}
								>
									<Menu.Item key='19' onClick={this.handleOnClick}>
										<Link to={ROUTES.DEPARTMENT_SETUP} />
										Manage Department{" "}
									</Menu.Item>
								</SubMenu>
							) : null}
							{this.state.permissions.includes(Permission.TRAINING_PROGRAM_VIEW) ||
							this.state.permissions.includes(Permission.T_M_S_VIEW) ||
							this.state.permissions.includes(Permission.T_A_A_VIEW) ||(//UserTypes == "Coach" &&
							this.state.permissions.includes(Permission.TRACK_ATHLETE_PERFORMANCE_VIEW)) ? (
								<SubMenu
									key='20'
									icon={
										<i>
											<FontAwesomeIcon icon={faChalkboardTeacher} />
										</i>
									}
									title='Training Management'
									className='default-backgroundColor'
									onClick={this.handleOnClick}
								>
									{this.state.permissions.includes(Permission.TRAINING_PROGRAM_VIEW) ? (
										<Menu.Item key='21' onClick={this.handleOnClick}>
											<Link to={ROUTES.MANAGE_PROGRAM} />
											Manage Programme
										</Menu.Item>
									) : null}
									{this.state.permissions.includes(Permission.T_M_S_VIEW) ? (
										<Menu.Item key='22' onClick={this.handleOnClick}>
											<Link to={ROUTES.MANAGE_SCHEDULES} />
											Manage Schedule
										</Menu.Item>
									) : null}
									{this.state.permissions.includes(Permission.T_A_A_VIEW) ? (
										<Menu.Item key='23' onClick={this.handleOnClick}>
											<Link to={ROUTES.TRACK_ATHLETE_ATTENDANCE} />
											Track Athlete Attendance
										</Menu.Item>
									) : null}
									{UserTypes == "Athlete" || 
									this.state.permissions.includes(Permission.TRACK_ATHLETE_WELLNESS_VIEW) ? (
										<Menu.Item key='24' onClick={this.handleOnClick}>
											<Link to={ROUTES.ATHLETE_WELLNESS} />
											Athlete Wellness
										</Menu.Item>
									 ) : null}
									{//UserTypes == "Coach" &&
									
									this.state.permissions.includes(Permission.TRACK_ATHLETE_PERFORMANCE_VIEW) ? (
									<Menu.Item key='31' onClick={this.handleOnClick}>
										<Link to={ROUTES.TRACK_ATHLETE_PERFORMANCE} />
										Track Athlete Performance
									</Menu.Item>
										) : null}
								</SubMenu>
							) : null}

							{this.state.permissions.includes(Permission.MANAGE_FOOD_VIEW) ||
							this.state.permissions.includes(Permission.DIET_CHART_VIEW) ? (
								<SubMenu
									key='29'
									icon={
										<i>
											<img src={DietMantNewLogo} style={{ height: "26px" }} />
										</i>
									}
									title='Diet Management'
									className='default-backgroundColor'
									onClick={this.handleOnClick}
								>
									{this.state.permissions.includes(Permission.MANAGE_FOOD_VIEW) ? (
										<Menu.Item key='26' onClick={this.handleOnClick}>
											<Link to={ROUTES.DIET_MANAGE_FOOD}>Manage Food</Link>
										</Menu.Item>
									) : null}
									{this.state.permissions.includes(Permission.DIET_CHART_VIEW) ? (
										<Menu.Item key='27' onClick={this.handleOnClick}>
											<Link to={ROUTES.DIET_MANAGE_CHART}>Diet Chart</Link>
										</Menu.Item>
									) : null}
								</SubMenu>
							) : null}
						</Menu>
					</div>
				</Sider>
			</Layout>
		)
	}
}

const mapPropsToState = (state) => ({
	user: state.userReducer,
})

const mapPropsToDispatch = (dispatch) => ({
	SetPersonCompleted: (PersonCompleted) => {
		dispatch(ReduxAction.setPersoanlCompleted(PersonCompleted))
	},
	SetContactCompleted: (ContactCompleted) => {
		dispatch(ReduxAction.setContactCompleted(ContactCompleted))
	},
})
export default PermissionProvider(
	connect(mapPropsToState, mapPropsToDispatch)(withRouter(SideMenu))
)
