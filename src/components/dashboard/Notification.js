import React, { useState, useEffect } from "react"
import { faBell } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Empty, Select } from "antd"
import * as $ from "jquery"
import { Badge } from "react-bootstrap"
import { APIRequest } from "../../components/api-manager/apiRequest"
import * as APIUrl from "../../components/api-manager/apiConstant"

const Notification = () => {
	const [notificationData, setNotificationData] = useState([])
	const [notifyDataLength, setNotifyDataLength] = useState("")
	const [unReadNotifyLength, setUnReadNotifyLength] = useState("")

	const viewNotificationtab = () => {
		APIRequest.getGetService(APIUrl.ROUTE_USER_NOTIFICATION_DETAIL_LIST).then((res) => {
			if (res.Succeeded) {
				setNotificationData(res.Data)
				setNotifyDataLength(res.Data.length)
				if (res.Data.length > 0) {
					setUnReadNotifyLength(res.Data[0].UnreadCount)
				}
			}
		})
	}

	$(".notification-drop .item").on("click", function () {
		$(this).find("ul").toggle()
	})

	useEffect(() => {
		APIRequest.getGetService(APIUrl.ROUTE_USER_NOTIFICATION_DETAIL_LIST).then((res) => {
			if (res.Succeeded) {
				setNotificationData(res.Data)
				setNotifyDataLength(res.Data.length)
				if (res.Data.length > 0) {
					setUnReadNotifyLength(res.Data[0].UnreadCount)
				}
			}
		})
	}, [])

	return (
		<>
			<div className='header-bell-section '>
				<ul className='notification-drop'>
					<li className='item'>
						<i
							className='notification-bell notify-bell-font'
							onClick={viewNotificationtab}
							aria-hidden='true'
						>
							<FontAwesomeIcon icon={faBell} />
						</i>
						<Badge
							pill
							variant='danger'
							style={{
								position: "absolute",
								margin: "14px -6px 0px -10px",
								fontSize: "60%",
							}}
						>
							{unReadNotifyLength}
						</Badge>
						{notifyDataLength > 0 ? (
							<ul>
								{notificationData.map((item, index) => (
									<li className='un-read-items'>{item.Content}</li>
								))}
							</ul>
						) : (
							<ul>
								<li>
									<Empty className='p-5'></Empty>
								</li>
							</ul>
						)}
					</li>
				</ul>
			</div>
		</>
	)
}
export default Notification
