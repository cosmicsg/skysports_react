import React from "react"
import AppLogo from "../content/images/logoIndex.png"
import "../styles/menu.css"
import { faBars, faBell } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Layout, Button } from "antd"
import {
	UploadOutlined,
	UserOutlined,
	VideoCameraOutlined,
	MenuUnfoldOutlined,
	MenuFoldOutlined,
	LaptopOutlined,
	NotificationOutlined,
} from "@ant-design/icons"

const { Header } = Layout

class header extends React.Component {
	constructor() {
		super()
	}
	render() {
		return (
			<div className=''>
				<header
					className='flex w-full header-height'
					style={{ position: "fixed", zIndex: 1, width: "100%" }}
				>
					{/* <div class="flex"> */}
					<span className='flex w-1/3 text-white text-center'></span>
					<span className='flex w-1/3 text-white text-center'></span>
					<span className='header-bell-right-pad flex w-1/3 text-white justify-end'>
						<div class='dropdown'>
							<a id='dLabel' role='button' data-toggle='dropdown' data-target='#' href='/page.html'>
								<i class='glyphicon glyphicon-bell'></i>
							</a>

							<ul class='dropdown-menu notifications' role='menu' aria-labelledby='dLabel'>
								{/* <div class='notification-heading'>
									<h4 class='menu-title'>Notifications</h4>
									<h4 class='menu-title pull-right'>
										View all<i class='glyphicon glyphicon-circle-arrow-right'></i>
									</h4>
								</div> */}
								<li class='divider'></li>
								<div class='notifications-wrapper'>
									<a class='content' href='#'>
										<div class='notification-item'>
											<h4 class='item-title'>Evaluation Deadline 1 · day ago</h4>
											<p class='item-info'>Marketing 101, Video Assignment</p>
										</div>
									</a>
								</div>
								<li class='divider'></li>
								<div class='notification-footer'>
									<h4 class='menu-title'>
										View all<i class='glyphicon glyphicon-circle-arrow-right'></i>
									</h4>
								</div>
							</ul>
						</div>
					</span>
				</header>
			</div>
		)
	}
}

export default header
