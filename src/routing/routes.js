import React from "react"
import LayoutArea from "../layout/layout"
import { Switch, Route } from "react-router-dom"
import PageNotFound from "../error/PageNotFound"
import PageUnAuthorize from "../error/PageUnAuthorize"
import { ROUTES } from "./routeConstants"
import PrivateRoute from "./privateRoute"
import Login from "../pages/common/Login"
import ForgotPassword from "../pages/common/ForgotPassword"
import ChangePassword from "../pages/common/Changepassword"
import { connect } from "react-redux"
import LinkExpired from "../error/LinkExpired"

const Routing = (props) => {
	return (
		<Switch>
			{/* ADD PUBLIC ROUTES HERE */}
			<Route exact path={ROUTES.LOGIN} component={Login} />
			<Route exact path={ROUTES.FORGOT_PASSWORD} component={ForgotPassword} />
			<Route exact path={ROUTES.CHANGE_PASSWORD} component={ChangePassword} />
			<Route exact path={ROUTES.LINK_EXPIRED} component={LinkExpired} />
			{/* ENDS PUBLIC ROUTES */}

			{/* ADD PRIVATES ROUTES HERE */}
			<PrivateRoute
				path={ROUTES.LAYOUT}
				authenticate={props.userAuthenticate}
				component={LayoutArea}
			/>
			{/* ENDS PRIVATES ROUTES */}

			{/* ALWAYS BELOW ROUTES SHOULD BE LAST BECAUSE THE BELOW PAGES WILL GETS
                EXECUTED IF IT IS NOT FOUND ABOVE ANY OF THE ROUTINGS*/}
			<Route path={ROUTES.PAGE_NOT_FOUND} component={PageNotFound} />
			<Route path={ROUTES.UN_AUTHORIZE} component={PageUnAuthorize} />
			
		</Switch>
	)
}

const mapStateToProps = (state) => {
	return {
		userAuthenticate: state.userReducer,
	}
}

export default connect(mapStateToProps, null)(Routing)
