export const ROUTES = {
	//Login
	LOGIN: "/login",
	FORGOT_PASSWORD: "/forgot-password",
	CHANGE_PASSWORD: "/change-password/:id",
	LINK_EXPIRED: "/link-expired",
	//alway below code should be last
	UN_AUTHORIZE: "*",
	PAGE_NOT_FOUND: "*",

	//sample routes
	AUTH: "/auth",
	HOME: "/home",
	CONTENT: "/content",
	ABOUT: "/about",
	SEND_FORGOT_PASSWORD_URL: "http://localhost:3000/change-password",

	//Common layout
	DASHBOARD: "/home/dashboard",
	LAYOUT: "/home",
	PROFILE: "/home/profile",

	//Admin Module
	CREATE_USER: "/home/createUser",
	MANAGE_USER: "/home/manageUser",
	MANAGE_ROLES: "/home/manageRoles",

	//Games Module
	MANAGE_GAMES: "/home/manageGames",
	SCHEDULE_SPORTS: "/home/scheduleSports",
	MANAGE_SPORT: "/home/ManageSport",
	MANAGE_SPORTS_LEVEL: "/home/manageSportsLevel",
	HOST_COUNTRY_ACTIVITY: "/home/host-country-activity",
	CREATE_TEAM: "/home/create-team",
	TRACK_INVITE_USER_STATUS: "/home/game-track-invite-status",
	DEPARTMENT_SETUP: "/home/department-setup",
	GAMES_TRACK_RESULT: "/home/track-results",
	GAMES_TRACK_FILES: "/home/uploaded-files-status",
	TRAINING_PROGRESS: "/training/training-progress",
	MANAGE_PROGRAM: "/home/manage-program",
	MANAGE_SCHEDULES: "/home/manage-schedules",
	TRACK_ATHLETE_ATTENDANCE: "/home/track-athlete-attendance",
	ATHLETE_WELLNESS: "/home/athlete-wellness",
	//Diet Management
	DIET_MANAGE_FOOD: "/home/diet-manage-food",
	DIET_CHART_VIEW: "/home/diet-chart-view",
	// My Schedule module
	MY_SCHEDULES: "/home/my-schedules",

	DIET_MANAGE_CHART: "/home/diet-manage-chart",
	TRAINING_PERFORMANCE:"/home/training-performance",
	TRACK_ATHLETE_PERFORMANCE:"/home/track-athlete-performance"
}
