// import logo from './logo.svg';
import "./App.css";
import { Provider } from "react-redux";
import store from "./components/redux/store/store";
import Routing from "./routing/routes";
import GlobalErrorHandler from "./error/GlobalErrorHandler";

function App() {
  return (
    <GlobalErrorHandler>
      <Provider store={store}>
        <Routing />
      </Provider>
    </GlobalErrorHandler>
  );
}

export default App;
