export const Message = {
	INVITE_DELETE: "You are about to delete an invite(s)",
	INVITE_DELETE_CONFIRM: "This will delete your invite(s) from the list",
	INVITE_DELETE_CONFIRM1: "Are you sure?",

	ROLE_DELETE: "You are about to delete an role(s)",
	ROLE_DELETE_CONFIRM: "This will delete your role(s) from the list",
	ROLE_DELETE_CONFIRM1: "Are you sure?",

	GAME_SETUP_DELETE: "You are about to delete an Game Setup",
	GAME_DELETE_CONFIRM: "This will delete your Game Setup from the list",
	GAME_DELETE_CONFIRM1: "Are you sure?",
	HOST_ACTIVITY_DELETE_HEADER: "You are about to delete an Host",
	HOST_ACTIVITY_DELETE_CONFIRM: "This will delete Host country activity list",
	TEAM_DELETE_CONFIRM: "This will delete Team list",
	TEAM_DELETE_HEADER: "Your are about to delete an Host Activity",

	GAME_SPORTLEVEL_DELETE: "You are about to delete an Sport Level",
	GAME_SPORTLEVEL_CONFIRM: "This will delete your Sport Level from list",

	GAME_MASTERSPORT_DELETE: "You are about to delete an Sport",
	GAME_MASTERSPORT_CONFIRM: "This will delete your Sport from list",

	DELETE_TRAINING_PROGRAM_HEADING: "You are about to delete Program",
	DELETE_TRAINING_PROGRAM_BODY: "This will delete your Training Program",

	DELETE_FOOD_ITEM_HEADING: "You are about to delete Food Item",
	DELETE_FOOD_ITEM_BODY: "This will delete your Food Item",

	DELETE_TRAINING_DIET_CHART_HEADING: "You are about to delete Diet Chart",
	DELETE_TRAINING_DIET_CHART_BODY: "This will delete your Diet Chart",

	TRACK_RESULTS_DELETE_HEADING: "Your are about to delete your Score",
	TRACK_RESULTS_DELETE_BODY: "This will delete your Score",

	UPLOAD_FILE_DELETE_HEADING:"Your are about to delete your Uploaded File ",
	UPLOAD_FILE_DELETE_BODY:"This will delete your Uploaded File ",


}
