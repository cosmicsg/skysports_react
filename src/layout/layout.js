import React from "react"
import { withRouter } from "react-router-dom"
import { Layout, Row, Col } from "antd"
import SiderPage from "../components/dashboard/SideMenu"
//import ContentPage from "../../src/pages/games/ScheduleSports/CalandarView"
import ContentPage from "../components/dashboard/Content"
import Notification from "../components/dashboard/Notification"
import * as ReduxAction from "../components/redux/actions/userAction" //"components/redux/actions/userAction"
import { connect } from "react-redux"
import LogoutIcon from "../content/images/logout.png"
import { ROUTES } from "../routing/routeConstants"

const { Header, Sider, Content } = Layout
class LayoutArea extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			isAuthenticated: true,
		}
		if (performance.navigation.type === 1) {
			let permission = sessionStorage.getItem("APP_PERMISSION")
			if (permission !== null && permission !== undefined && permission !== "") {
				permission = permission.split(",").map(function (item) {
					return parseInt(item, 10)
				})
				this.props.setPermission(permission)
			}
			let userType = sessionStorage.getItem("USER_TYPE")
			if (userType !== null && userType !== undefined && userType !== "") {
				this.props.setUserType(userType)
			}
			let invitedid = sessionStorage.getItem("INVITED_ID")
			if (invitedid !== null && invitedid !== undefined && invitedid !== "") {
				this.props.SetInvitedUserID(invitedid)
			}
			let profileID = sessionStorage.getItem("PROFILE_ID")
			if (profileID !== null && profileID !== undefined && profileID !== "") {
				this.props.setUserProfileID(profileID)
			}
			this.state = {
				isAuthenticated: true,
			}
			this.props.SetPersonCompleted(sessionStorage.getItem("PersonalCompleted"))

			this.props.SetContactCompleted(sessionStorage.getItem("ContactCompleted"))
		}
	}
	handleClick = () => {
		sessionStorage.clear()
		this.props.history.replace(ROUTES.LOGIN)
	}
	handleLogout = () => {
		sessionStorage.clear()
		this.props.SetFullName("")
		this.props.SetUserEmail("")
		this.props.history.replace(ROUTES.LOGIN)
	}
	render() {
		return (
			<>
				{/* <SessionTimeout isAuthenticated={this.state.isAuthenticated} logOut={this.handleClick} /> */}
				<Layout className='background-body-color overflow-auto'>
					<Sider className='transbox-body-sidebar-bg default-backgroundColor sidebar-scroll-overflow-auto sidebar-width-radius'>
						<SiderPage />
					</Sider>
					<Layout className='transbox-body-color  h-100 '>
						<Header>
							<Row className='header-padding '>
								<Col span={18} push={6}>
									<div className='float-right'>
										<div className='w-full justify-end'>
											<Notification />
											<a>
												<img
													className=''
													style={{
														margin: "1rem",
														height: "2rem",
														cursor: "pointer",
													}}
													src={LogoutIcon}
													onClick={this.handleLogout}
													alt='Logout'
												></img>
											</a>
										</div>
									</div>
								</Col>
							</Row>
						</Header>
						<Content className=' h-100' style={{}}>
							<ContentPage />
						</Content>
					</Layout>
				</Layout>
			</>
		)
	}
}

const mapPropsToState = (state) => ({
	user: state.userReducer,
})

const mapPropsToDispatch = (dispatch) => ({
	setPermission: (permissions) => {
		dispatch(ReduxAction.addPermission(permissions))
	},
	SetPersonCompleted: (PersonCompleted) => {
		dispatch(ReduxAction.setPersoanlCompleted(PersonCompleted))
	},
	SetContactCompleted: (ContactCompleted) => {
		dispatch(ReduxAction.setContactCompleted(ContactCompleted))
	},
	setUserType: (UserType) => {
		dispatch(ReduxAction.setUserType(UserType))
	},
	setUserProfileID: (UserProfileID) => {
		dispatch(ReduxAction.addUserPofileId(UserProfileID))
	},
	SetFullName: (UserFullname) => {
		dispatch(ReduxAction.addFullName(UserFullname))
	},
	SetUserEmail: (UserEmail) => {
		dispatch(ReduxAction.addEmail(UserEmail))
	},
	SetInvitedUserID: (InvitedUserID) => {
		dispatch(ReduxAction.setInvitedUserID(InvitedUserID))
	},
})

export default connect(mapPropsToState, mapPropsToDispatch)(withRouter(LayoutArea))
