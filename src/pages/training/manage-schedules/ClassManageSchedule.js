import React, { Component } from "react"
import { Modal, Button, Card, Collapse, Container, Row, Col } from "react-bootstrap"
import ReactTable from "react-table"
import "../../../styles/CosmicTable.css" //"../../../styles/CosmicTable.css"
import "antd/dist/antd.css"
import "../../../styles/grid.css"
import "../../../styles/Games.css"
import "../../../styles/menu.css"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { Select, Input, DatePicker, notification, Spin, Space } from "antd"
import Pagination from "../../../utils/Pagination"
import ExportData from "../../../utils/ExportData"
import {
	faEdit,
	faEye,
	faTrashAlt,
	faArrowUp,
	faArrowDown,
	faDownload,
	faPlusCircle,
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import moment from "moment"
import { Permissions as Permission } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"
import { connect } from "react-redux"

const { Option } = Select

const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	} else if (flag === 2) {
		//resend flag
		notification[type]({
			message: description,
			description: "",
			//  duration:20
		})
	}
}

const setIsExpanded = (data, self) => {
	let Icons = self.state.Icons === true ? false : true
	self.setState({ IsExpanded: data, Icons: Icons })
}

const NoDataConst = (props) => (
	<span className='table-nodata-style'>
		No records found Please check your connection or change your filters{" "}
	</span>
)

const openModal = (rowid, self, flg, id) => {
	self.setState({
		IsOpen: true,
		IsDeleteId: id,
		IsDeleteRowId: rowid,
		IsDeleteflag: flg,
		IsFromList: false,
	})
}

//set open model property
const openDelModal = (id, self) => {
	self.setState({ IsOpen: true, IsDeleteId: id, IsFromList: true })
}

const handleTblDelete = (id, self) => {
	self.setState({ loading: true })
	APIRequest.getGetService(APIUrl.DELETE_TRAINING_SCHEDULE_BY_SCHEDULE_ID + "/" + id)
		.then((response) => {
			self.setState({ loading: false })
			if (response.Data === true) {
				self.fetch(self.state)
				openNotificationWithIcon("success", "Record has been deleted", 1)
			}
		})
		.catch((error) => {
			self.setState({ loading: false })
		})
}

//Export label
const exportLabel = [
	{ columnName: "S.No", value: "SerialNo" },
	{ columnName: "Schedules", value: "ScheduleName" },
	{ columnName: "Program", value: "ProgramName" },
	{ columnName: "Sport", value: "SportName" },
	{ columnName: "Week/ Month", value: "WeekMonth" },
	{ columnName: "Start Date", value: "StartDate" },
	{ columnName: "End Date", value: "EndDate" },
]
const handlerFormatDate = (date) => {
	var d = new Date(date),
		month = "" + (d.getMonth() + 1),
		day = "" + d.getDate(),
		year = d.getFullYear()
	if (month.length < 2) month = "0" + month
	if (day.length < 2) day = "0" + day
	return [year, month, day].join("-")
}

class ClassManageSchedule extends Component {
	constructor(props) {
		super(props)

		let permissions = this.props.getAppPermissions()

		let roles = []

		this.state = {
			errors: {},
			selected: {},
			show: false,
			isEyeDatapopup: false,
			isAddSchedule: false,
			isEyeData: [],
			isAddEditDatapopup: false,
			isEdit: false,
			ID: 0,
			TrainingScheduleList: [],
			AddWorkText: "Add WorkOut",
			IsRefreshList: false,
			EditRow: {},
			RowCount: {},
			EditScheduleName: "",
			EditTrainingProgramID: "",
			EditSportID: "",
			EditWeekMonthID: "",
			EditRestTimeID: "",
			EditStartDate: "",
			EditEndDate: "",
			EditTrainingScheduleID: 0,
			EditSportNameData: [],
			EditWorkoutData: [],
			WorkoutList: [],
			IsOpen: false,
			IsFromList: false,
			IsDeleteId: 0,
			IsDeleteflag: false,
			WorkoutID: [],
			NoOfSets: 0,
			NoOfReps: 0,
			TimeID: [],
			TotalPages: 0,
			TotalRecords: 0,
			pageSize: 10,
			TotalCount: 0,
			SortColumn: "",
			SortValue: "",
			SDateValidate: 0,
			FilterTrainingScheduleID: [],
			FilterTrainingProgramID: [],
			FilterSportID: [],
			FilterWeekMonthID: [],
			FilterStartDate: "",
			FilterEndDate: "",
			DisableSearch: true,
			DocumentType: [],
			TrainingScheduleID: [],
			TrainingScheduleData: [],
			TrainingProgramID: [],
			TrainingProgramData: [],
			SportID: [],
			SportNameData: [],
			WeekMonthID: [],
			WeekMonthData: [],
			RestTimeIDData: [],
			WorkoutTimeIDData: [],
			StartDate: "",
			EndDate: "",
			IsExpanded: false,
			Icons: false,
			disableSearch: true,
			exportbtn: true,
			loading: false,
			isEditData: [],
			AddStartDate: "",
			AddEndDate: "",
			RecordAlreadyExists: "",
			IsExportExcel: false,
			exportData: [],
			exportFilename: "Manage Schedules",
			EndDateDisabled: true,
			permissions: permissions,
			roles: roles,
		}
		this.setRef = React.createRef()
		this.table = React.createRef()
		this.fetch = this.fetch.bind(this)
		this.toggleRow = this.toggleRow.bind(this)
		this.handleClearFilters = this.handleClearFilters.bind(this)
		this.handleOnchange = this.handleOnchange.bind(this)
		this.handleBtnDelete = this.handleBtnDelete.bind(this)
		this.tableColumnDataLimit = this.tableColumnDataLimit.bind(this)
	}

	tableColumnDataLimit = (str) => {
		if (str !== undefined && str !== null) {
			return str.length > 30 ? str.substring(0, 27) + "..." : str
		}
	}

	handleBtnDelete = (event) => {
		var InputData = []
		let self = this
		//let result = 0
		let selectedRowIds = this.handlerGetListIDs(this.state.selected, this.state.users)
		if (selectedRowIds) {
			self.setState({
				loading: true,
			})
			InputData = selectedRowIds.toString()
			APIRequest.getGetService(APIUrl.DELETE_MASTER_SPORTS + "/" + InputData)
				.then((response) => {
					self.setState({
						loading: false,
					})
					if (response.Data === true) {
						openNotificationWithIcon("success", "Record has been deleted", 1)
						self.setState({ fields: [], rowcount: 0, selected: [] })
						self.fetch(this.table.current.state)
					}
				})
				.catch((error) => { })
		}
	}

	// It is used to start loader
	handlerLoadingStart = () => {
		this.setState({ loading: true })
	}
	// It is used to stop loader
	handlerLoadingStop = () => {
		this.setState({ loading: false })
	}
	handleOnchange = (name, val) => {
		let searchbtn = true
		if (val !== "" && val !== undefined && val !== null && val.length !== 0) {
			searchbtn = false
		}

		//update value based on onchange event for particular fields
		this.setState({
			...this.state,
			[name]: val,
			searchbtn: searchbtn,
		})
	}

	handleOnchange = (name, val) => {
		this.setState({
			...this.state,
			[name]: val,
			disableSearch:
				val !== "" && val !== undefined && val !== null && val.length !== 0 ? false : true,
		})
	}

	//Search Filter Clear
	handleClearFilters = () => {
		this.setState(
			{
				...this.state,
				FilterTrainingScheduleID: [],
				FilterTrainingProgramID: [],
				FilterSportID: [],
				FilterWeekMonthID: [],
				FilterStartDate: "",
				FilterEndDate: "",
				disableSearch: true,
				isRefreshList: true,
			},
			function () {
				this.fetch(this.table.current.state)
			}
		)
	}
	NoDataConst = (props) => <span className='table-nodata-style'>No records found </span>

	toggleRow(SerialNo) {
		const newSelected = Object.assign({}, this.state.selected)
		newSelected[SerialNo] = !this.state.selected[SerialNo]
		this.setState({
			selected: newSelected,
			selectAll: 2,
		})
		let flag = 0
		for (const [value] of Object.entries(newSelected)) {
			if (value === true) {
				flag = 1
			}
		}
		if (flag === 1) {
			this.setState({
				deletebtn: false,
			})
		} else {
			this.setState({
				deletebtn: true,
			})
		}
	}
	//Fetch data from React Table

	async fetch(state) {
		let sortbyname = "ScheduleName"
		let orderby = "DESC"
		if (state.sorted !== undefined) {
			if (state.sorted.length !== 0) {
				sortbyname = state.sorted[0].id
				if (state.sorted[0].desc === true) {
					orderby = "DESC"
				}
				if (state.sorted[0].desc === false) {
					orderby = "ASC"
				}
			}
		}
		let pagelimit = 0
		let self = this
		if (state.Page === undefined || state.pageSize === undefined) {
			pagelimit = this.state.pageSize
		} else {
			pagelimit = state.pageSize
		}
		let startDate = ""
		if (this.state.FilterStartDate !== "") {
			startDate = moment(this.state.FilterStartDate).format("DD MMM YY")
		}
		let endDate = ""
		if (this.state.FilterEndDate !== "") {
			endDate = moment(this.state.FilterEndDate).format("DD MMM YY")
		}
		let data = {
			TrainingScheduleID: this.state.FilterTrainingScheduleID.toString(),
			TrainingProgramID: this.state.FilterTrainingProgramID.toString(),
			SportID: this.state.FilterSportID.toString(),
			WeekMonthID: this.state.FilterWeekMonthID.toString(),
			StartDate: startDate.toString(),
			EndDate: endDate.toString(),
			PageNo: state > 0 ? state : 1,
			PageSize: pagelimit,
			SortColumn: sortbyname,
			SortOrder: orderby,
		}
		self.setState({ loading: true })
		APIRequest.getPostService(APIUrl.GET_MANAGE_SCHEDULE_LIST, data)
			.then((response) => {
				if (response.Data.length > 0) {
					const firstRow = response.Data.find((x) => x)
					self.setState({
						loading: false,
						TrainingScheduleList: response.Data,
						TotalPages: firstRow.TotalPages,
						TotalRecords: firstRow.TotalRows,
						TotalCount: response.Data.length >= 1 ? response.Data.length : 0,
						SortColumn: sortbyname,
						SortValue: orderby,
						exportbtn: false,
					})
				} else {
					self.setState({
						exportbtn: true,
					})
				}
				self.setState({ loading: false, TrainingScheduleList: response.Data })
			})
			.catch((error) => {
				self.setState({ loading: false })
			})
	}
	handlerExportData = () => {
		let self = this
		let startDate = ""
		if (self.state.FilterStartDate !== "") {
			startDate = moment(self.state.FilterStartDate).format("DD MMM YY")
		}
		let endDate = ""
		if (self.state.FilterEndDate !== "") {
			endDate = moment(self.state.FilterEndDate).format("DD MMM YY")
		}
		let InputData = {
			TrainingScheduleID: self.state.FilterTrainingScheduleID.toString(),
			TrainingProgramID: self.state.FilterTrainingProgramID.toString(),
			SportID: self.state.FilterSportID.toString(),
			WeekMonthID: self.state.FilterWeekMonthID.toString(),
			StartDate: startDate.toString(),
			EndDate: endDate.toString(),
			PageNo: 1,
			PageSize: self.state.TotalRecords,
			SortColumn: self.sortbyname,
			SortOrder: self.orderby,
		}

		APIRequest.getPostService(APIUrl.GET_MANAGE_SCHEDULE_LIST, InputData)
			.then((response) => {
				if (response.Data.length > 0) {
					self.setState({
						exportData: response.Data,
						IsExportExcel: true,
					})
				}
			})
			.catch((error) => { })
	}

	pageNo = 2
	handlerPagination = (pageNo) => {
		this.fetch(pageNo)
	}

	openViewAlldataModal = (data, self) => {
		self.setState({
			isEyeDatapopup: true,
			isEyeData: data,
			isEdit: false,
		})
	}

	viewAlldatapopupClose = () => {
		this.setState({
			isEyeDatapopup: false,
			isEyeData: [],
			errors: {},
			AddStartDate: "",
			AddEndDate: "",
			EditSportNameData: [],
			EditWorkoutData: [],
			isAddEditDatapopup: false,
			isEdit: false,
			isEditData: [],
		})
	}

	openEditAlldataModal = (data, self) => {
		let trainingScheduleID = data.TrainingScheduleID > 0 ? data.TrainingScheduleID : 0
		//Load workout data
		APIRequest.getGetService(
			APIUrl.GET_TRAINING_WORKOUT_BY_SCHEDULE_ID + "/" + trainingScheduleID
		).then((res) => {
			if (res.Succeeded) {
				self.setState({
					...self.state,
					WorkoutList: res.Data,
					isEdit: true,
					isAddEditDatapopup: true,
					EditScheduleName: data.ScheduleName,
					EditTrainingProgramID: data.TrainingProgramID,
					EditSportID: data.SportID,
					EditWeekMonthID: data.WeekMonthID,
					EditRestTimeID: data.RestTimeID,
					EditStartDate: moment(data.StartDate),
					EditEndDate: moment(data.EndDate),
					EditTrainingScheduleID: data.TrainingScheduleID,
				})
			}
		})

		let trainingProgramID = data.TrainingProgramID > 0 ? data.TrainingProgramID : 0
		//Load sports data
		APIRequest.getPostService(APIUrl.GET_TRAINING_PROGRAM_DATE_ID + "/" + trainingProgramID).then(
			(res) => {
				if (res.Succeeded) {
					this.setState({
						...this.state,
						AddStartDate: moment(res.Data.StartDate),
						AddEndDate: moment(res.Data.EndDate),
						EditSportNameData: res.Data.LstSport,
					})
				}
			}
		)

		let workoutSportID = data.SportID > 0 ? data.SportID : 0

		APIRequest.getGetService(APIUrl.GET_WORKOUT_BY_SPORT_ID + "/" + workoutSportID).then((res) => {
			if (res.Succeeded) {
				this.setState({
					...this.state,
					EditWorkoutData: res.Data,
				})
			}
		})
		// self.setState({
		// 	...this.state,
		// 	isEdit: true,
		// 	isAddEditDatapopup: true,
		// 	EditScheduleName: data.ScheduleName,
		// 	EditTrainingProgram: data.TrainingProgramID,
		// 	EditSportID: data.SportID,
		// 	EditWeekMonthID: data.WeekMonthID,
		// 	EditRestTimeID: data.RestTimeID,
		// 	EditStartDate: moment(data.StartDate),
		// 	EditEndDate: moment(data.EndDate),
		// 	ID: data.TrainingScheduleID,
		// })
	}

	handleScheduleNameOnchange = (e) => {
		let errors = {}
		let scheduleName = e.target.value
		if (scheduleName !== "" && scheduleName !== undefined && scheduleName !== null) {
			errors["scheduleName"] = ""
			this.setState({
				...this.state,
				errors: errors,
			})
		}
		this.setState({
			...this.state,
			EditScheduleName: scheduleName,
		})
	}

	handleTrainingProgramOnchange = (editTrainingProgram) => {
		let errors = {}
		if (
			editTrainingProgram !== "" &&
			editTrainingProgram !== undefined &&
			editTrainingProgram !== null
		) {
			errors["editTrainingProgram"] = ""
			this.setState({
				...this.state,
				errors: errors,
				EditTrainingProgramID: editTrainingProgram,
				EditSportNameData: [],
				EditSportID: [],
			})
			let trainingProgramID = editTrainingProgram > 0 ? editTrainingProgram : 0
			//Load sports data
			APIRequest.getPostService(APIUrl.GET_TRAINING_PROGRAM_DATE_ID + "/" + trainingProgramID).then(
				(res) => {
					if (res.Succeeded) {
						this.setState({
							...this.state,
							AddStartDate: moment(res.Data.StartDate,"YYYY-MM-DD"),
							AddEndDate: moment(res.Data.EndDate,"YYYY-MM-DD").add(1, 'days'),
							EditSportNameData: res.Data.LstSport,
						})
					}
				}
			)
		} else {
			errors["editTrainingProgram"] = "Please select the program"
			this.setState({ ...this.state, errors: errors })
		}
	}

	handleSportsOnchange = (sportID) => {
		let errors = {}
		if (sportID !== "" && sportID !== undefined && sportID !== null) {
			errors["sportID"] = ""
			this.setState({
				...this.state,
				errors: errors,
			})
			let workoutSportID = sportID > 0 ? sportID : 0

			APIRequest.getGetService(APIUrl.GET_WORKOUT_BY_SPORT_ID + "/" + workoutSportID).then(
				(res) => {
					if (res.Succeeded) {
						this.setState({
							...this.state,
							EditSportID: sportID,
							EditWorkoutData: res.Data,
						})
					}
				}
			)
		} else {
			errors["editTrainingProgram"] = "Please select the Sport"
			this.setState({ ...this.state, errors: errors })
		}
	}

	handleweekMonthIDOnchange = (EditWeekMonthID) => {
		let errors = {}
		if (EditWeekMonthID !== "" && EditWeekMonthID !== undefined && EditWeekMonthID !== null) {
			errors["EditWeekMonthID"] = ""
			this.setState({
				...this.state,
				errors: errors,
			})
		}

		this.setState({
			...this.state,
			EditWeekMonthID: EditWeekMonthID,
		})
	}

	handlerestTimeIDOnchange = (restTimeID) => {
		let errors = {}
		if (restTimeID !== "" && restTimeID !== undefined && restTimeID !== null) {
			errors["restTimeID"] = ""
			this.setState({
				...this.state,
				errors: errors,
			})
		}
		this.setState({
			...this.state,
			EditRestTimeID: restTimeID,
		})
	}

	handleStartDateOnchange = (startDate) => {
		this.setState({
			...this.state,
			EditStartDate: startDate,
			EditEndDate: "",
			EndDateDisabled: false,
		})
	}

	DynamicdisabledDate = (current) => {
		let date = this.state.StartDate
		var dateconvert = new Date(date)
		var expiryDate = dateconvert.setDate(new Date(dateconvert).getDate() + 1)
		let dynamicexpiryDate = new Date(expiryDate)
		let Day = ("0" + dynamicexpiryDate.getDate()).slice(-2)
		let month = ("0" + (dynamicexpiryDate.getMonth() + 1)).slice(-2)
		let dataformet = dynamicexpiryDate.getFullYear() + "-" + month + "-" + Day
		const start = moment(dataformet, "YYYY-MM-DD")
		return current < start || current < moment()
		//return moment().add(-1, 'days') >= current
	}

	handleEndDateOnchange = (endDate) => {
		this.setState({
			...this.state,
			EditEndDate: endDate,
		})
	}
	handleDropdownChange = (cellInfo, event) => {
		let workoutLists = [...this.state.WorkoutList]
		workoutLists[cellInfo.index][cellInfo.column.id] = event
		this.setState({
			...this.state,
			WorkoutList: workoutLists,
		})
	}
	handleTextFieldsChange = (event, cellInfo) => {
		let workoutLists = [...this.state.WorkoutList]
		workoutLists[cellInfo.index][cellInfo.column.id] = event.target.value
		this.setState({
			...this.state,
			WorkoutList: workoutLists,
		})
	}
	handleWorkoutTextFieldsChange = (cellInfo, event) => {
		let workoutLists = [...this.state.WorkoutList]
		workoutLists[cellInfo.index][cellInfo.column.id] = event.target.value
		this.setState({
			...this.state,
			WorkoutList: workoutLists,
		})
	}
	handleFieldValidation = () => {
		let isErrorExist = false
		let errors = {}
		if (
			this.state.EditScheduleName === "" ||
			this.state.EditScheduleName === undefined ||
			this.state.EditScheduleName === null
		) {
			errors["EditScheduleName"] = "Please enter schedule"
			isErrorExist = true
		}
		if (
			this.state.EditTrainingProgramID.length === 0 ||
			this.state.EditTrainingProgramID === undefined ||
			this.state.EditTrainingProgramID === null
		) {
			errors["EditTrainingProgram"] = "Please select program"
			isErrorExist = true
		}
		if (
			this.state.EditSportID.length === 0 ||
			this.state.EditSportID === undefined ||
			this.state.EditSportID === null
		) {
			errors["EditSportID"] = "Please select sports"
			isErrorExist = true
		}
		if (
			this.state.EditWeekMonthID.length === 0 ||
			this.state.EditWeekMonthID === undefined ||
			this.state.EditWeekMonthID === null
		) {
			errors["EditWeekMonthID"] = "Please select Week/Month"
			isErrorExist = true
		}
		if (
			this.state.EditStartDate === "" ||
			this.state.EditStartDate === undefined ||
			this.state.EditStartDate === null
		) {
			errors["EditStartDate"] = "Please select Start Date"
			isErrorExist = true
		}
		if (
			this.state.EditEndDate.length === 0 ||
			this.state.EditEndDate === undefined ||
			this.state.EditEndDate === null
		) {
			errors["EditEndDate"] = "Please select End Date"
			isErrorExist = true
		}
		if (
			this.state.WorkoutList.length === 0 ||
			this.state.WorkoutList === undefined ||
			this.state.WorkoutList === null
		) {
			errors["WorkoutList"] = "Please enter Workout"
			isErrorExist = true
		}
		if (
			this.state.EditRestTimeID.length === 0 ||
			this.state.EditRestTimeID === undefined ||
			this.state.EditRestTimeID === null
		) {
			errors["EditRestTimeID"] = "Please enter Rest Time"
			isErrorExist = true
		}
		let rowData = this.state.WorkoutList.length
		let workoutLists = this.state.WorkoutList
		if (rowData >= 1 && workoutLists !== undefined) {
			let filteredArr = workoutLists.reduce((acc, current) => {
				const x = acc.find(
					(item) => item.WorkoutID === current.WorkoutID && item.TimeID === current.TimeID
				)
				if (!x) {
					return acc.concat([current])
				} else {
					return acc
				}
			}, [])

			let empltytemp = workoutLists.filter(
				(x) =>
					x.TimeID.length === 0 || x.WorkoutID.length === 0 || x.NoOfReps === 0 || x.NoOfSets === 0
			)
			if (empltytemp.length > 0) {
				errors["workoutLists"] = "Please enter the workout details."
				isErrorExist = true
			}
			if (filteredArr.length !== rowData) {
				errors["workoutLists"] = "Duplicate Workouts not allowed."
				isErrorExist = true
			}
		}

		this.setState({
			...this.state,
			errors: errors,
		})
		return isErrorExist
	}

	handleSave = (self) => {
		let isValid = this.handleFieldValidation()
		if (!isValid) {
			let selectdata = self.state.WorkoutList
			var InputData = []
			var obj = {}
			for (var i = 0; i < selectdata.length; i++) {
				let StartDate = handlerFormatDate(this.state.EditStartDate)
				let EndDate = handlerFormatDate(this.state.EditEndDate)
				let data = self.state.WorkoutList[i]

				obj["WorkoutID"] = data.WorkoutID.toString()
				obj["NoOfReps"] = data.NoOfReps
				obj["NoOfSets"] = data.NoOfSets
				obj["TimeID"] = data.TimeID.toString()
				obj["ScheduleName"] = this.state.EditScheduleName
				obj["TrainingProgramID"] = this.state.EditTrainingProgramID.toString()
				obj["SportID"] = this.state.EditSportID.toString()
				obj["WeekMonthID"] = this.state.EditWeekMonthID.toString()
				obj["StartDate"] = StartDate
				obj["EndDate"] = EndDate
				obj["RestTimeID"] = this.state.EditRestTimeID.toString()
				obj["TrainingScheduleID"] = this.state.EditTrainingScheduleID

				InputData.push(obj)
				obj = {}
			}
			APIRequest.getPostService(APIUrl.ROUTE_ADD_MANAGE_SCHEDULE, InputData)
				.then((response) => {
					if (response.Succeeded === true) {
						if (response.Data === "Already exists") {
							openNotificationWithIcon("success", "Sport already exists.", 3)
							self.setState({ ...this.state, RecordAlreadyExists: "Record already exists." })
						} else {
							self.setState({
								...this.state,
								loading: true,
								isAddEditDatapopup: false,
								isEditData: [],
								RecordAlreadyExists: "",
							})
							if (!this.state.isEdit) {
								openNotificationWithIcon("success", "Sport Added Successfully.", 2)
							} else {
								openNotificationWithIcon("success", "Sport Updated Successfully.", 2)
								
							}self.fetch(this.table.current.state)
						}
						self.fetch(this.state)
					}
				})
				.catch((error) => { })
		}
	}

	handleClear = () => {
		this.setState({
			...this.state,
			ScheduleName: "",
			TrainingProgram: [],
			SportID: [],
			WeekMonthID: [],
			RestTimeID: [],
			StartDate: "",
			EndDate: "",
			WorkoutList: [],
			errors: {},
		})
	}

	updateEditProps = () => {
		this.setState({
			isAddEditDatapopup: true,
			EditScheduleName: "",
			EditTrainingProgramID: [],
			EditTrainingScheduleID: 0,
			EditSportID: [],
			EditWeekMonthID: [],
			EditRestTimeID: [],
			EditStartDate: "",
			EditEndDate: "",
			WorkoutList: [],
		})
	}

	renderEditableWorkout = (cellInfo) => {
		let cellValue = ""
		if (this.state.WorkoutList.length > cellInfo.index) {
			cellValue = this.state.WorkoutList[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"Workout_tbl" + cellInfo.index}
				className={"tbl_Workout Workout_tbl" + cellInfo.index}
			>
				<Select
					className='textfiled w-80'
					bordered={false}
					maxTagCount="responsive"
					placeholder='Select Time'
					name='WorkoutID'
					size={200}
					value={cellValue}
					showSearch
					showArrow
					allowClear
					onChange={(val) => this.handleDropdownChange(cellInfo, val)}
					optionFilterProp='children'
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{this.state.EditWorkoutData.map((item, index) => {
						return (
							<Option key={index} value={item.OptionID}>
								{item.OptionData}
							</Option>
						)
					})}
				</Select>
			</div>
		)
	}

	renderEditableNoOfReps = (cellInfo) => {
		let cellValue = ""

		if (this.state.WorkoutList.length > cellInfo.index) {
			cellValue = this.state.WorkoutList[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"noOfReps_tbl" + cellInfo.index}
				className={"tbl_OtherInfo noOfReps_tbl" + cellInfo.index}
			>
				<Input
					size='large'
					autoComplete='off'
					type='number'
					name='NoOfReps'
					value={cellValue}
					onChange={(val) => this.handleTextFieldsChange(val, cellInfo)}
					bordered={false}
					className='textfiled w-80'
					placeholder='Enter No.Of Reps'
				></Input>
				<span className='val-err-msg'>{this.state.errors.NoOfReps}</span>
			</div>
		)
	}

	renderEditableNoOfSets = (cellInfo) => {
		let cellValue = ""

		if (this.state.WorkoutList.length > cellInfo.index) {
			cellValue = this.state.WorkoutList[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"noOfSets_tbl" + cellInfo.index}
				className={"tbl_OtherInfo noOfSets_tbl" + cellInfo.index}
			>
				<Input
					size='large'
					autoComplete='off'
					type='number'
					name='NoOfSets'
					value={cellValue}
					onChange={(val) => this.handleTextFieldsChange(val, cellInfo)}
					bordered={false}
					className='textfiled w-80'
					placeholder='Enter No.Of Sets'
				></Input>
				<span className='val-err-msg'>{this.state.errors.NoOfSets}</span>
			</div>
		)
	}

	renderEditableTime = (cellInfo) => {
		let cellValue = ""
		if (this.state.WorkoutList.length > cellInfo.index) {
			cellValue = this.state.WorkoutList[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"TimeID_tbl" + cellInfo.index} className={"tbl_TimeID TimeID_tbl" + cellInfo.index}>
				<Select
					className='textfiled w-80'
					bordered={false}
					maxTagCount="responsive"
					placeholder='Select Time'
					name='TimeID'
					size={200}
					value={cellValue}
					showSearch
					allowClear
					showSearch
					onChange={(val) => this.handleDropdownChange(cellInfo, val)}
					optionFilterProp='children'
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{this.state.WorkoutTimeIDData.map((item, index) => {
						return (
							<Option key={index} value={item.OptionID}>
								{item.OptionData}
							</Option>
						)
					})}
				</Select>
				{/* <span className='val-err-msg'>{errors.WorkoutID}</span> */}
			</div>
		)
	}
	handleAddRow = (e) => {
		let errors = {}
		e.preventDefault()
		let count = this.state.RowCount
		let rowData = this.state.WorkoutList.length
		let workoutLists = this.state.WorkoutList
		let temp = []
		if (rowData > 1 && workoutLists[rowData - 1] !== undefined) {
			let tempData = workoutLists[rowData - 1]

			let temp = workoutLists.filter(
				(x, index) =>
					index !== rowData - 1 &&
					x.TimeID === tempData.TimeID &&
					x.WorkoutID === tempData.WorkoutID &&
					x.NoOfReps === tempData.NoOfReps &&
					x.NoOfSets === tempData.NoOfSets
			)
			let filteredArr = workoutLists.reduce((acc, current) => {
				const x = acc.find(
					(item) => item.WorkoutID === current.WorkoutID && item.TimeID === current.TimeID
				)
				if (!x) {
					return acc.concat([current])
				} else {
					return acc
				}
			}, [])
			if (temp.length > 0 || filteredArr.length !== rowData) {
				errors["workoutLists"] = "Duplicate Workouts not allowed."
				this.setState({
					...this.state,
					errors: errors,
				})
				return
			}
		}
		if (rowData >= 1) {
			let empltytemp = workoutLists.filter(
				(x) =>
					x.TimeID.length === 0 && x.WorkoutID.length === 0 && x.NoOfReps === 0 && x.NoOfSets === 0
			)
			if (empltytemp.length > 0) {
				errors["workoutLists"] = "Please enter the workout details."
				this.setState({
					...this.state,
					errors: errors,
				})
				return
			}
		}
		if (rowData !== 0 && temp.length === 0) {
			count = 0
			this.setState({
				...this.state,
				AddWorkText: "Add another Workout",
			})
		}
		const values = [...this.state.WorkoutList]
		values.push({
			TimeID: [],
			WorkoutID: [],
			NoOfSets: 0,
			NoOfReps: 0,
			Id: count,
		})
		this.setState({
			...this.state,
			RowCount: count + 1,
			WorkoutList: values,
			AddWorkText: "Add another Workout",
		})
	}

	openNotificationWithIcon = (type, description, flag) => {
		if (flag === 1) {
			notification[type]({
				message: description,
				icon: <FontAwesomeIcon className='fa-xs icon_Delete ' icon={faTrashAlt}></FontAwesomeIcon>,
				description: "",
			})
		} else if (flag === 2) {
			notification[type]({
				message: description,
				description: "",
			})
		}
	}

	deleteModal = () => {
		if (this.state.isDeleteID === 0) {
			this.handleBtnDelete()
		} else {
			if (this.state.IsFromList === true) {
				handleTblDelete(this.state.IsDeleteId, this)
			} else {
				let id = this.state.IsDeleteId
				let Rowid = this.state.IsDeleteRowId
				let fields = this.state.WorkoutList
				fields.splice(Rowid, 1)
				this.setState({
					WorkoutList: fields,
				})
				this.closeModal()
				this.setState({
					errors: [],
				})
				let rowId = this.state.selected
				if (rowId !== undefined) {
					delete rowId[id]
				}
				openNotificationWithIcon("success", "Record has been deleted", 1)
				if (Object.keys(this.state.selected).length === 0) {
					this.setState({
						btnDelete: true,
						btnSend: true,
					})
				}
				if (this.state.WorkoutList.length === 0) {
					this.setState({
						RowCount: 0,
						AddWorkText: "Add WorkOut",
					})
				}
			}
			this.setState({ IsDeleteId: 0, IsOpen: false })
		}
	}

	closeModal = () => {
		this.setState({ IsOpen: false })
	}
	//update export excel flag as false
	componentDidUpdate() {
		if (this.state.IsExportExcel) {
			this.setState({
				IsExportExcel: false,
			})
		}
	}
	disableDateRanges = (range = { startDate: false, endDate: false }) => {
		const { startDate, endDate } = range
		return function disabledDate(current) {
			let startCheck = true
			let endCheck = true
			if (startDate) {
				startCheck = current && current < moment(startDate, "YYYY-MM-DD")
			}
			if (endDate) {
				endCheck = current && current > moment(endDate, "YYYY-MM-DD")
			}
			return (startDate && startCheck) || (endDate && endCheck)
		}
	}
	disabledDates = (value) => {
		return (
			// moment(value).format("DD MMM YYYY") &&
			// moment(value).format("DD MMM YYYY") >= moment(this.state.EditEndDate).format("DD MMM YYYY")
			moment(new Date(value)).format("YYYY-MM-DD") &&
			moment(new Date(value)).format("YYYY-MM-DD") >=
			moment(new Date(this.state.EditEndDate)).format("YYYY-MM-DD")
		)
	}

	async componentDidMount() {
		this.fetch(this.state)

		const TrainingSchedule = await APIRequest.getGetService(
			APIUrl.GET_TRAINING_SCHEDULE_DROPDOWN_LIST
		)
		this.setState({ TrainingScheduleData: TrainingSchedule.Data })

		const TrainingProgram = await APIRequest.getGetService(
			APIUrl.GET_TRAINING_PROGRAM_DROPDOWN_LIST
		)
		this.setState({ TrainingProgramData: TrainingProgram.Data })

		const WeekMonth = await APIRequest.getGetService(APIUrl.GET_WEEK_MONTH_DROPDOWN_LIST)
		this.setState({ WeekMonthData: WeekMonth.Data })

		const RestTimeID = await APIRequest.getGetService(APIUrl.GET_REST_TIME_DROPDOWN_LIST)
		this.setState({ RestTimeIDData: RestTimeID.Data })

		const SportName = await APIRequest.getGetService(APIUrl.GET_SPORT_LIST)
		this.setState({ SportNameData: SportName.Data })

		const WorkoutTimeID = await APIRequest.getGetService(APIUrl.GET_WORKOUT_TIME_DROPDOWN_LIST)
		this.setState({ WorkoutTimeIDData: WorkoutTimeID.Data })
	}

	render() {
		let Permissions = this.state.permissions
		const columns = [
			{
				Header: () => <div className='program-list-left'>S.No</div>,
				accessor: "SerialNo",
				sortable: false,
				resizable: false,
				className: "tableheader wordwrap",
				headerClassName: "BoldText ColoredText",
				Cell: (row) => {
					return <div className='tabledata program-list-left'>{row.original.SerialNo}</div>
				},
				width: Math.round(window.innerWidth * 0.04),
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Schedules</div>,
				resizable: false,
				accessor: "ScheduleName",
				style: { overflowWrap: "break-word" },
				width: Math.round(window.innerWidth * 0.12),
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.ScheduleName}>
							{this.tableColumnDataLimit(row.original.ScheduleName)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Programme</div>,
				resizable: false,
				accessor: "ProgramName",
				style: { overflowWrap: "break-word" },
				width: Math.round(window.innerWidth * 0.12),
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.ProgramName}>
							{this.tableColumnDataLimit(row.original.ProgramName)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Sport</div>,
				resizable: false,
				accessor: "SportName",
				style: { overflowWrap: "break-word" },
				width: Math.round(window.innerWidth * 0.1),
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.SportName}>
							{this.tableColumnDataLimit(row.original.SportName)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Week/ Month</div>,
				resizable: false,
				accessor: "WeekMonth",
				style: { overflowWrap: "break-word" },
				width: Math.round(window.innerWidth * 0.1),
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.WeekMonth}>
							{this.tableColumnDataLimit(row.original.WeekMonth)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Start Date</div>,
				resizable: false,
				accessor: "StartDate",
				style: { overflowWrap: "break-word" },
				width: Math.round(window.innerWidth * 0.1),
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.StartDate}>
							{this.tableColumnDataLimit(row.original.StartDate)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>End Date</div>,
				resizable: false,
				accessor: "EndDate",
				style: { overflowWrap: "break-word" },
				width: Math.round(window.innerWidth * 0.1),
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.EndDate}>
							{this.tableColumnDataLimit(row.original.EndDate)}
						</div>
					)
				},
			},
			{
				Header: () => <div className="align-right pl-3">Action</div>,
				accessor: "Action",
				// className: "tableheader wordwrap align-right",
				sortable: false,
				resizable: false,
				// textAlign: "center",
				filterable: false,
				width: Math.round(window.innerWidth * 0.07),
				Cell: (row) => {
					return (
						<div
							className='tabledata text-center pl-4'
							// style={{ display: "flex", justifyContent: "space-around" }}
						>
							{Permissions.includes(Permission.T_M_S_EDIT) &&
								this.props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									onClick={() => this.openEditAlldataModal(row.original, this)}
									title='Edit'
									className='mr-2 icon_edit'
									style={{ cursor: "pointer" }}
									icon={faEdit}
									width='3rem'
								/>
							) : null}
							{Permissions.includes(Permission.T_M_S_VIEW) &&
								this.props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									onClick={() => this.openViewAlldataModal(row.original, this)}
									title='View All details'
									color='green'
									// color=' #C29A37'
									style={{ cursor: "pointer" }}
									icon={faEye}
									className='mr-2 icon_eye'
									width='3rem'
								/>
							) : null}
							{Permissions.includes(Permission.T_M_S_DELETE) &&
								this.props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									title='Delete'
									color='red'
									className='mr-2 icon_Delete'
									style={{ cursor: "pointer" }}
									onClick={() => openDelModal(row.original.TrainingScheduleID, this)}
									icon={faTrashAlt}
									width='3rem'
								/>
							) : null}
						</div>
					)
				},
			},
		]
		const workoutcolumns = [
			{
				Header: () => <div className='otherInfo-lang-pad pl-2'>Time</div>,
				accessor: "TimeID",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader other-info-tabledata",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableTime,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='otherInfo-lang-pad pl-2'>Workout</div>,
				accessor: "WorkoutID",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader other-info-tabledata",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableWorkout,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='text-center pl-2'>No.of Sets</div>,
				accessor: "NoOfSets",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableNoOfSets,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='text-center pl-2'>No.of Reps</div>,
				accessor: "NoOfReps",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableNoOfReps,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='text-center'></div>,
				accessor: "Action",
				className: "tableheader",
				resizable: false,
				sortable: false,
				filterable: false,
				minWidth: 30,
				width: 70,
				maxWidth: 80,
				id: "delete",
				Cell: (row) => (
					<div className='mt-2'>
						<span
							className='icon_Delete '
							onClick={() => openModal(row.index, this, 3, row.original.id)} //{
						>
							<FontAwesomeIcon title='Delete' icon={faTrashAlt}></FontAwesomeIcon>
						</span>
					</div>
				),
			},
		]

		return (
			<>
				<Container fluid className='h-full'>
					<Row className='h-full'>
						<Col className='h-full'>
							<div className=' pt-10 '>
								<Spin size='large' spinning={this.state.loading} tip='Loading...'>
									<div className='row col-md-13 mt-1 form-group'>
										<div className='games_layout form-group col-md-3'>
											Manage Schedule
										</div>
										{Permissions.includes(Permission.T_M_S_CREATE) ? (
											<div className='col-md-9 d-flex justify-content-end'>
												<Button
													className='btn_green'
													style={{ height: "2rem" }}
													//viewAlldatapopupClose
													onClick={this.updateEditProps}
												>
													Add Workout
												</Button>
											</div>
										) : null}

									</div>
									<div className=' pt-10'>
										<div className='d-flex justify-content-start pt-1'></div>
										<Card className='w-100 card form-group'>
											<Card.Header
												className='text-white pl-1'
												onClick={() => setIsExpanded(!this.state.IsExpanded, this)}
											>
												<div className='float-left filter-heading-font'>
													Search Manage Schedule{" "}
												</div>
												<div className='float-right'>
													{this.state.Icons ? (
														<FontAwesomeIcon
															color='#FFFFFF'
															className='action-icon-font'
															icon={faArrowUp}
															style={{ cursor: "pointer" }}
														></FontAwesomeIcon>
													) : (
														<FontAwesomeIcon
															color='#FFFFFF'
															className='action-icon-font'
															icon={faArrowDown}
															style={{ cursor: "pointer" }}
														></FontAwesomeIcon>
													)}
												</div>
											</Card.Header>
											<Collapse in={this.state.IsExpanded}>
												<Card.Body>
													<div className='row col-sm-12'>
														<div className='col-sm-4 from-group'>
															<label>Schedules</label>
															<Select
																className='textfiled w-80'
																bordered={false}
																maxTagCount="responsive"
																size={200}
																name='TrainingScheduleID'
																value={this.state.FilterTrainingScheduleID}
																showSearch
																showArrow
																allowClear
																onChange={(val) =>
																	this.handleOnchange("FilterTrainingScheduleID", val)
																}
																placeholder='Select Schedule'
																optionFilterProp='children'
																filterSort={(optionA, optionB) =>
																	optionA.children
																		.toLowerCase()
																		.localeCompare(optionB.children.toLowerCase())
																}
															>
																{this.state.TrainingScheduleData.map((item, index) => {
																	return (
																		<Option key={index} value={item.OptionID}>
																			{item.OptionData}
																		</Option>
																	)
																})}
															</Select>
														</div>

														<div className='col-sm-4 form-group'>
															<label>Programme</label>
															<Select
																className='textfiled w-80'
																bordered={false}
																maxTagCount="responsive"
																placeholder='Select Programme'
																name='TrainingProgramID'
																size={200}
																value={this.state.FilterTrainingProgramID}
																showSearch
																showArrow
																allowClear
																onChange={(val) =>
																	this.handleOnchange("FilterTrainingProgramID", val)
																}
																optionFilterProp='children'
																filterSort={(optionA, optionB) =>
																	optionA.children
																		.toLowerCase()
																		.localeCompare(optionB.children.toLowerCase())
																}
															>
																{this.state.TrainingProgramData.map((item, index) => {
																	return (
																		<Option key={index} value={item.OptionID}>
																			{item.OptionData}
																		</Option>
																	)
																})}
															</Select>
														</div>
														{/*</div>*/}
														<div className='col-sm-4 form-group'>
															<label>Sports</label>
															<Select
																className='textfiled w-80'
																name='SportID'
																showSearch
																allowClear
																showArrow
																mode='multiple'
																maxTagCount={"responsive"}
																// maxTagTextLength={11}
																size={200}
																bordered={false}
																value={this.state.FilterSportID}
																onChange={(val) => this.handleOnchange("FilterSportID", val)}
																placeholder='Select Sports'
																optionFilterProp='children'
																filterSort={(optionA, optionB) =>
																	optionA.children
																		.toLowerCase()
																		.localeCompare(optionB.children.toLowerCase())
																}
															>
																{this.state.SportNameData.map((item, index) => {
																	return (
																		<Option key={index} value={item.OptionID}>
																			{item.OptionData}
																		</Option>
																	)
																})}
															</Select>
														</div>
														<div className='col-sm-4 form-group'>
															<label>Week/ Month</label>
															<Select
																className='textfiled w-80'
																name='WeekMonthID'
																showSearch
																allowClear
																showArrow
																maxTagCount="responsive"
																size={200}
																bordered={false}
																value={this.state.FilterWeekMonthID}
																onChange={(val) => this.handleOnchange("FilterWeekMonthID", val)}
																placeholder='Select Week/ Month'
																optionFilterProp='children'
																filterSort={(optionA, optionB) =>
																	optionA.children
																		.toLowerCase()
																		.localeCompare(optionB.children.toLowerCase())
																}
															>
																{this.state.WeekMonthData.map((item, index) => {
																	return (
																		<Option key={index} value={item.OptionID}>
																			{item.OptionData}
																		</Option>
																	)
																})}
															</Select>
														</div>
														<div className='col-sm-4 form-group'>
															<label>Start Date</label>
															<DatePicker
																size='large'
																format='DD MMM YYYY'
																allowClear
																showArrow
																disabled={false}
																bordered={false}
																name='StartDate'
																selected={this.state.FilterStartDate}
																value={this.state.FilterStartDate}
																onChange={(val) => this.handleOnchange("FilterStartDate", val)}
																// disabledDate={(current) => {
																//   return moment().add(-1, 'days')  >= current ;
																//   }}
																className='myDatePicker datepickerfiled add-prog-date'
																placeholder='Select Start Date'
															/>
														</div>
														<div className='col-sm-4 form-group'>
															<label>End Date</label>
															<DatePicker
																size='large'
																format='DD MMM YYYY'
																allowClear
																showArrow
																disabled={false}
																bordered={false}
																name='EndDate'
																selected={this.state.FilterEndDate}
																value={this.state.FilterEndDate}
																onChange={(val) => this.handleOnchange("FilterEndDate", val)}
																// disabledDate={(current) => {
																//   return moment().add(-1, 'days')  >= current ;
																//   }}
																className='myDatePicker datepickerfiled add-prog-date'
																placeholder='Select End Date'
															/>
														</div>
													</div>
													<div className='row col-sm-12'>
														<div className='col-sm-6'></div>
														<div className='col-sm-5 d-flex justify-content-end pt-4'>
														<Button className='btn_green mr-2' onClick={this.handleClearFilters}>
																Clear
															</Button>
															<Button
																className='btn_green'
																onClick={this.fetch}
																disabled={this.state.disableSearch}
															>
																Search
															</Button>
															
														</div>
													</div>
												</Card.Body>
											</Collapse>
										</Card>
										<br></br>
										<div className='admin-report-table'>
											<ReactTable
												data={this.state.TrainingScheduleList}
												columns={columns}
												showPaginationTop={false}
												showPaginationBottom={false}
												minRows={0}
												defaultPageSize={10}
												defaultPage={1}
												onFetchData={this.fetch}
												ref={this.table}
												sortable={true}
												multiSort={true}
												manual
												className={"CosmicTable"}
												showPageSizeOptions={false}
												NoDataComponent={NoDataConst}
												defaultSorting={[
													{
														id: "id",
														desc: true,
													},
												]}
												manualPagination={true}
											/>
										</div>

										<div className='mt-1'>
											<label>
												<b>Total Records: </b>
												{this.state.TotalRecords > 10
													? this.state.TotalCount + " of " + this.state.TotalRecords
													: this.state.TotalRecords !== 0
														? this.state.TotalRecords
														: 0}
											</label>
										</div>

										<div className='row'>
											<div className=' col-sm-6'>
												{Permissions.includes(Permission.T_M_S_EXPORT_EXCEL) ? (
													<Button
														className='mr-2 btn_green'
														style={{
															//paddingRight: "0.4rem", paddingLeft: "0.4rem",
															cursor: "pointer",
														}}
														disabled={this.state.exportbtn}
														onClick={this.handlerExportData}
													>
														<FontAwesomeIcon
															title='Delete'
															color={"#FFFFFF"}
															icon={faDownload}
														// marginRight={"5px !important"}
														></FontAwesomeIcon>{" "}
														Export Excel
													</Button>
												) : null}
											</div>
											<div className='col-sm-6 d-flex justify-content-end'>
												<Pagination
													totalPages={this.state.TotalPages}
													totalRecords={this.state.TotalRecords}
													TotalCount={this.state.TotalCount}
													paginationCall={this.handlerPagination}
												/>
											</div>
										</div>
									</div>
									{this.state.IsExportExcel ? (
										<ExportData
											data={this.state.exportData}
											label={exportLabel}
											filename={this.state.exportFilename}
										/>
									) : null}
								</Spin>
							</div>
						</Col>
					</Row>
				</Container>
				<div>
					<Modal
						backdrop='static'
						size='lg'
						className='notshow'
						keyboard={true}
						show={this.state.isAddEditDatapopup}
						onHide={this.viewAlldatapopupClose} //toggle={props.onCloseModal}
						aria-labelledby='contained-modal-title-vcenter'
					>
						<Modal.Header className=''>
							<div className='col-sm-12'>
								<div className='row'>
									<div className='col-sm-11'>
										<div className='d-flex justify-content-left'>
											{!this.state.isEdit ? (
												<h5 className='font-weight-bold text-white mb-0'>{"Add Schedule"}</h5>
											) : (
												<h5 className='font-weight-bold text-white mb-0'>{"Edit Schedule"}</h5>
											)}
										</div>
									</div>
									<div className='col-sm-1'>
										<div className='d-flex justify-content-end'>
											<button
												type='button'
												className='close text-white'
												onClick={this.viewAlldatapopupClose}
												data-dismiss='modal'
												aria-label='Close'
											>
												<span aria-hidden='true'>&times;</span>
											</button>{" "}
										</div>
									</div>
								</div>
							</div>
						</Modal.Header>
						<Modal.Body>
							<div className='row col-sm-12'>
								<div className='col-sm-4 from-group'>
									<label>
										Schedule<span className='text-danger'>*</span>
									</label>
									<Input
										size='large'
										autoComplete='off'
										type='text'
										name='EditScheduleName'
										value={this.state.EditScheduleName}
										onChange={this.handleScheduleNameOnchange}
										bordered={false}
										className='textfiled w-80'
										placeholder='Schedule Name'
									></Input>
									<span className='val-err-msg'>{this.state.errors.EditScheduleName}</span>
								</div>

								<div className='col-sm-4'>
									<label>
										Programme Name<span className='text-danger'>*</span>
									</label>
									<Select
										getPopupContainer={trigger=> trigger.parentNode}
										title="Programme Name"
										className='textfiled w-80'
										bordered={false}
										size="200"
										showSearch
										// mode="multiple"
										name='EditTrainingProgramID'
										maxTagCount="responsive"
										allowClear
										showArrow
										placeholder='Select Program'
										value={this.state.EditTrainingProgram}
										onChange={this.handleTrainingProgramOnchange}
										optionFilterProp='children'
										filterSort={(optionA, optionB) =>
											optionA.children
											.toLowerCase()
											.localeCompare(optionB.children.toLowerCase())
										}
									>
										{this.state.TrainingProgramData.map((item, index) => (
											<Option key={index} value={item.OptionID}>
												{item.OptionData}
											</Option>
										))}
									</Select>
									<span className='val-err-msg'>{this.state.errors.EditTrainingProgram}</span>
								</div>
								<div className='col-sm-4 form-group'>
									<label>
										Sport<span className='text-danger'>*</span>
									</label>
									{!this.state.isEdit ? (
										<Select
											getPopupContainer={trigger=> trigger.parentNode}
											className='textfiled w-80'
											bordered={false}
											style={{ width: "100%" }}
											placeholder='Select Sport'
											name='EditSportID'
											size={"large"}
											showSearch
											showArrow
											allowClear
											maxTagCount={"responsive"}
											value={this.state.EditSport}
											onChange={this.handleSportsOnchange}
											optionFilterProp='children'
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{this.state.EditSportNameData.map((item, index) => (
												<Option key={index} value={item.SportID}>
													{item.Name}
												</Option>
											))}
										</Select>
									) : (
										<Select
										getPopupContainer={trigger=> trigger.parentNode}
											className='textfiled w-80'
											bordered={false}
											style={{ width: "100%" }}
											placeholder='Select Sport'
											name='EditSportID'
											size={"large"}
											showSearch
											showArrow
											allowClear
											maxTagCount={"responsive"}
											value={this.state.EditSportID}
											onChange={this.handleSportsOnchange}
											// optionFilterProp='children'
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{this.state.EditSportNameData.map((item, index) => (
												<Option key={index} value={item.SportID}>
													{item.Name}
												</Option>
											))}
										</Select>
									)}
									<span className='val-err-msg'>{this.state.errors.EditSportID}</span>
								</div>
								<div className='col-sm-4 form-group'>
									<label>
										Week/Month<span className='text-danger'>*</span>
									</label>
									<Select
									getPopupContainer={trigger=> trigger.parentNode}
										className='textfiled w-80'
										bordered={false}
										maxTagCount="responsive"
										style={{ width: "100%" }}
										placeholder='Select Week/Month'
										name='EditWeekMonthID'
										size={"large"}
										showSearch
										showArrow
										allowClear
										value={this.state.EditWeekMonthID}
										onChange={this.handleweekMonthIDOnchange}
										optionFilterProp='children'
										filterSort={(optionA, optionB) =>
											optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
										}
									>
										{this.state.WeekMonthData.map((item, index) => (
											<Option key={index} value={item.OptionID}>
												{item.OptionData}
											</Option>
										))}
									</Select>
									<span className='val-err-msg'>{this.state.errors.EditWeekMonthID}</span>
								</div>
								<div className='col-sm-4 form-group'>
									<Space direction='vertical' size={2}>
										<label className='ant_text_pleace '>
											{" "}
											Start Date <span className='text-danger'>*</span>{" "}
										</label>
										<DatePicker
										getPopupContainer={trigger=> trigger.parentNode}
											size='large'
											style={{
												height: "auto",
												width: "auto",
												border: "none",
												borderRadius: "0px",
												cursor: "pointer",
												fontSize: "17px",
												margin: "0px",
												padding: "0px",
											}}
											format='DD MMM YY'
											disabled={false}
											bordered={false}
											name='EditStartDate'
											allowClear
											showArrow
											value={this.state.EditStartDate}
											className='myDatePicker datepickerfiled add-prog-date'
											placeholder='Select Start Date'
											disabledDate={(d) =>
												!d ||
												d.isAfter(moment(this.state.AddEndDate).format("YYYY-MM-DD")) ||
												d.isBefore(moment(this.state.AddStartDate).format("YYYY-MM-DD"))
											}
											onChange={this.handleStartDateOnchange}
										/>
									</Space>
									<span className='val-err-msg'>{this.state.errors.EditStartDate}</span>
								</div>
								<div className='col-sm-4 form-group'>
									<Space direction='vertical' size={2}>
										<label className='ant_text_pleace'>
											{" "}
											End Date<span className='text-danger'>*</span>{" "}
										</label>
										<DatePicker
										getPopupContainer={trigger=> trigger.parentNode}
											size='large'
											format='DD MMM YY'
											className='myDatePicker datepickerfiled add-prog-date'
											bordered={false}
											// defaultValue={moment(isDiabledDate)}
											showToday={true}
											allowClear
											showArrow
											value={this.state.EditEndDate}
											disabled={this.state.EndDateDisabled}
											name='EditEndDate'
											placeholder='Select End Date'
											disabledDate={(d) =>
												
												!d ||
												d.isAfter(moment(this.state.AddEndDate).format("YYYY-MM-DD")) ||
												d.isBefore(moment(this.state.EditStartDate).format("YYYY-MM-DD"))
											}
											onChange={this.handleEndDateOnchange}
										/>
									</Space>
									<span className='val-err-msg'>{this.state.errors.EditEndDate}</span>
								</div>
								<div className='col-sm-12 form-group'>
									<ReactTable
										data={this.state.WorkoutList}
										columns={workoutcolumns}
										showPaginationTop={false}
										showPaginationBottom={false}
										minRows={0}
										ref={this.table}
										sortable={false}
										multiSort={false}
										NoDataComponent={NoDataConst}
										manual
										style={{ overflow: "wrap" }}
										className='OtherInfoTable form-group'
										manualPagination={false}
									/>
									<div className='otherInfo-Add-btn'>
										<span className='val-err-msg'>{this.state.errors.workoutLists}</span>
										<span className='val-err-msg'>{this.state.errors.WorkoutList}</span>
										<button
											onClick={this.handleAddRow}
											className='btn-default border-0 bg-white link'
										>
											{this.state.AddWorkText}
											<FontAwesomeIcon icon={faPlusCircle}></FontAwesomeIcon>
										</button>
									</div>
								</div>
								<div className='col-sm-2 form-group'>
									<label className='ant_text_pleace'>
										{" "}
										Rest Time<span className='text-danger'>*</span>{" "}
									</label>
									<Select
										className='textfiled w-80'
										bordered={false}
										maxTagCount="responsive"
										style={{ width: "100%" }}
										placeholder='Select Rest Time'
										name='EditRestTimeID'
										size={"large"}
										showSearch
										showArrow
										allowClear
										value={this.state.EditRestTimeID}
										onChange={this.handlerestTimeIDOnchange}
										optionFilterProp='children'
										filterSort={(optionA, optionB) =>
											optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
										}
									>
										{this.state.RestTimeIDData.map((item, index) => (
											<Option key={index} value={item.OptionID}>
												{item.OptionData}
											</Option>
										))}
									</Select>
									<span className='val-err-msg'>{this.state.errors.EditRestTimeID}</span>
								</div>
							</div>
						</Modal.Body>
						<Modal.Footer className='m-0 py-0 '>
							<div className='col-sm-0'>
								<div className='col-sm d-flex justify-content-right '>
									<span className='val-err-msg'>{this.state.RecordAlreadyExists}</span>
									
									{/* <Button onClick={this.viewAlldatapopupClose} className='btn_green mr-2'>
										Close
									</Button> */}

									{!this.state.isEdit ? (
										<Button onClick={() => this.handleSave(this)} className='btn_green'>
											Save
										</Button>
									) : (
										<Button onClick={() => this.handleSave(this)} className='btn_green'>
											Update
										</Button>
									)}
								</div>
							</div>
						</Modal.Footer>
					</Modal>
					<Modal size='sm' backdrop='static' show={this.state.IsOpen} onHide={this.closeModal}>
						<div className='popup-content'>
							<div className='deletecircle'></div>
							<i className='fa fa-trash-o deleteicon'></i>
							<Modal.Body>
								<p className='pull-left' style={{ margin: "4rem 2rem" }}>
									{" "}
									<h5>You are about to delete an invite(s)</h5>
									<h6 style={{ color: "darkgray" }}>
										This will delete your invite(s) from the list
									</h6>
									<h6
										style={{
											color: "darkgray",
											marginRight: "7rem",
											marginBottom: "-3rem",
										}}
									>
										Are you sure?
									</h6>
								</p>
								<br />
								<div className='pull-right'>
									<Button className='btn_cancel mr-2' onClick={this.closeModal}>
										No
									</Button>
									<Button className='btn_green' onClick={this.deleteModal}>
										Yes
									</Button>
								</div>
							</Modal.Body>
						</div>
					</Modal>
				</div>
				<div>
					<Modal
						backdrop='static'
						size='md'
						className='popupborder'
						keyboard={true}
						show={this.state.isEyeDatapopup}
						onHide={this.viewAlldatapopupClose}
						aria-labelledby='contained-modal-title-vcenter'
					>
						<Modal.Header className=''>
							<div className='col-sm-12'>
								<div className='row'>
									<div className='col-sm-11'>
										<div className='d-flex justify-content-left'>
											<h5 className='font-weight-bold text-white mb-0'>{"Manage Schedule"}</h5>
										</div>
									</div>
									<div className='col-sm-1'>
										<div className='d-flex justify-content-end'>
											<button
												type='button'
												className='close text-white'
												onClick={this.viewAlldatapopupClose}
												data-dismiss='modal'
												aria-label='Close'
											>
												<span aria-hidden='true'>&times;</span>
											</button>{" "}
										</div>
									</div>
								</div>
							</div>
						</Modal.Header>
						<Modal.Body className=''>
							<div className='col-sm-12'>
								<div className='row pt-10'>
									<div className='col-sm-1'>{""}</div>
									<div className='col-sm-10'>
										<div className='form-group row'>
											<div className='col-sm-4'>
												{" "}
												<label className='font-weight-bold popup_txt'>Schedules</label>{" "}
											</div>
											<div className='col-sm-1'> </div>
											<div className='col-sm-7'>
												{" "}
												<label>{this.state.isEyeData.ScheduleName}</label>{" "}
											</div>
										</div>
										<div className='form-group row'>
											<div className='col-sm-4'>
												{" "}
												<label className='font-weight-bold popup_txt'>Program</label>{" "}
											</div>
											<div className='col-sm-1'> </div>
											<div className='col-sm-7'>
												{" "}
												<label>{this.state.isEyeData.ProgramName}</label>{" "}
											</div>
										</div>
										<div className='form-group row'>
											<div className='col-sm-4'>
												{" "}
												<label className='font-weight-bold popup_txt'>Sport</label>{" "}
											</div>
											<div className='col-sm-1'> </div>
											<div className='col-sm-7'>
												{" "}
												<label>{this.state.isEyeData.SportName}</label>{" "}
											</div>
										</div>
										<div className='form-group row'>
											<div className='col-sm-4'>
												{" "}
												<label className='font-weight-bold popup_txt'>Week / Month</label>{" "}
											</div>
											<div className='col-sm-1'> </div>
											<div className='col-sm-7'>
												{" "}
												<label>{this.state.isEyeData.WeekMonth}</label>{" "}
											</div>
										</div>
										<div className='form-group row'>
											<div className='col-sm-4'>
												{" "}
												<label className='font-weight-bold popup_txt'>Start Date </label>{" "}
											</div>
											<div className='col-sm-1'> </div>
											<div className='col-sm-7'>
												{" "}
												<label>{this.state.isEyeData.StartDate}</label>{" "}
											</div>
										</div>
										<div className='form-group row'>
											<div className='col-sm-4'>
												{" "}
												<label className='font-weight-bold popup_txt'>End Date </label>{" "}
											</div>
											<div className='col-sm-1'> </div>
											<div className='col-sm-7'>
												{" "}
												<label>{this.state.isEyeData.EndDate}</label>{" "}
											</div>
										</div>
										<div className='form-group row'>
											<div className='col-sm-4'>
												{" "}
												<label className='font-weight-bold popup_txt'>Athlete Name </label>{" "}
											</div>
											<div className='col-sm-1'> </div>
											<div className='col-sm-7'>
												{" "}
												<label>{this.state.isEyeData.AthleteName}</label>{" "}
											</div>
										</div>
									</div>
								</div>
							</div>
							{/* <div className='d-flex justify-content-end'>
								<Button className='cancel mr-3 btn_green' onClick={this.viewAlldatapopupClose}>
									Close
								</Button>
							</div> */}
						</Modal.Body>
					</Modal>
				</div>
			</>
		)
	}
}

const mapPropsToState = (state) => ({
	user: state.userReducer,
})

export default PermissionProvider(connect(mapPropsToState, null)(ClassManageSchedule))
