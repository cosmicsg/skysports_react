import React, { Component } from "react"
import { Row, Col, Container } from "react-bootstrap"
import ManageListAddSchedule from "./ClassManageSchedule"
import "../../../styles/grid.css"
import { Spin } from "antd"
import { Permissions as Permission } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"

//It is manage user all tab control
class ManageSchedules extends Component {
	constructor(props) {
		super(props)

		let permissions = this.props.getAppPermissions();
		let roles = [];

		this.state = {
			loading: false,
			permissions: permissions,
			roles: roles,
		}

		this.childRef = React.createRef()
	}
	//It is used to refresh SportLevel list page
	handlerRefreshSportLevelList = () => {
		//This below function is used to call the SportLevel list
		this.childRef.current.handlerRefreshSportLevelList()
	}
	// It is used to over all loader start
	handlerLoadingStart = () => {
		this.setState({ loading: true })
	}
	// It is used to over all loader stop
	handlerLoadingStop = () => {
		this.setState({ loading: false })
	}
	componentDidMount = () => { }
	render() {
		let permissions = this.state.permissions

		return (
			<>
				{permissions.includes(Permission.T_M_S_VIEW) ? (
					<Container fluid className='h-full'>
						<Spin size='large' spinning={this.state.loading} tip='Loading...'>
							<Row className='h-full'>
								<Col className='h-full'>
									<>
										<ManageListAddSchedule ref={this.childRef} />
									</>
								</Col>
							</Row>
						</Spin>
					</Container>
				) : null}
			</>
		)
	}
}

export default PermissionProvider(ManageSchedules)
