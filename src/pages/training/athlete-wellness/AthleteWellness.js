import React from "react"
import { DatePicker, Spin } from "antd"
import { Tabs, Tab, Row, Col } from "react-bootstrap"
import AthleteSleepInfo from "./AthleteSleep"
import AthleteWellnessQuestion from "./AthleteWellnessQuestion"
import TrackWellnessInfo from "./TrackAthleteWellness"
import "../../../styles/menu.css"
import moment from "moment"
import { Permissions } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"
import { connect } from "react-redux"

const { RangePicker } = DatePicker
const dateFormat = "YYYY-MM-DD"
class AthleteWellness extends React.Component {
	constructor(props) {
		super(props)

		let tempPermission = this.props.getAppPermissions()
		let tempRoles = this.props.getAppRoles()

		this.state = {
			loading: false,
			dates: [],
			hackValue: [],
			value: [],
			className: ["disable-arrow1", "disable-arrow3", "disable-arrow4"],
			isShowFilter: false,
			_startDate: "",
			_endDate: "",
			permissions: tempPermission,
			roles: tempRoles,
			defaultValue: [],
		}

		this.disabledDateSelection = this.disabledDateSelection.bind(this)
		this.getFilterData = this.getFilterData.bind(this)
		this.athleteWellnessRef = React.createRef(null)
	}

	// Life cycle method
	//
	//
	componentDidMount() {
		this.onOpenChange(true)
	}

	//
	// Below function which is used to disable date selection
	//
	disabledDateSelection = (current) => {
		let tempDate = this.state.dates

		if (!tempDate || tempDate.length === 0) {
			return false
		}
		let tooLate = tempDate[0] && current.diff(tempDate[0], "days") > 7
		let tooEarly = tempDate[1] && tempDate[1].diff(current, "days") > 7
		return tooEarly | tooLate
	}

	//
	// onOpenChange which is used to handle on open date picker
	//
	onOpenChange = (open) => {
		if (open) {
			this.setState({
				hackValue: [],
				dates: [],
			})
		} else {
			this.setState({
				hackValue: undefined,
			})
		}
	}

	//
	//  getFilterData which is used to accept date array and it will return start date, end date with reset timings.
	//
	getFilterData = (date) => {
		if (date && date[0] && date[1]) {
			let startDate = new Date(date[0]).setHours(0, 0, 0, 0)
			let endDate = new Date(date[1]).setHours(0, 0, 0, 0)
			return { startDate, endDate }
		}
		return {}
	}

	//
	//Show Date filter and legends for Track Wellness tab when the tab is active
	//
	showDateFilter = (flag) => {
		let self = this
		this.setState(
			{
				isShowFilter: flag, //this.state.isShowFilter
			},
			() => {
				if (self.state.isShowFilter) {
					self.assignSelectedDate(true)
				}
			}
		)
	}

	//
	// handle tab changes
	//
	handelTabChange = (key) => {
		if (key === "TrackWellnessInfo") {
			this.setState({
				defaultValue: [moment(this.getDefaultDate().startDate), moment(new Date())],
			})
			this.showDateFilter(true)
		} else this.showDateFilter(false)
	}

	//
	// handle onchange of date picker
	//
	onChange = (name, value) => {
		let self = this
		this.setState(
			{
				[name]: value,
			},
			() => {
				self.assignSelectedDate()
			}
		)
	}

	//
	// getDefaultDate which used to get default from date and start date
	//
	getDefaultDate = () => {
		let today = new Date()
		let fromDate = today.setDate(today.getDate() - 6)
		return { startDate: fromDate, endDate: new Date() }
	}

	//
	// assignSelectedDate which is used to filter data by from date and end date
	// the below function will be calling the method of TrackAthleteWellness component
	//
	assignSelectedDate = (isInitialLoad) => {
		let childRef = this.athleteWellnessRef.current
		if (childRef) {
			let date1
			let date2
			if (isInitialLoad) {
				date1 = this.getDefaultDate().startDate
				date2 = new Date()
			} else {
				const { startDate, endDate } = this.getFilterData(this.state.dates)
				date1 = startDate
				date2 = endDate
			}
			childRef(date1, date2)
		}
	}

	render() {
		const defaultTab = "AthleteSleepInfo"
		let UserTypes = this.props.user.UserType
		return (
			<div>
				{this.state.isShowFilter ? (
					<div>
						<div className='row col-sm-12 pr-0'>
							<div className='col-sm-6 mt-4  wellness-header-text '>Athlete Wellness</div>
							<div className='col-sm-6 d-flex justify-content-end pr-0 athlete-margin-heading'>
								<div className='track-wellness-header-legend'>
									<div style={{ backgroundColor: "yellow" }}></div>
									<span>Average</span>
									<div style={{ backgroundColor: "orange" }}></div>
									<span>Good</span>
									<div style={{ backgroundColor: "green" }}></div>
									<span>Great</span>
								</div>
							</div>
						</div>

						<div className='col-sm-12 d-flex justify-content-end'>
							<RangePicker
								dropdownClassName={this.state.className.join(",")}
								format={"DD-MM-YYYY"}
								suffixIcon
								style={{
									cursor: "pointer",
									border: "none",
									borderBottom: "1px solid #d9d9d9",
								}}
								className='disable-arrow1, disable-arrow2, disable-arrow3'
								title='Filter'
								name='selectedDate'
								defaultValue={this.state.defaultValue}
								disabledDate={this.disabledDateSelection}
								onCalendarChange={(val) => this.onChange("dates", val)}
								onChange={(val) => this.onChange("value", val)}
								onOpenChange={this.onOpenChange}
							/>
						</div>
					</div>
				) : null}
				{!this.state.isShowFilter ? (
					<div className='col-sm-6 mt-4  wellness-header-text '>Athlete Wellness</div>
				) : null}
				<Spin size='large' spinning={this.state.loading} tip='Loading...'>
					<Row className='overflow-hidden pl-0 ml-0 mr-0 pr-0 athlete-Wellness-header'>
						<Col className='h-full p-0'>
							<Tabs
								className='Wellness-tab athlete-Wellness-header-bg'
								defaultActiveKey={defaultTab}
								id='controlled-tab-example'
								onSelect={this.handelTabChange}
							>
								{UserTypes == "Athlete" ? (
									<Tab
										tabClassName='athlete-sleep-tab-heading-space'
										eventKey='AthleteSleepInfo'
										title={<span className='tab-header-text'>Track Sleep</span>}
									>
										<AthleteSleepInfo />
									</Tab>
								) : null}
								{UserTypes == "Athlete" ? (
									<Tab
										tabClassName='athlete-sleep-tab-heading-space'
										eventKey='AthleteWellnessQuestion'
										title={<span>Wellness Questions</span>}
									>
										<AthleteWellnessQuestion />
									</Tab>
								) : null}
								{this.state.permissions.includes(Permissions.TRACK_ATHLETE_WELLNESS_VIEW) ? (
									<Tab
										tabClassName='athlete-sleep-tab-last-heading-space'
										eventKey='TrackWellnessInfo'
										title={<span>Track Wellness</span>}
										onSelect={this.handelTabChange}
									>
										<TrackWellnessInfo ref={this.athleteWellnessRef} />
									</Tab>
								) : null}
							</Tabs>
						</Col>
					</Row>
				</Spin>
			</div>
		)
	}
}

const mapPropsToState = (state) => ({
	user: state.userReducer,
})
export default PermissionProvider(connect(mapPropsToState, null)(AthleteWellness))
