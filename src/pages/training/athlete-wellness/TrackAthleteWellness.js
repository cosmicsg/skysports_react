import React, {
  useState,
  forwardRef,
  useImperativeHandle,
} from "react";
import moment from "moment";
import Bed from "../../../content/Icons/Bed.svg";
import Star from "../../../content/Icons/Star.svg";
import "../../../styles/Wellness.css";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import {
  GET_ATHLETE_WELLNESS_QTS_DATA,
} from "../../../components/api-manager/apiConstant";

//
// static color properties
//
const colorProps = {
  YELLOW: "yellow",
  ORANGE: "orange",
  GREEN: "green",
};

//
// Below method which is used to check given two integer is greater than(>) or not
//
Number.prototype.between = function (a, b) {
  var min = Math.min.apply(Math, [a, b]);
  var max = Math.max.apply(Math, [a, b]);
  return this >= min && this <= max;
};
//
// Track Athlete Wellness component
//
const TrackAthleteWellness = forwardRef((props, ref) => {

  const [wellnessProps, setWellnessProps] = useState([]);

  //
  // Below method will gets executed whenever filter is changing by user.
  //
  useImperativeHandle(
    ref,
    () =>
      function callRefresh(date1, date2) {
        refreshData(date1, date2);
      }
  );

  //
  // Refresh List data
  //
  const refreshData = (startDate, endDate) => {
    if (startDate && endDate) {
      let param1 = moment(new Date(startDate)).format("DD-MM-YYYY");
      let param2 = moment(new Date(endDate)).format("DD-MM-YYYY");
      APIRequest.getGetService(
        GET_ATHLETE_WELLNESS_QTS_DATA + "/" + param1 + "/" + param2
      ).then((res) => {
        setWellnessProps(res.Data);
      });
    } else {
      setWellnessProps([]);
    }
  };

  //
  // Return user ratings ui
  //
  const showUserRatings = (itemLength) => {
    if (itemLength !== undefined && itemLength !== null && itemLength > 0) {
      let _itemLength = getRatings(itemLength);
      if (_itemLength === 1)
        return (
          <img
            key={1}
            className="track-wellness-item-right-star"
            src={Star}
          ></img>
        );
      else {
        _itemLength = _itemLength > 5 ? 5 : _itemLength;
        let tempArrays = [];
        for (let index = 0; index < _itemLength; index++) {
          tempArrays.push(
            <img
              key={index}
              className="track-wellness-item-right-star"
              src={Star}
            ></img>
          );
        }
        return tempArrays;
      }
    }
    return null;
  };
  //
  // Get Ratings based on the percentage which has given by the user
  //
  const getRatings = (itemLength) => {
    if (itemLength !== undefined && itemLength !== null && itemLength > 0) {
      let temp = parseInt(itemLength);
      let _itemLength = temp.between(0, 20)
        ? 1
        : temp.between(20, 40)
        ? 2
        : temp.between(40, 60)
        ? 3
        : temp.between(60, 80)
        ? 4
        : temp.between(80, 100)
        ? 5
        : 1;
      return _itemLength;
    }
    return 0;
  };

  //
  // summary:
  // Calculate ratings and return color based on the ratings
  //
  const calculateRatings = (item) => {
    let ratings =
      parseInt(getRatings(item.Sleep)) +
      parseInt(getRatings(item.BodyFeel)) +
      parseInt(getRatings(item.Mood)) +
      parseInt(getRatings(item.Energy)) +
      parseInt(getRatings(item.Soreness));

    if (ratings > 0) {
      return ratings.between(21, 25)
        ? colorProps.GREEN
        : ratings.between(11, 20)
        ? colorProps.ORANGE
        : colorProps.YELLOW;
    }
    return colorProps.YELLOW;
  };
  //
  // No records found
  //
  const NoDataConst = (props) => (
    <span style={{ marginLeft: "40%", fontSize: "20px" }}>
      No records found{" "}
    </span>
  );

  return (
    <>
      <div className="row ">
        {wellnessProps.map((item, index) => (
          <div key={index} className="card wellness-track-container mt-3">
            <div className="card-body">
              <div
                className="col-md-12 track-wellness-item-header"
                style={{ backgroundColor: calculateRatings(item) }}
              >
                {item.SleepDay}
              </div>
              <div className="col-md-5 wellness-track-item-line">
                <div className="track-wellness-sleeping-container">
                  <img
                    className="track-wellness-sleeping-img"
                    src={Bed}
                    alt={"sleeping icond"}
                  />
                </div>
                <p id="track-wellness-item-p">Sleeping</p>
                <p
                  className="track-wellness-item-left"
                  style={{ fontWeight: "bold" }}
                >
                  {item.StartTime + " to " + item.EndTime}
                </p>
                <p className="track-wellness-item-left">{item.totalHours}</p>
              </div>
              <div className="col-md-7 track-wellness-right-side pl-1 ml-0 pr-0 mr-0">
                <div
                  className="col-sm-6 pt-1 ml-0 pl-1"
                  style={{ float: "left" }}
                >
                  <p className="track-wellness-item-right">Sleep</p>
                  <p className="track-wellness-item-right">Body feels</p>
                  <p className="track-wellness-item-right">Mood</p>
                  <p className="track-wellness-item-right">Energy</p>
                  <p className="track-wellness-item-right">Soreness</p>
                  <p id="user-pain-indicate-text">
                  Today which part of your body has pain or soreness?
                  </p>
                </div>
                <div className="col-sm-6 pl-0 ml-0" style={{ float: "left" }}>
                  <p>{showUserRatings(item.Sleep)}</p>
                  <p>{showUserRatings(item.BodyFeel)}</p>
                  <p>{showUserRatings(item.Mood)}</p>
                  <p>{showUserRatings(item.Energy)}</p>
                  <p>{showUserRatings(item.Soreness)}</p>
                  {item.ImageBase64 ? (
                    <img
                      className="track-wellness-indicator"
                      src={item.ImageBase64}
                    />
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
      {wellnessProps.length == 0 ? (
        <div style={{ marginTop: "5px", color: "#555" }}>{NoDataConst()}</div>
      ) : null}
    </>
  );
});

export default React.memo(TrackAthleteWellness);
