import React from "react"
import { notification } from "antd"
import { Button } from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import * as ReduxAction from "../../../components/redux/actions/userAction"
import { connect } from "react-redux"
import {
	faAngleLeft,
	faAngleRight,
	faTrashAlt,
	faCalendarCheck,
	faCloudSun,
	faCloudMoon,
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import "../../../styles/Wellness.css"
import DaySun from "../../../content/images/day-sun.svg"
import NightMoon from "../../../content/images/night-moon.svg"

const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	} else if (flag === 2) {
		//resend flag
		notification[type]({
			message: description,
			description: "",
			//  duration:20
		})
	} else if (flag === 3) {
		//extend expires in
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_gray' icon={faCalendarCheck}></FontAwesomeIcon>,
			description: "",
		})
	}
}
class AthleteSleep extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			loading: false,
			TodayDate: "",
			FullSelectedDate: "",
			StartTime: "",
			EndTime: "",
			IsShowRight: false,
			IsSubmit: false,
			Submitbtn: true,
			Savebtn: true,
			RecordAlreadyExists: "",
		}
		this.handleChange = this.handleChange.bind(this)
		this.handleChangeLeftDate = this.handleChangeLeftDate.bind(this)
		this.handleChangeRightDate = this.handleChangeRightDate.bind(this)
	}

	handleChange(e) {
		let self = this
		this.setState(
			{
				...this.state,
				[e.target.name]: e.target.value,
			},
			() => {
				let submitbtn = true
				let savebtn = true
				if (
					self.state.StartTime !== null &&
					self.state.StartTime !== "" &&
					self.state.EndTime !== null &&
					self.state.EndTime !== ""
				) {
					submitbtn = false
				}
				if (
					(self.state.StartTime !== null && self.state.StartTime !== "") ||
					(self.state.EndTime !== "" && self.state.EndTime !== null)
				) {
					savebtn = false
				}

				self.setState({
					...self.state,
					Submitbtn: submitbtn,
					Savebtn: savebtn,
				})
			}
		)
	}
	handleValidation = () => {
		let submitbtn = true
		let savebtn = true
		if (
			this.state.StartTime !== null &&
			this.state.StartTime !== "" &&
			this.state.EndTime !== null &&
			this.state.EndTime !== ""
		) {
			submitbtn = false
		}
		if (
			(this.state.StartTime !== null &&
				this.state.StartTime !== "" &&
				this.state.EndTime === "" &&
				this.state.EndTime === null) ||
			(this.state.StartTime === null &&
				this.state.StartTime === "" &&
				this.state.EndTime !== "" &&
				this.state.EndTime !== null)
		) {
			savebtn = false
		}

		this.setState({
			...this.state,
			Submitbtn: submitbtn,
			Savebtn: savebtn,
		})
	}
	handleChangeLeftDate() {
		const strDate = this.state.FullSelectedDate
		strDate.setDate(strDate.getDate() - 1)
		const Selecteddate = `${strDate.getDate()}-${strDate.getMonth() + 1}-${strDate.getFullYear()}`
		this.setState({
			...this.state,
			TodayDate: Selecteddate,
			FullSelectedDate: strDate,
			IsShowRight: true,
		})

		if (this.state.FullSelectedDate === new Date()) {
			this.setState({
				...this.state,
				IsShowRight: true,
			})
		}
		this.fetch(Selecteddate)
	}

	handleChangeRightDate() {
		let Selecteddate
		if (this.state.FullSelectedDate <= new Date()) {
			const strDate = this.state.FullSelectedDate
			strDate.setDate(strDate.getDate() + 1)
			const Ndate = new Date()
			Selecteddate = `${strDate.getDate()}-${strDate.getMonth() + 1}-${strDate.getFullYear()}`
			const NewSelecteddate = `${Ndate.getDate()}-${Ndate.getMonth() + 1}-${Ndate.getFullYear()}`
			let Enableright = true
			if (NewSelecteddate === Selecteddate) {
				Enableright = false
			}
			this.setState({
				...this.state,
				TodayDate: Selecteddate,
				FullSelectedDate: strDate,
				IsShowRight: Enableright,
			})
		}
		this.fetch(Selecteddate)
	}
	SaveAthleteSleep = (IsSubmit) => {
		let self = this
		if (self.state.StartTime === null && self.state.EndTime === null) {
			return false
		}
		let InputData = [
			{
				SleepDate: self.state.TodayDate,
				StartTime: self.state.StartTime,
				EndTime: self.state.EndTime,
				IsSubmit: IsSubmit,
			},
		]
		APIRequest.getPostService(APIUrl.ADD_ATHLETE_SLEEP, InputData)
			.then((response) => {
				if (response.Succeeded === true) {
					if (response.Data === "Already exists") {
						openNotificationWithIcon("success", "Athlete Sleep already Submitted.", 3)
						self.setState({ ...self.state, RecordAlreadyExists: "Record already Submitted." })
					} else {
						let subBtn = false
						let savebtn = true
						if (IsSubmit) {
							openNotificationWithIcon("success", "Athlete Sleep Submitted Successfully.", 2)
							subBtn = true
						} else {
							openNotificationWithIcon("success", "Athlete Sleep Saved Successfully.", 2)
							savebtn = true
						}

						self.setState({
							...self.state,
							loading: true,
							RecordAlreadyExists: "",
							Submitbtn: subBtn,
							Savebtn: savebtn,
						})
						self.fetch(self.table.current.state)
					}
				}
			})
			.catch((error) => {})
	}
	fetch(date) {
		let SelectedDate = date
		this.setState({ loading: true })
		APIRequest.getPostService(APIUrl.GET_ATHLETE_SLEEP_BY_DATE + "/" + SelectedDate)
			.then((response) => {
				if (response.Data.InvitedUserID > 0) {
					let NSavebtn = true
					if (
						((response.Data.StartTime !== null && response.Data.StartTime !== "") ||
							(response.Data.EndTime !== "" && response.Data.EndTime !== null)) &&
						!response.Data.IsSubmit
					) {
						NSavebtn = false
					}
					let Nsubmitbtn = true
					if (
						response.Data.StartTime !== null &&
						response.Data.StartTime !== "" &&
						response.Data.EndTime !== "" &&
						response.Data.EndTime !== null &&
						!response.Data.IsSubmit
					) {
						Nsubmitbtn = false
					} else if (response.Data.IsSubmit) {
						Nsubmitbtn = true
					}
					this.setState({
						loading: false,
						StartTime: response.Data.StartTime,
						EndTime: response.Data.EndTime,
						IsSubmit: response.Data.IsSubmit,
						Submitbtn: Nsubmitbtn,
						Savebtn: NSavebtn,
					})
				} else {
					let NSavebtn = true
					if (
						(response.Data.StartTime !== null && response.Data.StartTime !== "") ||
						(response.Data.EndTime !== "" && response.Data.EndTime !== null)
					) {
						NSavebtn = false
					}
					this.setState({
						loading: false,
						StartTime: "",
						EndTime: "",
						Submitbtn: true,
						Savebtn: NSavebtn,
					})
				}
			})
			.catch((error) => {
				this.setState({ loading: false })
			})
	}

	componentDidMount() {
		const current = new Date()
		const date = `${current.getDate()}-${current.getMonth() + 1}-${current.getFullYear()}`
		this.setState({ FullSelectedDate: current, TodayDate: date })
		this.fetch(date)
	}
	render() {
		return (
			<div className='margin-top-2'>
				<form>
					<div className='row'>
						<div className='col-md-12 text-center' style={{ fontSize: "20px" }}>
							<span onClick={this.handleChangeLeftDate}>
								<i>
									<FontAwesomeIcon icon={faAngleLeft} />
								</i>
							</span>
							{/* &nbsp;&nbsp; */}
							<span className="m-3">
							{this.state.TodayDate}
							</span>
							{this.state.IsShowRight ? (
								<span onClick={this.handleChangeRightDate}>
									<i>
										<FontAwesomeIcon icon={faAngleRight} />
									</i>
								</span>
							) : null}
						</div>
					</div>
					<div className='row athlete-sleep-section'>
						<div className='col-md-1'></div>
						<div className='col-md-4 inputWithIcon'>
							<input
								size='medium'
								type='time'
								name='StartTime'
								value={this.state.StartTime}
								onChange={this.handleChange}
								className='form-control text-center athlete-sleep-input-border'
							/>
							<i className='athlete-sleep-icon-font'>
								<img src={DaySun} style={{ width: "50px" }} />
							</i>
						</div>
						<div className='col-md-2 text-center'></div>
						<div className='col-md-4 inputWithIcon'>
							<input
								size='medium'
								type='time'
								name='EndTime'
								value={this.state.EndTime}
								onChange={this.handleChange}
								className='form-control text-center athlete-sleep-input-border'
							/>
							<i className='athlete-sleep-icon-font'>
								<img src={NightMoon} style={{ width: "50px" }} />
							</i>
						</div>
						<div className='col-md-1'></div>
					</div>
					<div className='row text-center athlete-sleep-section'>
						<div className='col-sm-12'>
							<Button
								className='btn_save_green mr-5'
								disabled={this.state.Savebtn}
								onClick={() => this.SaveAthleteSleep(false)}
							>
								Save
							</Button>
							<Button
								className='btn_save_green'
								disabled={this.state.Submitbtn}
								onClick={() => this.SaveAthleteSleep(true)}
							>
								Submit
							</Button>
						</div>
					</div>
				</form>
			</div>
		)
	}
}

const mapPropsToDispatch = (dispatch) => {
	return {
		SetFullName: (UserFullname) => {
			dispatch(ReduxAction.addFullName(UserFullname))
		},
		SetPersonCompleted: (PersonCompleted) => {
			dispatch(ReduxAction.setPersoanlCompleted(PersonCompleted))
		},
	}
}

export default connect(null, mapPropsToDispatch)(AthleteSleep)
