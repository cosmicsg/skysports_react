import React, { Component } from "react"
import { notification, Spin, Input, Row, Col } from "antd"
import { Button, Form } from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import "../../../styles/rating.css"
import {
	faAngleLeft,
	faAngleRight,
	faTrashAlt,
	faCalendarCheck,
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import moment from "moment"
import InputRange from "react-input-range"
import "../../../styles/Wellness.css"
import MaleAthlete from "../../../content/images/Male.png"
import FemaleAthlete from "../../../content/images/Female.png"
import PainWhite from "../../../content/images/Pain Area.png"
import PainRed from "../../../content/images/Pain Indicator.png"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import domtoimage from "dom-to-image"

const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	} else if (flag === 2) {
		//resend flag
		notification[type]({
			message: description,
			description: "",
			//  duration:20
		})
	} else if (flag === 3) {
		//extend expires in
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_gray' icon={faCalendarCheck}></FontAwesomeIcon>,
			description: "",
		})
	}
};

var Picloading = false;
var PicMinizeImg = false;

class AthleteWellnessQuestion extends Component {
	constructor(props) {
		super(props)
		this.state = {
			loading: false,
			todayDate: "",
			fullSelcetedDate: "",
			savebtn: false,
			submitbtn: false,
			isShowRight: false,
			Male: false,
			MinizeImg: false,
			questionOneValue: 1,
			questionTwoValue: 1,
			questionThreeValue: 1,
			questionFourValue: 1,
			questionFiveValue: 1,

			Mforehead: false,
			MleftSholder: false,
			MrightSholder: false,
			MleftChest: false,
			MrightChest: false,
			MleftElbow: false,
			MrightElbow: false,
			MleftWrist: false,
			MrightWrist: false,
			MleftKnee: false,
			MrightKnee: false,
			MleftFoot: false,
			MrightFoot: false,
			MbackNeck: false,
			MFullback: false,
			MleftThigh: false,
			MrightThigh: false,
			MleftLeg: false,
			MrightLeg: false,
			MleftInStep: false,
			MrightInStep: false,

			Fforehead: false,
			FleftSholder: false,
			FrightSholder: false,
			FleftChest: false,
			FrightChest: false,
			FleftElbow: false,
			FrightElbow: false,
			FAbdomen: false,
			FleftWrist: false,
			FrightWrist: false,
			FleftKnee: false,
			FrightKnee: false,
			FleftFoot: false,
			FrightFoot: false,
			FbackNeck: false,
			FFullback: false,
			FleftThigh: false,
			FrightThigh: false,
			FleftLeg: false,
			FrightLeg: false,
			FleftInStep: false,
			FrightInStep: false,
		}
		this.handleRangeChange = this.handleRangeChange.bind(this)
		this.handleChangeLeftDate = this.handleChangeLeftDate.bind(this)
		this.handleChangeRightDate = this.handleChangeRightDate.bind(this)
		this.handleOnClick = this.handleOnClick.bind(this)
	}

	handleOnClick(name, val) {
		if (this.state.Male) {
			if (val === false) {
				this.setState({ ...this.state, [name]: true })
			} else {
				this.setState({ ...this.state, [name]: false })
			}
		} else {
			if (val === false) {
				this.setState({ ...this.state, [name]: true })
			} else {
				this.setState({ ...this.state, [name]: false })
			}
		}
	}

	handleChangeLeftDate() {
		const strDate = this.state.fullSelectedDate
		strDate.setDate(strDate.getDate() - 1)
		const Selecteddate = `${strDate.getDate()}-${strDate.getMonth() + 1}-${strDate.getFullYear()}`
		this.setState({
			...this.state,
			todayDate: Selecteddate,
			fullSelectedDate: strDate,
			isShowRight: true,
		})

		if (this.state.fullSelectedDate === new Date()) {
			this.setState({
				...this.state,
				isShowRight: true,
			})
		}
		this.fetch(Selecteddate)
	}

	handleChangeRightDate() {
		let Selecteddate
		if (this.state.fullSelectedDate <= new Date()) {
			const strDate = this.state.fullSelectedDate
			strDate.setDate(strDate.getDate() + 1)
			const Ndate = new Date()
			Selecteddate = `${strDate.getDate()}-${strDate.getMonth() + 1}-${strDate.getFullYear()}`
			const NewSelecteddate = `${Ndate.getDate()}-${Ndate.getMonth() + 1}-${Ndate.getFullYear()}`
			let Enableright = true
			if (NewSelecteddate === Selecteddate) {
				Enableright = false
			}
			this.setState({
				...this.state,
				todayDate: Selecteddate,
				fullSelectedDate: strDate,
				isShowRight: Enableright,
			})
		}
		this.fetch(Selecteddate)
	}
	handleRangeChange(e) {
		let self = this
		self.setState(
			{
				...self.state,
				[e.target.name]: e.target.value,
			},
			function () {
				if (
					self.state.questionOneValue > 1 &&
					self.state.questionTwoValue > 1 &&
					self.state.questionThreeValue > 1 &&
					self.state.questionFourValue > 1 &&
					self.state.questionFiveValue > 1
				) {
					self.setState({
						...self.state,
						submitbtn: true,
					})
				}
			}
		)
	}
	async SaveAthleteWellnessQA(IsSubmit) {
		debugger
		let self = this
		PicMinizeImg = true
		self.setState({
			...self.state,
			loading: true,
		})
		 window.scrollTo(0, 0)
		let Maleval = {
			Mforehead: self.state.Mforehead,
			MleftSholder: self.state.MleftSholder,
			MrightSholder: self.state.MrightSholder,
			MleftChest: self.state.MleftChest,
			MrightChest: self.state.MrightChest,
			MleftElbow: self.state.MleftElbow,
			MrightElbow: self.state.MrightElbow,
			MleftWrist: self.state.MleftWrist,
			MrightWrist: self.state.MrightWrist,
			MleftKnee: self.state.MleftKnee,
			MrightKnee: self.state.MrightKnee,
			MleftFoot: self.state.MleftFoot,
			MrightFoot: self.state.MrightFoot,
			MbackNeck: self.state.MbackNeck,
			MFullback: self.state.MFullback,
			MleftThigh: self.state.MleftThigh,
			MrightThigh: self.state.MrightThigh,
			MleftLeg: self.state.MleftLeg,
			MrightLeg: self.state.MrightLeg,
			MleftInStep: self.state.MleftInStep,
			MrightInStep: self.state.MrightInStep,
		}
		let Femaleval = {
			Fforehead: self.state.Fforehead,
			FleftSholder: self.state.FleftSholder,
			FrightSholder: self.state.FrightSholder,
			FleftChest: self.state.FleftChest,
			FrightChest: self.state.FrightChest,
			FleftElbow: self.state.FleftElbow,
			FrightElbow: self.state.FrightElbow,
			FAbdomen: self.state.FAbdomen,
			FleftWrist: self.state.FleftWrist,
			FrightWrist: self.state.FrightWrist,
			FleftKnee: self.state.FleftKnee,
			FrightKnee: self.state.FrightKnee,
			FleftFoot: self.state.FleftFoot,
			FrightFoot: self.state.FrightFoot,
			FbackNeck: self.state.FbackNeck,
			FFullback: self.state.FFullback,
			FleftThigh: self.state.FleftThigh,
			FrightThigh: self.state.FrightThigh,
			FleftLeg: self.state.FleftLeg,
			FrightLeg: self.state.FrightLeg,
			FleftInStep: self.state.FleftInStep,
			FrightInStep: self.state.FrightInStep,
		}
		let formData = self.state.Male ? JSON.stringify(Maleval) : JSON.stringify(Femaleval)

		var node = document.getElementById("my-node")
		node.setAttribute("style", "width: 300px;")
		var imageSrc = ""
		await domtoimage
			.toPng(node)
			.then(function (dataUrl) {
				var img = new Image()
				img.src = dataUrl
				imageSrc = img.src
			})
			.catch(function (error) {
				console.error("oops, something went wrong!", error)
			})
		node.setAttribute("style", "width: 100%; height: auto; ")
		let InputData = [
			{
				WellnessDate: self.state.todayDate,
				QuestionOne: self.state.questionOneValue,
				QuestionTwo: self.state.questionTwoValue,
				QuestionThree: self.state.questionThreeValue,
				QuestionFour: self.state.questionFourValue,
				QuestionFive: self.state.questionFiveValue,
				ImageBase64: imageSrc,
				FormData: formData,
				IsSubmit: IsSubmit,
			},
		]
		APIRequest.getPostService(APIUrl.ADD_ATHLETE_WELLNESS_QA, InputData)
			.then((response) => {
				if (response.Succeeded === true) {
					if (response.Data === "Already exists") {
						openNotificationWithIcon("success", "Athlete Sleep already Submitted.", 3)
						self.setState({ ...self.state, RecordAlreadyExists: "Record already Submitted." })
					} else {
						let subBtn = false
						let savebtn = false
						if (IsSubmit) {
							openNotificationWithIcon("success", "Athlete Sleep Submitted Successfully.", 2)
							subBtn = true
						} else {
							openNotificationWithIcon("success", "Athlete Sleep Saved Successfully.", 2)
							savebtn = true
						}

						self.setState({
							...self.state,
							loading: false,
							RecordAlreadyExists: "",
							submitbtn: subBtn,
							savebtn: savebtn,
							MinizeImg: false,
						})
						PicMinizeImg = false
						self.fetch(self.table.current.state)
					}
				}
			})
			.catch((error) => {})
	}

	async fetch(date) {
		let SelectedDate = date

		APIRequest.getPostService(APIUrl.GET_ATHLETE_WELLNESS_QA_BY_DATE + "/" + SelectedDate)
			.then((response) => {
				if (response.Data.InvitedUserID > 0) {
					let Nsavebtn = true
					if (
						((response.Data.QuestionOne !== null && response.Data.QuestionOne !== "") ||
							(response.Data.QuestionTwo !== null && response.Data.QuestionTwo !== "") ||
							(response.Data.QuestionThree !== null && response.Data.QuestionThree !== "") ||
							(response.Data.QuestionFour !== null && response.Data.QuestionFour !== "") ||
							(response.Data.QuestionFive !== null && response.Data.QuestionFive !== "") ||
							(response.Data.FormData !== "" && response.Data.FormData !== null)) &&
						!response.Data.IsSubmit
					) {
						Nsavebtn = false
					}
					let Nsubmitbtn = true
					if (
						response.Data.QuestionOne !== null &&
						response.Data.QuestionOne !== "" &&
						response.Data.QuestionTwo !== null &&
						response.Data.QuestionTwo !== "" &&
						response.Data.QuestionThree !== null &&
						response.Data.QuestionThree !== "" &&
						response.Data.QuestionFour !== null &&
						response.Data.QuestionFour !== "" &&
						response.Data.QuestionFive !== null &&
						response.Data.QuestionFive !== "" &&
						response.Data.FormData !== "" &&
						response.Data.FormData !== null &&
						!response.Data.IsSubmit
					) {
						Nsubmitbtn = false
					} else if (response.Data.IsSubmit) {
						Nsubmitbtn = true
					}
					let gender = response.Data.GenderID === 2 ? true : false
					if (response.Data.GenderID === 1) {
						let FemaleData = JSON.parse(response.Data.FormData)
						this.setState({
							loading: false,
							questionOneValue: response.Data.QuestionOne,
							questionTwoValue: response.Data.QuestionTwo,
							questionThreeValue: response.Data.QuestionThree,
							questionFourValue: response.Data.QuestionFour,
							questionFiveValue: response.Data.QuestionFive,
							Fforehead: FemaleData.Fforehead,
							FleftSholder: FemaleData.FleftSholder,
							FrightSholder: FemaleData.FrightSholder,
							FleftChest: FemaleData.FleftChest,
							FrightChest: FemaleData.FrightChest,
							FleftElbow: FemaleData.FleftElbow,
							FrightElbow: FemaleData.FrightElbow,
							FAbdomen: FemaleData.FAbdomen,
							FleftWrist: FemaleData.FleftWrist,
							FrightWrist: FemaleData.FrightWrist,
							FleftKnee: FemaleData.FleftKnee,
							FrightKnee: FemaleData.FrightKnee,
							FleftFoot: FemaleData.FleftFoot,
							FrightFoot: FemaleData.FrightFoot,
							FbackNeck: FemaleData.FbackNeck,
							FFullback: FemaleData.FFullback,
							FleftThigh: FemaleData.FleftThigh,
							FrightThigh: FemaleData.FrightThigh,
							FleftLeg: FemaleData.FleftLeg,
							FrightLeg: FemaleData.FrightLeg,
							FleftInStep: FemaleData.FleftInStep,
							FrightInStep: FemaleData.FrightInStep,
							Male: gender,
							submitbtn: Nsubmitbtn,
							savebtn: Nsavebtn,
						})
					} else {
						let MaleData = JSON.parse(response.Data.FormData)
						this.setState({
							loading: false,
							questionOneValue: response.Data.QuestionOne,
							questionTwoValue: response.Data.QuestionTwo,
							questionThreeValue: response.Data.QuestionThree,
							questionFourValue: response.Data.QuestionFour,
							questionFiveValue: response.Data.QuestionFive,
							Mforehead: MaleData.Mforehead,
							MleftSholder: MaleData.MleftSholder,
							MrightSholder: MaleData.MrightSholder,
							MleftChest: MaleData.MleftChest,
							MrightChest: MaleData.MrightChest,
							MleftElbow: MaleData.MleftElbow,
							MrightElbow: MaleData.MrightElbow,
							MleftWrist: MaleData.MleftWrist,
							MrightWrist: MaleData.MrightWrist,
							MleftKnee: MaleData.MleftKnee,
							MrightKnee: MaleData.MrightKnee,
							MleftFoot: MaleData.MleftFoot,
							MrightFoot: MaleData.MrightFoot,
							MbackNeck: MaleData.MbackNeck,
							MFullback: MaleData.MFullback,
							MleftThigh: MaleData.MleftThigh,
							MrightThigh: MaleData.MrightThigh,
							MleftLeg: MaleData.MleftLeg,
							MrightLeg: MaleData.MrightLeg,
							MleftInStep: MaleData.MleftInStep,
							MrightInStep: MaleData.MrightInStep,
							Male: gender,
							submitbtn: Nsubmitbtn,
							savebtn: Nsavebtn,
						})
					}
				} else {
					let Nsavebtn = true
					if (
						(response.Data.QuestionOne !== null && response.Data.QuestionOne !== "") ||
						(response.Data.QuestionTwo !== null && response.Data.QuestionTwo !== "") ||
						(response.Data.QuestionThree !== null && response.Data.QuestionThree !== "") ||
						(response.Data.QuestionFour !== null && response.Data.QuestionFour !== "") ||
						(response.Data.QuestionFive !== null && response.Data.QuestionFive !== "") ||
						(response.Data.FormData !== "" && response.Data.FormData !== null)
					) {
						Nsavebtn = false
					}
					this.setState({
						loading: false,
						questionOneValue: 1,
						questionTwoValue: 1,
						questionThreeValue: 1,
						questionFourValue: 1,
						questionFiveValue: 1,

						Mforehead: false,
						MleftSholder: false,
						MrightSholder: false,
						MleftChest: false,
						MrightChest: false,
						MleftElbow: false,
						MrightElbow: false,
						MleftWrist: false,
						MrightWrist: false,
						MleftKnee: false,
						MrightKnee: false,
						MleftFoot: false,
						MrightFoot: false,
						MbackNeck: false,
						MFullback: false,
						MleftThigh: false,
						MrightThigh: false,
						MleftLeg: false,
						MrightLeg: false,
						MleftInStep: false,
						MrightInStep: false,

						Fforehead: false,
						FleftSholder: false,
						FrightSholder: false,
						FleftChest: false,
						FrightChest: false,
						FleftElbow: false,
						FrightElbow: false,
						FAbdomen: false,
						FleftWrist: false,
						FrightWrist: false,
						FleftKnee: false,
						FrightKnee: false,
						FleftFoot: false,
						FrightFoot: false,
						FbackNeck: false,
						FFullback: false,
						FleftThigh: false,
						FrightThigh: false,
						FleftLeg: false,
						FrightLeg: false,
						FleftInStep: false,
						FrightInStep: false,

						submitbtn: true,
						savebtn: Nsavebtn,
					})
				}
			})
			.catch((error) => {
				this.setState({ loading: false })
			})
	}
	async componentDidMount() {
		const current = new Date()
		const date = `${current.getDate()}-${current.getMonth() + 1}-${current.getFullYear()}`
		this.setState({ fullSelectedDate: current, todayDate: date })

		const TrainingSchedule = await APIRequest.getPostService(APIUrl.GET_USER_PROFILE_BY_INVITED_ID)
		this.setState({ ...this.state, Male: TrainingSchedule.Data })

		this.fetch(date)
	}
	render() {
		return (
			<div className='margin-top-2 athlete-wellness-question'>
				<Spin size='large' spinning={this.state.loading} tip='Loading...'>
					<form>
						<div className='row'>
							<div className='col-md-12 text-center' style={{ fontSize: "20px" }}>
								<span onClick={this.handleChangeLeftDate}>
									<i>
										<FontAwesomeIcon icon={faAngleLeft} />
									</i>
								</span>
								{/* &nbsp;&nbsp; */}
								<span className="m-3">
								{this.state.todayDate}
								</span>
								{this.state.isShowRight ? (
									<span onClick={this.handleChangeRightDate}>
										<i>
											<FontAwesomeIcon icon={faAngleRight} />
										</i>
									</span>
								) : null}
							</div>
						</div>
						<br />
						<br />
						<br />
						<div>
							<div className='col-md-12 question-margin'>
								<div className='question-font-style'>
									Last night your sleep was restfull, satisfying and relaxing.
								</div>
								<div id='drink-holder'>
									<Row>
										<Col span={5}>a)Tribble</Col>
										<Col span={5}>b)Bad</Col>
										<Col span={5}>c)Ok</Col>
										<Col span={5}>d)Good</Col>
										<Col span={4}>e)Great Sleep</Col>
										<div className='slide-container'>
											<input
												type='range'
												name='questionOneValue'
												min='0'
												max='100'
												value={this.state.questionOneValue}
												onChange={this.handleRangeChange}
											/>
										</div>
									</Row>
								</div>
							</div>
							<div className='col-md-12 question-margin'>
								<div className='question-font-style'>
									Today your body feels physically strong and recovered.
								</div>
								<div id='drink-holder'>
									<Row>
										<Col span={5}>a)Exhausted</Col>
										<Col span={5}>b)Tired</Col>
										<Col span={5}>c)Ok</Col>
										<Col span={5}>d)Good</Col>
										<Col span={4}>e)Strong</Col>
										<div className='slide-container'>
											<input
												type='range'
												name='questionTwoValue'
												min='0'
												max='100'
												value={this.state.questionTwoValue}
												onChange={this.handleRangeChange}
											/>
										</div>
									</Row>
								</div>
							</div>
							<div className='col-md-12 question-margin'>
								<div className='question-font-style'>
									Today your laughed, are happy, and in a good mood.
								</div>
								<div id='drink-holder'>
									<Row>
										<Col span={5}>a)Miserable</Col>
										<Col span={5}>b)Grumpy</Col>
										<Col span={5}>c)Ok</Col>
										<Col span={5}>d)Good</Col>
										<Col span={4}>e)Great</Col>
										<div className='slide-container'>
											<input
												type='range'
												name='questionThreeValue'
												min='0'
												max='100'
												value={this.state.questionThreeValue}
												onChange={this.handleRangeChange}
											/>
										</div>
									</Row>
								</div>
							</div>
							<div className='col-md-12 question-margin'>
								<div className='question-font-style'>
									Today your are mentally alert, focused and engertic.
								</div>
								<div id='drink-holder'>
									<Row>
										<Col span={5}>a)Drained</Col>
										<Col span={5}>b)Little awake</Col>
										<Col span={5}>c)Ok</Col>
										<Col span={5}>d)Good</Col>
										<Col span={4}>e)Great</Col>
										<div className='slide-container'>
											<input
												type='range'
												name='questionFourValue'
												min='0'
												max='100'
												value={this.state.questionFourValue}
												onChange={this.handleRangeChange}
											/>
										</div>
									</Row>
								</div>
							</div>
							<div className='col-md-12 question-margin'>
								<div className='question-font-style'>
									Today your muscles are not feeling any soreness and pain.
								</div>
								<div id='drink-holder'>
									<Row>
										<Col span={5}>a)Very Sore</Col>
										<Col span={5}>b)Little sore</Col>
										<Col span={5}>c)Ok</Col>
										<Col span={5}>d)Good</Col>
										<Col span={4}>e)Not sore at all</Col>
										<div className='slide-container'>
											<input
												type='range'
												name='questionFiveValue'
												min='0'
												max='100'
												value={this.state.questionFiveValue}
												onChange={this.handleRangeChange}
											/>
										</div>
									</Row>
								</div>
							</div>
						</div>
						<br />
						<div>
							<div className='text-center'>TODAY A PART OF YOUR BODY HAS PAIN OR SORENESS.</div>
							{!this.state.Male ? (
								<div
									id='my-node'
									className={
										PicMinizeImg
											? "minize-image-size pain-indicator-box"
											: "maxi-image-size pain-indicator-box"
									}
								>
									<img src={MaleAthlete} className='male-female-img-size' alt='Male' />
									<div
										className='m-forehead'
										name='Mforehead'
										value={this.state.Mforehead}
										onClick={() => this.handleOnClick("Mforehead", this.state.Mforehead)}
									>
										{this.state.Mforehead ? (
											<img src={PainRed} alt='Pain Red' className='body-pain-pointer' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-left-sholder'
										name='MleftSholder'
										onClick={() => this.handleOnClick("MleftSholder", this.state.MleftSholder)}
									>
										{this.state.MleftSholder ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-right-sholder'
										name='MrightSholder'
										onClick={() => this.handleOnClick("MrightSholder", this.state.MrightSholder)}
									>
										{this.state.MrightSholder ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-left-chest'
										name='MleftChest'
										onClick={() => this.handleOnClick("MleftChest", this.state.MleftChest)}
									>
										{this.state.MleftChest ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-right-chest'
										name='MrightChest'
										onClick={() => this.handleOnClick("MrightChest", this.state.MrightChest)}
									>
										{this.state.MrightChest ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-left-elbow'
										name='MleftElbow'
										onClick={() => this.handleOnClick("MleftElbow", this.state.MleftElbow)}
									>
										{this.state.MleftElbow ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-right-elbow'
										name='MrightElbow'
										onClick={() => this.handleOnClick("MrightElbow", this.state.MrightElbow)}
									>
										{this.state.MrightElbow ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>

									<div
										className='m-left-wrist'
										name='MleftWrist'
										onClick={() => this.handleOnClick("MleftWrist", this.state.MleftWrist)}
									>
										{this.state.MleftWrist ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-right-wrist'
										name='MrightWrist'
										onClick={() => this.handleOnClick("MrightWrist", this.state.MrightWrist)}
									>
										{this.state.MrightWrist ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-left-knee'
										name='MleftKnee'
										onClick={() => this.handleOnClick("MleftKnee", this.state.MleftKnee)}
									>
										{this.state.MleftKnee ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-right-knee'
										name='MrightKnee'
										onClick={() => this.handleOnClick("MrightKnee", this.state.MrightKnee)}
									>
										{this.state.MrightKnee ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-left-foot'
										name='MleftFoot'
										onClick={() => this.handleOnClick("MleftFoot", this.state.MleftFoot)}
									>
										{this.state.MleftFoot ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-right-foot'
										name='MrightFoot'
										onClick={() => this.handleOnClick("MrightFoot", this.state.MrightFoot)}
									>
										{this.state.MrightFoot ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-back-neck'
										name='MbackNeck'
										onClick={() => this.handleOnClick("MbackNeck", this.state.MbackNeck)}
									>
										{this.state.MbackNeck ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-full-back'
										name='MFullback'
										onClick={() => this.handleOnClick("MFullback", this.state.MFullback)}
									>
										{this.state.MFullback ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-left-thigh'
										name='MleftThigh'
										onClick={() => this.handleOnClick("MleftThigh", this.state.MleftThigh)}
									>
										{this.state.MleftThigh ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-right-thigh'
										name='MrightThigh'
										onClick={() => this.handleOnClick("MrightThigh", this.state.MrightThigh)}
									>
										{this.state.MrightThigh ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-left-leg'
										name='MleftLeg'
										onClick={() => this.handleOnClick("MleftLeg", this.state.MleftLeg)}
									>
										{this.state.MleftLeg ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-right-leg'
										name='MrightLeg'
										onClick={() => this.handleOnClick("MrightLeg", this.state.MrightLeg)}
									>
										{this.state.MrightLeg ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-left-inStep'
										name='MleftInStep'
										onClick={() => this.handleOnClick("MleftInStep", this.state.MleftInStep)}
									>
										{this.state.MleftInStep ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='m-right-inStep'
										name='MrightInStep'
										onClick={() => this.handleOnClick("MrightInStep", this.state.MrightInStep)}
									>
										{this.state.MrightInStep ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
								</div>
							) : (
								<div
									id='my-node'
									className={
										PicMinizeImg
											? "minize-image-size pain-indicator-box"
											: "maxi-image-size pain-indicator-box"
									}
								>
									<img src={FemaleAthlete} alt='Fale' className='male-female-img-size' />
									<div
										className='f-forehead'
										name='Fforehead'
										onClick={() => this.handleOnClick("Fforehead", this.state.Fforehead)}
									>
										{this.state.Fforehead ? (
											<img src={PainRed} alt='Pain Red' className='body-pain-pointer' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-left-sholder'
										name='FleftSholder'
										onClick={() => this.handleOnClick("FleftSholder", this.state.FleftSholder)}
									>
										{this.state.FleftSholder ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-right-sholder'
										name='FrightSholder'
										onClick={() => this.handleOnClick("FrightSholder", this.state.FrightSholder)}
									>
										{this.state.FrightSholder ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-left-chest'
										name='FleftChest'
										onClick={() => this.handleOnClick("FleftChest", this.state.FleftChest)}
									>
										{this.state.FleftChest ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-right-chest'
										name='FrightChest'
										onClick={() => this.handleOnClick("FrightChest", this.state.FrightChest)}
									>
										{this.state.FrightChest ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-left-elbow'
										name='FleftElbow'
										onClick={() => this.handleOnClick("FleftElbow", this.state.FleftElbow)}
									>
										{this.state.FleftElbow ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-right-elbow'
										name='FrightElbow'
										onClick={() => this.handleOnClick("FrightElbow", this.state.FrightElbow)}
									>
										{this.state.FrightElbow ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>

									<div
										className='f-abdomen'
										name='FAbdomen'
										onClick={() => this.handleOnClick("FAbdomen", this.state.FAbdomen)}
									>
										{this.state.FAbdomen ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>

									<div
										className='f-left-wrist'
										name='FleftWrist'
										onClick={() => this.handleOnClick("FleftWrist", this.state.FleftWrist)}
									>
										{this.state.FleftWrist ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-right-wrist'
										name='FrightWrist'
										onClick={() => this.handleOnClick("FrightWrist", this.state.FrightWrist)}
									>
										{this.state.FrightWrist ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-left-knee'
										name='FleftKnee'
										onClick={() => this.handleOnClick("FleftKnee", this.state.FleftKnee)}
									>
										{this.state.FleftKnee ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-right-knee'
										name='FrightKnee'
										onClick={() => this.handleOnClick("FrightKnee", this.state.FrightKnee)}
									>
										{this.state.FrightKnee ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-left-foot'
										name='FleftFoot'
										onClick={() => this.handleOnClick("FleftFoot", this.state.FleftFoot)}
									>
										{this.state.FleftFoot ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-right-foot'
										name='FrightFoot'
										onClick={() => this.handleOnClick("FrightFoot", this.state.FrightFoot)}
									>
										{this.state.FrightFoot ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-back-neck'
										name='FbackNeck'
										onClick={() => this.handleOnClick("FbackNeck", this.state.FbackNeck)}
									>
										{this.state.FbackNeck ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-full-back'
										name='FFullback'
										onClick={() => this.handleOnClick("FFullback", this.state.FFullback)}
									>
										{this.state.FFullback ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-left-thigh'
										name='FleftThigh'
										onClick={() => this.handleOnClick("FleftThigh", this.state.FleftThigh)}
									>
										{this.state.FleftThigh ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-right-thigh'
										name='FrightThigh'
										onClick={() => this.handleOnClick("FrightThigh", this.state.FrightThigh)}
									>
										{this.state.FrightThigh ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-left-leg'
										name='FleftLeg'
										onClick={() => this.handleOnClick("FleftLeg", this.state.FleftLeg)}
									>
										{this.state.FleftLeg ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-right-leg'
										name='FrightLeg'
										onClick={() => this.handleOnClick("FrightLeg", this.state.FrightLeg)}
									>
										{this.state.FrightLeg ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-left-inStep'
										name='FleftInStep'
										onClick={() => this.handleOnClick("FleftInStep", this.state.FleftInStep)}
									>
										{this.state.FleftInStep ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
									<div
										className='f-right-inStep'
										name='FrightInStep'
										onClick={() => this.handleOnClick("FrightInStep", this.state.FrightInStep)}
									>
										{this.state.FrightInStep ? (
											<img className='body-pain-pointer' src={PainRed} alt='Pain Red' />
										) : (
											<img src={PainWhite} alt='Pain White' className='body-pain-pointer' />
										)}
									</div>
								</div>
							)}
							<div className='img-div'>
								<div id='h-output'></div>
							</div>
						</div>
						<br />
						<div className='row text-center'>
							<div className='col-sm-12'>
								<Button
									className='btn_save_green mr-5'
									disabled={this.state.savebtn}
									onClick={() => this.SaveAthleteWellnessQA(false)}
								>
									Save
								</Button>
								<Button
									className='btn_save_green'
									disabled={this.state.submitbtn}
									onClick={() => this.SaveAthleteWellnessQA(true)}
								>
									Submit
								</Button>
							</div>
						</div>
					</form>
				</Spin>
			</div>
		)
	}
}

export default AthleteWellnessQuestion
