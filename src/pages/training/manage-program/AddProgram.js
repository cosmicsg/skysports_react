import React, { useState, useEffect } from "react"
import { Select, Input, DatePicker, Space, Empty } from "antd"
import { Button, Modal } from "react-bootstrap"
import UserImage from "../../../content/images/UserImage.png"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlusSquare, faTrashAlt } from "@fortawesome/free-solid-svg-icons"
import { APIRequest, showNotification } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import moment from "moment"
const { Option } = Select

//concat string ends with dot notation if the string length > 25
const handleStringEndsWithDotNotation = (str) => {
	let tempString = str
	if (tempString !== "" && tempString !== undefined && tempString !== null) {
		let temp = tempString.toString()
		if (temp.length > 20) {
			tempString = temp.slice(0, 21).concat("...")
		}
	}
	return tempString
}

// Get only date from date time field and reset time as 0 for hh:mm:ss
const GetDate = (date) => {
	if (date) return new Date(date).setHours(0, 0, 0, 0)
	return new Date().setHours(0, 0, 0, 0)
}

//AddProgram Component
const AddProgram = (props) => {
	const [programProps, setProgramProps] = useState({
		programName: "",
		programID: 0,
		goal: "",
		sportID: [],
		athleteType: [],
		startDate: "",
		endDate: "",
	})

	const [isAdd] = useState(props.isAdd)
	const [errorProps, setErrorProps] = useState({})

	const [disableEndDate, setDisableEndDate] = useState(true)
	const [sportsData, setSportsData] = useState([])
	const [athleteTypeData, setAthleteTypeData] = useState([])
	const [tempAvailableUser, setTempAvailableUser] = useState([])

	const [searchKey, setSearchKey] = useState("")
	const [selectedUserData, setSelectedUserData] = useState([])
	const [availableUserData, setAvailableUserData] = useState([])

	//UserEffect, it is a lifecycle method which is used to load dropdown values.
	useEffect(() => {
		if (Object.keys(props.editProps).length > 0 && !props.isAdd) {
			setProgramProps((prev) => ({
				...prev,
				programName: props.editProps.ProgramName ? props.editProps.ProgramName : "",
				goal: props.editProps.FocusGoal ? props.editProps.FocusGoal : "",
				startDate: props.editProps.StartDate ? moment(GetDate(props.editProps.StartDate)) : "",
				endDate: props.editProps.EndDate ? moment(GetDate(props.editProps.EndDate)) : "",
			}))
			setDisableEndDate(false)
		}
		//Load sports data
		APIRequest.getGetService(APIUrl.GET_SPORT_LIST).then((res) => {
			setSportsData(res.Data)

			let isEdit = Object.keys(props.editProps).length > 0 && !props.isAdd ? true : false
			let arrSportIDs =
				isEdit && props.editProps
					? props.editProps.SportID.split(",").map((item) => parseInt(item))
					: []
			setProgramProps((prevState) => ({
				...prevState,
				sportID: arrSportIDs,
			}))

			if (isEdit) loadAvailableUserList(arrSportIDs, props.editProps.ProgramID, props.editProps.StartDate, props.editProps.EndDate)
		})
		APIRequest.getGetService(APIUrl.GET_ATHLETE_TYPE).then((res) => {
			setAthleteTypeData(res.Data)
			setProgramProps((prevState) => ({
				...prevState,
				athleteType:
					Object.keys(props.editProps).length > 0 && !props.isAdd
						? props.editProps.AthleteTypeID
						: [],
			}))
		})
	}, [])

	//handling onchange event
	const onChange = (name, eventOrValue) => {
		let tempArr = ["sportID", "athleteType", "startDate", "endDate"]
		let val = tempArr.includes(name) ? eventOrValue : eventOrValue.target.value

		//update onchange value with respective name of the field
		setProgramProps({
			...programProps,
			[name]: val,
		})



		//enable EndDate only when start date is having the values
		if (name === "startDate" && eventOrValue) {
			setProgramProps({
				...programProps,
				startDate: val,
				endDate: ""
			})
			setDisableEndDate(false)
		}

		//load available athlete let by based on selected sport
		if (name === "sportID" && eventOrValue.length === 0) clearUsers() //loadAvailableUserList(eventOrValue);
	}

	//Clear
	const clearUsers = () => {
		setAvailableUserData([])
		setSelectedUserData([])
	}

	//Load available users list
	const loadAvailableUserList = (val, programId, StartDate, EndDate) => {
		if (StartDate != "" && EndDate != "" && val) {
			if (val) {
				let programID = programId ? parseInt(programId) : 0
				let temp = val.join(",")
				let startDate = moment(StartDate).format("YYYY-MM-DD").toString();
				let endDate = moment(EndDate).format("YYYY-MM-DD").toString()

				let url = APIUrl.GET_AVAILABLE_ATHLETES + "/" + temp + "/" + programID + "/" + startDate + "/" + endDate
				APIRequest.getGetService(url).then((res) => {
					if (res.Succeeded) {
						setAvailableUserData(res.Data.AvailableAthletes)
						setSelectedUserData(res.Data.SelectedAthletes)
					}
				})
			}
		}
	}

	//Assign date
	const assignDate = (name, dateString, val) => {
		//update date value by calling onChange function
		let temp = val ? moment(GetDate(val)) : ""
		onChange(name, temp)
	}

	//Disable Start date selection
	const disabledStartDateSelection = (current) => {
		let today = moment(GetDate())
		return current < today
	}

	//Disable End date selection
	const disabledDateEndDateSelection = (current) => {
		if (programProps.startDate) return current < moment(programProps.startDate)
		else return current
	}

	//Search Available athlete
	const searchAthlete = (e) => {
		let val = e.target.value
		setSearchKey(val)
		SearchAvailableAthlete(val)
	}

	//Search available athletes
	const SearchAvailableAthlete = (val, data, isFromSelected) => {
		if (val) {
			let searchData = []
			if (tempAvailableUser.length === 0) {
				searchData = availableUserData
				setTempAvailableUser(availableUserData)
			}
			searchData = isFromSelected ? data : searchData.length === 0 ? tempAvailableUser : searchData
			let tempAvailable = searchData.filter(
				(x) =>
					x.FullName.toLowerCase().startsWith(val.trim()) ||
					x.UserName.toLowerCase().startsWith(val.trim())
			)
			setAvailableUserData(tempAvailable)
		} else {
			setAvailableUserData(tempAvailableUser)
		}
	}

	//Add User from Available athlete list
	const addUser = (id) => {
		let tempAvailable = availableUserData.filter((x) => x.UserProfileID !== id)
		let tempSelected = availableUserData.filter((x) => x.UserProfileID === id)
		setAvailableUserData(tempAvailable)
		if (availableUserData.length === tempAvailableUser.length) {
			setTempAvailableUser([])
		} else {
			let temp1 = tempAvailableUser.filter((x) => x.UserProfileID !== id)
			setTempAvailableUser(temp1)
		}

		let temp = tempSelected.concat(selectedUserData)
		setSelectedUserData(temp)
		showNotification("Available Athlete added successfully", "success")
	}

	//Remove user from Selected athlete List
	const removeUser = (id) => {
		let tempSelected = selectedUserData.filter((x) => x.UserProfileID !== id)
		let tempAvailable = selectedUserData.filter((x) => x.UserProfileID === id)
		setSelectedUserData(tempSelected)
		let temp = tempAvailable.concat(availableUserData)
		let temp2 = tempAvailable.concat(tempAvailableUser)
		setTempAvailableUser(temp2)
		setAvailableUserData(temp)
		if (searchKey) SearchAvailableAthlete(searchKey, temp2, true)
		showNotification("Selected Athlete removed successfully", "success")
	}

	//Check empty field validation
	const checkEmptyFieldValidation = () => {
		let isError = {}
		let flag = false
		if (
			programProps.programName === "" ||
			programProps.programName === undefined ||
			programProps.programName === null
		) {
			flag = true
			isError["programName"] = "Please enter Program Name"
		}
		if (programProps.goal === "" || programProps.goal === undefined || programProps.goal === null) {
			flag = true
			isError["goal"] = "Please enter Focus/Goal"
		}
		if (
			programProps.sportID === undefined ||
			programProps.sportID === null ||
			programProps.sportID.length === 0
		) {
			flag = true
			isError["sportID"] = "Please select Sport"
		}
		if (
			programProps.athleteType === undefined ||
			programProps.athleteType === null ||
			programProps.athleteType.length === 0
		) {
			flag = true
			isError["athleteType"] = "Please select Athlete Type"
		}
		if (
			programProps.startDate === "" ||
			programProps.startDate === undefined ||
			programProps.startDate === null
		) {
			flag = true
			isError["startDate"] = "Please select Start Date"
		}
		if (
			programProps.endDate === "" ||
			programProps.endDate === undefined ||
			programProps.endDate === null
		) {
			flag = true
			isError["endDate"] = "Please select End Date"
		}
		setErrorProps(isError)
		return flag
	}

	//Save
	const save = () => {
		if (!checkEmptyFieldValidation()) {
			let userIds = selectedUserData.map((item) => item.UserProfileID)

			let programModel = {
				ProgramName: programProps.programName,
				FocusGoal: programProps.goal,
				SportID: programProps.sportID ? programProps.sportID.join(",") : "",
				AthleteTypeID: programProps.athleteType,
				StartDate: moment(programProps.startDate).format("YYYY-MM-DD"),
				EndDate: moment(programProps.endDate).format("YYYY-MM-DD"),
				UserIDs: userIds.join(","),
				TrainingProgramID: props.editProps ? props.editProps.ProgramID : 0,
				TrainingProgramSportIDs: props.editProps ? props.editProps.SportID : "",
				TrainingProgramUserIDs: props.editProps ? props.editProps.UserIDs : "",
			}
			APIRequest.getPostService(APIUrl.SAVE_TRAINING_PROGRAM, programModel)
				.then((res) => {
					close(true)
					if (res.Data.Success) {
						props.refreshList()
						if (isAdd) {
							showNotification("Training Program Created Successfully.", "success")

						}
						else {
							showNotification("Training Program Updated Successfully.", "success")
						}
						// {isAdd ? (
						// showNotification("Training Program created successfully.", "success")
						// ) : (showNotification("Training Program Updated Successfully.", "success")
						// )}
					}

				})
				.catch((err) => {
					showNotification("Internal error occurred, please try again!", "error")
				})
		}
	}

	//Clear Properties
	const clearProperties = () => {
		setProgramProps({
			programName: "",
			goal: "",
			sportID: [],
			athleteType: [],
			startDate: "",
			endDate: "",
		})
		setErrorProps({})
		setDisableEndDate(true)
		setSportsData([])
		setAthleteTypeData([])
		setSearchKey("")
		setAvailableUserData([])
		setSelectedUserData([])
		setTempAvailableUser([])
	}

	//Close popup
	const close = (flag) => {
		clearProperties()
		closePopup(flag)
	}

	const closePopup = (flag) => {
		props.updateClosePopup(flag)
	}

	const getUsers = () => {
		loadAvailableUserList(programProps.sportID, programProps.progprogramID, programProps.startDate, programProps.endDate)
	}

	return (
		<>
			<Modal show={true} backdrop='static' size='lg'>
				<Modal.Header>
					<label>{isAdd === true ? "Add" : "Edit"} Programme</label>
					<div className='col-sm-1'>
						<button
							type='button'
							title='Close'
							className='close'
							area-label='Close'
							onClick={closePopup}
						>
							<span style={{ color: "#c29a37" }}>&times;</span>
						</button>
					</div>
				</Modal.Header>
				<Modal.Body>
					<>
						<div className='add-program-box form-group'>
							<div className='row col-sm-12 form-group'>
								<div className='col-sm-4'>
									<label>
										Programme Name<span className='text-danger'>*</span>
									</label>
									<Input
										type='text'
										name='programName'
										size='large'
										autoComplete='off'
										max={200}
										value={programProps.programName}
										onChange={(val) => onChange("programName", val)}
										bordered={false}
										className='textfiled w-80'
										placeholder='Program Name'
									></Input>
									<span className='text-danger'>{errorProps.programName}</span>
								</div>
								<div className='col-sm-4'>
									<label>
										Focus/Goal<span className='text-danger'>*</span>
									</label>
									<Input
										type='text'
										name='goal'
										size='large'
										autoComplete='off'
										value={programProps.goal}
										onChange={(val) => onChange("goal", val)}
										bordered={false}
										className='textfiled w-80'
										placeholder='Focus/Goal'
									></Input>
									<span className='text-danger'>{errorProps.goal}</span>
								</div>
								<div className='col-sm-4'>
									<label>
										Sport<span className='text-danger'>*</span>
									</label>
									<Select
									getPopupContainer={trigger=> trigger.parentNode}
										className='textfiled w-80'
										mode='multiple'
										maxTagCount="responsive"
										// maxTagTextLength={11}
										key='sportID'
										name='sportID'
										style={{ width: "100%" }}
										size={200}
										showSearch
										allowClear
										showArrow
										onBlur={getUsers}
										bordered={false}
										placeholder='Select Sport'
										value={programProps.sportID}
										onChange={(val) => onChange("sportID", val)}
										optionFilterProp='children'
										filterSort={(optionA, optionB) => {
											return optionA.children
												.toLowerCase()
												.localeCompare(optionB.children.toLowerCase())
										}}
									>
										{sportsData.map((item, index) => (
											<Option key={index} value={item.OptionID}>
												{item.OptionData}
											</Option>
										))}
									</Select>
									<span className='text-danger'>{errorProps.sportID}</span>
								</div>
							</div>
							<div className='row col-sm-12'>
								<div className='col-sm-4'>
									<label>
										Athlete Type<span className='text-danger'>*</span>
									</label>
									<Select
									getPopupContainer={trigger=> trigger.parentNode}
										className='textfiled w-80'
										name='athleteType'
										bordered={false}
										showArrow
										showSearch
										allowClear
										maxTagCount="responsive"
										// maxTagTextLength={11}
										placeholder='Select Athlete Type'
										value={programProps.athleteType}
										onChange={(val) => onChange("athleteType", val)}
										filterSort={(optionA, optionB) =>
											optionA.toLowerCase().toLocaleCompare(optionB.toLowerCase())
										}
									>
										{athleteTypeData.map((item, index) => (
											<Option key={index} value={item.OptionID}>
												{item.OptionData}
											</Option>
										))}
									</Select>
									<span className='text-danger'>{errorProps.athleteType}</span>
								</div>
								<div className='col-sm-4'>
									<label>
										Start Date<span className='text-danger'>*</span>
									</label>
									<DatePicker
									getPopupContainer={trigger=> trigger.parentNode}
										style={{ cursor: "pointer" }}
										className='datepickerfiled add-prog-date add-program-datepicker'
										size='large'
										format='DD MMM YYYY'
										bordered={false}
										showToday={true}
										allowClear
										onBlur={getUsers}
										name='startDate'
										value={programProps.startDate}
										disabledDate={disabledStartDateSelection}
										onChange={(date, dateString) => assignDate("startDate", date, dateString)}

									/>
									<span className='text-danger'>{errorProps.startDate}</span>
								</div>
								<div className='col-sm-4'>
									<label>
										End Date<span className='text-danger'>*</span>
									</label>
									<DatePicker
									
									getPopupContainer={trigger=> trigger.parentNode}
										name='endDate'
										style={{ cursor: "pointer" }}
										className='add-host-date-field add-program-datepicker add-prog-date'
										size='large'
										format='DD MMM YYYY'
										bordered={false}
										showToday={true}
										allowClear
										value={programProps.endDate}
										disabled={disableEndDate}
										onBlur={getUsers}
										disabledDate={disabledDateEndDateSelection}
										onChange={(date, dateString) => assignDate("endDate", date, dateString)}

									/>
									<span className='text-danger'>{errorProps.endDate}</span>
								</div>
							</div>
						</div>
						<div className='row col-sm-12'>
							<div className='col-sm-4 search-athlete-box'>
								<label>Athlete's Search</label>
							</div>
						</div>
						<div className='row col-sm-12'>
							<div className='col-sm-4'>
								<label
									className='add-program-color'
									style={{ marginTop: "6px", fontWeight: "bold" }}
								>
									Search Available Athlete's
								</label>
							</div>
							<div className='col-sm-8 form-group'>
								<Input
									size='large'
									//addonAfter="Search"
									className='add-program-search-box ml-0'
									defaultValue=''
									name='searchKey'
									value={searchKey}
									onChange={searchAthlete}
									placeholder='Name/Email'
								/>
								<i class='fa fa-search add-program-search-icon add-program-color'></i>
							</div>
						</div>
						<div className='row col-sm-12 form-group'>
							<div className='col-sm-6 '>
								<div className='add-program-in-add-user'>
									<label className='add-program-color available-user-in-add-program-label'>
										<b>Available Athlete's</b>
									</label>
									<ul
										className='list-group available-user-program'
										style={{ height: "330px", overflow: "auto" }}
									>
										{availableUserData.length > 0 ? (
											availableUserData.map((item, index) => (
												<li
													title={item.UserName}
													//style={{ cursor: "pointer" }}
													className='list-group-item add-user-list-item'
													key={index}
													value={item.UserProfileID}
												>
													<div className='row '>
														<div className='col-sm-2'>
															<img
																className='add-users-with-team'
																src={item.ImageFileData ? item.ImageFileData : UserImage}
																alt={"user"}
															></img>{" "}
														</div>
														<div className='col-sm-8'>
															<span>
																<b>{handleStringEndsWithDotNotation(item.FullName)}</b>
															</span>
															<br />
															<span>{handleStringEndsWithDotNotation(item.UserName)}</span>
														</div>
														<div
															className='col-sm-2 d-flex justify-content-end pt-0'
															style={{
																color: "green",
																marginTop: "13px",
																cursor: "pointer",
															}}
															onClick={() => addUser(item.UserProfileID)}
														>
															<FontAwesomeIcon icon={faPlusSquare}></FontAwesomeIcon>
														</div>
													</div>
												</li>
											))
										) : (
											<Empty
											description={<span>No athlete's available</span>}
											/>
												/*<span className="add-program-plus-icon">+</span>*/
											//<i class="fa fa-plus add-program-plus-icon"></i>
										)}
									</ul>
								</div>
							</div>
							<div className='col-sm-6 add-program-available-user'>
								{/*<div className="col-sm-5 add-program-in-add-user add-program-available-user ml-1 pr-0">*/}
								<div className='add-program-in-add-user'>
									<label className='add-program-color available-user-in-add-program-label'>
										<b>Selected Athlete's</b>
									</label>
									<ul className='list-group' style={{ height: "330px", overflow: "auto" }}>
										{selectedUserData.length > 0 ? (
											selectedUserData.map((item, index) => (
												<li
													title={item.UserName}
													//style={{ cursor: "pointer" }}
													className='list-group-item add-user-list-item'
													key={index}
													value={item.UserProfileID}
												>
													<div
														//style={{ cursor: "pointer" }}
														className='row'
													//onClick={() => handleAddUsers(item.UserProfileID)}
													>
														<div className='col-sm-2'>
															<img
																className='add-users-with-team'
																src={item.ImageFileData ? item.ImageFileData : UserImage}
																alt={"user"}
															></img>{" "}
														</div>
														<div className='col-sm-8'>
															<span>
																<b>{handleStringEndsWithDotNotation(item.FullName)}</b>
															</span>
															<br />
															<span>{handleStringEndsWithDotNotation(item.UserName)}</span>
														</div>
														<div
															className='col-sm-2 d-flex justify-content-end pt-0'
															style={{
																color: "red",
																marginTop: "13px",
																cursor: "pointer",
															}}
															onClick={() => removeUser(item.UserProfileID)}
														>
															<FontAwesomeIcon icon={faTrashAlt}></FontAwesomeIcon>
														</div>
													</div>
												</li>
											))
										) : (
											<Empty
											description={<span>No athlete's selected</span>}
											/>
											//<i class="fa fa-plus add-program-plus-icon"></i>
										)}
									</ul>
								</div>
							</div>
						</div>
						<div className='row col-sm-12 d-flex justify-content-end mt-4'>
							{/* <Button className='btn_green mr-2' onClick={close}>
								Close
							</Button> */}
							{isAdd ? (
								<Button
									className='btn_green '
									onClick={save}
								//disabled={selectedUserData.length > 0 ? false : true}
								>
									Save
								</Button>
							) : (
								<Button
									className='btn_green '
									onClick={save}
								//disabled={selectedUserData.length > 0 ? false : true}
								>
									Update
								</Button>
							)}
						</div>
					</>
				</Modal.Body>
			</Modal>
		</>
	)
}

export default React.memo(AddProgram)
