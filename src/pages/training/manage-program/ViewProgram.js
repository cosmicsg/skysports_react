import { Button } from "antd"
import React, { useState } from "react"
import { Modal } from "react-bootstrap"

//View Program component
const ViewProgram = (props) => {
	const [viewProps] = useState(props.viewProps)
	const closeModel = () => {
		props.updateStatus()
	}
	return (
		<>
			<Modal show={true} backdrop='static' keyboard='false' size='md'>
				<Modal.Header style={{ height: "4rem" }}>
					<p>Manage Programme</p>
					<div className='col-sm-1'>
						<div className='d-flex justify-content-end'>
							<button title='Close' type='button' className='close' onClick={closeModel}>
								<span style={{ color: "#c29a37" }} area-hidden={true}>
									&times;
								</span>
							</button>
						</div>
					</div>
				</Modal.Header>
				<Modal.Body>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className='   font-weight-bold popup_txt'>Programme </label>
						</div>
						<div className='col-sm-1'></div>
						<div className='col-sm-7'>
							<label>{viewProps.ProgramName}</label>
						</div>
					</div>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className='font-weight-bold popup_txt'>Focus/Goal</label>
						</div>
						<div className='col-sm-1'></div>
						<div className='col-sm-7'>
							<label>{viewProps.FocusGoal}</label>
						</div>
					</div>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className='   font-weight-bold popup_txt'>Sport</label>
						</div>
						<div className='col-sm-1'></div>
						<div className='col-sm-7'>
							<label>{viewProps.SportName}</label>
						</div>
					</div>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className='   font-weight-bold popup_txt'>Start Date</label>
						</div>
						<div className='col-sm-1'></div>
						<div className='col-sm-7'>
							<label>{viewProps.StartDate}</label>
						</div>
					</div>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className='   font-weight-bold popup_txt'>End Date</label>
						</div>
						<div className='col-sm-1'></div>
						<div className='col-sm-7'>
							<label>{viewProps.EndDate}</label>
						</div>
					</div>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className='   font-weight-bold popup_txt'>Type</label>
						</div>
						<div className='col-sm-1'></div>
						<div className='col-sm-7'>
							<label>{viewProps.AthleteType}</label>
						</div>
					</div>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className='   font-weight-bold popup_txt'>Athlete Name</label>
						</div>
						<div className='col-sm-1'></div>
						<div className='col-sm-7'>
							<label>{viewProps.UserName}</label>
						</div>
					</div>
					{/* <div className= 'row col-sm-12 d-flex justify-content-end mt-4'>
					<Button 
					className='btn_green float-right btn btn-secondary mr-3 '
					onClick={closeModel}>
						Close
						</Button>
						</div> */}
				</Modal.Body>
			</Modal>
		</>
	)
}

export default ViewProgram
