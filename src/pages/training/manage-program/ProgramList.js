import { Button } from "react-bootstrap"
import React, { useState, useRef, useEffect } from "react"
import AddProgram from "./AddProgram"
import ProgramListFilter from "./ProgramListFilter"
import ReactTable from "react-table"
import Pagination from "../../../utils/Pagination"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEdit, faEye, faTrashAlt, faDownload } from "@fortawesome/free-solid-svg-icons"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import ExportData from "../../../utils/ExportData"
import ViewProgram from "./ViewProgram"
import DeleteProgram from "./DeleteProgram"
import { Permissions } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"
import { connect } from "react-redux"
import moment from "moment"

//Export label
const exportLabel = [
	{ columnName: "S.No", value: "SerialNo"},
	{ columnName: "Program Name", value: "ProgramName" },
	{ columnName: "Focus Goal", value: "FocusGoal" },
	{ columnName: "Sport Name", value: "SportName" },
	{ columnName: "Start Date", value: "StartDate" },
	{ columnName: "End Date", value: "EndDate" },
	{ columnName: "Athlete Type", value: "AthleteType" },
	{ columnName: "Users", value: "UserName" },
]

//Program List component
const ProgramList = (props) => {
	let permissionGroup = props.getAppPermissions()

	const [permissionArray] = useState(permissionGroup)
	const [isAddProgram, setAddProgram] = useState({
		showPopup: false,
		isAdd: true,
	})

	const [mainListData, setMainListData] = useState([])
	const tableRef = useRef(null)
	const filterRef = useRef(null)

	const [editProps, setEditProps] = useState({})
	const [paginationProps, setPaginationProps] = useState({
		totalRecords: 0,
		totalCount: 0,
		totalPages: 0,
		pageSize: 10,
	})
	//Export fields properties
	const [exportExcelProps, setExportExcelProps] = useState({
		isExportExcel: false,
		exportData: [],
	})

	const [filterProps, setFilterProps] = useState({
		program: [],
		sport: [],
		startDate: "",
		endDate: "",
	})

	const [isRefreshList, setRefreshList] = useState(false)

	const [viewProgramProps, setViewProgramProps] = useState({
		canViewProgram: false,
		programProps: {},
	})

	const [deleteProps, setDeleteProps] = useState({
		canDelete: false,
		programID: 0,
	})

	//AddProgram
	const addProgram = () => {
		setAddProgram({
			...isAddProgram,
			isAdd: true,
			showPopup: true,
		})
		setEditProps({})
	}

	//Update Close popup
	const updateClosePopup = (flag) => {
		setEditProps({})
		setAddProgram({
			...isAddProgram,
			showPopup: !isAddProgram.showPopup,
		})
		if (flag === true && filterRef.current) filterRef.current.loadProgramData()
	}

	//No records found
	const NoDataConst = (props) => <span className='table-nodata-style'>No records found </span>

	//Submit filter
	const submitFilter = (filter) => {
		setFilterProps({
			program: filter.program,
			sport: filter.sport,
			startDate: filter.startDate,
			endDate: filter.endDate,
		})
		setRefreshList(true)
	}

	//Refresh list when isRefreshList flag is true
	useEffect(() => {
		if (isRefreshList) refreshList()
	}, [isRefreshList])

	//Refresh list
	const refreshList = (pageNo, isExport) => {
		let sortByName = "CreatedDate"
		let orderBy = "DESC"
		if (pageNo && pageNo.sorted !== undefined && pageNo.sorted.length > 0) {
			sortByName = pageNo.sorted[0].id
			if (pageNo.sorted[0].desc === true) {
				orderBy = "DESC"
			} else {
				orderBy = "ASC"
			}
		}
		let strStartDate = ""
		if (filterProps.startDate !== "") {
			strStartDate = moment(filterProps.startDate).format("DD MMM YY")
		}
		let strEndDate = ""
		if (filterProps.endDate !== "") {
			strEndDate = moment(filterProps.endDate).format("DD MMM YY")
		}

		let filterModel = {
			
			ProgramIDs: filterProps.program ? filterProps.program.join(",") : "",
			SportIDs: filterProps.sport ? filterProps.sport.join("") : "",
			StartDate: strStartDate,
			EndDate: strEndDate,
			PageNo: pageNo > 0 ? pageNo : 1,
			PageSize:
				isExport !== undefined && isExport !== null && isExport === true
					? paginationProps.totalRecords
					: paginationProps.pageSize,
			SortColumn: sortByName,
			SortOrder: orderBy,
		}
		APIRequest.getPostService(APIUrl.GET_TRAINING_PROGRAM_LIST, filterModel).then((res) => {
			if (res.Data.length > 0 && isExport === true) {
				if (res.Succeeded)
					setExportExcelProps({
						...exportExcelProps,
						exportData: res.Data,
						isExportExcel: true,
					})
				setExportExcelProps({
					...exportExcelProps,
					isExportExcel: false,
				})
			} else {
				if (res.Succeeded) setMainListData(res.Data)
				if (res.Data[0]) {
					setPaginationProps({
						...paginationProps,
						totalCount: res.Data.length >= 1 ? res.Data.length : 0,
						totalPages: res.Data[0].TotalPages,
						totalRecords: res.Data[0].TotalRows,
					})
				} else {
					setPaginationProps({
						...paginationProps,
						totalPages: 0,
						totalRecords: 0,
						totalCount: 0,
					})
				}
				setRefreshList(false)
			}
		})
	}

	//Pagination call
	const paginationCall = (page) => {
		refreshList(page, false)
	}

	//Table column data limit
	const tableColumnDataLimit = (str) => {
		if (str !== null && str !== undefined)
			return str.length > 30 ? str.toString().substring(0, 27) + "..." : str
		else return ""
	}

	//sport column data limit
	const sportTableColumnDataLimit = (str) => {
		if (str !== null && str !== undefined)
			return str.length > 25 ? str.toString().substring(0, 25) + "..." : str
		else return ""
	}

	//Edit program action
	const editProgram = (row) => {
		setEditProps(row)
		//setAddProgram(true);
		setAddProgram({
			...isAddProgram,
			showPopup: !isAddProgram.showPopup,
			isAdd: false,
		})
	}

	//View program action
	const viewProgram = (row) => {
		updateViewProgramStatus(row, true)
	}

	//Update view program status
	const updateViewProgramStatus = (row, flag) => {
		setViewProgramProps({
			canViewProgram: flag === true ? flag : false,
			programProps: row,
		})
	}

	//Delete program action
	const deleteProgram = (row) => {
		UpdateDeleteStatus(row.ProgramID, true)
	}

	//Update delete program status
	const UpdateDeleteStatus = (id, flag) => {
		setDeleteProps({
			canDelete: flag === true ? true : false,
			programID: id ? id : 0,
		})
	}

	//Call Export data
	const handleExportData = () => {
		refreshList(0, true)
	}

	//Program column list for table
	const columns = [

		{
			Header: () => <div className='program-list-left'>S.No</div>,
			accessor: "SerialNo",
			sortable: false,
			resizable: false,
			className: "tableheader wordwrap",
			headerClassName: "BoldText ColoredText",
			Cell: (row) => {
				return <div className='tabledata program-list-left'>{row.original.SerialNo}</div>
			},
			width: Math.round(window.innerWidth * 0.04),
		},
		{
			Header: () => <div className='program-list-left'>Programme</div>,
			accessor: "ProgramName",
			resizable: false,
			Cell: (row) => {
				return (
					<div className='program-list-left tabledata wordwrap'>{tableColumnDataLimit(row.original.ProgramName)}</div>
				)
			},
		},
		{
			Header: () => <div className='program-list-left'>Focus/Goal</div>,
			accessor: "FocusGoal",
			resizable: false,
			Cell: (row) => {
				return (
					<div className='program-list-left tabledata wordwrap'>{tableColumnDataLimit(row.original.FocusGoal)}</div>
				)
			},
		},
		{
			Header: () => <div className='program-list-left'>Sport</div>,
			accessor: "SportName",
			resizable: false,
			Cell: (row) => {
				return (
					<div className='program-list-left tabledata wordwrap' title={row.original.SportName}>
						{sportTableColumnDataLimit(row.original.SportName)}
					</div>
				)
			},
			//width: Math.round(window.innerWidth * 0.10),
		},
		{
			Header: () => <div className='program-list-left'>Start Date</div>,
			accessor: "StartDate",
			resizable: false,
			width: Math.round(window.innerWidth * 0.10),
			Cell: (row) => {
				return (
					<div className='program-list-left tabledata'>{tableColumnDataLimit(row.original.StartDate)}</div>
				)
			},
		},
		{
			Header: () => <div className='program-list-left'>End Date</div>,
			accessor: "EndDate",
			resizable: false,
			width: Math.round(window.innerWidth * 0.10),
			Cell: (row) => {
				return <div className='program-list-left tabledata'>{tableColumnDataLimit(row.original.EndDate)}</div>
			},
		},
		{
			Header: () => <div className='program-list-left'>Type</div>,
			accessor: "AthleteType",
			resizable: false,
			Cell: (row) => {
				return (
					<div className='program-list-left tabledata'>{tableColumnDataLimit(row.original.AthleteType)}</div>
				)
			},
			width: Math.round(window.innerWidth * 0.06),
		},
		{
			Header: () => <div className='program-list-center'>Action</div>,
			accessor: "",
			resizable: false,
			sortable: false,
			Cell: (row) => {
				return (
					<>
						<div className='tabledata text-center'>
							{permissionArray.includes(Permissions.TRAINING_PROGRAM_EDIT) &&
								props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									title='Edit'
									className='mr-2 icon_edit'
									icon={faEdit}
									style={{ cursor: "pointer" }}
									onClick={() => editProgram(row.original)}
								/>
							) : null}
							{props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									title='Add Users'
									className='mr-2 icon_eye'
									onClick={() => viewProgram(row.original)}
									icon={faEye}
									style={{ cursor: "pointer" }}
								/>
							) : null}
							{permissionArray.includes(Permissions.TRAINING_PROGRAM_DELETE) &&
								props.user.InvitedUserID === row.original.CreatedByID ? (
								<>
									{!row.original.CanDelete ? (
										<FontAwesomeIcon
											title='Delete'
											className='mr-2 icon_Delete'
											icon={faTrashAlt}
											style={{ cursor: "pointer" }}
											onClick={() => deleteProgram(row.original)}
										/>
									) : null}
								</>
							) : null}
						</div>
					</>
				)
			},
			width: Math.round(window.innerWidth * 0.07),
		},
	]
	//Return list
	return (
		<>
			<div>
				<div className='games_layout form-group row col-sm-12 mt-3 pr-0'>
					<div className='col-sm-6'>Manage Programme</div>
					{permissionArray.includes(Permissions.TRAINING_PROGRAM_CREATE) ? (
						<div className='col-sm-6 d-flex justify-content-end pr-0'>
							<Button type='button' className='btn_green' onClick={addProgram}>
								New Programme
							</Button>
						</div>
					) : null}
				</div>
				<div className='program-filter'>
					<ProgramListFilter ref={filterRef} refreshList={submitFilter} />
				</div>
				<div className='program-filter'>
					<ReactTable
						className='CosmicTable'
						data={mainListData}
						columns={columns}
						showPaginationTop={false}
						showPaginationBottom={false}
						minRows={0}
						defaultPageSize={10}
						onFetchData={refreshList}
						ref={tableRef}
						sortable={true}
						multiSort={true}
						manual
						showPageSizeOptions={false}
						NoDataComponent={NoDataConst}
						defaultSorting={[
							{
								id: "id",
								desc: true,
							},
						]}
						manualPagination={true}
					/>
				</div>
				<div className='row col-sm-12 mt-2 pr-0'>
					<div className='col-sm-6 mt-1'>
						<label>
							<b>Total Records: </b>
							{paginationProps.totalRecords > 10
								? paginationProps.totalCount + " of " + paginationProps.totalRecords
								: paginationProps.totalRecords !== 0
									? paginationProps.totalRecords
									: 0}
						</label>
					</div>

				</div>
				{permissionArray.includes(Permissions.TRAINING_PROGRAM_EXPORT) ? (
					<>
						<div className='row col-sm-12'>
							<div className='col-sm-6'>
								<Button className='btn_green mr-2' onClick={handleExportData}>
									<FontAwesomeIcon title='Export Data' color={"#FFFFFF"} icon={faDownload}>
										{" "}
									</FontAwesomeIcon>{" "}
									Export Excel
								</Button>
							</div>
							{paginationProps.totalPages > 0 ? (
								<div className='col-sm-6 d-flex justify-content-end pr-0'>
									<Pagination
										totalPages={paginationProps.totalPages}
										totalRecords={paginationProps.totalRecords}
										paginationCall={paginationCall}
									/>
								</div>
							) : null}
						</div>
						{exportExcelProps.isExportExcel ? (
							<ExportData
								data={exportExcelProps.exportData}
								label={exportLabel}
								filename='Training Program List'
							/>
						) : null}
					</>
				) : null}
			</div>

			{isAddProgram.showPopup ? (
				<AddProgram
					isAdd={isAddProgram.isAdd}
					editProps={editProps}
					updateClosePopup={updateClosePopup}
					refreshList={refreshList}
				/>
			) : null}

			{/* View Training Program Details */}
			{viewProgramProps.canViewProgram ? (
				<ViewProgram
					viewProps={viewProgramProps.programProps}
					updateStatus={updateViewProgramStatus}
				/>
			) : null}

			{deleteProps.canDelete ? (
				<DeleteProgram
					closePopup={UpdateDeleteStatus}
					refreshList={refreshList}
					programID={deleteProps.programID}
				/>
			) : null}
		</>
	)
}

const mapPropsToState = (state) => ({
	user: state.userReducer,
})

export default PermissionProvider(connect(mapPropsToState, null)(React.memo(ProgramList)))
