import { Card, Collapse, Button } from "react-bootstrap";
import React, { useState, useEffect, useImperativeHandle } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowUp, faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { DatePicker, Select } from "antd";
import moment from "moment";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";

const { Option } = Select;

//ProgramFilter component
const ProgramListFilter = React.forwardRef((props, ref) => {
  const [filterProps, setFilterProps] = useState({
    program: [],
    sport: [],
    startDate: "",
    endDate: "",
  });

  const [disableSearch, setDisableSearch] = useState(true);

  const [programsData, setProgramsData] = useState([]);
  const [sportsData, setSportsData] = useState([]);

  //Filter expand properties
  const [filterExpanded, setFilterExpanded] = useState({
    IsExpanded: false,
    Icons: false,
  });

  useImperativeHandle(ref, () => ({
    loadProgramData,
  }));

  //Lifecycle method, here it is loading the sport and program data loading for filter fields.
  useEffect(() => {
    loadProgramData();
    APIRequest.getGetService(APIUrl.GET_SPORT_LIST).then((res) => {
      setSportsData(res.Data);
    });
  }, []);

  //here loading the program data
  const loadProgramData = () => {
    APIRequest.getGetService(APIUrl.GET_TRAINING_PROGRAM_DROPDOWN).then(
      (res) => {
        setProgramsData(res.Data);
      }
    );
  };

  //Expand or collapse filter panel
  const expandFilter = (data) => {
    let Icons = filterExpanded.Icons === true ? false : true;
    setFilterExpanded({
      IsExpanded: data,
      Icons: Icons,
    });
  };

  //Onchange event
  const onChange = (name, val) => {
    setFilterProps({
      ...filterProps,
      [name]: val,
    });
    disableBtnSearch(name, val);
  };

  //Assign date
  const assignDate = (name, val) => {
    let temp = val ? moment(new Date(val)) : "";
    setFilterProps({
      ...filterProps,
      [name]: temp,
    });
    disableBtnSearch(name, val);
  };

  //Disable search button, which is enable only when the filter fields having the values
  const disableBtnSearch = (name, val) => {
    if (!val) {
      if (
        (name === "program" &&
          ((filterProps.sport && filterProps.sport.length !== 0) ||
            filterProps.startDate ||
            filterProps.endDate)) ||
        (name === "sport" &&
          ((filterProps.program && filterProps.sport.length !== 0) ||
            filterProps.startDate ||
            filterProps.endDate)) ||
        (name === "startDate" &&
          ((filterProps.program && filterProps.program.length !== 0) ||
            (filterProps.sport && filterProps.sport.length !== 0) ||
            filterProps.endDate)) ||
        (name === "endDate" &&
          ((filterProps.program && filterProps.program.length !== 0) ||
            (filterProps.sport && filterProps.sport.length !== 0) ||
            filterProps.startDate))
      ) {
        setDisableSearch(false);
      } else setDisableSearch(true);
    } else setDisableSearch(false);
  };

  //Submit Filter
  const submitFilter = () => {
    callParentToRefreshList(filterProps);
  };

  //Call ProgramList component to refresh list
  const callParentToRefreshList = (filterProps) => {
    props.refreshList(filterProps);
  };

  //Clear filter
  const clearFilter = () => {
    let temp = {
      program: [],
      sport: [],
      startDate: "",
      endDate: "",
    };
    setFilterProps(temp);
    setDisableSearch(true);
    callParentToRefreshList(temp);
  };

  //Return filter fields
  return (
    <>
      <Card className="w-100 card form-group">
        <Card.Header
          style={{ cursor: "pointer" }}
          className="text-white pl-1"
          onClick={() => expandFilter(!filterExpanded.IsExpanded)}
        >
          <div className="float-left filter-heading-font">Search Manage Programme</div>
          <div className="float-right">
            {filterExpanded.IsExpanded ? (
              <FontAwesomeIcon
                color="#FFF"
                className="action-icon-font"
                icon={faArrowUp}
              />
            ) : (
              <FontAwesomeIcon
                color="#FFF"
                className="action-icon-font"
                icon={faArrowDown}
              />
            )}
          </div>
        </Card.Header>
        <Collapse in={filterExpanded.IsExpanded}>
          <Card.Body>
            <div className="row col-sm-12 form-group">
              <div className="col-sm-3">
                <label>Programme</label>
                <Select
                  className="textfiled w-80"
                  bordered={false}
                  mode="multiple"
                  showSearch
                  allowClear
                  showArrow
                  maxTagCount="responsive"
                  // maxTagTextLength={11}
                  placeholder="Select Programme"
                  name="program"
                  value={filterProps.program}
                  onChange={(val) => onChange("program", val)}
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) => {
                    return optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase());
                  }}
                >
                  {programsData.map((item, index) => (
                    <Option key={index} value={item.OptionID}>
                      {item.OptionData}
                    </Option>
                  ))}
                </Select>
              </div>
              <div className="col-sm-3">
                <label>Sport</label>
                <Select
                  mode={"multiple"}
                  key="sport"
                  className="textfiled w-80"
                  name="sport"
                  size={200}
                  showSearch
                  allowClear
                  showArrow
                  maxTagCount="responsive"
                  // maxTagTextLength={11}
                  bordered={false}
                  placeholder="Select Sport"
                  value={filterProps.sport}
                  onChange={(val) => onChange("sport", val)}
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) => {
                    return optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase());
                  }}
                >
                  {sportsData.map((item, index) => (
                    <Option key={index} value={item.OptionID}>
                      {item.OptionData}
                    </Option>
                  ))}
                </Select>
              </div>
              <div className="col-sm-3">
                <label>Start Date</label>
                <div>
                  <DatePicker
                    name="startDate"
                    className="add-host-date-field program-datepicker pr-0 add-prog-date"
                    format="DD MMM YYYY"
                    size="large"
                    bordered={false}
                    allowClear={true} 
                    value={filterProps.startDate}
                    onChange={(date, dateString) =>
                      assignDate("startDate", dateString)
                    }
                  />
                </div>
              </div>
              <div className="col-sm-3">
                <label>End Date</label>
                <DatePicker
                  name="endDate"
                  className="add-host-date-field program-datepicker pr-0 add-prog-date"
                  format="DD MMM YYYY"
                  size="large"
                  bordered={false}
                  allowClear={true}
                  value={filterProps.endDate}
                  onChange={(date, dateString) =>
                    assignDate("endDate", dateString)
                  }
                />
              </div>
            </div>
            <div className="row col-sm-12 form-group">
              <div className="col-sm-6"></div>
              <div className="col-sm-6 d-flex justify-content-end">
              <Button className="btn_green mr-2" onClick={clearFilter}>
                  Clear
                </Button>
                <Button
                  className="btn_green"
                  disabled={disableSearch}
                  onClick={submitFilter}
                >
                  Search
                </Button>
                
              </div>
            </div>
          </Card.Body>
        </Collapse>
      </Card>
    </>
  );
});
export default React.memo(ProgramListFilter);
