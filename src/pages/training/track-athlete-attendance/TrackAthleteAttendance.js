import React, { Component } from "react"
import { Container, Row, Col } from "react-bootstrap"
import ReactTable from "react-table"
import "../../../styles/CosmicTable.css" //"../../../styles/CosmicTable.css"
import "antd/dist/antd.css"
import "../../../styles/Common.css"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { DatePicker, Spin } from "antd"
import moment from "moment"
import { Permissions as Permission } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"

const NoDataConst = (props) => (
	<span className='table-nodata-style'>
		No records found Please check your connection or change your filters{" "}
	</span>
)

//It is manage user all tab controll
class TrackAthleteAttendance extends Component {
	constructor(props) {
		super(props)

		let permissions = this.props.getAppPermissions();
		let roles = [];

		this.state = {
			loading: false,
			TotalPages: 0,
			TotalRecords: 0,
			SortColumn: "",
			SortValue: "",
			permissions: permissions,
			roles: roles,
			AthleteAttendanceList: [],
			ScheduleTime: [],
			AthleteName: "",
			Time: "",
			errors: [],
			SelectedDate: "",
		}
		this.table = React.createRef()
		this.fetch = this.fetch.bind(this)
		this.tableColumnDataLimit = this.tableColumnDataLimit.bind(this)
		this.handleOnchange = this.handleOnchange.bind(this)
	}

	handleOnchange = (name, val) => {
		let isErrorExist = false
		let errors = []
		if (val === "" && val === undefined && val === null && val.length === 0) {
			errors["SelectAttendanceDate"] = "Please select the Date"
			this.setState({ ...this.state, errors: errors })
			isErrorExist = true
		}

		if (!isErrorExist) {
			//update value based on onchange event for particular fields
			this.setState(
				{
					...this.state,
					[name]: val,
				},
				function () {
					this.fetch(this.state)
				}
			)
		}
	}

	tableColumnDataLimit = (str) => {
		if (str !== undefined && str !== null) {
			return str.length > 30 ? str.substring(0, 27) + "..." : str
		}
	}
	NoDataConst = (props) => <span className='table-nodata-style'>No records found </span>

	// It is used to over all loader start
	handlerLoadingStart = () => {
		this.setState({ loading: true })
	}
	// It is used to over all loader stop
	handlerLoadingStop = () => {
		this.setState({ loading: false })
	}

	//Fetch data from React Table
	async fetch(state) {
		let self = this
		let FilterDate = self.state.SelectedDate
		let FilterDefaultDate = self.state.SelectedDate
		if (FilterDate !== "" && FilterDate !== null) {
			FilterDate = FilterDate.toISOString().slice(0, 10)
		} else {
			FilterDefaultDate = new Date()
			FilterDate = new Date().toISOString().slice(0, 10)
		}
		self.setState({ loading: true, SelectedDate: moment(FilterDefaultDate) })
		APIRequest.getGetService(APIUrl.GET_TRAINING_TRACK_ATHLETE_ATTENDANCE_LIST + "/" + FilterDate)
			.then((response) => {
				if (response.Data.length > 0) {
					self.setState({
						loading: false,
						AthleteAttendanceList: response.Data,
					})
				} else {
					self.setState({
						loading: false,
					})
				}
				self.setState({ loading: false, AthleteAttendanceList: response.Data })
			})
			.catch((error) => {
				self.setState({ loading: false })
			})
	}

	async componentDidMount() {
		this.fetch(this.state)
	}
	render() {
		let permissions = this.state.permissions
		const columns = [
			{
				Header: () => <div>Athlete Name</div>,
				resizable: false,
				accessor: "AthleteName",
				style: { overflowWrap: "break-word" },
				width: Math.round(window.innerWidth * 0.2),
				Cell: (row) => {
					return (
						<div
							className='tabledata wordwrap'
							style={{ textAlign: "center" }}
							title={row.original.FullName}
						>
							{this.tableColumnDataLimit(row.original.FullName)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "center" }}>Time</div>,
				resizable: false,
				accessor: "ScheduleTime",
				style: { overflowWrap: "break-word" },
				width: Math.round(window.innerWidth * 0.3),
				Cell: (row) => {
					return (
						<>
							{row.original.WorkoutTimelst.map((item) => (
								<div className='workoutTime'>
									<span title={item.WorkoutTime}>
										{this.tableColumnDataLimit(item.WorkoutTime)}
									</span>
								</div>
							))}
						</>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "center" }}>TotalHours</div>,
				resizable: false,
				accessor: "Time",
				style: { overflowWrap: "break-word" },
				width: Math.round(window.innerWidth * 0.3),
				Cell: (row) => {
					return (
						<>
							{row.original.WorkoutTimelst.map((item) =>
								item.Time !== null ? (
									<div className='workoutCheckedIn'>
										<span title={item.Time}>{this.tableColumnDataLimit(item.Time)}</span>
									</div>
								) : (
									<div className='workoutCheckedOut'>
										<span>--</span>
									</div>
								)
							)}
						</>
					)
				},
			},
		]
		return (
			<>
				{permissions.includes(Permission.T_A_A_VIEW) ? (
					<Container fluid className='h-full'>
						<Row className='h-full'>
							<Col className='h-full'>
								<div className=' pt-10 ' style={{ margin: "10px" }}>
									<Spin size='large' spinning={this.state.loading} tip='Loading...'>
										<div className='row'>
											<div className='d-flex justify-content-start pt-1 col-md-9'>
												<p className='tab_form_content games_layout'>Track Athlete Attendance</p>
											</div>
											<div className='d-flex justify-content-end col-md-3'>
												<DatePicker
													size='large'
													format='DD MMM YYYY'
													disabled={false}
													bordered={false}
													allowClear={false}
													name='StartDate'
													selected={this.state.SelectedDate}
													value={this.state.SelectedDate}
													onChange={(val) => this.handleOnchange("SelectedDate", val)}
													defaultValue={moment()}
													className='myDatePicker datepickerfiled '
													placeholder='Select Date'
												/>
												<span className='val-err-msg'>
													{this.state.errors.SelectAttendanceDate}
												</span>
											</div>
										</div>
										<br></br>
										<div className=' pt-10'>
											<div className='d-flex justify-content-start pt-1'></div>
											<div className='admin-report-table'>
												<ReactTable
													data={this.state.AthleteAttendanceList}
													columns={columns}
													showPaginationTop={false}
													showPaginationBottom={false}
													minRows={0}
													defaultPageSize={10}
													defaultPage={1}
													onFetchData={this.fetch}
													ref={this.table}
													sortable={true}
													multiSort={true}
													manual
													className={"WorkoutAttendance"}
													showPageSizeOptions={false}
													NoDataComponent={NoDataConst}
													defaultSorting={[
														{
															id: "id",
															desc: true,
														},
													]}
													manualPagination={true}
												/>
											</div>
										</div>
									</Spin>
								</div>
							</Col>
						</Row>
					</Container>
				) : null}
			</>
		)
	}
}

export default PermissionProvider(TrackAthleteAttendance)
