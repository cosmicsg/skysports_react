

import { Chart } from "react-google-charts";
import { Col } from "react-bootstrap"
import { Select } from "antd"
import "../../..//styles/grid.css"
import { CaretDownOutlined } from "@ant-design/icons"
import React, { useEffect, useState } from "react";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import PermissionProvider from "../../../permissions/PermissionProvider";
import { Permissions as Permission } from "../../../permissions/AppPermissions";

const TrackAthletePerformance = (props) => {


    let permissions = props.getAppPermissions();

    const [inputData, setInputtData] = useState({
        displayHeader_Date: "",
        toDate: "",
        fromDate: "",
        traningPerformance: [],
        weekData: [],
        weekNumber: [],
        permissions: permissions,
        seriesList: [],
        programList: [],
        programID: [],
        scheduleList: [],
        scheduleID: [],
        userListData: [],
        userList: [],
        searchBTN: true
    });


    /// OnLoad function
    useEffect(() => {
        APIRequest.getGetService(APIUrl.GET_TRAINING_PROGRAMUSER_DROPDOWN_LIST).then((res) => {

            setInputtData((prevState) => ({
                ...prevState,
                programList: res.Data
            }))
        })
    }, [])
    // On search function
    const onSearchChartData = () => {

        /// Dashbord AthleteResult Data get API
        APIRequest.getGetService(APIUrl.GET_TRAINING_ATHLETE_PERFORMANCE_DATA + "/" + inputData.fromDate + "/" + inputData.toDate + "/" + (inputData.userListData.toString() == "" ? "0" : inputData.userListData.toString())
        ).then((res) => {
            let dataArray = [];
            let seriesArray = [];

            if (res.Succeeded) {

                if (res.Data.length > 0) {
                    let count = 0;
                    res.Data.map((x) => {

                        let names = x.ProfileName.split(",,");

                        if (count == 0) {
                            // dataArray.push(['x', x.ProfileName])

                            names.push("x")
                            dataArray.push(names.reverse())
                        }
                        count = 1;

                        var array = x.Data.match(/\d+(?:\.\d+)?/g).map(Number)

                        if (x.Data == null) {
                            x.Data = 0;
                        }
                        array.push(x.Date)
                        seriesArray.push({ visibleInLegend: true, pointShape: 'circle', pointSize: 10 })
                        dataArray.push(array.reverse())
                    })
                }

                setInputtData((prevState) => ({
                    ...prevState,
                    traningPerformance: dataArray, seriesList: seriesArray
                }))
            }
        })
    }

    // Week change function
    const onchangeWeek = (week, key) => {


        let data = inputData.weekData.filter((x) => x.WeekNumber == week);

        if (data.length > 0) {
            setInputtData((prev) => ({ ...prev, weekNumber: week, fromDate: data[0].StartDate, toDate: data[0].EndDate }))
        }
        else {
            setInputtData((prev) => ({ ...prev, weekNumber: week, }))
        }


    }
    const onOverAllchange = (Data, name) => {

        setInputtData((prev) => ({ ...prev, [name]: Data }))
    }
    // Schedule List Get
    useEffect(() => {

        APIRequest.getGetService(APIUrl.GET_PROGRAM_SCHEDULE_DROPDOWN_LIST + "/" + inputData.programID).then((res) => {

            setInputtData((prevState) => ({
                ...prevState,
                scheduleList: res.Data, scheduleID: []
            }))
        })
    }, [inputData.programID])

    // Week List Get
    useEffect(() => {

        APIRequest.getGetService(APIUrl.GET_TRAINING_WEEK_SCHEDULE_DATA + "/" + inputData.scheduleID).then((res) => {

            setInputtData((prevState) => ({
                ...prevState,
                weekData: res.Data, weekNumber: []
            }))
        })
    }, [inputData.userListData])
    // Athlete List Get
    useEffect(() => {

        APIRequest.getGetService(APIUrl.GET_TRAINING_SCHEDULE_USER_DATA + "/" + inputData.scheduleID).then((res) => {

            setInputtData((prevState) => ({
                ...prevState,
                userList: res.Data, userListData: []
            }))
        })
    }, [inputData.scheduleID])

    // Search btn enable disable function
    useEffect(() => {

        if (inputData.programID.toString() != "" && inputData.scheduleID.toString() != ""
            && inputData.userListData.toString() != "" && inputData.weekNumber.toString() != "") {
            setInputtData((prev) => ({ ...prev, searchBTN: false }))
        }
        else {
            setInputtData((prev) => ({ ...prev, searchBTN: true }))
        }
    }, [inputData.programID, inputData.userListData, inputData.scheduleID, inputData.weekNumber])


    return (
        <>
            {inputData.permissions.includes(Permission.TRACK_ATHLETE_PERFORMANCE_VIEW) ?
                <>
                    <h2 className="ml-3 font-weight-bold pt-3 text-secondary">Track Athlete Performance </h2>
                    <Col sm={12} lg={12} md={12} xl={12}>
                        <div className='training-performance-box'>
                            <div className="form-group pt-3">
                                <div className="row text-center form-group">
                                    <div className=" col-sm-12 col-xs-12 col-md-6 col-lg-3 col-xl-3 ">
                                        <div className="selectdiv form-group">
                                            <Select size="lg"
                                                placeholder="Select Programme"
                                                maxTagCount={1}
                                                //arrowIcon={<SomeIcon />}
                                                //loadingIcon={<SomeIcon />}
                                                //searchIcon={<SomeIcon />}
                                                suffixIcon={<CaretDownOutlined style={{ fontSize: '18px', color: '#297b86', marginTop: "-3px" }} />}
                                                onChange={(val, key) => onOverAllchange(val, "programID")}
                                                className=" color-textfiled text-center  font-weight-bold" value={inputData.programID}>
                                                {inputData.programList.map((c) => (
                                                    <option value={c.OptionID}>{c.OptionData}</option>
                                                ))}
                                            </Select>
                                        </div>
                                    </div>
                                    <div className=" col-sm-12 col-xs-12 col-md-6 col-lg-3 col-xl-3 ">
                                        <div className="selectdiv form-group">
                                            <Select size="lg"
                                                placeholder="Select Schedules"
                                                suffixIcon={<CaretDownOutlined style={{ fontSize: '18px', color: '#297b86', marginTop: "-3px" }} />}

                                                onChange={(val, key) => onOverAllchange(val, "scheduleID")}
                                                className="color-textfiled text-center  font-weight-bold" value={inputData.scheduleID}>

                                                {inputData.scheduleList.map((c) => (
                                                    <option value={c.OptionID}>{c.OptionData}</option>
                                                ))}
                                            </Select>
                                        </div>
                                    </div>
                                    <div className=" col-sm-12 col-xs-12 col-md-6 col-lg-3 col-xl-3 ">
                                        <div className="selectdiv form-group">
                                            <Select mode={"multiple"} size="lg"
                                                maxTagCount={1}
                                                placeholder="Select Athlete"
                                                showArrow='true'
                                                suffixIcon={<CaretDownOutlined style={{ fontSize: '18px', color: '#297b86', marginTop: "-3px" }} />}
                                                onChange={(val, key) => onOverAllchange(val, "userListData")}
                                                className="color-textfiled text-center  font-weight-bold" value={inputData.userListData}>

                                                {inputData.userList.map((c) => (
                                                    <option value={c.UserID}>{c.FullName}</option>
                                                ))}
                                            </Select>
                                        </div>
                                    </div>
                                    <div className=" col-sm-12 col-xs-12 col-md-6 col-lg-3 col-xl-3 ">
                                        <div className="selectdiv form-group">
                                            <Select size="lg" onChange={(val, key) => onchangeWeek(val, key)}
                                                placeholder="Select Week"
                                                suffixIcon={<CaretDownOutlined style={{ fontSize: '18px', color: '#297b86', marginTop: "-3px" }} />}
                                                className="color-textfiled text-center  font-weight-bold" value={inputData.weekNumber}>

                                                {inputData.weekData.map((c) => (
                                                    <option value={c.WeekNumber}>{"Week" + c.WeekNumber}</option>
                                                ))}
                                            </Select>
                                        </div>
                                    </div>

                                    <div className=" col-sm-12 col-xs-12 col-md-6 col-lg-9 col-xl-9"></div>
                                    <div className=" col-sm-12 col-xs-12 col-md-6 col-lg-3 col-xl-3">
                                        <button disabled={inputData.searchBTN} onClick={onSearchChartData} className="btn_lg_green btn">Search</button>
                                    </div>
                                </div>

                            </div>
                            {inputData.traningPerformance != 0 ? (
                                <Chart
                                    width={'100%'}
                                    height={'400px'}
                                    chartType="LineChart"
                                    loader={<div>Loading Chart</div>}
                                    borderRadius="10"
                                    data={inputData.traningPerformance}
                                    options={{
                                        colors: ['#e360f5', '#a5f7ff', '#615de4', '#73eb5f', '#c381b8', '#c0ebfe', '#fcd01b', '#e7474e', '#18219E', '#3B9E18', '#18999E', '#0094C0', '#FD4952', '#90B3C7'],
                                        legend: { position: "top" },

                                        hAxis: {
                                            title: 'Dates',
                                            minValue: 0,
                                            format: 'hh:mm',
                                            minorGridlines: {
                                                count: 0
                                            },
                                            minValue: 0,
                                            viewWindow: {
                                                min: 0
                                            },
                                            viewWindowMode: "explicit",
                                            gridlines: {
                                                color: 'gray',
                                                count: 0,

                                            }, textStyle: {

                                                color: 'gray',
                                                textalgin: "left",
                                                borderRadius: 10,
                                            },
                                        },

                                        vAxis: {
                                            minorGridlines: {
                                                count: 0
                                            },
                                            format: 'Hrs ',
                                            title: 'Hours',
                                            gridlines: {
                                                color: 'green',
                                                count: 0
                                            },
                                            minValue: 0,

                                            textStyle: {
                                                color: 'gray',
                                                fontWeight: "bold",
                                                textalgin: "left",
                                                borderRadius: 10,
                                                backgroundColor: '#b7b1b121',
                                            },

                                        },

                                        chartArea: {
                                            // "numberPrefix": "$",
                                            backgroundColor: {
                                                gradient: {
                                                    // Start color for gradient.
                                                    color1: '#d79db8',
                                                    // Finish color for gradient.
                                                    color2: 'white',
                                                    // Where on the boundary to start and
                                                    // end the color1/color2 gradient,
                                                    // relative to the upper left corner
                                                    // of the boundary.
                                                    x2: '5%', y2: '5%',
                                                    x1: '0%', y1: '100%',

                                                    // If true, the boundary for x1,
                                                    // y1, x2, and y2 is the box. If
                                                    // false, it's the entire chart.
                                                    useObjectBoundingBoxUnits: true
                                                },
                                            },
                                        },

                                        series: inputData.seriesList



                                    }}
                                    rootProps={{ 'data-testid': '1' }}
                                />
                            ) : null}
                        </div>
                    </Col>
                </> : null}
        </>

    )



}

export default PermissionProvider(TrackAthletePerformance);