import React from "react"
import ReactTable from "react-table"
import "../../../styles/CosmicTable.css"
import { Button, Modal } from "react-bootstrap"
import "../../../App"
import { Select, DatePicker, Input, Space } from "antd"
import "antd/dist/antd.css"
import moment from "moment"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlusCircle, faTrashAlt } from "@fortawesome/free-solid-svg-icons"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest, showNotification } from "../../../components/api-manager/apiRequest"
import { Message } from "../../../message/messageConstant"
import { Permissions as Permission } from "../../../permissions/AppPermissions";
import { connect } from "react-redux";

const { Option } = Select

const handleDelete = async (id, self) => {
	var romovedata = self.state.fields.filter((x) => x.id !== id)
	self.setState(
		{
			fields: romovedata,
		},
		function () {
			let rowId = self.state.selected
			let data = rowId[id]
			if (data !== undefined) {
				delete rowId[id]
			}
			if (Object.keys(self.state.selected).length === 0) {
				self.setState({ btnDelete: true, btnSend: true })
			}
			if (self.state.fields.length === 0) {
				self.setState({
					rowcount: 0,
					selected: [],
					btnDelete: true,
					btnDeleteAll: true,
					btnSend: true,
					btnSendall: true,
					addUsertxt: "Add User",
				})
			}
		}
	)
}
const handleBtnSendALL = (self) => {
	let selectdata = self.state.fields
	var InputData = []
	var obj = {}
	let result = 0
	if (self.state.emailExiest === 0) {
		for (var i = 0; i < selectdata.length; i++) {
			let data = self.state.fields[i]

			let role = data.role
			let roledata = role.toString()
			let email = document.getElementById("Email_tbl" + i)
			let fullname = document.getElementById("Fullname_tbl" + i)
			let Roleid = document.getElementById("Role_tbl" + i)
			let country = document.getElementById("Country_tbl" + i)
			let council = document.getElementById("council_tbl" + i)
			let association = document.getElementById("association_tbl" + i)
			let club = document.getElementById("club_tbl" + i)
			let department = document.getElementById("Department_tbl" + i)
			let date = document.getElementById("Date_tbl" + i)
			let sport = document.getElementById("sport_tbl" + i)
			if (data.fullname === undefined || data.fullname === "") {
				if (fullname !== null) {
					result = 1
					fullname.classList.add("mandatory_text")
				}
			}
			if (data.email === undefined || data.email === "") {
				if (email !== null) {
					result = 1
					email.classList.add("mandatory_text")
				}
			}
			if (data.role === undefined || roledata === "") {
				if (Roleid != null) {
					result = 1
					Roleid.classList.add("mandatory_text")
				}
			}
			if (
				(data.country === undefined || data.country.toString() === "") &&
				self.state.isCountryCloumn === true
			) {
				if (country !== null) {
					result = 1
					country.classList.add("mandatory_text")
				}
			}
			if (
				(data.council === undefined || data.council.toString() === "") &&
				self.state.iscouncilCloumn === true
			) {
				if (council !== null) {
					result = 1
					council.classList.add("mandatory_text")
				}
			}
			if (
				(data.association === undefined || data.association.toString() === "") &&
				self.state.isAssociationColumn === true
			) {
				if (association !== null) {
					result = 1
					association.classList.add("mandatory_text")
				}
			}
			if (
				(data.club === undefined || data.club.toString() === "") &&
				self.state.isClubColumn === true
			) {
				if (club !== null) {
					result = 1
					club.classList.add("mandatory_text")
				}
			}
			if (
				(data.sport === undefined || data.sport.toString() === "") &&
				self.state.isSportcloumn === true
			) {
				if (sport != null) {
					result = 1
					sport.classList.add("mandatory_text")
				}
			}
			if (
				(data.department === undefined || data.department.toString() === "") &&
				self.state.isDepartmentCloumn === true
			) {
				if (department != null) {
					result = 1
					//department.style.borderBottomColor = "red"
					department.classList.add("mandatory_text")
				}
			}
			if (data.date === undefined || data.date === "") {
				if (date != null) {
					result = 1
					date.classList.add("mandatory_text")
				}
			}
			if (result === 1) {
			} else {
				let expiryDate = handlerFormatDate(data.date)
				obj["ExpiryDate"] = expiryDate
				obj["DepartmentID"] = data.department.toString()
				obj["InvitedByID"] = "129"
				obj["FullName"] = data.fullname
				obj["Email"] = data.email
				obj["CountryID"] = data.country.toString()
				obj["RoleID"] = roledata
				obj["SportID"] = data.sport.toString() === "" ? "0" : data.sport.toString()
				obj["CouncilIDs"] = data.council.toString()
				obj["CouncilID"] = data.council.toString() === "" ? "0" : data.council.toString()
				obj["AssociationID"] =
					data.association.toString() === "" ? "0" : data.association.toString()
				obj["ClubID"] = data.club.toString() === "" ? "0" : data.club.toString()
				obj["Association"] = self.state.isAssociationUsertab
				obj["Committee"] = self.state.isCommitteetab
				obj["Council"] = self.state.isCounciltab
				obj["Club"] = self.state.isClubUsertab
				obj["Athlete"] = self.state.isAthleteUsertab
				obj["Coach"] = self.state.isCoachUsertab
				InputData.push(obj)
				obj = {}
			}
		}
		if (result === 0) {
			self.props.isLoadingStartFunc()
			APIRequest.getPostService(APIUrl.ADD_INVITE_USERS, InputData)
				.then((response) => {
					if (response.Succeeded === true) {
						showNotification("Saved Successfully, Email notification sent success", "success")
						self.setState({ fields: [], rowcount: 0, selected: [],btnDelete: true,
							btnDeleteAll: true,
							btnSend: true,
							btnSendall: true,})
						self.props.refreshInvitedListFunc()
						//self.props.isLoadingStopFunc()
					}
					self.props.isLoadingStopFunc()
				})
				.catch((error) => {
					self.props.isLoadingStopFunc()
				})
		} else if (result === 1) {
			let errors = {}
			errors["Addanotheruser"] =
				"One or more mandatory fields have an error. Please check and try again"
			self.setState({
				errors: errors,
			})
		}
	} else if (self.state.emailExiest === 1) {
		let errors = {}
		errors["Addanotheruser"] = "Email is not valid "
		self.setState({
			errors: errors,
		})
	} else if (self.state.emailExiest === 2) {
		let errors = {}
		errors["Addanotheruser"] = "Email already exists "
		self.setState({
			errors: errors,
		})
	}
}
const NoDataConst = (props) => <span className='table-nodata-style'>No records found. </span>

const openModal = (rowid, self, flg, id) => {
	self.setState({
		isOpen: true,
		isDeleteId: id,
		isDeleteRowId: rowid,
		isDeleteflag: flg,
	})
}

const handlerFormatDate = (date) => {
	var d = new Date(date),
		month = "" + (d.getMonth() + 1),
		day = "" + d.getDate(),
		year = d.getFullYear()
	if (month.length < 2) month = "0" + month
	if (day.length < 2) day = "0" + day
	return [year, month, day].join("-")
}

const handleBtnSend = (self) => {
	let selectdata = self.state.fields
	let result = 0
	if (self.state.emailExiest === 0) {
		for (var i = 0; i < selectdata.length; i++) {
			let data = self.state.fields[i]
			let id = data.id
			let role = data.role
			let roledata = role.toString()
			let email = document.getElementById("Email_tbl" + i)
			let fullname = document.getElementById("Fullname_tbl" + i)
			let Roleid = document.getElementById("Role_tbl" + i)
			let country = document.getElementById("Country_tbl" + i)
			let council = document.getElementById("council_tbl" + i)
			let department = document.getElementById("Department_tbl" + i)
			let date = document.getElementById("Date_tbl" + i)
			let association = document.getElementById("association_tbl" + i)
			let club = document.getElementById("club_tbl" + i)
			let sport = document.getElementById("sport_tbl" + i)
			let selectdatas = self.state.selected
			for (const [key, value] of Object.entries(selectdatas)) {
				if (value === true && parseInt(key) === id) {
					if (data.fullname === undefined || data.fullname === "") {
						if (fullname !== null) {
							result = 1
							fullname.classList.add("mandatory_text")
						}
					}
					if (data.email === undefined || data.email === "") {
						if (email !== null) {
							result = 1
							email.classList.add("mandatory_text")
						}
					}
					if (data.role === undefined || roledata === "") {
						if (Roleid != null) {
							result = 1
							Roleid.classList.add("mandatory_text")
						}
					}
					if (
						(data.country === undefined || data.country.toString() === "") &&
						self.state.isCountryCloumn === true
					) {
						if (country !== null) {
							result = 1
							country.classList.add("mandatory_text")
						}
					}
					if (
						(data.council === undefined || data.council.toString() === "") &&
						self.state.iscouncilCloumn === true
					) {
						if (council !== null) {
							result = 1
							council.classList.add("mandatory_text")
						}
					}
					if (
						(data.association === undefined || data.association.toString() === "") &&
						self.state.isAssociationColumn === true
					) {
						if (association !== null) {
							result = 1
							association.classList.add("mandatory_text")
						}
					}
					if (
						(data.club === undefined || data.club.toString() === "") &&
						self.state.isClubColumn === true
					) {
						if (club !== null) {
							result = 1
							club.classList.add("mandatory_text")
						}
					}
					if (
						(data.department === undefined || data.department.toString() === "") &&
						self.state.isDepartmentCloumn === true
					) {
						if (department != null) {
							result = 1
							department.classList.add("mandatory_text")
						}
					}
					if (
						(data.sport === undefined || data.sport.toString() === "") &&
						self.state.isSportcloumn === true
					) {
						if (sport != null) {
							result = 1
							sport.classList.add("mandatory_text")
						}
					}
					if (data.date === undefined || data.date === "") {
						if (date != null) {
							result = 1
							date.classList.add("mandatory_text")
						}
					}
				}
			}
		}
		if (result === 0) {
			let selectdata = self.state.selected
			for (const [key, value] of Object.entries(selectdata)) {
				if (value === true) {
					handleSend(parseInt(key), self, -1)
				}
			}
		} else if (result === 1) {
			let errors = {}
			errors["Addanotheruser"] =
				"One or more mandatory fields have an error. Please check and try again"
			self.setState({
				errors: errors,
			})
		}
	} else if (self.state.emailExiest === 1) {
		let errors = {}
		errors["Addanotheruser"] = "Email is not valid "
		self.setState({
			errors: errors,
		})
	} else if (self.state.emailExiest === 2) {
		let errors = {}
		errors["Addanotheruser"] = "Email already exists "
		self.setState({
			errors: errors,
		})
	}
}
const handelerBtnDelete = (self) => {
	self.props.isLoadingStartFunc()
	let rowId = Object.keys(self.state.selected)
	const fieldsCopy = self.state.fields.filter((row) => !rowId.toString().includes(row.id))
	self.setState({ fields: fieldsCopy, selected: [], errors: [], emailExiest: 0 }, function () {
		if (self.state.fields.length === 0) {
			self.setState({
				btnDelete: true,
				btnDeleteAll: true,
				btnSend: true,
				btnSendall: true,
				addUsertxt: "Add User",
			})
		}
		self.props.isLoadingStopFunc()
		showNotification("Record has been deleted", "success")
	})
}

const handleSend = (id, self, rowid) => {
	let data = self.state.fields.filter((x) => x.id === id)
	data = data[0]
	let role = data.role
	let roledata = role.toString()
	let valid = 0
	let email = document.getElementById("Email_tbl" + rowid)
	let fullname = document.getElementById("Fullname_tbl" + rowid)
	let Roleid = document.getElementById("Role_tbl" + rowid)
	let country = document.getElementById("Country_tbl" + rowid)
	let council = document.getElementById("council_tbl" + rowid)
	let association = document.getElementById("association_tbl" + rowid)
	let club = document.getElementById("club_tbl" + rowid)
	let department = document.getElementById("Department_tbl" + rowid)
	let date = document.getElementById("Date_tbl" + rowid)
	let sport = document.getElementById("sport_tbl" + rowid)

	if (self.state.emailExiest === 0) {
		if (data.fullname === undefined || data.fullname === "") {
			if (fullname !== null) {
				valid = 1
				fullname.classList.add("mandatory_text")
			}
		}
		if (data.email === undefined || data.email === "") {
			if (email !== null) {
				valid = 1
				email.classList.add("mandatory_text")
			}
		}
		if (data.role === undefined || roledata === "") {
			if (Roleid != null) {
				valid = 1
				Roleid.classList.add("mandatory_text")
			}
		}
		if (
			(data.country === undefined || data.country.toString() === "") &&
			self.state.isCountryCloumn === true
		) {
			if (country !== null) {
				valid = 1
				country.classList.add("mandatory_text")
			}
		}
		if (
			(data.council === undefined || data.council.toString() === "") &&
			self.state.iscouncilCloumn === true
		) {
			if (council !== null) {
				valid = 1
				council.classList.add("mandatory_text")
			}
		}
		if (
			(data.association === undefined || data.association.toString() === "") &&
			self.state.isAssociationColumn === true
		) {
			if (association !== null) {
				valid = 1
				association.classList.add("mandatory_text")
			}
		}
		if (
			(data.club === undefined || data.club.toString() === "") &&
			self.state.isClubColumn === true
		) {
			if (club !== null) {
				valid = 1
				club.classList.add("mandatory_text")
			}
		}
		if (
			(data.department === undefined || data.department.toString() === "") &&
			self.state.isDepartmentCloumn === true
		) {
			if (department != null) {
				valid = 1
				department.classList.add("mandatory_text")
			}
		}
		if (
			(data.sport === undefined || data.sport.toString() === "") &&
			self.state.isSportcloumn === true
		) {
			if (sport != null) {
				valid = 1
				sport.classList.add("mandatory_text")
			}
		}
		if (data.date === undefined || data.date === "") {
			if (date != null) {
				valid = 1
				date.classList.add("mandatory_text")
			}
		}
		if (valid === 0) {
			let expiryDate = handlerFormatDate(data.date)

			let InputData = [
				{
					ExpiryDate: expiryDate,
					DepartmentID: data.department.toString(),
					InvitedByID: "129",
					FullName: data.fullname,
					Email: data.email,
					CountryID: data.country.toString(),
					RoleID: roledata,
					CouncilIDs: data.council.toString(),
					CouncilID: data.council.toString() === "" ? "0" : data.council.toString(),
					AssociationID: data.association.toString() === "" ? "0" : data.association.toString(),
					ClubID: data.club.toString() === "" ? "0" : data.club.toString(),
					Association: self.state.isAssociationUsertab,
					Committee: self.state.isCommitteetab,
					Council: self.state.isCounciltab,
					Club: self.state.isClubUsertab,
					Athlete: self.state.isAthleteUsertab,
					Coach: self.state.isCoachUsertab,
					SportID: data.sport.toString() === "" ? "0" : data.sport.toString(),
				},
			]
			self.props.isLoadingStartFunc()
			APIRequest.getPostService(APIUrl.ADD_INVITE_USERS, InputData)
				.then((response) => {
					if (response.Succeeded === true) {
						handleDelete(id, self)
						self.deleteModal()
						//This below function is used to call the Invited User list
						self.props.refreshInvitedListFunc()
						self.props.isLoadingStopFunc()
						showNotification("Saved Successfully, Email notification sent success", "success")
					}
				})
				.catch((error) => {
					self.props.isLoadingStopFunc()
				})
		} else if (valid === 1) {
			let errors = {}
			errors["Addanotheruser"] =
				"One or more mandatory fields have an error. Please check and try again"
			self.setState({
				errors: errors,
			})
		}
	} else if (self.state.emailExiest === 1) {
		let errors = {}
		errors["Addanotheruser"] = "Email is not valid "
		self.setState({
			errors: errors,
		})
	} else if (self.state.emailExiest === 2) {
		let errors = {}
		errors["Addanotheruser"] = "Email already exists "
		self.setState({
			errors: errors,
		})
	}
}
const handleDeleteAll = (self) => {
	self.props.isLoadingStartFunc()
	self.setState(
		{
			fields: [],
			rowcount: 0,
			selected: [],
			btnDeleteAll: true,
			btnSendall: true,
			btnDelete: true,
			btnSend: true,
			addUsertxt: "Add User",
			errors: [],
			emailExiest: 0,
		},
		function () {
			self.props.isLoadingStopFunc()
			showNotification("Record has been deleted", "success")
		}
	)
}
class ManageAddInviteUser extends React.Component {
	constructor(props) {
		super(props)
		const { userPermissions, userRoles } = this.props.user
		let permissions =
			userPermissions !== undefined && userPermissions !== null ? userPermissions : []
		permissions = Array.isArray(permissions) ? permissions : [permissions]

		let roles = userRoles !== undefined && userRoles !== null ? userRoles : []
		roles = Array.isArray(userRoles) ? userRoles : [userRoles]

		this.state = {
			selected: {},
			selectAll: 0,
			rowcount: 0,
			fields: [],
			countries: [],
			role_list: [],
			department_list: [],
			sport_list: [],
			errors: {
				Addanotheruser: "",
			},
			btnSendall: true,
			btnSend: true,
			btnDelete: true,
			btnDeleteAll: true,
			btnAddinvite: true,
			emailExiest: 0,
			isOpen: false,
			isDelete: false,
			isDeleteId: 0,
			isDeleteflag: 0,
			addUsertxt: "Add User",
			isCommitteetab: false,
			isCounciltab: false,
			isCountryCloumn: false,
			isDepartmentCloumn: false,
			isSportcloumn: false,
			council: [],
			councilList: [],
			iscouncilCloumn: false,
			isAssociationColumn: false,
			associationList: [],
			isAssociationUsertab: false,
			isClubUsertab: false,
			isClubColumn: false,
			clubList: [],
			isCoachUsertab: false,
			isAthleteUsertab: false,
			permissions: permissions,
			roles: roles,
		}
		this.table = React.createRef()
		this.toggleRow = this.toggleRow.bind(this)
		this.handleAddRow = this.handleAddRow.bind(this)
		this.closeModal = this.closeModal.bind(this)
		this.deleteModal = this.deleteModal.bind(this)
		this.disabledDate = this.disabledDate.bind(this)
	}
	disabledDate = (current) => {
		return moment().add(-1, "days") >= current
		//current && current < (moment().endOf('day')-1);
	}
	deleteModal = async () => {
		let id = this.state.isDeleteId
		let Rowid = this.state.isDeleteRowId
		let flag = this.state.isDeleteflag
		if (flag === 1) {
			handleDeleteAll(this)
			this.closeModal()
		} else if (flag === 2) {
			handelerBtnDelete(this)
			this.closeModal()
		} else if (flag === 3) {
			this.props.isLoadingStartFunc()
			let fields = this.state.fields
			fields.splice(Rowid, 1)
			this.setState({ fields })
			this.closeModal()
			this.setState({ errors: [], emailExiest: 0 })
			let rowId = this.state.selected
			let data = rowId[id]
			if (data !== undefined) {
				delete rowId[id]
			}
			this.props.isLoadingStopFunc()
			showNotification("Record has been deleted", "success")
			if (Object.keys(this.state.selected).length === 0) {
				this.setState({ btnDelete: true, btnSend: true })
			}
			if (this.state.fields.length === 0) {
				this.setState({
					rowcount: 0,
					selected: [],
					btnDelete: true,
					btnDeleteAll: true,
					btnSend: true,
					btnSendall: true,
					addUsertxt: "Add user",
				})
			}
		}
	}
	closeModal = () => {
		this.setState({ isOpen: false })
	}

	handleAddRow = () => {
		let count = this.state.rowcount
		let rowData = this.state.fields.length
		let fields = this.state.fields
		let valid = 0
		if (this.state.emailExiest === 0) {
			let toggle = document.querySelectorAll(
				".rt-tbody>.rt-tr-group>.rt-tr>.rt-td.tableheader>.mandatory_text"
			)
			for (var i = 0; i < toggle.length; i++) {
				toggle[i].classList.remove("mandatory_text")
			}
			for (let i = 0; i < fields.length; i++) {
				let data = this.state.fields[i]

				let role = data.role
				let roledata = role.toString()
				let email = document.getElementById("Email_tbl" + i)
				let fullname = document.getElementById("Fullname_tbl" + i)
				let Roleid = document.getElementById("Role_tbl" + i)
				let country = document.getElementById("Country_tbl" + i)
				let council = document.getElementById("council_tbl" + i)
				let association = document.getElementById("association_tbl" + i)
				let club = document.getElementById("club_tbl" + i)
				let department = document.getElementById("Department_tbl" + i)
				let date = document.getElementById("Date_tbl" + i)
				let sport = document.getElementById("sport_tbl" + i)
				if (data.fullname === undefined || data.fullname === "") {
					valid = 1
					if (fullname !== null) {
						fullname.classList.add("mandatory_text")
					}
				}
				if (data.email === undefined || data.email === "") {
					valid = 1
					if (email !== null) {
						email.classList.add("mandatory_text")
					}
				}
				if (data.role === undefined || roledata === "") {
					valid = 1
					if (Roleid !== null) {
						Roleid.classList.add("mandatory_text")
					}
				}
				if (
					(data.country === undefined || data.country.toString() === "") &&
					this.state.isCountryCloumn === true
				) {
					valid = 1
					if (country !== null) {
						country.classList.add("mandatory_text")
					}
				}
				if (
					(data.council === undefined || data.council.toString() === "") &&
					this.state.iscouncilCloumn === true
				) {
					valid = 1
					if (council !== null) {
						council.classList.add("mandatory_text")
					}
				}
				if (
					(data.association === undefined || data.association.toString() === "") &&
					this.state.isAssociationColumn === true
				) {
					valid = 1
					if (association !== null) {
						association.classList.add("mandatory_text")
					}
				}
				if (
					(data.club === undefined || data.club.toString() === "") &&
					this.state.isClubColumn === true
				) {
					valid = 1
					if (club !== null) {
						club.classList.add("mandatory_text")
					}
				}

				if (
					(data.sport === undefined || data.sport.toString() === "") &&
					this.state.isSportcloumn === true
				) {
					valid = 1
					if (sport != null) {
						sport.classList.add("mandatory_text")
					}
				}
				if (
					(data.department === undefined || data.department.toString() === "") &&
					this.state.isDepartmentCloumn === true
				) {
					valid = 1
					if (department != null) {
						department.classList.add("mandatory_text")
					}
				}
				if (data.date === undefined || data.date === "aN/aN/NaN" || data.date === "") {
					valid = 1
					if (date != null) {
						date.classList.add("mandatory_text")
					}
				}
			}
			if (valid === 0) {
				if (rowData === 0) {
					count = 0
					this.setState({
						selected: [],
						btnSendall: false,
						btnSend: true,
						btnDelete: true,
						btnDeleteAll: false,
						//addUsertxt: "Add another user",
					})
				}
				this.setState({
					rowcount: count + 1,
				})
				const values = [...this.state.fields]
				values.push({
					fullname: "",
					email: "",
					id: count,
					role: [],
					country: [],
					department: [],
					council: [],
					association: [],
					club: [],
					date: "",
					sport: [],
				})
				this.setState({
					fields: values,
					errors: {
						Addanotheruser: "",
					},
				})
			} else {
				let errors = {}
				errors["Addanotheruser"] =
					"One or more mandatory fields have an error. Please check and try again"
				this.setState({
					errors: errors,
				})
			}
		} else if (this.state.emailExiest === 1) {
			let errors = {}
			errors["Addanotheruser"] = "Email is not valid "
			this.setState({
				errors: errors,
			})
		} else if (this.state.emailExiest === 2) {
			let errors = {}
			errors["Addanotheruser"] = "Email already exists "
			this.setState({
				errors: errors,
			})
		}
	}
	async componentDidMount() {
		let usertype = this.props.name
		let usertypeID = 0
		if (usertype === "committee") {
			this.setState({
				isCommitteetab: true,
				// isCountryCloumn: true,
				isDepartmentCloumn: true,
			})
			usertypeID = 6
		} else if (usertype === "council") {
			this.setState({
				isCounciltab: true,
				iscouncilCloumn: true,
				isDepartmentCloumn: true,
			})
			usertypeID = 1
		} else if (usertype === "association") {
			this.setState({
				isAssociationUsertab: true,
				isAssociationColumn: true,
			})
			usertypeID = 2
		} else if (usertype === "club") {
			this.setState({
				isClubUsertab: true,
				isClubColumn: true,
			})
			usertypeID = 3
		} else if (usertype === "coach") {
			this.setState({
				isCoachUsertab: true,
				isSportcloumn: true,
			})
			usertypeID = 4
		} else if (usertype === "athlete") {
			this.setState({
				isAthleteUsertab: true,
				isSportcloumn: true,
			})
			usertypeID = 5
		}
		const country = await APIRequest.getGetService(APIUrl.COUNTRY_LIST)
		this.setState({ countries: country.Data })

		const role = await APIRequest.getGetService(APIUrl.GET_USER_TYPE_ROLE + "/" + usertypeID + "/@")
		this.setState({ role_list: role.Data })
		const departmentlist = await APIRequest.getGetService(APIUrl.DEPARTMENT_LIST)
		this.setState({ department_list: departmentlist.Data })
		const councilList = await APIRequest.getGetService(APIUrl.COUNCIL_LIST)
		this.setState({ councilList: councilList.Data })
		const associationList = await APIRequest.getGetService(APIUrl.ASSOCIATION_LIST)
		this.setState({ associationList: associationList.Data })
		const clubList = await APIRequest.getGetService(APIUrl.CLUB_LIST)
		this.setState({ clubList: clubList.Data })
		const Sport = await APIRequest.getGetService(APIUrl.GET_SPORT_LIST)
		this.setState({ sport_list: Sport.Data })
	}
	// row check box check function

	toggleRow(id) {
		const newSelected = Object.assign({}, this.state.selected)
		newSelected[id] = !this.state.selected[id]
		this.setState(
			{
				selected: newSelected,
				selectAll: 2,
			},
			function () {
				let flag = 0
				for (const [key, value] of Object.entries(newSelected)) {
					if (value === true) {
						flag = 1
					} else {
						let rowId = this.state.selected
						let data = rowId[parseInt(key)]
						if (data !== undefined) {
							delete rowId[id]
						}
					}
				}
				if (flag === 1) {
					this.setState({
						btnSend: false,
						btnDelete: false,
					})
				} else {
					this.setState({
						btnSend: true,
						btnDelete: true,
					})
				}
			}
		)
	}
	handleDropdownChange = async (cellInfo, event) => {
		if (event === undefined) {
			await this.handlefocusoutChange(cellInfo, event)
			event = []
		}
		let fields = [...this.state.fields]
		fields[cellInfo.index][cellInfo.column.id] = event
		this.setState({ fields })
		let data = [cellInfo.column.id].toString()
		let id = [cellInfo.index].toString()
		let Roleid = document.getElementById("Role_tbl" + id)

		let value = event.toString()
		if (data === "role" && value === "") {
			if (Roleid !== null) {
				Roleid.classList.add("mandatory_text")
			}
		}
		if (data === "role" && value !== "") {
			if (Roleid !== null) {
				Roleid.classList.remove("mandatory_text")
			}
		}
	}
	handlefocusoutChange = (cellInfo, event) => {
		let data = [cellInfo.column.id].toString()
		let id = [cellInfo.index].toString()
		let Roleid = document.getElementById("Role_tbl" + id)
		let country = document.getElementById("Country_tbl" + id)
		let department = document.getElementById("Department_tbl" + id)
		let council = document.getElementById("council_tbl" + id)
		let sport = document.getElementById("sport_tbl" + id)
		let association = document.getElementById("association_tbl" + id)
		let club = document.getElementById("club_tbl" + id)
		let fields = [...this.state.fields]

		let value = fields[cellInfo.index][cellInfo.column.id].toString()
		if (data === "role" && value === "") {
			if (Roleid !== null) {
				Roleid.classList.add("mandatory_text")
			}
		}
		if (data === "role" && value !== "") {
			if (Roleid !== null) {
				Roleid.classList.remove("mandatory_text")
			}
		}
		if (data === "country" && value === "" && this.state.isCountryCloumn === true) {
			if (country != null) {
				country.classList.add("mandatory_text")
			}
		}
		if (data === "country" && value !== "" && this.state.isCountryCloumn === true) {
			if (country != null) {
				country.classList.remove("mandatory_text")
			}
		}
		if (data === "department" && value === "" && this.state.isDepartmentCloumn === true) {
			if (department != null) {
				department.classList.add("mandatory_text")
			}
		}
		if (data === "department" && value !== "" && this.state.isDepartmentCloumn === true) {
			if (department != null) {
				department.classList.remove("mandatory_text")
			}
		}
		if (data === "sport" && value === "" && this.state.isSportcloumn === true) {
			if (sport != null) {
				sport.classList.add("mandatory_text")
			}
		}
		if (data === "sport" && value !== "" && this.state.isSportcloumn === true) {
			if (sport != null) {
				sport.classList.remove("mandatory_text")
			}
		}
		if (data === "council" && value === "" && this.state.iscouncilCloumn === true) {
			if (council != null) {
				council.classList.add("mandatory_text")
			}
		}
		if (data === "council" && value !== "" && this.state.iscouncilCloumn === true) {
			if (council != null) {
				council.classList.remove("mandatory_text")
			}
		}
		if (data === "association" && value === "" && this.state.isAssociationColumn === true) {
			if (association != null) {
				association.classList.add("mandatory_text")
			}
		}
		if (data === "association" && value !== "" && this.state.isAssociationColumn === true) {
			if (association != null) {
				association.classList.remove("mandatory_text")
			}
		}
		if (data === "club" && value === "" && this.state.isClubColumn === true) {
			if (club != null) {
				club.classList.add("mandatory_text")
			}
		}
		if (data === "club" && value !== "" && this.state.isClubColumn === true) {
			if (club != null) {
				club.classList.remove("mandatory_text")
			}
		}
	}
	handleInputChange = (cellInfo, event) => {
		
		let fields = [...this.state.fields]
		let fullname = [cellInfo.column.id].toString()
		if(fullname==="fullname"){
	var valuess =	event.target.value.replace(/[^A-Za-z\s]/, '')
		fields[cellInfo.index][cellInfo.column.id] = valuess
	}
	else{
		fields[cellInfo.index][cellInfo.column.id] = event.target.value
	}

	this.setState({ fields })
		
		let id = [cellInfo.index].toString()
		if (fullname === "fullname" && valuess !== "") {
			var myElementID = document.getElementById("Fullname_tbl" + id)
			if (myElementID !== null) {
				var element = document.getElementById("Fullname_tbl" + id)
				element.classList.remove("mandatory_text")
			}
		} else if (fullname === "fullname" && valuess=== "") {
			var myElementID = document.getElementById("Fullname_tbl" + id)
			if (myElementID !== null) {
				var element = document.getElementById("Fullname_tbl" + id)
				element.classList.add("mandatory_text")
			}
		}
	}
	handleDatePickerBlur = (cellInfo, event) => {
		let dataformet = this.state.fields[cellInfo.index].date
		let id = [cellInfo.index].toString()
		if (dataformet !== "aN/aN/NaN" && dataformet !== "") {
			var myElementIDThree = document.getElementById("Date_tbl" + id)
			if (myElementIDThree !== null) {
				var elementThree = document.getElementById("Date_tbl" + id)
				elementThree.classList.remove("mandatory_text")
			}
		} else if (dataformet === "aN/aN/NaN" || dataformet === "") {
			var myElementIDFour = document.getElementById("Date_tbl" + id)
			if (myElementIDFour !== null) {
				var elementFour = document.getElementById("Date_tbl" + id)
				elementFour.classList.add("mandatory_text")
			}
		}
	}
	handleDatePickerChange = (cellInfo, event) => {
		let fields = [...this.state.fields]
		if (event === null) {
		}
		let dates = new Date(event)
		let Day = ("0" + dates.getDate()).slice(-2)
		let month = ("0" + (dates.getMonth() + 1)).slice(-2)
		let dataformet = month + "-" + Day + "-" + dates.getFullYear()
		let id = [cellInfo.index].toString()
		if (event !== null) {
			fields[cellInfo.index][cellInfo.column.id] = dataformet
			this.setState({ fields })
			let date = [cellInfo.column.id].toString()
			if (date === "date" && dataformet !== "aN/aN/NaN") {
				var myElementIDOne = document.getElementById("Date_tbl" + id)
				if (myElementIDOne !== null) {
					var elementOne = document.getElementById("Date_tbl" + id)
					elementOne.classList.remove("mandatory_text")
				}
			} else if (date === "date" && dataformet === "aN/aN/NaN") {
				var myElementIDTwo = document.getElementById("Date_tbl" + id)
				if (myElementIDTwo !== null) {
					var elementTwo = document.getElementById("Date_tbl" + id)
					elementTwo.classList.add("mandatory_text")
				}
			}
		} else {
			fields[cellInfo.index][cellInfo.column.id] = "" //"aN/aN/NaN"
			this.setState({ fields })
			var myElementIDThree = document.getElementById("Date_tbl" + id)
			if (myElementIDThree !== null) {
				var elementThree = document.getElementById("Date_tbl" + id)
				elementThree.classList.add("mandatory_text")
			}
		}
	}
	handleEmailCheck = async (cellInfo, event) => {
		let fields = [...this.state.fields]
		fields[cellInfo.index]["email"] = event.target.value
		var email = this.state.fields.filter((x) => x.email === event.target.value)
		let isValid = true

		if (event.target.value !== "undefined") {
			var pattern = new RegExp(
				/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			)
			if (email.length !== 1) {
				isValid = false
				this.setState({
					emailExiest: 2, /// not valid value 1
					errors: { Addanotheruser: "Email already exists " },
				})
			} else if (!pattern.test(event.target.value)) {
				isValid = false
				this.setState({
					emailExiest: 1, /// not valid value 1
					errors: { Addanotheruser: "Email is not valid " },
				})
			} else {
				const Email = await APIRequest.getGetService(
					APIUrl.VALIDATE_USER_EXIST + "/" + event.target.value
				)
				if (Email.Data === true) {
					isValid = false
					this.setState({
						emailExiest: 2, ///exiest value 2
						errors: { Addanotheruser: "Email already exists  " },
					})
				}
			}
		}
		if (isValid === false) {
			var element = document.getElementById("Email_tbl" + cellInfo.index)
			element.classList.add("mandatory_text")
		} else {
			var elementOne = document.getElementById("Email_tbl" + cellInfo.index)
			elementOne.classList.remove("mandatory_text")
			this.setState({
				emailExiest: 0,
				errors: {
					Addanotheruser: "",
				},
			})
		}
	}

	renderEditableEmail = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div className={"Input_height"}>
				<Input
					className={" pl-0 tbl_textfiled Email_tbl" + cellInfo.index}
					id={"Email_tbl" + cellInfo.index}
					size='large'
					placeholder='Email'
					name='input'
					type='text'
					bordered={false}
					autoComplete='off'
					onBlur={this.handleEmailCheck.bind(null, cellInfo)}
					onChange={this.handleInputChange.bind(null, cellInfo)}
					value={cellValue}
				/>
			</div>
		)
	}

	renderEditableRole = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div className={"tbl_listfiled Role_tbl" + cellInfo.index} id={"Role_tbl" + cellInfo.index}>
				<Space direction='vertical' style={{ width: "100%" }}>
					<Select
						mode='multiple'
						className={"  create-component-select multi-select"}
						maxTagCount="responsive"
						// maxTagTextLength={3}
						showSearch
						showArrow='true'
						allowClear
						style={{ width: "100%" }}
						value={cellValue}
						size={"large"}
						name='role'
						bordered={false}
						placeholder='Select Role'
						optionFilterProp='children'
						onBlur={this.handlefocusoutChange.bind(null, cellInfo)}
						onChange={this.handleDropdownChange.bind(null, cellInfo)}
						filterSort={(optionA, optionB) =>
							optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
						}
					>
						{this.state.role_list.map((c) => (
							<Option value={c.OptionID}>{c.OptionData}</Option>
						))}
					</Select>
				</Space>
			</div>
		)
	}
	renderEditableCountry = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"Country_tbl" + cellInfo.index}
				className={"tbl_listfiled Country_tbl" + cellInfo.index}
			>
				<Select
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					showSearch
					name='country'
					bordered={false}
					allowClear={true}
					placeholder='Select Country'
					optionFilterProp='children'
					defaultValue='0'
					onBlur={this.handlefocusoutChange.bind(null, cellInfo)}
					onChange={this.handleDropdownChange.bind(null, cellInfo)}
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{this.state.countries.map((c) => (
						<Option value={c.OptionID}>{c.OptionData}</Option>
					))}
				</Select>
			</div>
		)
	}
	renderEditableclub = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"club_tbl" + cellInfo.index} className={"tbl_listfiled club_tbl" + cellInfo.index}>
				<Select
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					showSearch
					name='club'
					bordered={false}
					allowClear={true}
					placeholder='Select Club'
					optionFilterProp='children'
					defaultValue='0'
					onBlur={this.handlefocusoutChange.bind(null, cellInfo)}
					onChange={this.handleDropdownChange.bind(null, cellInfo)}
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{this.state.clubList.map((c) => (
						<Option value={c.OptionID}>{c.OptionData}</Option>
					))}
				</Select>
			</div>
		)
	}
	renderEditableassociation = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"association_tbl" + cellInfo.index}
				className={"tbl_listfiled association_tbl" + cellInfo.index}
			>
				<Select
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					showSearch
					bordered={false}
					name='association'
					allowClear={true}
					placeholder='Select Association'
					optionFilterProp='children'
					defaultValue='0'
					onBlur={this.handlefocusoutChange.bind(null, cellInfo)}
					onChange={this.handleDropdownChange.bind(null, cellInfo)}
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{this.state.associationList.map((c) => (
						<Option value={c.OptionID}>{c.OptionData}</Option>
					))}
				</Select>
			</div>
		)
	}
	renderEditablecouncil = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"council_tbl" + cellInfo.index}
				className={"tbl_listfiled council_tbl" + cellInfo.index}
			>
				<Select
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					showSearch
					name='council'
					allowClear={true}
					bordered={false}
					placeholder='Select Council'
					optionFilterProp='children'
					defaultValue='0'
					onBlur={this.handlefocusoutChange.bind(null, cellInfo)}
					onChange={this.handleDropdownChange.bind(null, cellInfo)}
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{this.state.councilList.map((c) => (
						<Option value={c.OptionID}>{c.OptionData}</Option>
					))}
				</Select>
			</div>
		)
	}
	renderEditableDepartment = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"Department_tbl" + cellInfo.index}
				className={"tbl_listfiled Department_tbl" + cellInfo.index}
			>
				<Select
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					showSearch
					bordered={false}
					name='department'
					allowClear={true}
					placeholder='Select Department'
					optionFilterProp='children'
					onBlur={this.handlefocusoutChange.bind(null, cellInfo)}
					onChange={this.handleDropdownChange.bind(null, cellInfo)}
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{this.state.department_list.map((c) => (
						<Option value={c.OptionID}>{c.OptionData}</Option>
					))}
				</Select>
			</div>
		)
	}
	renderEditableSport = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"sport_tbl" + cellInfo.index} className={"tbl_listfiled sport_tbl" + cellInfo.index}>
				<Select
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					showSearch
					bordered={false}
					name='sport'
					allowClear={true}
					placeholder='Select Sport'
					optionFilterProp='children'
					onBlur={this.handlefocusoutChange.bind(null, cellInfo)}
					onChange={this.handleDropdownChange.bind(null, cellInfo)}
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{this.state.sport_list.map((c) => (
						<Option value={c.OptionID}>{c.OptionData}</Option>
					))}
				</Select>
			</div>
		)
	}
	renderEditableFullName = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div className={"Input_height"}>
				<Input
					size='large'
					id={"Fullname_tbl" + cellInfo.index}
					className={"pl-0 tbl_textfiled Fullname_tbl" + cellInfo.index}
					placeholder='Full Name'
					name='select'
					type='text'
					autoComplete='off'
					bordered={false}
					onChange={this.handleInputChange.bind(null, cellInfo)}
					onBlur={this.handleInputChange.bind(null, cellInfo)}
					value={cellValue}
				/>
			</div>
		)
	}
	renderEditableDate = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"Date_tbl" + cellInfo.index} className={"tbl_listfiled Date_tbl" + cellInfo.index}>
				<Space direction='vertical' size={1}>
					<DatePicker
						size='large'
						format='DD MMM YYYY'
						name='date'
						bordered={false}
						showToday={false}
						allowClear={true}
						allowEmpty={true}
						placeholder='Select Expiry Date'
						// defaultValue={cellValue}
						value={cellValue !== "" ? moment(cellValue) : ""}
						className={" "}
						disabledDate={this.disabledDate}
						onBlur={this.handleDatePickerBlur.bind(null, cellInfo)}
						onChange={this.handleDatePickerChange.bind(null, cellInfo)}
					/>
				</Space>
			</div>
		)
	}
	// table  checkbox all  check uncheck function
	toggleSelectAll() {
		let newSelected = {}
		if (this.state.selectAll === 0 && this.state.fields.length !== 0) {
			this.state.fields.forEach((x) => {
				newSelected[x.id] = true
			})
			this.setState({
				btnSend: false,
				btnDelete: false,
			})
		} else {
			this.setState({
				btnSend: true,
				btnDelete: true,
			})
		}
		this.setState({
			selected: newSelected,
			selectAll: this.state.selectAll === 0 ? 1 : 0,
		})
	}

	render() {
		let permissions = this.state.permissions

		// table columns
		const columns = [
			{
				Header: " ",
				resizable: false,
				filterable: false,
				id: "checkbox",
				accessor: "",
				Cell: ({ original }) => {
					return (
						<div className='pt-1 text-center'>
							<input
								type='checkbox'
								className='checkbox'
								checked={this.state.selected[original.id] === true}
								onChange={() => this.toggleRow(original.id)}
							/>
						</div>
					)
				},
				Header: (x) => {
					return (
						<input
							type='checkbox'
							className='checkbox m-0'
							title='Select All'
							checked={this.state.selectAll === 1}
							ref={(input) => {
								if (input) {
									input.indeterminate = this.state.selectAll === 2
								}
							}}
							onChange={() => this.toggleSelectAll()}
						/>
					)
				},
				sortable: false,
				width: 50,
			},
			{
				Header: () => <div className='text-left'>S.No</div>,
				accessor: "id",
				show: true,
				resizable: false,
				filterable: false,
				className: "tableheader ",
				headerClassName: "BoldText ColoredText",
				sortable: false,
				Cell: (row) => <div className='tabledata text-left'>{row.value + 1}</div>,
				width: 50,
			},
			{
				Header: () => <div className='text-left pl-0'>Full Name</div>,
				accessor: "fullname",
				align: "left",
				className: "tableheader text-left ",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableFullName,
			},
			{
				Header: () => <div className='text-left pl-0'>Email</div>,
				accessor: "email",
				sortable: false,
				resizable: false,
				className: "tableheader text-left",
				Cell: this.renderEditableEmail,
			},
			{
				Header: () => <div className='text-left pl-0'>Council</div>,
				show: this.state.iscouncilCloumn,
				accessor: "council",
				resizable: false,
				sortable: false,
				className: "tableheader text-left",
				Cell: this.renderEditablecouncil,
			},
			{
				Header: () => <div className='text-left pl-0'>Association</div>,
				show: this.state.isAssociationColumn,
				accessor: "association",
				resizable: false,
				sortable: false,
				className: "tableheader text-left",
				Cell: this.renderEditableassociation,
			},
			{
				Header: () => <div className='text-left pl-0 '>Club</div>,
				show: this.state.isClubColumn,
				accessor: "club",
				resizable: false,
				sortable: false,
				className: "tableheader text-left",
				Cell: this.renderEditableclub,
			},
			{
				Header: () => <div className='text-left pl-0'>Country</div>,
				show: this.state.isCountryCloumn,
				accessor: "country",
				resizable: false,
				sortable: false,
				className: "tableheader text-left",
				Cell: this.renderEditableCountry,
			},
			{
				Header: () => <div className='text-left pl-0'>Role</div>,
				accessor: "role",
				resizable: false,
				sortable: false,
				className: "tableheader text-left",
				Cell: this.renderEditableRole,
			},
			{
				Header: () => <div className='text-left  pl-0'>Department</div>,
				show: this.state.isDepartmentCloumn,
				accessor: "department",
				resizable: false,
				sortable: false,
				className: "tableheader text-left",
				Cell: this.renderEditableDepartment,
			},
			{
				Header: () => <div className='text-left  pl-0'>Sport</div>,
				show: this.state.isSportcloumn,
				accessor: "sport",
				resizable: false,
				sortable: false,
				className: "tableheader text-left",
				Cell: this.renderEditableSport,
			},

			{
				Header: () => <div className='text-left pl-0'>Expiry Date</div>,
				accessor: "date",
				resizable: false,
				sortable: false,
				className: "tableheader text-left",
				Cell: this.renderEditableDate,
			},
			{
				Header: () => <div className='text-center'>Action</div>,
				accessor: "Action",
				className: "tableheader",
				resizable: false,
				sortable: false,
				filterable: false,
				minWidth: 30,
				width: 70,
				maxWidth: 80,
				id: "delete",
				accessorOne: (str) => "delete",
				Cell: (row) => (
					<div className='mt-2 text-center'>
						{permissions.includes(Permission.CU_CREATE) ? (
							<span
								className='middle text icon_Green mr-2'
								title='Send'
								onClick={() => handleSend(row.original.id, this, row.index)}
								style={{ cursor: "pointer" }}
							>
								➤
							</span>
						) : null}
						{permissions.includes(Permission.CU_DELETE) ? (
							<span
								className='icon_Delete '
								onClick={() => openModal(row.index, this, 3, row.original.id)}
								style={{ cursor: "pointer" }}
							>
								<FontAwesomeIcon title='Delete' icon={faTrashAlt}></FontAwesomeIcon>
							</span>
						) : null}
					</div>
				),
			},
		]

		return (
			<div className='admin-report-table'>
				<div className=' d-flex justify-content-start pt-1 '>
					<p className='tab_form_content'>Invite Users</p>
				</div>

				<ReactTable
					data={this.state.fields}
					columns={columns}
					showPaginationTop={false}
					showPaginationBottom={false}
					minRows={0}
					ref={this.table}
					sortable={true}
					multiSort={true}
					NoDataComponent={NoDataConst}
					manual
					style={{ overflow: "wrap" }}
					defaultSorting={[
						{
							id: "id",
							desc: true, // not necessary unless set to true, but it's good to know its there.
						},
					]}
					className='CosmicTable InviteUser form-group'
					manualPagination={true}
				/>
				{permissions.includes(Permission.CU_CREATE) ? (
					<div>
						<button onClick={this.handleAddRow} className='btn-default border-0 bg-white link'>
							{this.state.addUsertxt} <FontAwesomeIcon icon={faPlusCircle}></FontAwesomeIcon>
						</button>
						{this.state.errors.Addanotheruser !== undefined &&
							this.state.errors.Addanotheruser.length > 0 && (
								<span className='error_danger_msg ml-10'>{this.state.errors.Addanotheruser}</span>
							)}
					</div>
				) : null}
				<div className='col-sm-12'>
					<div className='form-group d-flex justify-content-end row'>
						{permissions.includes(Permission.CU_CREATE) ? (
							<Button
								disabled={this.state.btnSend}
								className='btn btn-primary btn_green float-right  waves-effect waves-light btn-xs  mr-2 btn btn-primary'
								bsStyle='danger'
								bsSize='small'
								onClick={() => handleBtnSend(this)}
								//style={{ cursor: "pointer" }}
							>
								➤ Send
							</Button>
						) : null}
						{permissions.includes(Permission.CU_CREATE) ? (
							<Button
								disabled={this.state.btnSendall}
								className='btn btn-primary btn_green float-right  waves-effect waves-light btn-xs  mr-2 btn btn-primary'
								type='button'
								bsStyle='danger'
								bsSize='small'
								onClick={() => handleBtnSendALL(this)}
								// style={{ cursor: "pointer" }}
							>
								➤ Send All
							</Button>
						) : null}
						{permissions.includes(Permission.CU_DELETE) ? (
							<Button
								disabled={this.state.btnDelete}
								className='btn btn-primary btn_green float-right  waves-effect waves-light btn-xs  mr-2 btn btn-primary'
								bsStyle='danger'
								bsSize='small'
								onClick={() => openModal(0, this, 2, null)}
							>
								<FontAwesomeIcon
									title='Delete'
									color={"#FFFFFF"}
									icon={faTrashAlt}
									className='mr-2'
								></FontAwesomeIcon>
								Delete
							</Button>
						) : null}
						{permissions.includes(Permission.CU_DELETE) ? (
							<Button
								disabled={this.state.btnDeleteAll}
								className='btn btn-primary btn_green float-right  waves-effect waves-light btn-xs  mr-2 btn btn-primary'
								bsStyle='danger'
								bsSize='small'
								onClick={() => openModal(0, this, 1, null)}
							>
								<FontAwesomeIcon
									title='Delete'
									color={"#FFFFFF"}
									icon={faTrashAlt}
									className='mr-2'
								></FontAwesomeIcon>
								Delete All
							</Button>
						) : null}
					</div>
				</div>
				{this.state.isOpen ? (
					<>
						<Modal
							size='sm'
							//aria-labelledby="contained-modal-title-vcenter"
							//   aria-labelledby="contained-modal-title-vcenter"
							backdrop='static'
							//  centered
							show={this.state.isOpen}
							onHide={this.closeModal}
						>
							<div className='popup-content'>
								<div className='deletecircle'></div>
								<i className='fa fa-trash-o deleteicon'></i>
								<Modal.Body>
									<p className='pull-left' style={{ margin: "4rem 2rem" }}>
										{" "}
										<h5>{Message.INVITE_DELETE}</h5>
										<h6 style={{ color: "darkgray" }}>{Message.INVITE_DELETE_CONFIRM}</h6>
										<h6
											style={{
												color: "darkgray",
												marginRight: "7rem",
												marginBottom: "-3rem",
											}}
										>
											{Message.INVITE_DELETE_CONFIRM1}
										</h6>
									</p>
									<br />
									<div className='pull-right'>
										<Button className='btn_cancel mr-2' onClick={this.closeModal}>
											Cancel
										</Button>
										<Button
											className='btn_green'
											style={{ height: "2rem", padding: "5px 10px" }}
											onClick={this.deleteModal}
										>
											Delete
										</Button>
									</div>
								</Modal.Body>
							</div>
						</Modal>
					</>
				) : null}
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	user: state.userReducer,
})

//export default connect(mapStateToProps, null,null)(ManageAddInviteUser)
export default connect(mapStateToProps, null, null, { forwardRef: true })(ManageAddInviteUser)
