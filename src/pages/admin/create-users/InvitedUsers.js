import React, { Fragment } from "react"
import ReactTable from "react-table"
import "../../../styles/CosmicTable.css"
import "antd/dist/antd.css"
import moment from "moment"
import { Select, DatePicker, Input, Space, Spin, notification } from "antd"
import { Button, Modal, Card, Collapse } from "react-bootstrap"
import Pagination from "../../../utils/Pagination"
import UserImage from "../../../content/images/UserImage.png"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Resend from "../../../content/Icons/Resendicon.png"
import {
	faArrowDown,
	faArrowUp,
	faShare,
	faTrashAlt,
	faCalendarCheck,
	faDownload,
	faEye,
} from "@fortawesome/free-solid-svg-icons"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import ExportData from "../../../utils/ExportData"
import { Message } from "../../../message/messageConstant"
import { connect } from "react-redux"
import { Permissions as Permission } from "../../../permissions/AppPermissions"
//import ExportData from "../../Utilities/ExportData"
const { Option } = Select

const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	} else if (flag === 2) {
		//resend flag
		notification[type]({
			message: description,
			description: "",
			icon: (
				<img
					style={{ border: 3, height: 16, width: 16, marginTop: "-4px" }}
					title='Resend'
					src={Resend}
					alt='Resend'
				/>
			),
			//  duration:20
		})
	} else if (flag === 3) {
		//extend expires in
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_color' icon={faCalendarCheck}></FontAwesomeIcon>,
			description: "",
		})
	}
}

const setIsExpanded = (data, self) => {
	let Icons = self.state.Icons === true ? false : true
	self.setState({ IsExpanded: data, Icons: Icons })
}

const handleTblDelete = (id, self) => {
	self.setState({ loading: true })
	APIRequest.getGetService(APIUrl.DELETE_INACTIVE_USER + "/" + id)
		.then((response) => {
			self.setState({ loading: false })
			if (response.Data === true) {
				self.fetch(self.table.current.state)
				openNotificationWithIcon("success", "Record has been deleted", 1)
			}
		})
		.catch((error) => {
			self.setState({ loading: false })
		})
}
const handleResend = (id, self) => {
	self.setState({ loading: true })
	APIRequest.getGetService(APIUrl.RESENT_NOTIFICATION + "/" + id)
		.then((response) => {
			self.setState({ loading: false }, function () {
				if (response.Succeeded === true) {
					// alert(response.Data);
					self.fetch(self.table.current.state)
					openNotificationWithIcon("success", response.Data, 2)
				}
			})
		})
		.catch((error) => {
			self.setState({ loading: false })
		})
}
const NoDataConst = (props) => (
	<span className='table-nodata-style'>
		No records found Please check your connection or change your filters{" "}
	</span>
)

//set open model property
const openModal = (id, self) => {
	self.setState({ isOpen: true, isDeleteID: id })
}
const openDatePickerModal = (data, self) => {
	let id = data.InvitedUserID

	let date = data.ExpiryDate
	self.setState({
		isDatepickerpopup: true,
		isExpirtyUserId: id,
		isDiabledDate: date,
		isSubmitBtn: true,
	})
}
const openViewAlldataModal = (data, self) => {
	var responseDate = moment(data.ExpiryDate).format("DD MMM YY")
	data.ExpiryDate = responseDate
	self.setState({ isEyeDatapopup: true, isEyeData: data })
}

class UserInviteList extends React.Component {
	constructor(props) {
		super(props)

		const { userPermissions, userRoles } = this.props.user
		let permissions =
			userPermissions !== undefined && userPermissions !== null ? userPermissions : []
		permissions = Array.isArray(permissions) ? permissions : [permissions]

		let roles = userRoles !== undefined && userRoles !== null ? userRoles : []
		roles = Array.isArray(userRoles) ? userRoles : [userRoles]

		this.state = {
			users: [],
			loading: true,
			perPage: 1,
			totalpage: 10,
			total_pages: 10,
			Currentpage: 1,
			fullname: "",
			pageSize: 10,
			invitestatus: [],
			email: "",
			userstatus: [],
			department: [],
			selected: {},
			selectAll: 0,
			IsExpanded: false,
			Icons: false,
			value: [],
			departmentList: [],
			invitestatusList: [],
			userstatusList: [],
			TotalPages: 0,
			TotalRecords: 0,
			TotalCount:0,
			IncrementPage: 0,
			isOpen: false,
			isClose: true,
			isDeleteID: 0,
			//Export excel properties
			IsExportExcel: false,
			exportData: [],
			sortColumn: "",
			sortValue: "",
			exportFilename: "",
			resendbtn: true,
			deletebtn: true,
			exportbtn: true,
			isDatepickerpopup: false,
			isExpirtyDate: "",
			isExpirtyUserId: 0,
			isExpirtyError: "",
			isDiabledDate: "",
			isSubmitBtn: true,
			isCountryCloumn: false,
			isInvitedByCloumn: false,
			contry: [],
			countries: [],
			council: [],
			councilList: [],
			iscouncilCloumn: false,
			isDepartmentCloumn: false,
			isSportCloumn: false,
			sport: [],
			sport_list: [],
			isCounciltab: false,
			isCommitteetab: false,
			isSearchBtn: true,
			isAssociationUsertab: false,
			association: [],
			associationList: [],
			isAssociationColumn: false,
			isClubUsertab: false,
			isUserStatuslCloumn: false,
			club: [],
			clubList: [],
			isClubColumn: false,
			isCoachUsertab: false,
			isAthleteUsertab: false,
			isEyeData: [],
			isEyeDatapopup: false,
			permissions: permissions,
			roles: roles,
			exportLabel: [
				{ columnName: "S.No", value: "SerialNo" },
				{ columnName: "Name", value: "Name" },
				{ columnName: "Email", value: "Email" },
				{ columnName: "Invited By", value: "InvitedByName" },
				{ columnName: "Invited Status", value: "InvitedStatus" },
				{ columnName: "Expiry Day", value: "ExpiryDay" },
				{ columnName: "Role Name", value: "UserRoleName" },
				{ columnName: "Invited Date", value: "InvitedDate" },
				{ columnName: "Expiry Date", value: "ExpiryDate" },
			],
			selectedDeleteData: {},
			selectedResendData: {},
		}
		this.table = React.createRef()
		this.fetch = this.fetch.bind(this)
		this.setRef = React.createRef()
		this.toggleRow = this.toggleRow.bind(this)
		this.toggleResentRow = this.toggleResentRow.bind(this)
		this.toggleDeleteRow = this.toggleDeleteRow.bind(this)
		this.handleSetdata = this.handleSetdata.bind(this)
		this.handleClear = this.handleClear.bind(this)
		this.handleBtnDelete = this.handleBtnDelete.bind(this)
		this.handleBtnResend = this.handleBtnResend.bind(this)
		this.DynamicdisabledDate = this.DynamicdisabledDate.bind(this)
		this.DatepickerpopupClose = this.DatepickerpopupClose.bind(this)
		this.ExpirtyDateSubmite = this.ExpirtyDateSubmite.bind(this)
		this.escFunction = this.escFunction.bind(this)
		this.tableColumnDataLimit = this.tableColumnDataLimit.bind(this)
	}

	tableColumnDataLimit = (str, limit) => {
		str = str == null ? "" : str
		return str.length > limit ? str.substring(0, limit - 3) + "..." : str
	}

	handleDatePickerChange = (event) => {
		if (event !== null) {
			let dates = new Date(event)
			let Day = ("0" + dates.getDate()).slice(-2)
			let month = ("0" + (dates.getMonth() + 1)).slice(-2)
			let dataformet = month + "-" + Day + "-" + dates.getFullYear()

			this.setState({ isExpirtyDate: dataformet, isSubmitBtn: false })
		} else {
			this.setState({ isExpirtyDate: "aN-aN-NaN", isSubmitBtn: true })
		}
	}
	ExpirtyDateSubmite = async () => {
		let self = this
		let isExpirtyDate = this.state.isExpirtyDate
		let isExpirtyUserId = this.state.isExpirtyUserId
		if (isExpirtyDate !== "" && isExpirtyDate !== "aN-aN-NaN" && isExpirtyUserId !== 0) {
			self.setState({ loading: true, isDatepickerpopup: false })
			const Date = await APIRequest.getGetService(
				APIUrl.UPDATE_EXPIRTY_DATE_INVITE + "/" + isExpirtyUserId + "/" + isExpirtyDate
			)
			self.setState({ loading: false })
			if (Date.Data === true) {
				self.setState({
					fields: [],
					rowcount: 0,
					selected: [],
					isExpirtyUserId: 0,
					isExpirtyDate: "",
					isDiabledDate: "",
				})
				openNotificationWithIcon("success", "Submitted successfully", 3)
				self.fetch(this.table.current.state)
			}
		} else {
			//this.setState({isDatepickerpopup:false });
		}
	}
	DatepickerpopupClose = () => {
		this.setState({
			isDatepickerpopup: false,
			isExpirtyDate: "",
			isExpirtyUserId: 0,
			isDiabledDate: "",
			isSubmitBtn: true,
		})
	}
	viewAlldatapopupClose = () => {
		this.setState({ isEyeDatapopup: false, isEyeData: [] })
	}
	DynamicdisabledDate = (current) => {
		let date = this.state.isDiabledDate
		var dateconvert = new Date(date)
		var expiryDate = dateconvert.setDate(new Date(dateconvert).getDate() + 1)
		let dynamicexpiryDate = new Date(expiryDate)
		let Day = ("0" + dynamicexpiryDate.getDate()).slice(-2)
		let month = ("0" + (dynamicexpiryDate.getMonth() + 1)).slice(-2)
		let dataformet = dynamicexpiryDate.getFullYear() + "-" + month + "-" + Day
		const start = moment(dataformet, "YYYY-MM-DD")
		return current < start || current < moment()
		//return moment().add(-1, 'days') >= current
	}
	handleBtnDelete = (event) => {
		var InputData = []
		let self = this
		let selectedRowIds = this.handlerGetListIDs(this.state.selectedDeleteData, this.state.users)
		if (selectedRowIds) {
			self.setState({
				loading: true,
			})
			InputData = selectedRowIds.toString()
			APIRequest.getGetService(APIUrl.DELETE_INACTIVE_USER + "/" + InputData)
				.then((response) => {
					self.setState({
						loading: false,
					})
					if (response.Data === true) {
						openNotificationWithIcon("success", "Record has been deleted", 1)
						self.setState({ fields: [], rowcount: 0, selectedDeleteData: [] })
						self.fetch(this.table.current.state)
					}
				})
				.catch((error) => {})
		}
	}
	handlerGetListIDs = (state, userList) => {
		if (state && userList) {
			let rowId = Object.keys(state)
			let selectedRowIds = userList
				.filter((x) => rowId.toString().includes(x.SerialNo))
				.map((x) => x.InvitedUserID)
			return selectedRowIds
		} else return 0
	}
	handleBtnResend(event) {
		var InputData = []
		let selectedRowIds = this.handlerGetListIDs(this.state.selectedResendData, this.state.users)
		let self = this
		if (selectedRowIds) InputData = selectedRowIds.toString()
		self.setState({
			loading: true,
		})
		APIRequest.getGetService(APIUrl.RESENT_NOTIFICATION + "/" + InputData)
			.then((response) => {
				self.setState({
					loading: false,
				})
				if (response.Succeeded === true) {
					self.setState({
						selectedResendData: {},
					})
					self.fetch(self.table.current.state)
					openNotificationWithIcon("success", response.Data, 2)
				}
			})
			.catch((error) => {})
	}
	handleClear(event) {
		this.setState(
			{
				fullname: "",
				invitestatus: [],
				email: "",
				userstatus: [],
				department: [],
				contry: [],
				council: [],
				club: [],
				association: [],
				isSearchBtn: true,
				sport: [],
			},
			function () {
				this.fetch(this.table.current.state)
			}
		)
	}
	handleSetdata(event) {
		let input = this.state
		let searchbtn = true
		let data = this.state
		if (event.target.name === "email") {
			if (
				data.sport.length !== 0 ||
				data.association.length !== 0 ||
				data.club.length !== 0 ||
				data.council.length !== 0 ||
				data.contry.length !== 0 ||
				data.department.length !== 0 ||
				data.fullname.length !== 0 ||
				event.target.value !== "" ||
				data.userstatus.length !== 0 ||
				data.invitestatus.length !== 0
			) {
				searchbtn = false
			}
		}
		if (event.target.name === "fullname") {
			if (
				data.sport.length !== 0 ||
				data.association.length !== 0 ||
				data.club.length !== 0 ||
				data.council.length !== 0 ||
				data.contry.length !== 0 ||
				data.department.length !== 0 ||
				event.target.value !== "" ||
				data.email.length !== 0 ||
				data.userstatus.length !== 0 ||
				data.invitestatus.length !== 0
			) {
				searchbtn = false
			}
		}
		input[event.target.name] = event.target.value
		this.setState({
			input,
			isSearchBtn: searchbtn,
		})
	}
	handleinvitestatusChange(invitestatus) {
		let searchbtn = true
		let data = this.state
		if (
			data.sport.length !== 0 ||
			data.association.length !== 0 ||
			data.club.length !== 0 ||
			data.council.length !== 0 ||
			data.contry.length !== 0 ||
			data.department.length !== 0 ||
			data.fullname.length !== 0 ||
			data.email.length !== 0 ||
			data.userstatus.length !== 0 ||
			invitestatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ invitestatus: invitestatus, isSearchBtn: searchbtn })
	}
	handledepartmentChange(department) {
		let searchbtn = true
		let data = this.state
		if (
			data.sport.length !== 0 ||
			data.association.length !== 0 ||
			data.club.length !== 0 ||
			data.council.length !== 0 ||
			data.contry.length !== 0 ||
			department.length !== 0 ||
			data.fullname.length !== 0 ||
			data.email.length !== 0 ||
			data.userstatus.length !== 0 ||
			data.invitestatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ department: department, isSearchBtn: searchbtn })
	}
	handleSportChange(sport) {
		let searchbtn = true
		let data = this.state
		if (
			sport.length !== 0 ||
			data.association.length !== 0 ||
			data.club.length !== 0 ||
			data.council.length !== 0 ||
			data.contry.length !== 0 ||
			data.department.length !== 0 ||
			data.fullname.length !== 0 ||
			data.email.length !== 0 ||
			data.userstatus.length !== 0 ||
			data.invitestatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ sport: sport, isSearchBtn: searchbtn })
	}
	handleCouncilChange(council) {
		let searchbtn = true
		let data = this.state
		if (
			data.sport.length !== 0 ||
			data.association.length !== 0 ||
			data.club.length !== 0 ||
			council.length !== 0 ||
			data.contry.length !== 0 ||
			data.department.length !== 0 ||
			data.fullname.length !== 0 ||
			data.email.length !== 0 ||
			data.userstatus.length !== 0 ||
			data.invitestatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ council: council, isSearchBtn: searchbtn })
	}
	handleCountryChange(contry) {
		let searchbtn = true
		let data = this.state
		if (
			data.sport.length !== 0 ||
			data.association.length !== 0 ||
			data.club.length !== 0 ||
			data.council.length !== 0 ||
			contry.length !== 0 ||
			data.department.length !== 0 ||
			data.fullname.length !== 0 ||
			data.email.length !== 0 ||
			data.userstatus.length !== 0 ||
			data.invitestatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ contry: contry, isSearchBtn: searchbtn })
	}
	handleClubChange(club) {
		let searchbtn = true
		let data = this.state
		if (
			data.sport.length !== 0 ||
			data.association.length !== 0 ||
			club.length !== 0 ||
			data.council.length !== 0 ||
			data.contry.length !== 0 ||
			data.department.length !== 0 ||
			data.fullname.length !== 0 ||
			data.email.length !== 0 ||
			data.userstatus.length !== 0 ||
			data.invitestatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ club: club, isSearchBtn: searchbtn })
	}
	handleAssociationChange(association) {
		let searchbtn = true
		let data = this.state
		if (
			data.sport.length !== 0 ||
			association.length !== 0 ||
			data.club.length !== 0 ||
			data.council.length !== 0 ||
			data.contry.length !== 0 ||
			data.department.length !== 0 ||
			data.fullname.length !== 0 ||
			data.email.length !== 0 ||
			data.userstatus.length !== 0 ||
			data.invitestatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ association: association, isSearchBtn: searchbtn })
	}
	handleuserstatusChange(userstatus) {
		let searchbtn = true
		let data = this.state
		if (
			data.sport.length !== 0 ||
			data.association.length !== 0 ||
			data.club.length !== 0 ||
			data.council.length !== 0 ||
			data.contry.length !== 0 ||
			data.department.length !== 0 ||
			data.fullname.length !== 0 ||
			data.email.length !== 0 ||
			userstatus.length !== 0 ||
			data.invitestatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ userstatus: userstatus, isSearchBtn: searchbtn })
	}
	setRef = (ref) => (this.table = ref)
	handlerPagination = (pageNo) => {
		this.setState(
			{
				IncrementPage: pageNo,
			},
			function () {}
		)
		this.fetch(pageNo)
	}
	async fetch(state) {
		let sortbyname = "InvitedDate"
		let orderby = "DESC"
		if (state.sorted !== undefined) {
			if (state.sorted.length !== 0) {
				sortbyname = state.sorted[0].id
				if (state.sorted[0].desc === true) {
					orderby = "DESC"
				}
				if (state.sorted[0].desc === false) {
					orderby = "ASC"
				}
			}
		}
		let pagelimit = 0
		let self = this
		if (state.page === undefined || state.pageSize === undefined) {
			pagelimit = this.state.pageSize
		} else {
			pagelimit = state.pageSize
		}
		let data = {
			PageNo: state > 0 ? state : 1,
			PageSize: pagelimit,
			Name: this.state.fullname,
			Email: this.state.email,
			Department: this.state.department.toString(),
			Invite_Status: this.state.invitestatus.toString(),
			User_Status: this.state.userstatus.toString(),
			SortColumn: sortbyname,
			SortOrder: orderby,
			CountryIDs: this.state.contry.toString(),
			CouncilIDs: this.state.council.toString(),
			CouncilUser: this.state.isCounciltab,
			CommitteeUser: this.state.isCommitteetab,
			AssociationUser: this.state.isAssociationUsertab,
			ClubUser: this.state.isClubUsertab,
			CoachUser: this.state.isCoachUsertab,
			AthleteUser: this.state.isAthleteUsertab,
			AssociationID: this.state.association.toString(),
			CouncilID: this.state.council.toString(),
			ClubID: this.state.club.toString(),
			SportIDs: this.state.sport.toString(),
		}
		self.setState({ loading: true })
		APIRequest.getPostService(APIUrl.INVITED_USER_LIST, data)
			.then((response) => {
				if (response.Data.length > 0) {
					const firstRow = response.Data.find((x) => x)
					self.setState({
						loading: false,
						users: response.Data,
						TotalPages: firstRow.TotalPages,
						TotalRecords: firstRow.TotalRows,
						sortColumn: sortbyname,
						sortValue: orderby,
						exportbtn: false,
						selected: {},
						selectAll: 0,
					})
				} else {
					self.setState({
						exportbtn: true,
					})
				}
				self.setState({ loading: false, users: response.Data })
			})
			.catch((error) => {
				self.setState({ loading: false })
			})
	}

	async componentDidMount() {
		let usertype = this.props.name

		if (usertype === "committee") {
			this.setState({
				isCommitteetab: true,
				isInvitedByCloumn: true,
				isDepartmentCloumn: true,
				exportFilename: "Committee Invited Users List",
			})
			this.state.exportLabel.push({ columnName: "Department", value: "DepartmentName" })
		} else if (usertype === "council") {
			this.setState({
				isCounciltab: true,
				isDepartmentCloumn: true,
				iscouncilCloumn: true,
				exportFilename: "Council Invited Users List",
			})
			this.state.exportLabel.push({ columnName: "Department", value: "DepartmentName" })
			this.state.exportLabel.push({ columnName: "Council Name", value: "CouncilName" })
		} else if (usertype === "association") {
			this.setState({
				isAssociationUsertab: true,
				isAssociationColumn: true,
				exportFilename: "Association Invited Users List",
			})
			this.state.exportLabel.push({ columnName: "Association Name", value: "AssociationName" })
		} else if (usertype === "club") {
			this.setState({
				isClubUsertab: true,
				isClubColumn: true,
				exportFilename: "Club Invited Users List",
			})
			this.state.exportLabel.push({ columnName: "Club Name", value: "ClubName" })
		} else if (usertype === "coach") {
			this.setState({
				isCoachUsertab: true,
				isSportCloumn: true,
				exportFilename: "Coach Invited Users List",
			})
			this.state.exportLabel.push({ columnName: "Sport Name", value: "SportName" })
		} else if (usertype === "athlete") {
			this.setState({
				isAthleteUsertab: true,
				isSportCloumn: true,
				exportFilename: "Athlete Invited Users List",
			})
			this.state.exportLabel.push({ columnName: "Sport Name", value: "SportName" })
		}
		const country = await APIRequest.getGetService(APIUrl.COUNTRY_LIST)
		this.setState({ countries: country.Data })
		const councilList = await APIRequest.getGetService(APIUrl.COUNCIL_LIST)
		this.setState({ councilList: councilList.Data })
		const invitestatue = await APIRequest.getGetService(APIUrl.INVITED_STATUS_LIST)
		this.setState({ invitestatusList: invitestatue.Data })
		const userStatus = await APIRequest.getGetService(APIUrl.INVITED_USER_STATUS_LIST)
		this.setState({ userstatusList: userStatus.Data })
		const departmentlist = await APIRequest.getGetService(APIUrl.DEPARTMENT_LIST)
		this.setState({ departmentList: departmentlist.Data })
		const associationList = await APIRequest.getGetService(APIUrl.ASSOCIATION_LIST)
		this.setState({ associationList: associationList.Data })
		const clubList = await APIRequest.getGetService(APIUrl.CLUB_LIST)
		this.setState({ clubList: clubList.Data })
		const Sport = await APIRequest.getGetService(APIUrl.GET_SPORT_LIST)
		this.setState({ sport_list: Sport.Data })
		document.addEventListener("keydown", this.escFunction, false)
		this.setState({
			fullname: "",
			invitestatus: [],
			email: "",
			userstatus: [],
			department: [],
			contry: [],
			council: [],
			club: [],
			association: [],
			isSearchBtn: true,
			sport: [],
		})
	}
	escFunction(event) {
		if (event.keyCode === 27) {
			//Do whatever when esc is pressed
			this.setState({ isEyeDatapopup: false, isEyeData: [] })
		}
	}
	componentWillUnmount() {
		document.removeEventListener("keydown", this.escFunction, false)
	}
	async toggleResentRow(SerialNo) {
		const newSelected = Object.assign({}, this.state.selectedResendData)
		newSelected[SerialNo] = !this.state.selectedResendData[SerialNo]
		this.setState(
			{
				selectedResendData: newSelected,
				selectAll: 2,
			},
			async function () {
				let flag = 0
				for (const [key, value] of Object.entries(newSelected)) {
					if (value === true) {
						flag = 1
					} else {
						let rowId = this.state.selectedResendData
						let data = rowId[parseInt(key)]
						if (data !== undefined) {
							delete rowId[SerialNo]
						}
					}
				}
				if (flag === 1) {
					this.setState({
						resendbtn: false,
						//deletebtn: false,
					})
				} else {
					this.setState({
						resendbtn: true,
						//deletebtn: true,
					})
				}
				await this.toggleDeleteRow(SerialNo)
			}
		)
	}
	async toggleDeleteRow(SerialNo) {
		const newSelected = Object.assign({}, this.state.selectedDeleteData)
		newSelected[SerialNo] = !this.state.selectedDeleteData[SerialNo]
		this.setState(
			{
				selectedDeleteData: newSelected,
				selectAll: 2,
			},
			await function () {
				let flag = 0
				for (const [key, value] of Object.entries(newSelected)) {
					if (value === true) {
						flag = 1
					} else {
						let rowId = this.state.selectedDeleteData
						let data = rowId[parseInt(key)]
						if (data !== undefined) {
							delete rowId[SerialNo]
						}
					}
				}

				let reesult =
					JSON.stringify(this.state.selectedDeleteData) ===
					JSON.stringify(this.state.selectedResendData)
				if (flag !== 0 && reesult === true) {
					this.setState({
						resendbtn: false,
						deletebtn: false,
					})
				} else if (flag === 1 && reesult === false) {
					this.setState({
						resendbtn: true,
						deletebtn: false,
					})
				} else {
					this.setState({
						resendbtn: true,
						deletebtn: true,
					})
				}
			}
		)
	}

	toggleRow(SerialNo) {
		const newSelected = Object.assign({}, this.state.selected)
		newSelected[SerialNo] = !this.state.selected[SerialNo]
		this.setState(
			{
				selected: newSelected,
				selectAll: 2,
			},
			function () {
				let flag = 0
				for (const [key, value] of Object.entries(newSelected)) {
					if (value === true) {
						flag = 1
					} else {
						let rowId = this.state.selected
						let data = rowId[parseInt(key)]
						if (data !== undefined) {
							delete rowId[SerialNo]
						}
					}
				}
				// if (flag === 1) {
				// 	this.setState({
				// 		resendbtn: false,
				// 		deletebtn: false,
				// 	})
				// } else {
				// 	this.setState({
				// 		resendbtn: true,
				// 		deletebtn: true,
				// 	})
				// }
			}
		)
	}
	async toggleSelectAll() {
		let newSelected = {}
		let newSelectedDelete = {}
		let newSelectedResent = {}
		if (this.state.selectAll === 0 && this.state.users.length !== 0) {
			this.state.users.forEach(async (x) => {
				if (x.InvitedStatus === "Failure" || x.InvitedStatus === "Expired") {
					newSelectedResent[x.SerialNo] = true
					newSelectedDelete[x.SerialNo] = true
				} else if (x.InvitedStatus !== "Registered") {
					newSelectedDelete[x.SerialNo] = true
				}
				//newSelected[x.SerialNo] = true
			})
			let reesult = JSON.stringify(newSelectedDelete) === JSON.stringify(newSelectedResent)
			let flag = true
			if (reesult === true) {
				flag = false
			}
			this.setState({
				resendbtn: flag,
				deletebtn: false,
				selectedResendData: newSelectedResent,
				selectedDeleteData: newSelectedDelete,
			})
		} else {
			this.setState({
				resendbtn: true,
				deletebtn: true,
				selectedResendData: {},
				selectedDeleteData: {},
			})
		}
		this.setState({
			selected: newSelected,
			selectAll: this.state.selectAll === 0 ? 1 : 0,
		})
	}
	handlerRefreshInvitedList = () => {
		this.fetch(1)
	}
	deleteModal = () => {
		if (this.state.isDeleteID === 0) this.handleBtnDelete()
		else handleTblDelete(this.state.isDeleteID, this)
		this.setState({ isDeleteID: 0, isOpen: false })
	}
	closeModal = () => {
		this.setState({ isOpen: false })
	}
	handlerExportData = () => {
		let self = this
		let InputData = {
			PageNo: 1,
			PageSize: self.state.TotalRecords,
			Name: self.state.fullname,
			Email: self.state.email,
			Department: self.state.department.toString(),
			Invite_Status: self.state.invitestatus.toString(),
			User_Status: self.state.userstatus.toString(),
			SortColumn: self.state.sortColumn,
			SortOrder: self.state.sortValue,
			CountryIDs: this.state.contry.toString(),
			CouncilIDs: this.state.council.toString(),
			CouncilUser: this.state.isCounciltab,
			CommitteeUser: this.state.isCommitteetab,
			AssociationUser: this.state.isAssociationUsertab,
			ClubUser: this.state.isClubUsertab,
			CoachUser: this.state.isCoachUsertab,
			AthleteUser: this.state.isAthleteUsertab,
			AssociationID: this.state.association.toString(),
			CouncilID: this.state.council.toString(),
			ClubID: this.state.club.toString(),
			SportIDs: this.state.sport.toString(),
		}
		APIRequest.getPostService(APIUrl.INVITED_USER_LIST, InputData)
			.then((response) => {
				if (response.Data.length > 0) {
					self.setState({
						exportData: response.Data,
						IsExportExcel: true,
					})
				}
			})
			.catch((error) => {})
	}

	//update export excel flag as false
	componentDidUpdate() {
		if (this.state.IsExportExcel) {
			this.setState({
				IsExportExcel: false,
			})
		}
	}

	render() {
		let permissions = this.state.permissions

		const columns = [
			{
				Header: "Name",
				id: "checkbox",
				accessor: "",
				resizable: false,
				Cell: ({ original }) => {
					return (
						<div className='text-center'>
							{original.UserInviteID === original.InvitedByID ? (
								<>
									{original.InvitedStatus === "Failure" || original.InvitedStatus === "Expired" ? (
										<input
											type='checkbox'
											className='checkbox '
											checked={this.state.selectedResendData[original.SerialNo] === true}
											onChange={() => {
												this.toggleRow(original.SerialNo)
												this.toggleResentRow(original.SerialNo)
											}}
										/>
									) : original.InvitedStatus !== "Registered" ? (
										<input
											type='checkbox'
											className='checkbox '
											checked={this.state.selectedDeleteData[original.SerialNo] === true}
											onChange={() => {
												this.toggleRow(original.SerialNo)
												this.toggleDeleteRow(original.SerialNo)
											}}
										/>
									) : (
										<input
											type='checkbox'
											className='checkbox '
											// checked={this.state.selected[original.SerialNo] === true}
											// onChange={() => this.toggleRow(original.SerialNo)}
											disabled={true}
										/>
									)}
								</>
							) : (
								<input
									type='checkbox'
									className='checkbox '
									// checked={this.state.selected[original.SerialNo] === true}
									// onChange={() => this.toggleRow(original.SerialNo)}
									disabled={true}
								></input>
							)}
						</div>
					)
				},
				Header: (x) => {
					return (
						<input
							type='checkbox'
							className='checkbox m-0'
							title='Select All'
							checked={this.state.selectAll === 1}
							ref={(input) => {
								if (input) {
									input.indeterminate = this.state.selectAll === 2
								}
							}}
							onChange={() => this.toggleSelectAll()}
						/>
					)
				},
				sortable: false,
				width: Math.round(window.innerWidth * 0.04),
			},
			{
				Header: () => <div className='text-left'>S.No</div>,
				accessor: "SerialNo",
				sortable: false,
				resizable: false,
				className: "tableheader wordwrap",
				headerClassName: "BoldText ColoredText",

				Cell: (row) => <div className='tabledata text-left'>{row.value}</div>,
				width: Math.round(window.innerWidth * 0.04),
			},

			{
				Header: () => <div className='text-left'>Name & Details</div>,
				accessor: "Name",

				align: "left",
				className: "tableheader wordwrap",
				sortable: true,
				resizable: false,
				// width: Math.round(window.innerWidth * 0.155),
				Cell: (row) => (
					<div className='tabledata col-sm-12 text-center'>
						<div className='row'>
							<div className='4'>
								<img
									style={{
										height: 35,
										borderRadius: "50%",
										textAlign: "center",
										alt: "User Image",
									}}
									src={UserImage}
									alt='User'
								/>
							</div>
							<div className='col-sm-8 text-left' 
							// style={{ fontSize: "smaller" }}
							>
								<span className='grid-username-font'>{row.row.Name}</span>
								<br />
								<div className='tabledata wordwrap' title={row.row.UserRoleName}>
									{this.tableColumnDataLimit(row.row.UserRoleName, 20)}
								</div>
							</div>
						</div>
					</div>
				),
			},

			{
				Header: () => <div className='text-left'>Email</div>,
				//width: Math.round(window.innerWidth * 0.14),
				// width: Math.round(window.innerWidth * (10/100)),
				// maxWidth:Math.round(window.outerWidth * (20/100)),
				//width: "30%",
				resizable: false,
				accessor: "Email",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return (
						<div className='tabledata table_overme' title={row.original.Email}>
							{/* {this.tableColumnDataLimit(row.original.Email,25)} */}
							{row.original.Email}
						</div>
					)
				},
			},
			{
				Header: () => <div className='text-left'>Council</div>,
				width: Math.round(window.innerWidth * 0.14),
				accessor: "CouncilName",
				show: this.state.iscouncilCloumn,
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return (
						<div className='tabledata ' title={row.original.CouncilName}>
							{this.tableColumnDataLimit(row.original.CouncilName, 25)}{" "}
						</div>
					)
				},
			},
			{
				Header: () => <div className='text-left'>Association</div>,
				width: Math.round(window.innerWidth * 0.14),
				accessor: "AssociationName",
				show: this.state.isAssociationColumn,
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return (
						<div className='tabledata ' title={row.original.AssociationName}>
							{this.tableColumnDataLimit(row.original.AssociationName, 25)}
						</div>
					)
				},
			},
			{
				Header: () => <div className='text-left'>Club</div>,
				width: Math.round(window.innerWidth * 0.14),
				accessor: "ClubName",
				show: this.state.isClubColumn,
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return (
						<div className='tabledata ' title={row.original.ClubName}>
							{this.tableColumnDataLimit(row.original.ClubName, 25)}
						</div>
					)
				},
			},
			{
				Header: () => <div className='text-left'>Country</div>,
				accessor: "CountryName",
				show: this.state.isCountryCloumn,
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return (
						<div className='tabledata table_overme' title={row.original.CountryName}>
							{/* //{this.tableColumnDataLimit(row.original.CountryName,25)} */}
							{row.original.CountryName}
						</div>
					)
				},
			},
			{
				Header: () => <div className='text-left'>Department</div>,
				accessor: "DepartmentName",
				//show: this.state.isDepartmentCloumn,
				show: false,
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return <div className='tabledata'>{row.original.DepartmentName}</div>
				},
			},
			{
				Header: () => <div className='text-left'>Sport</div>,
				accessor: "SportName",
				show: this.state.isSportCloumn,
				//show: true,
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return <div className='tabledata'>{row.original.SportName}</div>
				},
			},
			{
				Header: () => <div className='text-left'>Invited By</div>,

				accessor: "InvitedByName",
				show: this.state.isInvitedByCloumn,
				filterable: false,
				width: Math.round(window.innerWidth * 0.09),
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return <div className='tabledata'>{row.original.InvitedByName}</div>
				},
			},
			{
				Header: () => <div className='text-center'>Invite Status</div>,

				accessor: "InvitedStatus",
				width: Math.round(window.innerWidth * 0.09),
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return (
						<div className='text-center'>
							{row.original.InvitedStatus === "Resent" ? (
								<>
									<label className='tabledata  status_Resent'>{row.original.InvitedStatus} </label>
								</>
							) : null}
							{row.original.InvitedStatus === "Sent" ? (
								<>
									<label className='tabledata  status_sent'>{row.original.InvitedStatus} </label>
								</>
							) : null}
							{row.original.InvitedStatus === "Expired" ? (
								<>
									<label className='tabledata status_Expired'>{row.original.InvitedStatus} </label>
								</>
							) : null}
							{row.original.InvitedStatus === "Registered" ? (
								<>
									<label className='tabledata status_Registered'>
										{row.original.InvitedStatus}{" "}
									</label>
								</>
							) : null}
							{row.original.InvitedStatus === "Failure" ? (
								<>
									<label className='tabledata status_Failure'>{row.original.InvitedStatus} </label>
								</>
							) : null}
							{row.original.InvitedStatus === "Pending" ? (
								<>
									<label className='tabledata status_Pending'>{row.original.InvitedStatus} </label>
								</>
							) : null}
						</div>
					)
				},
			},
			{
				Header: () => <div className=''>User Status</div>,

				accessor: "UserStatus",
				show: false,
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return <div className='tabledata'>{row.original.UserStatus}</div>
				},
			},
			{
				Header: () => <div className='text-center'> Expires in (Days)</div>,
				accessor: "ExpiryDay",
				filterable: false,
				width: Math.round(window.innerWidth * 0.11),
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return <div className='tabledata text-center'>{row.original.ExpiryDay}</div>
				},
			},
			{
				Header: () => <div className=''>UserRoleName</div>,
				show: false,
				accessor: "UserRoleName",
			},
			{
				Header: () => <div className='text-center'>Invited Date</div>,

				accessor: "InvitedDate",
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				width: Math.round(window.innerWidth * 0.09),
				Cell: (row) => {
					return <div className='tabledata  text-center'>{row.original.InvitedDate}</div>
				},
			},
			{
				Header: () => <div className=''>Expiry Date</div>,
				accessor: "ExpiryDate",
				show: false,
			},
			{
				Header: () => <div className=' text-center'>{"     "}Action</div>,
				accessor: "Action",
				className: "tableheader wordwrap",
				sortable: false,
				resizable: false,
				width: Math.round(window.innerWidth * 0.07),
				filterable: false,
				Cell: (row) => {
					return (
						<div className='tabledata text-center'>
							{row.original.UserInviteID === row.original.InvitedByID ? (
								<>
									{row.original.InvitedStatus === "Failure" ||
									row.original.InvitedStatus === "Expired" ? (
										<>
											{permissions.includes(Permission.C_COUNCIL_U_RESEND) ||
											permissions.includes(Permission.C_COACH_U_RESEND) ||
											permissions.includes(Permission.C_CLUB_U_RESEND) ||
											permissions.includes(Permission.C_ATHLETE_U_RESEND) ||
											permissions.includes(Permission.C_ASSOCIATION_U_RESEND) ||
											permissions.includes(Permission.C_COMMITTEE_U_RESENT) ? (
												<span>
													<img
														alt=''
														className='mr-2'
														style={{ border: 3, height: 16, width: 16, marginTop: "-4px" }}
														onClick={() => handleResend(row.original.InvitedUserID, this)}
														title='Resend'
														src={Resend}
													/>
												</span>
											) : null}
										</>
									) : null}
									{row.original.InvitedStatus === "Sent" ? (
										<>
											{permissions.includes(Permission.C_COMMITTEE_U_EXPIRY_DATE) ||
											permissions.includes(Permission.C_ASSOCIATION_U_DEADLINE) ||
											permissions.includes(Permission.C_ATHLETE_U_DEADLINE) ||
											permissions.includes(Permission.C_CLUB_U_DEADLINE) ||
											permissions.includes(Permission.C_COACH_U_DEADLINE) ||
											permissions.includes(Permission.C_COUNCIL_U_DEADLINE) ? (
												<FontAwesomeIcon
													title='Expires in'
													className='mr-2 icon_color'
													onClick={() => openDatePickerModal(row.original, this)}
													icon={faCalendarCheck}
													style={{ cursor: "pointer" }}
												/>
											) : null}
										</>
									) : null}
									{permissions.includes(Permission.C_COUNCIL_U_VIEW) ||
									permissions.includes(Permission.C_COMMITTEE_U_VIEW) ||
									permissions.includes(Permission.C_COACH_U_VIEW) ||
									permissions.includes(Permission.C_CLUB_U_VIEW) ||
									permissions.includes(Permission.C_ATHLETE_U_VIEW) ||
									permissions.includes(Permission.C_ASSOCIATION_U_VIEW) ? (
										<FontAwesomeIcon
											title='View All details'
											className='mr-2 icon_eye'
											onClick={() => openViewAlldataModal(row.original, this)}
											icon={faEye}
											style={{ cursor: "pointer" }}
										/>
									) : null}
									{row.original.InvitedStatus !== "Registered" ? (
										<>
											{permissions.includes(Permission.C_COMMITTEE_U_DELETE) ||
											permissions.includes(Permission.C_COUNCIL_U_DELETE) ||
											permissions.includes(Permission.C_COACH_U_DELETE) ||
											permissions.includes(Permission.C_CLUB_U_DELETE) ||
											permissions.includes(Permission.C_ATHLETE_U_DELETE) ||
											permissions.includes(Permission.C_ASSOCIATION_U_DELETE) ? (
												<FontAwesomeIcon
													title='Delete'
													className=' icon_Delete'
													onClick={() => openModal(row.original.InvitedUserID, this)}
													icon={faTrashAlt}
													style={{ cursor: "pointer" }}
												/>
											) : null}
										</>
									) : null}
								</>
							) : null}
						</div>
					)
				},
			},
		]

		const { invitestatusList, userstatusList, departmentList, councilList, isEyeData } = this.state

		return (
			<>
				<div className=' pt-10'>
					<Spin size='large' spinning={this.state.loading} tip='Loading...'>
						<div className=' d-flex justify-content-start pt-1 '>
							<p className='tab_form_content'>Invited Users</p>
						</div>
						<Card className='w-100 card from-group'>
							<Card.Header
								className='text-white pl-1'
								onClick={() => setIsExpanded(!this.state.IsExpanded, this)}
							>
								<div className='float-left filter-heading-font'>Search Invited Users </div>
								<div className='float-right'>
									{this.state.Icons ? (
										<FontAwesomeIcon
											color=' #FFFFFF'
											className='action-icon-font'
											icon={faArrowUp}
											style={{ cursor: "pointer" }}
										></FontAwesomeIcon>
									) : (
										<FontAwesomeIcon
											color=' #FFFFFF'
											className='action-icon-font'
											icon={faArrowDown}
											style={{ cursor: "pointer" }}
										></FontAwesomeIcon>
									)}
								</div>
							</Card.Header>
							<Collapse in={this.state.IsExpanded}>
								<Card.Body>
									<div className='row'>
										<div className='form-group col-sm-4'>
											<label className='pl-0'>Full Name</label>
											<div>
												<Input
													size='large'
													autoComplete='off'
													type='text'
													name='fullname'
													allowClear={true}
													bordered={false}
													value={this.state.fullname}
													onChange={this.handleSetdata}
													className='textfiled  pl-0'
													placeholder='Full Name'
												></Input>
											</div>
										</div>
										<div className='form-group col-sm-4'>
											<label className='pl-0'>Email</label>
											<div>
												<Input
													size='large'
													autoComplete='off'
													type='text'
													name='email'
													allowClear={true}
													//allowClear={true}
													value={this.state.email}
													bordered={false}
													onChange={this.handleSetdata}
													className='textfiled  pl-0'
													placeholder='Email'
												></Input>
											</div>
										</div>
										{this.state.isAssociationColumn ? (
											<>
												<div className='form-group col-sm-4'>
													<label className='pl-0'>Association</label>
													<Select
														className='textfiled '
														mode='multiple'
														maxTagCount="responsive"
														bordered={false}
														showArrow='true'
														placeholder='Select Association'
														name='association'
														value={this.state.association}
														allowClear={true}
														size={"large"}
														onChange={this.handleAssociationChange.bind(this)}
														optionFilterProp='children'
														filterSort={(Associationdata, Associationvalue) =>
															Associationdata.children
																.toLowerCase()
																.localeCompare(Associationvalue.children.toLowerCase())
														}
													>
														{this.state.associationList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
												</div>
											</>
										) : null}
										{this.state.isClubColumn ? (
											<>
												<div className='form-group col-sm-4'>
													<label className='pl-0'>Club</label>
													<Select
														className='textfiled '
														mode='multiple'
														maxTagCount="responsive"
														showArrow='true'
														bordered={false}
														style={{ width: "100%" }}
														placeholder='Select Club'
														name='club'
														allowClear={true}
														value={this.state.club}
														size={"large"}
														onChange={this.handleClubChange.bind(this)}
														optionFilterProp='children'
														filterSort={(Clubdata, Clubvalue) =>
															Clubdata.children
																.toLowerCase()
																.localeCompare(Clubvalue.children.toLowerCase())
														}
													>
														{this.state.clubList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
												</div>
											</>
										) : null}
										{this.state.isCountryCloumn ? (
											<>
												<div className='form-group col-sm-4'>
													<label className='pl-0'>Country</label>
													<Select
														className='textfiled '
														mode='multiple'
														maxTagCount="responsive"
														showArrow='true'
														style={{ width: "100%" }}
														placeholder='Select Country'
														bordered={false}
														name='country'
														allowClear={true}
														value={this.state.contry}
														size={"large"}
														onChange={this.handleCountryChange.bind(this)}
														optionFilterProp='children'
														filterSort={(contrydata, contryvalue) =>
															contrydata.children
																.toLowerCase()
																.localeCompare(contryvalue.children.toLowerCase())
														}
													>
														{this.state.countries.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
												</div>
											</>
										) : null}
										{this.state.isDepartmentCloumn ? (
											<>
												<div className='form-group col-sm-4'>
													<label className='pl-0'>Department</label>
													<Select
														className='textfiled '
														mode='multiple'
														style={{ width: "100%" }}
														bordered={false}
														showArrow='true'
														maxTagCount="responsive"
														// maxTagTextLength={10}
														placeholder='Select Department'
														allowClear={true}
														name='department'
														value={this.state.department}
														size={"large"}
														//   defaultValue={["a10", "c12"]}
														onChange={this.handledepartmentChange.bind(this)}
														optionFilterProp='children'
														filterSort={(optionA, optionB) =>
															optionA.children
																.toLowerCase()
																.localeCompare(optionB.children.toLowerCase())
														}
													>
														{/* {children} */}
														{departmentList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
												</div>
											</>
										) : null}
										{this.state.iscouncilCloumn ? (
											<>
												<div className='form-group col-sm-4'>
													<label className='pl-0'>Council</label>
													<Select
														//suffixIcon={<DownCircleTwoTone  />}
														mode='multiple'
														maxTagCount="responsive"
														showArrow='true'
														className='textfiled '
														style={{ width: "100%" }}
														bordered={false}
														placeholder='Select Council'
														name='Council'
														allowClear={true}
														value={this.state.council}
														size={"large"}
														onChange={this.handleCouncilChange.bind(this)}
														optionFilterProp='children'
														filterSort={(optionA, optionB) =>
															optionA.children
																.toLowerCase()
																.localeCompare(optionB.children.toLowerCase())
														}
													>
														{councilList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
												</div>
											</>
										) : null}
										{this.state.isSportCloumn ? (
											<>
												<div className='form-group col-sm-4'>
													<label className='pl-0'>Sport</label>

													<Select
														className='textfiled'
														mode='multiple'
														style={{ width: "100%" }}
														bordered={false}
														maxTagCount="responsive"
														showArrow='true'
														placeholder='Select Sport'
														allowClear={true}
														name='sport'
														value={this.state.sport}
														size={"large"}
														//   defaultValue={["a10", "c12"]}
														onChange={this.handleSportChange.bind(this)}
														optionFilterProp='children'
														filterSort={(optionA, optionB) =>
															optionA.children
																.toLowerCase()
																.localeCompare(optionB.children.toLowerCase())
														}
													>
														{/* {children} */}
														{this.state.sport_list.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
												</div>
											</>
										) : null}
										<div className='form-group col-sm-4'>
											<label className='pl-0'>Invite Status</label>
											<Select
												mode='multiple'
												maxTagCount="responsive"
												// maxTagTextLength={10}
												className='textfiled '
												style={{ width: "100%" }}
												name='invitestatus'
												showArrow='true'
												value={this.state.invitestatus}
												bordered={false}
												size={"large"}
												allowClear={true}
												showSearch
												placeholder='Select Invite Status'
												optionFilterProp='children'
												onChange={this.handleinvitestatusChange.bind(this)}
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{invitestatusList.map((c) => (
													<Option value={c.OptionID}>{c.OptionData}</Option>
												))}
												{/* {optionItems} */}
											</Select>
										</div>
										<div className='form-group hidden col-sm-4'>
											<label className='pl-0'>User Status </label>
											<br />
											<Select
												mode='multiple'
												suffixIcon={<FontAwesomeIcon icon={faArrowDown} />}
												className='textfiled '
												maxTagCount="responsive"
												showArrow='true'
												style={{ width: "100%" }}
												placeholder='Select User Status'
												bordered={false}
												allowClear={true}
												name='userstatus'
												size={"large"}
												value={this.state.userstatus}
												onChange={this.handleuserstatusChange.bind(this)}
												optionFilterProp='children'
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{userstatusList.map((c) => (
													<Option value={c.OptionID}>{c.OptionData}</Option>
												))}
											</Select>
										</div>
									</div>
									<div className='col-sm-12 '>
										<div className=' d-flex justify-content-end row'>
										<Button
										 onClick={this.handleClear}
										 className='mr-2 btn_green'>
												Clear
											</Button>
											<Button
												onClick={this.fetch}
												className='btn_green'
												disabled={this.state.isSearchBtn}
											>
												Search
											</Button>
											
										</div>
									</div>
								</Card.Body>
							</Collapse>
						</Card>
						{this.state.isCounciltab ||
						this.state.isCommitteetab ||
						this.state.isAssociationUsertab ||
						this.state.isClubUsertab ||
						this.state.isCoachUsertab ||
						this.state.isAthleteUsertab ? (
							<div className='admin-report-table pt-5'>
								<ReactTable
									data={this.state.users}
									columns={columns}
									showPaginationTop={false}
									showPaginationBottom={false}
									minRows={0}
									defaultPageSize={10}
									defaultPage={1}
									onFetchData={this.fetch}
									ref={this.table}
									sortable={true}
									multiSort={true}
									manual
									className={"CosmicTable"}
									showPageSizeOptions={false}
									NoDataComponent={NoDataConst}
									defaultSorting={[
										{
											id: "id",
											desc: true,
										},
									]}
									manualPagination={true}
								/>
							</div>
						) : null}
						<div className='mt-1'>
						<label>
							<b>Total Records: </b>
							{this.state.TotalRecords > 10
								? this.state.users.length + " of " + this.state.TotalRecords
								: this.state.TotalRecords !== 0
								? this.state.TotalRecords
								: 0}
						</label>
						</div>
						<div className='pt-3 row'>
							<div className=' col-sm-6'>
								{permissions.includes(Permission.C_ATHLETE_U_EXPORT_EXCEL) ||
								permissions.includes(Permission.C_CLUB_U_EXPORT_EXCEL) ||
								permissions.includes(Permission.C_COACH_U_EXPORT_EXCEL) ||
								permissions.includes(Permission.C_COUNCIL_U_EXPORT_EXCEL) ||
								permissions.includes(Permission.C_ASSOCIATION_U_EXPORT_EXCEL) ||
								permissions.includes(Permission.C_COMMITTEE_U_EXPORT_EXCEL) ? (
									<Button
										className='mr-2 btn_green'
										style={{
											//paddingRight: "0.4rem", paddingLeft: "0.4rem",
											cursor: "pointer",
										}}
										disabled={this.state.exportbtn}
										onClick={this.handlerExportData}
									>
										<FontAwesomeIcon
											title='Delete'
											color={"#FFFFFF"}
											icon={faDownload}
											// marginRight={"5px !important"}
										></FontAwesomeIcon>{" "}
										Export Excel
									</Button>
								) : null}
								{permissions.includes(Permission.C_COUNCIL_U_RESEND) ||
								permissions.includes(Permission.C_COACH_U_RESEND) ||
								permissions.includes(Permission.C_CLUB_U_RESEND) ||
								permissions.includes(Permission.C_ATHLETE_U_RESEND) ||
								permissions.includes(Permission.C_ASSOCIATION_U_RESEND) ||
								permissions.includes(Permission.C_COMMITTEE_U_RESENT) ? (
									<Button
										onClick={this.handleBtnResend}
										className=' btn btn-primary btn_green  waves-effect waves-light btn-xs  mr-2  '
										disabled={this.state.resendbtn}
									>
										<FontAwesomeIcon
											title='Delete'
											color={"#FFFFFF"}
											icon={faShare}
										></FontAwesomeIcon>{" "}
										Resend
									</Button>
								) : null}
								{permissions.includes(Permission.C_COMMITTEE_U_DELETE) ||
								permissions.includes(Permission.C_COUNCIL_U_DELETE) ||
								permissions.includes(Permission.C_COACH_U_DELETE) ||
								permissions.includes(Permission.C_CLUB_U_DELETE) ||
								permissions.includes(Permission.C_ATHLETE_U_DELETE) ||
								permissions.includes(Permission.C_ASSOCIATION_U_DELETE) ? (
									<Button
										onClick={() => openModal(0, this)}
										//onClick={this.handleBtnDelete}
										disabled={this.state.deletebtn}
										className='btn btn-primary btn_green   waves-effect waves-light btn-xs  '
									>
										<FontAwesomeIcon
											title='Delete'
											color={"#FFFFFF"}
											icon={faTrashAlt}
										></FontAwesomeIcon>{" "}
										Delete
									</Button>
								) : null}
							</div>
							<div className='col-sm-6 d-flex justify-content-end'>
								<Pagination
									totalPages={this.state.TotalPages}
									totalCount={this.state.TotalCount}
									totalRecords={this.state.TotalRecords}
									paginationCall={this.handlerPagination}
								/>
							</div>
						</div>
						<Fragment>
							<Modal
								size='sm'
								//aria-labelledby="contained-modal-title-vcenter"
								//   aria-labelledby="contained-modal-title-vcenter"
								backdrop='static'
								//  centered
								show={this.state.isOpen}
								onHide={this.closeModal}
							>
								<div className='popup-content'>
									<div className='deletecircle'></div>
									<i className='fa fa-trash-o deleteicon'></i>
									<Modal.Body>
										<p className='pull-left' style={{ margin: "4rem 2rem" }}>
											{" "}
											<h5>{Message.INVITE_DELETE}</h5>
											<h6 style={{ color: "darkgray" }}>{Message.INVITE_DELETE_CONFIRM}</h6>
											<h6
												style={{
													color: "darkgray",
													marginRight: "7rem",
													marginBottom: "-3rem",
												}}
											>
												{Message.INVITE_DELETE_CONFIRM1}
											</h6>
										</p>
										<br />
										<div className='pull-right'>
											<Button className='btn_cancel mr-2' onClick={this.closeModal}>
												No
											</Button>
											<Button
												className='btn_green'
												style={{ height: "2rem", padding: "5px 10px" }}
												onClick={this.deleteModal}
											>
												Yes
											</Button>
										</div>
									</Modal.Body>
								</div>
							</Modal>
						</Fragment>
						{this.state.IsExportExcel ? (
							<ExportData
								data={this.state.exportData}
								label={this.state.exportLabel}
								filename={this.state.exportFilename}
							/>
						) : null}

						<>
							<Modal
								size='md'
								//aria-labelledby="contained-modal-title-vcenter"
								//   aria-labelledby="contained-modal-title-vcenter"
								backdrop='static'
								//  centered
								show={this.state.isDatepickerpopup}
								onHide={this.DatepickerpopupClose}
							>
								<Modal.Header className=''>
									<div className='col-sm-12'>
										<div className='row'>
											<div className='col-sm-11'>
												<div className='d-flex justify-content-center'>
													<h5 className='font-weight-bold text-white mb-0'>
														{"Reset Expiry Date"}
													</h5>
												</div>
											</div>
											<div className='col-sm-1'>
												<div className='d-flex justify-content-end'>
													<button
														type='button'
														className='close text-white'
														onClick={this.DatepickerpopupClose}
														data-dismiss='modal'
														aria-label='Close'
													>
														<span aria-hidden='true'>&times;</span>
													</button>{" "}
												</div>
											</div>
										</div>
									</div>
								</Modal.Header>
								<Modal.Body>
									<div className='col-sm-12'>
										<div className='row form-group'>
											<div className='col-sm-6 '>
												<div className={""}>
													<Space direction='vertical' size={2}>
														<label className='ant_text_pleace'> Current Expiry Date </label>
														<DatePicker
															size='large'
															format='DD MMM YYYY'
															disabled={true}
															bordered={false}
															className='myDatePicker datepickerfiled '
															defaultValue={moment(this.state.isDiabledDate)}
														/>
													</Space>
												</div>
											</div>
											<div className='col-sm-6 '>
												<div className={""}>
													<Space direction='vertical' size={1}>
														<label className='ant_text_pleace'>Extend Expiry Date</label>
														<DatePicker
															size='large'
															format='DD MMM YYYY'
															className='myDatePicker datepickerfiled'
															bordered={false}
															defaultValue={moment(this.state.isDiabledDate)}
															showToday={true}
															allowClear={false}
															disabledDate={this.DynamicdisabledDate}
															name='isExpirtyDate'
															onChange={this.handleDatePickerChange.bind(null)}
															placeholder='Select extend expiry date'
														/>
													</Space>
												</div>
											</div>
										</div>
									</div>
								</Modal.Body>
								<Modal.Footer className='m-0 py-0 '>
									<div className='col-sm-3'>
										<div className='row  d-flex justify-content-right'>
											{this.state.isExpirtyError !== undefined && this.state.isExpirtyError > 0 && (
												<span className='error danger_msg'>{this.state.isExpirtyError}</span>
											)}
											<Button className='btn_green mr-2' onClick={this.DatepickerpopupClose}>
												Close
											</Button>
											<Button
												className='btn_green'
												disabled={this.state.isSubmitBtn}
												onClick={this.ExpirtyDateSubmite}
											>
												Submit
											</Button>
											
										</div>
									</div>
								</Modal.Footer>
							</Modal>
						</>
					</Spin>
					<div>
						<Modal // style={{width:"40%"}}
							//aria-labelledby="contained-modal-title-vcenter"
							//   aria-labelledby="contained-modal-title-vcenter"
							backdrop='static'
							size='lg'
							//className='popupborder'
							keyboard={true}
							show={this.state.isEyeDatapopup}
							onHide={this.viewAlldatapopupClose} //toggle={props.onCloseModal}
							aria-labelledby='contained-modal-title-vcenter'
							//centered
						>
							<Modal.Header className=''>
								<div className='col-sm-12'>
									<div className='row'>
										<div className='col-sm-11'>
											<div className='d-flex justify-content-left'>
												<h5 className='font-weight-bold text-white mb-0'>{"User Details"}</h5>
											</div>
										</div>
										<div className='col-sm-1'>
											<div className='d-flex justify-content-end'>
												<button
													type='button'
													className='close text-white'
													onClick={this.viewAlldatapopupClose}
													data-dismiss='modal'
													aria-label='Close'
												>
													<span aria-hidden='true'>&times;</span>
												</button>{" "}
											</div>
										</div>
									</div>
								</div>
							</Modal.Header>
							<Modal.Body className=''>
								{/* <IntlMessages id="modal.delete.user.description" /> */}
								<div className='col-sm-12'>
									<div className='row pt-10'>
										<div className='col-sm-1'>{""}</div>
										<div className='col-sm-10'>
											<div className='form-group row'>
												<div className='col-sm-5'>
													{" "}
													<label className='font-weight-bold popup_txt'>Full Name</label>{" "}
												</div>
												<div className='col-sm-7'>
													{" "}
													<label>{isEyeData.Name}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-5'>
													{" "}
													<label className='font-weight-bold popup_txt'>Email</label>{" "}
												</div>
												<div className='col-sm-7'>
													{" "}
													<label>{isEyeData.Email}</label>{" "}
												</div>
											</div>
											{this.state.isAssociationColumn ? (
												<>
													<div className='form-group row'>
														<div className='col-sm-5'>
															{" "}
															<label className='font-weight-bold popup_txt'>Association</label>{" "}
														</div>
														<div className='col-sm-7'>
															{" "}
															<label>{isEyeData.AssociationName}</label>{" "}
														</div>
													</div>
												</>
											) : null}
											{this.state.iscouncilCloumn ? (
												<>
													<div className='form-group row'>
														<div className='col-sm-5'>
															{" "}
															<label className='font-weight-bold popup_txt'>Council</label>{" "}
														</div>
														<div className='col-sm-7'>
															{" "}
															<label>{isEyeData.CouncilName}</label>{" "}
														</div>
													</div>
												</>
											) : null}
											{this.state.isClubColumn ? (
												<>
													<div className='form-group row'>
														<div className='col-sm-5'>
															{" "}
															<label className='font-weight-bold popup_txt'>Club</label>{" "}
														</div>
														<div className='col-sm-7'>
															{" "}
															<label>{isEyeData.ClubName}</label>{" "}
														</div>
													</div>
												</>
											) : null}
											{this.state.isDepartmentCloumn ? (
												<>
													<div className='form-group row'>
														<div className='col-sm-5'>
															{" "}
															<label className='font-weight-bold popup_txt'>Department</label>{" "}
														</div>
														<div className='col-sm-7'>
															{" "}
															<label>{isEyeData.DepartmentName}</label>{" "}
														</div>
													</div>
												</>
											) : null}
											{this.state.isSportCloumn ? (
												<>
													<div className='form-group row'>
														<div className='col-sm-5'>
															{" "}
															<label className='font-weight-bold popup_txt'>Sport</label>{" "}
														</div>
														<div className='col-sm-7'>
															{" "}
															<label>{isEyeData.SportName}</label>{" "}
														</div>
													</div>
												</>
											) : null}
											<div className='form-group row'>
												<div className='col-sm-5'>
													{" "}
													<label className='font-weight-bold popup_txt'>Invited By </label>{" "}
												</div>
												<div className='col-sm-7'>
													{" "}
													<label>{isEyeData.InvitedByName}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-5'>
													{" "}
													<label className='font-weight-bold popup_txt'>Invited Date</label>{" "}
												</div>
												<div className='col-sm-7'>
													{" "}
													<label>{isEyeData.InvitedDate}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-5'>
													{" "}
													<label className='font-weight-bold popup_txt'>Invite Status</label>{" "}
												</div>
												<div className='col-sm-7'>
													{isEyeData.InvitedStatus === "Resent" ? (
														<>
															{" "}
															<label className='status_Resent'> {isEyeData.InvitedStatus}</label>
														</>
													) : null}
													{isEyeData.InvitedStatus === "Sent" ? (
														<>
															{" "}
															<label className='status_sent'> {isEyeData.InvitedStatus}</label>
														</>
													) : null}
													{isEyeData.InvitedStatus === "Expired" ? (
														<>
															{" "}
															<label className='status_Expired'> {isEyeData.InvitedStatus}</label>
														</>
													) : null}
													{isEyeData.InvitedStatus === "Registered" ? (
														<>
															{" "}
															<label className='status_Registered'>
																{" "}
																{isEyeData.InvitedStatus}
															</label>
														</>
													) : null}
													{isEyeData.InvitedStatus === "Failure" ? (
														<>
															{" "}
															<label className='status_Failure'> {isEyeData.InvitedStatus}</label>
														</>
													) : null}
													{isEyeData.InvitedStatus === "Pending" ? (
														<>
															{" "}
															<label className='status_Pending'> {isEyeData.InvitedStatus}</label>
														</>
													) : null}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-5'>
													{" "}
													<label className='font-weight-bold popup_txt'>Expiry Date</label>{" "}
												</div>
												<div className='col-sm-7'>
													{" "}
													<label>{isEyeData.ExpiryDate}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-5'>
													{" "}
													<label className='font-weight-bold popup_txt'>
														Expires in (Days)
													</label>{" "}
												</div>
												<div className='col-sm-7'>
													{" "}
													<label>{isEyeData.ExpiryDay}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-5'>
													{" "}
													<label className='font-weight-bold popup_txt'>Role </label>{" "}
												</div>
												<div className='col-sm-7'>
													{" "}
													<label>{isEyeData.UserRoleName}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-5'>
													{" "}
													<label className='font-weight-bold popup_txt'>User Type</label>{" "}
												</div>
												<div className='col-sm-7'>
													{" "}
													<label>{isEyeData.UserType}</label>{" "}
												</div>
											</div>
										</div>
									</div>
								</div>
							</Modal.Body>
							{/* <Modal.Footer className='m-0 py-0 '>
								<div className='col-sm-12'>
									<div className='row  d-flex justify-content-center'>
										<Button className='btn_green' onClick={this.viewAlldatapopupClose}>
											Close
										</Button>
									</div>
								</div>
							</Modal.Footer> */}
						</Modal>
					</div>
				</div>
			</>
		)
	}
}

const mapStateToProps = (state) => ({
	user: state.userReducer,
})

//export default connect(mapStateToProps, null,null)(UserInviteList)
export default connect(mapStateToProps, null, null, { forwardRef: true })(UserInviteList)
