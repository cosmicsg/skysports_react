import React, { Component } from "react"
import { Tabs, Tab, Row, Col, Container } from "react-bootstrap"
import ManageUserInviteList from "./InvitedUsers"
import ManageAddInviteUser from "./AddUser"
import "../../../styles/grid.css"
import { Spin } from "antd"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faGolfBall, faRunning, faUserFriends, faUsers, } from "@fortawesome/free-solid-svg-icons"
import { Permissions as Permission, Permissions, Roles } from "../../../permissions/AppPermissions"
import { connect } from "react-redux"

//It is manage user all tab control
class CreateUser extends Component {
	constructor(props) {
		super(props)

		const { userPermissions, userRoles } = this.props.user
		let permissions =
			userPermissions !== undefined && userPermissions !== null ? userPermissions : []
		permissions = Array.isArray(permissions) ? permissions : [permissions]

		let roles = userRoles !== undefined && userRoles !== null ? userRoles : []
		roles = Array.isArray(userRoles) ? userRoles : [userRoles]

		let tabKey = "";

		if (permissions.includes(Permissions.C_ATHLETE_U_VIEW)) {
			tabKey = "athlete"
		}
		if (permissions.includes(Permissions.C_COACH_U_VIEW)) {
			tabKey = "coach"
		}
		if (permissions.includes(Permissions.C_CLUB_U_VIEW)) {
			tabKey = "club"
		}
		if (permissions.includes(Permissions.C_ASSOCIATION_U_VIEW)) {
			tabKey = "association"
		}
		if (permissions.includes(Permissions.C_COUNCIL_U_VIEW)) {
			tabKey = "council"
		}
		if (permissions.includes(Permissions.C_COMMITTEE_U_VIEW)) {
			tabKey = "committee"
		}
		this.state = {
			name: "",
			key: tabKey,
			iscouncil: false,
			iscommittee: true,
			isassociation: false,
			iscoach: false,
			isclub: false,
			isAthlete: false,
			loading: false,
			permissions: permissions,
			roles: roles,
		}

		this.childRef = React.createRef()
		this.handelTabChange = this.handelTabChange.bind(this)
	}
	//It is used to refresh invited user list page
	handlerRefreshInvitedList = () => {
		//This below function is used to call the Invited User list
		this.childRef.current.handlerRefreshInvitedList()
	}
	// It is used to over all loader start
	handlerLoadingStart = () => {
		this.setState({ loading: true })
	}
	// It is used to over all loader stop
	handlerLoadingStop = () => {
		this.setState({ loading: false })
	}
	//It is used to tab data change and  tab list data show
	handelTabChange = (key) => {
		if (key === "committee") {
			this.setState({
				name: key,
				key: key,
				iscouncil: false,
				iscommittee: true,
				loading: false,
				isAthlete: false,
			})
		} else if (key === "council") {
			this.setState({
				name: key,
				key: key,
				iscouncil: true,
				iscommittee: false,
				isassociation: false,
				isclub: false,
				iscoach: false,
				loading: false,
				isAthlete: false,
			})
		} else if (key === "association") {
			this.setState({
				name: key,
				key: key,
				iscouncil: false,
				iscommittee: false,
				isassociation: true,
				isclub: false,
				iscoach: false,
				loading: false,
				isAthlete: false,
			})
		} else if (key === "club") {
			this.setState({
				name: key,
				key: key,
				iscouncil: false,
				iscommittee: false,
				isassociation: false,
				isclub: true,
				iscoach: false,
				loading: false,
				isAthlete: false,
			})
		} else if (key === "coach") {
			this.setState({
				name: key,
				key: key,
				iscouncil: false,
				iscommittee: false,
				isassociation: false,
				isclub: false,
				loading: false,
				iscoach: true,
				isAthlete: false,
			})
		} else if (key === "athlete") {
			this.setState({
				name: key,
				key: key,
				iscouncil: false,
				iscommittee: false,
				isassociation: false,
				isclub: false,
				loading: false,
				iscoach: false,
				isAthlete: true,
			})
		}
	}
	componentDidMount = () => {
		let permissions = this.state.permissions
		if (permissions.includes(Permissions.C_COMMITTEE_U_VIEW)) {
			this.handelTabChange("committee");
		} else if (permissions.includes(Permissions.C_COUNCIL_U_VIEW)) {
			this.handelTabChange("council");
		} else if (permissions.includes(Permissions.C_ASSOCIATION_U_VIEW)) {
			this.handelTabChange("association");
		} else if (permissions.includes(Permissions.C_CLUB_U_VIEW)) {
			this.handelTabChange("club");
		} else if (permissions.includes(Permissions.C_COACH_U_VIEW)) {
			this.handelTabChange("coach");
		} else if (permissions.includes(Permissions.C_ATHLETE_U_VIEW)) {
			this.handelTabChange("athlete");
		}
	}
	render() {
		let userRoles = this.state.roles
		let permissions = this.state.permissions

		return (
			<>
				<Container fluid className='h-full'>
					<Spin size='large' spinning={this.state.loading} tip='Loading...'>
						<Row className='h-full'>
							<Col className='h-full'>
								<Tabs
									defaultActiveKey={this.state.key}
									id='controlled-tab-example'
									className='list-tab'
									onSelect={this.handelTabChange}
								>
									{permissions.includes(Permission.C_COMMITTEE_U_VIEW) ? (
										<Tab
											eventKey='committee'
											title={
												<span>
													<FontAwesomeIcon
														title='Committee'
														className='mr-2'
														size={"lg"}
														icon={faUsers}
													></FontAwesomeIcon>
													Committee
												</span>
											}
											name={this.state.name}
										>
											{this.state.iscommittee ? (
												<>
													{permissions.includes(Permission.CU_VIEW) ? (
														<ManageAddInviteUser
															refreshInvitedListFunc={this.handlerRefreshInvitedList}
															isLoadingStopFunc={this.handlerLoadingStop}
															isLoadingStartFunc={this.handlerLoadingStart}
															name='committee'
														/>
													) : null}
													{permissions.includes(Permission.C_COMMITTEE_U_VIEW) ? (
														<ManageUserInviteList ref={this.childRef} name='committee' />
													) : null}
												</>
											) : null}
										</Tab>
									) : null}
									{permissions.includes(Permission.C_COUNCIL_U_VIEW) ? (
										<Tab
											eventKey='council'
											title={
												<span>
													<FontAwesomeIcon
														title='council'
														className='mr-2'
														size={"lg"}
														icon={faUsers}
													></FontAwesomeIcon>
													Council
												</span>
											}
										>
											{""}{" "}
											{this.state.iscouncil ? (
												<>
													{permissions.includes(Permission.CU_VIEW) ? (
														<ManageAddInviteUser
															refreshInvitedListFunc={this.handlerRefreshInvitedList}
															isLoadingStopFunc={this.handlerLoadingStop}
															isLoadingStartFunc={this.handlerLoadingStart}
															name='council'
														/>
													) : null}
													{permissions.includes(Permission.C_COUNCIL_U_VIEW) ? (
														<ManageUserInviteList ref={this.childRef} name='council' />
													) : null}
												</>
											) : null}
										</Tab>
									) : null}
									{permissions.includes(Permission.C_ASSOCIATION_U_VIEW) ? (
										<Tab
											eventKey='association'

											title={
												<span>
													<FontAwesomeIcon
														title='Association'
														className='mr-2'
														size={"lg"}
														icon={faUserFriends}
													></FontAwesomeIcon>
													Association
												</span>
											}
										>
											{this.state.isassociation ? (
												<>
													{permissions.includes(Permission.CU_VIEW) ? (
														<ManageAddInviteUser
															refreshInvitedListFunc={this.handlerRefreshInvitedList}
															isLoadingStopFunc={this.handlerLoadingStop}
															isLoadingStartFunc={this.handlerLoadingStart}
															name='association'
														/>
													) : null}
													{permissions.includes(Permission.C_ASSOCIATION_U_VIEW) ? (
														<ManageUserInviteList ref={this.childRef} name='association' />
													) : null}
												</>
											) : null}
										</Tab>
									) : null}
									{permissions.includes(Permission.C_CLUB_U_VIEW) ? (
										<Tab
											eventKey='club'
											title={
												<span>
													<FontAwesomeIcon
														title='Club'
														className='mr-2'
														size={"lg"}
														icon={faGolfBall}
													></FontAwesomeIcon>
													Club
												</span>
											}
										>
											{this.state.isclub ? (
												<>
													{permissions.includes(Permission.CU_VIEW) ? (
														<ManageAddInviteUser
															refreshInvitedListFunc={this.handlerRefreshInvitedList}
															isLoadingStopFunc={this.handlerLoadingStop}
															isLoadingStartFunc={this.handlerLoadingStart}
															name='club'
														/>
													) : null}
													{permissions.includes(Permission.C_CLUB_U_VIEW) ? (
														<ManageUserInviteList ref={this.childRef} name='club' />
													) : null}
												</>
											) : null}
										</Tab>
									) : null}
									{permissions.includes(Permission.C_COACH_U_VIEW) ? (
										<Tab
											eventKey='coach'
											title={
												<span>
													<FontAwesomeIcon
														title='Coach'
														className='mr-2'
														size={"lg"}
														icon={faRunning}
													></FontAwesomeIcon>
													Coach
												</span>
											}
										>
											{this.state.iscoach ? (
												<>
													{permissions.includes(Permission.CU_VIEW) ? (
														<ManageAddInviteUser
															refreshInvitedListFunc={this.handlerRefreshInvitedList}
															isLoadingStopFunc={this.handlerLoadingStop}
															isLoadingStartFunc={this.handlerLoadingStart}
															name='coach'
														/>
													) : null}
													{permissions.includes(Permission.C_COACH_U_VIEW) ? (
														<ManageUserInviteList ref={this.childRef} name='coach' />
													) : null}
												</>
											) : null}
										</Tab>
									) : null}
									{permissions.includes(Permission.C_ATHLETE_U_VIEW) ? (
										<Tab
											eventKey='athlete'
											title={
												<span>
													<FontAwesomeIcon
														title='Athlete'
														className='mr-2'
														size={"lg"}
														icon={faRunning}
													></FontAwesomeIcon>
													Athlete
												</span>
											}
										>
											{this.state.isAthlete ? (
												<>
													{permissions.includes(Permission.CU_VIEW) ? (
														<ManageAddInviteUser
															refreshInvitedListFunc={this.handlerRefreshInvitedList}
															isLoadingStopFunc={this.handlerLoadingStop}
															isLoadingStartFunc={this.handlerLoadingStart}
															name='athlete'
														/>
													) : null}
													{permissions.includes(Permission.C_ATHLETE_U_VIEW) ? (
														<ManageUserInviteList ref={this.childRef} name='athlete' />
													) : null}
												</>
											) : null}
										</Tab>
									) : null}
								</Tabs>
							</Col>
						</Row>
					</Spin>
				</Container>
			</>
		)
	}
}

const mapStateToProps = (state) => ({
	user: state.userReducer,
})

// export default connect(mapStateToProps, null,null)(ManageUserTabMenu);
export default connect(mapStateToProps, null, null, { forwardRef: true })(CreateUser)
