import React, { Fragment } from "react"
import ReactTable from "react-table"
import "../../../styles/CosmicTable.css"
import "antd/dist/antd.css"
import { Select, Input, Spin, notification, Empty, } from "antd"
import { Tabs, Tab, Modal, Row, Button, Col, Card, Container, Collapse, } from "react-bootstrap"
import Pagination from "../../../utils/Pagination"
import Assign from "../../../content/Icons/Assign.svg"
import Assign25 from "../../../content/Icons/25.svg"
import { connect } from "react-redux"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {faArrowDown,faArrowUp,faTrashAlt,faDownload,faExclamationTriangle,faUsers,faPlus,faMinus,} from "@fortawesome/free-solid-svg-icons"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import ExportData from "../../../utils/ExportData"
import { Permissions as Permission } from "../../../permissions/AppPermissions";
import PermissionProvider from "../../../permissions/PermissionProvider"
const { Option } = Select

const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: "Delete notification",
			icon: (
				<FontAwesomeIcon color={"#ff0000"} className='fa-sm' icon={faTrashAlt}></FontAwesomeIcon>
			),
			description: description,
		})
	} else if (flag === 2) {
		notification[type]({
			message: description,
			icon: (
				<FontAwesomeIcon
					color={"red"}
					className='fa-sm'
					icon={faExclamationTriangle}
				></FontAwesomeIcon>
			),
			//  duration:20
		})
	} else if (flag === 3) {
		notification[type]({
			message: description,
			icon: <img alt='' style={{ border: 3, height: 25, width: 25 }} src={Assign} />,
		})
	}
}

const setIsExpanded = (data, self) => {
	let Icons = self.state.Icons === true ? false : true
	self.setState({ IsExpanded: data, Icons: Icons })
}



const NoDataConst = (props) => (
	<span className='table-nodata-style'>
		No records found Please check your connection or change your filters{" "}
	</span>
)

//set open model property

const AssignpopupModal = async (data, self) => {
	
	let id = data.InvitedUserID

	const availableroleList = await APIRequest.getGetService(
		APIUrl.GET_USER_TYPE_ROLE + "/" + data.UserTypeID + "/@"
	)
	self.setState({ AvailableRoleList: availableroleList.Data, userTypeID: data.UserTypeID })

	const assignRoleList = await APIRequest.getGetService(APIUrl.GET_USER_ASSIGN_ROLE_LIST + "/" + id)
	self.setState({ AssignroleList: assignRoleList.Data })

	self.AvailableRoleRemove()
	//var InputData = []
	// if (data.UserRoleID == null) {
	// 	data.UserRoleID = "";
	// }
	// const UserRoleID = data.UserRoleID.trim()
	// if (UserRoleID.indexOf(',') != -1) {
	// 	let UserRoleID1 = UserRoleID.split(', ');
	// 	UserRoleID1 = UserRoleID1.map(i => InputData.push(parseInt(i)));
	// 	//InputData=UserRoleID//.push(UserRoleID);

	// }
	// else {
	// 	if (data.UserRoleID == "") {
	// 		InputData = []
	// 	}
	// 	else {
	// 		InputData.push(parseInt(UserRoleID));
	// 	}
	// }
	self.setState({
		isAssignRolepopup: true,
		AssignName: data.Name,
		AssignUserId: id,
		//Assignrole: InputData,
		isSubmitBtn: true,
	})
}

class ManageUsers extends React.Component {
	constructor(props) {
		super(props)

		let permissions = this.props.getAppPermissions();

		this.state = {
			users: [],
			loading: true,
			fullname: "",
			pageSize: 10,
			invitestatus: [],
			email: "",
			userstatus: [],
			department: [],
			IsExpanded: false,
			Icons: false,
			value: [],
			userstatusList: [],
			role_list: [],
			role: [],
			TotalPages: 0,
			TotalRecords: 0,
			IncrementPage: 0,
			//Export excel properties
			IsExportExcel: false,
			exportData: [],
			sortColumn: "",
			sortValue: "",
			exportFilename: "Manage Users List",
			exportbtn: true,
			isAssignRolepopup: false,
			isExpirtyError: "",
			isSubmitBtn: true,
			contry: [],
			council: [],
			councilList: [],
			isCouncil: false,
			isCommittee: false,
			isSearchBtn: true,
			isAssociation: false,
			association: [],
			associationList: [],
			isClub: false,
			club: [],
			clubList: [],
			isCoach: false,
			isAthlete: false,
			userType: [],
			userTypeList: [],
			AssignName: "",
			Assignrole: [],
			AssignroleList: [],
			AvailableRoleList: [],
			AssignUserId: 0,
			exportLabel: [
				{ columnName: "S.No", value: "SerialNo" },
				{ columnName: "Name", value: "Name" },
				{ columnName: "User Type", value: "UserType" },
				{ columnName: "Role", value: "UserRoleName" },
				{ columnName: "User Status", value: "UserStatus" },
			],
			userTypeID: 0,
			permissions: permissions,
		}

		this.table = React.createRef()
		this.fetch = this.fetch.bind(this)
		this.setRef = React.createRef()
		this.handleSetdata = this.handleSetdata.bind(this)
		this.handleClear = this.handleClear.bind(this)
		this.AssignpopupClose = this.AssignpopupClose.bind(this)
		this.AssignRoleSubmite = this.AssignRoleSubmite.bind(this)
	}

	AvailableRoleRemove = async () => {
		let assignrole = this.state.AssignroleList
		var availableRoleList = this.state.AvailableRoleList

		let data = availableRoleList.filter(
			(o1) => !assignrole.some((o2) => o1.OptionID === o2.OptionID)
		)
		this.setState({
			AvailableRoleList: data,
		})
	}
	AssignRoleRemoveCheck = async () => {
		let assignrole = this.state.AssignroleList
		var availableRoleList = this.state.AvailableRoleList

		let data = availableRoleList.filter(
			(o1) => !assignrole.some((o2) => o1.OptionID === o2.OptionID)
		)
		this.setState({
			AvailableRoleList: data,
		})
		openNotificationWithIcon("success", "Assigned Role removed successfully", 3)
	}
	AssignRoleSubmite = async () => {
		let self = this
		var Assignrole = []
		self.setState({ loading: true, isAssignRolepopup: false })
		this.state.AssignroleList.map((i) => Assignrole.push(parseInt(i.OptionID)))

		if (this.state.AssignroleList.length > 0) {
			const Data = await APIRequest.getGetService(
				APIUrl.ASSIGN_ADD_ROLE + "/" + Assignrole.toString() + "/" + this.state.AssignUserId
			)
			self.setState({ loading: false })

			if (Data === true) {
				self.setState({
					isAssignRolepopup: false,
					AssignroleList: [],
					AssignName: "",
					AssignUserId: 0,
					isSubmitBtn: true,
					AvailableRoleList: [],
				})
			}
			openNotificationWithIcon("success", "Add Assign Role Submitted successfully", 3)
			self.fetch(1)
		} else {
			openNotificationWithIcon("success", "Please Add the Assign role and try again", 2)
			self.setState({
				loading: false,
				isAssignRolepopup: true,
				isSubmitBtn: true,
			})
		}
	}
	AssignpopupClose = () => {
		this.setState({
			isAssignRolepopup: false,
			Assignrole: [],
			AssignName: "",
			AssignUserId: 0,
			isSubmitBtn: true,
			userTypeID: 0,
		})
	}

	// role filter functons
	handleFilterOnChange = async (event) => {
		if (event.target.value === "") {
			event.target.value = "@"
		}
		let self = this
		const availableroleList = await APIRequest.getGetService(
			APIUrl.GET_USER_TYPE_ROLE + "/" + this.state.userTypeID + "/" + event.target.value
		)
		this.setState({ AvailableRoleList: availableroleList.Data }, function () {
			self.AssignRoleRemoveCheck()
		})
	}

	handleClear(event) {
		this.setState(
			{
				fullname: "",
				invitestatus: [],
				email: "",
				userstatus: [],
				department: [],
				contry: [],
				council: [],
				role: [],
				userType: [],
				isSearchBtn: true,
				isCommittee: false,
				isCouncil: false,
				isAssociation: false,
				isClub: false,
				isCoach: false,
				isAthlete: false,

				club: [],
				association: [],
			},
			function () {
				this.fetch(this.table.current.state)
			}
		)
	}
	handleSetdata(event) {
		let input = this.state
		let searchbtn = true
		let data = this.state

		if (event.target.name === "fullname") {
			if (
				data.council.length !== 0 ||
				data.contry.length !== 0 ||
				data.department.length !== 0 ||
				event.target.value !== "" ||
				data.email.length !== 0 ||
				data.userstatus.length !== 0 ||
				data.invitestatus.length !== 0
			) {
				searchbtn = false
			}
		}
		input[event.target.name] = event.target.value
		this.setState({
			input,
			isSearchBtn: searchbtn,
		})
	}

	handleuserstatusChange(userstatus) {
		let searchbtn = true
		let data = this.state
		if (data.role.length !== 0 || data.fullname.length !== 0 || userstatus.length !== 0) {
			searchbtn = false
		}
		this.setState({ userstatus: userstatus, isSearchBtn: searchbtn })
	}
	handleuserTypeChange(userType) {
		var userData = this.state.userTypeList.filter((x) => x.UserTypeID === parseInt(userType))
		let searchbtn = true
		userData = userData[0]
		let Association = userData.Association
		let Athlete = userData.Athlete
		let Club = userData.Club
		let Coach = userData.Coach
		let Committee = userData.Committee
		let Council = userData.Council
		let data = this.state
		if (
			data.userstatus.length !== 0 ||
			data.role.length !== 0 ||
			data.fullname.length !== 0 ||
			userType.length !== 0
		) {
			searchbtn = false
		}
		this.setState({
			isCommittee: Committee,
			isCouncil: Council,
			isAssociation: Association,
			isClub: Club,
			isCoach: Coach,
			isAthlete: Athlete,
			userType: userType,
			isSearchBtn: searchbtn,
		})
	}
	handleAssociationChange(association) {
		let searchbtn = true
		let data = this.state
		if (
			data.role.length !== 0 ||
			association.length !== 0 ||
			data.club.length !== 0 ||
			data.council.length !== 0 ||
			data.fullname.length !== 0 ||
			data.userstatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ association: association, isSearchBtn: searchbtn })
	}
	handleClubChange(club) {
		let searchbtn = true
		let data = this.state
		if (
			data.role.length !== 0 ||
			data.association.length !== 0 ||
			club.length !== 0 ||
			data.council.length !== 0 ||
			data.fullname.length !== 0 ||
			data.userstatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ club: club, isSearchBtn: searchbtn })
	}
	handleCouncilChange(council) {
		let searchbtn = true
		let data = this.state
		if (
			data.role.length !== 0 ||
			data.association.length !== 0 ||
			data.club.length !== 0 ||
			council.length !== 0 ||
			data.fullname.length !== 0 ||
			data.userstatus.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ council: council, isSearchBtn: searchbtn })
	}
	handleroleChange(role) {
		let searchbtn = true
		let data = this.state
		if (role.length !== 0 || data.fullname.length !== 0 || data.userstatus.length !== 0) {
			searchbtn = false
		}
		this.setState({ role: role, isSearchBtn: searchbtn })
	}
	handleAssignroleChange(AvailableRoleList, self) {
		let AvailableRoleLists = [...self.state.AvailableRoleList]
		AvailableRoleLists.push(AvailableRoleList)
		var AssignroleList = self.state.AssignroleList.filter(
			(x) => x.OptionID !== AvailableRoleList.OptionID
		)
		self.setState(
			{ AvailableRoleList: AvailableRoleLists, AssignroleList: AssignroleList, isSubmitBtn: false },
			function () {
				if (self.state.AssignroleList.length === 0) {
					self.setState({ isSubmitBtn: true })
				}
				self.AssignRoleRemoveCheck()
			}
		)
	}
	handleAvailableroleChange(AssignroleList, self) {
		// var toggle = document.getElementsByClassName("modal-backdrop")[0];
		//   toggle.classList.remove("modal-backdrop")
		let AssignroleLists = [...self.state.AssignroleList]
		AssignroleLists.push(AssignroleList)
		var AvailableRoleList = self.state.AvailableRoleList.filter(
			(x) => x.OptionID !== AssignroleList.OptionID
		)
		self.setState({
			AvailableRoleList: AvailableRoleList,
			AssignroleList: AssignroleLists,
			isSubmitBtn: false,
		})
		openNotificationWithIcon("success", "Available Role added successfully", 3)
	}
	handlerPagination = (pageNo) => {
		this.setState(
			{
				IncrementPage: pageNo,
			},
			function () { }
		)
		this.fetch(pageNo)
	}
	async fetch(state) {
		let sortbyname = "InvitedDate"
		let orderby = "DESC"
		if (state.sorted !== undefined) {
			if (state.sorted.length !== 0) {
				sortbyname = state.sorted[0].id
				if (state.sorted[0].desc === true) {
					orderby = "DESC"
				}
				if (state.sorted[0].desc === false) {
					orderby = "ASC"
				}
			}
		}
		//let pageclicked = 0
		let pagelimit = 0
		let self = this
		if (state.page === undefined || state.pageSize === undefined) {
			//pageclicked = 1
			pagelimit = this.state.pageSize
		} else {
			//pageclicked = state.page + 1
			pagelimit = state.pageSize
		}
		let data = {
			PageNo: state > 0 ? state : 1,
			PageSize: pagelimit,
			Name: this.state.fullname,
			Email: this.state.email,
			Department: this.state.department.toString(),
			Invite_Status: this.state.invitestatus.toString(),
			User_Status: this.state.userstatus.toString(),
			SortColumn: sortbyname,
			SortOrder: orderby,
			CountryIDs: this.state.contry.toString(),
			CouncilIDs: this.state.council.toString(),
			CouncilUser: this.state.isCouncil,
			CommitteeUser: this.state.isCommittee,
			AssociationUser: this.state.isAssociation,
			ClubUser: this.state.isClub,
			CoachUser: this.state.isCoach,
			AthleteUser: this.state.isAthlete,
			RoleID: this.state.role.toString(),
			AssociationID: this.state.association.toString(),
			CouncilID: this.state.council.toString(),
			ClubID: this.state.club.toString(),
		}
		self.setState({ loading: true })
		APIRequest.getPostService(APIUrl.MANAGE_USER_LIST, data)
			.then((response) => {
				
				if (response.Data.length > 0) {
					const firstRow = response.Data.find((x) => x)
					self.setState({
						loading: false,
						users: response.Data,
						TotalPages: firstRow.TotalPages,
						TotalRecords: firstRow.TotalRows,
						sortColumn: sortbyname,
						sortValue: orderby,
						exportbtn: false,
					})
				} else {
					self.setState({
						exportbtn: true,TotalRecords:0
					})
				}
				self.setState({ loading: false, users: response.Data })
			})
			.catch((error) => {
				self.setState({ loading: false })
			})
	}

	async componentDidMount() {
		const userStatus = await APIRequest.getGetService(APIUrl.INVITED_USER_STATUS_LIST)
		this.setState({ userstatusList: userStatus.Data })
		const role = await APIRequest.getGetService(APIUrl.ROLE_LIST)
		this.setState({ role_list: role.Data })

		const userTypeList = await APIRequest.getGetService(APIUrl.USER_TYPE_LIST)
		this.setState({ userTypeList: userTypeList.Data })
		const councilList = await APIRequest.getGetService(APIUrl.COUNCIL_LIST)
		this.setState({ councilList: councilList.Data })
		const associationList = await APIRequest.getGetService(APIUrl.ASSOCIATION_LIST)
		this.setState({ associationList: associationList.Data })
		const clubList = await APIRequest.getGetService(APIUrl.CLUB_LIST)
		this.setState({ clubList: clubList.Data })
	}

	handlerRefreshInvitedList = () => {
		this.fetch(1)
	}

	handlerExportData = () => {
		let self = this
		let InputData = {
			PageNo: 1,
			PageSize: self.state.TotalRecords,
			Name: self.state.fullname,
			Email: self.state.email,
			Department: self.state.department.toString(),
			Invite_Status: self.state.invitestatus.toString(),
			User_Status: self.state.userstatus.toString(),
			SortColumn: self.state.sortColumn,
			SortOrder: self.state.sortValue,
			CountryIDs: this.state.contry.toString(),
			CouncilIDs: this.state.council.toString(),
			CouncilUser: this.state.isCouncil,
			CommitteeUser: this.state.isCommittee,
			AssociationUser: this.state.isAssociation,
			ClubUser: this.state.isClub,
			CoachUser: this.state.isCoach,
			AthleteUser: this.state.isAthlete,
			RoleID: this.state.role.toString(),
			AssociationID: this.state.association.toString(),
			CouncilID: this.state.council.toString(),
			ClubID: this.state.club.toString(),
		}
		APIRequest.getPostService(APIUrl.MANAGE_USER_LIST, InputData)
			.then((response) => {
				if (response.Data.length > 0) {
					self.setState({
						exportData: response.Data,
						IsExportExcel: true,
					})
				}
			})
			.catch((error) => { })
	}

	//update export excel flag as false
	componentDidUpdate() {
		if (this.state.IsExportExcel) {
			this.setState({
				IsExportExcel: false,
			})
		}
	}

	render() {
		let permissions = this.state.permissions
		const columns = [
			{
				Header: () => <div className='text-left'>S.No</div>,
				accessor: "SerialNo",
				sortable: false,
				resizable: false,
				className: "tableheader text-center wordwrap",
				headerClassName: "BoldText ColoredText",
				width: Math.round(window.innerWidth * 0.04),
				Cell: (row) => <div className='tabledata text-left' >{row.value}</div>,

			},
			{
				Header: () => <div className='text-left'>Name</div>,
				accessor: "Name",
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				width: Math.round(window.innerWidth * 0.2),
				Cell: (row) => {
					return <div className='tabledata '>{row.original.Name}</div>
				},
			},
			{
				Header: () => <div className='text-left'>User Type</div>,
				accessor: "UserType",

				align: "left",
				className: "tableheader wordwrap",
				sortable: true,
				resizable: false,
				width: Math.round(window.innerWidth * 0.2),

				Cell: (row) => (
					<div className='tabledata col-sm-12 text-center'>
						<div className='row'>
							<div className=' text-left'> 
								<span className='grid-username-font'>{row.row.UserType}</span>
								<br />
								<div className='tabledata wordwrap'>
								{row.row.ClubName} {row.row.AssociationName} {row.row.CouncilName}
								</div>
								{/* <small> */}
							
								{/* </small> */}
							</div>
						</div>
					</div>
				),
				style: {
					textAlign: "center",
				},
			},
			{ show: false, accessor: "AssociationName" },
			{ show: false, accessor: "ClubName" },
			{ show: false, accessor: "CouncilName" },
			{ show: false, accessor: "UserRoleID" },

			{
				Header: () => <div className='text-left'>Role</div>,
				accessor: "UserRoleName",

				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return <div className='tabledata '>{row.original.UserRoleName}</div>
				},
			},

			{
				Header: () => <div className='text-center'>User Status</div>,

				accessor: "UserStatus",
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				width: Math.round(window.innerWidth * 0.1),
				Cell: (row) => {
					return (
						<div className='tabledata text-center'>
							<label className='status_sent'> {row.original.UserStatus}</label>
						</div>
					)
				},
			},

			{
				Header: () => <div className='text-center'>Action</div>,
				accessor: "Action",
				className: "tableheader wordwrap",
				sortable: false,
				resizable: false,
				textAlign: "center",
				filterable: false,
				width: Math.round(window.innerWidth * 0.1),
				Cell: (row) => {
					return (
						<div className='tabledata text-center'>
								{row.original.UserInviteID===row.original.InvitedByID?<>
							{permissions.includes(Permission.M_U_ASSIGNROLES) ? (
								<img
									alt=''
									//style={{ border: 3, height: 25, width: 25 }}
									onClick={() => AssignpopupModal(row.original, this)}
									title='Mapping'
									src={Assign25}
								/>
							) : null}
							</>:null}
							
						</div>
					)
				},
			},
		]

		const {councilList,
			userstatusList,
			role_list,
			userTypeList,
			AssignroleList,
			AvailableRoleList,
		} = this.state

		return (
			<Container fluid className='h-full'>
				{permissions.includes(Permission.M_U_VIEW) ? (
					<Spin size='large' spinning={this.state.loading} tip='Loading...'>
						<Row className='h-full singletab'>
							<Col className='h-full'>
								<Tabs
									defaultActiveKey={this.state.key}
									id='controlled-tab-example'
									className='list-tab row'
									onSelect={this.handelTabChange}
								>
									<Tab
										eventKey='committee'
										title={
											<span className=' d-flex justify-content-start row'>
												{/* <FontAwesomeIcon
													title='Manage Users'
													className='mr-2'
													size={"lg"}
													icon={faUsers}
												></FontAwesomeIcon> */}
												Manage Users
											</span>
										}
										name={this.state.name}
									>
										<div className=''>
											<Card className='w-100 card from-group mt-3'>
												<Card.Header
													className='text-white pl-1'
													onClick={() => setIsExpanded(!this.state.IsExpanded, this)}
												>
													<div className='float-left filter-heading-font'>Search Manage Users </div>
													<div className='float-right'>
														{this.state.Icons ? (
															<FontAwesomeIcon
																color=' #FFFFFF'
																className='action-icon-font'
																icon={faArrowUp}
																style={{ cursor: "pointer" }}
															></FontAwesomeIcon>
														) : (
															<FontAwesomeIcon
																color=' #FFFFFF'
																className='action-icon-font'
																icon={faArrowDown}
																style={{ cursor: "pointer" }}
															></FontAwesomeIcon>
														)}
													</div>
												</Card.Header>
												<Collapse in={this.state.IsExpanded}>
													<Card.Body>
														<div className='row'>
															<div className='form-group col-sm-4'>
																<label className='pl-0'>Name</label>
																<Input
																	size='large'
																	autoComplete='off'
																	type='text'
																	name='fullname'
																	value={this.state.fullname}
																	onChange={this.handleSetdata}
																	bordered={false}
																	className='textfiled w-80 pl-0'
																	placeholder='Name'
																></Input>
															</div>

															<div className='col-sm-4'>
																<label className='pl-0'>User Type </label>
																<Select
																	className='textfiled w-80 pl-0'
																	bordered={false}
																	// mode='multiple'
																	maxTagCount="responsive"
																	allowClear
																	showArrow
																	showSearch
																	style={{ width: "100%" }}
																	placeholder='Select User Type'
																	name='userType'
																	size={"large"}
																	value={this.state.userType}
																	onChange={this.handleuserTypeChange.bind(this)}
																	optionFilterProp='children'
																	filterSort={(optionA, optionB) =>
																		optionA.children
																			.toLowerCase()
																			.localeCompare(optionB.children.toLowerCase())
																	}
																>
																	{userTypeList.map((c) => (
																		<Option value={c.UserTypeID}>{c.Type}</Option>
																	))}
																</Select>
															</div>
															{this.state.isAssociation ? (
																<>
																	<div className='form-group col-sm-4'>
																		<label className='pl-0'>Association</label>
																		<Select
																			className='textfiled w-80 pl-0'
																			mode='multiple'
																			maxTagCount="responsive"
																			allowClear
																			showArrow
																			showSearch
																			bordered={false}
																			placeholder='Select Association'
																			name='association'
																			value={this.state.association}
																			size={"large"}
																			onChange={this.handleAssociationChange.bind(this)}
																			optionFilterProp='children'
																			filterSort={(Associationdata, Associationvalue) =>
																				Associationdata.children
																					.toLowerCase()
																					.localeCompare(Associationvalue.children.toLowerCase())
																			}
																		>
																			{this.state.associationList.map((c) => (
																				<Option value={c.OptionID}>{c.OptionData}</Option>
																			))}
																		</Select>
																	</div>
																</>
															) : null}
															{this.state.isClub ? (
																<>
																	<div className='form-group col-sm-4'>
																		<label className='pl-0'>Club</label>
																		<Select
																			className='textfiled w-80 pl-0'
																			mode='multiple'
																			maxTagCount="responsive"
																			showArrow
																			showSearch
																			allowClear
																			bordered={false}
																			style={{ width: "100%" }}
																			placeholder='Select Club'
																			name='club'
																			value={this.state.club}
																			size={"large"}
																			onChange={this.handleClubChange.bind(this)}
																			optionFilterProp='children'
																			filterSort={(Clubdata, Clubvalue) =>
																				Clubdata.children
																					.toLowerCase()
																					.localeCompare(Clubvalue.children.toLowerCase())
																			}
																		>
																			{this.state.clubList.map((c) => (
																				<Option value={c.OptionID}>{c.OptionData}</Option>
																			))}
																		</Select>
																	</div>
																</>
															) : null}
															{this.state.isCouncil ? (
																<>
																	<div className='form-group col-sm-4'>
																		<label className='pl-0'>Council</label>
																		<Select
																			mode='multiple'
																			maxTagCount="responsive"
																			allowClear
																			showArrow
																			showSearch
																			className='textfiled w-80 pl-0'
																			style={{ width: "100%" }}
																			bordered={false}
																			value={this.state.council}
																			placeholder='Select Council'
																			name='Council'

																			size={"large"}
																			onChange={this.handleCouncilChange.bind(this)}
																			optionFilterProp='children'
																			filterSort={(optionA, optionB) =>
																				optionA.children
																					.toLowerCase()
																					.localeCompare(optionB.children.toLowerCase())
																			}
																		>
																			{councilList.map((c) => (
																				<Option value={c.OptionID}>{c.OptionData}</Option>
																			))}
																		</Select>
																	</div>
																</>
															) : null}
															<div className='form-group col-sm-4'>
																<label className='pl-0'>Role</label>
																<Select
																	mode='multiple'
																	maxTagCount="responsive"
																	allowClear
																	showArrow
																	showSearch
																	className='textfiled w-80 pl-0'
																	style={{ width: "100%" }}
																	name='invitestatus'
																	value={this.state.role}
																	size={"large"}
																	bordered={false}
																	placeholder='Select Role'
																	optionFilterProp='children'
																	onChange={this.handleroleChange.bind(this)}
																	filterSort={(optionA, optionB) =>
																		optionA.children
																			.toLowerCase()
																			.localeCompare(optionB.children.toLowerCase())
																	}
																>
																	{role_list.map((c) => (
																		<Option value={c.OptionID}>{c.OptionData}</Option>
																	))}
																	{/* {optionItems} */}
																</Select>
															</div>
															<div className='form-group col-sm-4'>
																<label className='pl-0'>User Status </label>
																<br />

																<Select
																	className='textfiled w-80 pl-0'
																	bordered={false}
																	mode='multiple'
																	maxTagCount="responsive"
																	allowClear
																			showArrow
																			showSearch
																	style={{ width: "100%" }}
																	placeholder='Select User Status'
																	name='userstatus'
																	size={"large"}
																	value={this.state.userstatus}
																	onChange={this.handleuserstatusChange.bind(this)}
																	optionFilterProp='children'
																	filterSort={(optionA, optionB) =>
																		optionA.children
																			.toLowerCase()
																			.localeCompare(optionB.children.toLowerCase())
																	}
																>
																	{userstatusList.map((c) => (
																		<Option value={c.OptionID}>{c.OptionData}</Option>
																	))}
																</Select>
															</div>
														</div>
														<div className='col-sm-12 '>
															<div className=' d-flex justify-content-end row'>
															<Button onClick={this.handleClear} className='mr-2 btn_green'>
																	Clear
																</Button>
																<Button
																	onClick={this.fetch}
																	className='btn_green'
																	disabled={this.state.isSearchBtn}
																>
																	Search
																</Button>
																
															</div>
														</div>
													</Card.Body>
												</Collapse>
											</Card>

											<div className='admin-report-table pt-5'>
												<ReactTable
													data={this.state.users}
													columns={columns}
													showPaginationTop={false}
													showPaginationBottom={false}
													minRows={0}
													defaultPageSize={10}
													defaultPage={0}
													onFetchData={this.fetch}
													ref={this.table}
													sortable={true}
													multiSort={true}
													manual
													className={"CosmicTable"}
													showPageSizeOptions={false}
													NoDataComponent={NoDataConst}

													defaultSorting={[
														{
															id: "id",
															desc: true,
														},
													]}
													manualPagination={true}
												/>
											</div>
											<div className='mt-1'>
						<label>
							<b>Total Records: </b>
							{this.state.TotalRecords > 10
								? this.state.users.length + " of " + this.state.TotalRecords
								: this.state.TotalRecords !== 0
								? this.state.TotalRecords
								: 0}
						</label>
						</div>
											<div className='row '>
												<div className='col-sm-4'>
													{" "}
													{/*	{permissions.includes(Permission.C_COMMITTEE_U_EXPORT_EXCEL) ? ( */}
													{permissions.includes(Permission.M_U_EXPORT_EXCEL) ? (
														<Button
															className='mr-2 btn_green'
															style={{
																//paddingRight: "0.4rem", paddingLeft: "0.4rem",
																cursor: "pointer",
															}}
															disabled={this.state.exportbtn}
															onClick={this.handlerExportData}
														>
															<FontAwesomeIcon
																title='Delete'
																color={"#FFFFFF"}
																icon={faDownload}
															// marginRight={"5px !important"}
															></FontAwesomeIcon>{" "}
															Export Excel
														</Button>
													) : null}
												</div>
												<div className='col-sm-8'>
													<div className=' d-flex justify-content-end'>
														<Pagination
															totalPages={this.state.TotalPages}
															totalRecords={this.state.TotalRecords}
															paginationCall={this.handlerPagination}
														/>
													</div>
												</div>
											</div>
											{this.state.IsExportExcel ? (
												<ExportData
													data={this.state.exportData}
													label={this.state.exportLabel}
													filename={this.state.exportFilename}
												/>
											) : null}
										</div>
									</Tab>
								</Tabs>
							</Col>
						</Row>

						<>
							<Modal
								size='lg'
								//aria-labelledby="contained-modal-title-vcenter"
								//   aria-labelledby="contained-modal-title-vcenter"
								backdrop='static'
								//backdrop="true"
								backdropClassName='notshow'
								id='sdsd'
								//  centered
								show={this.state.isAssignRolepopup}
								onHide={this.AssignpopupClose}
							>
								<Modal.Header className=''>
									<div className='col-sm-12'>
										<div className='row'>
											<div className='col-sm-11'>
												<div className='d-flex justify-content-center'>
													<h5 className='font-weight-bold text-white mb-0'>{"Assign Roles"}</h5>
												</div>
											</div>
											<div className='col-sm-1'>
												<div className='d-flex justify-content-end'>
													<button
														type='button'
														className='close text-white'
														onClick={this.AssignpopupClose}
														data-dismiss='modal'
														aria-label='Close'
													>
														<span aria-hidden='true'>&times;</span>
													</button>{" "}
												</div>
											</div>
										</div>
									</div>
								</Modal.Header>
								<Modal.Body>
									<div className='col-sm-12'>
										<div className='row '>
											<div className='form-group col-sm-6'>
												<Input
													size='large'
													//addonBefore='Search'
													addonAfter='Search'
													className='label_textbox_right'
													defaultValue=''
													name='rolename'
													onChange={this.handleFilterOnChange.bind(null)}
												/>
											</div>
										</div>
										<div className='row form-group'>
											<div className='col-sm-6'>
												<li className='list-group-item font-weight-bold no-border border-0 list_header'>
													Available Role List{" "}
												</li>
												<ul class='list-group scrollbar List_group  pt-0 mt-0'>
													{AvailableRoleList.map((c) => (
														<li className='list-group-item'>
															<div className='row'>
																<div className='col-sm-10 d-flex justify-content-start'>
																	{" "}
																	{c.OptionData}
																</div>
																<div className='col-sm-2 d-flex justify-content-end'>
																	{" "}
																	<FontAwesomeIcon
																		className='icon_eye sm'
																		onClick={() => this.handleAvailableroleChange(c, this)}
																		icon={faPlus}
																	></FontAwesomeIcon>
																</div>
															</div>
														</li>
													))}
												</ul>
												{AvailableRoleList.length === 0 ? <Empty description={<span>No roles available</span>}></Empty> : null}
											</div>

											<div className='col-sm-6'>
												<li className='list_header list-group-item font-weight-bold no-border border-0'>
													Assgin Role List{" "}
												</li>
												<ul class='list-group scrollbar List_group  pt-0 mt-0'>
													{AssignroleList.map((c) => (
														<li className='list-group-item'>
															<div className='row'>
																<div className='col-sm-10 d-flex justify-content-start'>
																	{" "}
																	{c.OptionData}
																</div>
																<div className='col-sm-2 d-flex justify-content-end'>
																	{" "}
																	<FontAwesomeIcon
																		className='icon_eye sm'
																		onClick={() => this.handleAssignroleChange(c, this)}
																		icon={faMinus}
																	></FontAwesomeIcon>
																</div>
															</div>
														</li>
													))}
												</ul>
												{AssignroleList.length === 0 ? <Empty description={<span>No roles assigned</span>}></Empty> : null}
											</div>
										</div>
									</div>
								</Modal.Body>
								<Modal.Footer className='m-0 py-0 '>
									<div className='col-sm-12'>
										<div className='row  d-flex justify-content-end'>
											{this.state.isExpirtyError !== undefined && this.state.isExpirtyError > 0 && (
												<span className='error danger_msg'>{this.state.isExpirtyError}</span>
											)}

											<Button
												className='btn_green mr-2'
												disabled={this.state.isSubmitBtn}
												onClick={this.AssignRoleSubmite}
											>
												Save
											</Button>
											{/* <Button className='btn_green' onClick={this.AssignpopupClose}>
												Close
											</Button> */}
										</div>
									</div>
								</Modal.Footer>
							</Modal>
						</>
					</Spin>
				) : null}
			</Container>
		)
	}
}

export default PermissionProvider(ManageUsers)
