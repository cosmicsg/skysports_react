import React from "react"
import "../../../styles/CosmicTable.css"
import { Input, Spin, Checkbox, notification, Select } from "antd"
import { Row, Button, Col, Container } from "react-bootstrap"
import "../../../App"
import "antd/dist/antd.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
	faCheckCircle,
	faChevronUp,
	faChevronDown,
	faExclamationTriangle,
} from "@fortawesome/free-solid-svg-icons"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"

const { Option } = Select

const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////Save  flag
		notification[type]({
			message: description,
			icon: (
				<FontAwesomeIcon color={"green"} className='fa-sm' icon={faCheckCircle}></FontAwesomeIcon>
			),
		})
	} else if (flag === 2) {
		notification[type]({
			message: description,
			icon: (
				<FontAwesomeIcon
					color={"red"}
					className='fa-sm'
					icon={faExclamationTriangle}
				></FontAwesomeIcon>
			),
			//  duration:20
		})
	} else if (flag === 3) {
		notification[type]({
			message: description,
			icon: (
				<FontAwesomeIcon
					color={"red"}
					className='fa-sm'
					icon={faExclamationTriangle}
				></FontAwesomeIcon>
			),
			//  duration:20
		})
	}
}
const handelExpand = (id, self) => {
	var toggle = document.getElementById("ModuleGroup" + id)

	let data = self.state.roles
	if (data.length !== 0) {
		for (let masterid = 0; masterid < data.length; masterid++) {
			let masterdata = data[masterid]
			if (id === masterdata.PermissionModuleID) {
				if (masterdata.isExpend === true) {
					masterdata.isExpend = false
					toggle.classList.remove("rolegroupvisible")
				} else {
					masterdata.isExpend = true
					toggle.classList.add("rolegroupvisible")
				}
			}
		}
	}
	self.setState({
		roles: data,
	})
}

class AddRoles extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			roles: [],
			roleName: "",
			disabledRole: false,
			roleExistCheck: false,
			roleID: 0,
			roleDescription: "",
			rolePermission: [],
			loading: true,
			overAllcheck: false,
			errors: [],
			savebtntext: "",
			userType: [],
			userTypeList: [],
			disableClick: false,
		}
		this.table = React.createRef()
		this.handelSave = this.handelSave.bind(this)
		this.handelCancel = this.handelCancel.bind(this)
		this.handleSetdata = this.handleSetdata.bind(this)
	}

	handleuserTypeChange(userType) {
		this.setState({ userType: userType })
	}
	//cancel function
	handelCancel = () => {
		this.props.isBackToGridList()
	}
	//already exist role check
	handelExistRoleCheck = async (event) => {
		let data = false
		const role = await APIRequest.getGetService(
			APIUrl.VALIDATE_ROLE_EXIST + "/" + event.target.value
		)
		if (role.Data === false) {
			openNotificationWithIcon(
				"success",
				"Role Name is already exists. Please change the role name",
				2
			)
			data = true
			let roleid = document.getElementById("Role")
			roleid.classList.add("mandatory_text")
		}
		this.setState({
			roleExistCheck: data,
		})
	}
	//save and update functions
	handelSave = () => {
		
		let role = document.getElementById("Role")
		let UserType = document.getElementsByClassName("UserType")[0]
		let UserTypeID = this.state.userType.toString()
		let roleName = this.state.roleName
		let InputData = []
		this.state.rolePermission.map(i => InputData.push(parseInt(i.value)));
		let rolePermissionData = InputData.toString()
		let roleDescription = this.state.roleDescription
		let roleID = this.state.roleID
		let self = this
		self.setState({ loading: true })
		self.setState({ disableClick: true})
		if (this.state.roleExistCheck === true) {
			openNotificationWithIcon(
				"success",
				"Role Name is already exists. Please changed the role name and try again",
				2
			)
			role.classList.add("mandatory_text")
		} else if (roleName === "" || rolePermissionData === "" || UserTypeID === "") {
			openNotificationWithIcon(
				"success",
				"One or more mandatory fields have an error. Please check and try again",
				3
			)
			if (roleName === "") {
				role.classList.add("mandatory_text")
			}
			if (UserTypeID === "") {
				UserType.classList.add("mandatory_text")
			}
		} else {
			let AddRolePermissionModel = {
				Description: roleDescription,
				RoleID: roleID.toString(),
				RoleName: roleName,
				PermissionIDs: rolePermissionData,
				UsertypeIDs: UserTypeID,
			}
			APIRequest.getPostService(APIUrl.ADD_ROLE_PERMISSION, AddRolePermissionModel)
				.then((response) => {
					if (response.Succeeded === true) {
						if (roleID.toString() === "0") {
							openNotificationWithIcon("success", "Role Permission Saved Successfully", 1)
						}
						if (roleID.toString() !== "0") {
							openNotificationWithIcon("success", "Role Permission Updated successfully", 1)
						}

						self.props.isBackToGridList()
						// self.props.isLoadingStopFunc()
					}
					self.setState({ loading: false, disableClick: false})
				})
				.catch((error) => {
					self.setState({ loading: false, disableClick: false })
				})
		}
		self.setState({ loading: false })
	}
	//text fileds value seted
	handleSetdata(event) {
		if (this.state.disabledRole === false) {
			let role = document.getElementById("Role")
			let input = this.state
			input[event.target.name] = event.target.value

			this.setState({
				input,
			})
			if (event.target.name === "roleName" && event.target.value === "") {
				role.classList.add("mandatory_text")
			}
			if (event.target.name === "roleName" && event.target.value !== "") {
				role.classList.remove("mandatory_text")
			}
		}
	}
	//  permission single check function
	onChangeBox = async (SerialNo, PermissionID) => {
		let data = this.state.roles

		if (data.length !== 0) {
			for (let masterid = 0; masterid < data.length; masterid++) {
				let masterdata = data[masterid]
				for (let index = 0; index < masterdata.PermissionGroup.length; index++) {
					const element = masterdata.PermissionGroup[index]
					for (let i = 0; i < element.Permissions.length; i++) {
						let Permissionsdata = element.Permissions[i]

						if (
							SerialNo === Permissionsdata.SerialNo &&
							Permissionsdata.SubPermission.CreatePermissionID === PermissionID
						) {
							if (Permissionsdata.SubPermission.CreateChecked === true) {
								Permissionsdata.SubPermission.CreateChecked = false
								await this.rolePermissionRemove(
									Permissionsdata.SubPermission.CreatePermissionID,
									this
								)
							} else {
								Permissionsdata.SubPermission.CreateChecked = true
								await this.rolePermissionInsert(
									Permissionsdata.SubPermission.CreatePermissionID,
									this
								)
							}
						}
						if (
							SerialNo === Permissionsdata.SerialNo &&
							Permissionsdata.SubPermission.DeletePermissionID === PermissionID
						) {
							if (Permissionsdata.SubPermission.DeleteChecked === true) {
								Permissionsdata.SubPermission.DeleteChecked = false
								await this.rolePermissionRemove(
									Permissionsdata.SubPermission.DeletePermissionID,
									this
								)
							} else {
								Permissionsdata.SubPermission.DeleteChecked = true
								await this.rolePermissionInsert(
									Permissionsdata.SubPermission.DeletePermissionID,
									this
								)
							}
						}
						if (
							SerialNo === Permissionsdata.SerialNo &&
							Permissionsdata.SubPermission.EditPermissionID === PermissionID
						) {
							if (Permissionsdata.SubPermission.EditChecked === true) {
								Permissionsdata.SubPermission.EditChecked = false
								await this.rolePermissionRemove(
									Permissionsdata.SubPermission.EditPermissionID,
									this
								)
							} else {
								Permissionsdata.SubPermission.EditChecked = true
								await this.rolePermissionInsert(
									Permissionsdata.SubPermission.EditPermissionID,
									this
								)
							}
						}
						if (
							SerialNo === Permissionsdata.SerialNo &&
							Permissionsdata.PermissionID === PermissionID
						) {
							if (Permissionsdata.SubPermission.EnableChecked === true) {
								Permissionsdata.SubPermission.EnableChecked = false
								await this.rolePermissionRemove(Permissionsdata.PermissionID, this)
							} else {
								Permissionsdata.SubPermission.EnableChecked = true
								await this.rolePermissionInsert(Permissionsdata.PermissionID, this)
							}
						}
						if (
							SerialNo === Permissionsdata.SerialNo &&
							Permissionsdata.SubPermission.ViewPermissionID === PermissionID
						) {
							if (Permissionsdata.SubPermission.ViewChecked === true) {
								Permissionsdata.SubPermission.ViewChecked = false
								await this.rolePermissionRemove(
									Permissionsdata.SubPermission.ViewPermissionID,
									this
								)
							} else {
								await this.rolePermissionInsert(
									Permissionsdata.SubPermission.ViewPermissionID,
									this
								)
								Permissionsdata.SubPermission.ViewChecked = true
							}
						}
					}
				}
			}
		}
		this.setState({
			roles: data,
		})
	}
	///added  permissions selected
	rolePermissionInsert = async (PermissionId, self) => {
		let rolePermission = [...self.state.rolePermission]
		rolePermission.push({ value: PermissionId })
		self.setState({ rolePermission })
	}
	// selected permission removed
	rolePermissionRemove = async (PermissionId, self) => {
		var rolePermission = self.state.rolePermission.filter((x) => x.value !== PermissionId)
		self.setState({ rolePermission })
	}
	//group checkbox to group based all checked  permissions and unchecked permissions
	onChangeGroupcheckBox = async (SerialNo, PermissionID) => {
		let data = this.state.roles
		let checkedvalue = false

		if (data.length !== 0) {
			for (let masterid = 0; masterid < data.length; masterid++) {
				let masterdata = data[masterid]
				for (let index = 0; index < masterdata.PermissionGroup.length; index++) {
					const element = masterdata.PermissionGroup[index]
					for (let i = 0; i < element.Permissions.length; i++) {
						let Permissionsdata = element.Permissions[i]
						if (
							SerialNo === Permissionsdata.SerialNo &&
							Permissionsdata.PermissionID === PermissionID
						) {
							if (Permissionsdata.Checked === true) {
								checkedvalue = false
								await this.rolePermissionRemove(
									Permissionsdata.SubPermission.CreatePermissionID,
									this
								)
								await this.rolePermissionRemove(
									Permissionsdata.SubPermission.EditPermissionID,
									this
								)
								await this.rolePermissionRemove(
									Permissionsdata.SubPermission.ViewPermissionID,
									this
								)
								await this.rolePermissionRemove(
									Permissionsdata.SubPermission.DeletePermissionID,
									this
								)
								await this.rolePermissionRemove(Permissionsdata.PermissionID, this)
							} else {
								checkedvalue = true
								if (Permissionsdata.SubPermission.CreatePermissionID !== 0) {
									await this.rolePermissionInsert(
										Permissionsdata.SubPermission.CreatePermissionID,
										this
									)
								}
								if (Permissionsdata.SubPermission.EditPermissionID !== 0) {
									await this.rolePermissionInsert(
										Permissionsdata.SubPermission.EditPermissionID,
										this
									)
								}
								if (Permissionsdata.SubPermission.ViewPermissionID !== 0) {
									await this.rolePermissionInsert(
										Permissionsdata.SubPermission.ViewPermissionID,
										this
									)
								}
								if (Permissionsdata.SubPermission.DeletePermissionID !== 0) {
									await this.rolePermissionInsert(
										Permissionsdata.SubPermission.DeletePermissionID,
										this
									)
								}
								if (Permissionsdata.PermissionID !== 0) {
									await this.rolePermissionInsert(Permissionsdata.PermissionID, this)
								}
							}
							Permissionsdata.Checked = checkedvalue
							Permissionsdata.SubPermission.CreateChecked = checkedvalue
							Permissionsdata.SubPermission.EditChecked = checkedvalue
							Permissionsdata.SubPermission.DeleteChecked = checkedvalue
							Permissionsdata.SubPermission.EnableChecked = checkedvalue
							Permissionsdata.SubPermission.ViewChecked = checkedvalue
						}
					}
				}
			}
		}
		this.setState({
			roles: data,
		})
	}
	overAllLoader = () => {
		this.setState(
			{
				loading: true,
			},
			function () {
				return true
			}
		)
	}
	//over all checked ||unchecked to group checkbox checked ||unchecked, all permissions checked ||unchecked
	onChangeSelectAllcheckBox = async () => {
		let data = this.state.roles
		let checkedvalue = false
		await this.overAllLoader()
		if (this.state.overAllcheck === true) {
			checkedvalue = false
			this.setState({
				rolePermission: [],
			})
		} else {
			checkedvalue = true
		}

		if (data.length !== 0) {
			for (let masterid = 0; masterid < data.length; masterid++) {
				let masterdata = data[masterid]
				for (let index = 0; index < masterdata.PermissionGroup.length; index++) {
					const element = masterdata.PermissionGroup[index]

					element.Checked = checkedvalue
					for (let i = 0; i < element.Permissions.length; i++) {
						let Permissionsdata = element.Permissions[i]
						if (checkedvalue === true) {
							if (Permissionsdata.SubPermission.CreatePermissionID !== 0) {
								await this.rolePermissionInsert(
									Permissionsdata.SubPermission.CreatePermissionID,
									this
								)
							}
							if (Permissionsdata.SubPermission.EditPermissionID !== 0) {
								await this.rolePermissionInsert(
									Permissionsdata.SubPermission.EditPermissionID,
									this
								)
							}
							if (Permissionsdata.SubPermission.ViewPermissionID !== 0) {
								await this.rolePermissionInsert(
									Permissionsdata.SubPermission.ViewPermissionID,
									this
								)
							}
							if (Permissionsdata.SubPermission.DeletePermissionID !== 0) {
								await this.rolePermissionInsert(
									Permissionsdata.SubPermission.DeletePermissionID,
									this
								)
							}
							if (Permissionsdata.PermissionID !== 0) {
								await this.rolePermissionInsert(Permissionsdata.PermissionID, this)
							}
						}
						Permissionsdata.Checked = checkedvalue
						Permissionsdata.SubPermission.CreateChecked = checkedvalue
						Permissionsdata.SubPermission.EditChecked = checkedvalue
						Permissionsdata.SubPermission.DeleteChecked = checkedvalue
						Permissionsdata.SubPermission.EnableChecked = checkedvalue
						Permissionsdata.SubPermission.ViewChecked = checkedvalue
					}
				}
			}
		}

		this.setState({
			overAllcheck: checkedvalue,
			roles: data,
			loading: false,
		})
	}

	async componentDidMount() {
		let submitType = this.props.type
		let data = this.props.data
		let userTypeListdata = await APIRequest.getGetService(APIUrl.USER_TYPE_LIST)
		this.setState({ userTypeList: userTypeListdata.Data })

		if (submitType === "Add") {
			const permissionList = await APIRequest.getGetService(APIUrl.ROLE_PERMISSION_MASTER_LIST)
			this.setState({
				loading: false,
				roles: permissionList.Data.PermissionModule,
				savebtntext: "Save",
			})
		}
		if (submitType === "Edit" && data !== 0) {
			const permissionList = await APIRequest.getGetService(
				APIUrl.GET_SAVE_ROLE_PERMISSION_LIST + "/" + data
			)
			var InputData = []
			if (permissionList.Data.UsertypeIDs !== null) {
				if (permissionList.Data.UsertypeIDs.indexOf(",") !== -1) {
				//	let UserRoleID1 = permissionList.Data.UsertypeIDs.split(',');
					permissionList.Data.UsertypeIDs.split(',').map(i=>InputData.push(parseInt(i)));
					//InputData=UserRoleID//.push(UserRoleID);
				} else {
					if (permissionList.Data.UsertypeIDs === "") {
						InputData = []
					} else {
						InputData.push(parseInt(permissionList.Data.UsertypeIDs))
					}
				}
			}
			
			this.setState({
				loading: false,
				roles: permissionList.Data.PermissionModule,
				rolePermission: permissionList.Data.SelectedPermissionModel,
				roleName: permissionList.Data.RoleName,
				roleID: permissionList.Data.RoleID,
				roleDescription: permissionList.Data.Description,
				disabledRole: true,
				savebtntext: "Update",
				userType: InputData,
			})
		}
	}
	render() {
		return (
			<Container fluid className='h-full'>
				<Spin size='large' spinning={this.state.loading} tip='Loading...'>
					<Row className='h-full '>
						<Col className='h-full'>
							<label
								style={{ borderBottom: "4px solid" }}
								className=' d-flex justify-content-start   content_Header'
							>
								Manage Access Control
							</label>
							<div className='row mt-4'>
								<div className='col-sm-1 pr-0  pt-4'>
									<label className=''>User Type</label>
								</div>

								<div className='col-sm-3'>
									<Select
										className='textfiled w-80 mt-2 UserType'
										bordered={false}
										mode='multiple'
										allowClear
										maxTagCount={"responsive"}
										id={"UserType"}
										style={{ width: "100%" }}
										placeholder='Select User Type'
										name='userType'
										size={"large"}
										value={this.state.userType}
										onChange={this.handleuserTypeChange.bind(this)}
										optionFilterProp='children'
										filterSort={(optionA, optionB) =>
											optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
										}
									>
										{this.state.userTypeList.map((c) => (
											<Option value={c.UserTypeID}>{c.Type}</Option>
										))}
									</Select>
								</div>
								<div className='form-group pr-0 col-sm-1 pt-4'>
									<label className=''>Role Name</label>
								</div>
								<div className='form-group col-sm-3 mt-2'>
									<Input
										size='large'
										name='roleName'
										type='text'
										id={"Role"}
										className={"textfiled pb-1"}
										placeholder={"Enter Role Name"}
										value={this.state.roleName}
										disabled={this.state.disabledRole}
										bordered={false}
										onChange={this.handleSetdata}
										onBlur={this.handelExistRoleCheck}
										autoComplete='off'
									/>
								</div>

								<div className='form-group pr-0 col-sm-1 pt-4'>
									<label className=''>Description</label>
								</div>
								<div className='form-group col-sm-3 mt-2'>
									<Input
										size='large'
										name='roleDescription'
										type='text'
										id={"Description"}
										value={this.state.roleDescription}
										bordered={false}
										onChange={this.handleSetdata}
										placeholder="Enter Description"
										className={"textfiled pb-1"}
										autoComplete='off'
										disabled={this.state.disabledRole}
									/>
								</div>
							</div>

							<div className='filterSearch col-sm-12  p-0 mt-3'>
								<div id='content' className='content'>
									{this.state.roles.length !== 0 ? (
										<div className='standardInfo'>
											<div className='px-2 mx-2 '>
												<div className='row card-header text-white'>
													<div className='col-sm-1 pl-0'>
														<Checkbox
															className='dynamic_checkbox'
															onChange={() => this.onChangeSelectAllcheckBox()}
															checked={this.state.overAllcheck}
														></Checkbox>
													</div>
													{/* <div className="col-sm-1 pl-0"> <label className="filter-heading-font">#</label></div> */}
													<div className='col-sm-6 pl-0'>
														{" "}
														<label className='filter-heading-font'>Name</label>
													</div>
													<div className='col-sm-1 '>
														{" "}
														<label className='filter-heading-font'>Create</label>
													</div>
													<div className='col-sm-1 '>
														{" "}
														<label className='filter-heading-font'>Edit</label>
													</div>
													<div className='col-sm-1 '>
														{" "}
														<label className='filter-heading-font'>View</label>
													</div>
													<div className='col-sm-1 '>
														{" "}
														<label className='filter-heading-font'>Delete</label>
													</div>
													<div className='col-sm-1 '>
														{" "}
														<label className='filter-heading-font'>Enable</label>
													</div>
												</div>
											</div>
											{this.state.roles.map((c) => (
												<div>
													<button
														onClick={() => handelExpand(c.PermissionModuleID, this)}
														className={" filed_Panel  mb-1 accordion Modual" + c.PermissionModuleID}
													>
														<div className='row'>
															<div className='col-sm-10'>
																<label className='perimmison_module d-flex justify-content-start'>
																	{c.Name}
																</label>
															</div>
															<div className=' col-sm-2 d-flex justify-content-end'>
																{c.isExpend ? (
																	<FontAwesomeIcon
																		className='action-icon-font perimmison_module'
																		icon={faChevronUp}
																		style={{ cursor: "pointer" }}
																	></FontAwesomeIcon>
																) : (
																	<FontAwesomeIcon
																		className='action-icon-font perimmison_module'
																		icon={faChevronDown}
																		style={{ cursor: "pointer" }}
																	></FontAwesomeIcon>
																)}
															</div>
														</div>
													</button>
													<div
														className='panel px-0 ModuleGroup'
														id={"ModuleGroup" + c.PermissionModuleID}
													>
														{c.PermissionGroup.map((d, pop) => (
															<div className='col-sm-12 filed_Panel roleGroup'>
																{d.Permissions.map((e, index) => (
																	<div className='row'>
																		<div className='col-sm-1'>
																			<Checkbox
																				className='dynamic_checkbox allCheck'
																				key={index}
																				onChange={() =>
																					this.onChangeGroupcheckBox(e.SerialNo, e.PermissionID)
																				}
																				checked={e.Checked}
																				value={index}
																			></Checkbox>
																		</div>
																		{/* <div className="col-sm-1" >
                                      <label>{e.SerialNo}</label>


                                    </div> */}
																		<div className='col-sm-6'>{e.Name}</div>
																		{/* Create */}
																		<div className='col-sm-1'>
																			{e.SubPermission.Create === true ? (
																				<Checkbox
																					className='dynamic_checkbox allCheck'
																					key={e.SubPermission.CreatePermissionID}
																					onChange={() =>
																						this.onChangeBox(
																							e.SerialNo,
																							e.SubPermission.CreatePermissionID
																						)
																					}
																					value={e.SubPermission.CreatePermissionID}
																					checked={e.SubPermission.CreateChecked}
																				></Checkbox>
																			) : (
																				<Checkbox
																					className='dynamic_checkbox'
																					disabled={true}
																					value={0}
																				>
																					{" "}
																				</Checkbox>
																			)}
																		</div>

																		{/* Edit */}
																		<div className='col-sm-1'>
																			{e.SubPermission.Edit === true ? (
																				<Checkbox
																					className='dynamic_checkbox allCheck'
																					key={e.SubPermission.EditPermissionID}
																					onChange={() =>
																						this.onChangeBox(
																							e.SerialNo,
																							e.SubPermission.EditPermissionID
																						)
																					}
																					checked={e.SubPermission.EditChecked}
																					value={e.SubPermission.EditPermissionID}
																				></Checkbox>
																			) : (
																				<Checkbox
																					className='dynamic_checkbox'
																					disabled={true}
																					value={0}
																				>
																					{" "}
																				</Checkbox>
																			)}
																		</div>
																		{/* View */}
																		<div className='col-sm-1'>
																			{e.SubPermission.View === true ? (
																				<Checkbox
																					className='dynamic_checkbox allCheck'
																					key={e.SubPermission.ViewPermissionID}
																					onChange={() =>
																						this.onChangeBox(
																							e.SerialNo,
																							e.SubPermission.ViewPermissionID
																						)
																					}
																					value={e.SubPermission.ViewPermissionID}
																					checked={e.SubPermission.ViewChecked}
																				></Checkbox>
																			) : (
																				<Checkbox
																					className='dynamic_checkbox'
																					disabled={true}
																					value={0}
																				>
																					{" "}
																				</Checkbox>
																			)}
																		</div>
																		{/* Delete */}
																		<div className='col-sm-1'>
																			{e.SubPermission.Delete === true ? (
																				<Checkbox
																					className='dynamic_checkbox allCheck'
																					key={e.SubPermission.DeletePermissionID}
																					onChange={() =>
																						this.onChangeBox(
																							e.SerialNo,
																							e.SubPermission.DeletePermissionID
																						)
																					}
																					checked={e.SubPermission.DeleteChecked}
																					value={e.SubPermission.DeletePermissionID}
																				></Checkbox>
																			) : (
																				<Checkbox
																					className='dynamic_checkbox'
																					disabled={true}
																					value={0}
																				>
																					{" "}
																				</Checkbox>
																			)}
																		</div>
																		<div className='col-sm-1'>
																			{e.SubPermission.Enable === true ? (
																				<Checkbox
																					className='dynamic_checkbox allCheck'
																					key={e.PermissionID}
																					onChange={() =>
																						this.onChangeBox(e.SerialNo, e.PermissionID)
																					}
																					checked={e.SubPermission.EnableChecked}
																					value={e.PermissionID}
																				></Checkbox>
																			) : (
																				<Checkbox
																					className='dynamic_checkbox'
																					disabled={true}
																					value={0}
																				>
																					{" "}
																				</Checkbox>
																			)}
																		</div>
																	</div>
																))}
															</div>
														))}
													</div>
												</div>
											))}
										</div>
									) : null}
									<div className=''>
										<div className='form-group  d-flex justify-content-end '>
											<Button
												className=' btn_green float-right  waves-effect waves-light btn-lg  mr-2 btn btn-primary'
												bsStyle='danger'
												bsSize='small'
												onClick={this.handelCancel}
												disabled={this.state.disableClick}
											>
												Cancel
											</Button>
											<Button
												className='btn_green float-right  waves-effect waves-light btn-lg  btn btn-primary'
												type='button'
												bsStyle='danger'
												bsSize='small'
												onClick={this.handelSave}
												disabled={this.state.disableClick}
											>
												{this.state.savebtntext}
											</Button>
										</div>
									</div>
								</div>
							</div>
						</Col>
					</Row>
				</Spin>
			</Container>
		)
	}
}

export default AddRoles
