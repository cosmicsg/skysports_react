import React, { Fragment } from "react"
import ReactTable from "react-table"
import "../../../styles/CosmicTable.css"
import "antd/dist/antd.css"
import Pagination from "../../../utils/Pagination"
import { Input, Spin, notification } from "antd"
import { Message } from "../../../message/messageConstant"
import { Button, Modal } from "react-bootstrap"
import ManageAddRoles from "./AddRoles"
import ExportData from "../../../utils/ExportData"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEdit, faTrashAlt, faDownload } from "@fortawesome/free-solid-svg-icons"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import { Permissions as Permission } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"
// notification common function
const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: description,
			icon: (
				<FontAwesomeIcon color={"#ff0000"} className='fa-sm' icon={faTrashAlt}></FontAwesomeIcon>
			),
			//description: description,
		})
	}
}

/// no data found text data
const NoDataConst = (props) => (
	<span className='table-nodata-style'>
		No records found Please check your connection or change your filters{" "}
	</span>
)
const onDeleteFun = (roleID, self) => {
	self.setState({ loading: true })
	APIRequest.getGetService(APIUrl.REMOVE_ROLE_PERMISSION + "/" + roleID)
		.then((response) => {
			self.setState({ loading: false })
			if (response.Data === true) {
				self.fetch(self.table.current.state)
				openNotificationWithIcon("success", "Record has been deleted", 1)
			}
		})
		.catch((error) => {
			self.setState({ loading: false })
		})
}
const openModal = (roleID, self) => {
	self.setState({ isDeleteOpen: true, deleteData: roleID })
}
class UserRoleList extends React.Component {
	constructor(props) {
		super(props)

		let permissions = this.props.getAppPermissions()

		this.state = {
			roleList: [],
			loading: true,
			pageSize: 10,
			TotalPages: 0,
			TotalRecords: 0,
			isAddrole: false,
			isListRole: true,
			isDeleteOpen: false,
			deleteData: "",
			rolename: "",
			isselectedRoleID: 0,
			issave: "",
			IncrementPage: 0,
			exportbtn: true,
			sortColumn: "",
			sortValue: "",
			IsExportExcel: false,
			exportFilename: "Role User List",
			exportData: [],
			exportLabel: [
				{ columnName: "S.No", value: "SerialNo" },
				{ columnName: "Role Name", value: "RoleName" },
				{ columnName: "Description", value: "Description" },
				{ columnName: "Created By", value: "CreatedByName" },
				{ columnName: "Created On", value: "CreatedOn" },
				{ columnName: "Changed By", value: "UpdatedByName" },
				{ columnName: "Changed On", value: "UpdatedOn" },
			],
			permissions: permissions,
		}
		this.table = React.createRef()
		this.fetch = this.fetch.bind(this)
		this.setRef = React.createRef()
		this.handleBtnAddRole = this.handleBtnAddRole.bind(this)
		this.deleteModal = this.deleteModal.bind(this)
	}
	handlerExportData = () => {
		let self = this
		let InputData = {
			PageNo: 1,
			PageSize: self.state.TotalRecords,
			SortColumn: self.state.sortColumn,
			SortOrder: self.state.sortValue,
			RoleName: this.state.rolename,
		}
		APIRequest.getPostService(APIUrl.MANAGE_ROLE_PERMISSION_LIST, InputData)
			.then((response) => {
				if (response.Data.length > 0) {
					self.setState({
						exportData: response.Data,
						IsExportExcel: true,
					})
				}
			})
			.catch((error) => {})
	}

	handlerPagination = (pageNo) => {
		this.setState(
			{
				IncrementPage: pageNo,
			},
			function () {}
		)
		this.fetch(pageNo)
	}
	deleteModal = () => {
		onDeleteFun(this.state.deleteData, this)
		this.setState({ isDeleteOpen: false })
	}
	closeModal = () => {
		this.setState({ isDeleteOpen: false })
	}
	//grid list only showing
	handlerGridList = () => {
		this.setState({ isAddrole: false, isListRole: true, isselectedRoleID: 0, issave: "" })
	}

	// role filter functons
	handleFilterOnChange = (event) => {
		this.setState({ rolename: event.target.value }, function () {
			this.fetch(1)
		})
	}
	//add role or edit role only show and grid list hide
	handleBtnAddRole(roleID, type) {
		this.setState({
			isAddrole: true,
			isListRole: false,
			isselectedRoleID: roleID,
			issave: type,
		})
	}
	// table grid list get and bind functions
	async fetch(state) {
		let sortbyname = "RoleID"
		let orderby = "DESC"
		if (state.sorted !== undefined) {
			if (state.sorted.length !== 0) {
				sortbyname = state.sorted[0].id
				if (state.sorted[0].desc === true) {
					orderby = "DESC"
				}
				if (state.sorted[0].desc === false) {
					orderby = "ASC"
				}
			}
		}
		let pageclicked = 0
		let pagelimit = 0
		let self = this
		if (state.page === undefined || state.pageSize === undefined) {
			pageclicked = 1
			pagelimit = this.state.pageSize
		} else {
			pageclicked = state.page + 1
			pagelimit = state.pageSize
		}
		let data = {
			PageNo: state > 0 ? state : 1,
			PageSize: pagelimit,
			SortColumn: sortbyname,
			SortOrder: orderby,
			RoleName: this.state.rolename,
		}
		self.setState({ loading: true })
		APIRequest.getPostService(APIUrl.MANAGE_ROLE_PERMISSION_LIST, data)
			.then((response) => {
				if (response.Data.length > 0) {
					const firstRow = response.Data.find((x) => x)
					self.setState({
						loading: false,
						roleList: response.Data,
						TotalPages: firstRow.TotalPages,
						TotalRecords: firstRow.TotalRows,
						sortColumn: sortbyname,
						sortValue: orderby,
						exportbtn: false,
					})
				} else {
					self.setState({
						exportbtn: true,
						TotalRecords: 0,
					})
				}
				self.setState({ loading: false, roleList: response.Data })
			})
			.catch((error) => {
				self.setState({ loading: false })
			})
		if (pageclicked === 1) {
		}
	}

	handlerRefreshInvitedList = () => {
		this.fetch(1)
	}

	//update export excel flag as false
	componentDidUpdate() {
		if (this.state.IsExportExcel) {
			this.setState({
				IsExportExcel: false,
			})
		}
	}

	render() {
		let permissions = this.state.permissions
		const columns = [
			{
				Header: () => <div style={{ textAlign: "left" }}>S.No</div>,
				accessor: "SerialNo",
				sortable: false,
				resizable: false,
				className: "tableheader wordwrap",
				headerClassName: "BoldText ColoredText",

				Cell: (row) => (
					<div className='tabledata' style={{ textAlign: "left" }}>
						{row.value}
					</div>
				),
				width: Math.round(window.innerWidth * 0.04),
			},

			{
				Header: () => <div style={{ textAlign: "left" }}>Roles</div>,

				resizable: false,
				accessor: "RoleName",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return <div className='tabledata wordwrap'>{row.original.RoleName}</div>
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Description</div>,

				resizable: false,
				accessor: "Description",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return <div className='tabledata wordwrap'>{row.original.Description}</div>
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Created By</div>,

				resizable: false,
				accessor: "CreatedByName",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return <div className='tabledata wordwrap'>{row.original.CreatedByName}</div>
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Created On</div>,

				resizable: false,
				accessor: "CreatedOn",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return <div className='tabledata wordwrap'>{row.original.CreatedOn}</div>
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Changed By</div>,

				resizable: false,
				accessor: "UpdatedByName",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return <div className='tabledata wordwrap'>{row.original.UpdatedByName}</div>
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Changed On</div>,

				resizable: false,
				accessor: "UpdatedOn",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return <div className='tabledata wordwrap'>{row.original.UpdatedOn}</div>
				},
			},
			{
				show: false,
				accessor: "DeleteFlag",
			},
			{
				show: false,
				accessor: "RoleID",
			},
			{
				show: false,
				accessor: "UserID",
			},
			{
				show: false,
				accessor: "CreatedByID",
			},
			{
				Header: () => <div style={{ textAlign: "center" }}>Action</div>,
				accessor: "Action",
				className: "tableheader wordwrap",
				sortable: false,
				resizable: false,
				textAlign: "center",
				filterable: false,
				maxWidth: 100,
				Cell: (row) => {
					return (
						<div className='tabledata' style={{ textAlign: "center" }}>
							{row.original.UserID===row.original.CreatedByID?<>
							{permissions.includes(Permission.M_A_C_EDIT) ? (
							<FontAwesomeIcon
								title='Edit'
								className='mr-2 icon_edit'
								style={{ cursor: "pointer" }}
								icon={faEdit}
								onClick={() => this.handleBtnAddRole(row.original.RoleID, "Edit")}
							/>
							) : null} 
							{row.original.DeleteFlag === 0 ? (
								<>
									{permissions.includes(Permission.M_A_C_DELETE) ? (
										<FontAwesomeIcon
											title='Delete'
											className={"icon_Delete"}
											onClick={() => openModal(row.original.RoleID, this)}
											icon={faTrashAlt}
											style={{ cursor: "pointer" }}
										/>
									) : null}
								</>
							) : null}
							</>:null}
						</div>
					)
				},
			},
		]

		return (
			<div>
				{permissions.includes(Permission.M_A_C_VIEW) ? (
					<div className='col-sm-12 pt-10' style={{ padding: "15px" }}>
						{this.state.isListRole ? (
							<Spin size='large' spinning={this.state.loading} tip='Loading...'>
								<div className='filterSearch'>
									<div className='row '>
										<div className='col-sm-8  d-flex justify-content-start '>
											<label
												className=' d-flex justify-content-start   content_Header'
											>
												Manage Access Control
											</label>
										</div>
										<div className='col-sm-4'>
											<Input
												size='large'
												addonBefore='Search'
												className='label_textbox_left'
												defaultValue=''
												name='rolename'
												onChange={this.handleFilterOnChange.bind(null)}
											/>
										</div>
									</div>
								</div>
								{permissions.includes(Permission.M_A_C_CREATE) ? (
									<div className='col-sm-12 pt-2'>
										<div className=' d-flex justify-content-end row'>
											<Button
												onClick={() => this.handleBtnAddRole(0, "Add")}
												className=' btn_green '
												disabled={this.state.resendbtn}
											>
												Add Role
											</Button>
										</div>
									</div>
								) : null}
								<div className='admin-report-table pt-1'>
									<ReactTable
										data={this.state.roleList}
										columns={columns}
										showPaginationTop={false}
										showPaginationBottom={false}
										minRows={0}
										defaultPageSize={10}
										defaultPage={1}
										onFetchData={this.fetch}
										ref={this.table}
										sortable={true}
										multiSort={true}
										manual
										className={"CosmicTable"}
										showPageSizeOptions={false}
										NoDataComponent={NoDataConst}
										defaultSorting={[
											{
												id: "id",
												desc: true,
											},
										]}
										manualPagination={true}
									/>
								</div>
								<div className='mt-1'>
									<label>
										<b>Total Records: </b>
										{this.state.TotalRecords > 10
											? this.state.roleList.length + " of " + this.state.TotalRecords
											: this.state.TotalRecords !== 0
											? this.state.TotalRecords
											: 0}
									</label>
								</div>
								<div className='pt-3 row'>
									<div className=' col-sm-6'>
										{permissions.includes(Permission.M_A_C_EXPORT_EXCEL) ? (
											<Button
												className='mr-2 btn_green'
												style={{
													//paddingRight: "0.4rem", paddingLeft: "0.4rem",
													cursor: "pointer",
												}}
												disabled={this.state.exportbtn}
												onClick={this.handlerExportData}
											>
												<FontAwesomeIcon
													title='Delete'
													color={"#FFFFFF"}
													icon={faDownload}
													// marginRight={"5px !important"}
												></FontAwesomeIcon>{" "}
												Export Excel
											</Button>
										) : null}
									</div>
									<div className='col-sm-6 d-flex justify-content-end'>
										<Pagination
											totalPages={this.state.TotalPages}
											totalRecords={this.state.TotalRecords}
											paginationCall={this.handlerPagination}
										/>
									</div>
								</div>
							</Spin>
						) : null}
						{this.state.isAddrole ? (
							<ManageAddRoles
								type={this.state.issave}
								data={this.state.isselectedRoleID}
								isBackToGridList={this.handlerGridList}
							></ManageAddRoles>
						) : null}

						<Fragment>
							<Modal
								size='sm'
								//aria-labelledby="contained-modal-title-vcenter"
								//   aria-labelledby="contained-modal-title-vcenter"
								backdrop='static'
								//  centered
								show={this.state.isDeleteOpen}
								onHide={this.closeModal}
							>
								<div className='popup-content'>
									<div className='deletecircle'></div>
									<i className='fa fa-trash-o deleteicon'></i>
									<Modal.Body>
										<p className='pull-left' style={{ margin: "4rem 2rem" }}>
											{" "}
											<h5>{Message.ROLE_DELETE}</h5>
											<h6 style={{ color: "darkgray" }}>{Message.ROLE_DELETE_CONFIRM}</h6>
											<h6
												style={{
													color: "darkgray",
													marginRight: "7rem",
													marginBottom: "-3rem",
												}}
											>
												{Message.ROLE_DELETE_CONFIRM1}
											</h6>
										</p>
										<br />
										<div className='pull-right'>
											<Button className='btn_cancel mr-2' onClick={this.closeModal}>
												No
											</Button>
											<Button
												className='btn_green'
												style={{ height: "2rem", padding: "5px 10px" }}
												onClick={this.deleteModal}
											>
												Yes
											</Button>
										</div>
									</Modal.Body>
								</div>
							</Modal>
						</Fragment>
						{this.state.IsExportExcel ? (
							<ExportData
								data={this.state.exportData}
								label={this.state.exportLabel}
								filename={this.state.exportFilename}
							/>
						) : null}
					</div>
				) : null}
			</div>
		)
	}
}

export default PermissionProvider(UserRoleList)
