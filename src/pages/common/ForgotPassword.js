import React, { Component } from "react"
import "../../styles/Common.css"
import { APIRequest } from "../../components/api-manager/apiRequest"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { ROUTES } from "../../routing/routeConstants"

import Img from "../../content/images/SportsAppLogo.png"
class Forgotpassword extends Component {
	constructor(props) {
		super(props)
		this.state = {
			islogged: false,
			showInCorrectMsg: true,
			inCorrectMsg: "",
			msgColor: "red",
			Email: "",
			input: {},
			errors: {
				Email: "",
			},
			disableClick: false,
			isSuccess: false,
			resMessage: "",
			isError: false,
		}
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.Cancel = this.Cancel.bind(this)
		this.Setdata = this.Setdata.bind(this)
	}
	Setdata(event) {
		let input = this.state
		input[event.target.name] = event.target.value

		this.setState({
			input,
			showInCorrectMsg: true,
		})
	}
	Cancel(event) {
		event.preventDefault()

		this.props.history.replace({ pathname: "/" })
	}
	handleChange(event) {
		this.setState({
			Email: event.target.value,
			errors: {},
		})
		if (this.state.resMessage) {
			this.setState({
				isError: false,
				isSuccess: false,
				resMessage: "",
			})
		}
	}
	Loginpage = () => {
		this.props.history.push({ pathname: ROUTES.LOGIN })
	}
	handleSubmit(event) {
		event.preventDefault()

		let self = this
		if (this.validate() === true) {
			self.setState({
				Verify: true,
				disableClick: true,
			})
let  PageURL = window.location.href
PageURL = PageURL.replace("forgot-password","change-password");
			let InputData = {
				Email: this.state.Email,
				PageUrl: PageURL,
			}

			APIRequest.getPostService(APIUrl.SEND_FORGOT_PASSWORD_LINK, InputData)
				.then((response) => {
					if (response.Data.Success) {
						self.setState({
							msgColor: "green",
							resMessage: "Submitted successfully please check your mail.",
							isOpen: false,
							disableClick: false,
							Email: "",
							isSuccess: true,
						})
					} else {
						let errors = {}
						//errors["Email"] = "The email address you entered does not exist.";
						self.setState({
							msgColor: "red",
							Email: "",
							resMessage: "The email address you entered does not exist.",
							errors,
							disableClick: false,
							Verify: false,
							isError: true,
						})
					}
				})
				.catch((error) => {
					self.setState({
						disableClick: false,
						isError: true,
						resMessage: "Something went wrong!",
					})
				})
			//axios({
			//  method: "POST",
			//  url: "/api/account/resetpassword",
			//  headers: {
			//    Accept: "application/json, text/plain, */*",
			//    "Access-Control-Allow-Origin": "*",
			//    "Content-Type": "application/json",
			//  },
			//  data: InputData,
			//}).then(function (e) {
			//
			//  if (e.data.success === true) {
			//    alert("Submited successfully please check your mail");
			//    self.setState({
			//      isOpen: false,
			//    });
			//  } else {
			//    self.setState({
			//      Verify: false,
			//    });
			//    alert("incorrect email");
			//  }
			//});
		}
	}
	validate() {
		//let input = this.state.input;
		let errors = {}
		let isValid = true

		if (!this.state.Email) {
			isValid = false
			errors["Email"] = "Please enter the email."
		}

		if (this.state.Email !== "" && this.state.Email !== undefined) {
			var pattern = new RegExp(
				/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			)
			if (!pattern.test(this.state.Email)) {
				isValid = false
				errors["Email"] = "Please enter valid email."
			}
		}

		this.setState({
			//inCorrectMsg:errors,
			errors: errors,
		})

		return isValid
	}
	render() {
		return (
			// <div className="container-fluid form-group" >
			<div className='row' style={{ float: "right" }}>
				<div className='loginform'>
					<div className='loginlogo'>
						<img src={Img} alt='pic' width='200' height='160' />
					</div>
					<div className='row center form-group'>
						<div className='col-sm-12 text-center'>
							<h3 className='fpwhead'>Forgot Password</h3>
						</div>
					</div>
					<form>
						<div className='row'>
							<div className='col-sm-12'>
								<div className=' form-group' style={{ fontSize: "1.1rem" }}>
									<div className='inputWithIcon' style={{ top: "8px" }}>
										<input
											type='text'
											className='formcontrol'
											required='required'
											id='Email'
											name='Email'
											value={this.state.Email}
											onChange={this.handleChange}
											placeholder='Email'
										/>
										<span className='placeholder' style={{ marginLeft: "6rem" }}></span>
										<i className='fa fa-user fa-lg fa-fw' aria-hidden='true'></i>
									</div>
									<div className='danger_msg' style={{ marginTop: "1rem" }}>
										{this.state.errors.Email}
									</div>
								</div>
								<br />
								<button
									type='submit'
									id='btnlogin'
									className='loginbtn Login '
									onClick={this.handleSubmit}
									disabled={this.state.disableClick}
								>
									Reset Password
								</button>
								<br></br>
								{this.state.isSuccess || this.state.isError ? (
									<div
										className='password-screen-success forgot-pwd-error-msg'
										style={{
											color: this.state.isSuccess ? "green" : "red",
										}}
									>
										{this.state.resMessage}
									</div>
								) : null}
								<label
									className='labels'
									style={{ cursor: "pointer", marginLeft: "12.5rem" }}
									onClick={this.Loginpage}
								>
									Back
								</label>
								<br></br>
								<br></br>
								{/* <label className="labels" onClick={this.Loginpage}>back</label> */}
							</div>
						</div>
					</form>
				</div>
			</div>
			// </div>
		)
	}
}
export default Forgotpassword
