import React, { Component } from "react";
import "../../styles/Common.css";
import { ROUTES } from "../../routing/routeConstants";
import {
  APIRequest,
  
} from "../../components/api-manager/apiRequest";
import * as APIUrl from "../../components/api-manager/apiConstant";
import Img from "../../content/images/SportsAppLogo.png";
class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      islogged: false,
      input: {},
      errors: {},
      IsShowCurrentPage: false,
      Email: "",
      isSuccess: false,
      resMessage: "",
      isError: false,
      disableClick: false,
    };
    this.handlePasswordCheck = this.handlePasswordCheck.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmitValidate = this.handleSubmitValidate.bind(this);
    this.handleValidateUserGUID = this.handleValidateUserGUID.bind(this);
  }
  componentDidMount() {
    ////userGuID valid check
    this.handleValidateUserGUID(this.props.match.params.id);
  }
  ////userGuID valid check
  handleValidateUserGUID(UserGuid) {
    // let InputData = {
    //   userUniqueId: UserGuid,
    // };
    let apiUrl = APIUrl.GET_USER_UNIQUE_NAME + "/" + UserGuid;
    let self = this;
    var res = APIRequest.getGetService(apiUrl);

    res
      .then((response) => {
        if (response.Data.Expired) {
          self.props.history.replace({ pathname: ROUTES.LINK_EXPIRED });
        } else if (response.Data.IsExist) {
          self.props.history.replace({ pathname: ROUTES.PAGE_NOT_FOUND });
        } else {
          self.setState({
            IsShowCurrentPage: true,
            Email: response.Data.Email,
          });
        }
      })
      .catch((error) => {
        //showNotification("Your link has expired, please contact admin", "error");
        self.props.history.replace({ pathname: ROUTES.LOGIN });
      });
  }

  //   password validate
  handlePasswordCheck(event) {
    let input = this.state.input;
    input[event.target.name] = event.target.value;

    this.setState({
      input,
    });
    if(this.state.resMessage){
      this.setState({
        isError: false,
        isSuccess: false,
        resMessage:""
      });
    }

    let errors = {};
    
    if ("confirm_password" === event.target.name) {
      if (
        input["confirm_password"] === "" &&
        input["confirm_password"] === "undefined"
      ) {
        
        errors["confirm_password"] = "Please enter your confirm password.";
      }
      if (
        typeof input["Newpassword"] !== "undefined" &&
        typeof input["confirm_password"] !== "undefined"
      ) {
        if (input["Newpassword"] !== input["confirm_password"]) {
          
          errors["confirm_password"] = "Passwords don't match.";
        }
      }
    }
      
    if ("Newpassword" === event.target.name) {
      var regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,12}$/;
      if (regex.exec(event.target.value) === null) {
        errors["Newpassword"] =
          "Password must contain at least 8 characters with any one of a special character (@, $, !, &)";
      }
    }
    this.setState({
      errors: errors,
    });
  }

  //// save function
  handleSubmit(event) {
    debugger
    event.preventDefault();

    if (this.handleSubmitValidate()) {

      let self = this;
      //Disable button
      self.setState({
        disableClick: true,
      });
      let InputData = {
        Email: this.state.Email,
        Password: this.state.input.Newpassword,
      };
      APIRequest.getPostService(
        APIUrl.SAVE_RESET_PASSWORD_ACTION,
        InputData
      )
        .then((response) => {
          if (response.Succeeded === true) {
            //let input = self.state;
            self.setState({
              input: {},
              isSuccess: true,
              resMessage: "Password reset successful!",
              disableClick: false
            });
            self.props.history.replace({
              pathname: ROUTES.LOGIN,
              data: self.state,
            });
          } else {
          }
        })
        .catch((error) => {
          self.setState({
            input: {},
            isError: true,
            disableClick: false,
            resMessage: "Something went wrong!",
          });
        });
    }
  }
  handleSubmitValidate() {
    let input = this.state.input;
    let errors = {};
    let isValid = true;
    if (!input["Newpassword"]) {
      isValid = false;
      errors["Newpassword"] = "Please enter your new password.";
    }
      
    if (
      input["Newpassword"] !== "" &&
      input["Newpassword"] !== undefined &&
      input["Newpassword"] !== "undefined" &&
      input["Newpassword"] !== null
    ) {
      var regex =
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{7,12}$/;
      if (regex.exec(input["Newpassword"]) == null) {
        isValid = false;
        // errors["Newpassword"] = "Password must contain any one of a special character (@, $, !, &)";
        errors["password"] =
          "Password must contain at least 8 characters with any one of a special character (@, $, !, &)";
      }
    }
    if (!input["confirm_password"]) {
      isValid = false;
      errors["confirm_password"] = "Please enter your confirm password.";
    }
    if (
      typeof input["Newpassword"] !== "undefined" &&
      typeof input["confirm_password"] !== "undefined"
    ) {
      if (input["Newpassword"] !== input["confirm_password"]) {
        isValid = false;
        errors["confirm_password"] = "Passwords don't match.";
      }
    }

    this.setState({
      errors: errors,
    });

    return isValid;
  }
  render() {
    return (
      <>
        {this.state.IsShowCurrentPage ? (
          //<div className="container-fluid form-group">
          <form className="form-signin">
            <div className="row" style={{ float: "right" }}>
              <div className="loginform" style={{ height: "41rem" }}>
                <div className="loginlogo">
                  <img src={Img} alt="pic" width="200" height="130" />
                </div>
                <div className="row center form-group">
                  <div className="col-sm-12 text-center">
                    <h3
                      className="fpwhead"
                      style={{ marginTop: "0.5rem", marginBottom: "0.5rem" }}
                    >
                      Change Password
                    </h3>
                  </div>
                </div>
                {/*<form>*/}
                <div className="row">
                  <div className="col-sm-12">
                    <div
                      className=" form-group"
                      style={{
                        fontSize: "1.1rem",
                        marginTop: "-0.5rem",
                        marginBottom: "2rem",
                      }}
                    >
                      <div
                        className="inputWithIcon"
                        style={{ marginBottom: "1rem" }}
                      >
                        <input
                          type="password"
                          className="formcontrol"
                          required="required"
                          name="Newpassword"
                          autoComplete="off"
                          onChange={this.handlePasswordCheck}
                          placeholder="New Password"
                        />
                        <span className="placeholder"></span>
                        <i
                          className="fa fas fa-lock fa-lg fa-fw"
                          aria-hidden="true"
                        />
                      </div>
                      <div
                        className="danger_msg"
                        style={{ marginTop: "-1rem" }}
                      >
                        {this.state.errors.Newpassword
                          ? this.state.errors.Newpassword
                          : this.state.errors.password
                          ? this.state.errors.password
                          : ""}
                      </div>

                      <div
                        className="inputWithIcon "
                        style={{ marginBottom: "0.5rem" }}
                      >
                        <input
                          type="password"
                          className="formcontrol"
                          required="required"
                          name="confirm_password"
                          autoComplete="off"
                          onChange={this.handlePasswordCheck}
                          placeholder="Confirm Password"
                        />
                        <span
                          className="placeholder"
                          style={{ marginLeft: "12rem" }}
                        ></span>
                        <i
                          className="fa fas  fa-lock fa-lg fa-fw"
                          aria-hidden="true"
                        ></i>
                      </div>
                      <div className="danger_msg">
                        {this.state.errors.confirm_password}
                      </div>

                      <button
                        className="loginbtn"
                        style={{ marginTop: "0.5rem", marginBottom: "-1rem" },{ fontWeight: "550"}}
                        type="submit"
                        onClick={this.handleSubmit}
                        disabled={this.state.disableClick}
                      >
                        Change Password
                      </button>
                    </div>

                    <div
                      className=" form-group"
                      style={{ fontSize: "1.1rem", marginTop: "1rem" }}
                    >
                      {this.state.isSuccess || this.state.isError ? (
                        <div
                          className="password-screen-success password-reset-success"
                          style={{
                            color: this.state.isSuccess ? "green" : "red",
                          }}
                        >
                          {this.state.resMessage}
                        </div>
                      ) : null}
                      <div style={{ marginTop: "0.5rem" }}>
                        <h5
                          style={{
                            marginBottom: "0.3rem",
                            marginLeft: "1rem",
                            fontSize: "1rem",
                            fontWeight: "550",
                          }}
                        >
                          Create a Password that:
                        </h5>
                        <ul
                          style={{
                            fontSize: "medium",
                            fontWeight: "400",
                            fontWeight: "550",
                            // fontFamily:"Poppins"
                          }}
                        >
                          Contains at least 8 characters
                          <br />
                          Include all of the following:
                          <ul>
                            <li>An Uppercase character</li>
                            <li>A Lowercase character</li>
                            <li>A Number</li>
                            <li>A Special character</li>
                          </ul>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                {/*</form>*/}
              </div>
            </div>
          </form>
        ) : //</div>
        null}
      </>
    );
  }
}
export default ChangePassword;
