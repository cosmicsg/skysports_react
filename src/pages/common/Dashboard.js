import React from "react"
import { connect } from "react-redux"
import { addUserId } from "../../components/redux/actions/userAction"
//import "font-awesome/css/font-awesome.min.css";
import { useHistory } from "react-router-dom"
import { ROUTES } from "../../routing/routeConstants"
import "../../styles/Common.css"
function Dashboard(props) {
	let history = useHistory()
	function handleLogout() {
		history.replace(ROUTES.LOGIN)
	}
	return (
		<div className='row'>
			{/*<div className="col-sm-10">*/}
			<button
				style={{
					marginLeft: "65.5rem",
					marginTop: "2rem",
					borderRadius: "1rem",
					width: "7rem",
					height: "2rem",
				}}
				onClick={handleLogout}
			>
				LOGOUT
			</button>
			<h2 style={{ marginLeft: "22rem", marginTop: "-2rem" }}>Hi {props.user.username}</h2>

			{/*</div>*/}
			<div className='col-sm-2'></div>
		</div>
	)
}
const mapStateToProps = (state) => {
	return {
		user: state.userReducer,
	}
}

export default connect(mapStateToProps, null)(Dashboard)
