import "../../styles/Common.css"
import "font-awesome/css/font-awesome.min.css"
import React from "react"
import { APIRequest } from "../../components/api-manager/apiRequest"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { ROUTES } from "../../routing/routeConstants"

import * as ReduxAction from "../../components/redux/actions/userAction"
import { connect } from "react-redux"
import Img from "../../content/images/GoldLogo.png"

class Login extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			islogged: false,
			input: {},
			errors: {},
			authError: false,
			disableClick: false,
		}
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
	}
	handleChange(event) {
		let input = this.state.input
		input[event.target.name] = event.target.value
		let disableFieldName = event.target.name
		this.setState((prevState) => ({
			...prevState,
			input,
			authError: false,
			errors: {
				[disableFieldName]: "",
			},
		}))
	}
	ForgotPassword = () => {
		this.props.history.push({ pathname: ROUTES.FORGOT_PASSWORD })
	}
	handleSubmit(e) {
		debugger
		e.preventDefault()

		let InputData = {
			Username: this.state.input.user_id,
			Password: this.state.input.user_password,
		}

		let validate = this.validate()
		if (validate === true) {
			let self = this

			//Disable button
			self.setState({
				disableClick: true,
			})

			//API Call
			APIRequest.getPostService(APIUrl.AUTHENTICATE_USER, InputData)
				.then((response) => {
					if (response.Succeeded) {
						let LPersonCompleted = false
						let LContactCompleted = false
						sessionStorage.clear()
						sessionStorage.setItem("NAME", response.Data.Name)
						sessionStorage.setItem("AUTH_TOKEN", response.Data.Token)
						sessionStorage.setItem("REFRESH_TOKEN", response.Data.RefreshToken)
						self.props.setUsername(response.Data.Name)
						self.props.setToken(response.Data.Token)
						self.props.setUserProfileID(response.Data.UserProfileID)
						self.props.setUserType(response.Data.UserType)
						self.props.setRefreshToken(response.Data.RefreshToken)
						self.props.setUserPermissions(response.Data.PermissionIDs)
						self.props.SetInvitedUserID(response.Data.UserId)
						sessionStorage.setItem("INVITED_ID", response.Data.UserId)
						sessionStorage.setItem("APP_PERMISSION", response.Data.PermissionIDs)
						sessionStorage.setItem("USER_TYPE", response.Data.UserType)
						sessionStorage.setItem("PROFILE_ID", response.Data.UserProfileID)
						//history.push(ROUTES.LAYOUT);
						if (response.Data.IsProfileCompleted === 1 && response.Data.IsContactCompleted === 1) {
							this.props.SetPersonCompleted(true)
							this.props.SetContactCompleted(true)
							LPersonCompleted = true
							LContactCompleted = true
							self.props.history.push({
								pathname: ROUTES.DASHBOARD,
								data: response,
							})
						} else {
							if (response.Data.IsProfileCompleted === 1) {
								this.props.SetPersonCompleted(true)
								LPersonCompleted = true
							} else {
								this.props.SetPersonCompleted(false)
								LPersonCompleted = false
							}
							if (response.Data.IsContactCompleted === 1) {
								this.props.SetContactCompleted(true)
								LContactCompleted = true
							} else {
								this.props.SetContactCompleted(false)
								LContactCompleted = false
							}
							self.props.history.push({
								pathname: ROUTES.PROFILE,
								data: response,
							})
						}

						sessionStorage.setItem("PersonalCompleted", LPersonCompleted)
						sessionStorage.setItem("ContactCompleted", LContactCompleted)
					} else {
						self.setState({
							authError: true,
							disableClick: false,
						})
					}
				})
				.catch((error) => {
					self.setState({
						disableClick: false,
					})
				})

			//axios({
			//  method: "POST",
			//  url: "/api/account/validateuser",
			//  headers: {
			//    Accept: "application/json, text/plain, */*",
			//    "Access-Control-Allow-Origin": "*",
			//    "Content-Type": "application/json",
			//  },
			//  data: InputData,
			//})
			//  .then(function (e) {
			//    if (e.data.success === true) {
			//      self.props.history.push({ pathname: "/Home", data: e.data });
			//    } else {
			//      self.setState({
			//        showInCorrectMsg: true,
			//      });
			//      //alert("incorrect username or password");
			//    }
			//  })
			//  .catch((error) => {
			//  });
		}
	}
	validate() {
		let input = this.state.input
		let errors = {}
		let isValid = true

		if (!input["user_id"]) {
			isValid = false
			errors["user_id"] = "Please Enter Your Mobile Number/Email Address."
		}

		/*if (typeof input["user_id"] !== "undefined") {

			var pattern = new RegExp(
				/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			)
			var mobpattern = new RegExp(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
			var test = pattern.test(input["user_id"])
			var test1 = mobpattern.test(input["user_id"])
			if (!pattern.test(input["user_id"]) && mobpattern.test(input["user_id"])) {
				isValid = true
			} else if (pattern.test(input["user_id"]) && !mobpattern.test(input["user_id"])) {
				isValid = true
			} else {
				isValid = false
				errors["user_id"] = "Please enter valid Mobile or Email."
			}
		} */

		if (!input["user_password"]) {
			isValid = false
			errors["user_password"] = "Please enter your password."
		}

		if (typeof input["user_password"] !== "undefined") {
			//var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})")
			//if (strongRegex.test(input["user_password"])) {
			//  isValid = false;
			//  errors["user_password"] = "Please add at least 6 charachter.";
			//}
		}
		this.setState({
			errors: errors,
		})

		return isValid
	}
	render() {
		return (
			<div className='row' style={{ float: "right" }}>
				<form onSubmit={this.handleSubmit} className='form-signin'>
					<div className='loginform'>
						<div className='loginlogo'>
							<img src={Img} alt='pic' width='193' height='130' marginLeft='25%'/>
						</div>
						<div className='row center form-group'>
							<div className='col-sm-12 text-center'>
								<h3 className='head'>Login</h3>
							</div>
						</div>
						<div className='row'>
							<div className='col-sm-12'>
								<div className='form-group' style={{ fontSize: "1.1rem" }}>
									<div className='inputWithIcon'>
										<input
											type='text'
											className='formcontrol'
											required='required'
											placeholder='Mobile or Email'
											name='user_id'
											onChange={this.handleChange}
											value={this.state.input.user_id}
										/>
										<span className='placeholder'></span>
										<i className='fa fa-user fa-lg fa-fw' aria-hidden='true'></i>
										{/* <i className="asterisk_input" aria-hidden="true" style={{marginLeft: "1rem"}}></i> */}
									</div>
									<div className='danger_msg'>{this.state.errors.user_id}</div>
									<div className='inputWithIcon'>
										<input
											type='password'
											className='formcontrol'
											required='required'
											name='user_password'
											onChange={this.handleChange}
											placeholder='Password'
											value={this.state.input.user_password}
										/>
										<span className='placeholder' style={{ marginLeft: "7.6rem" }}></span>
										<i className='fa fas fa-lock fa-lg fa-fw' aria-hidden='true'></i>
										{/* <i className="asterisk_input" aria-hidden="true" style={{marginLeft:"-1.8rem"}}></i> */}
									</div>
									<div className='danger_msg'>{this.state.errors.user_password}</div>
								</div>
								<button
									type='submit'
									id='btnlogin'
									className='loginbtn'
									onClick={this.handleSubmit}
									disabled={this.state.disableClick}
								>
									{" "}
									Login{" "}
								</button>
								<br></br>
								{this.state.authError ? (
									<div className='danger_msg auth-error'>{"Incorrect username or password"}</div>
								) : null}
								<div className='row'>
									<div className='col-sm-12'>
										<a
											className='labels'
											style={{ cursor: "pointer", marginLeft: "10rem" }}
											onClick={this.ForgotPassword}
										>
											Forgot Password ?
										</a>
									</div>
									<br></br>
									<br></br>
									<br></br>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}
const mapDispatchToProps = (dispatch) => {
	return {
		setAuthenticate: (isAuthenticate) => {
			dispatch(ReduxAction.authenticateUser(isAuthenticate))
		},
		setUsername: (username) => {
			dispatch(ReduxAction.addName(username))
		},
		setToken: (token) => {
			dispatch(ReduxAction.addToken(token))
		},
		setUserProfileID: (UserProfileID) => {
			dispatch(ReduxAction.addUserPofileId(UserProfileID))
		},
		setRefreshToken: (refreshToken) => {
			dispatch(ReduxAction.addRefreshToken(refreshToken))
		},
		setUserPermissions: (permissions) => {
			dispatch(ReduxAction.addPermission(permissions))
		},
		SetPersonCompleted: (PersonCompleted) => {
			dispatch(ReduxAction.setPersoanlCompleted(PersonCompleted))
		},
		SetContactCompleted: (ContactCompleted) => {
			dispatch(ReduxAction.setContactCompleted(ContactCompleted))
		},
		setUserType: (UserType) => {
			dispatch(ReduxAction.setUserType(UserType))
		},
		SetInvitedUserID: (InvitedUserID) => {
			dispatch(ReduxAction.setInvitedUserID(InvitedUserID))
		},
	}
}
export default connect(null, mapDispatchToProps)(Login)
