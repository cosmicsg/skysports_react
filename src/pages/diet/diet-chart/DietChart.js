import React, { useState, useRef, useEffect } from "react"
import ReactTable from "react-table"
import { Button } from "react-bootstrap"
import { faEdit, faEye, faTrashAlt, faDownload, faSleigh } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import AddDietChart from "./AddDietChart"
import DietChartFilter from "./DietChartFilter"
import ViewDietChart from "./ViewDietChart"
import DeleteDietChart from "./DeleteDietChart"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import ExportData from "../../../utils/ExportData"
import Pagination from "../../../utils/Pagination"
import { Permissions as Permission } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"
import { connect } from "react-redux"
import moment from "moment"

//Export label
const exportLabel = [
	{ columnName: "S.No", value: "SerialNo" },
	{ columnName: "Athlete Name", value: "AthleteName" },
	{ columnName: "Diet Chart", value: "DietChart" },
	{ columnName: "Sport", value: "SportName" },
	{ columnName: "Start Date", value: "StartDate" },
	{ columnName: "End Date", value: "EndDate" },

]

const DietChart = (props) => {
	const [permissionGroup, setPermissionGroup] = useState([])
	const NoDataProp = (props) => <span className='table-nodata-style'>No records found </span>

	const [viewDietChartProps, setViewDietChartProps] = useState({
		canViewDietChart: false,
		DietChartProps: {},
	})

	const [addDietChart, setAddDietChart] = useState({
		showPopup: false,
		isAdd: true,
	})

	const filterRef = useRef(null)

	const [editProps, setEditProps] = useState({})
	const [paginationProps, setPaginationProps] = useState({
		totalRecords: 0,
		totalCount: 0,
		totalPages: 0,
		pageSize: 10,
	})
	//Export fields properties
	const [exportExcelProps, setExportExcelProps] = useState({
		isExportExcel: false,
		exportData: [],
	})

	const [filterProps, setFilterProps] = useState({
		DietChartID: "",
		AthleteID: [],
		SportID: [],
		startDate:"",
		endDate: "",
	})

	const [isRefreshList, setRefreshList] = useState(false)

	const [deleteProps, setDeleteProps] = useState({
		canDelete: false,
		DietChartID: 0,
	})

	//AddDietChart
	const addNewDietChart = () => {
		setAddDietChart({
			...addDietChart,
			isAdd: true,
			showPopup: true,
		})
		setEditProps({})
	}

	//Update Close popup
	const updateClosePopup = (flag) => {
		setEditProps({})
		setAddDietChart({
			...addDietChart,
			showPopup: !addDietChart.showPopup,
		})
		if (flag === true && filterRef.current) filterRef.current.loadDietChartData()
	}

	//No records found
	const NoDataConst = (props) => <span className='table-nodata-style'>No records found </span>

	//Submit filter
	const submitFilter = (filter) => {
		setFilterProps({
			DietChartID: filter.DietChartID,
			AthleteID: filter.AthleteID,
			SportID: filter.SportID,
			startDate: filter.startDate,
			endDate: filter.endDate,
		})
		setRefreshList(true)
	}

	//Refresh list when isRefreshList flag is true
	useEffect(() => {
		let permissionArray = props.getAppPermissions()

		//Assign permission
		setPermissionGroup(permissionArray)

		if (isRefreshList) refreshList()
	}, [isRefreshList])

	//Refresh list
	const refreshList = (pageNo, isExport) => {
		let sortByName = "CreatedDate"
		let orderBy = "DESC"
		if (pageNo && pageNo.sorted !== undefined && pageNo.sorted.length > 0) {
			sortByName = pageNo.sorted[0].id
			if (pageNo.sorted[0].desc === true) {
				orderBy = "DESC"
			} else {
				orderBy = "ASC"
			}
		}
		let strStartDate = ""
		if (filterProps.startDate !== "") {
			strStartDate = moment(filterProps.startDate).format("DD MMM YY")
		}
		let strEndDate = ""
		if (filterProps.endDate !== "") {
			strEndDate = moment(filterProps.endDate).format("DD MMM YY")
		}

		let filterModel = {
			
			DietChart: filterProps.DietChartID,
			AthleteIDs: filterProps.AthleteID ? filterProps.AthleteID.join(",") : "",
			SportIDs: filterProps.SportID ? filterProps.SportID.join("") : "",
			StartDate: strStartDate,
			EndDate: strEndDate,
			PageNo: pageNo > 0 ? pageNo : 1,
			PageSize:
				isExport !== undefined && isExport !== null && isExport === true
					? paginationProps.totalRecords
					: paginationProps.pageSize,
			SortColumn: sortByName,
			SortOrder: orderBy,
		}
		APIRequest.getPostService(APIUrl.GET_DIET_CHART_LIST, filterModel).then((res) => {
			if (res.Data.length > 0 && isExport === true) {
				if (res.Succeeded)
					setExportExcelProps({
						...exportExcelProps,
						exportData: res.Data,
						isExportExcel: true,
					})
				setExportExcelProps({
					...exportExcelProps,
					isExportExcel: false,
				})
			} else {
				if (res.Succeeded) setMainListData(res.Data)
				if (res.Data[0]) {
					setPaginationProps({
						...paginationProps,
						totalCount: res.Data.length >= 1 ? res.Data.length : 0,
						totalPages: res.Data[0].TotalPages,
						totalRecords: res.Data[0].TotalRows,
					})
				} else {
					setPaginationProps({
						...paginationProps,
						totalPages: 0,
						totalRecords: 0,
						totalCount: 0,
					})
				}
				setRefreshList(false)
			}
		})
	}

	//Pagination call
	const paginationCall = (page) => {
		refreshList(page, false)
	}

	//Table column data limit
	const tableColumnDataLimit = (str) => {
		if (str !== null && str !== undefined)
			return str.length > 30 ? str.toString().substring(0, 27) + "..." : str
		else return ""
	}

	//sport column data limit
	const sportTableColumnDataLimit = (str) => {
		if (str !== null && str !== undefined)
			return str.length > 25 ? str.toString().substring(0, 25) + "..." : str
		else return ""
	}

	//Edit DietChart action
	const editDietChart = (row) => {
		setEditProps(row)
		setAddDietChart({
			...addDietChart,
			showPopup: !addDietChart.showPopup,
			isAdd: false,
		})
	}

	//View DietChart action
	const viewDietChart = (row) => {
		updateViewDietChartStatus(row, true)
	}

	//Update view DietChart status
	const updateViewDietChartStatus = (row, flag) => {
		setViewDietChartProps({
			canViewDietChart: flag === true ? flag : false,
			DietChartProps: row,
		})
	}

	//Delete DietChart action
	const deleteDietChart = (row) => {
		UpdateDeleteStatus(row.DietChartID, true)
	}

	//Update delete DietChart status
	const UpdateDeleteStatus = (id, flag) => {
		setDeleteProps({
			canDelete: flag === true ? true : false,
			DietChartID: id ? id : 0,
		})
	}

	//Call Export data
	const handleExportData = () => {
		refreshList(0, true)
	}

	const [mainListData, setMainListData] = useState([])

	const [isAdd, setAdd] = useState(false)

	const table = useRef(null)

	const columns = [
		{
			Header: () => <div className='mg-fd-tbl-head'>S.No</div>,
			accessor: "SerialNo",
			sortable: false,
			resizable: false,
			className: "tableheader wordwrap",
			headerClassName: "BoldText ColoredText",
			Cell: (row) => {
				return <div className='tabledata pt-0 mg-fd-tbl-head'>{row.value}</div>
			},
			width: Math.round(window.innerWidth * 0.04),
		},
		{
			Header: () => <div className='mg-fd-tbl-head'>Diet Chart</div>,
			accessor: "DietChart",
			resizable: false,
			className: "tableheader wordwrap",
			Cell: (row) => {
				return (
					<div className='tabledata pt-0 mg-fd-tbl-head'>
						{tableColumnDataLimit(row.original.DietChart)}
					</div>
				)
			},
		},
		{
			Header: () => <div className="mg-fd-tbl-head">Start Date</div>,
			accessor: "StartDate",
			resizable: false,
			className: "tableheader wordwrap",
			Cell: (row) => {
				return (
					<div className="tabledata pt-0 mg-fd-tbl-head">
						{tableColumnDataLimit(moment(row.original.StartDate).format("DD MMM YY"))}
					</div>
				)
			},
		},
		{
			Header: () => <div className="mg-fd-tbl-head">End Date</div>,
			accessor: "EndDate",
			resizable: false,
			className: "tableheader wordwrap",
			Cell: (row) => {
				return (
					<div className="tabledata pt-0 mg-fd-tbl-head">
						{tableColumnDataLimit(moment(row.original.EndDate).format("DD MMM YY"))}
					</div>
				)
			},
		},
		{
		Header: () => <div className="mg-fd-tbl-head">Sport</div>,
		accessor: "SportName",
		resizable: false,
		className: "tableheader wordwrap",
		Cell: (row) => {
			return (
				<div className="tabledata pt-0 mg-fd-tbl-head">
					{tableColumnDataLimit(row.original.SportName)}
				</div>
			)
		},
		},
		{
			Header: () => <div className='mg-fd-tbl-head'>Athlete Name</div>,
			accessor: "AthleteName",
			resizable: false,
			className: "tableheader wordwrap",
			headerClassName: "BoldText ColorText",
			Cell: (row) => {
				return (
					<div className='tabledata pt-0 mg-fd-tbl-head'>
						{tableColumnDataLimit(row.original.AthleteName)}
					</div>
				)
			},
		},
		
		{
			Header: () => <div className="postion-list-center">Action</div>,
			accessor: "Action",
			// className: "tableheader wordwrap",
			sortable: false,
			resizable: false,
			// textAlign: "center",
			
			filterable: false,
			Cell: (row) => {
				return (
					<div
						className='tabledata text-center'
						// style={{ display: "flex", justifyContent: "space-around" }}
					>
						{permissionGroup.includes(Permission.DIET_CHART_EDIT) &&
							props.user.InvitedUserID === row.original.CreatedByID ? (
							<FontAwesomeIcon
								title='Edit Diet Chart'
								className='mr-2 icon_eye'
								onClick={() => editDietChart(row.original)}
								icon={faEdit}
								style={{ cursor: "pointer" }}
								width='3rem'
							/>
						) : null}
						{/* {permissionGroup.includes(Permission.DIET_CHART_VIEW) && */}
							{/* props.user.InvitedUserID === row.original.CreatedByID ? ( */}
							<FontAwesomeIcon
								title='View Diet Chart'
								className='icon_eye mr-2'
								onClick={() => viewDietChart(row.original)}
								icon={faEye}
								style={{ cursor: "pointer" }}
								width='3rem'
							/>
						 {/* ) : null} */}
						{permissionGroup.includes(Permission.DIET_CHART_DELETE) &&
							props.user.InvitedUserID === row.original.CreatedByID ? (
							<FontAwesomeIcon
								title='Delete'
								className='mr-2 icon_Delete'
								onClick={() => deleteDietChart(row.original)}
								icon={faTrashAlt}
								style={{ cursor: "pointer" }}
								width='3rem'
							/>
						) : null}
					</div>
				)
			},
			width: Math.round(window.innerWidth * 0.07),
		},
	]

	return (
		<>
			<div className='mr-3 ml-3 '>
				<div className='row col-md-13 mt-3 form-group'>
					<div className='games_layout form-group col-md-3'>Diet Chart</div>
					{permissionGroup.includes(Permission.DIET_CHART_CREATE) ? (
						<div className='col-md-9 d-flex justify-content-end'>
							<Button className='btn_green' onClick={addNewDietChart}>
								Create Diet Chart
							</Button>
						</div>
					) : null}
				</div>
				<div className='form-group mg-fd-box mr-0 pr-0'>
					<DietChartFilter ref={filterRef} refreshList={submitFilter} />
				</div>
				<div>
					<ReactTable
						className='CosmicTable'
						data={mainListData}
						columns={columns}
						showPaginationTop={false}
						showPaginationBottom={false}
						minRows={0}
						onFetchData={refreshList}
						defaultPageSize={10}
						defaultPage={1}
						ref={table}
						sortable={true}
						multiSort={true}
						manual
						showPageSizeOptions={false}
						NoDataComponent={NoDataProp}
						defaultSorting={[
							{
								id: "id",
								desc: true,
							},
						]}
						manualPagination={true}
					/>
				</div>
				<div className='row col-sm-12 mt-2 pr-0'>
					<div className='col-sm-6 mt-1'>
						<label>
							<b>Total Records: </b>
							{paginationProps.totalRecords > 10
								? paginationProps.totalCount + " of " + paginationProps.totalRecords
								: paginationProps.totalRecords !== 0
									? paginationProps.totalRecords
									: 0}
						</label>
					</div>

				</div>
				{permissionGroup.includes(Permission.DIET_CHART_EXPORT) ? (
					<>
						<div className='row col-sm-12'>
							<div className='col-sm-6'>
								<Button className='btn_green mr-2' onClick={handleExportData}>
									<FontAwesomeIcon title='Export Data' color={"#FFFFFF"} icon={faDownload}>
										{" "}
									</FontAwesomeIcon>{" "}
									Export Excel
								</Button>
							</div>
							{paginationProps.totalPages > 0 ? (
								<div className='col-sm-6 d-flex justify-content-end pr-0'>
									<Pagination
										totalPages={paginationProps.totalPages}
										totalRecords={paginationProps.totalRecords}
										paginationCall={paginationCall}
									/>
								</div>
							) : null}
						</div>
						{exportExcelProps.isExportExcel ? (
							<ExportData
								data={exportExcelProps.exportData}
								label={exportLabel}
								filename='Diet Chart List'
							/>
						) : null}
					</>
				) : null}
			</div>
			{addDietChart.showPopup ? (
				<AddDietChart
					isAdd={addDietChart.isAdd}
					editProps={editProps}
					updatePopupState={updateClosePopup}
					refreshList={refreshList}
				/>
			) : null}
			{/* View Diet Chart Details */}
			{viewDietChartProps.canViewDietChart ? (
				<ViewDietChart
					viewProps={viewDietChartProps.DietChartProps}
					updateStatus={updateViewDietChartStatus}
				/>
			) : null}
			{deleteProps.canDelete ? (
				<DeleteDietChart
					closePopup={UpdateDeleteStatus}
					refreshList={refreshList}
					DietChartID={deleteProps.DietChartID}
				/>
			) : null}{" "}
		</>
	)
}

const mapPropsToState = (state) => ({
	user: state.userReducer,
})

export default PermissionProvider(connect(mapPropsToState, null)(DietChart))
