import React, { useState, useEffect, useRef } from "react"
import { Input, Select, Row, Col, Switch, DatePicker, notification } from "antd"
import { Button, Modal } from "react-bootstrap"
import { faPlusSquare, faTrashAlt } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { showNotification, APIRequest } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import ReactTable from "react-table"
import * as $ from "jquery"
import moment from "moment"

const { Option } = Select

const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete ' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	} else if (flag === 2) {
		notification[type]({
			message: description,
			description: "",
		})
	}
}

const NoDataConst = (props) => (
	<span className='table-nodata-style'>
		No records found Please check your connection or change your filters{" "}
	</span>
)

const GetDate = (date) => {
	if (date) return new Date(date).setHours(0, 0, 0, 0)
	return new Date().setHours(0, 0, 0, 0)
}

const AddDietChart = (props) => {
	const [isAdd] = useState(props.isAdd)

	useEffect(() => {
		//Load sports data
		if (Object.keys(props.editProps).length > 0 && !props.isAdd) {
			APIRequest.getGetService(
				APIUrl.GET_ADDED_DIET_CHART_BY_CHART_ID + "/" + props.editProps.DietChartID
			).then((res) => {
				//.split(",").map((item) => parseInt(item))
				setFields((prev) => ({
					...prev,
					DietChartID: props.editProps.DietChartID,
					ChartName: res.Data.ChartName ? res.Data.ChartName : "",
					StartDate: res.Data.StartDate ? moment(GetDate(res.Data.StartDate)) : "",
					EndDate: res.Data.EndDate ? moment(GetDate(res.Data.EndDate)) : "",
					SportID: res.Data.SportID.split(",").map((item) => parseInt(item)),
					ChartType: res.Data.ChartType,
					AthleteName:
						res.Data.AthleteName !== ""
							? res.Data.AthleteName.split(",").map((item) => parseInt(item))
							: [],
				}))
				if (res.Data.BreakFastData.length > 0) {
					let BreakFast = res.Data.BreakFastData[0]
					if (BreakFast.FoodId > 0) {
						$(".BreakFastFood").removeClass("hidden")
						setBreakFast(res.Data.BreakFastData)
					}
				}
				if (res.Data.MorningSnackData.length > 0) {
					let MorningFast = res.Data.MorningSnackData[0]
					if (MorningFast.FoodId > 0) {
						$(".handleAddMorningRow").removeClass("hidden")
						setMorningSnack(res.Data.MorningSnackData)
					}
				}

				if (res.Data.LunchData.length > 0) {
					let LunchFast = res.Data.LunchData[0]
					if (LunchFast.FoodId > 0) {
						$(".handleAddLunchRow").removeClass("hidden")
						setLunchSnack(res.Data.LunchData)
					}
				}

				if (res.Data.EveningSnackData.length > 0) {
					let EveningFast = res.Data.EveningSnackData[0]
					if (EveningFast.FoodId > 0) {
						$(".handleAddEveningRow").removeClass("hidden")
						setEveningSnack(res.Data.EveningSnackData)
					}
				}

				if (res.Data.DinnerData.length > 0) {
					let DinnerFast = res.Data.DinnerData[0]
					if (DinnerFast.FoodId > 0) {
						$(".handleAddDinnerRow").removeClass("hidden")
						setDinnerSnack(res.Data.DinnerData)
					}
				}
				if (res.Data.SportID != null) {
					APIRequest.getGetService(
						APIUrl.GET_ATHLETE_DROPDOWN_BY_SPORT_ID + "/" + res.Data.SportID.toString()
					).then((res) => {
						if (res.Succeeded) {
							setAthleteData(res.Data)
						}
					})
				}
			})
		}

		APIRequest.getGetService(APIUrl.GET_SPORT_DROPDOWN_LIST).then((res) => {
			if (res.Succeeded) {
				setSportData(res.Data)
			}
		})

		APIRequest.getGetService(APIUrl.GET_BREAK_FAST_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setBreakFastData(res.Data)
			}
		})

		APIRequest.getGetService(APIUrl.GET_MORNING_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setMorningSnackData(res.Data)
			}
		})

		APIRequest.getGetService(APIUrl.GET_LUNCH_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setLunchSnackData(res.Data)
			}
		})

		APIRequest.getGetService(APIUrl.GET_EVENING_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setEveningSnackData(res.Data)
			}
		})

		APIRequest.getGetService(APIUrl.GET_DINNER_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setDinnerSnackData(res.Data)
			}
		})
	}, [])

	const table = useRef(null)

	const [errorProps, setErrorProps] = useState({})

	const [fields, setFields] = useState({
		DietChartID: 0,
		ChartName: "",
		StartDate: "",
		EndDate: "",
		SportID: [],
		ChartType: false,
		AthleteName: [],
	})
	const [deleteModel, setDeleteModel] = useState({
		isOpen: false,
		isDeleteId: 0,
		isDeleteflag: 0,
		isDeleteRowId: 0,
	})
	const [breakFast, setBreakFast] = useState([
		{
			index: 0,
			FoodId: 0,
			FoodName: [],
			Quantity: "",
		},
	])
	const [breakFastRowCount, setBreakFastRowCount] = useState(0)
	const [addbreakFastText, setAddBreakFastText] = useState("")
	const [breakFastData, setBreakFastData] = useState([])

	const [morningSnack, setMorningSnack] = useState([
		{
			index: 0,
			FoodId: 0,
			FoodName: [],
			Quantity: "",
		},
	])
	const [morningSnackRowCount, setMorningSnackRowCount] = useState(0)
	const [addmorningSnackText, setAddMorningSnackText] = useState("")
	const [morningSnackData, setMorningSnackData] = useState([])

	const [lunchSnack, setLunchSnack] = useState([
		{
			index: 0,
			FoodId: 0,
			FoodName: [],
			Quantity: "",
		},
	])
	const [lunchSnackRowCount, setLunchSnackRowCount] = useState(0)
	const [addlunchSnackText, setAddLunchSnackText] = useState("")
	const [lunchSnackData, setLunchSnackData] = useState([])

	const [eveningSnack, setEveningSnack] = useState([
		{
			index: 0,
			FoodId: 0,
			FoodName: [],
			Quantity: "",
		},
	])
	const [eveningSnackRowCount, setEveningSnackRowCount] = useState(0)
	const [addeveningSnackText, setAddEveningSnackText] = useState("")
	const [eveningSnackData, setEveningSnackData] = useState([])

	const [dinnerSnack, setDinnerSnack] = useState([
		{
			index: 0,
			FoodId: 0,
			FoodName: [],
			Quantity: "",
		},
	])
	const [dinnerSnackRowCount, setDinnerSnackRowCount] = useState(0)
	const [adddinnerSnackText, setAddDinnerSnackText] = useState("")
	const [dinnerSnackData, setDinnerSnackData] = useState([])

	const [selected, setSelected] = useState({})
	const [sportData, setSportData] = useState([])
	const [athleteData, setAthleteData] = useState([])
	const [athleteDDSelect, setAthleteDDSelect] = useState(false)
	const [errors, setErrors] = useState({})

	//Clear Properties
	const clearProperties = () => {
		setFields({
			DietChartID: 0,
			ChartName: "",
			StartDate: "",
			EndDate: "",
			SportID: [],
			AthleteName: [],
			ChartType: false,
		})
		setErrorProps({})
		setSportData([])
		setAthleteData([])
		setAthleteDDSelect([])
		//setSearchKey("")
	}
	//Close popup
	const close = (flag) => {
		clearProperties()
		closePopup(flag)
	}
	const closePopup = () => {
		props.updatePopupState()
	}

	const athelteAvailable = () => {
		if (fields.SportID != "" && fields.StartDate != "" && fields.EndDate != "") {
			APIRequest.getGetService(
				APIUrl.GET_ATHLETE_DROPDOWN_BY_SPORT_ID + "/" + fields.SportID.toString() + "/" + moment(fields.StartDate).format("YYYY-MM-DD") + "/" + moment(fields.EndDate).format("YYYY-MM-DD")
			).then((res) => {
				if (res.Succeeded) {
					setAthleteData(res.Data)
				}
			})
		}
	}

	useEffect(() => {
		athelteAvailable();

	}, [fields.EndDate, fields.SportID, fields.StartDate]);
	const onChange = (name, event) => {
		if (name.trim() === "ChartType") {
			setAthleteDDSelect(event)
			setFields({ ...fields, [name]: event, AthleteName: [] })
		} else if (name.trim() === "StartDate") {
			// setFields(event)
			setFields({ ...fields, [name]: event, EndDate: "" })
		} else if (name.trim() === "EndDate") {
			setFields({ ...fields, [name]: event, })
		} else if (name.trim() === "SportID") {
			let sportId = event

			setFields({
				...fields,
				[name]: event, AthleteName: []
			})
		}else if (name.trim() === "AthleteName") {
			setFields({
				...fields,
				[name]: event,
			})
		} else {
			setFields({
				...fields,
				[name]: event.target.value,
			})
		}

		setErrors({
			...errors,
			[name]: "",
		})
	}

	//Assign date
	const assignDate = (name, dateString, val) => {
		//update date value by calling onChange function
		let temp = val ? moment(GetDate(val)) : ""
		onChange(name, temp)
	}

	//Disable Start date selection
	const disabledStartDateSelection = (current) => {
		let today = moment(GetDate())
		return current < today
	}

	//Disable End date selection
	const disabledDateEndDateSelection = (current) => {
		if (fields.StartDate) return current < moment(fields.StartDate)
		else return current
	}


	//BreakFast Column
	const renderEditablebreakFastQuantity = (cellInfo) => {
		let cellValue = ""

		if (breakFast.length > cellInfo.index) {
			cellValue = breakFast[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"quantity_tbl" + cellInfo.index}
				className={"tbl_OtherInfo quantity_tbl" + cellInfo.index}
			>
				<Input
					size='large'
					autoComplete='off'
					type='text'
					name='Quantity'
					value={cellValue}
					id={"breakFastQuantity" + cellInfo.index}
					onChange={(e) => handlebreakFastTextFieldsChange(e, cellInfo)}
					bordered={false}
					className='textfiled w-80'
					placeholder='Enter Quantity'
				></Input>
			</div>
		)
	}

	const renderEditablebreakFastName = (cellInfo) => {
		let cellValue = ""
		if (breakFast.length > cellInfo.index) {
			cellValue = breakFast[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"Food_tbl" + cellInfo.index} className={"tbl_OtherInfo Food_tbl" + cellInfo.index}>
				<Select
				// getPopupContainer={trigger=> trigger.parentNode}
					className='textfiled w-80'
					bordered={false}
					maxTagCount="responsive"
					placeholder='Select Name'
					name='FoodName'
					size={200}
					value={cellValue}
					showSearch
					// allowClear
					showArrow
					onChange={(val, T) => handlebreakFastDropdownChange(cellInfo, val, T)}
					optionFilterProp='children'
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{breakFastData.map((item, index) => {
						return (
							<Option disabled={breakFastfilter(item.OptionID)} key={index} value={item.OptionID} textData={item.OptionData}>
								{item.OptionData}
							</Option>
						)
					})}
				</Select>
			</div>
		)
	}
	const breakFastfilter = (id) => {
		let result = false;
		let data = breakFast.filter((item) => (item.FoodId === id))

		if (data.length > 0) {
			result = true;
		}
		return result
	}
	const handlebreakFastDropdownChange = (cellInfo, event, text) => {
		let breakFastLists = [...breakFast]
		breakFastLists[cellInfo.index]["FoodId"] = event
		breakFastLists[cellInfo.index]["FoodName"] = text.textData
		setBreakFast(breakFastLists)
	}
	const handlebreakFastTextFieldsChange = (event, cellInfo) => {
		let breakFastLists = [...breakFast]
		breakFastLists[cellInfo.index]["Quantity"] = event.target.value
		setBreakFast(breakFastLists)
		setTimeout(() => document.getElementById("breakFastQuantity" + cellInfo.index).focus())
	}

	const breakFastcolumns = [
		{
			Header: () => <div className='pl-2'>Name</div>,
			accessor: "FoodName",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditablebreakFastName,
			style: {
				textAlign: "center",
			},
		},
		{
			Header: () => <div className='pl-2'>Quantity</div>,
			accessor: "Quantity",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditablebreakFastQuantity,
			style: {
				textAlign: "center",
			},
		},
	]
	const openModal = (rowid, flg, id) => {
		setDeleteModel({ isDeleteRowId: rowid, isOpen: true, isDeleteId: id, isDeleteflag: flg })
	}

	const closeModal = () => {
		setDeleteModel({ isOpen: false })
	}

	const deleteBreakFaseModal = async () => {
		let id = deleteModel.isDeleteId
		let Rowid = deleteModel.isDeleteRowId
		let fields = breakFast
		fields.splice(Rowid, 1)
		setBreakFast({ fields })
		closeModal()
		setErrors({ errors: [] })
		let rowId = selected
		if (rowId !== undefined) {
			delete rowId[id]
		}
		openNotificationWithIcon("success", "Record has been deleted", 1)
		if (breakFast.length === 0) {
			setBreakFastRowCount(0)
			setAddBreakFastText("Add Food")
		}
	}
	const handleAddBreakFastRow = (e) => {
		$(".BreakFastFood").removeClass("hidden")
		let errors = {}
		e.preventDefault()
		let count = breakFastRowCount

		let rowData = breakFast[breakFast.length - 1]
		count = rowData.index
		if (rowData.FoodId !== 0) {
			let breakFastLists = breakFast
			let temp = []

			if (rowData > 1 && breakFastLists[rowData - 1] !== undefined) {
				let tempData = breakFastLists[rowData - 1]

				let temp = breakFastLists.filter(
					(x, index) =>
						index !== rowData - 1 &&
						x.FoodId === tempData.FoodId &&
						x.FoodName === tempData.FoodName &&
						x.Quantity === tempData.Quantity
				)
				let filteredArr = breakFastLists.reduce((acc, current) => {
					const x = acc.find(
						(item) =>
							item.FoodName === current.FoodName &&
							item.FoodId === current.FoodId &&
							item.Quantity === current.Quantity
					)
					if (!x) {
						return acc.concat([current])
					} else {
						return acc
					}
				}, [])
				if (temp.length > 0 || filteredArr.length !== rowData) {
					errors["breakFastLists"] = "Duplicate Food not allowed."

					setErrors({ errors })
					return
				}
			}
			if (rowData >= 1) {
				let empltytemp = breakFastLists.filter(
					(x) => x.Quantity === "" && x.FoodName === "" && x.FoodId === 0
				)
				if (empltytemp.length > 0) {
					errors["breakFastLists"] = "Please enter the BreakFast details."

					setErrors({ errors })
					return
				}
			}
			if (rowData !== 0 && temp.length === 0) {
				setAddBreakFastText("Add another Food")
			}
			let rowcount = count + 1
			let tableRowIndex = parseFloat(rowcount)
			var updatedRows = [...breakFast]
			updatedRows[tableRowIndex] = { index: tableRowIndex, FoodId: 0, FoodName: "", Quantity: "" }
			setBreakFast(updatedRows)
			setBreakFastRowCount(tableRowIndex)
		}
	}

	//Morning Snack Column
	const renderEditableMorningSnackQuantity = (cellInfo) => {
		let cellValue = ""

		if (morningSnack.length > cellInfo.index) {
			cellValue = morningSnack[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"quantity_tbl" + cellInfo.index}
				className={"tbl_OtherInfo quantity_tbl" + cellInfo.index}
			>
				<Input
					size='large'
					type='text'
					name='Quantity'
					value={cellValue}
					id={"morningQuantity" + cellInfo.index}
					onChange={(e) => handlemorningSnackTextFieldsChange(e, cellInfo)}
					bordered={false}
					className='textfiled w-80'
					placeholder='Enter Quantity'
				></Input>
			</div>
		)
	}

	const renderEditableMorningSnackName = (cellInfo) => {
		let cellValue = ""
		if (morningSnack.length > cellInfo.index) {
			cellValue = morningSnack[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"Food_tbl" + cellInfo.index} className={"tbl_OtherInfo Food_tbl" + cellInfo.index}>
				<Select
					className='textfiled w-80'
					bordered={false}
					maxTagCount="responsive"
					placeholder='Select Name'
					name='FoodId'
					size={200}
					value={cellValue}
					showSearch
					showArrow
					// allowClear
					onChange={(val, T) => handlemorningSnackDropdownChange(cellInfo, val, T)}
					optionFilterProp='children'
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{morningSnackData.map((item, index) => {
						return (
							<Option disabled={morningSnackfilter(item.OptionID)} key={index} value={item.OptionID} textData={item.OptionData}>
								{item.OptionData}
							</Option>
						)
					})}
				</Select>
			</div>
		)
	}
	const morningSnackfilter = (id) => {
		let result = false;
		let data = morningSnack.filter((item) => (item.FoodId === id))

		if (data.length > 0) {
			result = true;
		}
		return result
	}


	const handlemorningSnackDropdownChange = (cellInfo, event, text) => {
		let morningSnackLists = [...morningSnack]
		morningSnackLists[cellInfo.index]["FoodId"] = event
		morningSnackLists[cellInfo.index]["FoodName"] = text.textData
		setMorningSnack(morningSnackLists)
	}
	const handlemorningSnackTextFieldsChange = (event, cellInfo) => {
		let morningSnackLists = [...morningSnack]
		morningSnackLists[cellInfo.index]["Quantity"] = event.target.value
		setMorningSnack(morningSnackLists)
		setTimeout(() => document.getElementById("morningQuantity" + cellInfo.index).focus())
	}
	const morningSnackcolumns = [
		{
			Header: () => <div className='pl-2'>Name</div>,
			accessor: "FoodName",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditableMorningSnackName,
			style: {
				textAlign: "center",
			},
		},
		{
			Header: () => <div className='pl-2'>Quantity</div>,
			accessor: "Quantity",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditableMorningSnackQuantity,
			style: {
				textAlign: "center",
			},
		},
	]

	const deleteMorningSnackModal = async () => {
		let id = deleteModel.isDeleteId
		let Rowid = deleteModel.isDeleteRowId
		let fields = morningSnack
		fields.splice(Rowid, 1)
		setMorningSnack({ fields })
		closeModal()
		setErrors({ errors: [] })
		let rowId = selected
		if (rowId !== undefined) {
			delete rowId[id]
		}
		openNotificationWithIcon("success", "Record has been deleted", 1)
		if (morningSnack.length === 0) {
			setMorningSnackRowCount(0)
			setAddMorningSnackText("Add Food")
		}
	}
	const handleAddMorningSnackRow = (e) => {
		$(".handleAddMorningRow").removeClass("hidden")
		let errors = {}
		e.preventDefault()
		let count = morningSnackRowCount
		let rowData = morningSnack[morningSnack.length - 1]
		count = rowData.index
		if (rowData.FoodId !== 0) {
			let morningSnackLists = morningSnack
			let temp = []
			if (rowData > 1 && morningSnackLists[rowData - 1] !== undefined) {
				let tempData = morningSnackLists[rowData - 1]

				let temp = morningSnackLists.filter(
					(x, index) =>
						index !== rowData - 1 &&
						x.FoodId === tempData.FoodId &&
						x.FoodName === tempData.FoodName &&
						x.Quantity === tempData.Quantity
				)
				let filteredArr = morningSnackLists.reduce((acc, current) => {
					const x = acc.find(
						(item) =>
							item.FoodName === current.FoodName &&
							item.FoodId === current.FoodId &&
							item.Quantity === current.Quantity
					)
					if (!x) {
						return acc.concat([current])
					} else {
						return acc
					}
				}, [])
				if (temp.length > 0 || filteredArr.length !== rowData) {
					errors["morningSnackLists"] = "Duplicate Food not allowed."

					setErrors({ errors })
					return
				}
			}
			if (rowData >= 1) {
				let empltytemp = morningSnackLists.filter(
					(x) => x.Quantity === "" && x.FoodName.length === "" && x.FoodId === 0
				)
				if (empltytemp.length > 0) {
					errors["morningSnackLists"] = "Please enter the MorningSnack details."

					setErrors({ errors })
					return
				}
			}
			if (rowData !== 0 && temp.length === 0) {
				setAddMorningSnackText("Add another Food")
			}
			let rowcount = count + 1
			let tableRowIndex = parseFloat(rowcount)
			var updatedRows = [...morningSnack]
			updatedRows[tableRowIndex] = { index: tableRowIndex, FoodId: 0, FoodName: "", Quantity: "" }
			setMorningSnack(updatedRows)
			setMorningSnackRowCount(tableRowIndex)
		}
	}

	//Lunch Snack Column
	const renderEditableLunchSnackQuantity = (cellInfo) => {
		let cellValue = ""

		if (lunchSnack.length > cellInfo.index) {
			cellValue = lunchSnack[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"quantity_tbl" + cellInfo.index}
				className={"tbl_OtherInfo quantity_tbl" + cellInfo.index}
			>
				<Input
					size='large'
					autoComplete='off'
					type='text'
					name='Quantity'
					value={cellValue}
					id={"LunchSnackQuantity" + cellInfo.index}
					onChange={(e) => handlelunchSnackTextFieldsChange(e, cellInfo)}
					bordered={false}
					className='textfiled w-80'
					placeholder='Enter Quantity'
				></Input>
			</div>
		)
	}

	const renderEditableLunchSnackName = (cellInfo) => {
		let cellValue = ""
		if (lunchSnack.length > cellInfo.index) {
			cellValue = lunchSnack[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"Food_tbl" + cellInfo.index} className={"tbl_OtherInfo Food_tbl" + cellInfo.index}>
				<Select
					className='textfiled w-80'
					bordered={false}
					maxTagCount="responsive"
					placeholder='Select Name'
					name='FoodId'
					size={200}
					value={cellValue}
					showSearch
					showArrow
					// allowClear
					onChange={(val, T) => handlelunchSnackDropdownChange(cellInfo, val, T)}
					optionFilterProp='children'
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{lunchSnackData.map((item, index) => {
						return (
							<Option disabled={lunchSnackfilter(item.OptionID)} key={index} value={item.OptionID} textData={item.OptionData}>
								{item.OptionData}
							</Option>
						)
					})}
				</Select>
			</div>
		)
	}

	const lunchSnackfilter = (id) => {
		let result = false;
		let data = lunchSnack.filter((item) => (item.FoodId === id))

		if (data.length > 0) {
			result = true;
		}
		return result
	}

	const handlelunchSnackDropdownChange = (cellInfo, event, text) => {
		let lunchSnackLists = [...lunchSnack]
		lunchSnackLists[cellInfo.index]["FoodId"] = event
		lunchSnackLists[cellInfo.index]["FoodName"] = text.textData
		setLunchSnack(lunchSnackLists)
	}
	const handlelunchSnackTextFieldsChange = (event, cellInfo) => {
		let lunchSnackLists = [...lunchSnack]
		lunchSnackLists[cellInfo.index]["Quantity"] = event.target.value
		setLunchSnack(lunchSnackLists)
		setTimeout(() => document.getElementById("LunchSnackQuantity" + cellInfo.index).focus())
	}

	const lunchSnackcolumns = [
		{
			Header: () => <div className='pl-2'>Name</div>,
			accessor: "FoodName",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditableLunchSnackName,
			style: {
				textAlign: "center",
			},
		},
		{
			Header: () => <div className='pl-2'>Quantity</div>,
			accessor: "Quantity",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditableLunchSnackQuantity,
			style: {
				textAlign: "center",
			},
		},
	]

	const deleteLunchSnackModal = async () => {
		let id = deleteModel.isDeleteId
		let Rowid = deleteModel.isDeleteRowId
		let fields = lunchSnack
		fields.splice(Rowid, 1)
		setLunchSnack({ fields })
		closeModal()
		setErrors({ errors: [] })
		let rowId = selected
		if (rowId !== undefined) {
			delete rowId[id]
		}
		openNotificationWithIcon("success", "Record has been deleted", 1)
		if (lunchSnack.length === 0) {
			setLunchSnackRowCount(0)
			setAddLunchSnackText("Add Food")
		}
	}
	const handleAddLunchSnackRow = (e) => {
		$(".handleAddLunchRow").removeClass("hidden")
		let errors = {}
		e.preventDefault()
		let count = lunchSnackRowCount
		let rowData = lunchSnack[lunchSnack.length - 1]
		count = rowData.index
		if (rowData.FoodId !== 0) {
			let lunchSnackLists = lunchSnack
			let temp = []
			if (rowData > 1 && lunchSnackLists[rowData - 1] !== undefined) {
				let tempData = lunchSnackLists[rowData - 1]

				let temp = lunchSnackLists.filter(
					(x, index) =>
						index !== rowData - 1 &&
						x.FoodId === tempData.FoodId &&
						x.FoodName === tempData.FoodName &&
						x.Quantity === tempData.Quantity
				)
				let filteredArr = lunchSnackLists.reduce((acc, current) => {
					const x = acc.find(
						(item) =>
							item.FoodName === current.FoodName &&
							item.FoodId === current.FoodId &&
							item.Quantity === current.Quantity
					)
					if (!x) {
						return acc.concat([current])
					} else {
						return acc
					}
				}, [])
				if (temp.length > 0 || filteredArr.length !== rowData) {
					errors["lunchSnackLists"] = "Duplicate Food not allowed."

					setErrors({ errors })
					return
				}
			}
			if (rowData >= 1) {
				let empltytemp = lunchSnackLists.filter(
					(x) => x.Quantity === "" && x.FoodName.length === "" && x.FoodId === 0
				)
				if (empltytemp.length > 0) {
					errors["lunchSnackLists"] = "Please enter the LunchSnack details."

					setErrors({ errors })
					return
				}
			}
			if (rowData !== 0 && temp.length === 0) {
				setAddLunchSnackText("Add another Food")
			}
			let rowcount = count + 1
			let tableRowIndex = parseFloat(rowcount)
			var updatedRows = [...lunchSnack]
			updatedRows[tableRowIndex] = { index: tableRowIndex, FoodId: 0, FoodName: "", Quantity: "" }
			setLunchSnack(updatedRows)
			setLunchSnackRowCount(tableRowIndex)
		}
	}

	//Evening Snack Column
	const renderEditableEveningSnackQuantity = (cellInfo) => {
		let cellValue = ""

		if (eveningSnack.length > cellInfo.index) {
			cellValue = eveningSnack[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"quantity_tbl" + cellInfo.index}
				className={"tbl_OtherInfo quantity_tbl" + cellInfo.index}
			>
				<Input
					size='large'
					autoComplete='off'
					type='text'
					name='Quantity'
					value={cellValue}
					id={"EveningSnackQuantity" + cellInfo.index}
					onChange={(e) => handleeveningSnackTextFieldsChange(e, cellInfo)}
					bordered={false}
					className='textfiled w-80'
					placeholder='Enter Quantity'
				></Input>
			</div>
		)
	}

	const renderEditableEveningSnackName = (cellInfo) => {
		let cellValue = ""
		if (eveningSnack.length > cellInfo.index) {
			cellValue = eveningSnack[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"Food_tbl" + cellInfo.index} className={"tbl_OtherInfo Food_tbl" + cellInfo.index}>
				<Select
					className='textfiled w-80'
					bordered={false}
					maxTagCount="responsive"
					placeholder='Select Name'
					name='FoodId'
					size={200}
					value={cellValue}
					showSearch
					allowClear
					showArrow
					onChange={(val, T) => handleeveningSnackDropdownChange(cellInfo, val, T)}
					optionFilterProp='children'
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{eveningSnackData.map((item, index) => {
						return (
							<Option disabled={eveningSnackfilter(item.OptionID)} key={index} value={item.OptionID} textData={item.OptionData}>
								{item.OptionData}
							</Option>
						)
					})}
				</Select>
			</div>
		)
	}

	const eveningSnackfilter = (id) => {
		let result = false;
		let data = eveningSnack.filter((item) => (item.FoodId === id))

		if (data.length > 0) {
			result = true;
		}
		return result
	}

	const handleeveningSnackDropdownChange = (cellInfo, event, text) => {
		let eveningSnackLists = [...eveningSnack]
		eveningSnackLists[cellInfo.index]["FoodId"] = event
		eveningSnackLists[cellInfo.index]["FoodName"] = text.textData
		setEveningSnack(eveningSnackLists)
	}
	const handleeveningSnackTextFieldsChange = (event, cellInfo) => {
		let eveningSnackLists = [...eveningSnack]
		eveningSnackLists[cellInfo.index]["Quantity"] = event.target.value
		setEveningSnack(eveningSnackLists)
		setTimeout(() => document.getElementById("EveningSnackQuantity" + cellInfo.index).focus())
	}

	const eveningSnackcolumns = [
		{
			Header: () => <div className='pl-2'>Name</div>,
			accessor: "FoodName",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditableEveningSnackName,
			style: {
				textAlign: "center",
			},
		},
		{
			Header: () => <div className='pl-2'>Quantity</div>,
			accessor: "Quantity",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditableEveningSnackQuantity,
			style: {
				textAlign: "center",
			},
		},
	]

	const deleteEveningSnackModal = async () => {
		let id = deleteModel.isDeleteId
		let Rowid = deleteModel.isDeleteRowId
		let fields = eveningSnack
		fields.splice(Rowid, 1)
		setEveningSnack({ fields })
		closeModal()
		setErrors({ errors: [] })
		let rowId = selected
		if (rowId !== undefined) {
			delete rowId[id]
		}
		openNotificationWithIcon("success", "Record has been deleted", 1)
		if (eveningSnack.length === 0) {
			setEveningSnackRowCount(0)
			setAddEveningSnackText("Add Food")
		}
	}
	const handleAddEveningSnackRow = (e) => {
		$(".handleAddEveningRow").removeClass("hidden")
		let errors = {}
		e.preventDefault()
		let count = eveningSnackRowCount
		let rowData = eveningSnack[eveningSnack.length - 1]
		count = rowData.index
		if (rowData.FoodId !== 0) {
			let eveningSnackLists = eveningSnack
			let temp = []
			if (rowData > 1 && eveningSnackLists[rowData - 1] !== undefined) {
				let tempData = eveningSnackLists[rowData - 1]

				let temp = eveningSnackLists.filter(
					(x, index) =>
						index !== rowData - 1 &&
						x.FoodId === tempData.FoodId &&
						x.FoodName === tempData.FoodName &&
						x.Quantity === tempData.Quantity
				)
				let filteredArr = eveningSnackLists.reduce((acc, current) => {
					const x = acc.find(
						(item) =>
							item.FoodName === current.FoodName &&
							item.FoodId === current.FoodId &&
							item.Quantity === current.Quantity
					)
					if (!x) {
						return acc.concat([current])
					} else {
						return acc
					}
				}, [])
				if (temp.length > 0 || filteredArr.length !== rowData) {
					errors["eveningSnackLists"] = "Duplicate Food not allowed."

					setErrors({ errors })
					return
				}
			}
			if (rowData >= 1) {
				let empltytemp = eveningSnackLists.filter(
					(x) => x.Quantity === "" && x.FoodName.length === "" && x.FoodId === 0
				)
				if (empltytemp.length > 0) {
					errors["eveningSnackLists"] = "Please enter the EveningSnack details."

					setErrors({ errors })
					return
				}
			}

			if (rowData !== 0 && temp.length === 0) {
				setAddEveningSnackText("Add another Food")
			}

			let rowcount = count + 1
			let tableRowIndex = parseFloat(rowcount)
			var updatedRows = [...eveningSnack]
			updatedRows[tableRowIndex] = { index: tableRowIndex, FoodId: 0, FoodName: "", Quantity: "" }
			setEveningSnack(updatedRows)
			setEveningSnackRowCount(tableRowIndex)
		}
	}

	//Dinner Snack Column
	const renderEditableDinnerSnackQuantity = (cellInfo) => {
		let cellValue = ""

		if (dinnerSnack.length > cellInfo.index) {
			cellValue = dinnerSnack[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"quantity_tbl" + cellInfo.index}
				className={"tbl_OtherInfo quantity_tbl" + cellInfo.index}
			>
				<Input
					size='large'
					autoComplete='off'
					type='text'
					name='Quantity'
					value={cellValue}
					id={"DinnerSnackQuantity" + cellInfo.index}
					onChange={(e) => handledinnerSnackTextFieldsChange(e, cellInfo)}
					bordered={false}
					className='textfiled w-80'
					placeholder='Enter Quantity'
				></Input>
			</div>
		)
	}

	const renderEditableDinnerSnackName = (cellInfo) => {
		let cellValue = ""
		if (dinnerSnack.length > cellInfo.index) {
			cellValue = dinnerSnack[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"Food_tbl" + cellInfo.index} className={"tbl_OtherInfo Food_tbl" + cellInfo.index}>
				<Select
					className='textfiled w-80'
					bordered={false}
					maxTagCount="responsive"
					placeholder='Select Name'
					name='FoodId'
					size={200}
					value={cellValue}
					showSearch
					// allowClear
					showArrow
					onChange={(val, T) => handledinnerSnackDropdownChange(cellInfo, val, T)}
					optionFilterProp='children'
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{dinnerSnackData.map((item, index) => {
						return (
							<Option disabled={dinnerSnackfilter(item.OptionID)} key={index} value={item.OptionID} textData={item.OptionData}>
								{item.OptionData}
							</Option>
						)
					})}
				</Select>
			</div>
		)
	}

	const dinnerSnackfilter = (id) => {
		let result = false;
		let data = dinnerSnack.filter((item) => (item.FoodId === id))

		if (data.length > 0) {
			result = true;
		}
		return result
	}

	const handledinnerSnackDropdownChange = (cellInfo, event, text) => {
		let dinnerSnackLists = [...dinnerSnack]
		dinnerSnackLists[cellInfo.index]["FoodId"] = event
		dinnerSnackLists[cellInfo.index]["FoodName"] = text.textData
		setDinnerSnack(dinnerSnackLists)
	}
	const handledinnerSnackTextFieldsChange = (event, cellInfo) => {
		let dinnerSnackLists = [...dinnerSnack]
		dinnerSnackLists[cellInfo.index]["Quantity"] = event.target.value
		setDinnerSnack(dinnerSnackLists)
		setTimeout(() => document.getElementById("DinnerSnackQuantity" + cellInfo.index).focus())
	}

	const dinnerSnackcolumns = [
		{
			Header: () => <div className='pl-2'>Name</div>,
			accessor: "FoodName",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditableDinnerSnackName,
			style: {
				textAlign: "center",
			},
		},
		{
			Header: () => <div className='pl-2'>Quantity</div>,
			accessor: "Quantity",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditableDinnerSnackQuantity,
			style: {
				textAlign: "center",
			},
		},
	]

	const deleteDinnerSnackModal = async () => {
		let id = deleteModel.isDeleteId
		let Rowid = deleteModel.isDeleteRowId
		let fields = dinnerSnack
		fields.splice(Rowid, 1)
		setDinnerSnack({ fields })
		closeModal()
		setErrors({ errors: [] })
		let rowId = selected
		if (rowId !== undefined) {
			delete rowId[id]
		}
		openNotificationWithIcon("success", "Record has been deleted", 1)
		if (dinnerSnack.length === 0) {
			setDinnerSnackRowCount(0)
			setAddDinnerSnackText("Add Food")
		}
	}
	const handleAddDinnerSnackRow = (e) => {
		$(".handleAddDinnerRow").removeClass("hidden")
		let errors = {}
		e.preventDefault()
		let count = dinnerSnackRowCount
		let rowData = dinnerSnack[dinnerSnack.length - 1]
		count = rowData.index
		if (rowData.FoodId !== 0) {
			let dinnerSnackLists = dinnerSnack
			let temp = []
			if (rowData > 1 && dinnerSnackLists[rowData - 1] !== undefined) {
				let tempData = dinnerSnackLists[rowData - 1]

				let temp = dinnerSnackLists.filter(
					(x, index) =>
						index !== rowData - 1 &&
						x.FoodId === tempData.FoodId &&
						x.FoodName === tempData.FoodName &&
						x.Quantity === tempData.Quantity
				)
				let filteredArr = dinnerSnackLists.reduce((acc, current) => {
					const x = acc.find(
						(item) =>
							item.FoodName === current.FoodName &&
							item.FoodId === current.FoodId &&
							item.Quantity === current.Quantity
					)
					if (!x) {
						return acc.concat([current])
					} else {
						return acc
					}
				}, [])
				if (temp.length > 0 || filteredArr.length !== rowData) {
					errors["dinnerSnackLists"] = "Duplicate Food not allowed."

					setErrors({ errors })
					return
				}
			}
			if (rowData >= 1) {
				let empltytemp = dinnerSnackLists.filter(
					(x) => x.Quantity === 0 && x.FoodName.length === 0 && x.FoodId === 0
				)
				if (empltytemp.length > 0) {
					errors["dinnerSnackLists"] = "Please enter the DinnerSnack details."

					setErrors({ errors })
					return
				}
			}
			if (rowData !== 0 && temp.length === 0) {
				setAddDinnerSnackText("Add another Food")
			}

			let rowcount = count + 1
			let tableRowIndex = parseFloat(rowcount)
			var updatedRows = [...dinnerSnack]
			updatedRows[tableRowIndex] = { index: tableRowIndex, FoodId: 0, FoodName: "", Quantity: "" }
			setDinnerSnack(updatedRows)
			setDinnerSnackRowCount(tableRowIndex)
		}
	}
	const saveDietChart = () => {
		if (!CheckFieldsValidation()) {
			var InputData = []
			var obj = {}
			obj["DietChartID"] = fields.DietChartID
			obj["ChartName"] = fields.ChartName
			obj["StartDate"] = fields.StartDate
			obj["EndDate"] = fields.EndDate
			obj["SportID"] = fields.SportID.toString()
			obj["AthleteInviteID"] = fields.AthleteName.toString()
			obj["ChartType"] = fields.ChartType
			obj["BreakFastData"] = JSON.stringify(breakFast)
			obj["MorningSnackData"] = JSON.stringify(morningSnack)
			obj["LunchData"] = JSON.stringify(lunchSnack)
			obj["EveningSnackData"] = JSON.stringify(eveningSnack)
			obj["DinnerData"] = JSON.stringify(dinnerSnack)
			InputData.push(obj)
			APIRequest.getPostService(APIUrl.ADD_ATHLETE_DIET_CHART, InputData)
				.then((response) => {
					close(true)
					if (response.Succeeded === true) {
						props.refreshList()
						if (response.Data === "Already exists") {
							openNotificationWithIcon("success", "Athlete Sleep already Submitted.", 3)
						} else {
							openNotificationWithIcon("success", "Athlete Sleep Updated Successfully.", 2)
						}
					}
				})
				.catch((error) => { })
		}
	}
	//Check empty field validation
	const CheckFieldsValidation = () => {
		let isError = {}
		let flag = false
		if (fields.ChartName === "" || fields.ChartName === undefined || fields.ChartName === null) {
			flag = true
			isError["ChartName"] = "Please enter Chart Name"
		}
		if (fields.StartDate === "" || fields.StartDate === undefined || fields.StartDate === null) {
			flag = true
			isError["StartDate"] = "Please enter Start Date"
		}
		if (fields.EndDate === "" || fields.EndDate === undefined || fields.EndDate === null) {
			flag = true
			isError["EndDate"] = "Please enter End Date"
		}
		if (fields.SportID === undefined || fields.SportID === null || fields.SportID.length === 0) {
			flag = true
			isError["SportID"] = "Please select Sport"
		}
		if (
			fields.AthleteName === undefined ||
			fields.AthleteName === null ||
			fields.AthleteName.length === 0
		) {
			flag = true
			isError["AthleteName"] = "Please select Athlete"
		}
		let Breakfast = breakFast[0]
		let MorningSnack = morningSnack[0]
		let LunchSnack = lunchSnack[0]
		let EveningSnack = eveningSnack[0]
		let DinnerSnack = dinnerSnack[0]
		if (
			Breakfast.FoodId === 0 &&
			MorningSnack.FoodId === 0 &&
			LunchSnack.FoodId === 0 &&
			EveningSnack.FoodId === 0 &&
			DinnerSnack.FoodId === 0
		) {
			flag = true
			isError["FoodDetails"] = "Please Enter the Food items"
		}
		setErrorProps(isError)
		return flag
	}
	return (
		<>
			<Modal className='user-list-toggle-main1' size='xl' backdrop='static' show={true}>
				<Modal.Header className='pb-0 border-bottom-0 popup_header font-weight-bold'>
					<p>{isAdd ? "Add " : "Edit "} Diet Chart</p>
					<div className='col-md-1'>
						<div className='d-flex justify-content-end'>
							<button
								title='Close'
								type='button'
								className='close'
								data-dismiss='modal'
								area-aria-label='Close'
								onClick={close}
							>
								<span style={{ color: "#c29a37" }} area-hidden='true'>
									&times;
								</span>
							</button>
						</div>
					</div>
				</Modal.Header>
				<Modal.Body>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className="font-weight-bold popup_txt">
								Diet Name<span className='text-danger'>*</span>
							</label>
							<Input
								type='text'
								title='Diet Name'
								size='large'
								autoComplete='off'
								value={fields.ChartName}
								onChange={(val) => onChange("ChartName", val)}
								bordered={false}
								className='textfiled w-80'
								placeholder='Enter Item Name'
							/>
							<span className='text-danger'>{errorProps.ChartName}</span>
						</div>
						<div className='col-sm-4'>
							<label className="font-weight-bold popup_txt">
								Start Date<span className='text-danger'>*</span>
							</label>
							<DatePicker
							
							getPopupContainer={trigger=> trigger.parentNode}
								size='large'
								format='DD MMM YYYY'
								disabled={false}
								bordered={false}
								name='StartDate'
								selected={fields.StartDate}
								value={fields.StartDate}
								disabledDate={disabledStartDateSelection}
								onChange={(val) => onChange("StartDate", val)}
								defaultValue={moment()}
								className='myDatePicker datepickerfiled add-prog-date'
								placeholder='Select Start Date'
							/>
							<span className='text-danger'>{errorProps.StartDate}</span>
						</div>
						<div className='col-sm-4'>
							<label className="font-weight-bold popup_txt">
								End Date<span className='text-danger'>*</span>
							</label>

							<DatePicker
							getPopupContainer={trigger=> trigger.parentNode}
								className='myDatePicker datepickerfiled  add-prog-date'
								size='large'
								format='DD MMM YYYY'
								bordered={false}
								disabled={false}
								disabledDate={disabledDateEndDateSelection}
								name='EndDate'
								selected={fields.EndDate}
								value={fields.EndDate}
								onChange={(val) => onChange("EndDate", val)}
								defaultValue={moment()}
								placeholder='Select End Date'
							/>
							<span className='text-danger'>{errorProps.EndDate}</span>
						</div>
					</div>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className="font-weight-bold popup_txt">
								Sport<span className='text-danger'>*</span>
							</label>
							<Select
							getPopupContainer={trigger=> trigger.parentNode}
								title='Sport'
								className='textfiled'
								bordered={false}
								size='200'
								showSearch
								mode='multiple'
								maxTagCount="responsive"
								allowClear
								showArrow
								placeholder='Select Sport'
								value={fields.SportID}
								onChange={(val) => onChange("SportID", val)}
								optionFilterProp='children'
								filterSort={(optionA, optionB) =>
									optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
								}
							>
								{sportData.map((item, index) => (

									<Option key={index} value={item.OptionID}>
										{item.OptionData}
									</Option>
								))}
							</Select>
							<span className='text-danger'>{errorProps.SportID}</span>
						</div>
						<div className='col-sm-4'>
							<label className="font-weight-bold popup_txt">
								Athlete<span className='text-danger'>*</span>
							</label>
							{athleteDDSelect ? (
								<Select
								
								getPopupContainer={trigger=> trigger.parentNode}
									title='Athlete'
									className='textfiled'
									bordered={false}
									size='200'
									showSearch
									maxTagCount="responsive"
									allowClear
									showArrow
									placeholder='Select Athlete'
									value={fields.AthleteName}
									onChange={(val) => onChange("AthleteName", val)}
									optionFilterProp='children'
									filterSort={(optionA, optionB) =>
										optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
									}
								>
									{athleteData.map((item, index) => (
										<Option key={item.UserProfileID} value={item.UserProfileID}>
											{item.FullName}
										</Option>
									))}
								</Select>
							) : (
								<Select
								
								getPopupContainer={trigger=> trigger.parentNode}
									title='Athlete'
									className='textfiled'
									bordered={false}
									size='200'
									showSearch
									mode='multiple'
									maxTagCount="responsive"
									allowClear
									showArrow
									placeholder='Select Athlete'
									value={fields.AthleteName}
									onChange={(val) => onChange("AthleteName", val)}
									optionFilterProp='children'
									filterSort={(optionA, optionB) =>
										optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
									}
								>
									{athleteData.map((item, index) => (
										<Option key={item.UserProfileID} value={item.UserProfileID}>
											{item.FullName}
										</Option>
									))}
								</Select>
							)}

							<span className='text-danger'>{errorProps.AthleteName}</span>
						</div>
						<div className='col-sm-4'>
							<br />
							<br />
							<label className='mr-3 font-weight-bold popup_txt'>Athlete Type</label>
							<Switch
								checkedChildren='Individual'
								unCheckedChildren='Team'
								defaultChecked
								onChange={(val) => onChange("ChartType", val)}
							/>
						</div>
					</div>
					<Row className='add-chart-data' gutter={[48, 48]}>
						<Col className='chart-data-col' span={7}>
							<span>BreakFast</span>
							<br />
							<div className='col-sm-12 BreakFastFood hidden form-group'>
								<ReactTable
								getPopupContainer={trigger=> trigger.parentNode}
									data={breakFast}
									columns={breakFastcolumns}
									showPaginationTop={false}
									showPaginationBottom={false}
									minRows={0}
									ref={table}
									sortable={false}
									multiSort={false}
									//NoDataComponent={NoDataConst}
									manual
									style={{ overflow: "wrap" }}
									className='OtherInfoTable form-group'
									manualPagination={false}
								/>
							</div>

							<div className='otherInfo-Add-btn'>
								<button
									onClick={handleAddBreakFastRow}
									className='btn-default border-0 bg-white link'
								>
									<FontAwesomeIcon
										title='Add'
										className='add-diet-icons icon_eye'
										icon={faPlusSquare}
										style={{ cursor: "pointer" }}
									/>
								</button>
								<div>
									Add Available Breakfast
								</div>
							</div>
						</Col>
						<Col className='chart-data-col-empty' span={1}></Col>
						<Col className='chart-data-col' span={7}>
							<span>Morning Snack</span>
							<br />
							<div className='col-sm-12 handleAddMorningRow hidden form-group'>
								<ReactTable
									data={morningSnack}
									columns={morningSnackcolumns}
									showPaginationTop={false}
									showPaginationBottom={false}
									minRows={1}
									ref={table}
									sortable={false}
									multiSort={false}
									//NoDataComponent={NoDataConst}
									manual
									style={{ overflow: "wrap" }}
									className='OtherInfoTable form-group'
									manualPagination={false}
								/>
							</div>

							<div className='otherInfo-Add-btn'>
								<button
									onClick={handleAddMorningSnackRow}
									className='btn-default border-0 bg-white link'
								>
									<FontAwesomeIcon
										title='Add'
										className='add-diet-icons icon_eye'
										icon={faPlusSquare}
										style={{ cursor: "pointer" }}
									/>
								</button>
								<div>
									Add Available MorningSnack
								</div>
							</div>
						</Col>
						<Col className='chart-data-col-empty' span={1}></Col>
						<Col className='chart-data-col' span={7}>
							<span>Lunch</span>
							<br />
							<div className='col-sm-12 handleAddLunchRow hidden form-group'>
								<ReactTable
									data={lunchSnack}
									columns={lunchSnackcolumns}
									showPaginationTop={false}
									showPaginationBottom={false}
									minRows={1}
									ref={table}
									sortable={false}
									multiSort={false}

									//NoDataComponent={NoDataConst}
									manual
									style={{ overflow: "wrap" }}
									className='OtherInfoTable form-group'
									manualPagination={false}
								/>
							</div>

							<div className='otherInfo-Add-btn'>
								<button
									onClick={handleAddLunchSnackRow}
									className='btn-default border-0 bg-white link'
								>
									<FontAwesomeIcon
										title='Add'
										className='add-diet-icons icon_eye'
										icon={faPlusSquare}
										style={{ cursor: "pointer" }}
									/>
								</button>
								<div>
									Add Available Lunch
								</div>
							</div>
						</Col>

						<Col className='chart-data-col' span={7}>
							<span>Evening Snack</span>
							<br />
							<div className='col-sm-12 handleAddEveningRow hidden form-group'>
								<ReactTable
									data={eveningSnack}
									columns={eveningSnackcolumns}
									showPaginationTop={false}
									showPaginationBottom={false}
									minRows={1}
									ref={table}
									sortable={false}
									multiSort={false}
									//NoDataComponent={NoDataConst}
									manual
									style={{ overflow: "wrap" }}
									className='OtherInfoTable form-group'
									manualPagination={false}
								/>
							</div>

							<div className='otherInfo-Add-btn'>
								<button
									onClick={handleAddEveningSnackRow}
									className='btn-default border-0 bg-white link'
								>
									<FontAwesomeIcon
										title='Add'
										className='add-diet-icons icon_eye'
										icon={faPlusSquare}
										style={{ cursor: "pointer" }}
									/>
								</button>
								<div>
									Add Available EveningSnack
								</div>
							</div>
						</Col>
						<Col className='chart-data-col-empty' span={1}></Col>
						<Col className='chart-data-col' span={7}>
							<span>Dinner</span>
							<br />
							<div className='col-sm-12 handleAddDinnerRow hidden form-group'>
								<ReactTable
									data={dinnerSnack}
									columns={dinnerSnackcolumns}
									showPaginationTop={false}
									showPaginationBottom={false}
									minRows={1}
									ref={table}
									sortable={false}
									multiSort={false}
									//NoDataComponent={NoDataConst}
									manual
									style={{ overflow: "wrap" }}
									className='OtherInfoTable form-group'
									manualPagination={false}
								/>
							</div>

							<div className='otherInfo-Add-btn'>
								<button
									onClick={handleAddDinnerSnackRow}
									className='btn-default border-0 bg-white link'
								>
									<FontAwesomeIcon
										title='Add'
										className='add-diet-icons icon_eye'
										icon={faPlusSquare}
										style={{ cursor: "pointer" }}
									/>
								</button>
								<div>
									Add Available DinnerSnack
								</div>
							</div>
						</Col>
						<Col className='chart-data-col-empty' span={1}></Col>

						<span className='text-danger'>{errorProps.FoodDetails}</span>
					</Row>
					<div className='row col-sm-12 mt-4 form-group'>
						<div className='col-sm-12 d-flex justify-content-end '>

							{/* <Button className='btn_green mr-2' onClick={close}>
								Close
							</Button> */}

							{isAdd ? (
								<Button className='btn_green' onClick={saveDietChart}>
									Save
								</Button>
							) : (
								<Button className='btn_green' onClick={saveDietChart}>
									Update
								</Button>
							)}
						</div>
						<div className='col-sm-1'></div>
					</div>
				</Modal.Body>
			</Modal>
			{deleteModel.isOpen ? (
				<>
					<Modal size='sm' backdrop='static' show={deleteModel.isOpen} onHide={closeModal}>
						<div className='popup-content'>
							<div className='deletecircle'></div>
							<i className='fa fa-trash-o deleteicon'></i>
							<Modal.Body>
								<p className='pull-left' style={{ margin: "4rem 2rem" }}>
									{" "}
									<h5>You are about to delete an invite(s)</h5>
									<h6 style={{ color: "darkgray" }}>
										This will delete your invite(s) from the list
									</h6>
									<h6
										style={{
											color: "darkgray",
											marginRight: "7rem",
											marginBottom: "-3rem",
										}}
									>
										Are you sure?
									</h6>
								</p>
								<br />
								<div className='pull-right'>
									<Button className='btn_cancel mr-2' onClick={closeModal}>
										No
									</Button>
									<Button className='btn_green'>
										Yes
									</Button>
								</div>
							</Modal.Body>
						</div>
					</Modal>
				</>
			) : null}
		</>
	)
}
export default AddDietChart
