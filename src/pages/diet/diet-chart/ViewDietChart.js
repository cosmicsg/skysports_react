import React, { useState, useRef, useEffect } from "react"
import { Input, Select, Row, Col } from "antd"
import { Modal } from "react-bootstrap"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import * as $ from "jquery"
import { Label } from "reactstrap"
import moment from "moment"
import ReactTable from "react-table"

const { Option } = Select

const ViewDietChart = (props) => {
	const [viewProps] = useState(props.viewProps)
	const closeModel = () => {
		props.updateStatus()
	}

	const [breakFastData, setBreakFastData] = useState([])
	const [morningSnackData, setMorningSnackData] = useState([])
	const [lunchSnackData, setLunchSnackData] = useState([])
	const [eveningSnackData, setEveningSnackData] = useState([])
	const [dinnerSnackData, setDinnerSnackData] = useState([])
	useEffect(() => {
		APIRequest.getGetService(APIUrl.GET_BREAK_FAST_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setBreakFastData(res.Data)
			}
		})

		APIRequest.getGetService(APIUrl.GET_MORNING_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setMorningSnackData(res.Data)
			}
		})

		APIRequest.getGetService(APIUrl.GET_LUNCH_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setLunchSnackData(res.Data)
			}
		})

		APIRequest.getGetService(APIUrl.GET_EVENING_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setEveningSnackData(res.Data)
			}
		})

		APIRequest.getGetService(APIUrl.GET_DINNER_DROPDOWN_CATEGORY_ID).then((res) => {
			if (res.Succeeded) {
				setDinnerSnackData(res.Data)
			}
		})
	}, [])

	const table = useRef(null)
	//BreakFast Column
	const renderEditablebreakFastQuantity = (cellInfo) => {
		let cellValue = ""

		if (viewProps.BreakFastData.length > cellInfo.index) {
			cellValue = viewProps.BreakFastData[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"quantity_tbl" + cellInfo.index}
				className={"tbl_OtherInfo quantity_tbl" + cellInfo.index}
			>
				<Input
					size='large'
					autoComplete='off'
					type='number'
					name='Quantity'
					value={cellValue}
					bordered={false}
					className='textfiled w-80'
					placeholder='Enter Quantity'
				></Input>
			</div>
		)
	}

	const renderEditablebreakFastName = (cellInfo) => {
		let cellValue = ""
		if (viewProps.BreakFastData.length > cellInfo.index) {
			cellValue = viewProps.BreakFastData[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div id={"Food_tbl" + cellInfo.index} className={"tbl_OtherInfo Food_tbl" + cellInfo.index}>
				<Select
					className='textfiled w-80'
					bordered={false}
					maxTagCount={2}
					placeholder='Select Name'
					name='FoodName'
					size={200}
					value={cellValue}
					showSearch
					optionFilterProp='children'
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{breakFastData.map((item, index) => {
						return (
							<Option key={index} value={item.OptionID}>
								{item.OptionData}
							</Option>
						)
					})}
				</Select>
			</div>
		)
	}
	const breakFastcolumns = [
		{
			Header: () => <div className='pl-2'>Name</div>,
			accessor: "FoodName",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditablebreakFastName,
			style: {
				textAlign: "center",
			},
		},
		{
			Header: () => <div className='pl-2'>Quantity</div>,
			accessor: "Quantity",
			headerClassName: "BoldText ColoredText",
			align: "left",
			className: "tableheader other-info-tabledata",
			sortable: false,
			resizable: false,
			Cell: renderEditablebreakFastQuantity,
			style: {
				textAlign: "center",
			},
		},
	]
	return (
		<>
			<Modal show={true} backdrop='static' size='xl'>
				<Modal.Header style={{ height: "4rem" }}>
					<p>Diet Chart</p>
					<div className='col-sm-1'>
						<div className='d-flex justify-content-end'>
							<button title='Close' type='button' className='close' onClick={closeModel}>
								<span style={{ color: "#c29a37" }} area-hidden={true}>
									&times;
								</span>
							</button>
						</div>
					</div>
				</Modal.Header>
				<Modal.Body>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className='font-weight-bold popup_txt'>Diet Name </label>
							<br />
							<label>{viewProps.DietChart}</label>
						</div>
						<div className='col-sm-4'>
							<label className='font-weight-bold popup_txt'>Start Date </label>
							<br />
							<label>{moment(viewProps.StartDate).format("DD MMM YY")}</label>
						</div>
						<div className='col-sm-4'>
							<Label className='font-weight-bold popup_txt'>End Date </Label>
							<br />
							<label>{moment(viewProps.EndDate).format("DD MMM YY")}</label>
						</div>
					</div>
					<div className='row col-sm-12 form-group'>
						<div className='col-sm-4'>
							<label className='font-weight-bold popup_txt'>Sport </label>
							<br />
							<label>{viewProps.SportName}</label>
						</div>
						<div className='col-sm-4'>
							<label className='font-weight-bold popup_txt'>Athlete name </label>
							<br />
							<label>{viewProps.AthleteName}</label>
						</div>
						<div className='col-sm-4'>
							<label className='font-weight-bold popup_txt'>Athletes Type </label>
							<br />
							<label>{viewProps.ChartType ? "Team" : "Individual"}</label>
						</div>
					</div>

					<Row className='add-chart-data' gutter={[48, 48]}>
						<Col className='chart-data-col' span={7}>
							<span>BreakFast</span>
							<br />
							<div className='col-sm-12 form-group'>
								{viewProps.BreakFastData.map((c) => (
									<div>
										{c.FoodId > 0 ? (
											<div>
												<label>{c.index + 1}.</label>&nbsp;&nbsp;
												<label>{c.FoodName}</label>&nbsp;&nbsp;
												<label>{c.Quantity}</label>
											</div>
										) : null}
									</div>
								))}
							</div>
						</Col>
						<Col className='chart-data-col-empty' span={1}></Col>
						<Col className='chart-data-col' span={7}>
							<span>Morning Snack</span>
							<br />
							<div className='col-sm-12 form-group'>
								{viewProps.MorningSnackData.map((c) => (
									<div>
										{c.FoodId > 0 ? (
											<div>
												<label>{c.index + 1}.</label>&nbsp;&nbsp;
												<label>{c.FoodName}</label>&nbsp;&nbsp;
												<label>{c.Quantity}</label>
											</div>
										) : null}
									</div>
								))}
							</div>
						</Col>
						<Col className='chart-data-col-empty' span={1}></Col>
						<Col className='chart-data-col' span={7}>
							<span>Lunch</span>
							<br />
							<div className='col-sm-12 form-group'>
								{viewProps.LunchData.map((c) => (
									<div>
										{c.FoodId > 0 ? (
											<div>
												<label>{c.index + 1}.</label>&nbsp;&nbsp;
												<label>{c.FoodName}</label>&nbsp;&nbsp;
												<label>{c.Quantity}</label>
											</div>
										) : null}
									</div>
								))}
							</div>
						</Col>

						<Col className='chart-data-col' span={7}>
							<span>Evening Snack</span>
							<br />
							<div className='col-sm-12 form-group'>
								{viewProps.EveningSnackData.map((c) => (
									<div>
										{c.FoodId > 0 ? (
											<div>
												<label>{c.index + 1}.</label>&nbsp;&nbsp;
												<label>{c.FoodName}</label>&nbsp;&nbsp;
												<label>{c.Quantity}</label>
											</div>
										) : null}
									</div>
								))}
							</div>
						</Col>
						<Col className='chart-data-col-empty' span={1}></Col>
						<Col className='chart-data-col' span={7}>
							<span>Dinner</span>
							<br />
							<div className='col-sm-12 form-group'>
								{viewProps.DinnerData.map((c) => (
									<div>
										{c.FoodId > 0 ? (
											<div>
												<label>{c.index + 1}.</label>&nbsp;&nbsp;
												<label>{c.FoodName}</label>&nbsp;&nbsp;
												<label>{c.Quantity}</label>
											</div>
										) : null}
									</div>
								))}
							</div>
						</Col>
					</Row>
				</Modal.Body>
			</Modal>
		</>
	)
}
export default ViewDietChart
