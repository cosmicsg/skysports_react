import React, { memo } from "react"
import { Button, Modal } from "react-bootstrap"
import { APIRequest, showNotification } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { Message } from "../../../message/messageConstant"

const DeleteDietChart = (props) => {
	const closePopup = () => {
		props.closePopup(0, false)
	}

	//delete diet Chart
	const deleteDietChart = () => {
		if (props.DietChartID > 0) {
			APIRequest.getGetService(APIUrl.DELETE_DIET_CHART + "/" + props.DietChartID)
				.then((res) => {
					props.closePopup(0, false)
					let msgType = "error"
					if (res.Data.Success) {
						props.refreshList()
						msgType = "success"
					}
					showNotification(res.Data.Message, msgType)
				})
				.catch((err) => {})
		}
	}
	return (
		<>
			<Modal size='sm' backdrop='static' show={true}>
				<div className='popup-content'>
					<div className='deletecircle'></div>
					<i className='fa fa-trash-o deleteicon'></i>
					<Modal.Body>
						<p className='pull-left' style={{ margin: "4rem 2rem", textAlign: "left" }}>
							{" "}
							<h5>{Message.DELETE_TRAINING_DIET_CHART_HEADING}</h5>
							<h6 style={{ color: "darkgray" }}>{Message.DELETE_TRAINING_DIET_CHART_BODY}</h6>
							<h6
								style={{
									color: "darkgray",
									marginRight: "6rem",
									marginBottom: "-3rem",
								}}
							>
								{Message.INVITE_DELETE_CONFIRM1}
							</h6>
						</p>
						<div className='pull-right'>
							<Button className='btn_cancel mr-2' onClick={closePopup}>
								No
							</Button>
							<Button className='btn_green' onClick={deleteDietChart}>
								Yes
							</Button>
						</div>
					</Modal.Body>
				</div>
			</Modal>
		</>
	)
}
export default memo(DeleteDietChart)
