import { Card, Collapse, Button } from "react-bootstrap"
import React, { useState, useEffect } from "react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowDown, faArrowUp } from "@fortawesome/free-solid-svg-icons"
import { Select, Input, DatePicker } from "antd"
import moment from "moment"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"

const { Option } = Select
const DietChartFilter = React.forwardRef((props, ref) => {
	const [isExpand, setExpand] = useState(false)
	const toggleFilterBody = () => {
		setExpand(!isExpand)
	}

	const [DietChart, setDietChart] = useState("")
	const [sport, setSport] = useState([])
	const [athlete, setAthlete] = useState([])
	// const [startDate, setStartDate] = useState("")
	// const [endDate, setEndDate] = useState("")

	const [filterProps, setFilterProps] = useState({
		DietChartID: "",
		AthleteID: [],
		SportID: [],
		startDate: "",
		endDate: "",
	});

	const [disableSearch, setDisableSearch] = useState(true)

	const onChange = (name, val) => {
		setFilterProps({
			...filterProps,
			[name]: val,
			disableSearch
		})
		disableBtnSearch(name, val)
	}

	//Assign date
	const assignDate = (name, val) => {
		let temp = val ? moment(new Date(val)) : ""
		setFilterProps({
			...filterProps,
			[name]: temp,
		})
	}

	const disableBtnSearch = (name, val) => {
		if (!val) {
			if (
				(name === "DietChart" &&
					(
						(filterProps.AthleteID && filterProps.AthleteID.length !== 0) ||
						(filterProps.SportID && filterProps.SportID.length !== 0) ||
						(filterProps.startDate) ||
							(filterProps.endDate))) ||

				(name === "AthleteID" &&
					((filterProps.DietChartID && filterProps.DietChartID.length !== 0) ||
						(filterProps.SportID && filterProps.SportID.length !== 0)||
							(filterProps.startDate) ||
							(filterProps.endDate))) ||
				(name === "SportID" &&
					((filterProps.DietChartID && filterProps.DietChartID.length !== 0) ||
						(filterProps.AthleteID && filterProps.AthleteID.length !== 0) ||
							(filterProps.startDate) ||
							(filterProps.endDate))) ||
				(name === "startDate" &&
					((filterProps.DietChartID && filterProps.DietChartID.length !== 0) ||
						(filterProps.AthleteID && filterProps.AthleteID.length !== 0) ||
						(filterProps.SportID && filterProps.SportID.length !== 0) ||
							(filterProps.endDate))) ||
				(name === "endDate" &&
					((filterProps.DietChartID && filterProps.DietChartID.length !== 0) ||
						(filterProps.AthleteID && filterProps.AthleteID.length !== 0) ||
						(filterProps.SportID && filterProps.SportID.length !== 0)||
							(filterProps.startDate)))
			) {
				setDisableSearch(false)
			} else setDisableSearch(true)
		} else setDisableSearch(false)
	}

	//Lifecycle method, here it is loading the sport and program data loading for filter fields.
	useEffect(() => {
		APIRequest.getGetService(APIUrl.GET_DIET_CHART_LIST).then((res) => {
			setDietChart(res.Data)
		})
		APIRequest.getGetService(APIUrl.GET_SPORT_LIST).then((res) => {
			setSport(res.Data)
		})
		APIRequest.getGetService(APIUrl.GET_DIET_ATHLETE_LIST).then((res) => {
			setAthlete(res.Data)
		})
		// APIRequest.getGetService(APIUrl.GET_DATE_FULL_LIST).then((res) => {
		// 	setStartDate(res.Data)
		// })
		// APIRequest.getGetService(APIUrl.GET_DATE_LIST).then((res) => {
		// 	setEndDate(res.Data)
		// })
	}, [])

	const submitFilter = () => {
		callParentToRefreshList(filterProps)
	}

	//Call ProgramList component to refresh list
	const callParentToRefreshList = (filterProps) => {
		props.refreshList(filterProps)
	}

	const clearFilter = () => {
		let temp = {
			DietChartID: " ",
			AthleteID: [],
			SportID: [],
			startDate: "",
			endDate: "",
		};
		setFilterProps(temp)
		setDisableSearch(true)
		callParentToRefreshList(temp)
	}

	return (
		<>
			<Card className='w-100 manage-food-filter-container'>
				<Card.Header className='mg-fd-filter-header pl-1' onClick={toggleFilterBody}>
					<div className='float-left filter-heading-font'>Search Diet Chart</div>
					<div className='float-right'>
						{isExpand ? (
							<FontAwesomeIcon
								className='action-icon-font'
								color='#FFF'
								icon={faArrowUp}
								style={{ cursor: "pointer" }}
							/>
						) : (
							<FontAwesomeIcon
								className='action-icon-font'
								color='#FFF'
								icon={faArrowDown}
								style={{ cursor: "pointer" }}
							/>
						)}
					</div>
				</Card.Header>
				<Collapse in={isExpand}>
					<Card.Body>
						<div className='row col-sm-12 form-group'>
							<div className="col-sm-4">
								<label>Diet Chart</label>
								<Input
									size="large"
									autoComplete="off"
									type="text"
									name="DietChart"
									value={filterProps.DietChartID}
									onChange={(val) =>
										onChange(
											"DietChartID",
											val.target.value
										)
									}
									bordered={false}
									className="textfiled w-80"
									placeholder="Enter Item"
								>
								</Input>
							</div>
							<div className='col-sm-4'>
								<label>Sport</label>
								<Select
									className='textfiled'
									bordered={false}
									size='200'
									showSearch
									mode='multiple'
									maxTagCount="responsive"
									allowClear
									showArrow
									placeholder='Select Item'
									value={filterProps.SportID}
									onChange={(val) => onChange("SportID", val)}
									optionFilterProp='children'
									filterSort={(optionA, optionB) =>
										optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
									}
								>
									{sport.map((item, index) => (
										<Option key={index} value={item.OptionID}>
											{item.OptionData}
										</Option>
									))}
								</Select>
							</div>

							<div className='col-sm-4'>
								<label>Athlete</label>
								<Select
									className='textfiled'
									bordered={false}
									size='200'
									showSearch
									mode='multiple'
									maxTagCount="responsive"
									allowClear
									showArrow
									placeholder='Select Item'
									value={filterProps.AthleteID}
									onChange={(val) => onChange("AthleteID", val)}
									optionFilterProp='children'
									filterSort={(optionA, optionB) =>
										optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
									}
								>
									{athlete.map((item, index) => (
										<Option key={index} value={item.OptionID}>
											{item.OptionData}
										</Option>
									))}
								</Select>
							</div>
							</div>
							<div className="row col-sm-12 form-group">
								<div className="col-sm-4">
									<label>Start Date</label>
									<div>
										<DatePicker
											name="startDate"
											className="add-host-date-field program-datepicker pr-0 add-prog-date"
											format="DD MMM YYYY"
											size="large"
											bordered={false}
											allowClear
											value={filterProps.startDate}
											onChange={(date, dateString) =>
												assignDate("startDate", dateString)
											}
										/>
									</div>
								</div>
								<div className="col-md-4">
									<label>End Date</label>
									<DatePicker
										name="endDate"
										className="add-host-date-field program-datepicker pr-0 add-prog-date"
										format="DD MMM YYYY"
										size="large"
										bordered={false}
										allowClear={true}
										value={filterProps.endDate}
										onChange={(date, dateString) =>
											assignDate("endDate", dateString)
										}
									/>
								</div>
							</div>

						<div className='row col-sm-12 form-group'>
							<div className='col-md-12 mg-fd-filter-btn d-flex justify-content-end'>
								<Button className='btn_green mr-2' onClick={clearFilter}>
									Clear
								</Button>
								<Button className='btn_green' onClick={submitFilter} disabled={disableSearch}>
									Search
								</Button>

							</div>
						</div>
					</Card.Body>
				</Collapse>
			</Card>
		</>
	)
})
export default React.memo(DietChartFilter)
