import { Card, Collapse, Button } from "react-bootstrap";
import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown, faArrowUp } from "@fortawesome/free-solid-svg-icons";
import { Select } from "antd";
import {
  APIRequest,
} from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";

const { Option } = Select;

//
// This is Filter component for ManageFood list
//
const ManageFoodFilter = (props) => {
  const [isExpand, setExpand] = useState(false);
  const toggleFilterBody = () => {
    setExpand(!isExpand);
  };

  //
  // local props
  //
  const [categories, setCategory] = useState([]);
  const [foodItems, setFoodItems] = useState([]);

  const [filterProps, setFilterProps] = useState({
    category: [],
    items: [],
  });

  const [isDisableSearch, setDisableSearch] = useState(true);

  //
  // Lifecycle method, here we are loading the dropdown data for filter fields.
  //
  useEffect(() => {
    APIRequest.getGetService(APIUrl.GET_FOOD_CATEGORY).then((res) => {
      setCategory(res.Data);
    });

    APIRequest.getGetService(APIUrl.GET_FOOD_ITEMS).then((res) => {
      setFoodItems(res.Data);
    });
  }, []);

  //
  // handle the onchange events for filter parameters
  //
  const onChange = (name, val) => {
    setFilterProps({
      ...filterProps,
      [name]: val,
    });
    enableSubmitBtn(name, val);
  };

  //
  // check length of given values
  //
  const checkLength = (val) => {
    if (val && val.length > 0) return true;
    return false;
  };

  //
  // here handling the submit button enable or disable based on the value which is selecting from filter params
  //
  const enableSubmitBtn = (name, val) => {
    let flag = false;
    if (
      (name === "category" && checkLength(val)) ||
      (name !== "category" && checkLength(filterProps.category))
    ) {
      flag = true;
    }
    if (
      (name === "items" && checkLength(val)) ||
      (name !== "items" && checkLength(filterProps.items))
    ) {
      flag = true;
    }
    setDisableSearch(flag ? false : true);
  };

  //
  // here handling the submit filter
  //
  const submitFilter = () => {
    props.submitFilter(filterProps.category, filterProps.items);
  };

  // here handling the clear filter
  const clearFilter = () => {
    setFilterProps({
      category: [],
      items: [],
    });
    props.submitFilter(null, null);
    setDisableSearch(true);
  };

  //
  // return filter main component
  // 
  return (
    <>
      <Card className="w-100 manage-food-filter-container">
        <Card.Header className="mg-fd-filter-header pl-1" onClick={toggleFilterBody}>
          <div className="float-left filter-heading-font">Search Manage Food</div>
          <div className="float-right">
            {isExpand ? (
              <FontAwesomeIcon
                className="action-icon-font"
                color="#FFF"
                icon={faArrowUp}
                style={{ cursor: "pointer" }}
              />
            ) : (
              <FontAwesomeIcon
                className="action-icon-font"
                color="#FFF"
                icon={faArrowDown}
                style={{ cursor: "pointer" }}
              />
            )}
          </div>
        </Card.Header>
        <Collapse in={isExpand}>
          <Card.Body>
            <div className="row col-md-12">
              <div className="col-md-5">
                <label>Category</label>
                <Select
                  className="textfiled"
                  bordered={false}
                  size="200"
                  showSearch
                  mode="multiple"
                  maxTagCount="responsive"
                  allowClear
                  showArrow
                  placeholder="Select Category"
                  value={filterProps.category}
                  onChange={(val) => onChange("category", val)}
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {categories.map((item, index) => (
                    <Option key={index} value={item.OptionID}>
                      {item.OptionData}
                    </Option>
                  ))}
                </Select>
              </div>
              <div className="col-md-5">
                <label>Food</label>
                <Select
                  className="textfiled"
                  bordered={false}
                  size="200"
                  showSearch
                  mode="multiple"
                  maxTagCount="responsive"
                  allowClear
                  showArrow
                  placeholder="Select Food"
                  value={filterProps.items}
                  onChange={(val) => onChange("items", val)}
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {foodItems.map((item, index) => (
                    <Option key={index} value={item.OptionID}>
                      {item.OptionData}
                    </Option>
                  ))}
                </Select>
              </div>
              <div className="col-sm-12">
              <div className="col-md-13 mg-fd-filter-btn d-flex justify-content-end">
              <Button className="btn_green  mr-2" onClick={clearFilter}>
                  Clear
                </Button>
                <Button
                  className="btn_green"
                  onClick={submitFilter}
                  disabled={isDisableSearch}
                >
                  Search
                </Button>
                
              </div>
              </div>
            </div>
          </Card.Body>
        </Collapse>
      </Card>
    </>
  );
};

export default React.memo(ManageFoodFilter);
