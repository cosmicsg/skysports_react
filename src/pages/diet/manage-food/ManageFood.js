import React, { useState, useRef, useEffect } from "react"
import ManageFoodFilter from "./ManageFoodFilter"
import "../../../styles/DietManagement.css"
import ReactTable from "react-table"
import { Button } from "react-bootstrap"
import AddFood from "./AddFood"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { faEdit, faTrashAlt, faDownload } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import DeleteFoodItem from "./DeleteFoodItem"
import Pagination from "../../../utils/Pagination"
import ExportData from "../../../utils/ExportData"
import { Permissions } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"
import { connect } from "react-redux"

//Export label
const exportLabel = [
	{ columnName: "S.No", value: "SerialNo" },
	{ columnName: "Category", value: "Category" },
	{ columnName: "Food", value: "ItemName" },
	{ columnName: "Calories", value: "Calories" },
	{ columnName: "Protein", value: "Protein" },
	{ columnName: "Fat", value: "Fat" },
	{ columnName: "Carbohydrates", value: "CarboHydrate" },
]

//
// No data found properties
//
const NoDataProp = (props) => <span className='table-nodata-style'>No records found </span>

//
// which is used to handle column data length
//
const tableResizeColumn = (str) => {
	if (str) return str && str.length > 25 ? str.slice(25, "...") : str
	else return ""
}

//
// Manage Food component
//
const ManageFood = (props) => {
	const [mainListData, setMainListData] = useState([])

	const [isAdd, setAdd] = useState(false)

	const table = useRef(null)

	const [editProps, setEditProps] = useState({})

	const [permissions, setPermissions] = useState([])

	const [deleteProps, setDeleteProps] = useState({
		isOpen: false,
		foodItemID: 0,
	})

	const [modelProps, setModelProps] = useState({
		isAdd: false,
		isEdit: false,
	})

	const [paginationProps, setPaginationProps] = useState({
		totalRecords: 0,
		totalCount: 0,
		totalPages: 0,
		pageSize: 10,
	})

	//Export fields properties
	const [exportExcelProps, setExportExcelProps] = useState({
		isExportExcel: false,
		exportData: [],
	})

	//
	// Lifecycle method, here used to refresh the manage food list items
	//
	useEffect(() => {
		// Getting permissions from PermissionProvider component
		let tempPermission = props.getAppPermissions()
		setPermissions(tempPermission)

		//Refresh list
		refreshList()
	}, [])

	//
	// which is used to edit food item
	//
	const editFoodItem = (row) => {
		setEditProps(row)
		setModelProps({
			isAdd: false,
			isEdit: true,
		})
		setAdd(true)
	}

	//
	// which is used to delete food item
	//
	const deleteFoodItem = (row) => {
		setDeleteProps({
			foodItemID: row.FoodItemID,
			isOpen: true,
		})
	}

	//
	// close delete popup
	//
	const closeDeletePopup = (flag) => {
		if (flag === true) refreshList()
		setDeleteProps({
			foodItemID: 0,
			isOpen: false,
		})
	}

	//
	// Manage food table columns
	//
	const columns = [
		{
			Header: () => <div className='mg-fd-tbl-head'>S.No</div>,
			accessor: "SerialNo",
			sortable: false,
			resizable: false,
			className: "tableheader wordwrap",
			headerClassName: "BoldText ColoredText",
			Cell: (row) => {
				return <div className='tabledata pt-0 mg-fd-tbl-head'>{row.value}</div>
			},
			width: Math.round(window.innerWidth * 0.04),
		},
		{
			Header: () => <div className='mg-fd-tbl-head'>Category</div>,
			accessor: "Category",
			resizable: false,
			className: "tableheader wordwrap",
			headerClassName: "BoldText ColorText",
			Cell: (row) => {
				return (
					<div className='tabledata pt-0 mg-fd-tbl-head'>
						{tableResizeColumn(row.original.Category)}
					</div>
				)
			},
		},
		{
			Header: () => <div className='mg-fd-tbl-head'>Food</div>,
			accessor: "ItemName",
			className: "tableheader wordwrap",
			Cell: (row) => {
				return (
					<div className='tabledata pt-0 mg-fd-tbl-head'>
						{tableResizeColumn(row.original.ItemName)}
					</div>
				)
			},
		},
		{
			Header: () => <div className='mg-fd-tbl-head'>Calories</div>,
			accessor: "Calories",
			className: "tableheader wordwrap",
			Cell: (row) => {
				return (
					<div className='tabledata pt-0 mg-fd-tbl-head'>
						{tableResizeColumn(row.original.Calories)}
					</div>
				)
			},
		},
		{
			Header: () => <div className='mg-fd-tbl-head'>Protein</div>,
			accessor: "Protein",
			className: "tableheader wordwrap",
			Cell: (row) => {
				return (
					<div className='tabledata pt-0 mg-fd-tbl-head'>
						{tableResizeColumn(row.original.Protein)}
					</div>
				)
			},
		},
		{
			Header: () => <div className='mg-fd-tbl-head'>Fat</div>,
			accessor: "Fat",
			className: "tableheader wordwrap",
			Cell: (row) => {
				return (
					<div className='tabledata pt-0 mg-fd-tbl-head'>{tableResizeColumn(row.original.Fat)}</div>
				)
			},
		},
		{
			Header: () => <div className='mg-fd-tbl-head'>Carbohydrates</div>,
			accessor: "CarboHydrate",
			className: "tableheader wordwrap",
			Cell: (row) => {
				return (
					<div className='tabledata pt-0 mg-fd-tbl-head'>
						{tableResizeColumn(row.original.CarboHydrate)}
					</div>
				)
			},
		},
		{
			Header: () => <div>Action</div>,
			accessor: "",
			resizable: false,
			sortable: false,
			show:
				permissions.includes(Permissions.MANAGE_FOOD_EDIT) ||
					permissions.includes(Permissions.MANAGE_FOOD_DELETE)
					? true
					: false,
			Cell: (row) => {
				return (
					<>
						<div className='tabledata text-center'>
							{permissions.includes(Permissions.MANAGE_FOOD_EDIT) &&
								props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									title='Edit'
									className='mr-3 icon_edit'
									icon={faEdit}
									style={{ cursor: "pointer" }}
									onClick={() => editFoodItem(row.original)}
								/>
							) : null}
							{permissions.includes(Permissions.MANAGE_FOOD_DELETE) &&
								props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									title='Delete'
									className='mr-3 icon_Delete'
									icon={faTrashAlt}
									style={{ cursor: "pointer" }}
									onClick={() => deleteFoodItem(row.original)}
								/>
							) : null}
						</div>
					</>
				)
			},
			width: Math.round(window.innerWidth * 0.06),
		},
	]

	//
	// addFood handler,
	//
	const addFood = () => {
		setEditProps({})
		setModelProps({
			isAdd: true,
			isEdit: false,
		})
		setAdd(true)
	}

	//
	// which is used to sorting table columns handler
	//
	const sortingColumnFunc = (page) => {
		if (page && page.sorted && page.sorted[0]) {
			refreshList(page)
		}
	}

	//
	// which is used to refresh the list based on the flag value
	//
	const updatePopupState = (flag) => {
		setModelProps({isAdd:false,isEdit:false})
		if (flag === true) refreshList()
	}

	//
	// which is used to refresh list handler
	//
	const refreshList = (pageProps, filterProps) => {
		let sortByName = "CreatedDate"
		let orderBy = "DESC"
		if (pageProps && pageProps.sorted !== undefined && pageProps.sorted.length > 0) {
			sortByName = pageProps.sorted[0].id
			if (pageProps.sorted[0].desc === true) {
				orderBy = "DESC"
			} else {
				orderBy = "ASC"
			}
		}

		let filterModel = {
			CategoryIDs: filterProps && filterProps.category ? [filterProps.category].join(",") : "",
			ItemIDs: filterProps && filterProps.item ? [filterProps.item].join(",") : "",
			// PageNo: 1,
			PageSize: 10,
			PageNo: pageProps && pageProps.page > 0 ? pageProps.page : 1,
			PageSize:
				pageProps &&
					pageProps.isExport !== undefined &&
					pageProps.isExport !== null &&
					pageProps.isExport === true
					? paginationProps.totalRecords
					: paginationProps.pageSize,
			SortColumn: sortByName,
			SortOrder: orderBy,
		}
		APIRequest.getPostService(APIUrl.GET_FOOD_ITEMS_LIST, filterModel).then((res) => {
			if (res.Data.length > 0 && pageProps && pageProps.isExport === true) {
				if (res.Succeeded)
					setExportExcelProps({
						...exportExcelProps,
						exportData: res.Data,
						isExportExcel: true,
					})
				setExportExcelProps({
					...exportExcelProps,
					isExportExcel: false,
				})
			} else {
				if (res.Succeeded) setMainListData(res.Data)
				if (res.Data[0]) {
					setPaginationProps({
						...paginationProps,
						totalCount: res.Data.length >= 1 ? res.Data.length : 0,
						totalPages: res.Data[0].TotalPages,
						totalRecords: res.Data[0].TotalRows,
					})
				} else {
					setPaginationProps({
						...paginationProps,
						totalPages: 0,
						totalRecords: 0,
						totalCount: 0,
					})
				}
			}
		})
	}

	//
	// Pagination handler
	//
	const paginationCall = (page) => {
		let pageProps = {
			page: page,
			isExport: false,
		}
		refreshList(pageProps, {})
	}

	//
	// Call Export data
	//
	const handleExportData = () => {
		let pageProps = {
			page: 0,
			isExport: true,
		}
		refreshList(pageProps, {})
	}

	//
	//which is handling the submit filter
	//
	const submitFilter = (category, item) => {
		let filterProps = {
			category: category,
			item: item,
		}
		refreshList({}, filterProps)
	}

	return (
		<>
			<div>
				<div className='row col-md-12 mt-3 form-group pr-0'>
					<div className='games_layout form-group col-md-6'>Manage Food</div>
					{permissions.includes(Permissions.MANAGE_FOOD_CREATE) ? (
						<div className='col-md-6 d-flex justify-content-end mr-0 pr-0'>
							<Button className='btn_green' onClick={addFood}>
								Add Food
							</Button>
						</div>
					) : null}
				</div>
				<div className='mr-3 ml-3 form-group mg-fd-box mr-0 pr-0'>
					<ManageFoodFilter submitFilter={submitFilter} />
				</div>
				<div className='mr-3 ml-3'>
					<ReactTable
						className='CosmicTable'
						data={mainListData}
						columns={columns}
						showPaginationTop={false}
						showPaginationBottom={false}
						minRows={0}
						onFetchData={sortingColumnFunc}
						defaultPageSize={10}
						defaultPage={1}
						ref={table}
						sortable={true}
						multiSort={true}
						manual={true}
						showPageSizeOptions={false}
						NoDataComponent={NoDataProp}
						defaultSorting={[{ id: "id", desc: true }]}
						manualPagination={true}
					/>
				</div>
				<div className='row col-sm-12 mt-2 pr-0'>
					<div className='col-sm-6 mt-1'>
						<label>
							<b>Total Records: </b>
							{paginationProps.totalRecords > 10
								? paginationProps.totalCount + " of " + paginationProps.totalRecords
								: paginationProps.totalRecords !== 0
									? paginationProps.totalRecords
									: 0}
						</label>
					</div>

				</div>
			</div>
			{permissions.includes(Permissions.MANAGE_FOOD_EXPORT) ? (
				<>
					<div className='row col-sm-12'>
						<div className='col-sm-6'>
							<Button className='btn_green mr-2' onClick={handleExportData}>
								<FontAwesomeIcon title='Export Data' color={"#FFFFFF"} icon={faDownload}>
									{" "}
								</FontAwesomeIcon>{" "}
								Export Excel
							</Button>
						</div>
						{paginationProps.totalPages > 0 ? (
							<div className='col-sm-6 d-flex justify-content-end pr-0'>
								<Pagination
									totalPages={paginationProps.totalPages}
									totalRecords={paginationProps.totalRecords}
									paginationCall={paginationCall}
								/>
							</div>
						) : null}
					</div>
					{exportExcelProps.isExportExcel ? (
						<ExportData
							data={exportExcelProps.exportData}
							label={exportLabel}
							filename='Manage Food Item List'
						/>
					) : null}
				</>
			) : null}

			{/* Add food component  */}
			{modelProps.isEdit ===true ||modelProps.isAdd===true ? (
				<AddFood
					modelProps={modelProps}
					updatePopupState={updatePopupState}
					editProps={editProps}
				/>
			) : null}
			{/* End add food component*/}
			{deleteProps.isOpen ? (
				<DeleteFoodItem closePopup={closeDeletePopup} foodItemID={deleteProps.foodItemID} />
			) : null}
		</>
	)
}

const mapPropsToState = (state) => ({
	user: state.userReducer,
})
export default PermissionProvider(connect(mapPropsToState, null)(ManageFood))
