import React, { memo, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import {
  APIRequest,
  showNotification,
} from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { Message } from "../../../message/messageConstant";

// Delete host country activity component
const DeleteFoodItem = (props) => {
  const [disableCancel, setDisableCancel] = useState(false);
  const [disableDelete, setDisableDelete] = useState(false);

  const closePopup = () => {
    props.closePopup(false);
  };

  //
  // Delete Program
  //
  const deleteProgram = () => {

      //call disable actions
        disableAction();

    let foodItemID = props.foodItemID ? props.foodItemID : 0;
    APIRequest.getGetService(APIUrl.DELETE_FOOD_ITEM + "/" + foodItemID)
      .then((res) => {
          //call disable actions
            disableAction();

        let mode = "error";
        if (res.Data.Success) {
          mode = "success";
        }
        props.closePopup(true);
        showNotification(res.Data.Message, mode);
      })
      .catch(() => {
          //call disable actions
            disableAction();

      });
  };

  //
  // Disable action once user clicks on delete button, we should not allow them to resubmit again.
  //
  const disableAction = () => {
    setDisableCancel(!disableCancel);
    setDisableDelete(!disableDelete);
  };

  //Return Delete
  return (
    <>
      <Modal size="sm" backdrop="static" show={true}>
        <div className="popup-content">
          <div className="deletecircle"></div>
          <i className="fa fa-trash-o deleteicon"></i>
          <Modal.Body>
            <p
              className="pull-left"
              style={{ margin: "4rem 2rem", textAlign: "left" }}
            >
              {" "}
              <h5>{Message.DELETE_FOOD_ITEM_HEADING}</h5>
              <h6 style={{ color: "darkgray" }}>
                {Message.DELETE_FOOD_ITEM_BODY}
              </h6>
              <h6
                style={{
                  color: "darkgray",
                  marginRight: "6rem",
                  marginBottom: "-3rem",
                }}
              >
                {Message.INVITE_DELETE_CONFIRM1}
              </h6>
            </p>
            <div className="pull-right">
              <Button
                className="btn_cancel mr-2"
                disabled={disableCancel}
                onClick={closePopup}
              >
                No
              </Button>
              <Button
                className="btn_green"
                disabled={disableDelete}
                onClick={deleteProgram}
              >
                Yes
              </Button>
            </div>
          </Modal.Body>
        </div>
      </Modal>
    </>
  );
};

export default memo(DeleteFoodItem);
