import React, { useEffect, useState } from "react";
import { Input, Select } from "antd";
import { Button, Modal } from "react-bootstrap";
import {
  showNotification,
  APIRequest,
} from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import "../../../styles/DietManagement.css";
import * as $ from "jquery";
import Upload from "../../../content/Icons/Upload.svg";

const { Option } = Select;

//
// AddFood component
//
const AddFood = (props) => {
  const [fields, setFields] = useState({
    category: [],
    itemName: "",
    calories: "",
    protein: "",
    fat: "",
    carboHydrate: "",
    foodImage: "",
    imageInput: "",
    header:""
  });

  const[errorExist, setErrorExist] = useState("");

  //
  // local states of component
  //
  const [categoryData, setCategoryData] = useState([]);

  const [disableSave, setDisableSave] = useState(false);

  const [disableCancel, setDisableCancel] = useState(false);

  const [errors, setErrors] = useState({});

  const [isShowImg, setShowImg] = useState(false);

  // const [isAdd, setIsAdd] = useState(true)


  // const isAdd = () => {

  // }

  //
  // Lifecycle method which is used to get the Food Category list items
  //
  useEffect(() => {
    APIRequest.getGetService(APIUrl.GET_FOOD_CATEGORY).then((res) => {
      if (res.Succeeded) {
        setCategoryData(res.Data);
      }
    });
    if (props.modelProps.isEdit) {
      let foodItemID = props.editProps.FoodItemID
        ? props.editProps.FoodItemID
        : 0;
      APIRequest.getGetService(
        APIUrl.GET_FOOD_ITEM_IMAGE_URL + "/" + foodItemID
      ).then((res) => {
        setFields({
          ...fields,
          itemName: props.editProps.ItemName,
          calories: props.editProps.Calories,
          protein: props.editProps.Protein,
          fat: props.editProps.Fat,
          carboHydrate: props.editProps.CarboHydrate,
          foodImage: res.Data,
          category: props.editProps.CategoryID,
        });
        setShowImg(true);
        $("#uploadContainer").removeClass("imag-content-default");
        $("#uploadICon").addClass("upload-icon");
        $("#removeHeader").removeClass("remove-img-header");
      });
    }
  }, []);

  //
  // onChange which is used to handle onchange event
  //
  const onChange = (name, event) => {
    let temp = "";
    if (name.trim() === "category") {
      temp = event;
    } else {
      temp = event.target.value;
    }
    setFields({
      ...fields,
      [name]: temp,
    });
    setErrors({
      ...errors,
      [name]: "",
    });
  };

  //
  // Close popup
  //
  const closePopup = (flag) => {
    props.updatePopupState(flag);
  };

  //
  // emptyFieldValidation method which is used to validate empty fields
  //

  const emptyFieldValidation = () => {
    let error = {};
    let isError = false;
    if (
      fields.category === undefined ||
      fields.category === null ||
      fields.category.length === 0
    ) {
      isError = true;
      error["category"] = "Please select Category";
    }
    if (
      fields.itemName === undefined ||
      fields.itemName === null ||
      fields.itemName.trim() === ""
    ) {
      isError = true;
      error["itemName"] = "Please enter Food Name";
    }
    if (
      fields.calories === undefined ||
      fields.calories === null ||
      fields.calories.trim() === ""
    ) {
      isError = true;
      error["calories"] = "Please enter Calories";
    }
    if (
      fields.protein === undefined ||
      fields.protein === null ||
      fields.protein.trim() === ""
    ) {
      isError = true;
      error["protein"] = "Please enter Protein";
    }
    if (
      fields.fat === undefined ||
      fields.fat === null ||
      fields.fat.trim() === ""
    ) {
      isError = true;
      error["fat"] = "Please enter Fat";
    }
    if (
      fields.carboHydrate === undefined ||
      fields.carboHydrate === null ||
      fields.carboHydrate.trim() === ""
    ) {
      isError = true;
      error["carboHydrate"] = "Please enter Carbohydrates";
    }
    setErrors(error);
    return isError;
  };

  //
  //disable action once user submit the data then we should not allow them to resubmit again
  //

  const disableAction = () => {
    setDisableSave(!disableSave);
    setDisableCancel(!disableCancel);
  };

  //
  // Save Food Items
  //

  const save = () => {
    handleFoodCheck()
    if (!emptyFieldValidation()&&errorExist=="") {
      //disable actions
      disableAction();

      let model = {
        CategoryID: fields.category ? [fields.category].join(",") : "",
        ItemName: fields.itemName,
        Calories: fields.calories,
        Protein: fields.protein,
        Fat: fields.fat,
        CarboHydrate: fields.carboHydrate,
        FoodImageUrl: fields.foodImage,
        FoodItemID: props.editProps.FoodItemID ? props.editProps.FoodItemID : 0,
        IsAdd: props.modelProps.isEdit ? false : true,
      };
      APIRequest.getPostService(APIUrl.SAVE_FOOD_ITEM, model)
        .then((res) => {
          //disable actions
          disableAction();
          if (res.Data.Success) {
            showNotification(res.Data.Message, "success");
            closePopup(true);
          } else {
            showNotification(res.Data.Message, "success");
          }
        })
        .catch((err) => {
          //disable actions
          disableAction();
        });
    }
  };

  //
  // Upload food image handler
  //

  const uploadImage = (e) => {
    dummyUpload(e.target.files[0])
      .then((res) => {
        setFields({
          ...fields,
          foodImage: res,
          imageInput: "",
        });
        setShowImg(true);
        $("#uploadContainer").removeClass("imag-content-default");
        $("#uploadICon").addClass("upload-icon");
        $("#removeHeader").removeClass("remove-img-header");
      })
      .catch((err) => {
        setShowImg(false);
      });
  };

  //
  //dummyUpload which is used to read the image file from local
  //

  const dummyUpload = (file) => {
    return new Promise((resolve, reject) => {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  };

  // Add image
  const addImage = () => {
    $("#upload-img-file").click();
  };

  //
  // removeImage which used to remove image handler
  //
  const removeImage = () => {
    setFields({
      ...fields,
      foodImage: null,
      imageInput: "",
    });
    setShowImg(false);
    $("#uploadContainer").addClass("imag-content-default");
    $("#uploadICon").removeClass("upload-icon");
    $("#removeHeader").addClass("remove-img-header");
  };


//To avoid duplicate food item for each category.
  const handleFoodCheck = () => {
     if (fields.category != 0 && fields.itemName != ""){
            
     APIRequest.getGetService(
        APIUrl.GET_CHECKED_FOOD_ITEM_URL + "/" + fields.category + "/" + fields.itemName
      ).then((res) => {
      if(res.Data==true){
        setErrorExist("Food already Exist")
      }
      else{
        setErrorExist("")
      }
      })
     }

  }

  return (
    <>
      <Modal
        className="user-list-toggle-main1"
        size="lg"
        backdrop="static"
        show={true}
      >
        <Modal.Header className="pb-0 border-bottom-0 popup_header font-weight-bold">
          <p>{!props.modelProps.isEdit ? "Add " : "Edit "}  Item</p>
          <div className="col-md-1">
            <div className="d-flex justify-content-end">
              <button
                title="Close"
                type="button"
                className="close"
                data-dismiss="modal"
                area-aria-label="Close"
                onClick={closePopup}
              >
                <span style={{ color: "#c29a37" }} area-hidden="true">
                  &times;
                </span>
              </button>
            </div>
          </div>
        </Modal.Header>
        <Modal.Body>
          <div className="row col-sm-12 form-group">
            <div className="col-sm-6">
              <label className="font-weight-bold popup_txt">
                Category<span className="text-danger">*</span>
              </label>
              <Select
                title="Category"
                className="textfiled"
                bordered={false}
                size="200"
                showSearch
                // mode="multiple"
                onBlur ={handleFoodCheck}
                maxTagCount="responsive"
                allowClear
                showArrow
                placeholder="Select Category"
                value={fields.category}
                onChange={(val) => onChange("category", val)}
                optionFilterProp="children"
                filterSort={(optionA, optionB) =>
                  optionA.children
                    .toLowerCase()
                    .localeCompare(optionB.children.toLowerCase())
                }
              >
                {categoryData.map((item, index) => (
                  <Option key={index} value={item.OptionID}>
                    {item.OptionData}
                  </Option>
                ))}
              </Select>

              <span className="text-danger">{errors.category}</span>
            </div>
            <div className="col-sm-6 ">
              <label className="font-weight-bold popup_txt">
                Food Name<span className="text-danger">*</span>
              </label>
              <Input
                type="text"
                title="Food Name"
                size="large"
                autoComplete="off"
                onBlur = {handleFoodCheck}
                value={fields.itemName}
                onChange={(val) => onChange("itemName", val)}
                bordered={false}
                className="textfiled w-80"
                placeholder="Enter Food Name"
              />
              <span className="text-danger">{errors.itemName}</span>
            </div>
          </div>
          <div className="row col-sm-12 form-group">
            <div className="col-sm-6">
              <label className="font-weight-bold popup_txt">
                Calories<span className="text-danger">*</span>
              </label>
              <Input
                type="text"
                title="Calories"
                size="large"
                autoComplete="off"
                value={fields.calories}
                onChange={(val) => onChange("calories", val)}
                bordered={false}
                className="textfiled w-80"
                placeholder="Enter Calories"
              />
              <span className="text-danger">{errors.calories}</span>
            </div>
            <div className="col-sm-6">
              <label className="font-weight-bold popup_txt">
                Protein<span className="text-danger">*</span>
              </label>
              <Input
                type="text"
                title="Protein"
                size="large"
                autoComplete="off"
                value={fields.protein}
                onChange={(val) => onChange("protein", val)}
                bordered={false}
                className="textfiled w-80"
                placeholder="Enter Protein"
              />
              <span className="text-danger">{errors.protein}</span>
            </div>
          </div>
          <div className="row col-sm-12 form-group">
            <div className="col-sm-6">
              <label className="font-weight-bold popup_txt">
                Fat<span className="text-danger">*</span>
              </label>
              <Input
                type="text"
                title="Fat"
                size="large"
                autoComplete="off"
                value={fields.fat}
                onChange={(val) => onChange("fat", val)}
                bordered={false}
                className="textfiled w-80"
                placeholder="Enter Fat"
              />
              <span className="text-danger">{errors.fat}</span>
            </div>
            <div className="col-sm-6">
              <label className="font-weight-bold popup_txt">
                Carbohydrates<span className="text-danger">*</span>
              </label>
              <Input
                type="text"
                title="Carbo Hydrate"
                size="large"
                autoComplete="off"
                value={fields.carboHydrate}
                onChange={(val) => onChange("carboHydrate", val)}
                bordered={false}
                className="textfiled w-80"
                placeholder="Enter Carbohydrates"
              />
              <span className="text-danger">{errors.carboHydrate}</span>
            </div>
          </div>
          <div className="img-container">
            <div
              id="removeHeader"
              className="img-remove-icon-header remove-img-header"
              onClick={removeImage}
            >
              <span area-hidden="true" title="Close">
                &times;
              </span>
            </div>
            <div
              id="uploadContainer"
              className="file-upload imag-content-default"
            >
              <div className="img-input-control">
                <input
                  style={{ visibility: "hidden" }}
                  id="upload-img-file"
                  className="file-upload-input"
                  type="file"
                  accept="image/*"
                  value={fields.imageInput}
                  onChange={uploadImage}
                />
              </div>
              <div className="image-upload-wrap">
                <div id="uploadICon">
                  <img src={Upload} onClick={addImage} />
                  <span
                    style={{
                      fontSize: "12px",
                      fontWeight: "bold",
                      padding: "5px",
                    }}
                  >
                    Upload Image
                  </span>
                </div>
                {isShowImg ? (
                  <div className="file-upload-content">
                    <img
                      class="file-upload-image"
                      src={fields.foodImage}
                      alt="your image"
                    />
                  </div>
                ) : null}
              </div>
            </div>
          </div>
          <div className="row col-sm-12 mt-4 form-group">
            <div className="col-sm-12 d-flex justify-content-end ">

            {/* <Button
                className="btn_green mr-2"
                disabled={disableCancel}
                onClick={closePopup}
              >
                Close
              </Button> */}

{!props.modelProps.isEdit ? (

              <Button
                className="btn_green"
                disabled={disableSave}
                onClick={save}
              >
                Save
              </Button>
) : (
  <Button
                className="btn_green"
                disabled={disableSave}
                onClick={save}
              >
                Update
              </Button>
)}
              
            </div>
            <div className="col-sm-1"></div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default React.memo(AddFood);
