import React, { Fragment } from "react"
import ReactTable from "react-table"
import "../../../styles/CosmicTable.css"
import "../../../styles/grid.css"
import "antd/dist/antd.css"
import moment from "moment"
import { Select, DatePicker, Spin, notification } from "antd"
import { Button, Modal, Card, Collapse } from "react-bootstrap"
import Pagination from "../../../utils/Pagination"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
//import Multiselect from "react-multi-select-component"
import {
	faArrowDown,
	faArrowUp,
	faTrashAlt,
	faDownload,
	faCalendarCheck,
	faEdit,
	faEye,
} from "@fortawesome/free-solid-svg-icons"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import ExportData from "../../../utils/ExportData"
import { Message } from "../../../message/messageConstant"
import PermissionProvider from "../../../permissions/PermissionProvider"

const { Option } = Select

const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	} else if (flag === 2) {
		//resend flag
		notification[type]({
			message: description,
			description: "",
			//  duration:20
		})
	} else if (flag === 3) {
		//extend expires in
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_gray' icon={faCalendarCheck}></FontAwesomeIcon>,
			description: "",
		})
	}
}

const setIsExpanded = (data, self) => {
	let Icons = self.state.Icons === true ? false : true
	self.setState({ IsExpanded: data, Icons: Icons })
}

const handleTblDelete = (id, self) => {
	self.setState({ loading: true })
	APIRequest.getGetService(APIUrl.DELETE_SPORTS_LEVEL + "/" + id)
		.then((response) => {
			self.setState({ loading: false })
			if (response.Data === true) {
				self.fetch(self.table.current.state)
				openNotificationWithIcon("success", "Record has been deleted", 1)
			}
		})
		.catch((error) => {
			self.setState({ loading: false })
		})
}

const NoDataConst = (props) => (
	<span className='table-nodata-style'>
		No records found Please check your connection or change your filters{" "}
	</span>
)

//set open model property
const openModal = (id, self) => {
	self.setState({ isOpen: true, isDeleteID: id })
}
//Export label
const exportLabel = [
	{ columnName: "S.No", value: "SerialNo", editable: "never" },
	{ columnName: "Games Setup", value: "GamesSetupName" },
	{ columnName: "Sport", value: "Sport" },
	{ columnName: "SportEvent", value: "SportEvent" },
	{ columnName: "Rounds/ Levels/ Sets", value: "RoundName" },
	{ columnName: "Facility", value: "Facility" },
	{ columnName: "Date", value: "SportFacilityDate" },
	{ columnName: "Slot", value: "SportSlot" },
	{ columnName: "Created by", value: "CreatedBy" },
]

class SportsLevel extends React.Component {
	constructor(props) {
		super(props)
		
		let permissions = this.props.getAppPermissions();
		let roles = this.props.getAppRoles();

		this.state = {
			users: [],
			loading: true,
			perPage: 1,
			totalpage: 10,
			total_pages: 10,
			Currentpage: 1,
			GamesSetupID: [],
			GamesSetupName: "",
			SportID: [],
			SportIDList: [],
			Sport: "",
			SportEventID: [],
			SportEventIDList: [],
			SportEvent: "",
			FacilityID: [],
			FacilityIDList: [],
			Facility: "",
			RoundID: [],
			RoundIDList: [],
			RoundName: "",
			SportFacilityDateID: [],
			SportFacilityDateIDList: [],
			FilterSportFacilityDateIDList: [],
			SportFacilityDate: "",
			SportFacilityTimeSlotID: [],
			SportFacilityTimeSlotIDList: [],
			FilterSportFacilityTimeSlotIDList: [],
			SportFacilityTimeSlot: "",
			FilterGamesSetupID: [],
			FilterGamesSetupIDList: [],
			FilterSportID: [],
			FilterSportIDList: [],
			FilterSportEventID: [],
			FilterSportEventIDList: [],
			FilterFacilityID: [],
			FilterRoundID: [],
			FilterSportFacilityDate: "",
			FilterSportFacilityTimeSlotID: [],
			FilterCreatedBy: "",
			pageSize: 10,

			selected: {},
			selectAll: 0,
			IsExpanded: false,
			Icons: false,
			value: [],
			TotalPages: 0,
			TotalRecords: 0,
			IncrementPage: 0,
			isOpen: false,
			isClose: true,
			isDeleteID: 0,
			//Export excel properties
			IsExportExcel: false,
			exportData: [],
			sortColumn: "",
			sortValue: "",
			exportFilename: "Added Sport List",
			resendbtn: true,
			deletebtn: true,
			exportbtn: true,
			isSearchBtn: true,
			permissions: permissions,
			roles: roles,
			EditGamesSportLevelID: 0,
			EditGamesSetupID: [],
			EditGamesSetupIDList: [],
			EditSportID: [],
			EditSportIDList: [],
			EditSportEventID: [],
			EditSportEventIDList: [],
			EditFacilityID: [],
			EditFacilityIDList: [],
			EditRoundID: [],
			EditRoundIDList: [],
			EditSportFacilityDateID: "",
			EditSportFacilityTimeSlotID: [],
			EditSportFacilityTimeSlotIDList: [],
			EditRoundName: "",
			EditSportFacilityDateName: "",
			EditSportFacilityStartDate: "",
			EditSportFacilityEndDate: "",
			EditSportFacilityTimeSlotName: "",
			RecordAlreadyExists: "",
			isEyeData: [],
			isEditData: [],
			isEyeDatapopup: false,
			isAddEditDatapopup: false,
			isAddSport: false,
			errors: {},
		}
		this.table = React.createRef()
		this.fetch = this.fetch.bind(this)
		this.setRef = React.createRef()
		this.toggleRow = this.toggleRow.bind(this)
		this.handleListClear = this.handleListClear.bind(this)
		this.handleBtnDelete = this.handleBtnDelete.bind(this)
		this.handleBtnResend = this.handleBtnResend.bind(this)
		this.tableColumnDataLimit = this.tableColumnDataLimit.bind(this)
		this.DynamicdisabledDate = this.DynamicdisabledDate.bind(this)
	}
	tableColumnDataLimit = (str) => {
		if (str !== undefined) {
			return str.length > 30 ? str.substring(0, 27) + "..." : str
		}
	}
	handleBtnDelete = (event) => {
		var InputData = []
		let self = this
		let selectedRowIds = this.handlerGetListIDs(this.state.selected, this.state.users)
		if (selectedRowIds) {
			self.setState({
				loading: true,
			})
			InputData = selectedRowIds.toString()
			APIRequest.getGetService(APIUrl.DELETE_SPORTS_LEVEL + "/" + InputData)
				.then((response) => {
					self.setState({
						loading: false,
					})
					if (response.Data === true) {
						openNotificationWithIcon("success", "Record has been deleted", 1)
						self.setState({ fields: [], rowcount: 0, selected: [] })
						self.fetch(this.table.current.state)
					}
				})
				.catch((error) => {})
		}
	}
	handlerGetListIDs = (state, userList) => {
		if (state && userList) {
			let rowId = Object.keys(state)
			let selectedRowIds = userList
				.filter((x) => rowId.toString().includes(x.SerialNo))
				.map((x) => x.InvitedUserID)
			return selectedRowIds
		} else return 0
	}
	handleBtnResend(event) {
		var InputData = []
		let selectedRowIds = this.handlerGetListIDs(this.state.selected, this.state.users)
		let self = this
		if (selectedRowIds) InputData = selectedRowIds.toString()
		self.setState({
			loading: true,
		})
		APIRequest.getGetService(APIUrl.RESENT_NOTIFICATION + "/" + InputData)
			.then((response) => {
				self.setState({
					loading: false,
				})
				if (response.Succeeded === true) {
					self.fetch(self.table.current.state)
					openNotificationWithIcon("success", response.Data, 2)
				}
			})
			.catch((error) => {})
	}
	handleListClear(event) {
		this.setState(
			{
				FilterGamesSetupID: [],
				FilterSportID: [],
				FilterSportEventID: [],
				FilterFacilityID: [],
				FilterRoundID: [],
				FilterSportFacilityDate: "",
				FilterSportFacilityTimeSlotID: [],
				isSearchBtn: true,
			},
			function () {
				this.fetch(this.table.current.state)
			}
		)
	}
	openViewAlldataModal = (data, self) => {
		self.setState({ isEyeDatapopup: true, isAddSport: false, isEyeData: data })
	}
	openAddSportModal = () => {
		this.setState({
			isAddEditDatapopup: true,
			isAddSport: true,
			EditGamesSetupID: [],
			EditGamesSportLevelID: [],
			EditSportID: [],
			EditSportIDList: [],
			EditSportEventID: [],
			EditSportEventIDList: [],
			EditFacilityID: [],
			EditFacilityIDList: [],
			EditRoundID: [],
			EditRoundIDList: [],
			EditSportFacilityDateID: "",
			EditSportFacilityTimeSlotID: [],
			EditRoundName: "",
			EditSportFacilityDateName: "",
			EditSportFacilityTimeSlotName: "",
		})
	}

	EditSportIdChange = (Sportsid) => {
		APIRequest.getPostService(APIUrl.GET_FACILITY_LIST + "/" + Sportsid)
			.then((response) => {
				if (response.Succeeded === true) {
					this.setState({ ...this.state, EditFacilityIDList: response.Data, AddSportID: Sportsid })
				}
			})
			.catch((error) => {})
		this.EditRoundIdChange(Sportsid)
	}

	EditSportEventIdChange = (Sportsid) => {
		APIRequest.getPostService(APIUrl.GET_SPORT_EVENT_LIST + "/" + Sportsid)
			.then((response) => {
				if (response.Succeeded === true) {
					this.setState({
						...this.state,
						EditSportEventIDList: response.Data,
						AddSportID: Sportsid,
					})
				}
			})
			.catch((error) => {})
	}

	EditGamesSetupIdChange = (GamesSetupid) => {
		APIRequest.getPostService(APIUrl.GET_SPORT_LIST_BY_GAMES_SETUPID + "/" + GamesSetupid)
			.then((response) => {
				if (response.Succeeded === true) {
					this.setState({ EditSportIDList: response.Data })
				}
			})
			.catch((error) => {})
	}
	EditRoundIdChange = (Sportsid) => {
		APIRequest.getPostService(APIUrl.GET_ROUND_LIST + "/" + Sportsid)
			.then((response) => {
				if (response.Succeeded === true) {
					this.setState({ EditRoundIDList: response.Data })
				}
			})
			.catch((error) => {
				//self.props.isLoadingStopFunc()
			})
	}

	openEditAlldataModal = (data, self) => {
		this.EditSportIdChange(data.SportID)
		const RoundID = data.RoundID
		if (isNaN(data.RoundID)) {
			RoundID.trim()
			if (RoundID.indexOf(",") !== -1) {
				// let RoundID1 = RoundID.split(", ")
				// RoundID1 = RoundID1.map((i) => (data.RoundID = parseInt(i)))
			}
		} else {
			data.RoundID = parseInt(RoundID)
		}
		const SportFacilityDateID = data.SportFacilityDateID
		if (isNaN(data.SportFacilityDateID)) {
			SportFacilityDateID.trim()
			if (SportFacilityDateID.indexOf(",") !== -1) {
				// let SportFacilityDateID1 = SportFacilityDateID.split(", ")
				// SportFacilityDateID1 = SportFacilityDateID1.map(
				// 	(i) => (data.SportFacilityDateID = parseInt(i))
				// )
			}
		} else {
			data.SportFacilityDateID = parseInt(SportFacilityDateID)
		}
		const SportFacilityTimeSlotID = data.SportSlotID
		if (isNaN(data.SportSlotID)) {
			SportFacilityTimeSlotID.trim()
			if (SportFacilityTimeSlotID.indexOf(",") !== -1) {
				// let SportFacilityTimeSlotID1 = SportFacilityTimeSlotID.split(", ")
				// SportFacilityTimeSlotID1 = SportFacilityTimeSlotID1.map(
				// 	(i) => (data.SportSlotID = parseInt(i))
				// )
			}
		} else {
			data.SportSlotID = parseInt(SportFacilityTimeSlotID)
		}
		self.setState({
			...this.state,
			isAddEditDatapopup: true,
			EditGamesSetupID: data.GamesSetupID,
			EditGamesSportLevelID: data.GamesSportLevelID,
			EditSportID: data.SportID,
			EditSport: data.Sport,
			EditSportEventID: data.SportEventID,
			EditSportEvent: data.SportEvent,
			EditFacilityID: data.FacilityID,
			EditRoundID: data.RoundID,
			EditSportFacilityDateID: data.SportFacilityDateID,
			EditSportFacilityTimeSlotID: data.SportSlotID,
			EditRoundName: data.RoundName,
			EditSportFacilityDateName: moment(data.SportFacilityDate),
			EditSportFacilityTimeSlotName: data.SportSlot,
		})
		this.GamesEditSetupIDDateChange(data.GamesSetupID)
	}
	SportLevelSubmite = async () => {
		let isValid = this.handleFieldValidation()
		let self = this
		let GamesSportLevelID = 0
		if (self.state.isAddSport) {
			GamesSportLevelID = 0
		} else {
			GamesSportLevelID = self.state.EditGamesSportLevelID
		}
		let Sdates = new Date(self.state.EditSportFacilityDateName)
		let startDates = Sdates.setDate(new Date(Sdates).getDate() + 1)
		let sDate = new Date(startDates)
		if (!isValid) {
			let InputData = [
				{
					GamesSportLevelID: GamesSportLevelID,
					GamesSetupID: self.state.EditGamesSetupID,
					SportID: self.state.EditSportID,
					SportEventID: self.state.EditSportEventID,
					FacilityID: self.state.EditFacilityID,
					SportRoundID: self.state.EditRoundID.toString(),
					SportFacilityDate: sDate,
					SportSlotID: self.state.EditSportFacilityTimeSlotID,
				},
			]
			//self.setState({ loading: true })
			await APIRequest.getPostService(APIUrl.ADD_SPORTS_LEVEL, InputData)
				.then((response) => {
					if (response.Succeeded === true) {
						if (response.Data === "Already exists") {
							openNotificationWithIcon("success", "Sport Level already exists.", 3)
							self.setState({ ...this.state, RecordAlreadyExists: "Record already exists." })
						} else {
							self.setState({
								...this.state,
								loading: true,
								isAddEditDatapopup: false,
								isEditData: [],
								RecordAlreadyExists: "",
							})

							openNotificationWithIcon("success", "Sport Level Added Successfully.", 2)
							self.fetch(this.table.current.state)
						}
					}
				})
				.catch((error) => {})
		}
	}
	handleFieldValidation = () => {
		let isErrorExist = false
		let errors = {}
		if (
			this.state.EditGamesSetupID.length === 0 ||
			this.state.EditGamesSetupID === undefined ||
			this.state.EditGamesSetupID === null
		) {
			errors["GamesSetup"] = "Please enter Games Setup"
			isErrorExist = true
		}

		if (
			this.state.EditSportID.length === 0 ||
			this.state.EditSportID === undefined ||
			this.state.EditSportID === null
		) {
			errors["Sport"] = "Please enter Sport"
			isErrorExist = true
		}

		if (
			this.state.EditSportEventID.length === 0 ||
			this.state.EditSportEventID === undefined ||
			this.state.EditSportEventID === null
		) {
			errors["SportEvent"] = "Please enter Events"
			isErrorExist = true
		}

		if (
			this.state.EditFacilityID.length === 0 ||
			this.state.EditFacilityID === undefined ||
			this.state.EditFacilityID === null
		) {
			errors["Facility"] = "Please enter Facility"
			isErrorExist = true
		}

		if (
			this.state.EditRoundID.length === 0 ||
			this.state.EditRoundID === undefined ||
			this.state.EditRoundID === null
		) {
			errors["Round"] = "Please enter Round"
			isErrorExist = true
		}

		if (
			this.state.EditSportFacilityDateName === "" ||
			this.state.EditSportFacilityDateName === undefined ||
			this.state.EditSportFacilityDateName === null
		) {
			errors["FacilityDate"] = "Please enter Facility Date"
			isErrorExist = true
		}

		if (
			this.state.EditSportFacilityTimeSlotID.length === 0 ||
			this.state.EditSportFacilityTimeSlotID === undefined ||
			this.state.EditSportFacilityTimeSlotID === null
		) {
			errors["TimeSlot"] = "Please enter Slot"
			isErrorExist = true
		}
		this.setState({ ...this.state, errors: errors })
		return isErrorExist
	}
	viewAlldatapopupClose = () => {
		this.setState({ isEyeDatapopup: false, isEyeData: [] })
		this.setState({ isAddEditDatapopup: false, isAddSport: false, isEditData: [] })
	}

	handleGamesSetupIDChange(GamesSetupIDs) {
		let searchbtn = true
		let data = this.state
		if (
			GamesSetupIDs.length !== 0 ||
			data.FilterSportFacilityTimeSlotID.length !== 0 ||
			data.FilterSportFacilityDate.length !== 0 ||
			data.FilterRoundID.length !== 0 ||
			data.FilterFacilityID.length !== 0 ||
			data.FilterSportID.length !== 0 ||
			data.FilterSportEventID.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ ...this.state, FilterGamesSetupID: GamesSetupIDs, isSearchBtn: searchbtn })
		this.GamesSetupIDDateChange(GamesSetupIDs)
	}
	GamesSetupIDDateChange(GamesSetupid) {
		let startDate = ""
		let endDate = ""
		APIRequest.getPostService(APIUrl.GET_SPORTS_DATE_BY_GAMES_SETUP + "/" + GamesSetupid)
			.then((response) => {
				if (response.Succeeded === true) {
					startDate = moment(response.Data.StartDate).subtract(1, "day")
					endDate = moment(response.Data.EndDate)
					this.setState({
						...this.state,
						EditSportFacilityStartDate: startDate.format("DD MMM YYYY"),
						EditSportFacilityEndDate: endDate.format("DD MMM YYYY"),
					})
				}
			})
			.catch((error) => {})
	}

	GamesEditSetupIDDateChange(GamesSetupid) {
		let startDate = ""
		let endDate = ""
		APIRequest.getPostService(APIUrl.GET_SPORTS_DATE_BY_GAMES_SETUP + "/" + GamesSetupid)
			.then((response) => {
				if (response.Succeeded === true) {
					startDate = moment(response.Data.StartDate).subtract(1, "day")
					endDate = moment(response.Data.EndDate)
					this.setState({
						EditSportFacilityStartDate: startDate.format("DD MMM YYYY"),
						EditSportFacilityEndDate: endDate.format("DD MMM YYYY"),
					})
					console.log(this.state.EditSportFacilityStartDate)
				}
			})
			.catch((error) => {})
	}
	handleSportIDChange(sportIDs) {
		let searchbtn = true
		let data = this.state
		if (
			data.FilterGamesSetupID.length !== 0 ||
			data.FilterSportFacilityTimeSlotID.length !== 0 ||
			data.FilterSportFacilityDate.length !== 0 ||
			data.FilterRoundID.length !== 0 ||
			data.FilterFacilityID.length !== 0 ||
			sportIDs.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ ...this.state, FilterSportID: sportIDs, isSearchBtn: searchbtn })
		this.GamesSportIDEventChange(sportIDs)
	}

	GamesSportIDEventChange(sportIDs) {
		APIRequest.getPostService(APIUrl.GET_SPORTS_EVENT_BY_SPORT + "/" + sportIDs)
			.then((response) => {
				if (response.Succeeded === true) {
					this.setState({
						FilterSportEventIDList: response.Data,
					})
				}
			})
			.catch((error) => {})
	}

	handleSportEventIDChange(sportEventIDs) {
		let searchbtn = true
		let data = this.state
		if (
			data.FilterGamesSetupID.length !== 0 ||
			data.FilterSportFacilityTimeSlotID.length !== 0 ||
			data.FilterSportFacilityDate.length !== 0 ||
			data.FilterRoundID.length !== 0 ||
			data.FilterFacilityID.length !== 0 ||
			data.FilterSportID.length !== 0 ||
			sportEventIDs.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ ...this.state, FilterSportEventID: sportEventIDs, isSearchBtn: searchbtn })
	}
	handleRoundIDChange(RoundID) {
		let searchbtn = true
		let data = this.state
		if (
			data.FilterGamesSetupID.length !== 0 ||
			data.FilterSportFacilityTimeSlotID.length !== 0 ||
			data.FilterSportFacilityDate.length !== 0 ||
			RoundID.length !== 0 ||
			data.FilterFacilityID.length !== 0 ||
			data.FilterSportID.length !== 0 ||
			data.FilterSportEventID.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ FilterRoundID: RoundID, isSearchBtn: searchbtn })
	}
	handleSportFacilityDateIDChange(SportFacilityDateID) {
		let searchbtn = true
		let data = this.state
		if (
			data.FilterGamesSetupID.length !== 0 ||
			data.FilterSportFacilityTimeSlotID.length !== 0 ||
			SportFacilityDateID.length !== 0 ||
			data.FilterRoundID.length !== 0 ||
			data.FilterFacilityID.length !== 0 ||
			data.FilterSportID.length !== 0 ||
			data.FilterSportEventID.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ FilterSportFacilityDate: SportFacilityDateID, isSearchBtn: searchbtn })
	}

	handleSportFacilityTimeSlotIDChange(SportFacilityTimeSlotID) {
		let searchbtn = true
		let data = this.state
		if (
			data.FilterGamesSetupID.length !== 0 ||
			SportFacilityTimeSlotID.length !== 0 ||
			data.FilterSportFacilityDate.length !== 0 ||
			data.FilterRoundID.length !== 0 ||
			data.FilterFacilityID.length !== 0 ||
			data.FilterSportID.length !== 0 ||
			data.FilterSportEventID.length !== 0
		) {
			searchbtn = false
		}
		this.setState({
			FilterSportFacilityTimeSlotID: SportFacilityTimeSlotID,
			isSearchBtn: searchbtn,
		})
	}
	handleFacilityIDChange(FacilityID) {
		let searchbtn = true
		let data = this.state
		if (
			data.FilterGamesSetupID.length !== 0 ||
			data.FilterSportFacilityTimeSlotID.length !== 0 ||
			data.FilterSportFacilityDate.length !== 0 ||
			data.FilterRoundID.length !== 0 ||
			FacilityID.length !== 0 ||
			data.FilterSportID.length !== 0 ||
			data.FilterSportEventID.length !== 0
		) {
			searchbtn = false
		}
		this.setState({ FilterFacilityID: FacilityID, isSearchBtn: searchbtn })
	}
	setRef = (ref) => (this.table = ref)
	handlerPagination = (pageNo) => {
		this.setState(
			{
				IncrementPage: pageNo,
			},
			function () {}
		)
		this.fetch(pageNo)
	}
	async fetch(state) {
		let sortbyname = "Sport"
		let orderby = "DESC"
		if (state.sorted !== undefined) {
			if (state.sorted.length !== 0) {
				sortbyname = state.sorted[0].id
				if (state.sorted[0].desc === true) {
					orderby = "DESC"
				}
				if (state.sorted[0].desc === false) {
					orderby = "ASC"
				}
			}
		}
		let pagelimit = 0
		let self = this
		if (state.page === undefined || state.pageSize === undefined) {
			pagelimit = this.state.pageSize
		} else {
			pagelimit = state.pageSize
		}
		let FilterSportDate = ""
		if (this.state.FilterSportFacilityDate !== "") {
			FilterSportDate = moment(this.state.FilterSportFacilityDate).format("DD MMM YYYY")
		}
		let data = {
			GamesSetupID: this.state.FilterGamesSetupID.toString(),
			SportID: this.state.FilterSportID.toString(),
			SportEventID: this.state.FilterSportEventID.toString(),
			RoundID: this.state.FilterRoundID.toString(),
			FacilityID: this.state.FilterFacilityID.toString(),
			SportDateID: FilterSportDate,
			SportSlotID: this.state.FilterSportFacilityTimeSlotID.toString(),
			CreatedBy: this.state.FilterCreatedBy,
			PageNo: state > 0 ? state : 1,
			PageSize: pagelimit,
			SortColumn: sortbyname,
			SortOrder: orderby,
		}
		self.setState({ loading: true })
		APIRequest.getPostService(APIUrl.GET_SPORTS_LEVEL_LIST, data)
			.then((response) => {
				if (response.Data.length > 0) {
					const firstRow = response.Data.find((x) => x)
					self.setState({
						loading: false,
						users: response.Data,
						TotalPages: firstRow.TotalPages,
						TotalRecords: firstRow.TotalRows,
						sortColumn: sortbyname,
						sortValue: orderby,
						exportbtn: false,
					})
				} else {
					self.setState({
						exportbtn: true,
					})
				}
				self.setState({ loading: false, users: response.Data })
			})
			.catch((error) => {
				self.setState({ loading: false })
			})
	}
	async componentDidMount() {
		this.fetch(this.state)
		const GamesSetup = await APIRequest.getGetService(APIUrl.GET_GAMES_SETUP_SPORT_LEVEL_LIST)
		this.setState({
			EditGamesSetupIDList: GamesSetup.Data,
			FilterGamesSetupIDList: GamesSetup.Data,
		})
		const AllSlot = await APIRequest.getGetService(APIUrl.GET_TIME_SLOT_FULL_LIST)
		this.setState({
			EditSportFacilityTimeSlotIDList: AllSlot.Data,
			FilterSportFacilityTimeSlotIDList: AllSlot.Data,
		})
		const Sports = await APIRequest.getGetService(APIUrl.GET_SPORT_LIST)
		this.setState({ SportIDList: Sports.Data })
		const MultiSports = await APIRequest.getGetService(APIUrl.GET_SPORT_LIST)
		this.setState({ FilterSportIDList: MultiSports.Data })
		const Round = await APIRequest.getGetService(APIUrl.GET_ROUND_FULL_LIST)
		this.setState({ RoundIDList: Round.Data })
		const Facility = await APIRequest.getGetService(APIUrl.GET_FACILITY_FULL_LIST)
		this.setState({ FacilityIDList: Facility.Data })
		const SportDate = await APIRequest.getGetService(APIUrl.GET_DATE_FULL_LIST)
		this.setState({
			SportFacilityDateIDList: SportDate.Data,
			FilterSportFacilityDateIDList: SportDate.Data.filter(
				(ele, ind) => ind === SportDate.Data.findIndex((elem) => elem.OptionData === ele.OptionData)
			),
		})
	}
	toggleRow(SerialNo) {
		const newSelected = Object.assign({}, this.state.selected)
		newSelected[SerialNo] = !this.state.selected[SerialNo]
		this.setState({
			selected: newSelected,
			selectAll: 2,
		})
		let flag = 0
		for (const [value] of Object.entries(newSelected)) {
			if (value === true) {
				flag = 1
			}
		}
		if (flag === 1) {
			this.setState({
				resendbtn: false,
				deletebtn: false,
			})
		} else {
			this.setState({
				resendbtn: true,
				deletebtn: true,
			})
		}
	}
	toggleSelectAll() {
		let newSelected = {}
		if (this.state.selectAll === 0 && this.state.users.length !== 0) {
			this.state.users.forEach((x) => {
				newSelected[x.SerialNo] = true
			})
			this.setState({
				resendbtn: false,
				deletebtn: false,
			})
		} else {
			this.setState({
				resendbtn: true,
				deletebtn: true,
			})
		}
		this.setState({
			selected: newSelected,
			selectAll: this.state.selectAll === 0 ? 1 : 0,
		})
	}
	handlerRefreshSportLevelList = () => {
		this.fetch(1)
	}
	deleteModal = () => {
		if (this.state.isDeleteID === 0) this.handleBtnDelete()
		else handleTblDelete(this.state.isDeleteID, this)
		this.setState({ isDeleteID: 0, isOpen: false })
	}
	closeModal = () => {
		this.setState({ isOpen: false })
	}
	handlerExportData = () => {
		let self = this
		let InputData = {
			GamesSetupID: self.state.GamesSetupID.toString(),
			SportID: self.state.SportID.toString(),
			RoundID: self.state.RoundID.toString(),
			FacilityID: self.state.FacilityID.toString(),
			SportFacilityDateID: self.state.SportFacilityDateID.toString(),
			SportFacilityTimeSlotID: self.state.SportFacilityTimeSlotID.toString(),
			PageNo: 1,
			PageSize: self.state.TotalRecords,
			SortColumn: self.sortColumn,
			SortOrder: self.sortValue,
		}

		APIRequest.getPostService(APIUrl.GET_SPORTS_LEVEL_LIST, InputData)
			.then((response) => {
				if (response.Data.length > 0) {
					self.setState({
						exportData: response.Data,
						IsExportExcel: true,
					})
				}
			})
			.catch((error) => {})
	}

	//update export excel flag as false
	componentDidUpdate() {
		if (this.state.IsExportExcel) {
			this.setState({
				IsExportExcel: false,
			})
		}
	}

	handleEditOnChange = (name, val) => {
		this.setState({
			...this.state,
			[name]: val,
		})
		if (name === "EditGamesSetupID") {
			this.setState({
				EditSportID: [],
				EditSportIDList: [],
			})
			this.EditGamesSetupIdChange(val)
			this.GamesEditSetupIDDateChange(val)
		}
		if (name === "EditSportID") {
			this.setState({
				EditSportEventID: [],
				EditSportEventIDList: [],
				EditFacilityID: [],
				EditFacilityIDList: [],
				EditRoundID: [],
				EditRoundIDList: [],
				EditSportFacilityDateID: "",
				EditSportFacilityTimeSlotID: [],
			})
			this.EditSportIdChange(val)
			this.EditSportEventIdChange(val)
		}
	}
	DynamicdisabledDate = (current) => {
		let StartDate = this.state.EditSportFacilityStartDate
		let EndDate = this.state.EditSportFacilityEndDate
		var StartDateconvert = new Date(StartDate)
		var expiryStartDate = StartDateconvert.setDate(new Date(StartDateconvert).getDate())
		let dynamicexpiryStartDate = new Date(expiryStartDate)
		let StartDateDay = ("0" + dynamicexpiryStartDate.getDate()).slice(-2)
		let StartDatemonth = ("0" + (dynamicexpiryStartDate.getMonth() + 1)).slice(-2)
		let StartDateformet =
			dynamicexpiryStartDate.getFullYear() + "-" + StartDatemonth + "-" + StartDateDay
		const start = moment(StartDateformet, "YYYY-MM-DD")
		var EndDateconvert = new Date(EndDate)
		var expiryEndDate = EndDateconvert.setDate(new Date(EndDateconvert).getDate())
		let dynamicexpiryEndDate = new Date(expiryEndDate)
		let EndDateDay = ("0" + dynamicexpiryEndDate.getDate()).slice(-2)
		let EndDatemonth = ("0" + (dynamicexpiryEndDate.getMonth() + 1)).slice(-2)
		let EndDateformet = dynamicexpiryEndDate.getFullYear() + "-" + EndDatemonth + "-" + EndDateDay
		const End = moment(EndDateformet, "YYYY-MM-DD")
		return current.isAfter(End) || current.isBefore(start)
	}
	DynamicdisabledFilterDate = (current) => {
		let date = this.state.FilterSportFacilityDate
		var dateconvert = new Date(date)
		var expiryDate = dateconvert.setDate(new Date(dateconvert).getDate() + 1)
		let dynamicexpiryDate = new Date(expiryDate)
		let Day = ("0" + dynamicexpiryDate.getDate()).slice(-2)
		let month = ("0" + (dynamicexpiryDate.getMonth() + 1)).slice(-2)
		let dataformet = dynamicexpiryDate.getFullYear() + "-" + month + "-" + Day
		const start = moment(dataformet, "YYYY-MM-DD")
		return current < start || current < moment()
	}

	render() {
		const columns = [
			{
				Header: () => <div className='vertical_center'>#</div>,
				accessor: "SerialNo",
				sortable: false,
				resizable: false,
				className: "tableheader wordwrap",
				headerClassName: "BoldText ColoredText",

				Cell: (row) => (
					<div className='tabledata' style={{ textAlign: "left" }}>
						{row.value}
					</div>
				),
				width: Math.round(window.innerWidth * 0.04),
			},
			{
				Header: () => <div className='vertical_center'>Games Setup</div>,
				accessor: "GamesSetupName",
				className: "tableheader wordwrap",
				resizable: false,
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.GamesSetupName}>
							{this.tableColumnDataLimit(row.original.GamesSetupName)}
						</div>
					)
				},
				width: Math.round(window.innerWidth * 0.1),
			},
			{
				Header: () => <div className='vertical_center'>Sport</div>,
				width: Math.round(window.innerWidth * 0.1),
				accessor: "Sport",
				className: "tableheader wordwrap",
				resizable: false,
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.Sport}>
							{this.tableColumnDataLimit(row.original.Sport)}
						</div>
					)
				},
			},
			{
				Header: () => <div className='vertical_center'>Events</div>,
				width: Math.round(window.innerWidth * 0.12),
				accessor: "SportEvent",
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return (
						<div className='tabledata' title={row.original.SportEvent}>
							{this.tableColumnDataLimit(row.original.SportEvent)}
						</div>
					)
				},
			},
			{
				Header: () => <div className='vertical_center'>Rounds/ Levels/ Sets</div>,
				width: Math.round(window.innerWidth * 0.1),
				accessor: "RoundName",
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return (
						<div className='tabledata' title={row.original.RoundName}>
							{this.tableColumnDataLimit(row.original.RoundName)}
						</div>
					)
				},
			},
			{
				Header: () => <div className='vertical_center'>Facility</div>,
				width: Math.round(window.innerWidth * 0.1),
				accessor: "Facility",
				className: "tableheader wordwrap",
				resizable: false,
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.Facility}>
							{this.tableColumnDataLimit(row.original.Facility)}
						</div>
					)
				},
			},
			{
				Header: () => <div className='vertical_center'>Date</div>,
				width: Math.round(window.innerWidth * 0.08),
				accessor: "SportFacilityDatestr",
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return (
						<div className='tabledata ' title={row.original.SportFacilityDatestr}>
							{this.tableColumnDataLimit(row.original.SportFacilityDatestr)}
						</div>
					)
				},
			},
			{
				Header: () => <div className='vertical_center'>Slot</div>,
				width: Math.round(window.innerWidth * 0.08),
				accessor: "SportSlot",
				filterable: false,
				className: "tableheader wordwrap",
				resizable: false,
				Cell: (row) => {
					return (
						<div className='tabledata' title={row.original.SportSlot}>
							{this.tableColumnDataLimit(row.original.SportSlot)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "center" }}>{"     "}Action</div>,
				accessor: "Action",
				className: "tableheader wordwrap",
				sortable: false,
				resizable: false,
				textAlign: "center",
				width: Math.round(window.innerWidth * 0.08),
				filterable: false,
				Cell: (row) => {
					return (
						<div
							className='tabledata text-left'
							style={{ display: "flex", justifyContent: "space-around" }}
						>
							<FontAwesomeIcon
								title='View'
								className='icon_eye'
								onClick={() => this.openViewAlldataModal(row.original, this)}
								icon={faEye}
								style={{ cursor: "pointer" }}
								width='3rem'
							/>

							<FontAwesomeIcon
								title='View All details'
								className='mr-2 icon_eye'
								onClick={() => this.openEditAlldataModal(row.original, this)}
								icon={faEdit}
								style={{ cursor: "pointer" }}
								width='3rem'
							/>
							<FontAwesomeIcon
								title='Delete'
								className=' icon_Delete'
								onClick={() => openModal(row.original.GamesSportLevelID, this)}
								icon={faTrashAlt}
								style={{ cursor: "pointer" }}
								width='3rem'
							/>
						</div>
					)
				},
			},
		]
		return (
			<>
				<div className=' pt-10 ' >
					<Spin size='large' spinning={this.state.loading} tip='Loading...'>
						<div className=' d-flex justify-content-start pt-1 '>
							<p className='tab_form_content games_layout'>Manage Sport Levels</p>
						</div>
						<div className='pull-right'>
							<Button
								className='mr-2 btn_green'
								style={{
									cursor: "pointer",
								}}
								onClick={this.openAddSportModal}
							>
								Add Sport Level
							</Button>
						</div>
						<Card className='w-100 card from-group'>
							<Card.Header
								className='text-white'
								onClick={() => setIsExpanded(!this.state.IsExpanded, this)}
							>
								<div className='float-left filter-heading-font'>Search Sport Levels </div>
								<div className='float-right'>
									{this.state.Icons ? (
										<FontAwesomeIcon
											color=' #FFFFFF'
											className='action-icon-font'
											icon={faArrowUp}
											style={{ cursor: "pointer" }}
										></FontAwesomeIcon>
									) : (
										<FontAwesomeIcon
											color=' #FFFFFF'
											className='action-icon-font'
											icon={faArrowDown}
											style={{ cursor: "pointer" }}
										></FontAwesomeIcon>
									)}
								</div>
							</Card.Header>
							<Collapse in={this.state.IsExpanded}>
								<Card.Body>
									<div className='row'>
										<div className='form-group col-sm-4'>
											<label>Games Setup</label>
											<Select
												showArrow={true}
												showSearch={true}
												className='textfiled w-80'
												mode='multiple'
												maxTagCount={2}
												bordered={false}
												placeholder='Select Games Setup'
												name='Games Setup'
												value={this.state.FilterGamesSetupID}
												size={"large"}
												onChange={this.handleGamesSetupIDChange.bind(this)}
												optionFilterProp='children'
												filterSort={(GamesSetupdata, GamesSetupvalue) =>
													GamesSetupdata.children
														.toLowerCase()
														.localeCompare(GamesSetupvalue.children.toLowerCase())
												}
											>
												{this.state.FilterGamesSetupIDList.map((c) => (
													<Option value={c.OptionID}>{c.OptionData}</Option>
												))}
											</Select>
										</div>
										<div className='form-group col-sm-4'>
											<label>Sport</label>
											<Select
												showArrow={true}
												showSearch={true}
												className='textfiled w-80'
												mode='multiple'
												maxTagCount={2}
												bordered={false}
												placeholder='Select Sport'
												name='Sport'
												value={this.state.FilterSportID}
												size={"large"}
												onChange={this.handleSportIDChange.bind(this)}
												optionFilterProp='children'
												filterSort={(Sportdata, Sportvalue) =>
													Sportdata.children
														.toLowerCase()
														.localeCompare(Sportvalue.children.toLowerCase())
												}
											>
												{this.state.FilterSportIDList.map((c) => (
													<Option value={c.OptionID}>{c.OptionData}</Option>
												))}
											</Select>
										</div>
										<div className='form-group col-sm-4'>
											<label>Events</label>
											<Select
												showArrow={true}
												showSearch={true}
												className='textfiled w-80'
												mode='multiple'
												maxTagCount={2}
												bordered={false}
												placeholder='Select Sport'
												name='Sport'
												value={this.state.FilterSportEventID}
												size={"large"}
												onChange={this.handleSportEventIDChange.bind(this)}
												optionFilterProp='children'
												filterSort={(Sportdata, Sportvalue) =>
													Sportdata.children
														.toLowerCase()
														.localeCompare(Sportvalue.children.toLowerCase())
												}
											>
												{this.state.FilterSportEventIDList.map((c) => (
													<Option value={c.OptionID}>{c.OptionData}</Option>
												))}
											</Select>
										</div>
										<div className='form-group col-sm-4'>
											<label>Rounds/ Levels/ Sets</label>
											<Select
												showArrow={true}
												showSearch={true}
												className='textfiled w-80'
												mode='multiple'
												maxTagCount={2}
												bordered={false}
												placeholder='Select Rounds/ Levels/ Sets'
												name='Round'
												value={this.state.FilterRoundID}
												size={"large"}
												onChange={this.handleRoundIDChange.bind(this)}
												optionFilterProp='children'
												filterSort={(Rounddata, Roundvalue) =>
													Rounddata.children
														.toLowerCase()
														.localeCompare(Roundvalue.children.toLowerCase())
												}
											>
												{this.state.RoundIDList.map((c) => (
													<Option value={c.OptionID}>{c.OptionData}</Option>
												))}
											</Select>
										</div>

										<div className='form-group col-sm-4'>
											<label>Facility</label>
											<Select
												showArrow={true}
												showSearch={true}
												className='textfiled w-80'
												mode='multiple'
												maxTagCount={2}
												bordered={false}
												placeholder='Select Facility'
												name='Facility'
												value={this.state.FilterFacilityID}
												size={"large"}
												onChange={this.handleFacilityIDChange.bind(this)}
												optionFilterProp='children'
												filterSort={(Facilitydata, Facilityvalue) =>
													Facilitydata.children
														.toLowerCase()
														.localeCompare(Facilityvalue.children.toLowerCase())
												}
											>
												{this.state.FacilityIDList.map((c) => (
													<Option value={c.OptionID}>{c.OptionData}</Option>
												))}
											</Select>
										</div>
										<div className='form-group col-sm-4'>
											<label>Date</label>
											<DatePicker
												size='large'
												format='DD MMM YY'
												disabled={false}
												bordered={false}
												allowClear
												name='FilterSportFacilityDate'
												value={this.state.FilterSportFacilityDate}
												onChange={this.handleSportFacilityDateIDChange.bind(this)}
												className='myDatePicker datepickerfiled '
												placeholder='Select Date'
											/>
										</div>
										<div className='form-group col-sm-4'>
											<label>Slot</label>
											<Select
												showArrow={true}
												showSearch={true}
												className='textfiled w-80'
												mode='multiple'
												maxTagCount={2}
												bordered={false}
												style={{ width: "100%" }}
												placeholder='Select Slot'
												name='TimeSlot'
												value={this.state.FilterSportFacilityTimeSlotID}
												size={"large"}
												onChange={this.handleSportFacilityTimeSlotIDChange.bind(this)}
												optionFilterProp='children'
												filterSort={(TimeSlotdata, TimeSlotvalue) =>
													TimeSlotdata.children
														.toLowerCase()
														.localeCompare(TimeSlotvalue.children.toLowerCase())
												}
											>
												{this.state.FilterSportFacilityTimeSlotIDList.map((c) => (
													<Option value={c.OptionData}>{c.OptionData}</Option>
												))}
											</Select>
										</div>
									</div>
									<div className='col-sm-12 '>
										<div className=' d-flex justify-content-end row'>
											<Button
												onClick={this.fetch}
												className='mr-2 btn_green'
												disabled={this.state.isSearchBtn}
											>
												Search
											</Button>
											<Button onClick={this.handleListClear} className='btn_green'>
												Clear
											</Button>
										</div>
									</div>
								</Card.Body>
							</Collapse>
						</Card>
						<div className='admin-report-table pt-5'>
							<ReactTable
								data={this.state.users}
								columns={columns}
								showPaginationTop={false}
								showPaginationBottom={false}
								minRows={0}
								defaultPageSize={10}
								defaultPage={1}
								onFetchData={this.fetch}
								ref={this.table}
								sortable={true}
								multiSort={true}
								manual
								className={"CosmicTable"}
								showPageSizeOptions={false}
								NoDataComponent={NoDataConst}
								defaultSorting={[
									{
										id: "id",
										desc: true,
									},
								]}
								manualPagination={true}
							/>
						</div>
						<div className='pt-3 row'>
							<div className=' col-sm-6'>
								<Button
									className='mr-2 btn_green'
									style={{
										//paddingRight: "0.4rem", paddingLeft: "0.4rem",
										cursor: "pointer",
									}}
									disabled={this.state.exportbtn}
									onClick={this.handlerExportData}
								>
									<FontAwesomeIcon
										title='Delete'
										color={"#FFFFFF"}
										icon={faDownload}
										// marginRight={"5px !important"}
									></FontAwesomeIcon>{" "}
									Export Excel
								</Button>
							</div>
							<div className='col-sm-6 d-flex justify-content-end'>
								<Pagination
									totalPages={this.state.TotalPages}
									totalRecords={this.state.TotalRecords}
									paginationCall={this.handlerPagination}
								/>
							</div>
						</div>
						<Fragment>
							<Modal
								size='sm'
								//aria-labelledby="contained-modal-title-vcenter"
								backdrop='static'
								//centered
								show={this.state.isOpen}
								onHide={this.closeModal}
							>
								<div className='popup-content'>
									<div className='deletecircle'></div>
									<i className='fa fa-trash-o deleteicon'></i>
									<Modal.Body>
										<p className='pull-left' style={{ margin: "4rem 2rem" }}>
											{" "}
											<h5>{Message.GAME_SPORTLEVEL_DELETE}</h5>
											<h6 style={{ color: "darkgray" }}>{Message.GAME_SPORTLEVEL_CONFIRM}</h6>
											<h6
												style={{
													color: "darkgray",
													marginRight: "7rem",
													marginBottom: "-3rem",
												}}
											>
												{Message.INVITE_DELETE_CONFIRM1}
											</h6>
										</p>
										<br />
										<div className='pull-right'>
											<Button className='btn_cancel mr-2' onClick={this.closeModal}>
												Cancel
											</Button>
											<Button
												className='btn_green'
												style={{ height: "2rem", padding: "5px 10px" }}
												onClick={this.deleteModal}
											>
												Delete
											</Button>
										</div>
									</Modal.Body>
								</div>
							</Modal>
						</Fragment>
						{this.state.IsExportExcel ? (
							<ExportData
								data={this.state.exportData}
								label={exportLabel}
								filename={this.state.exportFilename}
							/>
						) : null}
					</Spin>
					<div>
						<Modal // style={{width:"40%"}}
							//aria-labelledby="contained-modal-title-vcenter"
							//   aria-labelledby="contained-modal-title-vcenter"
							backdrop='static'
							size='md'
							className='popupborder'
							keyboard={true}
							show={this.state.isEyeDatapopup}
							onHide={this.viewAlldatapopupClose} //toggle={props.onCloseModal}
							aria-labelledby='contained-modal-title-vcenter'
							//centered
						>
							<Modal.Header className=''>
								<div className='col-sm-12'>
									<div className='row'>
										<div className='col-sm-11'>
											<div className='d-flex justify-content-center'>
												<h5 className='font-weight-bold text-white mb-0'>{"Sport Level"}</h5>
											</div>
										</div>
										<div className='col-sm-1'>
											<div className='d-flex justify-content-end'>
												<button
													type='button'
													className='close text-white'
													onClick={this.viewAlldatapopupClose}
													data-dismiss='modal'
													aria-label='Close'
												>
													<span aria-hidden='true'>&times;</span>
												</button>{" "}
											</div>
										</div>
									</div>
								</div>
							</Modal.Header>
							<Modal.Body className=''>
								<div className='col-sm-12'>
									<div className='row pt-10'>
										<div className='col-sm-1'>{""}</div>
										<div className='col-sm-10'>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Games Setup</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.isEyeData.GamesSetupName}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Sport</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.isEyeData.Sport}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>SportEvent</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.isEyeData.SportEvent}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>
														Rounds/ Levels/ Sets
													</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.isEyeData.RoundName}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Facility</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.isEyeData.Facility}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Date</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.isEyeData.SportFacilityDatestr}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Slot</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.isEyeData.SportSlot}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Created By </label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.isEyeData.CreatedBy}</label>{" "}
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className='d-flex justify-content-center'>
									<Button className='cancel mr-2 btn_green' onClick={this.viewAlldatapopupClose}>
										Cancel
									</Button>
								</div>
							</Modal.Body>
						</Modal>
					</div>
					<div>
						<Modal
							backdrop='static'
							size='md'
							className='popupborder'
							keyboard={true}
							show={this.state.isAddEditDatapopup}
							onHide={this.viewAlldatapopupClose}
							aria-labelledby='contained-modal-title-vcenter'
						>
							<Modal.Header className=''>
								<div className='col-sm-12'>
									<div className='row'>
										<div className='col-sm-11'>
											<div className='d-flex justify-content-center'>
												<h5 className='font-weight-bold text-white mb-0'>
													{this.state.isAddSport ? "Add Sport Level" : "Edit Sport level"}
												</h5>
											</div>
										</div>
										<div className='col-sm-1'>
											<div className='d-flex justify-content-end'>
												<button
													type='button'
													className='close text-white'
													onClick={this.viewAlldatapopupClose}
													data-dismiss='modal'
													aria-label='Close'
												>
													<span aria-hidden='true'>&times;</span>
												</button>{" "}
											</div>
										</div>
									</div>
								</div>
							</Modal.Header>
							<Modal.Body className=''>
								<div className='col-sm-12'>
									<div className='row pt-10'>
										<div className='col-sm-1'>{""}</div>
										<div className='col-sm-10'>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Games Setup</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7 editSportLevel'>
													<Select
														className={
															this.state.isAddSport ? "textfiled w-80" : "textfiled w-80 disabled"
														}
														showArrow={this.state.isAddSport ? true : false}
														maxTagCount={2}
														bordered={false}
														placeholder='Select Games Setup'
														name='GamesSetupID'
														value={this.state.EditGamesSetupID}
														size={"large"}
														onChange={(val) => this.handleEditOnChange("EditGamesSetupID", val)}
														optionFilterProp='children'
														filterSort={(Setupdata, Setupvalue) =>
															Setupdata.children
																.toLowerCase()
																.localeCompare(Setupvalue.children.toLowerCase())
														}
													>
														{this.state.EditGamesSetupIDList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
													<span className='val-err-msg'>{this.state.errors.GamesSetup}</span>
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Sport</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7 editSportLevel'>
													<Select
														className={
															this.state.isAddSport ? "textfiled w-80" : "textfiled w-80 disabled"
														}
														showArrow={this.state.isAddSport ? true : false}
														maxTagCount={2}
														bordered={false}
														placeholder='Select Sport'
														name='SportID'
														value={
															this.state.isAddSport ? this.state.EditSportID : this.state.EditSport
														}
														size={"large"}
														onChange={(val) => this.handleEditOnChange("EditSportID", val)}
														optionFilterProp='children'
														filterSort={(Sportdata, Sportvalue) =>
															Sportdata.children
																.toLowerCase()
																.localeCompare(Sportvalue.children.toLowerCase())
														}
													>
														{this.state.EditSportIDList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
													<span className='val-err-msg'>{this.state.errors.Sport}</span>
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Sport Events</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7 editSportLevel'>
													<Select
														className={
															this.state.isAddSport ? "textfiled w-80" : "textfiled w-80 disabled"
														}
														showArrow={this.state.isAddSport ? true : false}
														maxTagCount={2}
														bordered={false}
														placeholder='Select Events'
														name='SportEventID'
														value={
															this.state.isAddSport
																? this.state.EditSportEventID
																: this.state.EditSportEvent
														}
														size={"large"}
														onChange={(val) => this.handleEditOnChange("EditSportEventID", val)}
														optionFilterProp='children'
														filterSort={(Sportdata, Sportvalue) =>
															Sportdata.children
																.toLowerCase()
																.localeCompare(Sportvalue.children.toLowerCase())
														}
													>
														{this.state.EditSportEventIDList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
													<span className='val-err-msg'>{this.state.errors.SportEvent}</span>
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>
														Rounds/ Levels/ Sets
													</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													<Select
														className='textfiled w-80'
														maxTagCount={2}
														bordered={false}
														placeholder='Select Rounds/ Levels/ Sets'
														name='RoundID'
														mode='multiple'
														value={this.state.EditRoundID}
														size={"large"}
														onChange={(val) => this.handleEditOnChange("EditRoundID", val)}
														optionFilterProp='children'
														filterSort={(Rounddata, Roundvalue) =>
															Rounddata.children
																.toLowerCase()
																.localeCompare(Roundvalue.children.toLowerCase())
														}
													>
														{this.state.EditRoundIDList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
													<span className='val-err-msg'>{this.state.errors.Round}</span>
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Facility</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7 editSportLevel'>
													<Select
														className={
															this.state.isAddSport ? "textfiled w-80" : "textfiled w-80 disabled"
														}
														showArrow={this.state.isAddSport ? true : false}
														maxTagCount={2}
														bordered={false}
														placeholder='Select Facility'
														name='FacilityID'
														value={this.state.EditFacilityID}
														size={"large"}
														onChange={(val) => this.handleEditOnChange("EditFacilityID", val)}
														optionFilterProp='children'
														filterSort={(Facilitydata, Facilityvalue) =>
															Facilitydata.children
																.toLowerCase()
																.localeCompare(Facilityvalue.children.toLowerCase())
														}
													>
														{this.state.EditFacilityIDList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
													<span className='val-err-msg'>{this.state.errors.Facility}</span>
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Date</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													<DatePicker
														size='large'
														format='DD MMM YY'
														disabled={false}
														bordered={false}
														allowClear
														name='EditSportFacilityDateName'
														value={this.state.EditSportFacilityDateName}
														onChange={(val) =>
															this.handleEditOnChange("EditSportFacilityDateName", val)
														}
														className='myDatePicker datepickerfiled '
														placeholder='Select Date'
														disabledDate={this.DynamicdisabledDate}
													/>
													<span className='val-err-msg'>{this.state.errors.FacilityDate}</span>
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Slot</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													<Select
														className='textfiled w-80'
														maxTagCount={2}
														bordered={false}
														style={{ width: "100%" }}
														placeholder='Select Slot'
														name='SportFacilityTimeSlotID'
														value={this.state.EditSportFacilityTimeSlotID}
														size={"large"}
														onChange={(val) =>
															this.handleEditOnChange("EditSportFacilityTimeSlotID", val)
														}
														optionFilterProp='children'
														filterSort={(Leveldata, Levelvalue) =>
															Leveldata.children
																.toLowerCase()
																.localeCompare(Levelvalue.children.toLowerCase())
														}
													>
														{this.state.EditSportFacilityTimeSlotIDList.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
													<span className='val-err-msg'>{this.state.errors.TimeSlot}</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div className='d-flex justify-content-center'>
									<span className='val-err-msg'>{this.state.RecordAlreadyExists}</span>
									<Button
										className='mr-2 btn_green'
										disabled={this.state.isSubmitBtn}
										onClick={this.SportLevelSubmite}
									>
										Submit
									</Button>
									<Button className='cancel btn_green' onClick={this.viewAlldatapopupClose}>
										Close
									</Button>
								</div>
							</Modal.Body>
						</Modal>
					</div>
				</div>
			</>
		)
	}
}

export default PermissionProvider(SportsLevel)
