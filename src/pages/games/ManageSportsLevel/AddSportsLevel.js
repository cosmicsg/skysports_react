import React from "react";

import "../../../styles/CosmicTable.css";
import { Button, Container } from "react-bootstrap";
import "../../../App";
import { Select, notification } from "antd";
import "antd/dist/antd.css";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";

import PermissionProvider from "../../../permissions/PermissionProvider";

const { Option } = Select;

const openNotificationWithIcon = (type, description, flag) => {
  if (flag === 1) {
    ///////delete flag
    notification[type]({
      message: description,
      icon: (
        <FontAwesomeIcon
          className="fa-xs icon_Delete "
          icon={faTrashAlt}
        ></FontAwesomeIcon>
      ),
      description: "",
    });
  } else if (flag === 2) {
    notification[type]({
      message: description,
      description: "",
      //  duration:20
    });
  }
};
const handelerBtnDelete = (self) => {
  //this.props.isLoadingStartFunc()
  //self.props.isLoadingStartFunc()
  let rowId = Object.keys(self.state.selected);
  const fieldsCopy = self.state.fields.filter(
    (row) => !rowId.toString().includes(row.id)
  );
  self.setState({ fields: fieldsCopy, selected: [], errors: [] }, function () {
    if (self.state.fields.length === 0) {
      self.setState({
        btnDelete: true,
        btnDeleteAll: true,
        btnSend: true,
        btnSendall: true,
        addUsertxt: "Add Sport",
      });
    }
    //self.props.isLoadingStopFunc()
    openNotificationWithIcon("success", "Record has been deleted", 1);
  });
};

const handleDeleteAll = (self) => {
  //this.props.isLoadingStartFunc()
  self.setState(
    {
      fields: [],
      rowcount: 0,
      selected: [],
      btnDeleteAll: true,
      btnSendall: true,
      btnDelete: true,
      btnSend: true,
      addUsertxt: "Add Sport",
      errors: [],
    },
    function () {
      //self.props.isLoadingStopFunc()
      openNotificationWithIcon("success", "Record has been deleted", 1);
    }
  );
};
class AddSportsLevel extends React.Component {
  constructor(props) {
    super(props);

    let permissions = this.props.getAppPermissions();

    let roles = this.props.getAppRoles();

    //let SelectedSportid = 0
    this.state = {
      selected: {},
      selectAll: 0,
      rowcount: 0,
      fields: [],
      countries: [],
      Sports_list: [],
      Round_list: [],
      //Level_list: [],
      TimeSlot_list: [],
      Facility_list: [],
      SportDate_list: [],
      errors: {
        Addanotheruser: "",
      },
      btnSendall: true,
      btnSend: true,
      btnDelete: true,
      btnDeleteAll: true,
      btnAddinvite: true,
      isOpen: false,
      isDelete: false,
      isDeleteId: 0,
      isDeleteflag: 0,
      addUsertxt: "Add Sport",
      permissions: permissions,
      Sports: roles,
      Round: [],
      //Level: [],
      TimeSlot: [],
      Facility: [],
      SportDate: [],
      AddSportID: [],
      AddFacilityID: [],
      AddRoundID: [],
      //AddLevelID: [],
      AddSportDateID: [],
      AddSportFacilityTimeSlotID: [],
      RecordAlreadyExists: "",
      // loading: false,
    };
    this.table = React.createRef(null);
    this.closeModal = this.closeModal.bind(this);
    this.deleteModal = this.deleteModal.bind(this);
    this.disabledDate = this.disabledDate.bind(this);
    this.SportIdChange = this.SportIdChange.bind(this);
    this.RoundIdChange = this.RoundIdChange.bind(this);
    //this.LevelIdChange = this.LevelIdChange.bind(this)
    this.DateIDChange = this.DateIDChange.bind(this);
  }
  disabledDate = (current) => {
    return moment().add(-1, "days") >= current;
    //current && current < (moment().endOf('day')-1);
  };
  onSubmit = () => {
    let Sports = this.state.AddSportID;
    let Sportsdata = Sports.toString();
    let Round = this.state.AddRoundID;
    let Rounddata = Round.toString();
    // let Level = this.state.AddLevelID
    // let Leveldata = Level.toString()
    let SportDate = this.state.AddSportDateID;
    let SportDatedata = SportDate.toString();
    let TimeSlot = this.state.AddSportFacilityTimeSlotID;
    let TimeSlotdata = TimeSlot.toString();
    let Facility = this.state.AddFacilityID;
    let Facilitydata = Facility.toString();
    let valid = 0;
    let Sportsid = document.getElementById("Sports_tbl");
    let Roundid = document.getElementById("Round_tbl");
    // let Levelid = document.getElementById("Level_tbl")
    let TimeSlotid = document.getElementById("TimeSlot_tbl");
    let Facilityid = document.getElementById("Facility_tbl");
    let SportDateid = document.getElementById("SportDate_tbl");
    if (this.state.AddSportID === undefined || Sportsdata === "") {
      if (Sportsid != null) {
        valid = 1;
        Sportsid.classList.add("mandatory_text");
      }
    }
    if (this.state.AddRoundID === undefined || Rounddata === "") {
      if (Roundid != null) {
        valid = 1;
        Roundid.classList.add("mandatory_text");
      }
    }
    if (
      this.state.AddSportFacilityTimeSlotID === undefined ||
      TimeSlotdata === ""
    ) {
      if (TimeSlotid != null) {
        valid = 1;
        TimeSlotid.classList.add("mandatory_text");
      }
    }
    if (this.state.AddFacilityID === undefined || Facilitydata === "") {
      if (Facilityid != null) {
        valid = 1;
        Facilityid.classList.add("mandatory_text");
      }
    }
    if (this.state.AddSportDateID === undefined || SportDatedata === "") {
      if (SportDateid != null) {
        valid = 1;
        SportDateid.classList.add("mandatory_text");
      }
    }
    if (valid === 0) {
      let InputData = [
        {
          GamesSportLevelID: 0,
          SportFacilityDateID: SportDatedata,
          SportID: Sportsdata,
          SportRoundID: Rounddata,
          //SportLevelID: Leveldata,
          SportFacilityTimeSlotID: TimeSlotdata,
          FacilityID: Facilitydata,
        },
      ];
      this.props.isLoadingStartFunc();
      APIRequest.getPostService(APIUrl.ADD_SPORTS_LEVEL, InputData)
        .then((response) => {
          if (response.Succeeded === true) {
            if (response.Data === "Already exists") {
              this.props.isLoadingStopFunc();
              this.setState({
                ...this.state,
                RecordAlreadyExists: "Record already exists.",
              });
            } else {
              this.handleClear();
              this.props.refreshSportLevelListFunc();
              this.props.isLoadingStopFunc();
            }
          }
        })
        .catch((error) => {
          this.props.isLoadingStopFunc();
        });
    } else if (valid === 1) {
      let errors = {};
      errors["Addanotheruser"] =
        "One or more mandatory fields have an error. Please check and try again";
      this.setState({
        errors: errors,
      });
    }
  };

  handleClear = () => {
    this.setState({
      AddSportDateID: [],
      AddSportID: [],
      AddRoundID: [],
      AddSportFacilityTimeSlotID: [],
      AddFacilityID: [],
      Round_list: [],
      TimeSlot_list: [],
      Facility_list: [],
      SportDate_list: [],
      RecordAlreadyExists: "",
    });
  };

  deleteModal = async () => {
    let id = this.state.isDeleteId;
    let Rowid = this.state.isDeleteRowId;
    let flag = this.state.isDeleteflag;
    if (flag === 1) {
      handleDeleteAll(this);
      this.closeModal();
    } else if (flag === 2) {
      handelerBtnDelete(this);
      this.closeModal();
    } else if (flag === 3) {
      this.props.isLoadingStartFunc();
      let fields = this.state.fields;
      fields.splice(Rowid, 1);
      this.setState({ fields });
      this.closeModal();
      this.setState({ errors: [] });
      let rowId = this.state.selected;
      let data = rowId[id];
      if (data !== undefined) {
        delete rowId[id];
      }
      this.props.isLoadingStopFunc();
      openNotificationWithIcon("success", "Record has been deleted", 1);
      if (Object.keys(this.state.selected).length === 0) {
        this.setState({ btnDelete: true, btnSend: true });
      }
      if (this.state.fields.length === 0) {
        this.setState({
          rowcount: 0,
          selected: [],
          btnDelete: true,
          btnDeleteAll: true,
          btnSend: true,
          btnSendall: true,
          addUsertxt: "Add Sport",
        });
      }
    }
  };
  closeModal = () => {
    this.setState({ isOpen: false });
  };

  async componentDidMount() {
    //let usertype = this.props.name
    const Sports = await APIRequest.getGetService(APIUrl.GET_SPORT_LIST);
    this.setState({ Sports_list: Sports.Data });
  }
  SportIdChange = (Sportsid) => {
    this.setState({
      ...this.state,
      AddSportDateID: [],
      AddRoundID: [],
      AddSportFacilityTimeSlotID: [],
      AddFacilityID: [],
      Round_list: [],
      TimeSlot_list: [],
      Facility_list: [],
      SportDate_list: [],
    });
    APIRequest.getPostService(APIUrl.GET_FACILITY_LIST + "/" + Sportsid)
      .then((response) => {
        if (response.Succeeded === true) {
          this.setState({
            ...this.state,
            Facility_list: response.Data,
            AddSportID: Sportsid,
          });
        }
      })
      .catch((error) => {
        //self.props.isLoadingStopFunc()
      });
    this.RoundIdChange(Sportsid);
    //this.LevelIdChange(Sportsid)
  };
  RoundIdChange = (Sportsid) => {
    APIRequest.getPostService(APIUrl.GET_ROUND_LIST + "/" + Sportsid)
      .then((response) => {
        if (response.Succeeded === true) {
          this.setState({ Round_list: response.Data });
        }
      })
      .catch((error) => {
        //self.props.isLoadingStopFunc()
      });
  };

  DateIDChange = (Facilityid) => {
    let Sportsid = this.state.AddSportID;
    APIRequest.getPostService(
      APIUrl.GET_DATE_LIST + "/" + Sportsid + "/" + Facilityid
    )
      .then((response) => {
        if (response.Succeeded === true) {
          this.setState({
            ...this.state,
            SportDate_list: response.Data,
            AddFacilityID: Facilityid,
          });
        }
      })
      .catch((error) => {});
  };
  RoundChange = (e) => {
    this.setState({
      ...this.state,
      AddRoundID: e,
    });
  };

  FacilityTimeSlotChange = (e) => {
    this.setState({
      ...this.state,
      AddSportFacilityTimeSlotID: e,
    });
  };

  render() {
    const { errors } = this.state;
    return (
      <div>
        <Container fluid>
          <div
            className="admin-report-table"
            style={{ margin: "-5px !important" }}
          >
            <div className=" d-flex justify-content-start pt-1 ">
              <p className="tab_form_content games_layout">Add Sport</p>
            </div>
            <div className="row col-sm-12" style={{ marginTop: "2rem" }}>
              <div className="col-sm-6 Sports_tbl">
                <label>
                  Sport<span className="text-danger">*</span>
                </label>

                <Select
                  showSearch={true}
                  className="textfiled w-80"
                  bordered={false}
                  maxTagCount={2}
                  placeholder="Select Sport"
                  name="AddSportID"
                  size={"large"}
                  value={this.state.AddSportID}
                  onChange={this.SportIdChange.bind(this)}
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {this.state.Sports_list.map((c) => (
                    <Option value={c.OptionID}>{c.OptionData}</Option>
                  ))}
                </Select>
                <span className="val-err-msg">{errors.Sport}</span>
              </div>
              <div className="col-sm-6 Facility_tbl">
                <label>
                  Facility<span className="text-danger">*</span>
                </label>

                <Select
                  showSearch={true}
                  className="textfiled w-80"
                  bordered={false}
                  maxTagCount={2}
                  placeholder="Select Facility"
                  name="AddFacilityID"
                  size={"large"}
                  value={this.state.AddFacilityID}
                  onChange={this.DateIDChange.bind(this)}
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {this.state.Facility_list.map((c) => (
                    <Option value={c.OptionID}>{c.OptionData}</Option>
                  ))}
                </Select>
                <span className="val-err-msg">{errors.Facility}</span>
              </div>
            </div>
            <div className="row col-sm-12" style={{ marginTop: "2rem" }}>
              <div className="col-sm-6 Round_tbl">
                <label>
                  Round/ Level/ Set<span className="text-danger">*</span>
                </label>

                <Select
                  showSearch={true}
                  className="textfiled w-80"
                  bordered={false}
                  maxTagCount={2}
                  placeholder="Select Round/ Level/ Set"
                  name="AddRoundID"
                  size={"large"}
                  value={this.state.AddRoundID}
                  optionFilterProp="children"
                  onChange={this.RoundChange}
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {this.state.Round_list.map((c) => (
                    <Option value={c.OptionID}>{c.OptionData}</Option>
                  ))}
                </Select>
                <span className="val-err-msg">{errors.Round}</span>
              </div>
              {/* <div className='col-sm-6 Level_tbl'>
								<label>
									Level<span className='text-danger'>*</span>
								</label>

								<Select
									showSearch={true}
									className='textfiled w-80'
									bordered={false}
									maxTagCount={2}
									placeholder='Select Level'
									name='AddLevelID'
									size={"large"}
									value={this.state.AddLevelID}
									optionFilterProp='children'
									onChange={this.LevelChange}
									filterSort={(optionA, optionB) =>
										optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
									}
								>
									{this.state.Level_list.map((c) => (
										<Option value={c.OptionID}>{c.OptionData}</Option>
									))}
								</Select>
								<span className='val-err-msg'>{errors.Level}</span>
							</div> */}
              <div className="col-sm-6 SportDate_tbl">
                <label>
                  Date<span className="text-danger">*</span>
                </label>

                <Select
                  showSearch={true}
                  className="textfiled w-80"
                  bordered={false}
                  maxTagCount={2}
                  placeholder="Select Date"
                  name="AddSportDateID"
                  size={"large"}
                  value={this.state.AddSportDateID}
                  optionFilterProp="children"
                  //onChange={this.TimeslotIDChange.bind(this)}
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {this.state.SportDate_list.map((c) => (
                    <Option value={c.OptionID}>{c.OptionData}</Option>
                  ))}
                </Select>
                <span className="val-err-msg">{errors.Round}</span>
              </div>
            </div>
            <div className="row col-sm-12" style={{ marginTop: "2rem" }}>
              <div className="col-sm-6 TimeSlot_tbl">
                <label>
                  Time Slot<span className="text-danger">*</span>
                </label>

                <Select
                  showSearch={true}
                  className="textfiled w-80"
                  bordered={false}
                  maxTagCount={2}
                  placeholder="Select Time Slot"
                  name="AddSportFacilityTimeSlotID"
                  size={"large"}
                  value={this.state.AddSportFacilityTimeSlotID}
                  optionFilterProp="children"
                  onChange={this.FacilityTimeSlotChange}
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {this.state.TimeSlot_list.map((c) => (
                    <Option value={c.OptionID}>{c.OptionData}</Option>
                  ))}
                </Select>
                <span className="val-err-msg">{errors.Level}</span>
              </div>
            </div>

            <div className="col-sm-12 row">
              <div className="col-sm-6"></div>
              <div className=" col-sm-6 d-flex justify-content-end mt-4">
                <span className="val-err-msg">
                  {this.state.RecordAlreadyExists}
                </span>
                <Button onClick={this.onSubmit} className="btn_green mr-2">
                  Submit
                </Button>
                <Button onClick={this.handleClear} className="mr-2 btn_green">
                  Clear
                </Button>
              </div>
            </div>
          </div>
        </Container>
        {/* <ManageUserInviteList ref={this.table} /> */}
      </div>
    );
  }
}

export default PermissionProvider(AddSportsLevel);
