import React from "react"
import Fullcalendar from "@fullcalendar/react"
import timeGridPlugin from "@fullcalendar/timegrid"
import dayGridPlugin from "@fullcalendar/daygrid"
import listWeek from "@fullcalendar/list"
import interactionPlugin from "@fullcalendar/interaction"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import { Button, Modal } from "react-bootstrap"
import "../../../styles/Calendar.css"
import { Permissions as Permission, Roles } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"

class CalandarView extends React.Component {
	constructor(props) {
		super(props)

		let permissions = this.props.getAppPermissions();
		let roles = this.props.getAppRoles();

		this.state = {
			eventModel: false,
			event: [],
			eventtitle: "",
			eventfacility: "",
			rounddetail: "",
			eventDate: "",
			eventsourceID: 0,
			eventrounds: "",
			eventcreatedby: "",
			eventTimeSlot: "",
			permissions: permissions,
			roles: roles,
		}
		this.fetch = this.fetch.bind(this)
	}
	formatDate = (dateStr) => {
		const [year, month, day] = dateStr.split("-")
		let newDate = `${day}-${month}-${year}`
		return newDate
	}

	handleEventClick = ({ event }) => {
		this.setState({
			...this.state,
			eventModel: true,
			eventsourceID: event._def.sourceID,
			eventtitle: event._def.title,
			eventfacility: event._def.extendedProps.Facility,
			eventrounds: event._def.extendedProps.RoundName,
			eventcreatedby: event._def.extendedProps.CreatedBy,
			eventTimeSlot: event._def.extendedProps.Timeslot,
			eventDate: this.formatDate(event._def.extendedProps.SportDate),
		})
	}
	viewAlldatapopupClose = () => {
		this.setState({ ...this.state, eventModel: false })
	}
	async fetch(state) {
			let userRoles = this.state.roles
			userRoles.includes(Roles.ADMIN)
				? APIRequest.getGetService(APIUrl.GET_SCHEDULE_SPORT_PARTICIPANT_INFO)
						.then((response) => {
							if (response.Data.length > 0) {
								this.setState({ event: response.Data })
							} else {
							}
						})
						.catch((error) => {})
				: APIRequest.getGetService(APIUrl.GET_SCHEDULE_SPORT_LEVEL_INFO)
						.then((response) => {
							if (response.Data.length > 0) {
								this.setState({ event: response.Data })
							} else {
							}
						})
						.catch((error) => {})
	}
	componentDidMount = () => {
		this.fetch(this.state)
	}
	render() {
		let Permissions = this.state.permissions
		return (
			<>
				{Permissions.includes(Permission.G_S_S_VIEW) ? (
					<div className='fullCalendarelement calendar-border-layout'>
						<Fullcalendar
							plugins={[timeGridPlugin, interactionPlugin, dayGridPlugin, listWeek]}
							defaultAllDay={false}
							initialDate={new Date()}
							initialView='timeGridDay'
							headerToolbar={{
								left: "prev,next today",
								center: "title",
								right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek",
							}}
							buttonText={{
								today: "Today",
								month: "Month",
								day: "Day",
								week: "Week",
								list: "List",
							}}
							nowIndicator={true}
							navLinks={true} // can click day/week names to navigate views
							editable={true}
							selectable={true}
							selectMirror={true}
							dayMaxEvents={true} // allow "more" link when too many events
							events={this.state.event}
							eventClick={this.handleEventClick}
						/>
						<Modal
							className='m-t-5'
							size='md'
							backdrop='static'
							backdropClassName='notshow'
							id='sdsd'
							keyboard={true}
							onHide={this.viewAlldatapopupClose}
							aria-labelledby='contained-modal-title-vcenter'
							show={this.state.eventModel}
						>
							<Modal.Header className=''>
								<div className='col-sm-12'>
									<div className='row'>
										<div className='col-sm-10'>
												<h5 className='font-weight-bold text-white mb-1'>{"Event Detail"}</h5>
										</div>
										{/* <div className='col-sm-1'> */}
											<div className='col-sm pt-0'>
												<button
													type='button'
													className='close text-white'
													onClick={this.viewAlldatapopupClose}
													data-dismiss='modal'
													aria-label='Close'
												>
													<span aria-hidden='true'>&times;</span>
												</button>{" "}
											</div>
										{/* </div> */}
									</div>
								</div>
							</Modal.Header>
							<Modal.Body className=''>
								<div className='col-sm-12'>
									<div className='row pt-10'>
										<div className='col-sm-1'>{""}</div>
										<div className='col-sm-10'>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Title</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.eventtitle}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Facility</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.eventfacility}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>
														Round/ Level/ Set
													</label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.eventrounds}</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>Date / Time </label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>
														{this.state.eventDate} / {this.state.eventTimeSlot}
													</label>{" "}
												</div>
											</div>
											<div className='form-group row'>
												<div className='col-sm-4'>
													{" "}
													<label className='font-weight-bold popup_txt'>CreatedBy </label>{" "}
												</div>
												<div className='col-sm-1'> </div>
												<div className='col-sm-7'>
													{" "}
													<label>{this.state.eventcreatedby}</label>{" "}
												</div>
											</div>
										</div>
									</div>
									</div>
									<div className="row col-sm-12 d-flex justify-content-end mt-4">
										<Button className="btn_green" onClick={this.viewAlldatapopupClose}>
											Close
										</Button>
									</div>
								
							</Modal.Body>
						</Modal>
					</div>
				) : null}
			</>
		)
	}
}

export default PermissionProvider(CalandarView)
