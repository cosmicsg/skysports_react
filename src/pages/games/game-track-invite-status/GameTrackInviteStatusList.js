import React, { useState, useRef, useEffect } from "react"
import GameTrackInviteStatusFilter from "./GameTrackInviteStatusFilter"
import { Button } from "react-bootstrap"
import ReactTable from "react-table"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faBell, faDownload } from "@fortawesome/free-solid-svg-icons"
import Pagination from "../../../utils/Pagination"
import ExportData from "../../../utils/ExportData"
import { APIRequest, showNotification } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { Permissions } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"
import { connect } from "react-redux"

const exportLabel = [
	{ columnName: "S.No", value: "SerialNo" },
	{ columnName: "Name", value: "FullName" },
	{ columnName: "Team", value: "TeamName" },
	{ columnName: "Game", value: "GameName" },
	{ columnName: "Category", value: "Category" },
	{ columnName: "Status", value: "GamesInviteStatus" },
]

//Track Invite status list component
const GameTrackInviteStatusList = React.memo((props) => {
	//const [permissionGroup, setPermissionGroup] = useState([]);

	const [showAction, setShowAction] = useState(false)
	const [mainListData, setMainListData] = useState([])
	const table = useRef(null)
	const childRef = useRef(null)

	//Pagination properties
	const [paginationProp, setPaginationProp] = useState({
		pageNo: 1,
		pageSize: 10,
		totalRecords: 0,
		totalPages: 0,
		sortColumn: "UserName",
		sortOrder: "ASC",
		totalCount: 0,
	})

	//Export excel flag and its data properties
	const [exportExcelProps, setExportExcelProps] = useState({
		isExportExcel: false,
		exportData: [],
	})
	const [canExport, setCanExport] = useState(true)
	const [isClickedExport, setClickedExport] = useState(false)

	//Refresh List flag
	const [isRefreshList, setRefreshList] = useState(false)

	//No records found
	const NoDataProp = (props) => <span className='table-nodata-style'>No records found </span>

	//Concat with dot(...) notation when the string length > 30
	const tableColumnDataLimit = (str) => {
		return str.length > 30 ? str.substring(0, 27) + "..." : str
	}

	//Lifecycle method, here refreshing the mainList data
	useEffect(() => {
		//
		// Reading user's permissions from PermissionProvider component
		//
		let permissionArray = props.getAppPermissions()

		//Assign permission for this page
		//setPermissionGroup(permissionArray);
		if (permissionArray.includes(Permissions.TRACK_HOST_COUNTRY_REMAINDER)) setShowAction(true)
		//Call refreshListFunc method to load data
		refreshListFunc()
	}, [])

	const refreshListFunc = (pageNo) => {
		/* This method is from GameTrackInviteStatusFilter component, which is 
    used to refresh the mainList data */
		childRef.current.refreshListData(pageNo)
	}

	/* This method will be called by GameTrackInviteStatusFilter component, which is used to assign the 
  mainList data */
	const childCallBackFunc = (data) => {
		if (isClickedExport && data !== undefined) {
			setExportExcelProps((prevState) => ({
				...prevState,
				exportData: data,
				isExportExcel: true,
			}))
			setExportExcelProps({
				exportData: [],
				isExportExcel: false,
			})
			setClickedExport(false)
		} else if (data !== undefined) {
			setMainListData(data)
			setCanExport(false)
			if (data.length > 0) {
				let firstRow = data[0]
				setPaginationProp((prevState) => ({
					...prevState,
					totalPages: firstRow.TotalPages,
					totalRecords: firstRow.TotalRows,
					totalCount: data.length >= 1 ? data.length : 0,
				}))

				/* isRefreshList flag set to false, because we do not want to refresh the list
				 */
			} else {
				setPaginationProp((prevState) => ({
					...prevState,
					totalPages: 0,
					totalRecords: 0,
					totalCount: 0,
				}))
			}
		}
		if (data !== undefined && data.length === 0) {
			setCanExport(true)
		}
		setRefreshList(false)
	}

	//Sorting column
	const sortingColumnFunc = (page) => {
		if (page && page.sorted && page.sorted[0]) {
			setPaginationProp((prevState) => ({
				...prevState,
				pageNo: 1,
				sortColumn: page.sorted[0].id,
				sortOrder: page.sorted[0].desc === true ? "DESC" : "ASC",
			}))
			//
			setRefreshList(true)
		}
	}

	//Pagination
	const pageRouterFunc = (pageNo) => {
		
		if (pageNo > 0) {
			setPaginationProp((prevState) => ({
				...prevState,
				pageNo: pageNo,
			}))

			setRefreshList(true)
		}
	}

	//Refresh list when isRefreshList flag is true
	useEffect(() => {
		if (isRefreshList) {
			refreshListFunc()
		}
	}, [isRefreshList])

	//Export excel
	const exportExcelFunc = () => {
		setClickedExport(true)

		setRefreshList(true)
	}

	//Remainder
	const remainderFunc = (row) => {
		const { UserProfileID, GamesSetupID } = row
		if (UserProfileID > 0) {
			resendGamesInviteNotification(UserProfileID, GamesSetupID)
		} else {
			showNotification("Invalid user", "error")
		}
	}

	//Resend or remainder notification for games invites
	const resendGamesInviteNotification = (userIDs, GamesSetupID) => {
		let users = [userIDs].join(",")

		APIRequest.getGetService(
			APIUrl.RESEND_GAMES_INVITE_NOTIFICATION + "/" + users + "/" + GamesSetupID
		)
			.then((res) => {
				if (res.Succeeded && res.Data) {
					showNotification("Notification Resent successfully.", "success")
				}
			})
			.catch((err) => {})
	}

	//Columns
	let columns = [
		{
			Header: () => <div style={{ textAlign: "left" }}>S.No</div>,
			accessor: "SerialNo",
			sortable: false,
			resizable: false,
			className: "tableheader wordwrap",
			headerClassName: "BoldText ColoredText",

			Cell: (row) => {
				return (
					<div className='tabledata' style={{ textAlign: "left" }}>
						{row.value}
					</div>
				)
			},
			width: Math.round(window.innerWidth * 0.04),
		},
		{
			Header: () => <div style={{ textAlign: "left" }}>User Name</div>,
			accessor: "FullName",
			resizable: false,
			style: { overflowWrap: "break-word" },

			Cell: (row) => {
				return (
					<div className='tabledata wordwrap' titile={row.original.FullName}>
						{tableColumnDataLimit(row.original.FullName)}
					</div>
				)
			},
		},
		{
			Header: () => <div style={{ textAlign: "left" }}>Team</div>,
			accessor: "TeamName",
			resizable: false,
			style: { overflowWrap: "break-word" },

			Cell: (row) => {
				return (
					<div className='tabledata wordwrap' title={row.original.TeamName}>
						{tableColumnDataLimit(row.original.TeamName)}
					</div>
				)
			},
		},
		{
			Header: () => <div style={{ textAlign: "left" }}>Game</div>,
			accessor: "GameName",
			resizable: false,
			style: { overflowWrap: "break-word" },
			Cell: (row) => {
				return (
					<div className='tabledata wordwrap' titile={row.original.GameName}>
						{tableColumnDataLimit(row.original.GameName)}
					</div>
				)
			},
		},
		{
			Header: () => <div style={{ textAlign: "left" }}>Category</div>,
			accessor: "Category",
			resizable: false,
			style: { overflowWrap: "break-word" },

			Cell: (row) => {
				return (
					<div className='tabledata -wordwrap' title={row.original.Category}>
						{tableColumnDataLimit(row.original.Category)}
					</div>
				)
			},
		},
		{
			Header: () => <div style={{ textAlign: "left" }}>Status</div>,
			accessor: "GamesInviteStatus",
			resizable: false,
			style: { overflowWrap: "break-word" },
			Cell: (row) => {
				return (
					<>
						<div
							//style={{textAlign:"center"}}
							className='tabledata wordwrap '
							title={row.original.GamesInviteStatus}
						>
							<div
								className={
									row.original.GamesInviteStatus === "Pending"
										? "tabledata wordwrap invite-game-pending-status"
										: row.original.GamesInviteStatus === "Accepted"
										? "tabledata wordwrap invite-game-accepted-status"
										: row.original.GamesInviteStatus === "Rejected"
										? "tabledata wordwrap invite-game-declined-status"
										: "tabledata wordwrap"
								}
							>
								{tableColumnDataLimit(row.original.GamesInviteStatus)}
							</div>
						</div>
						
					</>
				)
			},
		},
		{
			Header: () => (
				<div style={{ textAlign: "center" }}>
					{"     "}Action
				</div>
			),
			accessor: "Action",
			className: "tableheader wordwrap",
			sortable: false,
			resizable: false,
			width: Math.round(window.innerWidth * 0.06),
			filterable: false,
			show: showAction,
			Cell: (row) => {
				return (
					<div className='tabledata text-left pl-4 ml-3'>
						{props.user.InvitedUserID === row.original.CreatedByID ? (
							<FontAwesomeIcon
								title='Remainder'
								className=''
								size={200}
								icon={faBell}
								style={{ cursor: "pointer" }}
								onClick={() => remainderFunc(row.original)}
							/>
						) : null}
					</div>
				)
			},
		},
	]

	//columns = permissionGroup.includes(Permissions.TRACK_HOST_COUNTRY_REMAINDER)
	//  ? columns
	//  : columns.filter((x) => !x.hidden);
	//
	return (
		<>
			<div>
				{/*<Greeting name="muthu"/>*/}
				<div className='h-full singletab'>
					<div className='games_layout form-group mt-3 ml-3'>Track Invites</div>
				</div>
				<div className='row col-sm-12 mr-0 ml-0'>
					<GameTrackInviteStatusFilter
						ref={childRef}
						paginationProp={paginationProp}
						parentCallBackFunc={childCallBackFunc}
						isClickedExport={isClickedExport}
					/>
				</div>
				<div style={{ marginLeft: "15px", marginRight: "15px" }}>
				<div className="admin-report-table pt-0">
					<ReactTable
						className={"CosmicTable"}
						data={mainListData}
						columns={columns}
						showPaginationTop={false}
						showPaginationBottom={false}
						minRows={0}
						onFetchData={sortingColumnFunc}
						defaultPageSize={10}
						defaultPage={1}
						ref={table}
						sortable={true}
						multiSort={true}
						manual={true}
						showPageSizeOptions={false}
						NoDataComponent={NoDataProp}
						defaultSorting={[
							{
								id: "id",
								desc: true,
							},
						]}
						manualPagination={true}
					/>
					</div>
				</div>
				
				<div >
					<div style={{marginLeft:"15px"}}>
						<label>
							<b>Total Records: </b>
							{paginationProp.totalRecords > 10
								? paginationProp.totalCount + " of " + paginationProp.totalRecords
								: paginationProp.totalRecords !== 0
								? paginationProp.totalRecords
								: 0}
						</label>
					</div>
					
				</div>
				<div className='row col-sm-12 mt-2 ml-0 mr-1 export_excel_in_invite_status'>
					<div className='col-sm-6 pl-0'>
						<Button
							disabled={canExport}
							className='btn_green'
							title='Export Excel'
							onClick={exportExcelFunc}
						>
							<FontAwesomeIcon icon={faDownload} /> Export Excel
						</Button>
					</div>
					
					{paginationProp.totalPages > 0 ? (
						<div className='col-sm-6 d-flex justify-content-end mt-0 pr-0'>
							<Pagination
								totalPages={paginationProp.totalPages}
								totalRecords={paginationProp.totalRecords}
								paginationCall={pageRouterFunc}
							></Pagination>
						</div>
					) : null}
					</div>
			</div>
			{exportExcelProps.isExportExcel ? (
				<ExportData
					data={exportExcelProps.exportData}
					label={exportLabel}
					filename='Game Track InviteStatus'
				/>
			) : null}
		</>
	)
})

const mapPropsToState = (state) => ({
	user: state.userReducer,
})

export default PermissionProvider(connect(mapPropsToState, null)(GameTrackInviteStatusList))
