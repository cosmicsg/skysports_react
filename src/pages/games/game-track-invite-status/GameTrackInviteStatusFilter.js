import { Select } from "antd";
import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Button, Card, Collapse } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown, faArrowUp } from "@fortawesome/free-solid-svg-icons";

//get Option from select tag
const { Option } = Select;

//Filter properties
const GameTrackInviteStatusFilter = React.memo(
  forwardRef((props, ref) => {
    const [filterProps, setFilterProps] = useState({
      user: [],
      team: [],
      game: [],
      sport: [],
      department: [],
      status: [],
      category: [],
    });

    //Filter Dropdown data properties
    const [filterDataProps, setFilterDataProps] = useState({
      usersData: [],
      teamsData: [],
      gamesData: [],
      sportsData: [],
      departmentsData: [],
      statusData: [],
    });

    //Search button enable or disable properties
    const [isDisableSearch, setDisableSearch] = useState(true);

    //Filter Expand properties
    const [filterExpandProp, setFilterExpandProp] = useState({
      isExpanded: false,
      icon: false,
    });

    const [isClear, setClear] = useState(false);
    //onChangeFunc event
    const onChangeFunc = (name, val) => {
      setFilterProps({
        ...filterProps,
        [name]: val,
      });
      enableSearchBtn(name, val);
    };

    //Empty check
    const emptyCheckFunc = (val) => {
      let flag = false;
      if (val === undefined || val === null || val === "" || val.length === 0) {
        flag = true;
      }
      return flag;
    };

    //Validate empty check for filter properties
    const enableSearchBtn = (name, val) => {
      let isEnableBtn = false;
      if (name === "user") {
        if (!emptyCheckFunc(val)) isEnableBtn = true;
      } else if (filterProps.user.length > 0) isEnableBtn = true;

      if (name === "game") {
        if (!emptyCheckFunc(val)) isEnableBtn = true;
      } else if (filterProps.game.length > 0) isEnableBtn = true;

      if (name === "team") {
        if (!emptyCheckFunc(val)) isEnableBtn = true;
      } else if (filterProps.team.length > 0) isEnableBtn = true;

      if (name === "sport") {
        if (!emptyCheckFunc(val)) isEnableBtn = true;
      } else if (filterProps.sport.length > 0) isEnableBtn = true;

      if (name === "department") {
        if (!emptyCheckFunc(val)) isEnableBtn = true;
      } else if (filterProps.department.length > 0) isEnableBtn = true;

      if (name === "status") {
        if (!emptyCheckFunc(val)) isEnableBtn = true;
      } else if (filterProps.status.length > 0) isEnableBtn = true;

      if (name === "category") {
        if (!emptyCheckFunc(val)) isEnableBtn = true;
      } else if (!emptyCheckFunc(filterProps.category)) isEnableBtn = true;

      setDisableSearch(isEnableBtn === true ? false : true);
    };

    //Lifecycle method to handle when page getting load
    useEffect(() => {
      //Load users dropdown value
      APIRequest.getGetService(APIUrl.GET_USERNAME_DROPDOWN_LIST).then(
        (res) => {
          setFilterDataProps((prevState) => ({
            ...prevState,
            usersData: res.Data,
          }));
        }
      );
      //Load Games Team dropdown value
      APIRequest.getGetService(APIUrl.GET_GAMES_TEAM).then((res) => {
        setFilterDataProps((prevState) => ({
          ...prevState,
          teamsData: res.Data,
        }));
      });
      //Load games dropdown value
      APIRequest.getGetService(APIUrl.GET_GAMES_LIST).then((res) => {
        if (res.Succeeded) {
          setFilterDataProps((prevState) => ({
            ...prevState,
            gamesData: res.Data,
          }));
        }
      });
      //load department dropdown value
      APIRequest.getGetService(APIUrl.DEPARTMENT_LIST).then((res) => {
        if (res.Succeeded) {
          setFilterDataProps((prevState) => ({
            ...prevState,
            departmentsData: res.Data,
          }));
        }
      });
      //Load Sports dropdown value
      APIRequest.getGetService(APIUrl.GET_SPORT_DROPDOWN_LIST).then((res) => {
        if (res.Succeeded) {
          setFilterDataProps((prevState) => ({
            ...prevState,
            sportsData: res.Data,
          }));
        }
      });

      //Load Status dropdown value
      APIRequest.getGetService(APIUrl.GET_TEAM_INVITED_USER_STATUS).then(
        (res) => {
          if (res.Succeeded) {
            setFilterDataProps((prevState) => ({
              ...prevState,
              statusData: res.Data,
            }));
          }
        }
      );
    }, []);

    //Filter Expand handler
    const filterExpandHandler = (flag) => {
      let temp = filterExpandProp.isExpanded === true ? false : true;
      setFilterExpandProp({
        isExpanded: temp,
        icon: flag,
      });
    };

    useImperativeHandle(ref, () => ({
      /* This method will be getting invoked by parent component.
    here is the parent component would be 'GameTrackInviteStatusList' component
    */
      refreshListData() {
        submitFilterFunc();
      },
    }));

    useEffect(() => {
      /* whenever the clear function called then the below code will gets executed only when isClear is true
    for refreshing the mainList data */
      if (isClear === true) submitFilterFunc();
    }, [isClear]);

    // Search Filter
    const submitFilterFunc = () => {
      const { user, team, game, sport, department, status, category } =
        filterProps;
      const { pageNo, pageSize, sortColumn, sortOrder } = props.paginationProp;
      let isExportExcel = props.isClickedExport;

      let tempPageSize = pageSize && pageSize > 0 ? pageSize : 10;
      let tempPageNo = pageNo && pageNo > 0 ? pageNo : 1;
      if (isExportExcel) {
        tempPageSize = props.paginationProp.totalRecords;
        tempPageNo = 1;
      }

      let params = {
        UserIDs: user.length > 0 ? user.join(",") : "",
        TeamIDs: team.length > 0 ? team.join(",") : "",
        GameIDs: game.length > 0 ? game.join(",") : "",
        SportIDs: sport.length > 0 ? sport.join(",") : "",
        DepartmentIDs: department.length > 0 ? department.join(",") : "",
        StatusIDs: status.length > 0 ? status.join(",") : "",
        Category: category.length > 0 ? category.join(",") : "",
        PageNo: tempPageNo,
        PageSize: tempPageSize,
        SortColumn: sortColumn ? sortColumn : "",
        SortOrder: sortOrder ? sortOrder : "",
      };

      APIRequest.getPostService(APIUrl.GET_GAMES_TRACK_INVITE_STATUS, params)
        .then((res) => {
          if (res.Succeeded) {
            //Update MainList Data from GameTrackInviteStatusList component
            props.parentCallBackFunc(res.Data);
            setClear(false);
          }
        })
        .catch((err) => {});
    };

    //Clear Filter
    const clearFilterFunc = () => {
      setFilterProps({
        department: [],
        game: [],
        sport: [],
        status: [],
        team: [],
        user: [],
        category: [],
      });
      setClear(true);
      setDisableSearch(true);
    };

    //return render props
    return (
      <>
        <Card className="w-100 form-group" style={{marginTop:"13px"}}>
          <Card.Header
            style={{ cursor: "pointer" }}
            className="text-white"
            onClick={() => filterExpandHandler(!filterExpandProp.isExpanded)}
          >
            <div className="float-left filter-heading-font" style={{marginLeft:"-12px"}}>Search Track Invites</div>
            <div className="float-right">
              {filterExpandProp.isExpanded ? (
                <FontAwesomeIcon
                  color="#FFFFFF"
                  className="action-icon-font"
                  icon={faArrowUp}
                  style={{ cursor: "pointer" }}
                />
              ) : (
                <FontAwesomeIcon
                  color="#FFFFFF"
                  className="action-icon-font"
                  icon={faArrowDown}
                  style={{ cursor: "pointer" }}
                />
              )}
            </div>
          </Card.Header>
          <Collapse in={filterExpandProp.isExpanded}>
            <Card.Body>
              <div
                className="row col-sm-12 form-group"
                style={{ marginTop: "1rem" }}
              >
                <div className="col-sm-4">
                  <label>Username</label>
                  <Select
                    className="textfiled w-80"
                    name="user"
                    size={200}
                    showSearch
                    mode="multiple"
                    // maxTagCount={1}
                    // maxTagTextLength={15}
                    maxTagCount="responsive"
                    allowClear
                    showArrow
                    bordered={false}
                    placeholder="Select Username"
                    value={filterProps.user}
                    onChange={(val) => onChangeFunc("user", val)}
                    optionFilterProp="children"
                    filterSort={(optionA, optionB) => {
                      return optionA.children
                        .toLowerCase()
                        .localeCompare(optionB.children.toLowerCase());
                    }}
                  >
                    {filterDataProps.usersData.map((item, index) => (
                      <Option key={index} value={item.OptionID}>
                        {item.OptionData}
                      </Option>
                    ))}
                  </Select>
                </div>
                <div className="col-sm-4">
                  <label>Team</label>
                  <Select
                    className="textfiled w-80"
                    name="team"
                    size={200}
                    showSearch
                    allowClear
                    showArrow
                    mode="multiple"
                    // maxTagCount={1}
                    // maxTagTextLength={15}
                    maxTagCount="responsive"
                    bordered={false}
                    placeholder="Select Team"
                    value={filterProps.team}
                    onChange={(val) => onChangeFunc("team", val)}
                    optionFilterProp="children"
                    filterSort={(optionA, optionB) => {
                      return optionA.children
                        .toLowerCase()
                        .localeCompare(optionB.children.toLowerCase());
                    }}
                  >
                    {filterDataProps.teamsData.map((item, index) => (
                      <Option key={index} value={item.OptionID}>
                        {item.OptionData}
                      </Option>
                    ))}
                  </Select>
                </div>
                <div className="col-sm-4">
                  <label>Game</label>
                  <Select
                    className="textfiled w-80"
                    name="game"
                    allowClear
                    showSearch
                    showArrow
                    mode="multiple"
                    // maxTagCount={1}
                    // maxTagTextLength={15}
                    maxTagCount="responsive"
                    bordered={false}
                    placeholder="Select Game"
                    value={filterProps.game}
                    onChange={(val) => onChangeFunc("game", val)}
                    optionFilterProp="children"
                    filterSort={(optionA, optionB) =>
                      optionA.children
                        .toLowerCase()
                        .localeCompare(optionB.children.toLowerCase())
                    }
                  >
                    {filterDataProps.gamesData.map((item, index) => (
                      <Option key={index} value={item.OptionID}>
                        {item.OptionData}
                      </Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="row col-sm-12 form-group">

                <div className="col-sm-4">
                  <label>Sport</label>
                  <Select
                    className="textfiled w-80"
                    name="sport"
                    allowClear
                    showSearch
                    showArrow
                    mode="multiple"
                    // maxTagCount={1}
                    // maxTagTextLength={15}
                    maxTagCount="responsive"
                    bordered={false}
                    placeholder="Select Sport"
                    value={filterProps.sport}
                    onChange={(val) => onChangeFunc("sport", val)}
                    optionFilterProp="children"
                    filterSort={(optionA, optionB) =>
                      optionA.children
                        .toLowerCase()
                        .localeCompare(optionB.children.toLowerCase())
                    }
                  >
                    {filterDataProps.sportsData.map((item, index) => (
                      <Option key={index} value={item.OptionID} >
                        {item.OptionData}
                      </Option>
                    ))}
                  </Select>
                </div>
                <div className="col-sm-4">
                  <label>Department</label>
                  <Select
                    className="textfiled w-80"
                    name="department"
                    allowClear
                    showSearch
                    showArrow
                    mode="multiple"
                    // maxTagCount={1}
                    // maxTagTextLength={15}
                    maxTagCount="responsive"
                    bordered={false}
                    placeholder="Select Department"
                    value={filterProps.department}
                    onChange={(val) => onChangeFunc("department", val)}
                    optionFilterProp="children"
                    filterSort={(optionA, optionB) =>
                      optionA.children
                        .toLowerCase()
                        .localeCompare(optionB.children.toLowerCase())
                    }
                  >
                    {filterDataProps.departmentsData.map((item, index) => (
                      <Option key={index} value={item.OptionID}>
                        {item.OptionData}
                      </Option>
                    ))}
                  </Select>
                </div>
                <div className="col-sm-4">
                  <label>Invite Status</label>
                  <Select
                    className="textfiled w-80"
                    name="status"
                    showSearch
                    allowClear
                    showArrow
                    mode="multiple"
                    // maxTagCount={1}
                    // maxTagTextLength={15}
                    maxTagCount="responsive"
                    bordered={false}
                    placeholder="Select Status"
                    value={filterProps.status}
                    onChange={(val) => onChangeFunc("status", val)}
                    optionFilterProp="children"
                    filterSort={(optionA, optionB) =>
                      optionA.children
                        .toLowerCase()
                        .localeCompare(optionB.children.toLowerCase())
                    }
                  >
                    {filterDataProps.statusData.map((item, index) => (
                      <Option key={index} value={item.OptionID}>
                        {item.OptionData}
                      </Option>
                    ))}
                  </Select>
                </div>
              </div>
              <div className="row col-sm-12 form-group">
                
                <div className="col-sm-4">
                  <label>Category</label>
                  <Select
                    className="textfiled w-80"
                    name="category"
                    showSearch
                    allowClear
                    showArrow
                    mode="multiple"
                    // maxTagCount={1}
                    // maxTagTextLength={15}
                    maxTagCount="responsive"
                    bordered={false}
                    placeholder="Select Category"
                    value={filterProps.category}
                    onChange={(val) => onChangeFunc("category", val)}
                    optionFilterProp="children"
                    filterSort={(optionA, optionB) =>
                      optionA.children
                        .toLowerCase()
                        .localeCompare(optionB.children.toLowerCase())
                    }
                  >
                    <Option key={1} value={"department"}>
                      {"Department"}
                    </Option>
                    <Option key={2} value={"sport"}>
                      {"Sport"}
                    </Option>
                  </Select>
                </div>
              </div>
              <div className="row col-sm-12">
                <div className="col-sm-8"></div>
                <div className="col-sm-4 d-flex justify-content-end">
                <Button className="btn_green " onClick={clearFilterFunc}>
                    Clear
                  </Button>
                  <Button
                    className="btn_green ml-2"
                    onClick={submitFilterFunc}
                    disabled={isDisableSearch}
                  >
                    Search
                  </Button>
                  
                </div>
              </div>
            </Card.Body>
          </Collapse>
        </Card>
      </>
    );
  })
);

export default GameTrackInviteStatusFilter;
