import React, { memo, useState, useEffect } from "react";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { Card, Collapse, Button } from "react-bootstrap";
import { Select } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown, faArrowUp } from "@fortawesome/free-solid-svg-icons";

//Get Option from Select tag
const { Option } = Select;

//Host Country Activity Filter component
const HostCountryActivityFilter = (props) => {
  //Filter expand properties
  const [filterExpanded, setFilterExpanded] = useState({
    IsExpanded: false,
    Icons: false,
  });

  //filter properties
  const [filterProps, setFilterProps] = useState({
    games: [],
    sport: [],
    department: [],
    team: [],
    disableSearch: true,
  });

  //filter dropdown list data fields
  const [filterDataProps, setFilterDataProps] = useState({
    gamesData: [],
    departmentData: [],
    sportsData: [],
    teamsData: [],
  });

  //expand Filter handler
  const expandFilterHandler = (data) => {
    let Icons = filterExpanded.Icons === true ? false : true;
    setFilterExpanded({
      IsExpanded: data,
      Icons: Icons,
    });
  };

  //useEffect, it is lifecycle method, it will gets invoked whenever the page gets refresh.
  useEffect(() => {
    //Getting games dropdown value
    APIRequest.getGetService(APIUrl.GET_GAMES_DROPDOWN_LIST).then((res) => {
      if (res.Succeeded) {
        setFilterDataProps((prevState) => ({
          ...prevState,
          gamesData: res.Data,
        }));
      }
    });
    //Getting Sports dropdown value
    APIRequest.getGetService(APIUrl.GET_SPORT_DROPDOWN_LIST).then((res) => {
      
      if (res.Succeeded) {
        setFilterDataProps((prevState) => ({
          ...prevState,
          sportsData: res.Data,
        }));
      }
    });
    //Getting department dropdown value
    APIRequest.getGetService(APIUrl.DEPARTMENT_LIST).then((res) => {
      if (res.Succeeded) {
        setFilterDataProps((prevState) => ({
          ...prevState,
          departmentData: res.Data,
        }));
      }
    });
    //Getting Games Team Dropdown value
    APIRequest.getGetService(APIUrl.GET_GAMES_TEAM).then((res) => {
      
      if (res.Succeeded) {
        setFilterDataProps((prevState) => ({
          ...prevState,
          teamsData: res.Data,
        }));
      }
    });
  }, []);

  //Onchange method
  const onChange = (name, val) => {
    
    setFilterProps((prevState) => ({
      ...prevState,
      [name]: val,
    }));

    //handle the enable search button handler
    enableSearchBtnHandler(name, val);
  };

  //Check here is empty fields
  const emptyCheck = (val) => {
    let temp = true;
    if (val !== "" && val !== undefined && val !== null && val.length !== 0) {
      temp = false;
    }
    return temp;
  };

  //handle the enable search button handler
  const enableSearchBtnHandler = (name, val) => {
    const { department, team, sport, games } = filterProps;
    let flag = true;
    if (name === "games") {
      if (!emptyCheck(val)) flag = false;
    } else if (games.length !== 0) flag = false;
    if (name === "team") {
      if (!emptyCheck(val)) flag = false;
    } else if (team.length !== 0) flag = false;
    if (name === "sport") {
      if (!emptyCheck(val)) flag = false;
    } else if (sport.length !== 0) flag = false;
    if (name === "department") {
      if (!emptyCheck(val)) flag = false;
    } else if (department.length !== 0) flag = false;

    setFilterProps((prevState) => ({
      ...prevState,
      disableSearch: flag,
    }));
  };

  /* Submit Filter */
  const submitFilter = () => {
    
    //calling the refresh list function with passing the isSubmitFilter flag as true
    refreshList(true);
  };

  const refreshList = (isSubmitFilter) => {
    /* Here is calling the parentFilterHandle function from HostActivityList component
    with passed the filter properties to refresh list */
    props.parentFilterHandler(filterProps, isSubmitFilter);
  };

  //Clear filter
  const clearFilter = () => {
    setFilterProps({
      department: [],
      disableSearch: true,
      games: [],
      sport: [],
      team: [],
    });

    //calling the refresh list function with passing the isSubmitFilter flag as false
    refreshList(false);
  };
  return (
    <>
      <Card className="w-100 card form-group">
        <Card.Header
          style={{ cursor: "pointer" }}
          className="text-white"
          onClick={() => expandFilterHandler(!filterExpanded.IsExpanded)}
        >
          <div className="float-left filter-heading-font" style={{marginLeft:"-12px"}}>Search Host Country Activities</div>
          <div className="float-right">
            {filterExpanded.Icons ? (
              <FontAwesomeIcon
                color="#FFFFFF"
                className="action-icon-font"
                icon={faArrowUp}
                style={{ cursor: "pointer" }}
              ></FontAwesomeIcon>
            ) : (
              <FontAwesomeIcon
                color="#FFFFFF"
                className="action-icon-font"
                icon={faArrowDown}
                style={{ cursor: "pointer" }}
              ></FontAwesomeIcon>
            )}
          </div>
        </Card.Header>
        <Collapse in={filterExpanded.IsExpanded}>
          <Card.Body>
            <div className="row col-sm-14">
              <div className="col-sm-4 form-group">
                <label>Game Name</label>
                <Select
                  className="textfiled w-80"
                  bordered={false}
                  mode={"multiple"}
                  showSearch
                  allowClear
                  // maxTagCount={1}
                  maxTagCount="responsive"
                  placeholder="Select Game"
                  name="games"
                  size={200}
                  value={filterProps.games}
                  onChange={(val) => onChange("games", val)}
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {filterDataProps.gamesData.map((item, index) => {
                    return (
                      <Option key={index} value={item.OptionID}>
                        {item.OptionData}
                      </Option>
                    );
                  })}
                </Select>
              </div>
              {/* <div className="col-sm-1"></div> */}
              <div className="form-group col-sm-4">
                <label>Sport</label>
                <Select
                  className="textfiled w-80"
                  name="sport"
                  mode={"multiple"}
                  showSearch
                  // maxTagCount={1}
                  // maxTagTextLength={15}
                  maxTagCount="responsive"
                  allowClear
                  size={200}
                  bordered={false}
                  value={filterProps.sport}
                  onChange={(val) => onChange("sport", val)}
                  placeholder="Select Sport"
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {filterDataProps.sportsData.map((item, index) => {
                    return (
                      <Option key={index} value={item.OptionID}>
                        {item.OptionData}
                      </Option>
                    );
                  })}
                </Select>
              </div>
              <div className="form-group col-sm-4">
                <label>Department</label>
                <Select
                  className="textfiled w-80"
                  name="department"
                  mode={"multiple"}
                  showSearch
                  allowClear
                  // maxTagCount={1}
                  // maxTagTextLength={15}
                  maxTagCount="responsive"
                  bordered={false}
                  size={200}
                  value={filterProps.department}
                  onChange={(val) => onChange("department", val)}
                  placeholder="Select Department"
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {filterDataProps.departmentData.map((item, index) => {
                    return (
                      <Option key={index} value={item.OptionID}>
                        {item.OptionData}
                      </Option>
                    );
                  })}
                </Select>
              </div>
            </div>
            <div className="row col-sm-14">
              
              {/* <div className="col-sm-1"></div> */}
              <div className="form-group col-sm-4">
                <label>Team</label>
                <Select
                  className="textfiled w-80"
                  name="team"
                  mode={"multiple"}
                  showSearch
                  allowClear
                  // maxTagCount={1}
                  // maxTagTextLength={15}
                  maxTagCount="responsive"
                  bordered={false}
                  size={200}
                  value={filterProps.team}
                  onChange={(val) => onChange("team", val)}
                  placeholder="Select Team"
                  optionFilterProp="children"
                  filterSort={(optionA, optionB) =>
                    optionA.children
                      .toLowerCase()
                      .localeCompare(optionB.children.toLowerCase())
                  }
                >
                  {filterDataProps.teamsData.map((item, index) => {
                    return (
                      <Option key={index} value={item.OptionID}>
                        {item.OptionData}
                      </Option>
                    );
                  })}
                </Select>
              </div>
            </div>
            <div className="col-sm-12">
              {/* <div className="col-sm-6"></div> */}
              <div className=" d-flex justify-content-end row">
              <Button className="btn_green mr-2" onClick={clearFilter}>
                  Clear
                </Button>
                <Button
                  className="btn_green"
                  onClick={submitFilter}
                  disabled={filterProps.disableSearch}
                >
                  Search
                </Button>
               
              </div>
            </div>
          </Card.Body>
        </Collapse>
      </Card>
    </>
  );
};
export default memo(HostCountryActivityFilter);
