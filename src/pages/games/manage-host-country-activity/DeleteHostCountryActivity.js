import React, { memo } from "react";
import { Button, Modal } from "react-bootstrap";
import {
  APIRequest,
  showNotification,
} from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { Message } from "../../../message/messageConstant";

// Delete host country activity component
const DeleteHostCountryActivity = (props) => {
  const closePopup = () => {
    props.closePopup();
  };

  //delete
  const deleteTeam = () => {
    const { GamesParticipantID } = props.row ? props.row : 0;
    if (GamesParticipantID > 0) {
      let temp = Array.isArray(GamesParticipantID)
        ? GamesParticipantID
        : [GamesParticipantID];
      temp = temp.join(",");
      let url = APIUrl.DELETE_HOST_COUNTRY_ACTIVITY + "/" + temp;
      APIRequest.getGetService(url)
        .then((res) => {
          props.closePopup();
          let response = res.Data;
          if (response.IsRefInHostActivity) {
            showNotification(
              "Cannot delete Host Country Activity references exist",
              "error"
            );
          } else if (response.IsError) {
            showNotification(
              "Error occurred when deleting Host Country Activity",
              "error"
            );
          } else {
            showNotification(
              "Host Country Activity deleted successfully.",
              "success"
            );
            props.refreshList();
          }
        })
        .catch((err) => {});
    }
  };

  return (
    <>
      <Modal size="sm" backdrop="static" show={true}>
        <div className="popup-content">
          <div className="deletecircle"></div>
          <i className="fa fa-trash-o deleteicon"></i>
          <Modal.Body>
            <p className="pull-left" style={{ margin: "4rem 2rem", textAlign:"left" }}>
              {" "}
              <h5>{Message.HOST_ACTIVITY_DELETE_HEADER}</h5>
              <h6 style={{ color: "darkgray" }}>
                {Message.HOST_ACTIVITY_DELETE_CONFIRM}
              </h6>
              <h6
                style={{
                  color: "darkgray",
                  marginRight: "6rem",
                  marginBottom: "-3rem",
                }}
              >
                {Message.INVITE_DELETE_CONFIRM1}
              </h6>
            </p>
            <div className="pull-right mr-2">
            <Button className="btn_cancel " onClick={closePopup}>
                No
              </Button>
              <Button
                className="btn_green "
                style={{ height: "2rem", padding: "5px 10px" }}
                onClick={deleteTeam}
              >
                Yes
              </Button>
              
            </div>
          </Modal.Body>
        </div>
      </Modal>
    </>
  );
};

export default memo(DeleteHostCountryActivity);
