import React, { memo, useState, useEffect } from "react";
import { Modal } from "react-bootstrap";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import moment from "moment";

const ViewHostCountryActivity = (props) => {
  const [viewProps, setViewProps] = useState({});
  const closeModel = () => {
    props.updateOpenViewState();
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    let row = props.selectedRow;
    let url =
      APIUrl.GET_SINGLE_HOST_COUNTRY_ACTIVITY + "/" + row.GamesParticipantID;
    APIRequest.getGetService(url)
      .then((res) => {
        if (res.Data) {
          let row = res.Data;

          setViewProps({
            GameName: row.GameName,
            SportName: row.SportName,
            DepartmentName: row.DepartmentName,
            Category: row.Category,
            TeamName: row.TeamName,
            SportRoundName: row.SportRoundName,
            SportFacilityName: row.SportFacilityName,
            SportFacilityDate:
              moment(row.SportFacilityDate).year() === 1900
                ? ""
                : moment(row.SportFacilityDate).format("DD MMM YYYY"),
            SportSlotName: row.SportSlotName,
            UserName: row.UserName,
          });
        }
      })
      .catch(() => {});
  }, []);
  return (
    <>
      <Modal show={true} backdrop="static" size="md">
        <Modal.Header style={{ height: "4rem" }}>
          <p>Host Country Activity</p>
          <div className="col-sm-1">
            <div className="d-flex justify-content-end">
              <button
                title="Close"
                type="button"
                className="close"
                onClick={closeModel}
              >
                <span style={{ color: "#c29a37" }} area-hidden={true}>
                  &times;
                </span>
              </button>
            </div>
          </div>
        </Modal.Header>
        <Modal.Body>
          <div className="row col-sm-12 form-group">
            <div className="col-sm-4">
              <label className="   font-weight-bold popup_txt">
                Game Name{" "}
              </label>
            </div>
            <div className="col-sm-1">
              {/*<span className="font-weight-bold popup_txt">:</span>*/}
            </div>
            <div className="col-sm-7">
              <label>{viewProps.GameName}</label>
            </div>
          </div>
          <div className="row col-sm-12 form-group">
            <div className="col-sm-4">
              <label className="   font-weight-bold popup_txt">Category</label>
            </div>
            <div className="col-sm-1">
              {/*<span className="font-weight-bold popup_txt">:</span>*/}
            </div>
            <div className="col-sm-7">
              <label>{viewProps.Category}</label>
            </div>
          </div>
          {viewProps.Category === "Department" ? (
            <div className="row col-sm-12 form-group">
              <div className="col-sm-4">
                <label className="   font-weight-bold popup_txt">
                  Department
                </label>
              </div>
              <div className="col-sm-1">
                {/*<span className="font-weight-bold popup_txt">:</span>*/}
              </div>
              <div className="col-sm-7">
                <label>{viewProps.DepartmentName}</label>
              </div>
            </div>
          ) : (
            <>
              <div className="row col-sm-12 form-group">
                <div className="col-sm-4">
                  <label className="   font-weight-bold popup_txt">Sport</label>
                </div>
                <div className="col-sm-1">
                  {/*<span className="font-weight-bold popup_txt">:</span>*/}
                </div>
                <div className="col-sm-7">
                  <label>{viewProps.SportName}</label>
                </div>
              </div>
              <div className="row col-sm-12 form-group">
                <div className="col-sm-4">
                  <label className="   font-weight-bold popup_txt">Round</label>
                </div>
                <div className="col-sm-1">
                  {/*<span className="font-weight-bold popup_txt">:</span>*/}
                </div>
                <div className="col-sm-7">
                  <label>{viewProps.SportRoundName}</label>
                </div>
              </div>
              <div className="row col-sm-12 form-group">
                <div className="col-sm-4">
                  <label className="   font-weight-bold popup_txt">
                    Facility
                  </label>
                </div>
                <div className="col-sm-1">
                  {/*<span className="font-weight-bold popup_txt">:</span>*/}
                </div>
                <div className="col-sm-7">
                  <label>{viewProps.SportFacilityName}</label>
                </div>
              </div>
              <div className="row col-sm-12 form-group">
                <div className="col-sm-4">
                  <label className="   font-weight-bold popup_txt">
                    Facility Date
                  </label>
                </div>
                <div className="col-sm-1">
                  {/*<span className="font-weight-bold popup_txt">:</span>*/}
                </div>
                <div className="col-sm-7">
                  <label>{viewProps.SportFacilityDate}</label>
                </div>
              </div>
              <div className="row col-sm-12 form-group">
                <div className="col-sm-4">
                  <label className="   font-weight-bold popup_txt">Slot</label>
                </div>
                <div className="col-sm-1">
                  {/*<span className="font-weight-bold popup_txt">:</span>*/}
                </div>
                <div className="col-sm-7">
                  <label>{viewProps.SportSlotName}</label>
                </div>
              </div>
              <div className="row col-sm-12 form-group">
                <div className="col-sm-4">
                  <label className="   font-weight-bold popup_txt">
                    Participant Type
                  </label>
                </div>
                <div className="col-sm-1">
                  {/*<span className="font-weight-bold popup_txt">:</span>*/}
                </div>
                <div className="col-sm-7">
                  <label>{viewProps.TeamName ? "Team" : "Individual"}</label>
                </div>
              </div>
            </>
          )}
          {viewProps.TeamName ? (
            <div className="row col-sm-12 form-group">
              <div className="col-sm-4">
                <label className="   font-weight-bold popup_txt">Team</label>
              </div>
              <div className="col-sm-1">
                {/*<span className="font-weight-bold popup_txt">:</span>*/}
              </div>
              <div className="col-sm-7">
                <label>{viewProps.TeamName}</label>
              </div>
            </div>
          ) : null}
          <div className="row col-sm-12 form-group">
            <div className="col-sm-4">
              <label className="   font-weight-bold popup_txt">Users</label>
            </div>
            <div className="col-sm-1">
              {/*<span className="font-weight-bold popup_txt">:</span>*/}
            </div>
            <div className="col-sm-7">
              <label>{viewProps.UserName}</label>
            </div>
          </div>
          {/*<div className="row col-sm-12 d-flex justify-content-end">
            <Button 
              className="btn_green"
              onClick={closeModel}
            >
              Close
            </Button>
          </div>*/}
        </Modal.Body>
      </Modal>
    </>
  );
};
export default memo(ViewHostCountryActivity);
