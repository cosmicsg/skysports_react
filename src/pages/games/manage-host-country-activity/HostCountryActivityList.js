import React, { useState, useRef, forwardRef, useImperativeHandle, useEffect } from "react"
import { Button } from "react-bootstrap"
import ReactTable from "react-table"
import ExportData from "../../../utils/ExportData"
import {
	faDownload,
	faEdit,
	faEye,
	faTrashAlt,
	faUserPlus,
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Pagination from "../../../utils/Pagination"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import AddUsers from "./AddUser"
import DeleteHostCountryActivity from "./DeleteHostCountryActivity"
import AddHostCountryActivity from "./AddHostCountryActivity"
import HostCountryActivityFilter from "./HostCountryActivityFilter"
import ViewHostCountryActivity from "./ViewHostCountryActivity"
import moment from "moment"
import { Permissions } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"
import { connect } from "react-redux"

//Export label
const exportLabel = [
	{ columnName: "S.No", value: "SerialNo" },
	{ columnName: "Game Name", value: "GameName" },
	{ columnName: "Sport Name", value: "SportName" },
	{ columnName: "Category", value: "Category" },
	{ columnName: "Department Name", value: "DepartmentName" },
	{ columnName: "Team Name", value: "TeamName" },
	{ columnName: "Round", value: "SportRoundName" },
	{ columnName: "Facility", value: "SportFacilityName" },
	{ columnName: "Facility Date", value: "SportFacilityDate" },
	{ columnName: "Slot", value: "SportSlotName" },
]

//string concat with(...) when the string has more that 30 characters
const tableColumnDataLimit = (str) => {
	if (str !== null && str !== undefined) return str.length > 30 ? str.substring(0, 27) + "..." : str
	else return ""
}

//Host Country Activity List component
const HostCountryActivityList = React.memo(
	forwardRef((props, ref) => {
		const [permissionGroup, setPermissionGroup] = useState([])
		//table ref
		const table = useRef(null)

		//Add Host Country Activity component ref
		const addHostRef = useRef(null)

		//filter parameters
		const [filterProps, setFilterProps] = useState({
			games: [],
			sport: [],
			department: [],
			team: [],
		})

		//Export fields properties
		const [exportExcelProps, setExportExcelProps] = useState({
			isExportExcel: false,
			exportData: [],
		})
		//pagination fields properties
		const [paginationProps, setPaginationProps] = useState({
			totalPages: 0,
			totalRecords: 0,
			pageSize: 10,
			totalCount: 0,
			sortColumn: "",
			sortValue: "",
		})
		//Selected row properties
		const [selectedRow, setSelectedRow] = useState({
			sportID: "",
			gamesTeamID: "",
			gamesSetupID: "",
			gamesParticipantID: 0,
		})
		const [canExport, setCanExport] = useState(true)

		const [isOpenView, setOpenView] = useState(false)

		const [viewRow, setViewRow] = useState({})

		const [editRow, setEditRow] = useState({})

		const [isAddUser, setAddUser] = useState(false)

		const [teamsData, setTeamsData] = useState([])

		const [isRefreshList, setRefreshList] = useState(false)

		const [deleteProps, setDeleteProps] = useState({
			isOpenPopup: false,
			row: {},
		})

		const [addHostCountryProp, setAddHostCountryProp] = useState({
			showPopup: false,
			isAdd: false,
			isEdit: false,
			GamesTeamID:" ",
			gamesParticipantID: 0,
		})

		//Lifecycle method, here call the refreshList function to load the list data
		useEffect(() => {
			//
			// Reading user's permissions from PermissionProvider component
			//
			let permissionArray = props.getAppPermissions()
			//Assign permission for this pages
			setPermissionGroup(permissionArray)

			window.scrollTo(0, 0)
			refreshList(0)
		}, [])

		//Here below method will be called by child component for refresh the list data
		useImperativeHandle(ref, () => ({
			refreshListData() {
				refreshList(0)
			},
		}))

		//Refresh List
		const refreshList = (pageNo) => {
			let departmentIDs = Array.isArray(filterProps.department)
				? filterProps.department
				: [filterProps.department]
			let gamesIDs = Array.isArray(filterProps.games) ? filterProps.games : [filterProps.games]
			let sportIDs = Array.isArray(filterProps.sport) ? filterProps.sport : [filterProps.sport]
			let teamsIDs = Array.isArray(filterProps.team) ? filterProps.team : [filterProps.team]

			let isSortOrder = false
			let sortByName = "GameName"
			let orderBy = "ASC"
			if (pageNo && pageNo.sorted !== undefined && pageNo.sorted.length > 0) {
				isSortOrder = true
				sortByName = pageNo.sorted[0].id
				if (pageNo.sorted[0].desc === true) {
					orderBy = "DESC"
				} else {
					orderBy = "ASC"
				}
			}
			let filterModel = {
				GameIDs: gamesIDs.length > 0 ? gamesIDs.join(",") : "",
				SportIDs: sportIDs.length > 0 ? sportIDs.join(",") : "",
				DepartmentIDs: departmentIDs.length > 0 ? departmentIDs.join(",") : "",
				TeamIDs: teamsIDs.length > 0 ? teamsIDs.join(",") : "",
				PageNo: pageNo > 0 ? pageNo : 1,
				PageSize: paginationProps.pageSize,
				SortColumn: sortByName,
				SortOrder: orderBy,
				IsExport: false,
			}
			let url = APIUrl.GET_GAMES_TEAM_LIST
			APIRequest.getPostService(url, filterModel)
				.then((res) => {
					if (res.Succeeded) {
						setTeamsData(res.Data)

						if (res.Data.length > 0) {
							setCanExport(false)
							setPaginationProps({
								...paginationProps,
								totalPages: res.Data[0].TotalPages,
								totalRecords: res.Data[0].TotalRows,
								totalCount: res.Data.length >= 1 ? res.Data.length : 0,
								sortColumn: sortByName,
								sortValue: orderBy,
							})
						} else {
							setPaginationProps({
								...paginationProps,
								totalPages: 0,
								totalRecords: 0,
								totalCount: 0,
							})
							setCanExport(true)
						}
						setRefreshList(false)
					}
				})
				.catch((err) => {})
		}

		//Filter handler, it will call by Filter child(HostCountryActivityFilter) component.
		const parentFilterHandler = (filterProps, isSubmitFilter) => {
			if (isSubmitFilter) {
				const { team, sport, games, department } = filterProps
				setFilterProps({
					team: team,
					department: department,
					games: games,
					sport: sport,
				})
			} else {
				setFilterProps({
					team: [],
					department: [],
					games: [],
					sport: [],
				})
			}
			//set refreshList flag as true
			setRefreshList(true)
		}

		//Call the refresh list, based on the isRefreshList flag
		useEffect(() => {
			if (isRefreshList) refreshList(0)
		}, [isRefreshList])

		//Call Export data
		const handleExportData = () => {
			let pageNo = 0
			let departmentIDs = Array.isArray(filterProps.department)
				? filterProps.department
				: [filterProps.department]
			let gamesIDs = Array.isArray(filterProps.games) ? filterProps.games : [filterProps.games]
			let sportIDs = Array.isArray(filterProps.sport) ? filterProps.sport : [filterProps.sport]
			let teamsIDs = Array.isArray(filterProps.team) ? filterProps.team : [filterProps.team]

			let isSortOrder = false
			let sortByName = "GameName"
			let orderBy = "ASC"
			if (pageNo && pageNo.sorted !== undefined && pageNo.sorted.length > 0) {
				isSortOrder = true
				sortByName = pageNo.sorted[0].id
				if (pageNo.sorted[0].desc === true) {
					orderBy = "DESC"
				} else {
					orderBy = "ASC"
				}
			}
			let filterModel = {
				GameIDs: gamesIDs.length > 0 ? gamesIDs.join(",") : "",
				SportIDs: sportIDs.length > 0 ? sportIDs.join(",") : "",
				DepartmentIDs: departmentIDs.length > 0 ? departmentIDs.join(",") : "",
				TeamIDs: teamsIDs.length > 0 ? teamsIDs.join(",") : "",
				PageNo: pageNo > 0 ? pageNo : 1,
				PageSize: paginationProps.totalRecords,
				SortColumn: sortByName,
				SortOrder: orderBy,
				IsExport: true,
			}
			let url = APIUrl.GET_HOST_COUNTRY_ACTIVITY_EXPORT_DATA
			APIRequest.getPostService(url, filterModel)
				.then((res) => {
					if (res.Succeeded) {
						let exportData = []
						if (res.Data && res.Data.length > 0) {
							exportData = res.Data.map((x) => {
								if (x.SportFacilityDate) {
									let getYear = moment(x.SportFacilityDate).year()
									x.SportFacilityDate =
										getYear === 1900 || getYear <= 1900
											? ""
											: moment(x.SportFacilityDate).format("DD MMM YYYY")
								}
								return x
							})
							setExportExcelProps({
								...exportExcelProps,
								exportData: exportData,
								isExportExcel: true,
							})
							setExportExcelProps({
								...exportExcelProps,
								exportData: [],
								isExportExcel: false,
							})
						}
					}
				})
				.catch((err) => {})
		}

		//Call pagination
		const handlePagination = (pageNo) => {
			refreshList(pageNo)
		}

		//Add user
		const addUser = (rows) => {
			setSelectedRow({
				sportID: rows.SportID,
				gamesTeamID: rows.GamesTeamID,
				departmentID: rows.DepartmentID,
				teamName: rows.TeamName,
				gamesSetupID: rows.GamesSetupID,
				gamesParticipantID: rows.GamesParticipantID,
			})
			setAddUser(true)
		}

		//No records found
		const NoDataConst = (props) => <span className='table-nodata-style'>No records found </span>

		//Update add user prop
		const handleUpdateAddUser = () => {
			setAddUser(false)
		}

		//Edit Host Country Activity by selected row
		const editHostCountryActivity = (row) => {
			window.scrollTo(0, 0)
			if(addHostCountryProp.gamesParticipantID == 0)
			{
			let	GamesTeamID=" ";
			}
			setAddHostCountryProp({
				...addHostCountryProp,
				isEdit: true,
				showPopup: true,
			
			})
			setEditRow(row)
		}

		//The below method will gets executed when addHostCountryProp.isEdit flag is true
		useEffect(() => {
			if (addHostCountryProp.isEdit) {
				addHostRef.current.editChildHandler(editRow)
			}
		}, [addHostCountryProp.isEdit])

		//set delete open model popup states
		const deleteTeams = (row) => {
			setDeleteProps({
				isOpenPopup: true,
				row: row,
			})
		}

		//Close popup
		const closePopup = () => {
			setDeleteProps({
				isOpenPopup: false,
				row: {},
			})
		}

		//Add or Edit
		const addOrEdit = () => {
			setAddHostCountryProp({
				...addHostCountryProp,
				showPopup: true,
				isAdd: true,
				isEdit: false,
			})
		}

		//update close model popup states
		const updatePopupState = (isRefresh) => {
			setAddHostCountryProp({
				...addHostCountryProp,
				showPopup: false,
				isAdd: false,
				isEdit: false,
			})
			setEditRow({})
			if (isRefresh)
				//refresh List Data
				refreshList()
		}

		//view host country activity model popup
		const viewHostCountryActivity = (row) => {
			setViewRow(row)
			updateOpenViewState()
		}

		//update open or close model popup
		const updateOpenViewState = () => {
			setOpenView(!isOpenView)
		}

		//Columns
		const columns = [
			{
				Header: () => <div style={{ textAlign: "left" }}>S.No</div>,
				accessor: "SerialNo",
				sortable: false,
				resizable: false,
				className: "tableheader wordwrap",
				headerClassName: "BoldText ColoredText",

				Cell: (row) => {
					return (
						<div className='tabledata' style={{ textAlign: "left" }}>
							{row.value}
						</div>
					)
				},
				width: Math.round(window.innerWidth * 0.04),
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Game</div>,
				resizable: false,
				accessor: "GameName",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.GameName}>
							{tableColumnDataLimit(row.original.GameName)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Category</div>,
				resizable: false,
				accessor: "Category",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.Category}>
							{tableColumnDataLimit(row.original.Category)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Department</div>,
				resizable: false,
				accessor: "DepartmentName",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.DepartmentName}>
							{tableColumnDataLimit(row.original.DepartmentName)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Sport</div>,
				resizable: false,
				accessor: "SportName",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.SportName}>
							{tableColumnDataLimit(row.original.SportName)}
						</div>
					)
				},
			},
			{
				Header: () => <div style={{ textAlign: "left" }}>Team</div>,
				resizable: false,
				accessor: "TeamName",
				style: { overflowWrap: "break-word" },
				Cell: (row) => {
					return (
						<div className='tabledata wordwrap' title={row.original.TeamName}>
							{tableColumnDataLimit(row.original.TeamName)}
						</div>
					)
				},
			},
			{
				Header: () => (
					<div style={{ textAlign: "center" }} >
						{"     "}Action
					</div>
				),
				accessor: "Action",
				className: "tableheader wordwrap",
				sortable: false,
				resizable: false,
				width: Math.round(window.innerWidth * 0.07),
				filterable: false,
				Cell: (row) => {
					return (
						<div className='tabledata text-center'>
							{permissionGroup.includes(Permissions.HOST_COUNTRY_EDIT) &&
							props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									title='Edit'
									className='mr-2 icon_edit'
									icon={faEdit}
									style={{ cursor: "pointer" }}
									onClick={() => editHostCountryActivity(row.original)}
								/>
							) : null}
							{permissionGroup.includes(Permissions.HOST_COUNTRY_CREATE) &&
							props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									title='Add Users'
									className='mr-2 icon_color'
									onClick={() => addUser(row.original)}
									icon={faUserPlus}
									style={{ cursor: "pointer" }}
								/>
							) : null}
							{props.user.InvitedUserID === row.original.CreatedByID ? (
								<FontAwesomeIcon
									title='View Host Country Activity'
									className='mr-2 icon_eye'
									onClick={() => viewHostCountryActivity(row.original)}
									icon={faEye}
									style={{ cursor: "pointer" }}
								/>
							) : null}
							{permissionGroup.includes(Permissions.HOST_COUNTRY_DELETE) &&
							props.user.InvitedUserID === row.original.CreatedByID ? (
								// <>
								//  	{!row.original.CanDelete ? (
										<FontAwesomeIcon
											title='Delete'
											className='mr-2 icon_Delete'
											icon={faTrashAlt}
											style={{ cursor: "pointer" }}
											onClick={() => deleteTeams(row.original)}
										/>
								//  	) : null}
								// </>
							) : null}
						</div>
					)
				},
			},
		]
		return (
			<>
				<div>
					<div className='h-full singletab'>
						<div className='games_layout form-group mt-3 row col-sm-12 pr-0'>
							<div className='col-sm-6'>Host Country Activities</div>
							{permissionGroup.includes(Permissions.HOST_COUNTRY_CREATE) ? (
								<div className='col-sm-6 d-flex justify-content-end pr-0'>
									<Button type='button' className='btn_green' onClick={addOrEdit}>
										Add Host Country Activity
									</Button>
								</div>
							) : null}
						</div>
						</div>
						{/* Host Country Activity Filter component below */}
						<div className='row col-sm-12 mr-0 ml-0'>
						<HostCountryActivityFilter parentFilterHandler={parentFilterHandler} />
						</div>
						{/* End filter component */}
						<div style={{ marginLeft: "15px", marginRight: "15px" }}>
						<ReactTable
							data={teamsData}
							columns={columns}
							showPaginationTop={false}
							showPaginationBottom={false}
							minRows={0}
							defaultPageSize={10}
							defaultPage={1}
							onFetchData={refreshList}
							ref={table}
							sortable={true}
							multiSort={true}
							manual
							className={"CosmicTable"}
							showPageSizeOptions={false}
							NoDataComponent={NoDataConst}
							defaultSorting={[
								{
									id: "id",
									desc: true,
								},
							]}
							manualPagination={true}
						/>
						</div>
						<div className='row col-sm-12 mt-2 pr-0'>
						<div className='col-sm-6 mt-1'>
							<label>
								<b>Total Records: </b>
								{paginationProps.totalRecords > 10
									? paginationProps.totalCount + " of " + paginationProps.totalRecords
									: paginationProps.totalRecords !== 0
									? paginationProps.totalRecords
									: 0}
							</label>
						</div>
						
						</div>
						{permissionGroup.includes(Permissions.HOST_COUNTRY_EXPORT) ? (
						<>
							<div className='row col-sm-12'>
								<div className='col-sm-6'>
									<Button
										className='btn_green mr-2'
										disabled={canExport}
										onClick={handleExportData}
									>
										<FontAwesomeIcon title='Export Data' color={"#FFFFFF"} icon={faDownload}>
											{" "}
										</FontAwesomeIcon>{" "}
										Export Excel
									</Button>
								</div>
								
								
								{paginationProps.totalPages > 0 ? (
							// <div  style={{marginTop:"1px"}}>
							<div className='col-sm-6 d-flex justify-content-end mt-0 pr-0'>
								<Pagination
									totalPages={paginationProps.totalPages}
									totalRecords={paginationProps.totalRecords}
									paginationCall={handlePagination}
								/>
							</div>
							// </div>
						
						) : null}
						</div>
						
							{exportExcelProps.isExportExcel ? (
								<ExportData
									data={exportExcelProps.exportData}
									label={exportLabel}
									filename={"Host Country Activity List"}
								/>
							) : null}
						</>
						) : null}
						{isAddUser ? (
						<AddUsers selectedRowProps={selectedRow} updateAddUserState={handleUpdateAddUser} />
						) : null}
						{deleteProps.isOpenPopup ? (
						<DeleteHostCountryActivity
							row={deleteProps.row}
							closePopup={closePopup}
							refreshList={refreshList}
						/>
						) : null}
						{addHostCountryProp.showPopup ? (
						<AddHostCountryActivity
							ref={addHostRef}
							modelProp={addHostCountryProp}
							refreshList={refreshList}
							updatePopupState={updatePopupState}
						/>
						) : null}
						{isOpenView ? (
						<ViewHostCountryActivity
							selectedRow={viewRow}
							updateOpenViewState={updateOpenViewState}
						/>
						) : null}
				</div>
			</>
		)
	})
)

const mapPropsToState = (state) => ({
	user: state.userReducer,
})

export default PermissionProvider(connect(mapPropsToState, null)(HostCountryActivityList))
