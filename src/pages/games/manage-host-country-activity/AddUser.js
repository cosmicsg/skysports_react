import { Input, Empty } from "antd";
import React, { memo, useEffect, useState } from "react";
import { Modal, Button } from "react-bootstrap";
import {
  APIRequest,
  showNotification,
} from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import UserImage from "../../../content/images/UserImage.png"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTimes } from "@fortawesome/free-solid-svg-icons";

var SelectedUserProfileID = null;
//Add user component
const AddUser = (props) => {
  const [searchUser, setSearchUser] = useState("");

  const [team, setTeam] = useState("");

  const [availableUserData, setAvailableUserData] = useState([]);
  const [selectedUserData, setSelectedUserData] = useState([]);
  const [prevSelectedUser, setPrevSelectedUser] = useState([]);
  const [tempData, setTempData] = useState([]);
  const [prevAvailableUser, setPrevAvailableUser] = useState([]);
  const [doSearch, setDoSearch] = useState(false);
  const [doHide, setDoHide] =useState(false);
  //close model popup
  const handleClosePopup = () => {
    props.updateAddUserState();
  };
 
  
  const clear = () => {
    setSearchUser("");
    setAvailableUserData([]);
    setSelectedUserData([]);
    setTempData([]);
    setPrevAvailableUser([]);
  };
  // onchange
  const handleOnchange = (event) => {
    const { value } = event.target;
    setSearchUser(value);
    searchFilter(value);
  };

  const searchFilter = (value) => {
    let searchKey = value.trim();
    let val = searchKey.toUpperCase();
    if (tempData.length === 0) setTempData(availableUserData);
    if (val) {
      let temp = prevAvailableUser.filter(
        (x) =>
          x.UserName.toUpperCase().startsWith(val) ||
          x.FullName.toUpperCase().startsWith(val)
      );
      setAvailableUserData(temp);
    } else {
      setAvailableUserData(prevAvailableUser);
      setTempData([]);
    }
    setDoSearch(false);
  };

  // life cycle method for initial load
  useEffect(() => {
    window.scrollTo(0,0);
    //clear fields
    clear();

    const { sportID, gamesTeamID, teamName, gamesParticipantID, departmentID } =
      props.selectedRowProps;
    setTeam(teamName);
    let filterParams = {
      SportID: sportID ? sportID : 0,
      TeamID: gamesTeamID ? gamesTeamID : 0,
      GamesParticipantID: gamesParticipantID ? gamesParticipantID : 0,
      DepartmentID: departmentID ? departmentID : 0,
    };
    APIRequest.getPostService(APIUrl.GET_SEARCH_USERNAME, filterParams)
      .then((res) => {
        if (res.Succeeded) {
          setAvailableUserData(res.Data.AvailableUsers);
          setPrevAvailableUser(res.Data.AvailableUsers);
          setSelectedUserData(res.Data.SelectedUsers);
          setPrevSelectedUser(res.Data.SelectedUsers);
        }
      })
      .catch((err) => {});
  }, []);

  // add user handler
  const handleAddUsers = (id) => {
    let availableUser = availableUserData.filter((x) => x.UserProfileID !== id);
    let selectedUser = availableUserData.filter((x) => x.UserProfileID === id);

    let tempSelectedUser = selectedUserData.concat(selectedUser);
    setAvailableUserData(availableUser);
    setSelectedUserData(tempSelectedUser);
    let temp2 = tempData.filter((x) => x.UserProfileID !== id);
    //let temp3 = tempData.filter(x=> !availableUser.some(y=> x.UserProfileID === y.UserProfileID));
    //let temp = temp2.concat(availableUser);
    setPrevAvailableUser(temp2); //changed
    setTempData(temp2);
    if (searchUser) {
      setDoSearch(true);
    }
  };

  useEffect(() => {
    if (doSearch) searchFilter(searchUser);
  }, [doSearch]);

  
  
  const onClickyes =() =>{
    setDoHide(false);
    let selectedUsers = selectedUserData.filter((x) => x.UserProfileID !== SelectedUserProfileID);
    let removedUser = selectedUserData.filter((x) => x.UserProfileID === SelectedUserProfileID);
    let tempAvailableUser = availableUserData.concat(removedUser);
    setSelectedUserData(selectedUsers);
    setAvailableUserData(tempAvailableUser);
    if (searchUser) {
      let temp2 = tempData.filter((x) => x.UserProfileID !== SelectedUserProfileID);
      let temp = temp2.concat(removedUser);
      setTempData(temp);
      setPrevAvailableUser(temp);
      setDoSearch(true);
    } else {
      setPrevAvailableUser(tempAvailableUser);
    }
  }
  const onClickNo =()=>{
    setDoHide(false);
    
  }
  // remove user handler
  const handleRemoveUsers = (id) => {
    setDoHide(true);
    SelectedUserProfileID = id;
   
  };

  //concat string ends with dot notation if the string length > 25
  const handleStringEndsWithDotNotation = (str) => {
    let tempString = str;
    if (tempString !== "" && tempString !== undefined && tempString !== null) {
      let temp = tempString.toString();
      if (temp.length > 25) {
        tempString = temp.slice(0, 25).concat("...");
      }
    }
    return tempString;
  };

  //save
  const handleSaveUser = () => {
    if (selectedUserData.length === 0 && availableUserData.length > 0) {
    }
    let removedUser = prevSelectedUser.filter(
      (x) => !selectedUserData.some((y) => x.UserProfileID === y.UserProfileID)
    );

    let newlyAddedUser = selectedUserData.filter(
      (x) => !prevSelectedUser.some((y) => x.UserProfileID === y.UserProfileID)
    );
    let tUserIDs = newlyAddedUser.map((x) => x.UserProfileID);
    let rUserIDs = removedUser.map((x) => x.UserProfileID);
    const { gamesTeamID, gamesSetupID, gamesParticipantID } =
      props.selectedRowProps;
    let params = {
      TeamID: gamesTeamID ? gamesTeamID : 0,
      RemovedUser: rUserIDs ? rUserIDs.join(",") : "",
      NewlyAddedUser: tUserIDs ? tUserIDs.join(",") : "",
      GamesSetupID: gamesSetupID ? gamesSetupID : 0,
      GamesParticipantID: gamesParticipantID ? gamesParticipantID : 0,
    };
    APIRequest.getPostService(APIUrl.ADD_GAMES_TEAM_USER, params)
      .then((res) => {
        {
          showNotification(
            "Successfully Saved.",
            "success"
          );
        }
        //else if(res.Data.IsUserAddedOrRemoved && !res.Data.IsSentNotification) {
        //  showNotification(
        //    "Error in sending notification to added user.",
        //    "error"
        //  );
        //}
        handleClosePopup();
      })
      .catch((err) => {});
  };

  //cancel or close model popup
  const handleCancel = () => {
    handleClosePopup();
  };
  return (
    <>
    
     
      <Modal
      className="user-list-toggle-main1"
      size="lg"
      backdrop="static"
      show={true}
      >
        <Modal.Header className="pb-0 border-bottom-0 popup_header font-weight-bold">
          <p>Team Users</p>
          <div className="col-md-1">
            <div className="d-flex justify-content-end">
              <button
                title="Close"
                type="button"
                className="close"
                data-dismiss="modal"
                area-aria-label="Close"
                onClick={handleClosePopup}
              >
                <span style={{ color: "#c29a37" }} area-hidden="true">
                  &times;
                </span>
              </button>
            </div>
          </div>
        </Modal.Header>
        <Modal.Body>
          <>
            <div className="row col-sm-12 form-group">
              <div className="col-sm-6">
                <label style={{ marginTop: "2px", color: "#03534c" }}>
                  <b>Team Name</b>
                </label>
                <label style={{ marginLeft: "10px" }}>{team}</label>
              </div>
            </div>
            <div className="col-sm-6 form-group">
              <label>
                <b style={{ color: "#03534c" }}>Search Available User</b>
              </label>
              <Input
                type="text"
                name="searchUser"
                size="large"
                autoComplete="off"
                value={searchUser}
                onChange={handleOnchange}
                bordered={false}
                className="textfiled w-80"
                placeholder="Name / Email"
              ></Input>
            </div>
            <div className="row col-sm-12">
              <div className="col-sm-6">
                <label style={{ color: "#03534c" }}>
                  <b>Available Users</b>
                </label>
                <ul
                  className="list-group"
                  style={{ height: "300px", overflow: "auto" }}
                >
                  {availableUserData.length > 0 ? (
                    availableUserData.map((item, index) => (
                      <li
                        title={item.UserName}
                        style={{ cursor: "pointer" }}
                        className="list-group-item"
                        key={index}
                        value={item.UserProfileID}
                      >
                        <div
                          style={{ cursor: "pointer" }}
                          className="row"
                          onClick={() => handleAddUsers(item.UserProfileID)}
                        >
                          <div className="col-sm-2">
                            <img
                              className="add-users-with-team"
                              src={
                                item.ImageFileData
                                  ? item.ImageFileData
                                  : UserImage
                              }
                              alt={"user"}
                            ></img>{" "}
                          </div>
                          <div className="col-sm-8">
                            <span>
                              <b>
                                {handleStringEndsWithDotNotation(item.FullName)}
                              </b>
                            </span>
                            <br />
                            <span>
                              {handleStringEndsWithDotNotation(item.UserName)}
                            </span>
                          </div>
                          <div
                            className="col-sm-2 d-flex justify-content-end pt-0"
                            style={{ color: "green", marginTop: "13px" }}
                          >
                            <FontAwesomeIcon icon={faPlus}></FontAwesomeIcon>
                          </div>
                        </div>
                      </li>
                    ))
                  ) : (
                    <Empty
                      className="mt-5"
                      image={UserImage}
                      description="No users found"
                    ></Empty>
                  )}
                </ul>
              </div>
              <div className="col-sm-6">
                <label style={{ color: "#03534c" }}>
                  <b>Selected Users</b>
                </label>
                <ul
                  className="list-group form-group"
                  style={{ height: "300px", overflow: "auto" }}
                >
                  {selectedUserData.length > 0 ? (
                    selectedUserData.map((item, index) => (
                      <li
                        title={item.UserName}
                        style={{ cursor: "pointer" }}
                        className="list-group-item"
                        key={index}
                        value={item.UserProfileID}
                      >
                        <div
                          style={{ cursor: "pointer" }}
                          className="row"
                          onClick={() => handleRemoveUsers(item.UserProfileID)}
                        >
                          <div className="col-sm-2">
                            <img
                              className="add-users-with-team"
                              src={
                                item.ImageFileData
                                  ? item.ImageFileData
                                  : UserImage
                              }
                              alt={"user"}
                            ></img>{" "}
                          </div>
                          <div className="col-sm-8">
                            <span>
                              <b>
                                {handleStringEndsWithDotNotation(item.FullName)}
                              </b>
                            </span>
                            <br />
                            <span>
                              {handleStringEndsWithDotNotation(item.UserName)}
                            </span>
                          </div>
                          <div
                            className="col-sm-2 d-flex justify-content-end pt-0"
                            style={{ color: "red", marginTop: "13px" }}
                          >
                            <FontAwesomeIcon icon={faTimes}></FontAwesomeIcon>
                          </div>
                        </div>
                      </li>
                    ))
                  ) : (
                    <Empty
                      className="mt-5"
                      image={UserImage}
                      description="No users found"
                    ></Empty>
                  )}
                </ul>
              </div>
            </div>

            <div className="row col-sm-12 d-flex justify-content-end mt-4">
            <Button className="btn_green mr-2" onClick={handleCancel}>
                Close
              </Button>
              <Button
                className="btn_green "
                onClick={handleSaveUser}
                //disabled={selectedUserData.length > 0 ? false : true}
              >
                Save
              </Button>
              
            </div>
          </>
        </Modal.Body>
      </Modal>
      <Modal
      className="user-list-toggle-main1"
        size="sm"
        backdrop="static"
        show={doHide}
        border-color="black"
      >
        <Modal.Body>
          <div>Your are sure about to Remove this Player</div>
          <div className='col-sm-12'>
          <div className='col-sm d-flex justify-content-end pt-4' >
            <Button className='btn_green mr-2 '  value={"No"} onClick={onClickNo}>No</Button>
            <Button  className='btn_green ' value={"Yes"} onClick={onClickyes}>Yes</Button>
          </div>
          </div>
        </Modal.Body>
      </Modal>
      {/* </Modal> */}
    </>
  );
};

export default memo(AddUser);
