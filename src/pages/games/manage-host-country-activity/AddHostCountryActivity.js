import { Input, Select, DatePicker } from "antd"
import moment from "moment"
import React, { useState, useEffect, forwardRef, useImperativeHandle } from "react"
import { Button, Modal } from "react-bootstrap"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest, showNotification } from "../../../components/api-manager/apiRequest"

//get Option from select tag
const { Option } = Select

//Add Games Host Country Activity component
const AddHostCountryActivity = forwardRef((props, ref) => {
	// Add Host Country Activity fields declaration
	const [hostActivityFields, setHostActivityFields] = useState({
		game: [],
		category: [],
		department: [],
		users: [],
		sport: [],
		teamName: "",
		round: [],
		facility: [],
		facilityDate: "",
		slot: [],
		participantType: [],
		event: [],
	})

	const [gameStartDate, setGameStartDate] = useState("")
	const[gameEndDate, setGameEndDate]=useState("")
	const [disableStartDate, setDisableStartDate] = useState("")
	const[disableEndDate, setDisableEndDate]=useState("")
	const [disableScheduleDate, setDisableScheduleDate] = useState(true)
	const [current] = useState(null)

	//flags declaration for Category change field
	const [conditionField, setConditionField] = useState({
		showDepartment: false,
		showSports: false,
	})

	//form dropdown list data variable declaration
	const [formData, setFormData] = useState({
		gamesData: [],
		departmentData: [],
		usersData: [],
		sportsData: [],
		//roundsData: [],
		//facilityData: [],
		//facilityDateData: [],
		//slotsData: [],
		//eventsData: [],
	})

	const [roundsData, setRoundData] = useState([])
	const [facilityData, setFacilityData] = useState([])
	const [slotsData, setSlotsData] = useState([])
	const [eventsData, setEventsData] = useState([])
	//error object declaration
	const [errors, setErrors] = useState({})

	//for Edit
	const [selectedRow, setSelectedRow] = useState({})
	//Edit Host Country Activity fields declaration
	const [editProps, setEditProps] = useState({
		isEdit: false,
		ID: 0,
		PrevTeamName: "",
		teamID: 0,
		gamesParticipantID: 0,
	})

	//edit callback from parent component
	useImperativeHandle(ref, () => ({
		editChildHandler(row) {
			editHostCountryActivity(row)
			setSelectedRow(row)
		},
	}))

	async function Test() {
		throw new Error("this is error occurred")
	}

	useEffect(() => {
		let te = Test()
		te.then((res) => {
		}).catch((err) => {
		})
		if (editProps.isEdit && formData.gamesData.length > 0 && selectedRow.GamesSetupID > 0) {
			setHostActivityFields((prevState) => ({
				...prevState,
				game: selectedRow.GamesSetupID,
			}))
		}
	}, [formData.gamesData])

	useEffect(() => {
		if (editProps.isEdit && formData.departmentData.length > 0 && selectedRow.DepartmentID > 0) {
			setHostActivityFields((prevState) => ({
				...prevState,
				department: selectedRow.DepartmentID,
			}))
		}
	}, [formData.departmentData])

	useEffect(() => {
		if (editProps.isEdit && formData.sportsData.length > 0 && selectedRow.SportID > 0) {
			setHostActivityFields((prevState) => ({
				...prevState,
				sport: selectedRow.SportID,
			}))
		}
	}, [formData.sportsData])

	useEffect(() => {
		if (editProps.isEdit && eventsData.length > 0 && selectedRow.SportEventID > 0) {
			setHostActivityFields((prevState) => ({
				...prevState,
				event: selectedRow.SportEventID,
			}))
		}
	}, [eventsData])

	useEffect(() => {
		if (editProps.isEdit && roundsData.length > 0 && selectedRow.SportRoundID > 0) {
			setHostActivityFields((prevState) => ({
				...prevState,
				round: selectedRow.SportRoundID,
			}))
		}
	}, [roundsData])

	useEffect(() => {
		if (editProps.isEdit && facilityData.length > 0 && selectedRow.SportFacilityID > 0) {
			setHostActivityFields((prevState) => ({
				...prevState,
				facility: selectedRow.SportFacilityID,
			}))
		}
	}, [facilityData])

	useEffect(() => {
		if (editProps.isEdit && slotsData.length > 0 && selectedRow.SportSlotID > 0) {
			setHostActivityFields((prevState) => ({
				...prevState,
				slot: selectedRow.SportSlotID,
			}))
		}
	}, [slotsData])

	//edit handler
	const editHostCountryActivity = (row) => {
		setGameStartDate(row.GameStartDate ? row.GameStartDate : " ")
		setGameEndDate(row.gameEndDate? row.gameEndDate:" ")
		setDisableScheduleDate(false)
		let isDepartment = row.DepartmentID > 0 ? true : false
		setConditionField({
			showDepartment: isDepartment,
			showSports: !isDepartment,
		})
		let participantID = row.GamesParticipantID > 0 ? row.GamesParticipantID : 0
		setEditProps({
			ID: participantID,
			isEdit: true,
			PrevTeamName: row.TeamName ? row.TeamName : "",
			gamesParticipantID: participantID,
			teamID: row.GamesTeamID > 0 ? row.GamesTeamID : 0,
		})
		if (isDepartment) {
			setHostActivityFields({
				category: "departments",
				teamName: row.TeamName,
			})
			loadDepartment()
		} else {
			setHostActivityFields({
				category: "sport",
				facilityDate: row.SportFacilityDate ? moment(row.SportFacilityDate) : new Date(),
				participantType: row.TeamName ? "team" : "individual",
				teamName: row.TeamName ? row.TeamName : "",
			})
			if (row.GamesSetupID > 0 && row.SportID > 0) {
				handleLoadSports(row.GamesSetupID)
				loadSportBased(row.SportID)
			}
		}
	}

	//It is lifecycle method of reactJS. In this I we have loaded our default dropdown values
	useEffect(() => {
		window.scrollTo(0, 0)
		//Getting games dropdown value
		APIRequest.getGetService(APIUrl.GET_GAMES_LIST).then((res) => {
			if (res.Succeeded) {
				setFormData((prevState) => ({
					...prevState,
					gamesData: res.Data,
				}))
			}
		})
	}, [])

	//Onchange event for all the fields
	const handleOnchange = (name, val) => {
		//update value based on onchange event for particular fields
		setHostActivityFields({
			...hostActivityFields,
			[name]: val,
		})

		if (name === "game") assignStartDate(val)
		if (name === "game") assignEndDate(val)
		//check and update values for the error object, this will clear the error msg for the selected values
		if (val !== "" && val !== undefined && val !== null && val.length !== 0) {
			//below condition only for the department onchange or sport onchange
			if (name === "department" || name === "sport") handleLoadUsers(name, val)

			setErrors({
				...errors,
				[name]: "",
			})
		}

		if (name === "sport") clearDependency()
		//if it is coming from category onchange
		if (name === "category") handleCategoryChange(val)

		if (name !== "category" && name === "game" && hostActivityFields.category === "sport")
			handleLoadSports(val)

		if (name === "sport" && val !== "" && val !== undefined && val !== null) loadSportBased(val)
	}

	//Assign Games startDate
	const assignStartDate = (val) => {
		setHostActivityFields((prevState) => ({
			...prevState,
			facilityDate: "",
		}))
		if (val !== "" && val !== null && val !== undefined && val.length !== 0) {
			setDisableScheduleDate(false)
			let temp = formData.gamesData.filter((x) => x.OptionID === val)
			if (temp && temp[0].StartDate) {

				let ScheduleStartDate = moment(temp[0].StartDate)
				ScheduleStartDate.format("YYYY-MM-DD")
				setDisableStartDate(ScheduleStartDate)
				setGameStartDate(temp[0].StartDate)
			} else {
				setGameStartDate("")
			}
		} else {
			setDisableScheduleDate(true)
			setGameStartDate("")
		}
	}

	const assignEndDate = (val) => {
		setHostActivityFields((prevState) => ({
			...prevState,
			facilityDate: "",
		}))
		if (val !== "" && val !== null && val !== undefined && val.length !== 0) {
			setDisableScheduleDate(false)
			let temp = formData.gamesData.filter((x) => x.OptionID === val)
			if (temp && temp[0].EndDate) {

				let ScheduleEndDate = moment(temp[0].EndDate)
				ScheduleEndDate.format("YYYY-MM-DD")
				setDisableEndDate(ScheduleEndDate)
				setGameEndDate(temp[0].EndDate)
			} else {
				setGameEndDate("")
			}
		} else {
			setDisableScheduleDate(true)
			setGameEndDate("")
		}
	}
	

	//Clear
	const clearDependency = () => {
		setHostActivityFields((prevState) => ({
			...prevState,
			round: [],
			facility: [],
			facilityDate: "",
			slot: [],
			event: [],
		}))
		setRoundData([])
		setFacilityData([])
		setEventsData([])
		setSlotsData([])
	}
	//handle onchange for category change event
	const handleCategoryChange = (val) => {
		setHostActivityFields({
			...hostActivityFields,
			category: val,
			department: [],
			users: [],
			teamName: "",
			participantType: [],
		})
		if (val === "departments") {
			//Getting department dropdown value
			loadDepartment()

			setConditionField({
				showDepartment: true,
				showSports: false,
			})
		} else if (val === "sport") {
			//Call handleLoadSports func
			handleLoadSports(hostActivityFields.game)

			//change flags to sports based on selected sports option in the category
			setConditionField({
				showDepartment: false,
				showSports: true,
			})
		}
	}

	//Load department
	const loadDepartment = () => {
		APIRequest.getGetService(APIUrl.DEPARTMENT_LIST).then((res) => {
			if (res.Succeeded) {
				setFormData((prevState) => ({
					...prevState,
					departmentData: res.Data,
				}))
			}
		})
	}

	//Load sport
	const loadSportBased = (sportId) => {
		//load Sport round data
		APIRequest.getGetService(APIUrl.GET_GAMES_SPORT_ROUND + "/" + sportId).then((res) => {
			if (res.Succeeded) {
				setRoundData(res.Data)
			}
		})
		//load Games sport facility data
		APIRequest.getGetService(APIUrl.GET_GAMES_SPORT_FACILITY + "/" + sportId).then((res) => {
			if (res.Succeeded) {
				setFacilityData(res.Data)
			}
		})
		//load games sport facility date data
		APIRequest.getGetService(APIUrl.GET_GAMES_SPORT_EVENT + "/" + parseInt(sportId)).then((res) => {
			setEventsData(res.Data)
		})
		//load games sport slot data
		APIRequest.getGetService(APIUrl.GET_GAMES_SPORT_SLOT + "/" + sportId).then((res) => {
			if (res.Succeeded) {
				setSlotsData(res.Data)
			}
		})
	}

	//Load data list
	const handleLoadSports = (val) => {
		setHostActivityFields((prevState) => ({
			...prevState,
			sport: [],
		}))
		if (!val) {
			setFormData((prevState) => ({
				...prevState,
				usersData: [],
				sportsData: [],
			}))
		} else {
			let gameID = val > 0 ? val : 0
			//Load sports data
			APIRequest.getGetService(APIUrl.GET_GAMES_SPORT_DROPDOWN_LIST + "/" + gameID).then((res) => {
				if (res.Succeeded) {
					setFormData((prevState) => ({
						...prevState,
						usersData: [],
						sportsData: res.Data,
					}))
				}
			})
		}
	}

	//Getting UsersList dropdown value
	const handleLoadUsers = (name, ID) => {
		let params = {
			DepartmentID: name === "department" && ID ? parseInt(ID) : 0,
			SportID: name === "sport" && ID ? parseInt(ID) : 0,
		}
		APIRequest.getPostService(APIUrl.GET_USERS, params)
			.then((res) => {
				if (res.Succeeded) {
					setFormData({
						...formData,
						usersData: res.Data,
					})
				}
			})
			.catch((err) => {})
	}

	useEffect(() => {
		if (current) disabledDateSelection(current)
	}, [gameStartDate, gameEndDate])

	const disabledDateSelection = (current) => {
		//
		//current && current.valueOf() > Date.now();
		let startDate = new Date(gameStartDate)
		let endDate = new Date(gameEndDate) //"2021-10-21T18:30:00+00:00");
		return startDate.valueOf() > current.valueOf() && startDate.valueOf() > current.valueOf()
	}

	//handle empty field validation
	const handleFieldValidation = () => {
		let isErrorExist = false
		const {
			game,
			category,
			department,
			sport,
			teamName,
			round,
			facilityDate,
			facility,
			slot,
			participantType,
			event,
		} = hostActivityFields
		const { showDepartment, showSports } = conditionField

		let error = {}
		if (game === "" || game === undefined || game === null || game.length === 0) {
			error["game"] = "Please select Game"
			isErrorExist = true
		}
		if (category === "" || category === undefined || category === null || category.length === 0) {
			error["category"] = "Please select Category"
			isErrorExist = true
		}
		if (showDepartment) {
			if (
				department === "" ||
				department === undefined ||
				department === null ||
				department.length === 0
			) {
				error["department"] = "Please select Department"
				isErrorExist = true
			}
		} else if (showSports) {
			if (sport === "" || sport === undefined || sport === null || sport.length === 0) {
				error["sport"] = "Please select Sport"
				isErrorExist = true
			}
		}
		if (
			((department !== undefined && department !== null && department > 0) ||
				participantType !== "individual") &&
			(teamName === "" || teamName === undefined || teamName === null)
		) {
			error["teamName"] = "Please enter TeamName"
			isErrorExist = true
		}
		if(teamName.length>200){
			error["teamName"]="Only 200 Characters"
			isErrorExist = true
		}
		if (category === "sport") {
			if (event === "" || event === undefined || event === null || event.length === 0) {
				isErrorExist = true
				error["event"] = "Please select Event"
			}
			if (round === "" || round === undefined || round === null || round.length === 0) {
				isErrorExist = true
				error["round"] = "Please select Round"
			}
			if (facility === "" || facility === undefined || facility === null || facility.length === 0) {
				error["facility"] = "Please select Facility"
				isErrorExist = true
			}
			if (facilityDate === "" || facilityDate === undefined || facilityDate === null) {
				error["facilityDate"] = "Please select Facility Date"
				isErrorExist = true
			}
			if (slot === "" || slot === undefined || slot === null || slot.length === 0) {
				error["slot"] = "Please select Slot"
				isErrorExist = true
			}
		}
		if (
			category === "sport" &&
			(participantType === "" ||
				participantType === undefined ||
				participantType === null ||
				participantType.length === 0)
		) {
			error["participantType"] = "Please select Participant Type"
			isErrorExist = true
		}

		setErrors(error)
		return isErrorExist
	}

	//handle submit func
	const save = (e) => {
		e.preventDefault()

		let isValid = handleFieldValidation()

		if (!isValid) {
			const { game, teamName, sport, department, round, facility, facilityDate, slot, event } =
				hostActivityFields
			let inputData = {
				GamesSetupID: game > 0 ? game : 0,
				TeamName: teamName,
				SportID: sport > 0 ? sport : 0,
				DepartmentID: department > 0 ? department : 0,
				GamesTeamID: editProps.teamID > 0 ? editProps.teamID : 0,
				isEdit: editProps.isEdit,
				PrevTeamName: editProps.PrevTeamName,
				SportRoundID: round > 0 ? round : 0,
				PrevRoundID: "",
				SportFacilityID: facility > 0 ? facility : 0,
				SportFacilityDate: facilityDate,
				SportSlotID: slot > 0 ? slot : 0,
				GamesParticipantID: editProps.gamesParticipantID > 0 ? editProps.gamesParticipantID : 0,
				SportEventID: event > 0 ? event : 0,
			}

			APIRequest.getPostService(APIUrl.CREATE_GAMES_TEAM, inputData)
				.then((res) => {
					if (res.Succeeded) {
						if (res.Data.IsExist) {
							let error = {}
							error["alreadyExist"] = "Same Host Country Activity already exist"
							setErrors(error)
						} else {
							if (editProps.isEdit)
								showNotification("Host Country Activity updated successfully.", "success")
							else showNotification("Host Country Activity created successfully.", "success")
							props.updatePopupState(true)
							handleClear()
						}
					} else {
						showNotification("Something went wrong. Please close model and try again", "error")
					}
				})
				.catch((err) => {
				})
		}
	}

	//handle clear func
	const handleClear = () => {
		setHostActivityFields({
			game: [],
			category: [],
			department: [],
			users: [],
			sport: [],
		})
		setConditionField({
			showDepartment: false,
			showSports: false,
		})
		setErrors({})
	}

	//close model popup
	const closePopup = () => {
		clearDependency()
		props.updatePopupState(false)
	}

	return (
		<>
			<Modal show={true} backdrop='static' size='md'>
				<Modal.Header className='pb-0 border-bottom-0 popup_header font-weight-bold'>
					<p> {props.modelProp.isAdd ? "Add " : "Edit "}Host Country Activity</p>
					<div className='col-md-1'>
						<div className='d-flex justify-content-end'>
							<button
								title='Close'
								type='button'
								className='close'
								aria-label='Close'
								onClick={closePopup}
							>
								<span style={{ color: "#c29a37" }} area-hidden='true'>
									&times;
								</span>
							</button>
						</div>
					</div>
				</Modal.Header>
				<Modal.Body>
					<>
						<div className='row col-sm-12 form-group'>
							<div className='col-sm-6'>
								<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
									Game Name<span className='text-danger'>*</span>
								</label>
								<Select
									key='game'
									className='textfiled w-80'
									name='Game'
									size={200}
									showSearch
									allowClear
									bordered={false}
									placeholder='Select Game'
									value={hostActivityFields.game}
									onChange={(val) => handleOnchange("game", val)}
									optionFilterProp='children'
									filterSort={(optionA, optionB) => {
										return optionA.children
											.toLowerCase()
											.localeCompare(optionB.children.toLowerCase())
									}}
								>
									{formData.gamesData.map((item, index) => (
										<Option key={index} value={item.OptionID}>
											{item.OptionData}
										</Option>
									))}
								</Select>
								<span className='text-danger'>{errors.game}</span>
							</div>
							<div className='col-sm-6'>
								<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
									Category<span className='text-danger'>*</span>
								</label>
								<Select
									className='textfiled w-80'
									bordered={false}
									allowClear
									showSearch
									size={200}
									value={hostActivityFields.category}
									onChange={(val) => handleCategoryChange(val)}
									placeholder='Select Category'
									optionFilterProp='children'
									filterSort={(optionA, optionB) =>
										optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
									}
									key='category'
								>
									<Option value='departments'>Department</Option>
									<Option value='sport'>Sport</Option>
								</Select>
								<span className='text-danger'>{errors.category}</span>
							</div>
						</div>
						<div
							className={conditionField.showDepartment ? "row col-sm-12 form-group" : "form-group"}
						>
							{conditionField.showDepartment ? (
								<>
									<div className='col-sm-6'>
										<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Departments<span className='text-danger'>*</span>
										</label>
										<Select
											className='textfiled w-80'
											name='department'
											showSearch
											allowClear
											bordered={false}
											value={hostActivityFields.department}
											onChange={(val) => handleOnchange("department", val)}
											placeholder='Select Department'
											optionFilterProp='children'
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{formData.departmentData.map((item, index) => {
												return (
													<Option key={index} value={item.OptionID}>
														{item.OptionData}
													</Option>
												)
											})}
										</Select>
										<span className='text-danger'>{errors.department}</span>
									</div>
								</>
							) : null}
							{conditionField.showSports ? (
								<>
									<div className='row col-sm-12 form-group'>
										<div className='col-sm-6'>
											<label style={{color:"#03534c"}} class="font-weight-bold popup_txt"> 
												Sport<span className='text-danger'>*</span>
											</label>
											<Select
												className='textfiled w-80'
												name='sport'
												showSearch
												allowClear
												bordered={false}
												value={hostActivityFields.sport}
												onChange={(val) => handleOnchange("sport", val)}
												placeholder='Select Sport'
												optionFilterProp='children'
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{formData.sportsData.map((item, index) => (
													<Option key={index} value={item.OptionID}>
														{item.OptionData}
													</Option>
												))}
											</Select>
											<span className='text-danger'>{errors.sport}</span>
										</div>
										<div className='col-sm-6'>
											<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
												Event<span className='text-danger'>*</span>
											</label>
											<Select
												className='textfiled w-80'
												name='event'
												showSearch
												allowClear
												bordered={false}
												value={hostActivityFields.event}
												onChange={(val) => handleOnchange("event", val)}
												placeholder='Select Event'
												optionFilterProp='children'
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{eventsData.map((item, index) => (
													<Option key={index} value={item.OptionID}>
														{item.OptionData}
													</Option>
												))}
											</Select>
											<span className='text-danger'>{errors.event}</span>
										</div>
									</div>
									<div className='row col-sm-12 form-group'>
										<div className='col-sm-6'>
											<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
												Rounds/Levels/Sets<span className='text-danger'>*</span>
											</label>
											<Select
												className='textfiled w-80'
												name='round'
												showSearch
												allowClear
												bordered={false}
												value={hostActivityFields.round}
												onChange={(val) => handleOnchange("round", val)}
												placeholder='Select Round'
												optionFilterProp='children'
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{roundsData.map((item, index) => (
													<Option key={index} value={item.OptionID}>
														{item.OptionData}
													</Option>
												))}
											</Select>
											<span className='text-danger'>{errors.round}</span>
										</div>
										<div className='col-sm-6'>
											<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
												Facility<span className='text-danger'>*</span>
											</label>
											<Select
												className='textfiled w-80'
												name='facility'
												showSearch
												allowClear
												bordered={false}
												value={hostActivityFields.facility}
												onChange={(val) => handleOnchange("facility", val)}
												placeholder='Select Facility'
												optionFilterProp='children'
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{facilityData.map((item, index) => (
													<Option key={index} value={item.OptionID}>
														{item.OptionData}
													</Option>
												))}
											</Select>
											<span className='text-danger'>{errors.facility}</span>
										</div>
									</div>
									<div className='row col-sm-12 form-group'>
										<div className='col-sm-6'>
											<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
												Schedule Date<span className='text-danger'>*</span>
											</label>
											<DatePicker
												style={{ cursor: "pointer" }}
												className='add-host-date-field'
												size='large'
												format='DD MMM YYYY'
												bordered={false}
												showToday={true}
												allowClear
												value={hostActivityFields.facilityDate}
												disabled={disableScheduleDate}
												disabledDate={d=> d.isAfter(disableEndDate) || d.isSameOrBefore(disableStartDate)}
												onChange={(val) => handleOnchange("facilityDate", val)}
											/>
											{/*<Select
                        className="textfiled w-80"
                        name="facilityDate"
                        showSearch
                        allowClear
                        bordered={false}
                        value={hostActivityFields.facilityDate}
                        onChange={(val) => handleOnchange("facilityDate", val)}
                        placeholder="Select Facility Date"
                        optionFilterProp="children"
                        filterSort={(optionA, optionB) =>
                          optionA.children
                            .toLowerCase()
                            .localeCompare(optionB.children.toLowerCase())
                        }
                      >
                        {formData.facilityDateData.map((item, index) => (
                          <Option key={index} value={item.OptionID}>
                            {item.OptionData}
                          </Option>
                        ))}
                      </Select>*/}
											<span className='text-danger'>{errors.facilityDate}</span>
										</div>
										<div className='col-sm-6'>
											<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
												Slots<span className='text-danger'>*</span>
											</label>
											<Select
												className='textfiled w-80'
												name='slot'
												showSearch
												allowClear
												bordered={false}
												value={hostActivityFields.slot}
												onChange={(val) => handleOnchange("slot", val)}
												placeholder='Select Slot'
												optionFilterProp='children'
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{slotsData.map((item, index) => (
													<Option key={index} value={item.OptionID}>
														{item.OptionData}
													</Option>
												))}
											</Select>
											<span className='text-danger'>{errors.slot}</span>
										</div>
									</div>
									<div className='row col-sm-12 form-group'>
										<div className='col-sm-6'>
											<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
												Participant Type<span className='text-danger'>*</span>
											</label>
											<Select
												className='textfiled w-80'
												name='participantType'
												showSearch
												allowClear
												bordered={false}
												value={hostActivityFields.participantType}
												onChange={(val) => handleOnchange("participantType", val)}
												placeholder='Select Participant Type'
												optionFilterProp='children'
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												<Option key={1} value='team'>
													Team
												</Option>
												<Option key={2} value='individual'>
													Individual
												</Option>
											</Select>
											<span className='text-danger'>{errors.participantType}</span>
										</div>
										{hostActivityFields.participantType === "team" ? (
											<div className='col-sm-6'>
												<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
													Team Name <span className='text-danger'>*</span>
												</label>
												<Input
													style={{ borderBottomLeftRadius: "1px" }}
													type='text'
													name='teamName'
													size='large'
													max={200}
													autoComplete='off'
													value={hostActivityFields.teamName}
													onChange={(e) => handleOnchange("teamName", e.target.value)}
													bordered={false}
													className='textfiled w-80'
													placeholder='Enter Team Name'
												/>
												<span className='text-danger'>{errors.teamName}</span>
											</div>
										) : null}
									</div>
								</>
							) : null}
							{conditionField.showDepartment ? (
								<div className='col-sm-6'>
									<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
										Team Name <span className='text-danger'>*</span>
									</label>
									<Input
										style={{ borderColor: "red !important" }}
										type='text'
										name='teamName'
										size='large'
										autoComplete='off'
										value={hostActivityFields.teamName}
										onChange={(e) => handleOnchange("teamName", e.target.value)}
										bordered={false}
										className='textfiled w-80'
										placeholder='Create Team'
									/>
									<span className='text-danger'>{errors.teamName}</span>
								</div>
							) : null}
						</div>
						<div className='row col-sm-12 form-group' style={{ textAlign: "Right" }}>
							{/*<div className="col-sm-3"></div>
              <div className="col-sm-7 mt-4 d-flex justify-content-end">
                <span className="text-danger">{errors.alreadyExist}</span>
              </div>*/}
							<div className='col-sm pt-2'>
								<Button className='btn_green  ' onClick={save}>
									Update
								</Button>
							</div>
						</div>
					</>
				</Modal.Body>
			</Modal>
		</>
	)
})

export default React.memo(AddHostCountryActivity)
