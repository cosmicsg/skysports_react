import React, { Fragment, Component } from "react";
import {
  Row,
  Col,
  Container,
  Button,
  Modal,
  Card,
  Collapse,
} from "react-bootstrap";
import "../../../styles/CosmicTable.css";
import "../../../styles/grid.css";
import "antd/dist/antd.css";
import { Select, Spin, notification, Input } from "antd";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import Pagination from "../../../utils/Pagination";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ExportData from "../../../utils/ExportData";
import { Message } from "../../../message/messageConstant";
import {
  faArrowDown,
  faArrowUp,
  faTrashAlt,
  faDownload,
  faCalendarCheck,
  faEdit,
  faEye,
  faPlusCircle,
} from "@fortawesome/free-solid-svg-icons";
import ReactTable from "react-table";
import { Permissions as Permission } from "../../../permissions/AppPermissions";
import PermissionProvider from "../../../permissions/PermissionProvider";

const { Option } = Select;

const openNotificationWithIcon = (type, description, flag) => {
  if (flag === 1) {
    ///////delete flag
    notification[type]({
      message: description,
      icon: (
        <FontAwesomeIcon
          className="fa-xs icon_Delete"
          icon={faTrashAlt}
        ></FontAwesomeIcon>
      ),
      description: "",
    });
  } else if (flag === 2) {
    //resend flag
    notification[type]({
      message: description,
      description: "",
      //  duration:20
    });
  } else if (flag === 3) {
    //extend expires in
    notification[type]({
      message: description,
      icon: (
        <FontAwesomeIcon
          className="fa-xs icon_gray"
          icon={faCalendarCheck}
        ></FontAwesomeIcon>
      ),
      description: "",
    });
  }
};

const setIsExpanded = (data, self) => {
  let Icons = self.state.Icons === true ? false : true;
  self.setState({ IsExpanded: data, Icons: Icons });
};

const NoDataConst = (props) => (
  <span className="table-nodata-style">
    No records found Please check your connection or change your filters{" "}
  </span>
);

//set open model property
const openModal = (id, self) => {
  self.setState({ isOpen: true, isDeleteID: id });
};

const handleTblDelete = (id, self) => {
  self.setState({ loading: true });
  APIRequest.getGetService(APIUrl.DELETE_MASTER_SPORTS + "/" + id)
    .then((response) => {
      self.setState({ loading: false });
      if (response.Data === true) {
        self.fetch(self.table.current.state);
        openNotificationWithIcon("success", "Record has been deleted", 1);
      }
    })
    .catch((error) => {
      self.setState({ loading: false });
    });
};

//Export label
const exportLabel = [
  { columnName: "S.No", value: "SerialNo" },
  { columnName: "Sport", value: "Sport" },
  { columnName: "Event", value: "SportEvent" },
  { columnName: "Round/ Level/ Set", value: "SportRound" },
  { columnName: "Facility", value: "Facility" },
  { columnName: "Created by", value: "CreatedBy" },
];

class ManageSport extends Component {
  constructor(props) {
    super(props);

    //
    // Reading user's permissions from PermissionProvider component
    //
    let permissions = props.getAppPermissions();

    this.state = {
      permissions: permissions,
      roles: [],

      users: [],
      pageSize: 10,
      selected: {},
      selectAll: 0,
      IsExpanded: false,
      Icons: false,
      value: [],

      loading: false,
      perPage: 1,
      totalpage: 10,
      total_pages: 10,
      Currentpage: 1,
      TotalPages: 0,
      TotalRecords: 0,
      TotalCount: 0,
      IncrementPage: 0,

      SportID: [],
      SportEventID: [],
      FacilityID: [],
      SportRoundID: [],
      CreatedBy: "",
      IsExportExcel: false,
      exportData: [],

      FilterSportID: [],
      FilterSportIDList: [],
      FilterSportEventID: [],
      FilterSportEventIDList: [],
      FilterRoundID: [],
      FilterRoundIDList: [],
      FilterFacilityID: [],
      FilterFacilityIDList: [],
      FilterCreatedBy: "",

      EditRoundID: [],
      EditRoundIDList: [],
      EditFacilityID: [],
      EditFacilityIDList: [],

      EditSportID: [],
      EditSport: "",
      EditSportEvent: "",
      EditFacility: "",

      deletebtn: true,
      exportbtn: true,
      sortColumn: "",
      sortValue: "",
      exportFilename: "Added Sport List",

      RecordAlreadyExists:"",

      isAddEditDatapopup: false,
      isEyeDatapopup: false,
      isAddSport: false,
      isEyeData: [],
      errors: {},
      Events: [],
      isOpen: false,
      isClose: true,
      isDeleteID: 0,
    };

    this.setRef = React.createRef();
    this.table = React.createRef();
    this.fetch = this.fetch.bind(this);
    this.toggleRow = this.toggleRow.bind(this);
    this.handleListClear = this.handleListClear.bind(this);
    this.handleOnchange = this.handleOnchange.bind(this);
    this.handleBtnDelete = this.handleBtnDelete.bind(this);
    this.tableColumnDataLimit = this.tableColumnDataLimit.bind(this);
  }

  tableColumnDataLimit = (str) => {
    if (str !== undefined && str !== null) {
      return str.length > 30 ? str.substring(0, 27) + "..." : str;
    }
  };

  handleBtnDelete = (event) => {
    var InputData = [];
    let self = this;
    //let result = 0
    let selectedRowIds = this.handlerGetListIDs(
      this.state.selected,
      this.state.users
    );
    if (selectedRowIds) {
      self.setState({
        loading: true,
      });
      InputData = selectedRowIds.toString();
      APIRequest.getGetService(APIUrl.DELETE_MASTER_SPORTS + "/" + InputData)
        .then((response) => {
          self.setState({
            loading: false,
          });
          if (response.Data === true) {
            openNotificationWithIcon("success", "Record has been deleted", 1);
            self.setState({ fields: [], rowcount: 0, selected: [] });
            self.fetch(this.table.current.state);
          }
        })
        .catch((error) => { });
    }
  };

  // It is used to start loader
  handlerLoadingStart = () => {
    this.setState({ loading: true });
  };
  // It is used to stop loader
  handlerLoadingStop = () => {
    this.setState({ loading: false });
  };
  handleOnchange = (name, val) => {
    let searchbtn = true;
    if (val !== "" && val !== undefined && val !== null && val.length !== 0) {
      searchbtn = false;
    }

    //update value based on onchange event for particular fields
    this.setState({
      ...this.state,
      [name]: val,
      isSearchBtn: searchbtn,
    });
  };

  handlerGetListIDs = (state, userList) => {
    if (state && userList) {
      let rowId = Object.keys(state);
      let selectedRowIds = userList
        .filter((x) => rowId.toString().includes(x.SerialNo))
        .map((x) => x.InvitedUserID);
      return selectedRowIds;
    } else return 0;
  };
  handleSportIDChange(SportID) {
    let searchbtn = true;
    let data = this.state;
    if (
      SportID.length !== 0 ||
      data.FilterSportEventID.length !== 0 ||
      data.FilterFacilityID.length !== 0 ||
      data.FilterCreatedBy !== ""
    ) {
      searchbtn = false;
    }
    this.setState({
      ...this.state,
      FilterSportID: SportID,
      isSearchBtn: searchbtn,
    });
    this.GamesSportIDEventChange(SportID);
  }
  handleSportEventIDChange(SportEventID) {
    let searchbtn = true;
    let data = this.state;
    if (
      data.FilterSportID.length !== 0 ||
      SportEventID.length !== 0 ||
      data.FilterFacilityID.length !== 0 ||
      data.FilterCreatedBy !== ""
    ) {
      searchbtn = false;
    }
    this.setState({
      ...this.state,
      FilterSportEventID: SportEventID,
      isSearchBtn: searchbtn,
    });
  }

  handleRoundIDChange(RoundID) {
    let searchbtn = true;
    let data = this.state;
    if (
      data.FilterSportID.length !== 0 ||
      data.FilterSportEventID.length !== 0 ||
      RoundID.length !== 0 ||
      data.FilterFacilityID.length !== 0 ||
      data.FilterCreatedBy !== ""
    ) {
      searchbtn = false;
    }
    this.setState({
      ...this.state,
      FilterRoundID: RoundID,
      isSearchBtn: searchbtn,
    });
  }
  handleFacilityIDChange(FacilityID) {
    let searchbtn = true;
    let data = this.state;
    if (
      data.FilterSportID.length !== 0 ||
      data.FilterSportEventID.length !== 0 ||
      FacilityID.length !== 0 ||
      data.FilterCreatedBy !== ""
    ) {
      searchbtn = false;
    }
    this.setState({
      ...this.state,
      FilterFacilityID: FacilityID,
      isSearchBtn: searchbtn,
    });
  }

  GamesSportIDEventChange(SportID) {
    APIRequest.getPostService(APIUrl.GET_SPORTS_EVENT_BY_SPORT + "/" + SportID)
      .then((response) => {
        if (response.Succeeded === true) {
          this.setState({
            FilterSportEventIDList: response.Data,
          });
        }
      })
      .catch((error) => { });
  }

  handleListClear(event) {
    this.setState(
      {
        FilterSportID: [],
        FilterSportEventID: [],
        FilterRoundID: [],
        FilterFacilityID: [],
        FilterCreatedBy: "",
        isSearchBtn: true,
      },
      function () {
        this.fetch(this.table.current.state);
      }
    );
  }

  openViewAlldataModal = (data, self) => {
    self.setState({ isEyeDatapopup: true, isAddSport: false, isEyeData: data });
  };

  openEditAlldataModal = (data, self) => {
    var EditSportRoundID = [];
    if (data.SportRoundID !== null) {
      const SportRoundIDs = data.SportRoundID.trim();
      if (SportRoundIDs.indexOf(",") !== -1) {
        let SportRoundIDs1 = SportRoundIDs.split(",");
        SportRoundIDs1.map((i) => EditSportRoundID.push(parseInt(i)));
      } else {
        EditSportRoundID.push(parseInt(SportRoundIDs));
      }
    }
    var EditFacilityID = [];
    if (data.FacilityID !== null) {
      const FacilityIDs = data.FacilityID.trim();
      if (FacilityIDs.indexOf(",") !== -1) {
        let FacilityIDs1 = FacilityIDs.split(",");
        FacilityIDs1.map((i) => EditFacilityID.push(parseInt(i)));
      } else {
        EditFacilityID.push(parseInt(FacilityIDs));
      }
    }
    const LstSportEvent = [];
    const SportEvent = data.SportEvent;
    if (isNaN(data.SportEvent)) {
      SportEvent.trim();
      if (SportEvent.indexOf(",") !== -1) {
        let SportEvent1 = SportEvent.split(", ")
        SportEvent1 = SportEvent1.map((i) => LstSportEvent.push(i))
      } else {
        LstSportEvent.push(SportEvent);
      }
    }
    self.setState({
      ...this.state,
      isAddEditDatapopup: true,
      EditSportID: data.SportID,
      EditSport: data.Sport,
      EditSportEvent: data.SportEvent,
      Events: LstSportEvent,
      EditRoundID: EditSportRoundID,
      EditFacilityID: EditFacilityID,
    });
  };

  viewAlldatapopupClose = () => {
    this.setState({ isEyeDatapopup: false, isEyeData: [], errors: {} });
    this.setState({
      isAddEditDatapopup: false,
      isAddSport: false,
      isEditData: [],
      errors: {},
    });
  };

  createUI() {
    return this.state.Events.map((el, i) => (
      <div key={i} className="row">
        <div className="col-md-11">
          <Input
            bordered={false}
            className="textfiled w-80"
            size="large"
            autoComplete="off"
            type="text"
            placeholder="Enter Event"
            value={el || ""}
            onChange={this.handleChange.bind(this, i)}
          />
        </div>
        {/* <Button className='mr-2 btn_green' value='remove' onClick={this.removeClick.bind(this, i)}> */}
        <div
          className="col-md-1 tabledata text-left"
          style={{
            display: "flex",
            justifyContent: "space-around",
            paddingTop: "10px",
          }}
        >
          <FontAwesomeIcon
            title="Remove"
            className="icon_Delete"
            onClick={this.removeClick.bind(this, i)}
            icon={faTrashAlt}
            style={{ cursor: "pointer" }}
          />
        </div>
        {/* Remove
				</Button> */}
      </div>
    ));
  }

  handleChange(i, event) {
    let values = [...this.state.Events];
    values[i] = event.target.value;
    let errors = {};
    //let isErrorExist = false
    if (this.state.Events.length !== 0) {
      this.state.Events.forEach((x) => {
        if (x.length <= 2) {
          // errors["SportEvent"] = "Please Enter valid Event";
          //isErrorExist = true
        }
      });
    }
    this.setState({ Events: values, errors: errors });
  }

  addClick() {
    let errors = {};
    let isErrorExist = false;
    if (this.state.Events.length !== 0) {
      this.state.Events.forEach((x) => {
        if (x.length <= 2) {
          errors["SportEvent"] = "Please Enter Valid Data";
          isErrorExist = true;
        }
      });
    }
    if (!isErrorExist) {
      this.setState((prevState) => ({ Events: [...prevState.Events, ""] }));
    } else {
      this.setState({ errors: errors });
    }
  }

  removeClick(i) {
    let values = [...this.state.Events];
    values.splice(i, 1);
    this.setState({ Events: values });
    openNotificationWithIcon("success", "Event has been deleted.", 1)
  }

  toggleRow(SerialNo) {
    const newSelected = Object.assign({}, this.state.selected);
    newSelected[SerialNo] = !this.state.selected[SerialNo];
    this.setState({
      selected: newSelected,
      selectAll: 2,
    });
    let flag = 0;
    for (const [value] of Object.entries(newSelected)) {
      if (value === true) {
        flag = 1;
      }
    }
    if (flag === 1) {
      this.setState({
        deletebtn: false,
      });
    } else {
      this.setState({
        deletebtn: true,
      });
    }
  }

  setRef = (ref) => (this.table = ref);

  handlerPagination = (pageNo) => {
    this.setState(
      {
        IncrementPage: pageNo,
      },
      function () { }
    );
    this.fetch(pageNo);
  };

  async fetch(state) {
    let sortbyname = "Sport";
    let orderby = "DESC";
    if (state.sorted !== undefined) {
      if (state.sorted.length !== 0) {
        sortbyname = state.sorted[0].id;
        if (state.sorted[0].desc === true) {
          orderby = "DESC";
        }
        if (state.sorted[0].desc === false) {
          orderby = "ASC";
        }
      }
    }
    let pagelimit = 0;
    let self = this;
    if (state.page === undefined || state.pageSize === undefined) {
      pagelimit = this.state.pageSize;
    } else {
      pagelimit = state.pageSize;
    }
    let data = {
      SportID: this.state.FilterSportID.toString(),
      SportEventID: this.state.FilterSportEventID.toString(),
      SportRoundID: this.state.FilterRoundID.toString(),
      FacilityID: this.state.FilterFacilityID.toString(),
      CreatedBy: this.state.FilterCreatedBy,
      PageNo: state > 0 ? state : 1,
      PageSize: pagelimit,
      SortColumn: sortbyname,
      SortOrder: orderby,
    };
   // self.setState({ loading: true });
    APIRequest.getPostService(APIUrl.GET_MANAGE_SPORT_LIST, data)
      .then((response) => {

        if (response.Data.length > 0) {
          const firstRow = response.Data.find((x) => x);
          self.setState({
            loading: false,
            users: response.Data,
            TotalPages: firstRow.TotalPages,
            TotalRecords: firstRow.TotalRows,
            TotalCount: response.Data.length >= 1 ? response.Data.length : 0,
            sortColumn: sortbyname,
            sortValue: orderby,
            exportbtn: false,
          });
        } else {
          self.setState({
            exportbtn: true,
          });
        }
        self.setState({ loading: false, users: response.Data });
      })
      .catch((error) => {
        self.setState({ loading: false });
      });
  }

  deleteModal = () => {
    if (this.state.isDeleteID === 0) this.handleBtnDelete();
    else handleTblDelete(this.state.isDeleteID, this);
    this.setState({ isDeleteID: 0, isOpen: false });
  };
  closeModal = () => {
    this.setState({ isOpen: false });
  };
  //update export excel flag as false
  componentDidUpdate() {
    if (this.state.IsExportExcel) {
      this.setState({
        IsExportExcel: false,
      });
    }
  }
  openAddSportModal = () => {
    this.setState({
      isAddEditDatapopup: true,
      isAddSport: true,
      EditFacilityID: [],
      EditRoundID: [],
      EditSport: "",
      EditSportEvent: "",
      Events: [],
    });
  };

  handleEditOnChange = (name, val) => {
    let errors = {};
    if (name === "EditSport") {
      if (val !== "" && val !== undefined && val !== null) {
        errors["Sport"] = "";
      }
      this.setState({
        EditSportEvent: "",
      });
    }

    if (name === "EditSportEvent") {
      if (val !== "" && val !== undefined && val !== null) {
        errors["SportEvent"] = "";
      }
      this.setState({
        EditSportEvent: "",
      });
    }

    if (name === "EditFacilityID") {
      // var FacilityID = []
      // const FacilityIDs = val.trim()
      // if (FacilityIDs.indexOf(",") != -1) {
      // 	let FacilityIDs1 = FacilityIDs.split(",")
      // 	FacilityIDs1.map((i) => FacilityID.push(parseInt(i)))
      // } else {
      // 	FacilityID.push(parseInt(FacilityIDs))
      // }
      // val = FacilityID
      if (val.length !== 0 && val !== undefined && val !== null) {
        errors["Facility"] = "";
      }
      this.setState({
        EditFacilityID: [],
      });
    }

    if (name === "EditRoundID") {
      // var RoundID = []
      // const RoundIDs = val.trim()
      // if (RoundIDs.indexOf(",") != -1) {
      // 	let RoundIDs1 = RoundIDs.split(",")
      // 	RoundIDs1.map((i) => FacilityID.push(parseInt(i)))
      // } else {
      // 	RoundID.push(parseInt(RoundIDs))
      // }
      // val = RoundID

      if (val.length !== 0 && val !== undefined && val !== null) {
        errors["Round"] = "";
      }
      this.setState({
        EditRoundID: [],
      });
    }

    this.setState({
      ...this.state,
      [name]: val,
      errors: errors,
    });
  };

  SportSubmit = () => {
    let isValid = this.handleFieldValidation();
    let self = this;
    let SportID = 0;
    if (self.state.isAddSport) {
      SportID = 0;
    } else {
      SportID = self.state.EditSportID;
    }
    if (!isValid) {
      let InputData = [
        {
          SportID: SportID,
          Sport: self.state.EditSport,
          SportEvent: self.state.Events.join(", "),
          RoundID: self.state.EditRoundID.toString(),
          FacilityID: self.state.EditFacilityID.toString(),
        },
      ];
      
      APIRequest.getPostService(APIUrl.ADD_MASTER_SPORTS, InputData)
        .then((response) => {
          if (response.Succeeded === true) {
            if (response.Data === "Already exists") {
              openNotificationWithIcon("success", "Sport already exists.", 3);
              self.setState({
                ...this.state, RecordAlreadyExists: "Record already exists." })
            } else {
              self.setState({
                ...this.state,
                loading: true,
                isAddEditDatapopup: false,
                isEditData: [],
                RecordAlreadyExists: "",
              });
              if(this.state.isAddSport)
                {
              openNotificationWithIcon("success", "Sport Added Successfully.", 2)
              }else{
                openNotificationWithIcon("success", "Sport Updated Successfully.", 2)
              }
               self.fetch(this.table.current.state);
            }
          }
        })
        .catch((error) => { });
    }
  };

  handleFieldValidation = () => {
    let isErrorExist = false;
    let errors = {};
    if (
      this.state.EditSport === "" ||
      this.state.EditSport === undefined ||
      this.state.EditSport === null
    ) {
      errors["Sport"] = "Please enter Sport";
      isErrorExist = true;
    }
    let SportEvent = JSON.stringify(this.state.Events);
    if (
      SportEvent === "" ||
      SportEvent === "[]" ||
      SportEvent === undefined ||
      SportEvent === null
    ) {
      errors["SportEvent"] = "Please enter Event";
      isErrorExist = true;
    }
    else
    {
      this.state.Events.every(function(i){
        if(i === null ||  i === "")
        {
          errors["SportEvent"] = "Please enter Event";
          isErrorExist = true;
      }
      else{
        if( i.length <= 3)
        {
          errors["SportEvent"] = "Please enter Valid Event";
          isErrorExist = true;
        }
      }
    })
    }
    // let HandleChange = this.addClick();
    // if (
    //   HandleChange === "" ||
    //   HandleChange === undefined ||
    //   HandleChange === null
    // ) {
    //   errors["SportEvent"] = "Please enter Event";
    //   isErrorExist = true;
    // }else{
    //   return HandleChange
    // }
    if (
      this.state.EditFacilityID.length === 0 ||
      this.state.EditFacilityID === undefined ||
      this.state.EditFacilityID === null
    ) {
      errors["Facility"] = "Please enter Facility";
      isErrorExist = true;
    }
    if (
      this.state.EditRoundID.length === 0 ||
      this.state.EditRoundID === undefined ||
      this.state.EditRoundID === null
    ) {
      errors["Round"] = "Please select Round/ Level/ Set";
      isErrorExist = true;
    }
    this.setState({ ...this.state, errors: errors });
    return isErrorExist;
  };

  async componentDidMount() {
    this.fetch(this.state);

    const Sports = await APIRequest.getGetService(APIUrl.GET_SPORT_LIST);
    this.setState({ FilterSportIDList: Sports.Data });

    const Round = await APIRequest.getGetService(APIUrl.GET_ROUND_FULL_LIST);
    this.setState({
      FilterRoundIDList: Round.Data,
      EditRoundIDList: Round.Data,
    });

    const Facility = await APIRequest.getGetService(
      APIUrl.GET_FACILITY_FULL_LIST
    );
    this.setState({
      FilterFacilityIDList: Facility.Data,
      EditFacilityIDList: Facility.Data,
    });
  }
  handlerExportData = () => {
    let self = this;
    let InputData = {
      SportID: self.state.FilterSportID.toString(),
      SportEventID: self.state.FilterSportEventID.toString(),
      SportRoundID: self.state.FilterRoundID.toString(),
      FacilityID: self.state.FilterFacilityID.toString(),
      CreatedBy: self.state.FilterCreatedBy,
      PageNo: 1,
      PageSize: self.state.TotalRecords,
      SortColumn: self.sortColumn,
      SortOrder: self.sortValue,
    };

    APIRequest.getPostService(APIUrl.GET_MANAGE_SPORT_LIST, InputData)
      .then((response) => {
        if (response.Data.length > 0) {
          self.setState({
            exportData: response.Data,
            IsExportExcel: true,
          });
        }
      })
      .catch((error) => { });
  };
  render() {
    let Permissions = this.state.permissions;

    const columns = [
      {
        Header: () => <div className="vertical_center">S.No</div>,
        accessor: "SerialNo",
        sortable: false,
        resizable: false,
        className: "tableheader wordwrap",
        headerClassName: "BoldText ColoredText",

        Cell: (row) => (
          <div className="tabledata" style={{ textAlign: "left" }}>
            {row.value}
          </div>
        ),
        width: Math.round(window.innerWidth * 0.04),
      },
      {
        Header: () => <div className="vertical_center">Sport</div>,
        width: Math.round(window.innerWidth * 0.1),
        accessor: "Sport",
        className: "tableheader wordwrap",
        resizable: false,
        style: { overflowWrap: "break-word" },
        Cell: (row) => {
          return (
            <div className="tabledata wordwrap" title={row.original.Sport}>
              {this.tableColumnDataLimit(row.original.Sport)}
            </div>
          );
        },
      },
      {
        Header: () => <div className="vertical_center">Event</div>,
        width: Math.round(window.innerWidth * 0.14),
        accessor: "SportEvent",
        filterable: false,
        className: "tableheader wordwrap",
        resizable: false,
        Cell: (row) => {
          return (
            <div className="tabledata" title={row.original.SportEvent}>
              {this.tableColumnDataLimit(row.original.SportEvent)}
            </div>
          );
        },
      },
      {
        Header: () => (
          <div className="vertical_center">Round/ Level/ Set</div>
        ),
        width: Math.round(window.innerWidth * 0.14),
        accessor: "SportRound",
        filterable: false,
        className: "tableheader wordwrap",
        resizable: false,
        Cell: (row) => {
          return (
            <div className="tabledata" title={row.original.SportRound}>
              {this.tableColumnDataLimit(row.original.SportRound)}
            </div>
          );
        },
      },
      {
        Header: () => <div className="vertical_center">Facility</div>,
        width: Math.round(window.innerWidth * 0.14),
        accessor: "Facility",
        className: "tableheader wordwrap",
        resizable: false,
        style: { overflowWrap: "break-word" },
        Cell: (row) => {
          return (
            <div className="tabledata wordwrap" title={row.original.Facility}>
              {this.tableColumnDataLimit(row.original.Facility)}
            </div>
          );
        },
      },
      {
        Header: () => <div className="vertical_center">Created By</div>,
        width: Math.round(window.innerWidth * 0.14),
        accessor: "CreatedBy",
        filterable: false,
        className: "tableheader wordwrap",
        resizable: false,
        Cell: (row) => {
          return (
            <div className="tabledata " title={row.original.CreatedBy}>
              {this.tableColumnDataLimit(row.original.CreatedBy)}
            </div>
          );
        },
      },
      {
        Header: () => (
          <div style={{ textAlign: "center" }}>{"     "}Action</div>
        ),
        accessor: "Action",
        className: "tableheader wordwrap",
        sortable: false,
        resizable: false,
        textAlign: "center",
        width: Math.round(window.innerWidth * 0.08),
        filterable: false,
        Cell: (row) => {
          return (
            <div
              className="tabledata text-left"
              style={{ display: "flex", justifyContent: "space-around" }}
            >
              {row.original.CreatedByID===row.original.CurrentUserID?<>
              {Permissions.includes(Permission.G_M_S_EDIT) ? (
                <FontAwesomeIcon
                  title="View All details"
                  className="mr-2 icon_eye"
                  onClick={() => this.openEditAlldataModal(row.original, this)}
                  icon={faEdit}
                  style={{ cursor: "pointer" }}
                  width="3rem"
                />
              ) : null}
              </>:null}
              {row.original.CreatedByID===row.original.CurrentUserID?<>
              {Permissions.includes(Permission.G_M_S_VIEW) ? (
                <FontAwesomeIcon
                  title="View"
                  className="icon_eye"
                  onClick={() => this.openViewAlldataModal(row.original, this)}
                  icon={faEye}
                  style={{ cursor: "pointer" }}
                  width="3rem"
                />
              ) : null}
              </>:null}
              {row.original.CreatedByID===row.original.CurrentUserID?<>
              {Permissions.includes(Permission.G_M_S_DELETE) ? (
                <FontAwesomeIcon
                  title="Delete"
                  className="mr-2 icon_Delete"
                  onClick={() => openModal(row.original.SportID, this)}
                  icon={faTrashAlt}
                  style={{ cursor: "pointer" }}
                  width="3rem"
                />
              ) : null}
              </>:null}
            </div>
          );
        },
      },
    ];
    return (
      <>
      <div className='h-full singletab' >
        {Permissions.includes(Permission.G_M_S_VIEW) ? (
          <Container fluid className="h-full">
            <Row className="h-full">
              <Col className="h-full">
                <div className=" pt-10 ">
                  <Spin
                    size="large"
                    spinning={this.state.loading}
                    tip="Loading..."
                  >
                    <div className='games_layout form-group mt-3 row col-sm-12 pr-0'>
                      <div  className='col-sm-6'>
                        <div className='d-flex justify-content-start  row'>
                        Manage Sport
                      </div>
                      </div>
                      
                     
                      {Permissions.includes(Permission.G_M_S_CREATE) ? (
                        <div className='col-sm-6 pr-0'>
                       <div className='d-flex justify-content-end row pr-0'>
                       <div className='tab_form_content games_layout'>
                          <Button
                            className="btn_green"
                            style={{
                              cursor: "pointer",
                            }}
                            onClick={this.openAddSportModal}
                          >
                            Add Sport
                          </Button>
                        
                          </div>
                          </div>
                          </div>
                      ) : null}
                      
                    </div>
                    <Card className="w-100 card from-group">
                      <Card.Header
                        className="text-white pl-1"
                        onClick={() =>
                          setIsExpanded(!this.state.IsExpanded, this)
                        }
                      >
                        <div className="float-left filter-heading-font">
                          Search Manage Sport{" "}
                        </div>
                        <div className="float-right">
                          {this.state.Icons ? (
                            <FontAwesomeIcon
                              color=" #FFFFFF"
                              className="action-icon-font"
                              icon={faArrowUp}
                              style={{ cursor: "pointer" }}
                            ></FontAwesomeIcon>
                          ) : (
                            <FontAwesomeIcon
                              color=" #FFFFFF"
                              className="action-icon-font"
                              icon={faArrowDown}
                              style={{ cursor: "pointer" }}
                            ></FontAwesomeIcon>
                          )}
                        </div>
                      </Card.Header>
                      <Collapse in={this.state.IsExpanded}>
                        <Card.Body>
                          <div className="row">
                            <div className="form-group col-sm-4">
                              <label>Sport</label>
                              <Select
                                showArrow={true}
                                showSearch={true}
                                allowClear
                                className="textfiled w-80"
                                mode="multiple"
                                maxTagCount="responsive"
                                // maxTagTextLength={26}
                                bordered={false}
                                placeholder="Select Sport"
                                name="Sport"
                                value={this.state.FilterSportID}
                                size={"large"}
                                optionFilterProp="children"
                                onChange={this.handleSportIDChange.bind(this)}
                                filterSort={(Sportdata, Sportvalue) =>
                                  Sportdata.children
                                    .toLowerCase()
                                    .localeCompare(
                                      Sportvalue.children.toLowerCase()
                                    )
                                }
                              >
                                {this.state.FilterSportIDList.map((c) => (
                                  <Option value={c.OptionID}>
                                    {c.OptionData}
                                  </Option>
                                ))}
                              </Select>
                            </div>
                            <div className="form-group col-sm-4">
                              <label>Event</label>
                              <Select
                                showArrow={true}
                                showSearch={true}
                                allowClear
                                className="textfiled w-80"
                                mode="multiple"
                                maxTagCount="responsive"
                                // maxTagTextLength={26}
                                bordered={false}
                                placeholder="Select Event"
                                name="SportEvent"
                                value={this.state.FilterSportEventID}
                                size={"large"}
                                optionFilterProp="children"
                                onChange={this.handleSportEventIDChange.bind(
                                  this
                                )}
                                filterSort={(SportEventdata, SportEventvalue) =>
                                  SportEventdata.children
                                    .toLowerCase()
                                    .localeCompare(
                                      SportEventvalue.children.toLowerCase()
                                    )
                                }
                              >
                                {this.state.FilterSportEventIDList.map((c) => (
                                  <Option value={c.OptionID}>
                                    {c.OptionData}
                                  </Option>
                                ))}
                              </Select>
                            </div>
                            <div className="form-group col-sm-4">
                              <label>Round/ Level/ Set</label>
                              <Select
                                showArrow={true}
                                showSearch={true}
                                allowClear
                                className="textfiled w-80"
                                mode="multiple"
                                maxTagCount="responsive"
                                bordered={false}
                                placeholder="Select Round/ Level/ Set"
                                name="Round"
                                value={this.state.FilterRoundID}
                                size={"large"}
                                optionFilterProp="children"
                                onChange={this.handleRoundIDChange.bind(this)}
                                filterSort={(Rounddata, Roundvalue) =>
                                  Rounddata.children
                                    .toLowerCase()
                                    .localeCompare(
                                      Roundvalue.children.toLowerCase()
                                    )
                                }
                              >
                                {this.state.FilterRoundIDList.map((c) => (
                                  <Option value={c.OptionID}>
                                    {c.OptionData}
                                  </Option>
                                ))}
                              </Select>
                            </div>
                            <div className="form-group col-sm-4">
                              <label>Facility</label>
                              <Select
                                showArrow={true}
                                showSearch={true}
                                allowClear
                                className="textfiled w-80"
                                mode="multiple"
                                maxTagCount="responsive"
                                bordered={false}
                                placeholder="Select Facility"
                                name="Facility"
                                value={this.state.FilterFacilityID}
                                size={"large"}
                                optionFilterProp="children"
                                onChange={this.handleFacilityIDChange.bind(
                                  this
                                )}
                                filterSort={(Facilitydata, Facilityvalue) =>
                                  Facilitydata.children
                                    .toLowerCase()
                                    .localeCompare(
                                      Facilityvalue.children.toLowerCase()
                                    )
                                }
                              >
                                {this.state.FilterFacilityIDList.map((c) => (
                                  <Option value={c.OptionID}>
                                    {c.OptionData}
                                  </Option>
                                ))}
                              </Select>
                            </div>
                            <div className="from-group col-sm-4">
                              <label>Created By</label>
                              <Input
                                size="large"
                                autoComplete="off"
                                type="text"
                                name="CreatedBy"
                                value={this.state.FilterCreatedBy}
                                onChange={(e) =>
                                  this.handleOnchange(
                                    "FilterCreatedBy",
                                    e.target.value
                                  )
                                }
                                bordered={false}
                                className="textfiled w-80"
                                placeholder="Created By"
                              ></Input>
                            </div>
                          </div>

                          <div className="col-sm-12 ">
                            <div className=" d-flex justify-content-end row">
                            <Button
                                onClick={this.handleListClear}
                                className="mr-2 btn_green"
                              >
                                Clear
                              </Button>
                              <Button
                                onClick={this.fetch}
                                className=" btn_green"
                                disabled={this.state.isSearchBtn}
                              >
                                Search
                              </Button>
                              
                            </div>
                          </div>
                        </Card.Body>
                      </Collapse>
                    </Card>
                    <div className="admin-report-table pt-5">
                      <ReactTable
                        data={this.state.users}
                        columns={columns}
                        showPaginationTop={false}
                        showPaginationBottom={false}
                        minRows={0}
                        defaultPageSize={10}
                        defaultPage={1}
                        onFetchData={this.fetch}
                        ref={this.table}
                        sortable={true}
                        multiSort={true}
                        className={"CosmicTable"}
                        showPageSizeOptions={false}
                        NoDataComponent={NoDataConst}
                        manual
                        defaultSorting={[
                          {
                            id: "id",
                            desc: true,
                          },
                        ]}
                        manualPagination={true}
                      />
                    </div>

                    <div className='mt-1'>
                      <label>
                        <b>Total Records: </b>
                        {this.state.TotalRecords > 10
                          ? this.state.TotalCount + " of " + this.state.TotalRecords
                          : this.state.TotalRecords !== 0
                            ? this.state.TotalRecords
                            : 0}
                      </label>
                    </div>

                    <div className="row">
                      <div className=" col-sm-4">
                        {Permissions.includes(Permission.G_M_S_EXPORT_EXCEL) ? (
                          <Button
                            className="mr-2 btn_green"
                            style={{
                              //paddingRight: "0.4rem", paddingLeft: "0.4rem",
                              cursor: "pointer",
                            }}
                            disabled={this.state.exportbtn}
                            onClick={this.handlerExportData}
                          >
                            <FontAwesomeIcon
                              title="Delete"
                              color={"#FFFFFF"}
                              icon={faDownload}
                            // marginRight={"5px !important"}
                            ></FontAwesomeIcon>{" "}
                            Export Excel
                          </Button>
                        ) : null}
                      </div>
                      <div className="col-sm-8 d-flex justify-content-end">
                        <Pagination
                          totalPages={this.state.TotalPages}
                          totalRecords={this.state.TotalRecords}
                          totalCount={this.state.TotalCount}
                          paginationCall={this.handlerPagination}
                        />
                      </div>
                    </div>
                    <Fragment>
                      <Modal
                        size="sm"
                        //aria-labelledby="contained-modal-title-vcenter"
                        backdrop="static"
                        //centered
                        show={this.state.isOpen}
                        onHide={this.closeModal}
                      >
                        <div className="popup-content">
                          <div className="deletecircle"></div>
                          <i className="fa fa-trash-o deleteicon"></i>
                          <Modal.Body>
                            <p
                              className="pull-left"
                              style={{ margin: "4rem 2rem" }}
                            >
                              {" "}
                              <h5>{Message.GAME_MASTERSPORT_DELETE}</h5>
                              <h6 style={{ color: "darkgray" }}>
                                {Message.GAME_MASTERSPORT_CONFIRM}
                              </h6>
                              <h6
                                style={{
                                  color: "darkgray",
                                  marginRight: "7rem",
                                  marginBottom: "-3rem",
                                }}
                              >
                                {Message.INVITE_DELETE_CONFIRM1}
                              </h6>
                            </p>
                            <br />
                            <div className="pull-right">
                              <Button
                                className="btn_cancel mr-2"
                                onClick={this.closeModal}
                              >
                                No
                              </Button>
                              <Button
                                className="btn_green"
                                style={{ height: "2rem", padding: "5px 10px" }}
                                onClick={this.deleteModal}
                              >
                                Yes
                              </Button>
                            </div>
                          </Modal.Body>
                        </div>
                      </Modal>
                    </Fragment>
                    {this.state.IsExportExcel ? (
                      <ExportData
                        data={this.state.exportData}
                        label={exportLabel}
                        filename={this.state.exportFilename}
                      />
                    ) : null}
                  </Spin>
                  <div>
                    <Modal
                      backdrop="static"
                      size="md"
                      className="popupborder"
                      keyboard={true}
                      show={this.state.isEyeDatapopup}
                      onHide={this.viewAlldatapopupClose}
                      aria-labelledby="contained-modal-title-vcenter"
                    >
                      <Modal.Header className="">
                        <div className="col-sm-12">
                          <div className="row">
                            <div className="col-sm-11">
                              <div className="d-flex justify-content-left">
                                <h5 className="font-weight-bold text-white mb-0">
                                  {"Manage Sport"}
                                </h5>
                              </div>
                            </div>
                            <div className="col-sm-1">
                              <div className="d-flex justify-content-end">
                                <button
                                  type="button"
                                  className="close text-white"
                                  onClick={this.viewAlldatapopupClose}
                                  data-dismiss="modal"
                                  aria-label="Close"
                                >
                                  <span aria-hidden="true">&times;</span>
                                </button>{" "}
                              </div>
                            </div>
                          </div>
                        </div>
                      </Modal.Header>
                      <Modal.Body className="">
                        <div className="col-sm-12">
                          <div className="row pt-10">
                            <div className="col-sm-1">{""}</div>
                            <div className="col-sm-10">
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  {" "}
                                  <label className="font-weight-bold popup_txt">
                                    Sport
                                  </label>{" "}
                                </div>
                                <div className="col-sm-1"> </div>
                                <div className="col-sm-7">
                                  {" "}
                                  <label>
                                    {this.state.isEyeData.Sport}
                                  </label>{" "}
                                </div>
                              </div>
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  {" "}
                                  <label className="font-weight-bold popup_txt">
                                    Event
                                  </label>{" "}
                                </div>
                                <div className="col-sm-1"> </div>
                                <div className="col-sm-7">
                                  {" "}
                                  <label>
                                    {this.state.isEyeData.SportEvent}
                                  </label>{" "}
                                </div>
                              </div>
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  {" "}
                                  <label className="font-weight-bold popup_txt">
                                    Round/ Level/ Set
                                  </label>{" "}
                                </div>
                                <div className="col-sm-1"> </div>
                                <div className="col-sm-7">
                                  {" "}
                                  <label>
                                    {this.state.isEyeData.SportRound}
                                  </label>{" "}
                                </div>
                              </div>
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  {" "}
                                  <label className="font-weight-bold popup_txt">
                                    Facility
                                  </label>{" "}
                                </div>
                                <div className="col-sm-1"> </div>
                                <div className="col-sm-7">
                                  {" "}
                                  <label>
                                    {this.state.isEyeData.Facility}
                                  </label>{" "}
                                </div>
                              </div>
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  {" "}
                                  <label className="font-weight-bold popup_txt">
                                    Created By{" "}
                                  </label>{" "}
                                </div>
                                <div className="col-sm-1"> </div>
                                <div className="col-sm-7">
                                  {" "}
                                  <label>
                                    {this.state.isEyeData.CreatedBy}
                                  </label>{" "}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        {/* <div className="d-flex justify-content-center">
                          <Button
                            className="cancel mr-2 btn_green"
                            onClick={this.viewAlldatapopupClose}
                          >
                            Cancel
                          </Button>
                        </div> */}
                      </Modal.Body>
                    </Modal>
                  </div>
                  <div>
                    <Modal
                      backdrop="static"
                      size="lg"
                      className="popupborder"
                      keyboard={true}
                      show={this.state.isAddEditDatapopup}
                      onHide={this.viewAlldatapopupClose}
                      aria-labelledby="contained-modal-title-vcenter"
                    >
                      <Modal.Header className="">
                        <div className="col-sm-12">
                          <div className="row">
                            <div className="col-sm-11">
                              <div className="d-flex justify-content-left">
                                {this.state.isAddSport ? (
                                  <h5 className="font-weight-bold text-white mb-0">
                                    {"Add Sport"}
                                  </h5>
                                ) : (
                                  <h5 className="font-weight-bold text-white mb-0">
                                    {"Edit Sport"}
                                  </h5>
                                )}
                              </div>
                            </div>
                            <div className="col-sm-1">
                              <div className="d-flex justify-content-end">
                                <button
                                  type="button"
                                  className="close text-white"
                                  onClick={this.viewAlldatapopupClose}
                                  data-dismiss="modal"
                                  aria-label="Close"
                                >
                                  <span aria-hidden="true">&times;</span>
                                </button>{" "}
                              </div>
                            </div>
                          </div>
                        </div>
                      </Modal.Header>
                      <Modal.Body className="">
                        <div className="col-sm-12">
                          <div className="row pt-10">
                            <div className="col-sm-1">{""}</div>
                            <div className="col-sm-10">
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  {" "}
                                  <label className="font-weight-bold popup_txt">
                                    Sport
                                  </label>
                                  <span className="text-danger">*</span>{" "}
                                </div>
                                <div className="col-sm-1"> </div>
                                <div className="col-sm-7 editSport">
                                  <Input
                                    className="textfiled w-80"
                                    size="large"
                                    autoComplete="off"
                                    type="text"
                                    name="EditSport"
                                    value={this.state.EditSport}
                                    onChange={(e) =>
                                      this.handleEditOnChange(
                                        "EditSport",
                                        e.target.value
                                      )
                                    }
                                    bordered={false}
                                    placeholder="Sport"
                                  ></Input>

                                  <span className="val-err-msg">
                                    {this.state.errors.Sport}
                                  </span>
                                </div>
                              </div>
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  {" "}
                                  <label className="font-weight-bold popup_txt">
                                    Event
                                  </label>
                                  <span className="text-danger">*</span>{" "}
                                </div>
                                <div className="col-sm-1"> </div>
                                <div className="col-sm-7 editSport">
                                  {this.createUI()}
                                  <Button
                                    onClick={this.addClick.bind(this)}
                                    className="btn-default border-0 p-0 m-0 bg-white link"
                                  >
                                    Add More &nbsp;
                                    <FontAwesomeIcon
                                      icon={faPlusCircle}
                                    ></FontAwesomeIcon>
                                  </Button>
                                  &nbsp;
                                  <span className="val-err-msg">
                                    {this.state.errors.SportEvent}
                                  </span>
                                </div>
                              </div>
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  {" "}
                                  <label className="font-weight-bold popup_txt">
                                    Round/ Level/ Set
                                  </label>
                                  <span className="text-danger">*</span>{" "}
                                </div>
                                <div className="col-sm-1"> </div>
                                <div className="col-sm-7 editSport">
                                  <Select
                                    showSearch={true}
                                    allowClear
                                    mode="multiple"
                                    className="textfiled w-80"
                                    maxTagCount="responsive"
                                    bordered={false}
                                    placeholder="Select Round/ Level/ Set"
                                    name="RoundID"
                                    value={this.state.EditRoundID}
                                    size={"large"}
                                    onChange={(val) =>
                                      this.handleEditOnChange(
                                        "EditRoundID",
                                        val
                                      )
                                    }
                                    optionFilterProp="children"
                                    filterSort={(Rounddata, Roundvalue) =>
                                      Rounddata.children
                                        .toLowerCase()
                                        .localeCompare(
                                          Roundvalue.children.toLowerCase()
                                        )
                                    }
                                  >
                                    {this.state.EditRoundIDList.map((c) => (
                                      <Option value={c.OptionID}>
                                        {c.OptionData}
                                      </Option>
                                    ))}
                                  </Select>
                                  <span className="val-err-msg">
                                    {this.state.errors.Round}
                                  </span>
                                </div>
                              </div>
                              <div className="form-group row">
                                <div className="col-sm-4">
                                  {" "}
                                  <label className="font-weight-bold popup_txt">
                                    Facility
                                  </label>
                                  <span className="text-danger">*</span>{" "}
                                </div>
                                <div className="col-sm-1"> </div>
                                <div className="col-sm-7 editSport">
                                  <Select
                                    showSearch={true}
                                    mode="multiple"
                                    allowClear
                                    className="textfiled w-80"
                                    maxTagCount="responsive"
                                    bordered={false}
                                    placeholder="Select Facility"
                                    name="FacilityID"
                                    value={this.state.EditFacilityID}
                                    size={"large"}
                                    onChange={(val) =>
                                      this.handleEditOnChange(
                                        "EditFacilityID",
                                        val
                                      )
                                    }
                                    optionFilterProp="children"
                                    filterSort={(Facilitydata, Facilityvalue) =>
                                      Facilitydata.children
                                        .toLowerCase()
                                        .localeCompare(
                                          Facilityvalue.children.toLowerCase()
                                        )
                                    }
                                  >
                                    {this.state.EditFacilityIDList.map((c) => (
                                      <Option value={c.OptionID}>
                                        {c.OptionData}
                                      </Option>
                                    ))}
                                  </Select>
                                  <span className="val-err-msg">
                                    {this.state.errors.Facility}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="d-flex justify-content-end mr-3">
                          <span className="val-err-msg">
                            {this.state.RecordAlreadyExists}
                          </span>
                          {/* <Button
                            className="cancel btn_green mr-2"
                            onClick={this.viewAlldatapopupClose}
                          >
                            Close
                          </Button> */}
                          {this.state.isAddSport ? (
                          <Button
                            className="btn_green"
                            disabled={this.state.isSubmitBtn}
                            onClick={this.SportSubmit}
                          >
                            Save
                          </Button> ) : (
                            <Button
                            className="btn_green"
                            disabled={this.state.isSubmitBtn}
                            onClick={this.SportSubmit}
                          >
                            Update
                          </Button> ) }
                          
                        </div>
                      </Modal.Body>
                    </Modal>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        ) : null}
        </div>
      </>
    );
  }
}

export default PermissionProvider(ManageSport);
