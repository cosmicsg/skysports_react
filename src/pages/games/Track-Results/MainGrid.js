import React, { Fragment, useState, useEffect, useRef, useImperativeHandle } from "react"
import ReactTable from "react-table"
import "../../../styles/CosmicTable.css"
import "antd/dist/antd.css"

import { Select, Input, Spin, notification } from "antd"
import { Button, Modal, Card, Collapse, Container } from "react-bootstrap"
import Pagination from "../../../utils/Pagination"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import { faArrowDown, faArrowUp, faTrashAlt, faDownload } from "@fortawesome/free-solid-svg-icons"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import ExportData from "../../../utils/ExportData"
import { Message } from "../../../message/messageConstant"
import { Permissions as Permission } from "../../../permissions/AppPermissions"
import { forwardRef } from "react"
import { SearchOutlined } from "@ant-design/icons"
import PermissionProvider from "../../../permissions/PermissionProvider"
//import ExportData from "../../Utilities/ExportData"
const { Option } = Select

const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	}
}

const NoDataConst = (props) => (
	<span className='table-nodata-style'>
		No records found Please check your connection or change your filters{" "}
	</span>
)

// const openViewAlldataModal = (data, self) => {
// 	let id = data.InvitedUserID

// 	let date = data.ExpiryDate
// 	var responseDate = moment(data.ExpiryDate).format("DD MMM YY")
// 	data.ExpiryDate = responseDate
// 	self.setState({ isEyeDatapopup: true, isEyeData: data })
// }

// function getRandomInt(max, min = 0) {

// 	return Math.floor(Math.random() * (max - min + 1)) + min; // eslint-disable-line no-mixed-operators
// }

export const UserInviteList = forwardRef((props, ref) => {
	let permissions = props.getAppPermissions()

	const [Permissions] = useState(permissions)
	const [listdata, setListdat] = useState({
		users: [],

		sportData: [],
		teamsData: [],
		gamesData: [],
		eventData: [],
	})

	const [inputData, setinputData] = useState({
		loading: true,
		pageSize: 10,
		selected: {},
		selectAll: 0,
		totalCount:0,
		IsExpanded: false,
		Icons: false,
		value: [],
		TotalPages: 0,
		TotalRecords: 0,
		IncrementPage: 0,
		isOpen: false,
		//Export excel properties
		IsExportExcel: false,
		exportData: [],
		sortColumn: "",
		sortValue: "",
		exportFilename: "Games Track Result List",
		exportbtn: true,
		isDeleteID: 0,
		event: [],
		isSearchBtn: true,
		exportLabel: [
			{ columnName: "S.No", value: "SerialNo" },
			{ columnName: "Players", value: "FullName" },
			{ columnName: "Game", value: "GameName" },
			{ columnName: "Sport", value: "SportName" },
			{ columnName: "Event", value: "SportEvent" },
			{ columnName: "Team", value: "TeamName" },
			{ columnName: "Department", value: "DepartmentName" },
			{ columnName: "Position", value: "Position" },
			{ columnName: "Score", value: "Score" },
		],
		teamsData: [],
		gamesData: [],
		sportdata: [],
		score: "",
		Position: "",
		AthleteID: 0,
		AthleteData: [],
		clearValue: false,
	})

	const handleTblDelete = async (id, self) => {
		setinputData((prev) => ({ ...prev, loading: true }))
		await APIRequest.getGetService(APIUrl.DELETE_GAME_TRACK_RESULT + "/" + id)
			.then((response) => {
				setinputData((prev) => ({ ...prev, loading: false }))
				if (response.Data === true) {
					fetch(reactTable.current.state)
					openNotificationWithIcon("success", "Record has been deleted", 1)
				}
			})
			.catch((error) => {
				setinputData((prev) => ({ ...prev, loading: false }))
			})
	}

	const openModal = (id) => {
		setinputData((inputData) => ({ ...inputData, isOpen: true, isDeleteID: id }))
	}
	const setIsExpanded = (data, self) => {
		let Icons = inputData.Icons === true ? false : true
		setinputData((inputDatas) => ({ ...inputDatas, IsExpanded: data, Icons: Icons }))
	}

	const tableColumnDataLimit = (str) => {
		return str.length > 25 ? str.substring(0, 22) + "..." : str
	}

	const handleBtnDelete = (event) => {
		var InputData = []
		let self = this
		//let result = 0
		let selectedRowIds = this.handlerGetListIDs(this.state.selected, this.state.users)
		if (selectedRowIds) {
			self.setState({
				loading: true,
			})
			InputData = selectedRowIds.toString()
			APIRequest.getGetService(APIUrl.DELETE_INACTIVE_USER + "/" + InputData)
				.then((response) => {
					self.setState({
						loading: false,
					})
					if (response.Data === true) {
						openNotificationWithIcon("success", "Record has been deleted", 1)
						self.setState({ fields: [], rowcount: 0, selected: [] })
						self.fetch(reactTable.current.useState)
					}
				})
				.catch((error) => {})
		}
	}
	// handlerGetListIDs = (state, userList) => {
	// 	if (state && userList) {
	// 		let rowId = Object.keys(state)
	// 		let selectedRowIds = userList
	// 			.filter((x) => rowId.toString().includes(x.SerialNo))
	// 			.map((x) => x.InvitedUserID)
	// 		return selectedRowIds
	// 	} else return 0
	// }

	const handleClear = (event) => {
		setinputData((prev) => ({
			...prev,
			teamsData: [],
			gamesData: [],
			sportdata: [],
			score: "",
			event: [],
			AthleteID: 0,
			AthleteData: [],
			isSearchBtn: true,
			clearValue: true,
		}))
	}
	useEffect(() => {
		if (inputData.clearValue === true) {
			fetch(0)
			setinputData((prev) => ({ ...prev, clearValue: false }))
		}
	}, [inputData.clearValue])
	const handleOverAllOnchange = async (label, val, lable) => {
		if (label === "score" && val === undefined) {
			val = ""
		} else if (label !== "score" && val === undefined) {
			val = []
		}

		if (label === "game") {
			setinputData((prev) => ({ ...prev, gamesData: val }))
		}
		if (label === "sport") {
			setinputData((prev) => ({ ...prev, sportdata: val }))
			if (val.toString() !== "") {
				await APIRequest.getPostService(
					APIUrl.GET_SPORTS_EVENT_BY_SPORT + "/" + val.toString()
				).then((response) => {
					if (response.Succeeded === true) {
						setinputData((prev) => ({ ...prev, event: [] }))
						setListdat((prev) => ({ ...prev, eventData: response.Data }))
					} else {
						setinputData((prev) => ({ ...prev, event: [] }))
						setListdat((prev) => ({ ...prev, eventData: [] }))
					}
				})
			} else {
				setinputData((prev) => ({ ...prev, event: [] }))
				setListdat((prev) => ({ ...prev, eventData: [] }))
			}
		}
		if (label === "team") {
			setinputData((prev) => ({ ...prev, teamsData: val }))
		}
		if (label === "score") {
			setinputData((prev) => ({ ...prev, score: val }))
		}
		if (label === "athlete") {
			setinputData((prev) => ({ ...prev, AthleteID: val }))
		}
		if (label === "event") {
			setinputData((prev) => ({ ...prev, event: val }))
		}
	}

	useEffect(() => {
		let data = true
		// Update the document title using the browser API
		if (
			inputData.gamesData.length !== 0 ||
			inputData.teamsData.length !== 0 ||
			inputData.sportdata.length !== 0 ||
			inputData.event.length !== 0 ||
			inputData.AthleteID !== 0 ||
			inputData.score !== ""
		) {
			data = false
		}
		setinputData((prev) => ({ ...prev, isSearchBtn: data }))
	}, [
		inputData.gamesData,
		inputData.teamsData,
		inputData.sportdata,
		inputData.AthleteID,
		inputData.score,
		inputData.event,
	])

	// setRef = (ref) => (this.table = ref)

	const reactTable = useRef(null)
	const handlerPagination = (pageNo) => {
		setinputData(
			(inputData) => ({
				...inputData,
				IncrementPage: pageNo,
				
			}),
			function () {}
		)
		fetch(pageNo)
	}
	const fetch = async (state) => {
		let sortbyname = "CreatedOn"
		let orderby = "DESC"
		if (state.sorted !== undefined) {
			if (state.sorted.length !== 0) {
				sortbyname = state.sorted[0].id
				if (state.sorted[0].desc === true) {
					orderby = "DESC"
				}
				if (state.sorted[0].desc === false) {
					orderby = "ASC"
				}
			}
		}

		let pagelimit = 0
		//let self = this
		if (state.page === undefined || state.pageSize === undefined) {
			pagelimit = inputData.pageSize
		} else {
			pagelimit = state.pageSize
		}
		let data = {
			PageNo: state > 0 ? state : 1,
			PageSize: pagelimit,
			SortColumn: sortbyname,
			SortOrder: orderby,
			SportID: inputData.sportdata.toString(),
			GamesTeamID: inputData.teamsData.toString(),
			Score: inputData.score,
			GameID: inputData.gamesData.toString(),
			AthleteID: inputData.AthleteID.toString(),
			EventId: inputData.event.toString(),
		}
		//		self.setState({ loading: true })
		APIRequest.getPostService(APIUrl.GET_TRACKRESULTS_MAIN_GRID, data)
			.then((response) => {
				if (response.Data.length > 0) {
					const firstRow = response.Data.find((x) => x)

					setinputData((inputData) => ({
						...inputData,
						TotalPages: firstRow.TotalPages,
						TotalRecords: firstRow.TotalRows,
						// TotalCount: inputData.length >= 1 ? inputData.length : 0,
						totalCount: response.Data.length >= 1 ? response.Data.length : 0,
						sortColumn: sortbyname,
						sortValue: orderby,
						exportbtn: false,
						loading: false,
					}))
					setListdat((listdata) => ({
						...listdata,
						users: response.Data,
					}))
				} else {
					setinputData((inputData) => ({
						...inputData,
						exportbtn: true,
						loading: false,
					}))
					setListdat((listdata) => ({
						...listdata,
						users: [],
					}))
				}
			})
			.catch((error) => {})
	}
	useEffect(() => {
		async function fetchData() {
			//let usertype = this.props.name
			const Sports = await APIRequest.getGetService(APIUrl.GET_SPORT_DROPDOWN_LIST)

			setListdat((formData1) => ({ ...formData1, sportData: Sports.Data }))
			const teamsData = await APIRequest.getGetService(APIUrl.GET_GAMES_TEAM)
			setListdat((ListData) => ({ ...ListData, teamsData: teamsData.Data }))
			const gamesData = await APIRequest.getGetService(APIUrl.GET_GAMES_DROPDOWN_LIST)
			setListdat((listdata) => ({ ...listdata, gamesData: gamesData.Data }))
		}
		fetchData()
	}, [])

	useImperativeHandle(ref, () => ({
		handlerRefreshInvitedList,
	}))

	const handlerRefreshInvitedList = () => {
		fetch(1)
	}

	const deleteModal = () => {
		if (inputData.isDeleteID === 0) handleBtnDelete()
		else handleTblDelete(inputData.isDeleteID)
		setinputData((inputData) => ({ ...inputData, isDeleteID: 0, isOpen: false }))
	}
	const closeModal = () => {
		setinputData((inputData) => ({ ...inputData, isOpen: false }))
	}
	const handlerExportData = () => {
		let InputData = {
			PageNo: 1,
			PageSize: inputData.TotalRecords,
			SortColumn: inputData.sortColumn,
			SortOrder: inputData.sortValue,
			SportID: inputData.sportdata.toString(),
			GamesTeamID: inputData.teamsData.toString(),
			Score: inputData.score,
			GameID: inputData.gamesData.toString(),
			AthleteID: inputData.AthleteID.toString(),
		}
		APIRequest.getPostService(APIUrl.GET_TRACKRESULTS_MAIN_GRID, InputData)
			.then((response) => {
				if (response.Data.length > 0) {
					setinputData((inputData) => ({
						...inputData,
						exportData: response.Data,
						IsExportExcel: true,
					}))
					setinputData((inputData) => ({
						...inputData,
						exportData: [],
						IsExportExcel: false,
					}))
				}
			})
			.catch((error) => {})
	}
	const [optionss, setOptions] = useState([])
	const searchResult = async (query) => {
		setOptions([])
		let dataList = []
		await APIRequest.getGetService(APIUrl.GET_GAME_TEAMS_USER_LIST + "/" + query).then(
			(response) => {
				if (response.Data.length !== 0) {
					response.Data.map((data) => {
						let userdata = {
							key: data.Value,
							value: data.Lable,
							label: (
								<div
									style={{
										display: "flex",
										justifyContent: "space-between",
									}}
								>
									<span>
										<img
											style={{
												border: 3,
												height: 25,
												width: 25,
												borderRadius: "50%", //,marginTop: "-4px",aspectRatio:135/76
											}}
											src={data.ImageData}
											alt='Team User'
										/>
										{data.Lable}{" "}
									</span>
								</div>
							),
						}
						dataList.push(userdata)
					})
				}
			}
		)
		setOptions(dataList)
	}

	const handleSearch = async (value) => {
		setOptions([])

		await searchResult(value)
	}

	const AutocompleteOnSelect = (value, data) => {
		let datas = inputData
		let searchbtns = true
		let keydata = 0
		let labelData = []
		if (data !== undefined) {
			keydata = data.key
			labelData = data
		}
		if (
			datas.gamesData.length !== 0 ||
			datas.teamsData.length !== 0 ||
			datas.sportdata.length !== 0 ||
			keydata !== 0 ||
			datas.score !== ""
		) {
			searchbtns = false
		}

		setinputData((prev) => ({
			...prev,
			AthleteData: labelData,
			AthleteID: keydata,
			isSearchBtn: searchbtns,
		}))
	}
	//update export excel flag as false
	// componentDidUpdate() {
	// 	if (this.state.IsExportExcel) {
	// 		this.setState({
	// 			IsExportExcel: false,
	// 		})
	// 	}
	// }

	const columns = [
		{
			Header: () => <div className='text-left'>S.No</div>,
			accessor: "SerialNo",
			sortable: false,
			resizable: false,
			className: "tableheader wordwrap",
			headerClassName: "BoldText ColoredText",

			Cell: (row) => <div className='tabledata text-left'>{row.value}</div>,
			width: Math.round(window.innerWidth * 0.04),
		},

		{
			Header: () => <div className='text-left'>Players</div>,

			resizable: false,
			accessor: "FullName",
			style: { overflowWrap: "break-word" },
			Cell: (row) => {
				return (
					<div className='tabledata wordwrap' title={row.original.FullName}>
						{tableColumnDataLimit(row.original.FullName)}
						{/* {row.original.FullName} */}
					</div>
				)
			},
		},

		{
			Header: () => <div className='text-left'> Game</div>,
			accessor: "GameName",
			filterable: false,
			width: Math.round(window.innerWidth * 0.08),
			className: "tableheader wordwrap",
			resizable: false,
			Cell: (row) => {
				return <div className='tabledata text-left'>{row.original.GameName}</div>
			},
		},
		{
			Header: () => <div className='text-left'>Sport</div>,

			accessor: "SportName",
			filterable: false,
			width: Math.round(window.innerWidth * 0.14),
			className: "tableheader wordwrap",
			resizable: false,
			Cell: (row) => {
				return <div className='tabledata'>{row.original.SportName}</div>
			},
		},
		{
			Header: () => <div className='text-left'>Event</div>,

			accessor: "SportEvent",
			filterable: false,
			width: Math.round(window.innerWidth * 0.14),
			className: "tableheader wordwrap",
			resizable: false,
			Cell: (row) => {
				return <div className='tabledata'>{row.original.SportEvent}</div>
			},
		},
		{
			Header: () => <div className='text-left'>Team Name</div>,

			accessor: "TeamName",
			filterable: false,
			width: Math.round(window.innerWidth * 0.14),
			className: "tableheader wordwrap",
			resizable: false,
			Cell: (row) => {
				return <div className='tabledata'>{row.original.TeamName}</div>
			},
		},

		{
			Header: () => <div className='text-left'>Score</div>,
			show: true,
			accessor: "Score",
			Cell: (row) => {
				return <div className='tabledata'>{row.original.Score}</div>
			},
		},
		{
			Header: () => <div className='text-center'>UserProfileID</div>,
			show: false,
			accessor: "UserProfileID",
			filterable: false,
			className: "tableheader wordwrap",
			resizable: false,
			width: Math.round(window.innerWidth * 0.07),
			Cell: (row) => {
				return <div className='tabledata  text-center'>{row.original.UserProfileID}</div>
			},
		},
		{
			Header: () => <div className=''>GamesSetupID</div>,
			accessor: "GamesSetupID",
			show: false,
		},
		{
			Header: () => <div className=' text-center'>{"     "}Action</div>,
			accessor: "Action",
			className: "tableheader wordwrap",
			sortable: false,
			resizable: false,
			width: Math.round(window.innerWidth * 0.07),
			filterable: false,
			Cell: (row) => {
				return (
					<div className='tabledata text-center'>
						{Permissions.includes(Permission.G_T_R_DELETE) ? (
							<FontAwesomeIcon
								title='Delete'
								className=' icon_Delete'
								onClick={() => openModal(row.original.GamesTreackresultID)}
								icon={faTrashAlt}
								style={{ cursor: "pointer" }}
							/>
						) : null}
					</div>
				)
			},
		},
	]

	return (
		<>
			{Permissions.includes(Permission.G_T_R_VIEW) ? (
				<div className=' pt-10'>
					<Container fluid>
						<Spin size='large' className='' spinning={inputData.loading} tip='Loading...'>
							<Card className='w-100 card from-group' style={{marginTop:"13px"}}>
								<Card.Header
									className='text-white'
									onClick={() => setIsExpanded(!inputData.IsExpanded, null)}
									
								>
									<div className='float-left filter-heading-font' style={{marginLeft:"-12px"}}>Search Track Results </div>
									<div className='float-right'>
										{inputData.Icons ? (
											<FontAwesomeIcon
												color=' #FFFFFF'
												className='action-icon-font'
												icon={faArrowUp}
												style={{ cursor: "pointer" }}
											></FontAwesomeIcon>
										) : (
											<FontAwesomeIcon
												color=' #FFFFFF'
												className='action-icon-font'
												icon={faArrowDown}
												style={{ cursor: "pointer" }}
											></FontAwesomeIcon>
										)}
									</div>
								</Card.Header>
								<Collapse in={inputData.IsExpanded}>
									<Card.Body>
										<div className='row'>
											<div className='form-group col-sm-4'>
												<label className='pl-0 mb-0'>Games</label>
												<div>
													<Select
														mode='multiple'
														maxTagCount={1}
														maxTagTextLength={15}
														size='large'
														name='Games'
														allowClear
														showSearch
														bordered={false}
														value={inputData.gamesData}
														onChange={(val) => handleOverAllOnchange("game", val)}
														optionFilterProp='children'
														className='textfiled  pl-0'
														placeholder='Select Games'
														filterSort={(optionA, optionB) =>
															optionA.children
																.toLowerCase()
																.localeCompare(optionB.children.toLowerCase())
														}
													>
														{listdata.gamesData.map((c) => (
															<Option value={c.OptionID}>{c.OptionData}</Option>
														))}
													</Select>
												</div>
											</div>

											<div className='form-group col-sm-4'>
												<label className='pl-0 mb-0'>Sport</label>
												<Select
													mode='multiple'
													maxTagCount={1}
													maxTagTextLength={15}
													className='textfiled '
													style={{ width: "100%" }}
													name='Sport'
													value={inputData.sportdata}
													bordered={false}
													size={"large"}
													allowClear
													showSearch
													placeholder='Select Sport'
													optionFilterProp='children'
													onChange={(val) => handleOverAllOnchange("sport", val)}
													filterSort={(optionA, optionB) =>
														optionA.children
															.toLowerCase()
															.localeCompare(optionB.children.toLowerCase())
													}
												>
													{listdata.sportData.map((c) => (
														<Option value={c.OptionID}>{c.OptionData}</Option>
													))}
													{/* {optionItems} */}
												</Select>
											</div>
											<div className='form-group col-sm-4'>
												<label className='pl-0 mb-0'>Event</label>
												<Select
													mode='multiple'
													maxTagCount={1}
													maxTagTextLength={15}
													className='textfiled '
													style={{ width: "100%" }}
													name='event'
													value={inputData.event}
													bordered={false}
													size={"large"}
													allowClear
													showSearch
													placeholder='Select Event'
													optionFilterProp='children'
													onChange={(val) => handleOverAllOnchange("event", val)}
													filterSort={(optionA, optionB) =>
														optionA.children
															.toLowerCase()
															.localeCompare(optionB.children.toLowerCase())
													}
												>
													{listdata.eventData.map((c) => (
														<Option value={c.OptionID}>{c.OptionData}</Option>
													))}
													{/* {optionItems} */}
												</Select>
											</div>
											<div className='form-group  col-sm-4'>
												<label className='pl-0 mb-0'>Team </label>
												<Select
													mode='multiple'
													className='textfiled '
													maxTagCount={1}
													maxTagTextLength={15}
													showArrow
													style={{ width: "100%" }}
													placeholder='Select Team'
													bordered={false}
													allowClear
													name='team'
													size={"large"}
													value={inputData.teamsData}
													onChange={(val) => handleOverAllOnchange("team", val)}
													optionFilterProp='children'
													filterSort={(optionA, optionB) =>
														optionA.children
															.toLowerCase()
															.localeCompare(optionB.children.toLowerCase())
													}
												>
													{listdata.teamsData.map((c) => (
														<Option value={c.OptionID}>{c.OptionData}</Option>
													))}
												</Select>
											</div>
											<div className='form-group  col-sm-4'>
												<label className='pl-0 mb-0'>Players </label>
												<Select
													dropdownMatchSelectWidth={252}
													className='textfiled w-80'
													autoComplete='off'
													options={optionss}
													suffixIcon={<SearchOutlined />}
													onSearch={handleSearch}
													onSelect={AutocompleteOnSelect}
													onChange={AutocompleteOnSelect}
													//onChange={(val,key)=>handleOverAllOnchange("athlete",key,val)}
													bordered={false}
													showSearch={true}
													labelInValue={true}
													allowClear
													backfill={false}
													value={inputData.AthleteData}
													placeholder='Enter the Players'
													filterOption={(inputValue, option) =>
														option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
													}
												>
													{/* <Input.Search size="large" placeholder="Search" enterButton></Input.Search> */}
												</Select>
											</div>
											<div className='form-group col-sm-4'>
												<label className='pl-0 mb-0'>Score</label>
												<div>
													<Input
														size='large'
														autoComplete='off'
														type='text'
														name='Score'
														allowClear
														value={inputData.score}
														bordered={false}
														onChange={(event) => handleOverAllOnchange("score", event.target.value)}
														//onChange={handleSetdata}
														className='textfiled  pl-0'
														placeholder='Score'
													></Input>
												</div>
											</div>
										</div>
										<div className='col-sm-12 '>
											<div className=' d-flex justify-content-end row'>
											<Button onClick={() => handleClear()} className='mr-2 btn_green'>
													Clear
												</Button>
												<Button
													onClick={() => fetch(0)}
													className=' btn_green'
													disabled={inputData.isSearchBtn}
												>
													Search
												</Button>
												
											</div>
										</div>
									</Card.Body>
								</Collapse>
							</Card>

							<div className='admin-report-table pt-3'>
								<ReactTable
									data={listdata.users}
									columns={columns}
									showPaginationTop={false}
									showPaginationBottom={false}
									minRows={0}
									defaultPageSize={10}
									defaultPage={1}
									onFetchData={fetch}
									ref={reactTable}
									sortable={true}
									multiSort={true}
									manual
									className={"CosmicTable"}
									showPageSizeOptions={false}
									NoDataComponent={NoDataConst}
									defaultSorting={[
										{
											id: "id",
											desc: true,
										},
									]}
									manualPagination={true}
								/>
							</div>

							<div className='mt-1'>
								<label>
								<b >Total Records: </b>
								{inputData.TotalRecords > 10
									? inputData.totalCount + " of " + inputData.TotalRecords
									: inputData.TotalRecords !== 0
									? inputData.TotalRecords
									: 0}
								</label>
							</div>
							<div className="row">
        					<div  className='col-sm-4'>
									{Permissions.includes(Permission.G_T_R_EXPORT_EXCEL) ? (
										<Button
											className='mr-2 btn_green'
											style={{
												//paddingRight: "0.4rem", paddingLeft: "0.4rem",
												cursor: "pointer",
											}}
											disabled={inputData.exportbtn}
											onClick={handlerExportData}
										>
											<FontAwesomeIcon
												title='Delete'
												color={"#FFFFFF"}
												icon={faDownload}
												// marginRight={"5px !important"}
											>
												{" "}
											</FontAwesomeIcon>{" "}
											Export Excel
										</Button>
									) : null}
								</div>
								
								
								<div className="col-sm-8 d-flex justify-content-end">
									<Pagination
										totalPages={inputData.TotalPages}
										totalRecords={inputData.TotalRecords}
										paginationCall={handlerPagination}
									/>
								</div>
							
							</div>
							<Fragment>
								<Modal
									size='sm'
									//aria-labelledby="contained-modal-title-vcenter"
									//   aria-labelledby="contained-modal-title-vcenter"
									backdrop='static'
									//  centered
									show={inputData.isOpen}
									onHide={closeModal}
								>
									<div className='popup-content'>
										<div className='deletecircle'></div>
										<i className='fa fa-trash-o deleteicon'></i>
										<Modal.Body>
											<div className='pull-left' style={{ margin: "4rem 2rem" }}>
												{" "}
												<h5>{Message.TRACK_RESULTS_DELETE_HEADING}</h5>
												<h6 style={{ color: "darkgray" }}>{Message.TRACK_RESULTS_DELETE_BODY}</h6>
												<h6
													style={{
														color: "darkgray",
														marginRight: "7rem",
														marginBottom: "-3rem",
													}}
												>
													{Message.INVITE_DELETE_CONFIRM1}
												</h6>
											</div>
											<br />
											<div className='pull-right'>
											<Button className='btn_cancel mr-2' onClick={closeModal}>
													No
												</Button>
												<Button
													className='btn_green'
													style={{ height: "2rem", padding: "5px 10px" }}
													onClick={deleteModal}
												>
													Yes
												</Button>
												
											</div>
										</Modal.Body>
									</div>
								</Modal>
							</Fragment>
							{inputData.IsExportExcel ? (
								<ExportData
									data={inputData.exportData}
									label={inputData.exportLabel}
									filename={inputData.exportFilename}
								/>
							) : null}
						</Spin>
					</Container>
				</div>
			) : null}{" "}
		</>
	)
})

export default PermissionProvider(UserInviteList)
