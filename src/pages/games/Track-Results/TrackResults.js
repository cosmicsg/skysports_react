import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Input, notification, Select, Upload, Button, Spin } from "antd"
import React, { useState, useEffect, useRef, forwardRef } from "react"
import { Container, Modal } from "react-bootstrap"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import MainGrid from "./MainGrid"
import { faCloudUploadAlt } from "@fortawesome/free-solid-svg-icons"
import { Permissions as Permission } from "../../../permissions/AppPermissions"
import PermissionProvider from "../../../permissions/PermissionProvider"
const { Option } = Select

//notification properties
const openNotification = (type, desc, flag) => {
	if (flag === 1) {
		/// save msg
		notification[type]({
			message: desc,

			//description: "",
		})
	}
	if (flag === 2) {
		notification[type]({
			message: desc,
			//duration:50
			//description: "",
		})
	}
}
const { Dragger } = Upload

//Trackresults hooks component
const Trackresults = (props) => {

	let userPermissions = props.getAppPermissions();

	const parentRef = React.useRef(null);
		
	const [Permissions] = useState(userPermissions)

	const allowedFiles = [
		"xlsb",
		"xlsm",
		"xlsx",
		"xltx",
		"xltm",
		"xls",
		"xlt",
		"xls",
		"xml",
		"xlam",
		"xla",
		"xlw",
		"xlr",
		"csv",
	]
	//const [list, setList] = useState([])
	//ChildRef
	const childRef = React.useRef(null)

	const [ListData, setListData] = useState({
		gamesData: [],
		sportDateData: [],
		sportLevelData: [],
		sportsData: [],
		facilityData: [],
		sporttimeslotData: [],
		teamData: [],
		UsersData: [],
		departmentData: [],
		eventData: [],
	})
	const [TrackResultFields, setTrackResultFields] = useState({
		game: [],
		sportDate: [],
		sportLevel: [],
		sport: [],
		facility: [],
		sporttimeslot: [],
		participantType: [],
		teamName: [],
		addResultPopUp: false,
		addUploadResultPopUp: false,
		Users: [],
		score: "",
		deparment: [],
		GamesParticipantID: [],
		position:"",
		loading: false,
		event: [],
		headerName: "",
		uploadbtn: true,
		uploadbtnFlag: false,
	})
	const [Cleardata] = useState({
		game: [],
		sportDate: [],
		sportLevel: [],
		sport: [],
		facility: [],
		sporttimeslot: [],
		participantType: [],
		teamName: [],
		addResultPopUp: false,
		addUploadResultPopUp: false,
		Users: [],
		score: "",
		position:"",
		deparment: [],
		GamesParticipantID: [],
		loading: false,
		event: [],
		headerName: "",
		uploadbtn: true,
		uploadbtnFlag: false,
	})
	const [uploadList, setuploadList] = useState([])
	//const [availaleUploadList, setAvailaleUploadList] = useState([]);
	//error object declaration
	const [errors, setErrors] = useState({})
	useEffect(() => {
		//Getting games dropdown value
		APIRequest.getGetService(APIUrl.GET_GAMES_DROPDOWN_LIST).then((res) => {
			if (res.Succeeded) {
				setListData((prevState) => ({
					...prevState,
					gamesData: res.Data,
				}))
			}
		})
	}, [])
	const handleOverallChange = async (name, value, key, listData) => {
		
		if (name === "game") {
			
			setTrackResultFields((TrackResultFields) => ({
				...TrackResultFields,
				game: value,
				event: [],
				sportDate: [],
				sportLevel: [],
				sport: [],
				facility: [],
				sporttimeslot: [],
				participantType: [],
				teamName: [],
				Users: [],
				score: "",
				deparment: [],
			}))
			setListData((prevState) => ({
				...prevState,
				sportDateData: [],
				sportLevelData: [],
				facilityData: [],
				sporttimeslotData: [],
				teamData: [],
				sportsData: listData,
				departmentData: [],
				eventData: [],
			}))
		}

		if (name === "sport") {
			
			setTrackResultFields((TrackResultFields) => ({
				...TrackResultFields,
				sport: value,
				event: [],
				sportDate: [],
				sportLevel: [],
				facility: [],
				sporttimeslot: [],
				participantType: [],
				teamName: [],
				Users: [],
				score: "",
				GamesParticipantID: [],
			}))
			setListData((ListData) => ({
				...ListData,
				sportDateData: [],
				facilityData: [],
				sporttimeslotData: [],
				teamData: [],
				sportLevelData: [],
				eventData: listData,
			}))
		}
		if (name === "event") {
			
			setTrackResultFields((TrackResultFields) => ({
				...TrackResultFields,
				event: value,
				sportDate: [],
				sportLevel: [],
				facility: [],
				sporttimeslot: [],
				participantType: [],
				teamName: [],
				Users: [],
				score: "",
				GamesParticipantID: [],
			}))
			setListData((ListData) => ({
				...ListData,
				sportDateData: [],
				facilityData: [],
				sporttimeslotData: [],
				teamData: [],
				sportLevelData: listData,
			}))
		}
		if (name === "sportLevel") {
			
			setTrackResultFields((TrackResultFields) => ({
				...TrackResultFields,
				sportLevel: value,
				sportDate: [],
				facility: [],
				sporttimeslot: [],
				participantType: [],
				teamName: [],
				Users: [],
				score: "",
				GamesParticipantID: [],
			}))
			setListData((ListData) => ({
				...ListData,
				sportDateData: [],
				sporttimeslotData: [],
				teamData: [],
				facilityData: listData,
			}))
		}
		if (name === "facility") {
			
			setTrackResultFields((TrackResultFields) => ({
				...TrackResultFields,
				facility: value,
				sportDate: [],
				sporttimeslot: [],
				participantType: [],
				teamName: [],
				Users: [],
				score: "",
				GamesParticipantID: [],
			}))
			setListData((ListData) => ({
				...ListData,
				sporttimeslotData: [],
				teamData: [],
				sportDateData: listData,
			}))
		}
		if (name === "sportDate") {
			
			setTrackResultFields((TrackResultFields) => ({
				...TrackResultFields,
				sportDate: value,
				sporttimeslot: [],
				participantType: [],
				teamName: [],
				Users: [],
				score: "",
				GamesParticipantID: [],
			}))
			setListData({ ...ListData, sporttimeslotData: listData, teamData: [] })
		}
		if (name === "sporttimeslot") {
			setTrackResultFields({
				...TrackResultFields,
				sporttimeslot: value,
				participantType: [],
				teamName: [],
				Users: [],
				score: "",
				GamesParticipantID: [],
			})
			setListData({ ...ListData, teamData: listData })
		}
		if (name === "participantType") {
			setTrackResultFields({
				...TrackResultFields,
				participantType: value,
				teamName: [],
				Users: [],
				score: "",
				GamesParticipantID: [],
			})
			setListData({ ...ListData, UsersData: listData })
		}
		if (name === "team") {
			setTrackResultFields({
				...TrackResultFields,
				teamName: value,
				Users: [],
				score: "",
				GamesParticipantID: [],
			})
			setListData({ ...ListData, UsersData: listData })
		}
		if (name === "Users") {
			setTrackResultFields({
				...TrackResultFields,
				Users: value,
				position: "",
				GamesParticipantID: key,
			})
			//setListData({ ...ListData, UsersData: listData })
		}
		if (name === "position") {
			setTrackResultFields({ ...TrackResultFields,position:value, score: "" })
			//setListData({ ...ListData, UsersData: listData })
		}
		if (name === "score") {
			setTrackResultFields({ ...TrackResultFields, score: value })
			//setListData({ ...ListData, UsersData: listData })
		}
	}

	const handleGameOnchange = async (val, data) => {
		//update value based on onchange event for particular fields
		let values = []
		let listData = []
		if (val !== undefined) {
			values = val
			const Sports = await APIRequest.getGetService(APIUrl.GET_GAMES_SPORT_LIST + "/" + val)

			// setTrackResultFields((Prev)=>({
			//     ...Prev,exportFilename:data.children
			// }))
			listData = Sports.Data
		}
		handleOverallChange("game", values, 0, listData)
	}
	const handleTimeslotOnchange = async (val) => {
		let values = []
		let listData = []
		if (val !== undefined) {
			values = val
			const Team = await APIRequest.getGetService(
				APIUrl.GET_GAME_SPORT_TEAM_LIST +
					"/" +
					TrackResultFields.game +
					"/" +
					TrackResultFields.sport +
					"/0"
			)

			listData = Team.Data
		}
		handleOverallChange("sporttimeslot", values, 0, listData)
	}
	const handleParticipantTypeOnchange = async (val) => {
		let values = []
		let listData = []
		if (val !== undefined) {
			values = val
			if (val === 2) {
				let inputdata = {
					TeamID: "0",
					SportID:
						TrackResultFields.sport.toString() === "" ? "0" : TrackResultFields.sport.toString(),
					SportRoundID:
						TrackResultFields.sportLevel.toString() === ""
							? "0"
							: TrackResultFields.sportLevel.toString(),
					SportSlotID:
						TrackResultFields.sporttimeslot.toString() === ""
							? "0"
							: TrackResultFields.sporttimeslot.toString(),
					FacilityID:
						TrackResultFields.facility.toString() === ""
							? "0"
							: TrackResultFields.facility.toString(),
					GamesSetupID:
						TrackResultFields.game.toString() === "" ? "0" : TrackResultFields.game.toString(),
					EventID:
						TrackResultFields.event.toString() === "" ? "0" : TrackResultFields.event.toString(),
					Name: "",
				}
				const UserData = await APIRequest.getPostService(APIUrl.GET_GAME_TEAM_USER_LIST, inputdata)

				listData = UserData.Data
			}
		}
		handleOverallChange("participantType", values, 0, listData)
	}
	const handlePositionOnchange = async (val) => {
		let values = []
		let listData = []
		
		if (val !== undefined) {
			var numbers = /^[0-9]+$/;
			if(val.match(numbers))
			{
				values = val
			}
			
		}
		handleOverallChange("position", values, 0, listData)
	}
	const handleScoreOnchange = async (val) => {
		let values = []
		let listData = []
		if (val !== undefined) {
			values = val
		}
		handleOverallChange("score", values, 0, listData)
	}
	const handleTeamOnchange = async (val) => {
		let values = []
		let listData = []
		if (val !== undefined) {
			values = val
			let inputdata = {
				TeamID: val.toString(),
				Name: "",
			}
			const UserData = await APIRequest.getPostService(APIUrl.GET_GAME_TEAM_USER_LIST, inputdata)

			listData = UserData.Data
		}
		handleOverallChange("team", values, 0, listData)
	}

	const handleUserOnChange = async (val, data) => {
		let values = []
		let listData = []
		let key = []
		if (data !== undefined) {
			values = val
			let keydata = data[0].key.split("--")
			key = keydata[0]
		}
		handleOverallChange("Users", values, key, listData)
	}

	const LevelMasterData = async (Sportsid) => {
		let result = []
		await APIRequest.getGetService(APIUrl.GET_GAMES_SPORT_ROUND + "/" + Sportsid)
			.then((response) => {
				if (response.Succeeded === true) {
					result = response.Data
					// setListData((ListData)=>({
					//     ...ListData,
					//     sportLevelData: response.Data,
					// }));
				}
				return result
			})
			.catch((error) => {})

		return result
	}
	const handelEventOnchange = async (event) => {
		let values = []
		let listData = []
		if (event !== undefined) {
			values = event
			listData = await LevelMasterData(TrackResultFields.sport)
		}
		handleOverallChange("event", values, 0, listData)
	}
	const handelSportRoundOnchange = async (round) => {
		let values = []
		let listData = []
		if (round !== undefined) {
			values = round
			await APIRequest.getGetService(
				APIUrl.GET_GAMES_SPORT_FACILITY + "/" + TrackResultFields.sport
			)
				.then((response) => {
					if (response.Succeeded === true) {
						listData = response.Data
					}
				})
				.catch((error) => {})
		}
		handleOverallChange("sportLevel", values, 0, listData)
	}
	const handleDateOnchange = async (sportDate) => {
		let values = []
		let listData = []
		if (sportDate !== undefined) {
			values = sportDate
			await APIRequest.getGetService(APIUrl.GET_GAMES_SPORT_SLOT + "/" + TrackResultFields.sport)
				.then((response) => {
					if (response.Succeeded === true) {
						listData = response.Data
					}
				})
				.catch((error) => {})
		}
		handleOverallChange("sportDate", values, 0, listData)
	}
	const FacilityChange = async (Facilityid) => {
		let values = []
		let listData = []
		if (Facilityid !== undefined) {
			values = Facilityid
			let Sportsid = TrackResultFields.sport
			await APIRequest.getGetService(APIUrl.GET_GAMES_SPORT_FACILITY_DATE + "/" + Sportsid)
				.then((response) => {
					if (response.Succeeded === true) {
						listData = response.Data
					}
				})
				.catch((error) => {})
		}

		handleOverallChange("facility", values, 0, listData)
	}
	const SportOnChange = async (Sportsid) => {
		let values = []
		let result = []
		if (Sportsid !== undefined) {
			values = Sportsid
			await APIRequest.getGetService(
				APIUrl.GET_SPORT_EVENT_DROPDOWN_LIST + "/" + Sportsid.toString()
			).then((response) => {
				if (response.Succeeded === true) {
					result = response.Data
				}
			})
		}
		handleOverallChange("sport", values, 0, result)
	}
	const handlerDownload = async () => {
		var validation = AddresultVaidation()
		if (validation === false) {
			let InputData = {
				GamesSetupID:
					TrackResultFields.game.toString() === "" ? "0" : TrackResultFields.game.toString(),
				SportID:
					TrackResultFields.sport.toString() === "" ? "0" : TrackResultFields.sport.toString(),
				FacilityID:
					TrackResultFields.facility.toString() === ""
						? "0"
						: TrackResultFields.facility.toString(),
				SportSlotID:
					TrackResultFields.sporttimeslot.toString() === ""
						? "0"
						: TrackResultFields.facility.toString(),
				SportRoundID:
					TrackResultFields.sportLevel.toString() === ""
						? "0"
						: TrackResultFields.sportLevel.toString(),
				TeamID:
					TrackResultFields.teamName.toString() === ""
						? "0"
						: TrackResultFields.teamName.toString(), //"57",
				DepartmentID:
					TrackResultFields.deparment.toString() === ""
						? "0"
						: TrackResultFields.deparment.toString(), // "1"
			}
			setTrackResultFields((prev) => ({ ...prev, loading: true }))
			await APIRequest.getPostService(APIUrl.GET_BLUG_UPLOAD_GAME_TRACK_MASTER_DATA, InputData)
				.then((response) => {
					setTrackResultFields((prev) => ({ ...prev, loading: false }))

					if (response.FileID !== 0) {
						var temp =
							"data:application/vnd.ms-excel;base64," + encodeURIComponent(response.FileData)
						var download = document.createElement("a")
						download.href = temp
						download.download = response.FileName + ".xlsx"
						document.body.appendChild(download)
						download.click()
						document.body.removeChild(download)

						if (response.FileID !== 0) {
							let participentid = response.FileID
							setTrackResultFields((inputData) => ({
								...inputData,
								//  exportData: response.Data,
								// IsExportExcel: true,
								uploadbtn: false,
								uploadbtnFlag: true,
								GamesParticipantID: participentid,
							}))
						}
					}
				})
				.catch((error) => {
					setTrackResultFields((prev) => ({ ...prev, loading: false }))
				})
		} else {
			setTrackResultFields((prev) => ({ ...prev, loading: false }))
			openNotification(
				"error",
				"One or more mandatory fields have an error. Please check and try again",
				2
			)
		}
	}
	//     useEffect(()=>{
	//         if(TrackResultFields.IsExportExcel==true){
	//             setTrackResultFields((prev)=>({...prev,IsExportExcel:false}))
	//         }

	//     },TrackResultFields.IsExportExcel)
	const config = {
		name: "file",
		multiple: true,
		showUploadList: true,
		fileList: uploadList.reverse(),
		accept:
			"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel",

		onChange(info) {
			const { status } = info.file
			if (status === "removed") {
				//"uploading"

				let uploadLists = uploadList.filter((x) => x.uid !== info.file.uid)
				setuploadList(uploadLists)
			}
			if (status === "done") {
				//props.onFileAdded(info.fileList)
			} else if (status === "error") {
				openNotification("error", `${info.file.name} file upload failed.`, 2)
			}
		},
		beforeUpload: (file) => {
			let filetype = file.name
			filetype = filetype.split(".")
			if (allowedFiles.includes(filetype[1])) {
				let uploadLists = uploadList
				uploadLists.unshift(file)
				return false
			}
		},
		progress: {
			strokeColor: {
				"0%": "#108ee9",
				"100%": "#87d068",
			},
			strokeWidth: 3,
			format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
		},
	}
	const uploadData = async (e) => {
		//e.preventDefault();
		let formData = new FormData()
		if (uploadList.length > 0) {
			let files = uploadList
			for (let i = 0; i < files.length; i++) {
				formData.append("files", files[i], files[i].name)
			}

			var validation = AddresultVaidation()
			if (validation === false) {
				if (TrackResultFields.GamesParticipantID.toString() !== "") {
					setTrackResultFields((prev) => ({ ...prev, loading: true }))
					await APIRequest.getPostService(
						APIUrl.UPLOAD_GAME_TRACK_RESULT + "/" + TrackResultFields.GamesParticipantID,
						formData
					)
						.then((response) => {
							setTrackResultFields((prev) => ({ ...prev, loading: false }))
							if (response.Succeeded === true) {
								//listData=response.Data;

								//   let uploadList = uploadList;
								//   var availaleUploadList = availaleUploadList;

								//   let data = availaleUploadList.filter(o1 => !uploadList.some(o2 => o1 === o2));
								setTrackResultFields({ ...Cleardata })
								openNotification("success", "Upload successfully", 1)
								setuploadList([])
								// setAvailaleUploadList(data);
							}
						})
						.catch((error) => {
							setTrackResultFields((prev) => ({ ...prev, loading: false }))
						})
				} else {
					setTrackResultFields((prev) => ({ ...prev, loading: false }))
					openNotification("error", "Please  file  download here", 2)
				}
			} else {
				setTrackResultFields((prev) => ({ ...prev, loading: false }))
				openNotification(
					"error",
					"One or more mandatory fields have an error. Please check and try again",
					2
				)
			}
		} else {
			setTrackResultFields((prev) => ({ ...prev, loading: false }))
			openNotification("error", "Please Select file or drog & drop file here", 2)
		}
	}
	const AddresultVaidation = () => {
		let result = false
		let error = {}
		if (TrackResultFields.game.toString() === "") {
			result = true
			error["game"] = "Please Select Games"
		}
		if (TrackResultFields.sport.toString() === "") {
			result = true
			error["sport"] = "Please Select Sport"
		}

		if (TrackResultFields.event.toString() === "") {
			result = true
			error["Event"] = "Please Select Event"
		}
		if (TrackResultFields.sportLevel.toString() === "") {
			result = true
			error["sportLevel"] = "Please Select Sport Level"
		}
		if (TrackResultFields.facility.toString() === "") {
			result = true
			error["facility"] = "Please Select Facility"
		}
		if (TrackResultFields.sportDate.toString() === "") {
			result = true
			error["sportDate"] = "Please Select Date"
		}
		if (TrackResultFields.sporttimeslot.toString() === "") {
			result = true
			error["sporttimeslot"] = "Please Select Slot "
		}
		if (TrackResultFields.participantType.toString() === "") {
			result = true
			error["participantType"] = "Please Select Participant Type"
		}
		if (
			TrackResultFields.teamName.toString() === "" &&
			TrackResultFields.participantType.toString() !== "2"
		) {
			result = true
			error["teamName"] = "Please Select Team"
		}
		if (
			TrackResultFields.Users.toString() === "" &&
			TrackResultFields.addUploadResultPopUp === false
		) {
			result = true
			error["Users"] = "Please Select Users"
		}
		if (TrackResultFields.position.length === 0 && TrackResultFields.addUploadResultPopUp === false) {
			result = true
			error["position"] = "Please Enter The Position "
		}
		if (TrackResultFields.score === "" && TrackResultFields.addUploadResultPopUp === false) {
			result = true
			error["score"] = "Please Enter The Score "
		}

		setErrors(error)
		return result
	}
	const handelSaveResult = async () => {
		
		
		var validation = AddresultVaidation()
		if (validation === false) {
			let departmentid =
				TrackResultFields.deparment.toString() === "" ? "0" : TrackResultFields.deparment.toString()
			let sportID =
				TrackResultFields.sport.toString() === "" ? "0" : TrackResultFields.sport.toString()
			let teamID =
				TrackResultFields.teamName.toString() === "" ? "0" : TrackResultFields.teamName.toString()

			let GamesTreackResult = {
				UserProfileID: "0",
				GamesSetupID: TrackResultFields.game.toString(),
				GamesTeamID: teamID,
				SportsID: sportID,
				Score: TrackResultFields.score,
				DepartmentID: departmentid,
				Position:TrackResultFields.position.toString(),
				GamesParticipantID: TrackResultFields.GamesParticipantID.toString(),
			}
			setTrackResultFields((prev) => ({ ...prev, loading: true }))
			let PlayerIDs = TrackResultFields.Users.toString()
			setTrackResultFields((prev) => ({ ...prev, loading: true }))
			await APIRequest.getPostService(
				APIUrl.ADD_TEAM_USER_RESULT + "/" + PlayerIDs,
				GamesTreackResult
			)
				.then((response) => {
					setTrackResultFields((prev) => ({ ...prev, loading: false }))
					if (response.Data === true) {
						setTrackResultFields({ ...Cleardata })
						
						parentRef.current.handlerRefreshInvitedList()
						openNotification("success", "Result Saved successfully", 1)
					}
				})
				.catch((error) => {
					setTrackResultFields((prev) => ({ ...prev, loading: false }))
				})
		}
	}
	return (
		<>
			{Permissions.includes(Permission.G_T_R_VIEW) ? (
				<Container fluid>
					<Spin size='large' className="pl-3 pr-3" spinning={TrackResultFields.loading} tip='Loading...'>
						<div className='row pt-0'>
							<div className='col-sm-6'>
								<label
									// style={{ borderBottom: "4px solid" }}
									className='games_layout form-group mt-3 row col-sm-12 pr-0'
								>
									Track Results
								</label>
							</div>
							<div className='col-sm-6 d-flex justify-content-end'>
								{Permissions.includes(Permission.G_T_R_CREATE) ? (
									<Button
									className='games_layout form-group mt-3 ml-3'
									className='btn_green btn -games_layout form-group mt-4 ml-3'
										onClick={() => {
											setTrackResultFields((TrackResultFields) => ({
												...TrackResultFields,
												addResultPopUp: true,
												addUploadResultPopUp: false,
												headerName: "Add Result",
											}))
											setErrors({})
										}}
									>
										Add Result
									</Button>
								) : null}
								{Permissions.includes(Permission.G_T_R_UPLOAD) ? (
									<Button
									
										className='btn_green btn -games_layout form-group mt-4 ml-3'
										onClick={() => {
											setTrackResultFields((TrackResultFields) => ({
												...TrackResultFields,
												addResultPopUp: true,
												addUploadResultPopUp: true,
												headerName: "Add Upload Result",
											}))
											setErrors({})
										}}
									>
										Add Upload Result
									</Button>
								) : null}{" "}
							</div>
						</div>
						<Modal
							backdrop='static'
							size='md'
							//className='popupborder'
							keyboard={true}
							show={TrackResultFields.addResultPopUp}
							onHide={() => setTrackResultFields({ ...TrackResultFields, addResultPopUp: false })}
							aria-labelledby='contained-modal-title-vcenter'
							//centered
						>
							<Modal.Header className=''>
								<div className='col-sm-12'>
									<div className='row'>
										<div className='col-sm-11'>
											<div class="text-left">
												<h5 className='font-weight-bold text-white mb-0'>
													{TrackResultFields.headerName}
												</h5>
											</div>
										</div>
										<div className='col-sm-1'>
											<div className='d-flex justify-content-end'>
												<button
													type='button'
													className='close text-white'
													onClick={() => setTrackResultFields({ ...Cleardata })}
													data-dismiss='modal'
													aria-label='Close'
												>
													<span aria-hidden='true'>&times;</span>
												</button>{" "}
											</div>
										</div>
									</div>
								</div>
							</Modal.Header>
							<Modal.Body className=''>
								<div className='row '>
									<div className='form-group col-sm-6'>
										<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Game<span className='text-danger'>*</span>
										</label>
										<Select
											disabled={TrackResultFields.uploadbtnFlag}
											key='game'
											className='textfiled w-80'
											name='Game'
											size={200}
											showSearch
											allowClear
											bordered={false}
											maxTagCount={1}
											maxTagTextLength={15}
											placeholder='Select Game'
											value={TrackResultFields.game}
											onChange={(val, key) => handleGameOnchange(val, key)}
											optionFilterProp='children'
											filterSort={(optionA, optionB) => {
												return optionA.children
													.toLowerCase()
													.localeCompare(optionB.children.toLowerCase())
											}}
										>
											{ListData.gamesData.map((item, index) => (
												<Option key={index} value={item.OptionID}>
													{item.OptionData}
												</Option>
											))}
										</Select>
										<span className='text-danger'>{errors.game}</span>
									</div>

									<div className='form-group col-sm-6'>
										<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Sport<span className='text-danger'>*</span>
										</label>
										<Select
											disabled={TrackResultFields.uploadbtnFlag}
											showSearch={true}
											allowClear
											className='textfiled w-80'
											bordered={false}
											maxTagCount={1}
											maxTagTextLength={15}
											placeholder='Select Sport'
											name='sport'
											size={"large"}
											value={TrackResultFields.sport}
											optionFilterProp='children'
											onChange={(val) => SportOnChange(val)}
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{ListData.sportsData.map((c) => (
												<Option value={c.OptionID}>{c.OptionData}</Option>
											))}
										</Select>
										<span className='val-err-msg'>{errors.sport}</span>
									</div>
									<div className='form-group col-sm-6'>
										<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Event<span className='text-danger'>*</span>
										</label>
										<Select
											disabled={TrackResultFields.uploadbtnFlag}
											showSearch={true}
											allowClear
											className='textfiled w-80'
											bordered={false}
											maxTagCount={1}
											maxTagTextLength={15}
											placeholder='Select Event'
											name='event'
											size={"large"}
											value={TrackResultFields.event}
											optionFilterProp='children'
											onChange={(val) => handelEventOnchange(val)}
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{ListData.eventData.map((c) => (
												<Option value={c.OptionID}>{c.OptionData}</Option>
											))}
										</Select>
										<span className='val-err-msg'>{errors.Event}</span>
									</div>
									<div className='form-group col-sm-6'>
										<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Round/ Level/ Set<span className='text-danger'>*</span>
										</label>
										<Select
											disabled={TrackResultFields.uploadbtnFlag}
											showSearch={true}
											allowClear
											className='textfiled w-80'
											bordered={false}
											maxTagCount={1}
											maxTagTextLength={15}
											placeholder='Select Rounds/ Levels/ Sets'
											name='sportLevel'
											size={"large"}
											value={TrackResultFields.sportLevel}
											optionFilterProp='children'
											onChange={(val) => handelSportRoundOnchange(val)}
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{ListData.sportLevelData.map((c) => (
												<Option value={c.OptionID}>{c.OptionData}</Option>
											))}
										</Select>
										<span className='val-err-msg'>{errors.sportLevel}</span>
									</div>
									<div className='form-group col-sm-6'>
										<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Facility<span className='text-danger'>*</span>
										</label>
										<Select
											disabled={TrackResultFields.uploadbtnFlag}
											showSearch={true}
											allowClear
											className='textfiled w-80'
											bordered={false}
											maxTagCount={1}
											maxTagTextLength={15}
											placeholder='Select Facility'
											name='facility'
											size={"large"}
											value={TrackResultFields.facility}
											onChange={(val) => FacilityChange(val)}
											optionFilterProp='children'
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{ListData.facilityData.map((c) => (
												<Option value={c.OptionID}>{c.OptionData}</Option>
											))}
										</Select>
										<span className='val-err-msg'>{errors.facility}</span>
									</div>
									<div className='form-group col-sm-6'>
										<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Schedule Date<span className='text-danger'>*</span>
										</label>
										<Select
											disabled={TrackResultFields.uploadbtnFlag}
											showSearch={true}
											allowClear
											className='textfiled w-80'
											bordered={false}
											maxTagCount={1}
											maxTagTextLength={15}
											placeholder='Select Schedule Date'
											name='sportDate'
											size={"large"}
											value={TrackResultFields.sportDate}
											optionFilterProp='children'
											onChange={(val) => handleDateOnchange(val)}
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{ListData.sportDateData.map((c) => (
												<Option value={c.OptionID}>{c.OptionData}</Option>
											))}
										</Select>
										<span className='val-err-msg'>{errors.sportDate}</span>
									</div>
									<div className='form-group col-sm-6'>
										<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Slot<span className='text-danger'>*</span>
										</label>
										<Select
											disabled={TrackResultFields.uploadbtnFlag}
											showSearch={true}
											allowClear
											className='textfiled w-80'
											bordered={false}
											maxTagCount={1}
											maxTagTextLength={15}
											placeholder='Select Slot'
											name='sporttimeslot'
											size={"large"}
											value={TrackResultFields.sporttimeslot}
											onChange={(val) => handleTimeslotOnchange(val)}
											optionFilterProp='children'
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{ListData.sporttimeslotData.map((c) => (
												<Option value={c.OptionID}>{c.OptionData}</Option>
											))}
										</Select>
										<span className='val-err-msg'>{errors.sporttimeslot}</span>
									</div>

									<div className='form-group col-sm-6'>
										<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Participant Type <span className='text-danger'>*</span>
										</label>
										<Select
											disabled={TrackResultFields.uploadbtnFlag}
											showSearch={true}
											allowClear
											className='textfiled w-80'
											bordered={false}
											maxTagCount={1}
											maxTagTextLength={15}
											placeholder='Select Participant Type'
											name='participantType'
											size={"large"}
											value={TrackResultFields.participantType}
											onChange={(val) => handleParticipantTypeOnchange(val)}
											optionFilterProp='children'
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											<Option value={1}>{"Team"}</Option>
											<Option value={2}>{"Individual"}</Option>
										</Select>
										<span className='val-err-msg'>{errors.participantType}</span>
									</div>

									{TrackResultFields.participantType === 1 ? (
										<div className='form-group col-sm-6'>
											<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
												Team <span className='text-danger'>*</span>
											</label>
											<Select
												disabled={TrackResultFields.uploadbtnFlag}
												showSearch={true}
												allowClear
												className='textfiled w-80'
												bordered={false}
												maxTagCount={1}
												maxTagTextLength={15}
												placeholder='Select Team'
												name='team'
												size={"large"}
												value={TrackResultFields.teamName}
												onChange={(val) => handleTeamOnchange(val)}
												optionFilterProp='children'
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{ListData.teamData.map((c) => (
													<Option value={c.OptionID}>{c.OptionData}</Option>
												))}
											</Select>
											<span className='val-err-msg'>{errors.teamName}</span>
										</div>
									) : null}
									{TrackResultFields.addUploadResultPopUp === false ||
									(TrackResultFields.participantType.toString() !== "" &&
										TrackResultFields.addUploadResultPopUp === false) ? (
										<div className='form-group col-sm-6'>
											<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
												Player(s)<span className='text-danger'>*</span>
											</label>
											<Select
												disabled={TrackResultFields.uploadbtnFlag}
												showSearch={true}
												allowClear
												mode='multiple'
												className='textfiled w-80'
												bordered={false}
												maxTagCount={1}
												maxTagTextLength={15}
												placeholder='Select Team Players'
												name='Users'
												size={"large"}
												value={TrackResultFields.Users}
												optionFilterProp='children'
												onChange={(val, key) => handleUserOnChange(val, key)}
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{ListData.UsersData.map((c) => (
													<Option key={c.OtherID + "--" + c.Value} value={c.Value}>
														{c.Lable}
													</Option>
												))}
											</Select>
											<span className='val-err-msg'>{errors.Users}</span>
										</div>
									) : null}
									{TrackResultFields.addUploadResultPopUp === false ? (
										<div className='form-group col-sm-6'>
											<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
											Position  <span className='text-danger'>*</span>
											</label>
											<Input
												disabled={TrackResultFields.uploadbtnFlag}
												allowClear
												type="number"
												className='textfiled '
												bordered={false}
												placeholder='Position'
												name='position'
												size={"large"}
												value={TrackResultFields.position}
												onChange={(event) => handlePositionOnchange(event.target.value)}
											></Input>
											<span className='val-err-msg'>{errors.position}</span>
										</div>
									) : null}
									{TrackResultFields.addUploadResultPopUp === false ? (
										<div className='form-group col-sm-6'>
											<label className='mb-0' style={{color:"#03534c"}} class="font-weight-bold popup_txt">
												Score/ Time/ Distance <span className='text-danger'>*</span>
											</label>
											<Input
												disabled={TrackResultFields.uploadbtnFlag}
												allowClear
												className='textfiled '
												bordered={false}
												placeholder='Score /Time /Distance'
												name='Individual'
												size={"large"}
												value={TrackResultFields.score}
												onChange={(event) => handleScoreOnchange(event.target.value)}
											></Input>
											<span className='val-err-msg'>{errors.score}</span>
										</div>
										
									) : null}
									{TrackResultFields.addUploadResultPopUp === true ? (
										<div className='col-sm-12'>
											<Dragger {...config} disabled={TrackResultFields.uploadbtn}>
												<p className='ant-upload-drag-icon text-center'>
													<FontAwesomeIcon
														className='icon_color'
														size='md'
														icon={faCloudUploadAlt}
													></FontAwesomeIcon>
												</p>
												<p className='ant-upload-text text-center'>
													Click or drag file to this area to upload
												</p>
												<p className='ant-upload-hint text-center'>
													Support for a single or bulk upload.
												</p>
											</Dragger>
										</div>
									) : null}
								</div>
							</Modal.Body>
							<Modal.Footer className='m-0 py-0 '>
								<div className='col-sm-12'>
									<div className='row  d-flex justify-content-end'>
										{TrackResultFields.addUploadResultPopUp === true ? (
											<>
												<Button
													disabled={TrackResultFields.uploadbtnFlag}
													onClick={handlerDownload}
													className='btn_green  mr-2 btn'
												>
													Download
												</Button>
												<Button
													onClick={uploadData}
													disabled={TrackResultFields.uploadbtn}
													className='btn_green mr-2 btn'
												>
													Upload
												</Button>
											</>
										) : null }{" "}
										{TrackResultFields.addUploadResultPopUp === false ? (
										<Button
													onClick={handelSaveResult}
													className='btn_green mr-2 btn'
												>
													Save
												</Button>
										):null}
									</div>
								</div>
							</Modal.Footer>
						</Modal>

						
						{/* {TrackResultFields.IsExportExcel ? (
						<ExportData
							data={TrackResultFields.exportData}
							label={TrackResultFields.exportLabel}
							filename={TrackResultFields.exportFilename}
						/>
					) : null} */}
					</Spin>{" "}
				</Container>
			) : null}
			<MainGrid ref={parentRef}></MainGrid>
		</>
	)
}

export default PermissionProvider(Trackresults)
