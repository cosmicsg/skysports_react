import React, { useState } from "react";
import { Modal, Button } from "react-bootstrap";
import {
  APIRequest,
  showNotification,
} from "../../../components/api-manager/apiRequest";
import { Message } from "../../../message/messageConstant";
import * as APIUrl from "../../../components/api-manager/apiConstant";

const ModelPopup = (props) => {
  const [IsOpenPopup, setOpenPopup] = useState(true);

  const closePopup = () => {
    props.closePopupFuc();
  };

  const deleteHostActivity = () => {
    const { ID } = props ? props.row : {};
    if (ID) {
      let temp = Array.isArray(ID) ? ID : [ID];
      temp = temp.join(",");
      let url = APIUrl.DELETE_GAMES_HOST_ACTIVITY + "/" + temp;
      APIRequest.getGetService(url).then((res) => {
        if (res.Data) {
          showNotification(
            "Games Host Activity deleted successfully.",
            "success"
          );
          props.refreshList();
          props.closePopupFuc();
        }
      });
    }
  };

  return (
    <>
      <Modal size="sm" backdrop="static" show={IsOpenPopup}>
        <div className="popup-content">
          <div className="deletecircle"></div>
          <i className="fa fa-trash-o deleteicon"></i>
          <Modal.Body>
            <p className="pull-left" style={{ margin: "4rem 2rem" }}>
              {" "}
              <h5>{Message.HOST_ACTIVITY_DELETE_HEADER}</h5>
              <h6 style={{ color: "darkgray" }}>
                {Message.HOST_ACTIVITY_DELETE_CONFIRM}
              </h6>
              <h6
                style={{
                  color: "darkgray",
                  marginRight: "4rem",
                  marginBottom: "-3rem",
                }}
              >
                {Message.INVITE_DELETE_CONFIRM1}
              </h6>
            </p>
            <br />
            <div className="pull-right">
              <Button className="btn_cancel mr-2" onClick={closePopup}>
                Cancel
              </Button>
              <Button
                className="btn_green"
                style={{ height: "2rem", padding: "5px 10px" }}
                onClick={deleteHostActivity}
              >
                Delete
              </Button>
            </div>
          </Modal.Body>
        </div>
      </Modal>
    </>
  );
};

export default ModelPopup;
