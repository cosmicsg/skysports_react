import React, {
  useRef,
  useState,
  forwardRef,
  useImperativeHandle,
  useEffect,
} from "react";
import ReactTable from "react-table";
import "../../../styles/CosmicTable.css";
import "antd/dist/antd.css";
import "../../../styles/grid.css";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import "../../../styles/Games.css";
import {
  faArrowDown,
  faArrowUp,
  faDownload,
  faEdit,
  faTrashAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Pagination from "../../../utils/Pagination";
import { Card, Collapse, Button } from "react-bootstrap";
import { Select } from "antd";
import ExportData from "../../../utils/ExportData";
import ModelPopup from "./ModelPopup";

const { Option } = Select;

//Export label
const exportLabel = [
  { columnName: "Game", value: "GameName" },
  { columnName: "Department Type", value: "DepartmentType" },
  { columnName: "Team", value: "TeamName" },
];

export const HostCountryActivity = forwardRef((props, ref) => {
  const [hostCountryList, setHostCountryList] = useState([]);
  const [addUser, setAddUser] = useState({
    isAddUser: false,
    departmentTypeID: 0,
  });
  const table = useRef(null);
  const childRef = useRef(null);
  const [paginationProps, setPaginationProps] = useState({
    totalPages: 0,
    totalRecords: 0,
    pageSize: 10,
  });

  const [filterExpanded, setFilterExpanded] = useState({
    IsExpanded: false,
    Icons: false,
  });

  const [filterProps, setFilterProps] = useState({
    departmentType: [],
    teamName: [],
    disableSearch: true,
  });

  const [filterDataProps, setFilterDataProps] = useState({
    departmentTypeData: [],
    teamsData: [],
  });
  const [exportExcelProps, setExportExcelProps] = useState({
    isExportExcel: false,
    exportData: [],
  });
  const [isRefreshList, setRefreshList] = useState(false);

  const [deleteProps, setDeleteProps] = useState({
    isOpenPopup: false,
    row: {},
  });

  const [editProps, setEditProps] = useState({});

  const handleFilterExpanded = (data) => {
    let Icons = filterExpanded.Icons === true ? false : true;
    setFilterExpanded({
      IsExpanded: data,
      Icons: Icons,
    });
  };

  const handleOnchange = (name, val) => {
    setFilterProps((prevState) => ({
      ...prevState,
      [name]: val,
    }));
    handleSearchBtn(name, val);
  };

  const emptyCheck = (val) => {
    let temp = true;
    if (val !== "" && val !== undefined && val !== null && val.length !== 0) {
      temp = false;
    }
    return temp;
  };

  const handleSearchBtn = (name, val) => {
    const { departmentType, teamName } = filterProps;
    let flag = true;
    if (name === "departmentType") {
      if (!emptyCheck(val)) flag = false;
    } else if (departmentType.length !== 0) flag = false;
    if (name === "teamName") {
      if (!emptyCheck(val)) flag = false;
    } else if (teamName.length !== 0) flag = false;

    setFilterProps((prevState) => ({
      ...prevState,
      disableSearch: flag,
    }));
  };

  const handleFetchListData = (pageNo) => {
    const { departmentType, teamName } = filterProps;
    let departmentIDs = Array.isArray(departmentType)
      ? departmentType
      : [departmentType];
    let teamIDs = Array.isArray(teamName) ? teamName : [teamName];

    let filterModel = {
      GameIDs: "",
      SportIDs: "",
      DepartmentTypeIDs:
        departmentIDs.length > 0 ? departmentIDs.join(",") : "",
      TeamIDs: teamIDs.length > 0 ? teamIDs.join(",") : "",
      PageNo: pageNo ? pageNo : 1,
      PageSize: paginationProps.pageSize,
      SortColumn: "GameName",
      SortOrder: "ASC",
    };
    APIRequest.getPostService(
      APIUrl.GET_HOST_COUNTRY_ACTIVITY_LIST,
      filterModel
    )
      .then((res) => {
        if (res.Succeeded) {
          setHostCountryList(res.Data);
          if (res.Data && res.Data.length > 0) {
            setPaginationProps({
              ...paginationProps,
              totalPages: res.Data[0].TotalPages,
              totalRecords: res.Data[0].TotalRows,
            });
          }
          setRefreshList(false);
        }
      })
      .catch((err) => {});
  };

  const NoDataConst = (props) => (
    <span className="table-nodata-style">No records found </span>
  );

  useImperativeHandle(ref, () => ({
    refreshList() {
      handleFetchListData();
    },
  }));

  useEffect(() => {
    handleFetchListData();
    APIRequest.getGetService(APIUrl.GET_DEPARTMENT_TYPE_DROPDOWN_LIST).then(
      (res) => {
        if (res.Succeeded) {
          setFilterDataProps((prevState) => ({
            ...prevState,
            departmentTypeData: res.Data,
          }));
        }
      }
    );
    APIRequest.getGetService(APIUrl.GET_GAMES_TEAM)
      .then((res) => {
        if (res.Succeeded) {
          setFilterDataProps((prevState) => ({
            ...prevState,
            teamsData: res.Data,
          }));
        }
      })
      .catch((err) => {});
    return () => {
      setHostCountryList([]);
    };
  }, []);

  const tableColumnDataLimit = (str) => {
    return str.length > 30 ? str.substring(0, 27) + "..." : str;
  };

  const handleAddUsers = (row) => {
    setAddUser({
      isAddUser: true,
      departmentTypeID: row.DepartmentTypeID,
    });

    if (childRef.current !== null) childRef.current.handleUpdateState();
  };

  const handleAddUserUpdateState = () => {
    setAddUser({
      isAddUser: false,
      departmentTypeID: 0,
    });
  };

  const handlePagination = (pageNo) => {
    //Refresh list data
    handleFetchListData(pageNo);
  };

  const handleFilterSubmit = () => {
    const { departmentType, teamName } = filterProps;

    if (
      departmentType !== "" ||
      departmentType !== undefined ||
      departmentType !== null ||
      departmentType.length !== 0 ||
      teamName !== "" ||
      teamName !== undefined ||
      teamName !== null ||
      teamName.length !== 0
    ) {
      setFilterProps({
        ...filterProps,
        disableSearch: false,
      });
      handleFetchListData(0);
    } else {
      setFilterProps({
        ...filterProps,
        disableSearch: true,
      });
    }
  };

  useEffect(() => {
    if (isRefreshList) handleFetchListData(0);
  }, [isRefreshList]);

  const handleClearFilters = () => {
    setFilterProps({
      departmentType: [],
      teamName: [],
      disableSearch: true,
      isRefreshList: true,
    });
    setRefreshList(true);
  };

  const handleExportData = () => {
    const { departmentType, teamName } = filterProps;
    let departmentIDs = Array.isArray(departmentType)
      ? departmentType
      : [departmentType];
    let teamIDs = Array.isArray(teamName) ? teamName : [teamName];

    let filterModel = {
      GameIDs: "",
      SportIDs: "",
      DepartmentTypeIDs:
        departmentIDs.length > 0 ? departmentIDs.join(",") : "",
      TeamIDs: teamIDs.length > 0 ? teamIDs.join(",") : "",
      PageNo: 1,
      PageSize: paginationProps.totalRecords,
      SortColumn: "GameName",
      SortOrder: "ASC",
    };
    APIRequest.getPostService(
      APIUrl.GET_HOST_COUNTRY_ACTIVITY_LIST,
      filterModel
    )
      .then((res) => {
        if (res.Succeeded) {
          setExportExcelProps({
            exportData: res.Data,
            isExportExcel: true,
          });
        }
        setExportExcelProps({
          exportData: [],
          isExportExcel: false,
        });
      })
      .catch((err) => {});
  };

  const editHostActivity = (row) => {
    setEditProps(row);
    props.updateEditProps(row);
  };
  const deleteHostActivity = (row) => {
    setDeleteProps({
      isOpenPopup: true,
      row: row,
    });
  };
  const updateOpenModalPopup = () => {
    setDeleteProps({
      ...deleteProps,
      isOpenPopup: false,
    });
  };
  const columns = [
    {
      Header: () => <div style={{ textAlign: "left" }}>#</div>,
      accessor: "SerialNo",
      sortable: false,
      resizable: false,
      className: "tableheader wordwrap",
      headerClassName: "BoldText ColoredText",

      Cell: (row) => {
        return (
          <div className="tabledata pt-2" style={{ textAlign: "left" }}>
            {row.value}
          </div>
        );
      },
      width: Math.round(window.innerWidth * 0.04),
    },
    {
      Header: () => <div style={{ textAlign: "left" }}>Game</div>,
      resizable: false,
      accessor: "GameName",
      style: { overflowWrap: "break-word" },
      Cell: (row) => {
        return (
          <div className="tabledata wordwrap" title={row.original.GameName}>
            {tableColumnDataLimit(row.original.GameName)}
          </div>
        );
      },
    },
    {
      Header: () => <div style={{ textAlign: "left" }}>Department Type</div>,
      resizable: false,
      accessor: "DepartmentType",
      style: { overflowWrap: "break-word" },
      Cell: (row) => {
        return (
          <div
            className="tabledata wordwrap"
            title={row.original.DepartmentType}
          >
            {tableColumnDataLimit(row.original.DepartmentType)}
          </div>
        );
      },
    },
    {
      Header: () => <div style={{ textAlign: "left" }}>Team</div>,
      resizable: false,
      accessor: "TeamName",
      style: { overflowWrap: "break-word" },
      Cell: (row) => {
        return (
          <div className="tabledata wordwrap" title={row.original.TeamName}>
            {tableColumnDataLimit(row.original.TeamName)}
          </div>
        );
      },
    },
    {
      Header: () => (
        <div style={{ textAlign: "center" }} className="vertical_center ">
          {"     "}Action
        </div>
      ),
      accessor: "Action",
      className: "tableheader wordwrap",
      sortable: false,
      resizable: false,
      width: Math.round(window.innerWidth * 0.06),
      filterable: false,
      Cell: (row) => {
        return (
          <div className="tabledata text-left">
            <FontAwesomeIcon
              title="Edit"
              className="mr-2 icon_Edit"
              onClick={() => editHostActivity(row.original)}
              icon={faEdit}
              style={{ cursor: "pointer" }}
            />
            <FontAwesomeIcon
              title="Delete"
              className="icon_Delete"
              onClick={() => deleteHostActivity(row.original)}
              icon={faTrashAlt}
              style={{ cursor: "pointer" }}
            />
          </div>
        );
      },
    },
  ];

  return (
    <>
      <div className=" pt-10 form-group">
        <div className="d-flex justify-content-start pt-1"></div>
        <Card className="w-100 card form-group">
          <Card.Header
            className="text-white"
            onClick={() => handleFilterExpanded(!filterExpanded.IsExpanded)}
          >
            <div className="float-left filter-heading-font">
              Search Host Activity{" "}
            </div>
            <div className="float-right">
              {filterExpanded.Icons ? (
                <FontAwesomeIcon
                  color="#FFFFFF"
                  className="action-icon-font"
                  icon={faArrowUp}
                  style={{ cursor: "pointer" }}
                ></FontAwesomeIcon>
              ) : (
                <FontAwesomeIcon
                  color="#FFFFFF"
                  className="action-icon-font"
                  icon={faArrowDown}
                  style={{ cursor: "pointer" }}
                ></FontAwesomeIcon>
              )}
            </div>
          </Card.Header>
          <Collapse in={filterExpanded.IsExpanded}>
            <Card.Body>
              <div className="row col-sm-12">
                <div className="col-sm-5 form-group">
                  <label>Department Type</label>
                  <Select
                    className="textfiled w-80"
                    bordered={false}
                    maxTagCount={2}
                    mode={"multiple"}
                    showSearch
                    allowClear
                    placeholder="Select Department type"
                    name="departmentType"
                    size={200}
                    value={filterProps.departmentType}
                    onChange={(val) => handleOnchange("departmentType", val)}
                    optionFilterProp="children"
                    filterSort={(optionA, optionB) =>
                      optionA.children
                        .toLowerCase()
                        .localeCompare(optionB.children.toLowerCase())
                    }
                  >
                    {filterDataProps.departmentTypeData.map((item, index) => {
                      return (
                        <Option key={index} value={item.OptionID}>
                          {item.OptionData}
                        </Option>
                      );
                    })}
                  </Select>
                </div>
                {/*</div>*/}
                <div className="col-sm-1"></div>
                <div className="col-sm-5 form-group">
                  <label>Team</label>
                  <Select
                    className="textfiled w-80"
                    name="teamName"
                    mode={"multiple"}
                    showSearch
                    allowClear
                    size={200}
                    bordered={false}
                    value={filterProps.teamName}
                    onChange={(val) => handleOnchange("teamName", val)}
                    placeholder="Select Team"
                    optionFilterProp="children"
                    filterSort={(optionA, optionB) =>
                      optionA.children
                        .toLowerCase()
                        .localeCompare(optionB.children.toLowerCase())
                    }
                  >
                    {filterDataProps.teamsData.map((item, index) => {
                      return (
                        <Option key={index} value={item.OptionID}>
                          {item.OptionData}
                        </Option>
                      );
                    })}
                  </Select>
                </div>
              </div>
              <div className="row col-sm-12">
                <div className="col-sm-6"></div>
                <div className="col-sm-5 d-flex justify-content-end pt-4">
                  <Button
                    className="btn_green mr-2"
                    onClick={handleFilterSubmit}
                    disabled={filterProps.disableSearch}
                  >
                    Search
                  </Button>
                  <Button className="btn_green" onClick={handleClearFilters}>
                    Clear
                  </Button>
                </div>
              </div>
            </Card.Body>
          </Collapse>
        </Card>
        <div className="admin-report-table">
          <ReactTable
            data={hostCountryList}
            columns={columns}
            showPaginationTop={false}
            showPaginationBottom={false}
            minRows={0}
            defaultPageSize={10}
            defaultPage={1}
            ref={table}
            sortable={true}
            multiSort={true}
            manual
            className={"CosmicTable"}
            showPageSizeOptions={false}
            NoDataComponent={NoDataConst}
            manual
            defaultSorting={[
              {
                id: "id",
                desc: true,
              },
            ]}
            manualPagination={true}
          />
        </div>
      </div>
      <div className="row col-sm-12">
        <div className="col-sm-6">
          <Button
            className="btn_green mr-2"
            disabled={false}
            onClick={handleExportData}
          >
            <FontAwesomeIcon
              title="Export Data"
              color={"#FFFFFF"}
              icon={faDownload}
            >
              {" "}
            </FontAwesomeIcon>{" "}
            Export Excel
          </Button>
        </div>
        <div className="row col-sm-6 d-flex justify-content-end">
          <Pagination
            totalPages={paginationProps.totalPages}
            totalRecords={paginationProps.totalRecords}
            paginationCall={handlePagination}
          />
        </div>
      </div>
      <>
        {exportExcelProps.isExportExcel ? (
          <ExportData
            data={exportExcelProps.exportData}
            label={exportLabel}
            filename={"Host Country Activity"}
          />
        ) : null}
        {deleteProps.isOpenPopup ? (
          <ModelPopup
            row={deleteProps.row}
            closePopupFuc={updateOpenModalPopup}
            refreshList={handleFetchListData}
          />
        ) : null}
      </>
    </>
  );
});
