import React, { useState, useEffect, useRef } from "react";
import { Button, Container } from "react-bootstrap";
import { Select, Col, Input } from "antd";
import {
  APIRequest,
  showNotification,
} from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { HostCountryActivity } from "./HostCountryActivity";
import "../../../styles/Games.css";

const { Option } = Select;

const CreateHost = (props) => {
  const [games, setGames] = useState([]);
  const [sports, setSports] = useState([]);
  const [departmentType, setDepartmentType] = useState([]);
  const [teamName, setTeamName] = useState([]);
  const [errors, setErrors] = useState({});
  const [editProps, setEditProps] = useState({
    isEdit: false,
    ID: 0,
  });

  const [formData, setFormData] = useState({
    gamesData: [],
    sportsData: [],
    departmentTypeData: [],
    teamsData: [],
  });

  const childRef = useRef(null);

  useEffect(() => {
    APIRequest.getGetService(APIUrl.GET_DEPARTMENT_TYPE_DROPDOWN_LIST).then(
      (res) => {
        if (res.Succeeded) {
          setFormData((prevState) => ({
            ...prevState,
            departmentTypeData: res.Data,
          }));
        }
      }
    );
    APIRequest.getGetService(APIUrl.GET_GAMES_TEAM)
      .then((res) => {
        if (res.Succeeded) {
          setFormData((prevState) => ({
            ...prevState,
            teamsData: res.Data,
          }));
        }
      })
      .catch((err) => {});
  }, []);

  const handleGameOnchange = (game) => {
    if (game !== "" && game !== undefined && game !== null) {
      errors["games"] = "";
      setErrors(errors);
    }
    setGames(game);
  };

  const handleSportsOnchange = (sport) => {
    if (sport !== "" && sport !== undefined && sport !== null) {
      errors["sports"] = "";
      setErrors(errors);
    }
    setSports(sport);
  };

  const handleDepartmentTypeOnchange = (deptType) => {
    if (deptType !== "" && deptType !== undefined && deptType !== null) {
      errors["departmentType"] = "";
      setErrors(errors);
    }
    setDepartmentType(deptType);
  };
  const handleCreateTeamOnchange = (e) => {
    let team = e.target.value;
    if (team !== "" && team !== undefined && team !== null) {
      errors["teamName"] = "";
      setErrors(errors);
    }
    setTeamName(team);
  };

  const handleOnchange = (name, val) => {
    setTeamName(val);
  };

  const handleFieldValidation = () => {
    let isErrorExist = false;
    let errors = {};
    if (
      departmentType === "" ||
      departmentType === undefined ||
      departmentType === null ||
      departmentType.length === 0
    ) {
      errors["departmentType"] = "Please select Department type";
      isErrorExist = true;
    }
    if (
      teamName === "" ||
      teamName === undefined ||
      teamName === null ||
      teamName.length === 0
    ) {
      errors["teamName"] = "Please enter Team";
      isErrorExist = true;
    }
    setErrors(errors);

    return isErrorExist;
  };

  const handleSubmit = () => {
    let isErrorExist = handleFieldValidation();

    if (!isErrorExist) {
      //alert("error does not exist");
      let inputData = {
        DepartmentTypeID: departmentType,
        GamesTeamID: teamName,
        IsUpdate: editProps.isEdit ? editProps.isEdit : false,
        GamesHostActivityID: editProps.ID ? editProps.ID : 0,
      };

      APIRequest.getPostService(APIUrl.ADD_HOST_COUNTRY_ACTIVITY, inputData)
        .then((res) => {
          handleClear();
          if (!editProps.isEdit)
            showNotification(
              "Games Host Activity added successfully.",
              "success"
            );
          else
            showNotification(
              "Games Host Activity updated successfully.",
              "success"
            );

          childRef.current.refreshList();
          setEditProps({
            ID: 0,
            isEdit: false,
          });
        })
        .catch((err) => {
        });
    }
  };

  const handleClear = () => {
    setGames([]);
    setSports([]);
    setDepartmentType([]);
    setTeamName([]);
    setErrors({});
  };

  const updateEditProps = (row) => {
    
    if (row && row.ID > 0 && row.DepartmentTypeID > 0 && row.TeamID > 0) {
      setDepartmentType(row.DepartmentTypeID);
      setTeamName(row.TeamID);
      setEditProps({
        ID: row.ID,
        isEdit: true,
      });
    }
  };

  return (
    <Container fluid>
      <div className="h-full singletab">
        <div className="games_layout form-group mt-3">
          Host Country Activities
        </div>
      </div>
      <div className="row col-sm-12" style={{ marginTop: "2rem" }}>
        <div className="col-sm-5">
          <label>
            Department Type<span className="text-danger">*</span>
          </label>

          <Select
            className="textfiled w-80"
            bordered={false}
            maxTagCount={2}
            placeholder="Select Department type"
            name="gamesType"
            size={"large"}
            value={departmentType}
            onChange={handleDepartmentTypeOnchange}
            optionFilterProp="children"
            filterSort={(optionA, optionB) =>
              optionA.children
                .toLowerCase()
                .localeCompare(optionB.children.toLowerCase())
            }
          >
            {formData.departmentTypeData.map((item, index) => {
              return (
                <Option key={index} value={item.OptionID}>
                  {item.OptionData}
                </Option>
              );
            })}
          </Select>
          <span className="val-err-msg">{errors.departmentType}</span>
        </div>
        <div className="col-sm-1"></div>
        <div className="col-sm-5 form-group">
          <label>Team </label>
          <span className="text-danger">*</span>
          <br />
          <Select
            className="textfiled w-80"
            name="teamName"
            showSearch
            allowClear
            size={200}
            bordered={false}
            value={teamName}
            onChange={(val) => handleOnchange("teamName", val)}
            placeholder="Select Team"
            optionFilterProp="children"
            filterSort={(optionA, optionB) =>
              optionA.children
                .toLowerCase()
                .localeCompare(optionB.children.toLowerCase())
            }
          >
            {formData.teamsData.map((item, index) => {
              return (
                <Option key={index} value={item.OptionID}>
                  {item.OptionData}
                </Option>
              );
            })}
          </Select>
          <span className="val-err-msg">{errors.teamName}</span>
        </div>
        <div className="col-sm-1"></div>
      </div>
      <div className="col-sm-12 form-group row">
        <div className="col-sm-5"></div>
        <div className=" col-sm-6 d-flex justify-content-end">
          <Button onClick={handleSubmit} className="btn_green mr-2">
            Save
          </Button>
          <Button onClick={handleClear} className="btn_green">
            Cancel
          </Button>
        </div>
      </div>
      <div className="col-sm-12">
        <HostCountryActivity
          ref={childRef}
          property={props}
          updateEditProps={updateEditProps}
        />
      </div>
    </Container>
  );
};

export default CreateHost;
