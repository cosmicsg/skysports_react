import React, {
  useRef,
  useState,
  forwardRef,
  useImperativeHandle,
  useEffect,
} from "react";
import ReactTable from "react-table";
import moment from "moment";
import {
  faEdit,
  faEye,
  faTrashAlt,
  faFolder,
  faArrowUp,
  faArrowDown,
  faDownload,
  faExclamationTriangle,
  faEraser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../../../styles/CosmicTable.css"; //"../../../styles/CosmicTable.css"
import "antd/dist/antd.css";
import "../../../styles/grid.css";
import "../../../styles/Games.css";
import "../../../styles/menu.css";
import { Message } from "../../../message/messageConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { Modal, Button, Card, Collapse } from "react-bootstrap";
import { Select, Input, DatePicker, notification, Upload } from "antd";
import Pagination from "../../../utils/Pagination";
import ExportData from "../../../utils/ExportData";
import { InboxOutlined } from "@ant-design/icons";
import { Permissions } from "../../../permissions/AppPermissions";
import PermissionProvider from "../../../permissions/PermissionProvider";

const { Option } = Select;
const { Dragger } = Upload;
const exportLabel = [
  { columnName: "S.No", value: "SerialNo" },
  { columnName: "GameName", value: "GameName" },
  { columnName: "GameType", value: "GameTypeName" },
  { columnName: "SportsName", value: "SportName" },
  { columnName: "HostCountry", value: "CountryName" },
  { columnName: "StartDate", value: "StartDate" },
  { columnName: "EndDate", value: "EndDate" },
];

export const CreateGameList1 = PermissionProvider(forwardRef((props, ref) => {
    const [permissionGroup, setPermissionGroup] = useState([]);
    const [gameSetupList, setGameSetupList] = useState([]);
    const [isOpenModal, setOpenModal] = useState(false);
    const table = useRef(null);
    const [isEyeData] = useState([]);
    const [isEyeDatapopup, setEyeDatapopup] = useState(false);
    const [isUploadDatapopup, setUploaDatapopup] = useState(false);
    const [errors] = useState({});

    const [paginationProps, setPaginationProps] = useState({
      totalPages: 0,
      totalRecords: 0,
      pageSize: 10,
      totalCount: 0,
      sortColumn: "",
      sortValue: "",
      SDateValidate: 0,
    });
    const [filterExpanded, setFilterExpanded] = useState({
      IsExpanded: false,
      Icons: false,
    });
    //Search Filter in Game Setup
    const [filterProps, setFilterProps] = useState({
      GameName: "",
      GameType: [],
      SportName: [],
      HostCountry: [],
      StartDate: "",
      EndDate: "",
      disableSearch: true,
      DocumentType: [],
      //isRefreshList: false,
    });

    const [filterDataProps, setFilterDataProps] = useState({
      GameName: "",
      gameTypeData: [],
      sportNameData: [],
      hostCountryData: [],
      StartDate: "",
      EndDate: "",
    });
    const [exportExcelProps, setExportExcelProps] = useState({
      isExportExcel: false,
      exportData: [],
    });
    const [isRefreshList, setRefreshList] = useState(false);

    const handleFilterExpanded = (data) => {
      let Icons = filterExpanded.Icons === true ? false : true;
      setFilterExpanded({
        IsExpanded: data,
        Icons: Icons,
      });
    };
    const [documentDataProps, setDocumentDataProps] = useState({
      DocumentCategoryName: [],
    });
    const handleOnchange = (name, val) => {
      debugger
      setFilterProps({
        ...filterProps,
        [name]: val,
        disableSearch:
          val !== "" && val !== undefined && val !== null && val.length !== 0
            ? false
            : true,
      });
    };
    //Fetch data from React Table
    const handleFetchData = (state) => {
      
      let sortbyname = "GameName";
      let orderby = "ASC";
      if (state.sorted !== undefined) {
        if (state.sorted.length !== 0) {
          sortbyname = state.sorted[0].id;
          if (state.sorted[0].desc === true) {
            orderby = "ASC";
          }
          if (state.sorted[0].desc === false) {
            orderby = "DESC";
          }
        }
      }
      let gameName = filterProps.GameName;
      let gameType = Array.isArray(filterProps.GameType)
        ? filterProps.GameType
        : [filterProps.GameType];
      let sportID = Array.isArray(filterProps.SportName)
        ? filterProps.SportName
        : [filterProps.SportName];
      let hostCountry = Array.isArray(filterProps.HostCountry)
        ? filterProps.HostCountry
        : [filterProps.HostCountry];
      let startDate = filterProps.StartDate;
      if (filterProps.StartDate !== "") {
        startDate = moment(filterProps.StartDate).format("DD MMM YY");
      }
      let endDate = filterProps.EndDate;
      if (filterProps.EndDate !== "") {
        endDate = moment(filterProps.EndDate).format("DD MMM YY");
      }

      let filterModel = {
        GameName: gameName,
        GamesTypeID: gameType.length > 0 ? gameType.join(",") : "",
        SportID: sportID.length > 0 ? sportID.join(",") : "",
        GamesCountryID: hostCountry.length > 0 ? hostCountry.join(",") : "",
        StartDate: startDate,
        EndDate: endDate,
        PageNo: state > 0 ? state : 1,
        PageSize: paginationProps.pageSize,
        SortColumn: sortbyname,
        SortOrder: orderby,
      };
      APIRequest.getPostService(APIUrl.GET_GAMES_SETUP_LIST, filterModel)
        .then((res) => {
          
          if (res.Succeeded) {
            setGameSetupList(res.Data);
            if (res.Data && res.Data.length > 0) {
              const firstRow = res.Data.find((x) => x);
              setPaginationProps({
                ...paginationProps,
                totalPages: firstRow.TotalPages,
                totalRecords: firstRow.TotalRows,
                totalCount: res.Data.length >= 1 ? res.Data.length : 0,
                sortColumn: sortbyname,
                sortValue: orderby,
              });
            } else {
              setPaginationProps({
                ...paginationProps,
                totalPages: 0,
                totalRecords: 0,
                totalCount: 0,
              });
            }
            setRefreshList(false);
          }
        })
        .catch((err) => {});
    };
    const openModal = (rowid, self, flg, id) => {
      setOpenModal({
        isOpen: true,
        isDeleteId: id,
        deleteProps: rowid,
        isDeleteflag: flg,
      });
    };
    const CloseModal = () => {
      setOpenModal(false);
    };
    const NoDataConst = (props) => (
      <span className="table-nodata-style">No records found </span>
    );

    useImperativeHandle(ref, () => ({
      refreshList() {
        handleFetchData(1);
      },
    }));

    //useEffect lifecycle method
    useEffect(() => {
    
      // 
		  //	Reading user's permissions from PermissionProvider component
		  //
      let userPermissions = props.getAppPermissions();
      setPermissionGroup(userPermissions);
    }, []);

    useEffect(() => {
      handleFetchData(0);
      APIRequest.getGetService(APIUrl.GAME_TYPE_LIST).then((res) => {
        if (res.Succeeded) {
          setFilterDataProps((prevState) => ({
            ...prevState,
            gameTypeData: res.Data,
          }));
        }
      });
      APIRequest.getGetService(APIUrl.GET_SPORT_LIST).then((res) => {
        if (res.Succeeded) {
          setFilterDataProps((prevState) => ({
            ...prevState,
            sportNameData: res.Data,
          }));
        }
      });
      APIRequest.getGetService(APIUrl.COUNTRY_LIST).then((res) => {
        if (res.Succeeded) {
          setFilterDataProps((prevState) => ({
            ...prevState,
            hostCountryData: res.Data,
          }));
        }
      });
      APIRequest.getGetService(APIUrl.GAME_DOCUMENT_CATEGORY_TYPE).then(
        (res) => {
          if (res.Succeeded) {
            setDocumentDataProps((prevState) => ({
              ...prevState,
              DocumentCategoryName: res.Data,
            }));
          }
        }
      );
      return () => {
        setGameSetupList([]);
      };
    }, [isRefreshList]);

    const handlerPagination = (pageNo) => {
      
      handleFetchData(pageNo);
    };
    const tableColumnDataLimit = (str) => {
      return str.length > 30 ? str.substring(0, 27) + "..." : str;
    };
    const handleFilterSubmit = () => {
      const { GameName, GameTypeID, SportID, HostCountry, StartDate, EndDate } =
        filterProps;

      if (
        GameName !== "" ||
        GameName !== undefined ||
        GameName !== null ||
        GameTypeID !== "" ||
        GameTypeID !== undefined ||
        GameTypeID !== null ||
        GameTypeID.length !== 0 ||
        SportID !== "" ||
        SportID !== undefined ||
        SportID !== null ||
        SportID.length !== 0 ||
        HostCountry !== "" ||
        HostCountry !== undefined ||
        HostCountry !== null ||
        HostCountry.length !== 0 ||
        StartDate !== "" ||
        StartDate !== undefined ||
        StartDate !== null ||
        EndDate !== "" ||
        EndDate !== undefined ||
        EndDate !== null
      ) {
        setFilterProps({
          ...filterProps,
          disableSearch: false,
        });

        handleFetchData(0);
      } else {
        setFilterProps({
          ...filterProps,
          disableSearch: true,
        });
      }
    };
    //Search Filter Clear
    const handleClearFilters = () => {
      setFilterProps({
        GameName: "",
        GameTypeID: [],
        SportID: [],
        HostCountry: [],
        StartDate: "",
        EndDate: "",
        disableSearch: true,
        isRefreshList: true,
      });
      setRefreshList(true);
    };
    //Generate the Export Excel
    const handleExportData = () => {
      let GameNames = filterProps.GameName;
      let GameTypes = Array.isArray(filterProps.GameType)
        ? filterProps.GameType
        : [filterProps.GameType];
      let SportNames = Array.isArray(filterProps.SportName)
        ? filterProps.SportName
        : [filterProps.SportName];
      let hostCountrys = Array.isArray(filterProps.HostCountry)
        ? filterProps.HostCountry
        : [filterProps.HostCountry];
      let startDate = filterProps.StartDate;
      if (filterProps.StartDate !== "") {
        startDate = moment(filterProps.StartDate).format("DD MMM YY");
      }
      let endDate = filterProps.EndDate;
      if (filterProps.EndDate !== "") {
        endDate = moment(filterProps.EndDate).format("DD MMM YY");
      }
      let filterModel = {
        GameName: GameNames,
        GamesTypeID: GameTypes.length > 0 ? GameTypes.join(",") : "",
        SportID: SportNames.length > 0 ? SportNames.join(",") : "",
        GamesCountryID: hostCountrys.length > 0 ? hostCountrys.join(",") : "",
        StartDate: startDate,
        EndDate: endDate,
        PageNo: 1,
        PageSize: paginationProps.totalRecords,
        SortColumn: "GameName",
        SortOrder: "ASC",
      };
      APIRequest.getPostService(APIUrl.GET_GAMES_SETUP_LIST, filterModel)
        .then((res) => {
          if (res.Succeeded) {
            setExportExcelProps({
              exportData: res.Data,
              isExportExcel: true,
            });
          }
          setExportExcelProps({
            exportData: [],
            isExportExcel: false,
          });
        })
        .catch((err) => {
        });
    };
    //   const DynamicdisabledDate = (current) => {
    //     let date =startDate
    //    var dateconvert = new Date(date)
    //    var expiryDate = dateconvert.setDate(new Date(dateconvert).getDate() + 1)
    //    let dynamicexpiryDate = new Date(expiryDate)
    //    let Day = ("0" + dynamicexpiryDate.getDate()).slice(-2)
    //    let month = ("0" + (dynamicexpiryDate.getMonth() + 1)).slice(-2)
    //    let dataformet = dynamicexpiryDate.getFullYear() + "-" + month + "-" + Day
    //    const start = moment(dataformet, "YYYY-MM-DD")
    //    return current < start || current < moment()
    //    //return moment().add(-1, 'days') >= current
    //  }
    // const handleEndDateOnchange = (endDate) => {
    // 	setEndDate(endDate);
    // }
    const openViewAlldataModal = (data, self) => {
      setEyeDatapopup({ isEyeDatapopup: true, isEyeData: data });
      isEyeData.gameName = data.GameName;
      isEyeData.gameType = data.GameTypeName;
      isEyeData.SportName = data.SportName;
      isEyeData.HostCountry = data.CountryName;
      isEyeData.StartDate = data.StartDate;
      isEyeData.EndDate = data.EndDate;
      isEyeData.CreatedBy = data.CreatedBy;
      isEyeData.CreatedOn = data.CreatedOn;
      isEyeData.UpdatedBy = data.UpdatedBy;
      isEyeData.UpdatedOn = data.UpdatedOn;
      // if ((data.UpdatedOn = "01 Jan 01")) {
      //   isEyeData.UpdatedOn = "";
      // } else {
      //   isEyeData.UpdatedOn = data.UpdatedOn;
      // }
    };
    const viewAlldatapopupClose = () => {
      setEyeDatapopup(false);
      setUploaDatapopup(false);
    };
    //const [editProps, setEditProps] = useState({});
    const openEditAlldataModal = (row) => {
      //setEditProps(row);
      props.updateEditProps(row, true);
    };
    const [uploadProps, setUploadProps] = useState({
      //GameName: "",
      GamesSetupID: "",
      DocumentCategoryID: "",
    });
    const [UPDocument, setUPDocument] = useState([]);
    const openFileUploaddataModal = (row) => {
      setUploaDatapopup({ isUploadDatapopup: true, uploadProps: row });
      uploadProps.GameName = row.GameName;
      uploadProps.GamesSetupID = row.GamesSetupID;
    };
    const allowedFiles = [".zip"];

    const UploadOutlinedprops = {
      name: "file",
      multiple: "true",
      onChange(info) {
        const status = info.file;
        //UPDocument = info.fileList
        if (status !== "uploading") {
        }
        if (status === "done") {
          // message.success( info.name + " " + ("Image uploaded successfully") );
        } else if (status === "error") {
          // message.error(file.name + " " + ("Image upload failed"));
        }
        //  file=null;
      },
      beforeUpload: (file) => {
        let filetype = file.name;
        filetype = filetype.split(".");
        if (allowedFiles.includes(filetype[1])) {
          let uploadLists = UPDocument;
          uploadLists.push(file);
          // setuploadList((uploadList1) => ({...uploadList1, uploadLists}));
          // await  uploadData(file)
          return false;
        }
        let AvailaleUploadList = UPDocument;
        AvailaleUploadList.push(file);
        return false;
      },
    };
    const handleOnchangeDocument = (name, val) => {
      setUploadProps({
        ...uploadProps,
        [name]: val,
        disableSearch:
          val !== "" && val !== undefined && val !== null && val.length !== 0
            ? false
            : true,
      });
    };
    const handleSaveUploadFile = async (e) => {
      //let isValid = handleFieldValidation();
      let fileData = new FormData();

      if (UPDocument.length > 0) {
        let files = UPDocument;
        let GameTypeSetupID =
          uploadProps.DocumentCategoryID + "_" + uploadProps.GamesSetupID;
        for (let i = 0; i < files.length; i++) {
          // let doc = files[i];
          fileData.append("files", files[i], files[i].name);
        }
        let url = APIUrl.ROUTE_UPLOAD_GAMES_DOCUMENT + "/" + GameTypeSetupID;

        APIRequest.getPostService(url, fileData)
          .then((response) => {
            if (response.Succeeded === true) {
              //listData=response.Data;

              //   let uploadList = uploadList;
              //   var availaleUploadList = availaleUploadList;

              //   let data = availaleUploadList.filter(o1 => !uploadList.some(o2 => o1 === o2));
              if (!uploadProps.isEdit) {
                if (response.Data === false) {
                  handleShow("success", "File Upload is already exist", 1)
                } else {
                  handleShow("success", "File Uploaded", 0)
                }
              } else {
                if (response.Data === false) {
                  handleShow("success", "File Upload is already exist", 1)
                } else {
                  handleShow("success", "File Uploaded", 0)
                }
              }

              setUPDocument([]);
              // setAvailaleUploadList(data);
            }
            viewAlldatapopupClose();
          })
          .catch((error) => {});
      }
    };
    // const handleFieldValidation = () => {
    //   let isErrorExist = false;
    //   let errors = {};
    //   if (
    //     uploadProps.DocumentCategoryID.length === 0 ||
    //     uploadProps.DocumentCategoryID === undefined ||
    //     uploadProps.DocumentCategoryID === null
    //   ) {
    //     errors["DocumentCategoryID"] = "Please select Document Type";
    //     isErrorExist = true;
    //   }
    //   setErrors(errors);
    //   return isErrorExist;
    // };
    const handleShow = (type, description, flag) => {
      if (flag === 1) {
        notification[type]({
          message: description,
          icon: (
            <FontAwesomeIcon
              className="fa-xs icon_Delete "
              icon={faExclamationTriangle}
            ></FontAwesomeIcon>
          ),
          description: "",
        });
      } else if (flag === 0) {
        notification[type]({
          message: description,
          icon: (
            <FontAwesomeIcon
              color={"fa-xs icon_Delete"}
              className="fa-sm"
              icon={faEraser}
            ></FontAwesomeIcon>
          ),
          description: "",
        });
      }
    };
    //Delete Game Setup list
    const deleteGameSetup = () => {
      var rowIDs = isOpenModal.deleteProps;
      const GameSetupID = rowIDs.GamesSetupID ? rowIDs.GamesSetupID : 0;
      if (GameSetupID > 0) {
        let url = APIUrl.ROUTE_DELETE_GAMES_SETUP + "/" + GameSetupID;
        APIRequest.getGetService(url).then((res) => {
          CloseModal();
          let response = res.Data;
          if (response.IsRefInGamesTeam) {
            handleShow(
              "error",
              "Cannot delete, Team is created for this setup.",
              1
            );
          } else if (response.IsError) {
            handleShow("error", "Error occurred when deleting Setup", 1);
          } else {
            handleShow("success", "Game Setup deleted successfully.", 0);
            // setRefreshList(true);
          }
        });
      }
    };
    const columns = [
      {
        Header: () => <div   style={{ textAlign: "left" }}>S.No</div>,
        accessor: "SerialNo",
        sortable: false,
        resizable: false,
        className: "tableheader wordwrap",
        headerClassName: "BoldText ColoredText GamesSNoWidth",
        // maxWidth:"64%",

        Cell: (row) => {
          return (
            <div className="tabledata" style={{ textAlign: "left" , borderBottom: "-100px"}}>
              {row.value}
            </div>
          );
        },
       width: Math.round(window.innerWidth * 0.04),
      },
      {
        Header: () => <div style={{ textAlign: "left" }}>Game Name</div>,
        resizable: false,
        accessor: "GameName",
        style: { overflowWrap: "break-word" },
        width: Math.round(window.innerWidth * 0.15),
        Cell: (row) => {
          return (
            <div className="tabledata wordwrap" title={row.original.GameName} style={{ borderBottom: "-100px"}}>
              {tableColumnDataLimit(row.original.GameName)}
            </div>
          );
        },
      },
      {
        Header: () => <div style={{ textAlign: "left" }}>Game Type</div>,
        resizable: false,
        accessor: "GameType",
        style: { overflowWrap: "break-word" },
        width: Math.round(window.innerWidth * 0.10),
        Cell: (row) => {
          return (
            <div className="tabledata wordwrap" title={row.original.GameType}>
              {tableColumnDataLimit(row.original.GameTypeName)}
            </div>
          );
        },
      },
      {
        Header: () => <div style={{ textAlign: "left" }}>Sport</div>,
        resizable: false,
        accessor: "Sports",
        style: { overflowWrap: "break-word" },
        width: Math.round(window.innerWidth * 0.15),
        Cell: (row) => {
          return (
            <div className="tabledata wordwrap" title={row.original.SportName}>
              {tableColumnDataLimit(row.original.SportName)}
            </div>
          );
        },
      },
      {
        Header: () => <div style={{ textAlign: "left" }}>Host Country</div>,
        resizable: false,
        accessor: "HostCountry",
        style: { overflowWrap: "break-word" },
        width: Math.round(window.innerWidth * 0.1),
        Cell: (row) => {
          return (
            <div
              className="tabledata wordwrap"
              title={row.original.HostCountry}
            >
              {tableColumnDataLimit(row.original.CountryName)}
            </div>
          );
        },
      },
      {
        Header: () => <div style={{ textAlign: "left" }}>Start Date</div>,
        resizable: false,
        accessor: "StartDate",
        style: { overflowWrap: "break-word" },
        width: Math.round(window.innerWidth * 0.08),
        Cell: (row) => {
          return (
            <div className="tabledata wordwrap" title={row.original.StartDate}>
              {tableColumnDataLimit(row.original.StartDate)}
            </div>
          );
        },
      },
      {
        Header: () => <div style={{ textAlign: "left" }}>End Date</div>,
        resizable: false,
        accessor: "EndDate",
        style: { overflowWrap: "break-word" },
        width: Math.round(window.innerWidth * 0.08),
        Cell: (row) => {
          return (
            <div className="tabledata wordwrap" title={row.original.EndDate}>
              {tableColumnDataLimit(row.original.EndDate)}
            </div>
          );
        },
      },
      {
        Header: () => (
          <div style={{ textAlign: "left" }}>Short Description</div>
        ),
        resizable: false,
        show: false,
        style: { overflowWrap: "break-word" },
        width: Math.round(window.innerWidth * 0.08),
        Cell: (row) => {
          return (
            <div
              className="tabledata wordwrap"
              title={row.original.ShortDescription}
            >
              {row.original.ShortDescription}
            </div>
          );
        },
      },
      // {   Header: () => (
      //   <div style={{ textAlign: "left" }}>SDateValidate</div>
      // ),
      // accessor: "SDateValidate",show:false},
      {
        Header: () => <div style={{ textAlign: "center" }}>Action</div>,
        accessor: "Action",
        className: "tableheader wordwrap",
        sortable: false,
        resizable: false,
        textAlign: "center",
        filterable: false,
        width: Math.round(window.innerWidth * 0.08),
        Cell: (row) => {
          
          return (
            <div class="tabledata text-center"
              // className="tabledata text-left"
              // style={{ display: "flex", justifyContent: "space-around" }}
            >
              {permissionGroup.includes(Permissions.GAME_SETUP_EDIT) ? (
                <>
                  {row.original.SDateValidate === 1 ? (
                    <FontAwesomeIcon
                      onClick={() => openEditAlldataModal(row.original)}
                      title="Edit"
                      // className="icon_edit"
                      style={{ cursor: "pointer" },{color: "#017c86"}}
                      icon={faEdit}
                      width="3rem"
                      className="mr-2"
                    />
                  ) : null}
                </>
              ) : null}
              {row.original.SDateValidate === 1 ? (
                <FontAwesomeIcon
                  title="Upload file"
                  color="red"
                  //color=" #C29A37"
                  className="mr-2"
                  style={{ cursor: "pointer" }}
                  onClick={() => openFileUploaddataModal(row.original)}
                  icon={faFolder}
                  width="3rem"
                />
              ) : null}
              <FontAwesomeIcon
                onClick={() => openViewAlldataModal(row.original, this)}
                title="View All details"
                color="green"
                // color=' #C29A37'
                style={{ cursor: "pointer" }}
                icon={faEye}
                className="mr-2 icon_eye"
                width="3rem"
              />
              {permissionGroup.includes(Permissions.GAME_SETUP_DELETE) ? (
                <>
                  {row.original.SDateValidate === 1 ? (
                    <FontAwesomeIcon
                      title="Delete"
                      color="red"
                      //color=" #C29A37"
                      className="icon_Delete"
                      style={{ cursor: "pointer" }}
                      onClick={() => openModal(row.original, this, 1, null)}
                      icon={faTrashAlt}
                      width="3rem"
                    />
                  ) : null}
                </>
              ) : null}
            </div>
          );
        },
      },
    ];
    return (
      <>
      <div className='row col-sm-12 mr-0 ml-0'>
          <Card className='w-100 card from-group'>
            <Card.Header
            style={{ cursor: "pointer" }}
              className="text-white"
              onClick={() => handleFilterExpanded(!filterExpanded.IsExpanded)}
            >
              <div className="float-left filter-heading-font" style={{marginLeft:"-12px"}}>Search Game Setup</div>
              <div className="float-right">
                {filterExpanded.Icons ? (
                  <FontAwesomeIcon
                    color="#FFFFFF"
                    className="action-icon-font"
                    icon={faArrowUp}
                    style={{ cursor: "pointer" }}
                  ></FontAwesomeIcon>
                ) : (
                  <FontAwesomeIcon
                    color="#FFFFFF"
                    className="action-icon-font"
                    icon={faArrowDown}
                    style={{ cursor: "pointer" }}
                  ></FontAwesomeIcon>
                )}
              </div>
            </Card.Header>
            <Collapse in={filterExpanded.IsExpanded}>
              <Card.Body>
                <div className="row col-sm-14">
                  <div className='col-sm-4 form-group'>
                    <label>Game Name</label>
                    <Input
                      size="large"
                      autoComplete="off"
                      type="text"
                      name="GameName"
                      value={filterProps.GameName}
                      onChange={(event) =>
                        handleOnchange("GameName", event.target.value)
                      }
                      bordered={false}
                      className="textfiled w-80"
                      placeholder="Game Name"
                    ></Input>
                  </div>

                  <div className='form-group col-sm-4'>
                    <label>Game Type</label>
                    <Select
                      className="textfiled w-80"
                      bordered={false}
                      // maxTagCount={2}
                      mode="multiple"
                      placeholder="Select Game Type"
                      name="GameType"
                      size={200}
                      value={filterProps.GameType}
                      showSearch
                      onChange={(val) => handleOnchange("GameType", val)}
                      optionFilterProp="children"
                      filterSort={(optionA, optionB) =>
                        optionA.children
                          .toLowerCase()
                          .localeCompare(optionB.children.toLowerCase())
                      }
                    >
                      {filterDataProps.gameTypeData.map((item, index) => {
                        return (
                          <Option key={index} value={item.OptionID}>
                            {item.OptionData}
                          </Option>
                        );
                      })}
                    </Select>
                  </div>
                  {/*</div>*/}
                  <div className='form-group col-sm-4'>
                    <label>Sport</label>
                    <Select
                      className="textfiled w-80"
                      name="SportName"
                      showSearch
                      allowClear
                      mode="multiple"
                      maxTagCount="responsive"
                      // maxTagTextLength={15}
                      size={200}
                      bordered={false}
                      value={filterProps.SportName}
                      onChange={(val) => handleOnchange("SportName", val)}
                      placeholder="Select Sport"
                      optionFilterProp="children"
                      filterSort={(optionA, optionB) =>
                        optionA.children
                          .toLowerCase()
                          .localeCompare(optionB.children.toLowerCase())
                      }
                    >
                      {filterDataProps.sportNameData.map((item, index) => {
                        return (
                          <Option key={index} value={item.OptionID}>
                            {item.OptionData}
                          </Option>
                        );
                      })}
                    </Select>
                  </div>
                  <div className="col-sm-4 form-group">
                    <label>Host Country</label>
                    <Select
                      className="textfiled w-80"
                      name="HostCountry"
                      showSearch
                      allowClear
                      size={200}
                      bordered={false}
                      value={filterProps.HostCountry}
                      onChange={(val) => handleOnchange("HostCountry", val)}
                      placeholder="Select Host Country"
                      optionFilterProp="children"
                      filterSort={(optionA, optionB) =>
                        optionA.children
                          .toLowerCase()
                          .localeCompare(optionB.children.toLowerCase())
                      }
                    >
                      {filterDataProps.hostCountryData.map((item, index) => {
                        return (
                          <Option key={index} value={item.OptionID}>
                            {item.OptionData}
                          </Option>
                        );
                      })}
                    </Select>
                  </div>
                 
                  
                  <div className="col-sm-4 form-group">
                    <label>Start Date</label>
                    <DatePicker
                      size="large"
                      format="DD MMM YYYY"
                      disabled={false}
                      bordered={false}
                      name="StartDate"
                      selected={filterProps.StartDate}
                      value={filterProps.StartDate}
                      onChange={(val) => handleOnchange("StartDate", val)}
                      // disabledDate={(current) => {
                      //   return moment().add(-1, 'days')  >= current ;
                      //   }}
                      className="myDatePicker datepickerfiled "
                      placeholder="Select Start Date"
                    />
                  </div>
                  <div className="col-sm-4 form-group">
                    <label>End Date</label>
                    <DatePicker
                      size="large"
                      format="DD MMM YYYY"
                      disabled={false}
                      bordered={false}
                      name="EndDate"
                      selected={filterProps.EndDate}
                      value={filterProps.EndDate}
                      onChange={(val) => handleOnchange("EndDate", val)}
                      // disabledDate={(current) => {
                      //   return moment().add(-1, 'days')  >= current ;
                      //   }}
                      className="myDatePicker datepickerfiled "
                      placeholder="Select End Date"
                    />
                  </div>
                </div>
                <div className="col-sm-12 ">
                  {/* <div className="col-sm-6"></div> */}
                  <div className=" d-flex justify-content-end row">
                  <Button className="btn_green mr-2" onClick={handleClearFilters}>
                      Clear
                    </Button>
                    <Button
                      className="btn_green "
                      onClick={handleFilterSubmit}
                      disabled={filterProps.disableSearch}
                    >
                      Search
                    </Button>
                    
                  </div>
                </div>
              </Card.Body>
            </Collapse>
          </Card>
      </div>
          <div className="admin-report-table pt-3"  style={{ marginLeft: "15px", marginRight: "15px" }}>
            <ReactTable
              data={gameSetupList}
              columns={columns}
              showPaginationTop={false}
              showPaginationBottom={false}
              minRows={0}
              defaultPageSize={10}
              defaultPage={1}
              onFetchData={handleFetchData}
              ref={table}
              sortable={true}
              multiSort={true}
              manual
              className={"CosmicTable"}
              showPageSizeOptions={false}
              NoDataComponent={NoDataConst}
              defaultSorting={[
                {
                  id: "id",
                  desc: true,
                },
              ]}
              manualPagination={true}
            />
          </div>
        <div className='row col-sm-12 mt-2 pr-0'>
        <div className='col-sm-6 mt-1'>
        <label>
              <b>Total Records: </b>
              {paginationProps.totalRecords > 10
                ? paginationProps.totalCount + " of " + paginationProps.totalRecords
                : paginationProps.totalRecords !== 0
                ? paginationProps.totalRecords
                : 0}
            </label>
            </div>
            </div>
        <div className="row col-sm-12">
        <div  className='col-sm-6'>
            <Button
              className="btn_green mr-2"
              disabled={false}
              onClick={handleExportData}
              // style={{marginTop:"-25px"},{marginLeft:"0px"}}
            >
              <FontAwesomeIcon
                title="Export Data"
                color={"#FFFFFF"}
                icon={faDownload}
              >
                {" "}
              </FontAwesomeIcon>{" "}
              Export Excel
            </Button>
            
          </div>
          
          {paginationProps.totalPages > 0 ? (
             <div className='col-sm-6 d-flex justify-content-end mt-0 pr-0'>
              <Pagination
                totalPages={paginationProps.totalPages}
                totalRecords={paginationProps.totalRecords}
                paginationCall={handlerPagination}
              />
              </div>
          
          ) : null}
          </div>
         
        {exportExcelProps.isExportExcel ? (
          <ExportData
            data={exportExcelProps.exportData}
            label={exportLabel}
            filename={"Game Setup"}
          />
        ) : null}
        <>
          <Modal
            size="sm"
            //aria-labelledby="contained-modal-title-vcenter"
            //   aria-labelledby="contained-modal-title-vcenter"
            backdrop="static"
            //  centered
            show={isOpenModal}
          >
            <div className="popup-content">
              <div className="deletecircle"></div>
              <i className="fa fa-trash-o deleteicon"></i>
              <Modal.Body>
                <p className="pull-left" style={{ margin: "4rem 0rem" }}>
                  {" "}
                  <h5>{Message.GAME_SETUP_DELETE}</h5>
                  <h6 style={{ color: "darkgray" }}>
                    {Message.GAME_DELETE_CONFIRM}
                  </h6>
                  <h6
                    style={{
                      color: "darkgray",
                      marginRight: "7rem",
                      marginBottom: "-3rem",
                    }}
                  >
                    {Message.GAME_DELETE_CONFIRM1}
                  </h6>
                </p>
                <br />
                <div className="pull-right">
                <Button className="btn_cancel mr-2" onClick={CloseModal}>
                    No
                  </Button>
                  <Button
                    className="btn_green"
                    style={{ height: "2rem", padding: "5px 10px" }}
                    value={setOpenModal.GameSetupID}
                    onClick={deleteGameSetup}
                  >
                    Yes
                  </Button>
                </div>
              </Modal.Body>
            </div>
          </Modal>
          <div>
            <Modal
              backdrop="static"
              size="md"
              //className='notshow'
              keyboard={true}
              show={isEyeDatapopup}
              onHide={viewAlldatapopupClose} //toggle={props.onCloseModal}
              aria-labelledby="contained-modal-title-vcenter"
              //centered
            >
              <Modal.Header className="">
                <div className="col-sm-12">
                  <div className="row">
                    <div className="col-sm-11">
                      <div class="text-left">
                        <h5 className="font-weight-bold text-white mb-0">
                          {"Game Setup"}
                        </h5>
                      </div>
                    </div>
                    <div className="col-sm-1">
                      <div className="d-flex justify-content-end">
                        <button
                          type="button"
                          className="close text-white"
                          onClick={viewAlldatapopupClose}
                          data-dismiss="modal"
                          aria-label="Close"
                        >
                          <span aria-hidden="true">&times;</span>
                        </button>{" "}
                      </div>
                    </div>
                  </div>
                </div>
              </Modal.Header>
              <Modal.Body className="">
                <div className="col-sm-12">
                  <div className="row pt-10">
                    <div className="col-sm-1">{""}</div>
                    <div className="col-sm-10">
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            Game Name
                          </label>{" "}
                        </div>
                        <div className="col-sm-1"> </div>
                        <div className="col-sm-7">
                          {" "}
                          <label>{isEyeData.gameName}</label>{" "}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            Game Type
                          </label>{" "}
                        </div>
                        <div className="col-sm-1"> </div>
                        <div className="col-sm-7">
                          {" "}
                          <label>{isEyeData.gameType}</label>{" "}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            Sport
                          </label>{" "}
                        </div>
                        <div className="col-sm-1"> </div>
                        <div className="col-sm-7 align-self-end">
                          {" "}
                          <label className="col-sm-12 pl-0">
                            {isEyeData.SportName}
                          </label>{" "}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            Host Country
                          </label>{" "}
                        </div>
                        <div className="col-sm-1"> </div>
                        <div className="col-sm-7">
                          {" "}
                          <label>{isEyeData.HostCountry}</label>{" "}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            Start Date
                          </label>{" "}
                        </div>
                        <div className="col-sm-1"> </div>
                        <div className="col-sm-7">
                          {" "}
                          <label>{isEyeData.StartDate}</label>{" "}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            End Date
                          </label>{" "}
                        </div>
                        <div className="col-sm-1"> </div>
                        <div className="col-sm-7">
                          {" "}
                          <label>{isEyeData.EndDate}</label>{" "}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            Created On{" "}
                          </label>{" "}
                        </div>
                        <div className="col-sm-1">
                          {" "}
                          {/* <label className='font-weight-bold'>:</label>{" "} */}
                        </div>
                        <div className="col-sm-7">
                          {" "}
                          <label>{isEyeData.CreatedOn}</label>{" "}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            Created By{" "}
                          </label>{" "}
                        </div>
                        <div className="col-sm-1">
                          {" "}
                          {/* <label className='font-weight-bold'>:</label>{" "} */}
                        </div>
                        <div className="col-sm-7">
                          {" "}
                          <label>{isEyeData.CreatedBy}</label>{" "}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            Updated On{" "}
                          </label>{" "}
                        </div>
                        <div className="col-sm-1">
                          {" "}
                          {/* <label className='font-weight-bold'>:</label>{" "} */}
                        </div>
                        <div className="col-sm-7">
                          {" "}
                          <label>{isEyeData.UpdatedOn}</label>{" "}
                        </div>
                      </div>
                      <div className="form-group row">
                        <div className="col-sm-4">
                          {" "}
                          <label className="font-weight-bold popup_txt">
                            UpdatedBy{" "}
                          </label>{" "}
                        </div>
                        <div className="col-sm-1">
                          {" "}
                          {/* <label className='font-weight-bold'>:</label>{" "} */}
                        </div>
                        <div className="col-sm-7">
                          {" "}
                          <label>{isEyeData.UpdatedBy}</label>{" "}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Modal.Body>
              {/* <Modal.Footer className="m-0 py-0 ">
                <div className="col-sm-1">
                  <div className="row  d-flex justify-content-center">
                    <Button
                      className="cancel mr-2 btn_green"
                      onClick={viewAlldatapopupClose}
                    >
                      Close
                    </Button>
                  </div>
                </div>
              </Modal.Footer> */}
            </Modal>
          </div>
          <div>
            <Modal
              backdrop="static"
              size="md"
              //className='notshow'
              keyboard={true}
              show={isUploadDatapopup}
              onHide={viewAlldatapopupClose} //toggle={props.onCloseModal}
              aria-labelledby="contained-modal-title-vcenter"
              //centered
            >
              <Modal.Header className="">
                <div className="col-sm-12">
                  <div className="row">
                    <div className="col-sm-11">
                      <div class="text-left">
                        <h5 className="font-weight-bold text-white mb-0">
                          {"File Upload"}
                        </h5>
                      </div>
                    </div>
                    <div className='col-sm-1'>
                      <div className='d-flex justify-content-end'>
                        <button
                          type='button'
                          className='close text-white'
                          onClick={viewAlldatapopupClose}
                          data-dismiss='modal'
                          aria-label='Close'
                        >
                          <span aria-hidden="true">&times;</span>
                        </button>{" "}
                      </div>
                    </div>
                  </div>
                </div>
              </Modal.Header>
              <Modal.Body className="">
                <div className="row col-sm-12">
                  <div className="col-sm-6 from-group">
                    <label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
                      Game Name<span className="text-danger">*</span>
                    </label>
                    <Input
                      size="large"
                      autoComplete="off"
                      type="text"
                      name="gameName"
                      value={uploadProps.GameName}
                      bordered={false}
                      className="textfiled w-80 disabled"
                      placeholder="Game Name"
                    ></Input>
                  </div>
                  <div className="col-sm-6 form-group">
                    <label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
                      Document Category<span className="text-danger">*</span>
                    </label>
                    <Select
                      className="textfiled w-80"
                      bordered={false}
                      // maxTagCount={2}
                      style={{ width: "100%" }}
                      placeholder="Select Document Category"
                      name="documentCategory"
                      showSearch
                      size={"large"}
                      value={uploadProps.DocumentCategoryID}
                      onChange={(val) =>
                        handleOnchangeDocument("DocumentCategoryID", val)
                      }
                      optionFilterProp="children"
                      filterSort={(optionA, optionB) =>
                        optionA.children
                          .toLowerCase()
                          .localeCompare(optionB.children.toLowerCase())
                      }
                    >
                      {documentDataProps.DocumentCategoryName.map(
                        (item, index) => (
                          <Option key={index} value={item.OptionID}>
                            {item.OptionData}
                          </Option>
                        )
                      )}
                    </Select>
                    <span className="val-err-msg">
                      {errors.DocumentCategoryID}
                    </span>
                  </div>
                  <div
                    className="col-sm-12 form-group"
                    style={{ maxHeight: "300px" }}
                  >
                    <Dragger {...UploadOutlinedprops}>
                      {" "}
                      <p className="ant-upload-drag-icon text-center">
                        <InboxOutlined />
                      </p>
                      <p className="ant-upload-text text-center">
                        Click or drag file to this area to upload
                      </p>
                      <p className="ant-upload-hint text-center">
                        Support for a single or bulk upload.
                      </p>
                    </Dragger>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer className="m-0 py-0 ">
                {/* <div className="col-sm-1"> */}


                <div className="row col-sm-12 d-flex justify-content-end mt-4">
            <Button className="btn_green mr-2" onClick={viewAlldatapopupClose}>
                Close
              </Button>
                    <Button
                     className='btn_green'
                      onClick={handleSaveUploadFile}
                    >
                      Save
                    </Button>
                  </div>
                {/* </div> */}
              </Modal.Footer>
            </Modal>
          </div>
        </>
      </>
    );
  })
);
