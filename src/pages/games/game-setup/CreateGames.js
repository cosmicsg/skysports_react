import React, { useState, useEffect, useRef } from "react"
import "../../../styles/menu.css"
import "../../../styles/CosmicTable.css"
import "../../../styles/Games.css"
import { CreateGameList1 } from "../game-setup/CreateGameList"
import moment from "moment"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Select, DatePicker, Input, Space, notification } from "antd"
import { Button, Modal,Container } from "react-bootstrap"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import { faExclamationTriangle, faCheckCircle } from "@fortawesome/free-solid-svg-icons"
import { connect } from "react-redux"

const { Option } = Select
// const setIsExpanded = (data, self) => {
//   let Icons = self.state.Icons === true ? false : true;
//   self.setState({ IsExpanded: data, Icons: Icons });
// };
const CreateGames = (props) => {
	const [permissionGroup, setPermissionGroup] = useState([])
	const [gameName, setGameName] = useState("")
	const [gameType, setGameType] = useState([])
	const [sportID, setSports] = useState([])
	const [hostCountry, setHostCountry] = useState([])
	const [startDate, setStartDate] = useState("")
	const [endDate, setEndDate] = useState("")
	const [shortDescription, setShortDescription] = useState("")
	const [errors, setErrors] = useState({})
	//const [show, setShow] = useState(false);
	const [isAddDatapopup, setAddDatapopup] = useState(false)
	const [editProps, setEditProps] = useState({
		isEdit: false,
		ID: 0,
	})
	//const handleClose = () => setShow(false);
	//Delete Popup notification
	const handleShow = (type, description, flag) => {
		if (flag === 1) {
			notification[type]({
				message: description,
				icon: (
					<FontAwesomeIcon
						className='fa-xs icon_Delete '
						icon={faExclamationTriangle}
					></FontAwesomeIcon>
				),
				description: "",
			})
		} else if (flag === 0) {
			notification[type]({
				message: description,
				icon: (
					<FontAwesomeIcon color={"green"} className='fa-sm' icon={faCheckCircle}></FontAwesomeIcon>
				),
				description: "",
			})
		}
	}

	const [formData, setFormData] = useState({
		gametypeData: [],
		sportsData: [],
		hostCountryData: [],
	})
	const childRef = useRef(null)

	useEffect(() => {
		let { userPermissions } = props.user
		userPermissions =
			userPermissions !== undefined && userPermissions !== null ? userPermissions : []
		let permissionArray = Array.isArray(userPermissions) ? userPermissions : [userPermissions]

		//Assign permission
		setPermissionGroup(permissionArray)

		APIRequest.getGetService(APIUrl.GAME_TYPE_LIST).then((res) => {
			if (res.Succeeded) {
				setFormData((prevState) => ({
					...prevState,
					gametypeData: res.Data,
				}))
			}
		})

		APIRequest.getGetService(APIUrl.GET_SPORT_LIST).then((res) => {
			if (res.Succeeded) {
				setFormData((prevState) => ({
					...prevState,
					sportsData: res.Data,
				}))
			}
		})
		APIRequest.getGetService(APIUrl.COUNTRY_LIST).then((res) => {
			if (res.Succeeded) {
				setFormData((prevState) => ({
					...prevState,
					hostCountryData: res.Data,
				}))
			}
		})
	}, [])
	const handleGameNameOnchange = (e) => {
		let gameName = e.target.value
		if (gameName !== "" && gameName !== undefined && gameName !== null) {
			errors["gameName"] = ""
			setErrors(errors)
		}
		setGameName(gameName)
	}
	const handleGameTypeOnchange = (gameType) => {
		if (gameType !== "" && gameType !== undefined && gameType !== null) {
			errors["gameType"] = ""
			setErrors(errors)
		}
		setGameType(gameType)
	}
	const handleSportsOnchange = (sportID) => {
		if (sportID !== "" && sportID !== undefined && sportID !== null) {
			errors["sportID"] = ""
			setErrors(errors)
		}
		setSports(sportID)
	}
	const handleHostCountryOnchange = (hostCountry) => {
		if (hostCountry !== "" && hostCountry !== undefined && hostCountry !== null) {
			errors["hostCountry"] = ""
			setErrors(errors)
		}
		setHostCountry(hostCountry)
	}
	const handleStartDateOnchange = (startDate) => {
		setEndDate("")
		setStartDate(startDate)
	}
	

	// const handleDatePickerChange = (event) => {
	//   if (event !== null) {
	//     let dates = new Date(event);
	//     let Day = ("0" + dates.getDate()).slice(-2);
	//     let month = dates.toLocaleString("en-us", { month: "short" });
	//     let shortYear = dates.getFullYear().toString().substr(-2);
	//     let dataformet = Day + " " + month + " " + shortYear;

	//     setStartDate({ dataformet });
	//   } else {
	//     setStartDate({ dataformet: "aN-aN-NaN" });
	//   }
	// };
	const DynamicdisabledDate = (current) => {
		let date = startDate
		var dateconvert = new Date(date)
		var expiryDate = dateconvert.setDate(new Date(dateconvert).getDate() + 1)
		let dynamicexpiryDate = new Date(expiryDate)
		let Day = ("0" + dynamicexpiryDate.getDate()).slice(-2)
		let month = ("0" + (dynamicexpiryDate.getMonth() + 1)).slice(-2)
		let dataformet = dynamicexpiryDate.getFullYear() + "-" + month + "-" + Day
		const start = moment(dataformet, "YYYY-MM-DD")
		return current < start || current < moment()
		//return moment().add(-1, 'days') >= current
	}
	const handleEndDateOnchange = (endDate) => {
		setEndDate(endDate)
		// if (!endDate || !startDate) {
		// 		return moment().add(-1, 'days');
		// }
		// return endDate.valueOf() <= startDate.valueOf()
		// if (endDate !== "" && endDate !== undefined && endDate !== null) {
		//   errors["endDate"] = "";
		//   setErrors(errors);
		// }
	}
	const handleShortDescriptionOnchange = (e) => {
		let shortDescription = e.target.value
		if (shortDescription !== "" && shortDescription !== undefined && shortDescription !== null) {
			errors["shortDescription"] = ""
			setErrors(errors)
		}
		setShortDescription(shortDescription)
	}

	const handleFieldValidation = () => {
		let isErrorExist = false
		let errors = {}
		if (gameName === "" || gameName === undefined || gameName === null) {
			errors["gameName"] = "Please enter Game Name"
			isErrorExist = true
		}
		if(gameName.length!==0 && gameName.length <3){
			errors["gameName"] ="Minimum 3 Characters"
			isErrorExist=true
		}
		if(gameName.length > 200){
			errors["gameName"] = "Only 200 Characters"
			isErrorExist = true
		}
		if (gameType.length === 0 || gameType === undefined || gameType === null) {
			errors["gameType"] = "Please select Game Type"
			isErrorExist = true
		}
		if (sportID.length === 0 || sportID === undefined || sportID === null) {
			errors["sportID"] = "Please select sport"
			isErrorExist = true
		}
		if (hostCountry.length === 0 || hostCountry === undefined || hostCountry === null) {
			errors["hostCountry"] = "Please select Host Country"
			isErrorExist = true
		}
		if (startDate === "" || startDate === undefined || startDate === null) {
			errors["startDate"] = "Please select Start Date"
			isErrorExist = true;
		}
		if (endDate.length === 0 || endDate === undefined || endDate === null) {
			errors["endDate"] = "Please select End Date"
			isErrorExist = true
		}
		if (
			shortDescription.trim() === "" ||
			shortDescription === undefined ||
			shortDescription === null
		) {
			errors["shortDescription"] = "Please Enter Short Description"
			isErrorExist = true
		}
		if(shortDescription.length > 200)
		{
			errors["shortDescription"]= "Only 200 Characters"
			isErrorExist=true
		}
		if(shortDescription.length !== 0 && shortDescription.length < 3){
			errors["shortDescription"]="Minimum 3 Characters"
			isErrorExist=true
		}
		setErrors(errors)
		return isErrorExist
	}
	//Game Setup Save & Update
	const handleSave = () => {
		let isValid = handleFieldValidation()
		let Sdates = new Date(startDate)
		let sDate = new Date()
		let eDate = new Date()
		if (!editProps.isEdit) {
			sDate = startDate
		} else {
			let startDates = Sdates.setDate(new Date(Sdates).getDate() + 1)
			sDate = new Date(startDates)
		}
		if (!editProps.isEdit) {
			eDate = endDate
		} else {
			let Edates = new Date(endDate)
			let endDates = Edates.setDate(new Date(Edates).getDate() + 1)
			eDate = new Date(endDates)
		}

		if (!isValid) {
			//alert("error does not exist");
			let inputData = {
				Name: gameName,
				GamesTypeID: gameType,
				SportID: sportID.toString(),
				CountryID: hostCountry,
				StartDate: sDate,
				EndDate: eDate,
				ShortDescription: shortDescription,
				IsUpdate: editProps.isEdit ? editProps.isEdit : false,
				GamesSetupID: editProps.ID ? editProps.ID : 0,
			}
;
			APIRequest.getPostService(APIUrl.SAVE_CREATE_GAMES_SETUP, inputData)
				.then((res) => {
					if (!editProps.isEdit) {
						if (res.Data === false) {
							handleShow("success", "Game setup is already exist", 1)
						} else {
							handleShow("success", "Game setup created", 0)
						}
					} else {
						if (res.Data === false) {
							handleShow("success", "Game setup is already exist", 1)
						} else {
							handleShow("success", "Game setup Updated", 0)
						}
					}

					handleClear()
					childRef.current.refreshList()
					setEditProps({
						ID: 0,
						isEdit: false,
					})
					viewAlldatapopupClose()
				})
				.catch((err) => {
				})
		}
	}
	const handleClear = () => {
		setGameName("")
		setGameType([])
		setSports([])
		setHostCountry([])
		setStartDate("")
		setEndDate("")
		setShortDescription("")
		setErrors({})
	}
	const updateEditProps = (row, flag) => {
		if (flag) {
			var EditSportID = []
			const SportIDs = row.SportID.trim()
			if (SportIDs.indexOf(",") !== -1) {
				let SportIDs1 = SportIDs.split(",")
				SportIDs1.map((i) => EditSportID.push(parseInt(i)))
			} else {
				EditSportID.push(parseInt(SportIDs))
			}
			if (
				row &&
				row.GamesSetupID > 0 &&
				row.GamesTypeID > 0 &&
				row.SportID &&
				row.GamesCountryID > 0 &&
				row.StartDate &&
				row.EndDate &&
				row.ShortDescription
			) {
				setGameName(row.GameName)
				setGameType(row.GamesTypeID)
				setSports(EditSportID)
				setHostCountry(row.GamesCountryID)
				setStartDate(moment(row.StartDate))
				setEndDate(moment(row.EndDate))
				setShortDescription(row.ShortDescription)
				setEditProps({
					ID: row.GamesSetupID,
					isEdit: true,
				})
			}
			openViewAlldataModal()
		} else {
			handleClear()
			setEditProps({
				ID: "",
				isEdit: false,
			})
			openViewAlldataModal()
		}
	}
	const openViewAlldataModal = () => {
		window.scrollTo(0, 0)
		setAddDatapopup({ isAddDatapopup: true })
	}
	const viewAlldatapopupClose = () => {
		handleClear()
		setAddDatapopup(false)
		// setEditDatapopup(false)
	}
	return (
		<>
		<div>
			<div className='h-full singletab' >
			<div className='games_layout form-group mt-3 row col-sm-12 pr-0'>
			<div className='col-sm-6'>Game Setup</div>
			<div className='col-sm-6 d-flex justify-content-end pr-0'>
						<div className='d-flex justify-content-end'>
							<Button
							type='button'
								className='btn_green'
								onClick={updateEditProps}
							>
								Add Game
							</Button>
						</div>
				</div>
				</div>

					<br></br>
					<div>
						<CreateGameList1 ref={childRef} property={props} updateEditProps={updateEditProps} />
					</div>
			<div>
				<Modal
					backdrop='static'
					size='lg'
					size='md'
					className='notshow'
					keyboard={true}
					show={isAddDatapopup}
					onHide={viewAlldatapopupClose} //toggle={props.onCloseModal}
					aria-labelledby='contained-modal-title-vcenter'
				>
					<Modal.Header className=''>
						<div className='col-sm-12'>
							<div className='row'>
								<div className='col-sm-11'>
									<div class="text-left">
										{!editProps.isEdit ? (
											<h5 className='font-weight-bold text-white mb-0'>{"Add Game Setup"}</h5>
										) : (
											<h5 className='font-weight-bold text-white mb-0'>{"Edit Game Setup"}</h5>
										)}
									</div>
								</div>
								<div className='col-sm-1'>
									<div className='row  d-flex justify-content-center'>
										<button
											type='button'
											className='close text-white'
											onClick={viewAlldatapopupClose}
											data-dismiss='modal'
											aria-label='Close'
										>
											<span aria-hidden='true'>&times;</span>
										</button>{" "}
									</div>
								</div>
							</div>
						</div>
					</Modal.Header>
					<Modal.Body>
						<div className='row col-sm-12'>
							<div className='col-sm-6 from-group'>
								<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
									Game Name<span className='text-danger'>*</span>
								</label>
								<Input
									size='large'
									autoComplete='off'
									type='text'
									name='gameName'
									min={3}
									max={200}
									value={gameName}
									onChange={handleGameNameOnchange}
									bordered={false}
									className='textfiled w-80'
									placeholder='Game Name'
									text-color="#03534c"
								></Input>
								<span className='val-err-msg'>{errors.gameName}</span>
							</div>

							<div className='col-sm-6 form-group'>
										<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
										Game Type<span className='text-danger'>*</span>
										</label>
										<Select
											className='textfiled w-80'
											name='gameType'
											showSearch
											allowClear
											bordered={false}
											value={gameType}
											onChange={handleGameTypeOnchange}
											placeholder='Select Game Type'
											optionFilterProp='children'
											filterSort={(optionA, optionB) =>
												optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
											}
										>
											{formData.gametypeData.map((item, index) => {
												return (
													<Option key={index} value={item.OptionID}>
														{item.OptionData}
													</Option>
												)
											})}
										</Select>
										<span className='text-danger'>{errors.department}</span>
									</div>
							{/*</div>*/}
							<div className='col-sm-6 form-group'>
							<Space direction='vertical' size={2}>
								<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
									Sport<span className='text-danger'>*</span>
								</label>
								{!editProps.isEdit ? (
									<Select
										className='textfiled w-80'
										bordered={false}
										mode='multiple'
										maxTagCount="responsive"
										// maxTagTextLength={15}
										style={{ width: "100%" }}
										placeholder='Select Sport'
										name='sportID'
										size={"large"}
										showSearch
										value={sportID}
										onChange={handleSportsOnchange}
										optionFilterProp='children'
										filterSort={(optionA, optionB) =>
											optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
										}
									>
										{formData.sportsData.map((item, index) => (
											<Option key={index} value={item.OptionID}>
												{item.OptionData}
											</Option>
										))}
									</Select>
								) : (
									<Select
										className='textfiled w-80'
										bordered={false}
										mode='multiple'
										maxTagCount={1}
										maxTagTextLength={15}
										style={{ width: "100%" }}
										placeholder='Select Sport'
										name='sportID'
										size={"large"}
										showSearch
										value={sportID}
										onChange={handleSportsOnchange}
										// optionFilterProp='children'
										filterSort={(optionA, optionB) =>
											optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
										}
									>
										{formData.sportsData.map((item, index) => (
											<Option key={index} value={item.OptionID}>
												{item.OptionData}
											</Option>
										))}
									</Select>
								)}
								<span className='val-err-msg'>{errors.sportID}</span>
								</Space>
							</div>
							<div className='col-sm-6 form-group'>
							<Space direction='vertical' size={2}>
								<label style={{color:"#03534c"}} class="font-weight-bold popup_txt">
									Host Country<span className='text-danger'>*</span>
								</label>
								<Select
									className='textfiled w-80'
									name='HostCountry'
									showSearch
									allowClear
									bordered={false}
									value={hostCountry}
									onChange={handleHostCountryOnchange}
									placeholder='Select Country'
									optionFilterProp='children'
									// mode='multiple'
									maxTagCount={1}
									style={{ width: "100%" }}
									size={"large"}
									filterSort={(optionA, optionB) =>
										optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
									}
								>
									{formData.hostCountryData.map((item, index) => (
										<Option key={index} value={item.OptionID}>
											{item.OptionData}
										</Option>
									))}
								</Select>
								<span className='val-err-msg'>{errors.hostCountry}</span>
								</Space>
							</div>
							<div className='col-sm-6 form-group'>
								<Space direction='vertical' size={2}>
									<label className='ant_text_pleace ' class="font-weight-bold popup_txt" style={{color:"#03534c"}}>
										{" "}
										Start Date <span className='text-danger'>*</span>{" "}
									</label>

									{!editProps.isEdit ? (
										<DatePicker
											size='large'
											style={{
												height: "auto",
												width: "auto",
												border: "none",
												borderRadius: "0px",
												cursor: "pointer",
												fontSize: "17px",
												margin: "0px",
												padding: "0px",
											}}
											format='DD MMM YY'
											disabled={false}
											bordered={false}
											name='startDate'
											allowClear
											//selected={moment(startDate)}
											value={startDate}
											onChange={handleStartDateOnchange}
											disabledDate={(current) => {
												return moment().add(-1, "days") >= current
											}}
											className='myDatePicker datepickerfiled '
											placeholder='Select Start Date'
										/>
									) : (
										<DatePicker
											size='large'
											style={{
												height: "auto",
												width: "auto",
												border: "none",
												borderRadius: "0px",
												cursor: "pointer",
												fontSize: "17px",
												margin: "0px",
												padding: "0px",
											}}
											format='DD MMM YY'
											disabled={false}
											bordered={false}
											allowClear
											name='startDate'
											//selected={moment(startDate)}
											value={startDate}
											onChange={handleStartDateOnchange}
											disabledDate={(current) => {
												return moment().add(-1, "days") >= current
											}}
											className='myDatePicker datepickerfiled '
											placeholder='Select Start Date'
										/>
									)}
								</Space>
								<span className='val-err-msg'>{errors.startDate}</span>
							</div>
							<div className='col-sm-6 form-group'>
								<Space direction='vertical' size={2}>
									<label className='ant_text_pleace' class="font-weight-bold popup_txt" style={{color:"#03534c"}}>
										{" "}
										End Date<span className='text-danger'>*</span>{" "}
									</label>

									{!editProps.isEdit ? (
										<DatePicker
											size='large'
											format='DD MMM YY'
											className='myDatePicker datepickerfiled'
											bordered={false}
											// defaultValue={moment(isDiabledDate)}
											showToday={true}
											allowClear
											value={endDate}
											disabledDate={DynamicdisabledDate}
											name='endDate'
											onChange={handleEndDateOnchange}
											placeholder='Select End Date'
										/>
									) : (
										<DatePicker
											size='large'
											format='DD MMM YY'
											className='myDatePicker datepickerfiled'
											bordered={false}
											// defaultValue={moment(isDiabledDate)}
											showToday={true}
											allowClear
											value={endDate}
											disabledDate={DynamicdisabledDate}
											name='endDate'
											onChange={handleEndDateOnchange}
											placeholder='Select End Date'
										/>
									)}
								</Space>
								<span className='val-err-msg'>{errors.endDate}</span>
							</div>
							<div className='col-sm-12 form-group'>
								<label class="font-weight-bold popup_txt" style={{color:"#03534c"}}>Short Description</label>
								<span className='text-danger'>*</span>
								<Input
									size='large'
									autoComplete='off'
									type='text'
									name='ShortDescription'
									min={3}
									max={200}
									value={shortDescription}
									onChange={handleShortDescriptionOnchange}
									bordered={false}
									className='textfiled w-80'
									style={{ width: "100%" }}
									placeholder='Short Description'
								></Input>
								<span className='val-err-msg'>{errors.shortDescription}</span>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer className='m-0 py-0 '>
						<div className='col-sm-12'>
							<div className='col-sm d-flex justify-content-end pt-4'>
								
								{!editProps.isEdit ? (
									<Button onClick={handleSave} className='btn_green '>
									Save
								</Button>
								) : (
									<Button onClick={handleSave} className='btn_green '>
										Update
									</Button>
								)}
							</div>
						</div>
					</Modal.Footer>
				</Modal>
			</div>
					</div>
					</div>
		</>
	)
}

const mapPropsToState = (state) => ({
	user: state.userReducer,
})

export default connect(mapPropsToState, null)(CreateGames)
