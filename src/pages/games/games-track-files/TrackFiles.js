import React, { Fragment, useState, useEffect, useRef } from "react"
import ReactTable from "react-table"
import "../../../styles/CosmicTable.css"
import "antd/dist/antd.css"
import { Select, Spin, notification } from "antd"
import { Button, Modal, Card, Collapse } from "react-bootstrap"
import Pagination from "../../../utils/Pagination"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowDown, faArrowUp, faTrashAlt, faDownload } from "@fortawesome/free-solid-svg-icons"
import * as APIUrl from "../../../components/api-manager/apiConstant"
import { APIRequest } from "../../../components/api-manager/apiRequest"
import ExportData from "../../../utils/ExportData"
import { Message } from "../../../message/messageConstant"
import { Permissions as Permission } from "../../../permissions/AppPermissions"
import { forwardRef } from "react"
import PermissionProvider from "../../../permissions/PermissionProvider"
import { connect } from "react-redux"

const { Option } = Select
const NoDataConst = (props) => (
	<span className='table-nodata-style'>
		No records found Please check your connection or change your filters{" "}
	</span>
)
const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	}
}
export const TrackFiles = forwardRef((props, ref) => {
	//
	// Reading user's permissions from PermissionProvider component
	//
	let permissions = props.getAppPermissions()

	const [Permissions] = useState(permissions)

	const [listdata, setListdat] = useState({
		users: [],
		FilesData: [],
		StatusData: [],
	})
	const [inputData, setinputData] = useState({
		loading: true,
		pageSize: 10,
		IsExpanded: false,
		Icons: false,
		TotalPages: 0,
		TotalRecords: 0,
		IncrementPage: 0,
		TotalCount:0,
		//Export excel properties
		IsExportExcel: false,
		exportData: [],
		sortColumn: "",
		sortValue: "",
		exportFilename: "Games Track File List",
		exportbtn: true,
		isSearchBtn: true,
		FilesData: [],
		StatusData: [],
		clearValue: false,
		exportLabel: [
			{ columnName: "S.No", value: "SerialNo" },
			{ columnName: "File Name", value: "FileName" },
			{ columnName: "File Size", value: "FileSize" },
			{ columnName: "Status", value: "Status" },
			{ columnName: "Error", value: "Remarks" },
		],
		isDeleteID: 0,
		isOpen: false,
	})

	const tableColumnDataLimit = (str) => {
		return str.length > 25 ? str.substring(0, 22) + "..." : str
	}
	useEffect(() => {
		if (inputData.IsExportExcel === true) {
			setinputData((prev) => ({ ...prev, IsExportExcel: false }))
		}
	}, [inputData.IsExportExcel])
	useEffect(() => {
		async function fetchData() {
			const status = await APIRequest.getGetService(APIUrl.GET_JOB_SERVICE_STATUS_DROPDOWN)
			setListdat((prevState) => ({ ...prevState, StatusData: status.Data }))

			const file = await APIRequest.getGetService(
				APIUrl.GET_GAMES_TRACK_UPLOAD_FILE_DROPDOWN_LIST + "/@"
			)
			setListdat((prevState) => ({ ...prevState, FilesData: file.Data }))
		}
		fetchData()
	}, [])
	const handleClear = (event) => {
		setinputData((prev) => ({
			...prev,

			FilesData: [],
			StatusData: [],
			isSearchBtn: true,
			clearValue: true,
		}))
	}
	useEffect(() => {
		if (inputData.clearValue === true) {
			fetch(0)
			setinputData((prev) => ({ ...prev, clearValue: false }))
		}
	}, [inputData.clearValue])
	useEffect(() => {
		if (inputData.FilesData.toString() !== "" || inputData.StatusData.toString() !== "") {
			// fetch(0)
			setinputData((prev) => ({ ...prev, isSearchBtn: false }))
		} else {
			setinputData((prev) => ({ ...prev, isSearchBtn: true }))
		}
	}, [inputData.StatusData, inputData.FilesData])
	const handleTblDelete = async (id) => {
		setinputData((prev) => ({ ...prev, loading: true }))
		await APIRequest.getGetService(APIUrl.REMOVE_GAMES_TRACK_FILE + "/" + id)
			.then((response) => {
				setinputData((prev) => ({ ...prev, loading: false }))
				if (response.Data === true) {
					fetch(reactTable.current.state)
					openNotificationWithIcon("success", "Record has been deleted", 1)
				}
			})
			.catch((error) => {
				setinputData((prev) => ({ ...prev, loading: false }))
			})
	}

	const deleteModal = () => {
		handleTblDelete(inputData.isDeleteID)
		setinputData((inputData) => ({
			...inputData,
			isDeleteID: 0,
			isOpen: false,
		}))
	}
	const openModal = (id) => {
		setinputData((inputData) => ({
			...inputData,
			isOpen: true,
			isDeleteID: id,
		}))
	}
	const columns = [
		{
			Header: <div className='text-left'>S.No</div>,
			accessor: "SerialNo",
			show: true,
			sortable: false,
			resizable: false,
			width: Math.round(window.innerWidth * 0.04),
		},
		{
			Header: <div className='text-left'>File Name</div>,
			accessor: "FileName",
			show: true,
			resizable: false,
			Cell: (row) => {
				return (
					<div className='tabledata wordwrap' title={row.original.FileName}>
						{tableColumnDataLimit(row.original.FileName)}
					</div>
				)
			},
		},
		{
			Header: <div className='text-left'>Status</div>,
			accessor: "Status",
			show: true,
			resizable: false,
			Cell: (row) => {
				return (
					<div className='tabledata wordwrap' title={row.original.Status}>
						{tableColumnDataLimit(row.original.Status)}
					</div>
				)
			},
		},
		{
			Header: <div className='text-left'>File Size</div>,
			accessor: "FileSize",
			show: true,
			resizable: false,
		},
		{
			Header: <div className='text-left'>Errors</div>,
			accessor: "Remarks",
			show: true,
			resizable: false,
			Cell: (row) => {
				return (
					<div className='tabledata wordwrap' title={row.original.Remarks}>
						{tableColumnDataLimit(row.original.Remarks)}
					</div>
				)
			},
		},
		{
			Header: () => <div className=' text-center'>{"     "}Action</div>,
			accessor: "Action",
			className: "tableheader wordwrap",
			sortable: false,
			resizable: false,
			width: Math.round(window.innerWidth * 0.07),
			filterable: false,
			Cell: (row) => {
				return (
					<div className='tabledata text-center'>
						{Permissions.includes(Permission.G_T_F_DELETE) &&
						props.user.InvitedUserID === row.original.CreatedByID ? (
							<FontAwesomeIcon
								title='Delete'
								className=' icon_Delete'
								onClick={() => openModal(row.original.GamesTreackUploadResultID)}
								icon={faTrashAlt}
								style={{ cursor: "pointer" }}
							/>
						) : null}
					</div>
				)
			},
		},
	]

	const setIsExpanded = (data, self) => {
		let Icons = inputData.Icons === true ? false : true
		setinputData((inputDatas) => ({
			...inputDatas,
			IsExpanded: data,
			Icons: Icons,
		}))
	}
	const handleOverAllOnchange = async (label, val, lable) => {
		if (label === "FilesData") {
			setinputData((prev) => ({ ...prev, FilesData: val }))
		}
		if (label === "StatusData") {
			setinputData((prev) => ({ ...prev, StatusData: val }))
		}
	}
	const fetch = async (state) => {
		let sortbyname = "CreatedOn"
		let orderby = "DESC"
		if (state.sorted !== undefined) {
			if (state.sorted.length !== 0) {
				sortbyname = state.sorted[0].id
				if (state.sorted[0].desc === true) {
					orderby = "DESC"
				}
				if (state.sorted[0].desc === false) {
					orderby = "ASC"
				}
			}
		}
		let pagelimit = 0
		//let self = this
		if (state.page === undefined || state.pageSize === undefined) {
			pagelimit = inputData.pageSize
		} else {
			pagelimit = state.pageSize
		}
		let data = {
			PageNo: state > 0 ? state : 1,
			PageSize: pagelimit,
			SortColumn: sortbyname,
			SortOrder: orderby,
			Status: inputData.StatusData.toString(),
			Files: inputData.FilesData.toString(),
		}
		//		self.setState({ loading: true })
		APIRequest.getPostService(APIUrl.GET_GAMES_TRACK_FILE_GRID_LIST, data)
			.then((response) => {
				if (response.Data.length > 0) {
					const firstRow = response.Data.find((x) => x)

					setinputData((inputDatas) => ({
						...inputDatas,
						TotalPages: firstRow.TotalPages,
						TotalRecords: firstRow.TotalRows,
						TotalCount: response.Data.length >= 1 ? response.Data.length : 0,
						sortColumn: sortbyname,
						sortValue: orderby,
						exportbtn: false,
						loading: false,
					}))
					setListdat((listdata) => ({
						...listdata,
						users: response.Data,
					}))
				} else {
					setinputData((inputData) => ({
						...inputData,
						exportbtn: true,
						loading: false,
					}))
					setListdat((listdata) => ({
						...listdata,
						users: [],
					}))
				}
			})
			.catch((error) => {})
	}

	const handlerExportData = () => {
		let InputData = {
			PageNo: 1,
			PageSize: inputData.TotalRecords,
			SortColumn: inputData.sortColumn,
			SortOrder: inputData.sortValue,
			Status: inputData.StatusData.toString(),
			Files: inputData.FilesData.toString(),
		}

		APIRequest.getPostService(APIUrl.GET_GAMES_TRACK_FILE_GRID_LIST, InputData)
			.then((response) => {
				if (response.Data.length > 0) {
					setinputData((inputData) => ({
						...inputData,
						exportData: response.Data,
						IsExportExcel: true,
					}))
				}
			})
			.catch((error) => {})
	}
	const closeModal = () => {
		setinputData((inputData) => ({ ...inputData, isOpen: false }))
	}
	const reactTable = useRef(null)
	const handlerPagination = (pageNo) => {
		setinputData(
			(inputData) => ({
				...inputData,
				IncrementPage: pageNo,
			})
			// function () { }
		)
		fetch(pageNo)
	}
	return (
		<>
			{Permissions.includes(Permission.G_T_F_VIEW) ? (
				<Spin size='large' spinning={inputData.loading}>
					<div className='p-3'>
						<div className='row pt-0 '>
							<div className='col-sm-6'>
								<label
									// style={{ borderBottom: "4px solid" }}
									className='games_layout form-group mt-0 row col-sm-12 pr-0'
								>
									Uploaded File Status{" "}
								</label>
							</div>
						</div>
						<Card className='w-100 card from-group' style={{marginTop:"13px"}}>
							<Card.Header
								className='text-white'
								onClick={() => setIsExpanded(!inputData.IsExpanded, null)}
							>
								<div className='float-left filter-heading-font' style={{marginLeft:"-12px"}}>Search Uploaded File Status </div>
								<div className='float-right'>
									{inputData.Icons ? (
										<FontAwesomeIcon
											color=' #FFFFFF'
											className='action-icon-font'
											icon={faArrowUp}
											style={{ cursor: "pointer" }}
										></FontAwesomeIcon>
									) : (
										<FontAwesomeIcon
											color=' #FFFFFF'
											className='action-icon-font'
											icon={faArrowDown}
											style={{ cursor: "pointer" }}
										></FontAwesomeIcon>
									)}
								</div>
							</Card.Header>
							<Collapse in={inputData.IsExpanded}>
								<Card.Body>
									<div className='row'>
										<div className='form-group col-sm-4'>
											<label className='pl-0 mb-0'>File Name</label>
											<div>
												<Select
													mode='multiple'
													maxTagCount={1}
													maxTagTextLength={15}
													size='large'
													name='Games'
													allowClear
													showSearch
													bordered={false}
													value={inputData.FilesData}
													onChange={(val) => handleOverAllOnchange("FilesData", val)}
													optionFilterProp='children'
													className='textfiled  pl-0'
													placeholder='Select File Name'
													filterSort={(optionA, optionB) =>
														optionA.children
															.toLowerCase()
															.localeCompare(optionB.children.toLowerCase())
													}
												>
													{listdata.FilesData.map((c) => (
														<Option value={c.OptionID}>{c.OptionData}</Option>
													))}
												</Select>
											</div>
										</div>

										<div className='form-group col-sm-4'>
											<label className='pl-0 mb-0'>Status</label>
											<Select
												mode='multiple'
												maxTagCount={1}
												maxTagTextLength={15}
												className='textfiled '
												style={{ width: "100%" }}
												name='Sport'
												value={inputData.StatusData}
												bordered={false}
												size={"large"}
												allowClear
												showSearch
												placeholder='Select Status'
												optionFilterProp='children'
												onChange={(val) => handleOverAllOnchange("StatusData", val)}
												filterSort={(optionA, optionB) =>
													optionA.children
														.toLowerCase()
														.localeCompare(optionB.children.toLowerCase())
												}
											>
												{listdata.StatusData.map((c) => (
													<Option value={c.OptionID}>{c.OptionData}</Option>
												))}
												{/* {optionItems} */}
											</Select>
										</div>
									</div>
									<div className='col-sm-12 '>
										<div className=' d-flex justify-content-end row'>
										<Button onClick={() => handleClear()} className='mr-2 btn_green'>
												Clear
											</Button>
											<Button
												onClick={() => fetch(0)}
												className=' btn_green'
												disabled={inputData.isSearchBtn}
											>
												Search
											</Button>
											
										</div>
									</div>
								</Card.Body>
							</Collapse>
						</Card>
						<div className='admin-report-table pt-3'>
							<ReactTable
								data={listdata.users}
								columns={columns}
								showPaginationTop={false}
								showPaginationBottom={false}
								minRows={0}
								defaultPageSize={10}
								defaultPage={1}
								onFetchData={fetch}
								ref={reactTable}
								sortable={true}
								multiSort={true}
								manual
								className={"CosmicTable"}
								showPageSizeOptions={false}
								NoDataComponent={NoDataConst}
								defaultSorting={[
									{
										id: "id",
										desc: true,
									},
								]}
								manualPagination={true}
							/>
						</div>
						
							<div className='mt-1'>
							<label>
								<b >Total Records: </b>
								{inputData.TotalRecords > 10
									? inputData.TotalCount + " of " + inputData.TotalRecords
									: inputData.TotalRecords !== 0
									? inputData.TotalRecords
									: 0}
							</label>
							</div>
							<div className="row">
							<div className=" col-sm-4">
								{Permissions.includes(Permission.G_T_F_EXPORT_EXCEL) ? (
									<Button
										className='mr-2 btn_green'
										style={{
											//paddingRight: "0.4rem", paddingLeft: "0.4rem",
											cursor: "pointer",
										}}
										disabled={inputData.exportbtn}
										onClick={handlerExportData}
									>
										<FontAwesomeIcon
											title='Delete'
											color={"#FFFFFF"}
											icon={faDownload}
											// marginRight={"5px !important"}
										></FontAwesomeIcon>{" "}
										Export Excel
									</Button>
								) : null}
							</div>
							
								{inputData.TotalPages > 0 ? (
							// <div  style={{marginTop:"1px"}}>
							// <div className='col-sm-6 d-flex justify-content-end mt-0 pr-0'>
							<div className="col-sm-8 d-flex justify-content-end">
								<Pagination
									totalPages={inputData.TotalPages}
									totalRecords={inputData.TotalRecords}
									paginationCall={handlerPagination}
								/>
						</div>
								):null}
					</div>
					{inputData.IsExportExcel ? (
						<ExportData
							data={inputData.exportData}
							label={inputData.exportLabel}
							filename={inputData.exportFilename}
						/>
					) : null}
					</div>
					<Fragment>
						<Modal
							size='sm'
							//aria-labelledby="contained-modal-title-vcenter"
							//   aria-labelledby="contained-modal-title-vcenter"
							backdrop='static'
							//  centered
							show={inputData.isOpen}
							onHide={closeModal}
						>
							<div className='popup-content'>
								<div className='deletecircle'></div>
								<i className='fa fa-trash-o deleteicon'></i>
								<Modal.Body>
									<div className='pull-left' style={{ margin: "4rem 1rem" }}>
										{" "}
										<h5>{Message.UPLOAD_FILE_DELETE_HEADING}</h5>
										<h6 style={{ color: "darkgray" }}>{Message.UPLOAD_FILE_DELETE_BODY}</h6>
										<h6
											style={{
												color: "darkgray",
												marginRight: "7rem",
												marginBottom: "-4rem",
											}}
										>
											{Message.INVITE_DELETE_CONFIRM1}
										</h6>
									</div>
									<br />
									<div className='pull-right'>
									<Button className='btn_cancel mr-2' onClick={closeModal}>
											No
										</Button>
										<Button
											className='btn_green'
											style={{ height: "2rem", padding: "5px 10px" }}
											onClick={deleteModal}
										>
											Yes
										</Button>
										
									</div>
								</Modal.Body>
							</div>
						</Modal>
					</Fragment>
				</Spin>
			) : null}
		</>
	)
})

const mapPropsToState = (state) => ({
	user: state.userReducer,
})
export default PermissionProvider(connect(mapPropsToState, null)(TrackFiles))
