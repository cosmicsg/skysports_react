import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

export const SessionTimeoutModel = ({
  showModal,
  handleClose,
  handleLogout,
  remainingTime,
}) => {
  return (
    <Modal show={showModal} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Session Timeout!</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        The current session is about to expire in <span>{remainingTime}</span>{" "}
        seconds.
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="danger"
          className="btn_red mr-2"
          style={{ width: "80px !important;" }}
          onClick={handleLogout}
        >
          Logout
        </Button>
        <Button
          variant="primary"
          className="btn_green mr-2"
          style={{ width: "75px;" }}
          onClick={handleClose}
        >
          Continue Session
        </Button>
      </Modal.Footer>
    </Modal>
  );
};
