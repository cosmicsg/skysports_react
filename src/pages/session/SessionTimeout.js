import React, { useState, useRef } from 'react'
import IdleTimer from "react-idle-timer"
import {SessionTimeoutModel} from './SessionTimeoutModel';


let countdownInterval;
let timeout;
const SessionTimeout=({ isAuthenticated, logOut })=> {

    const [timeoutModelOpen, setTimeoutModelOpen] = useState(false);
    const [timeoutCountdown, setTimeoutCountdown] = useState(0);

    const idleTimer = useRef(null);

    const clearSessionTimeout = () =>{
        clearTimeout(timeout);
    };

    const clearSessionInterval = () =>{
        clearInterval(countdownInterval);
    };

    const handleLogout = async(isTimeOut = false) =>{
        try{
            setTimeoutModelOpen(false);
            clearSessionInterval();
            clearSessionTimeout();
            logOut();
        }catch(err){
        }
    }

    const handleContinue = () =>{
        setTimeoutModelOpen(false);
        clearSessionInterval();
        clearSessionTimeout();
    };

    const onActive = () =>{
        if(!timeoutModelOpen){
            clearSessionInterval();
            clearSessionTimeout();
        }
    }

    const onIdle = () =>{
       
        const delay = 1000 * 1;
        if(isAuthenticated && !timeoutModelOpen){
            timeout = setTimeout(() =>{
                let countDown = 30;
                setTimeoutModelOpen(true);
                setTimeoutCountdown(countDown);

                countdownInterval = setInterval(() =>{
                    if(countDown > 0){
                        setTimeoutCountdown(--countDown);
                    }else{
                        handleLogout(true);
                    }
                }, 1000);
            }, delay);
        }
    };

    return (
        <>
            <IdleTimer 
                ref = {idleTimer}
                onActive = {onActive}
                onIdle = {onIdle}
                debounce = {250}
                timeout = {120000}
            />
            <SessionTimeoutModel 
                    showModal={timeoutModelOpen} 
                    handleClose={handleContinue}
                    handleLogout={handleLogout}
                    remainingTime={timeoutCountdown}
        />
        </>
    )
}

export default SessionTimeout
