import { Button } from "antd"
import React, { useState } from "react"
import { Modal } from "react-bootstrap"

const ViewDepartment = (props) => {
    const [viewProps] = useState(props.viewProps)
    const closeModel = () => {
        props.updateStatus()
    }


    return (
        <>
            <Modal
             backdrop="static"
             show={true}>
                <Modal.Header>
                    <p>Manage Department</p>
                    <div className="col-sm-1">
                        <div className="d-flex justify-content-end">
                            <button title="Close" type="button" className="close" onClick={closeModel}>
                                <span style={{ color: "#c29a37" }} area-hidden>
                                    &times;
                                </span>
                            </button>
                        </div>
                    </div>
                </Modal.Header>
                <Modal.Body>
                    <div className="row col-sm-12 form-group">
                        <div className="col-sm-4">
                            <label className="font-weight-bold">Department</label>
                        </div>
                        <div className="col-sm-1"></div>
                        <div className="col-sm-7">
                            <label>{viewProps.DepartmentName}</label>
                        </div>
                    </div>
                    <div className="row col-sm-12 form-group">
                        <div className="col-sm-4">
                            <label className="font-weight-bold">Description</label>
                        </div>
                        <div className="col-sm-1"></div>
                        <div className="col-sm-7">
                            <label>{viewProps.Description}</label>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    )
}
export default ViewDepartment
