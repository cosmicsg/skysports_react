import React, { useState } from "react";
import { Input } from "antd";

// Search Component
const SearchDepartment = (props) => {
  //Search field
  const [search, setSearch] = useState("");

  //Onchange
  const onChange = (e) => {
    const { value } = e.target;
    setSearch(value);
    props.search(value);
  };
  return (
    <>
      <div className="col-sm-5 pr-0">
        <Input
          size="large"
          addonBefore="Search"
          className="label_textbox_left"
          defaultValue=""
          name="search"
          value={search}
          onChange={onChange}
        />
      </div>
    </>
  );
};

export default React.memo(SearchDepartment);
