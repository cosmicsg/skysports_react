import React, { useEffect, useRef, useState } from "react";
import AddDepartment from "./AddDepartment";
import { faDownload, faEdit } from "@fortawesome/free-solid-svg-icons";
import { Button } from "react-bootstrap";
import ReactTable from "react-table";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import {
  APIRequest,
  showNotification,
} from "../../components/api-manager/apiRequest";
import * as APIUrl from "../../components/api-manager/apiConstant";
import SearchDepartment from "./SearchDepartment";
import Pagination from "../../utils/Pagination";
import ViewDepartment from "./ViewDepartment";
import ExportData from "../../utils/ExportData";

const exportLabel = [
  { columnName: "S.No", value: "SerialNo" },
  { columnName: "Department", value: "DepartmentName" },
  { columnName: "Descripition", value: "Description" },
]

//string concat with(...) when the string has more that 30 characters
const tableColumnDataLimit = (str) => {
  if (str !== null && str !== undefined)
    return str.length > 30 ? str.substring(0, 27) + "..." : str;
  else return "";
};

//No records found label text
const NoDataConst = (props) => (
  <span className="table-nodata-style">No records found </span>
);

// Department List component
const DepartmentList = () => {
  const table = useRef(null);
  const [departmentData, setDepartmentData] = useState([]);

  //filter parameters
  const [filterProp, setFilterProp] = useState([]);

  //Export fields properties
  const [exportExcelProps, setExportExcelProps] = useState({
    isExportExcel: false,
    exportData: [],
  });

  //pagination fields properties
  const [paginationProp, setPaginationProp] = useState({
    totalPages: 0,
    totalRecords: 0,
    pageSize: 10,
    totalCount: 0,
  });

  //model popup properties
  const [modelProps, setModelProps] = useState({
    isShowPopup: false,
    isAdd: true,
  });


  //selected row field for edit row
  const [selectedRow, setSelectedRow] = useState({});
  
  const[isRefreshLists, setRefreshList] = useState(false)

  //lifecycle method for load the list when page getting load at first time
  useEffect(() => {
    //refresh List method
    refreshList()
  }, []);

  //Search
  const search = (key) => {
    refreshList(undefined, key);
  };
  //Refresh List
  const refreshList = (pageNo, isExport, search) => {
    let departmentIDs = Array.isArray(filterProp) ? filterProp : [filterProp];

    let page = pageNo > 0 ? pageNo : 1;
    let sortByName = "CreatedOn";
    let orderBy = "DESC";
    if (pageNo && pageNo.sorted !== undefined && pageNo.sorted.length > 0) {
      sortByName = pageNo.sorted[0].id;
      if (pageNo.sorted[0].desc === true) {
        orderBy = "DESC";
      } else {
        orderBy = "ASC";
      }
    }
    let input = {
      DepartmentIDs: departmentIDs.length > 0 ? departmentIDs.join(",") : "",
      DepartmentName: search ? search : "",
      PageNo: page,
      PageSize: paginationProp.pageSize,
      SortColumn: sortByName,
      SortOrder: orderBy,
    };
    APIRequest.getPostService(APIUrl.GET_DEPARTMENTS_LIST, input).then(
      (res) => {
        if (res.Data.length > 0 && isExport === true) {
          if (res.Succeeded)
          setExportExcelProps({
            ...exportExcelProps,
            exportData: res.Data,
            isExportExcel: true,
          })
          setExportExcelProps({
            ...exportExcelProps,
            isExportExcel: false,
          })
        }
        setDepartmentData(res.Data);

        setPaginationProp((prevState) => ({
          ...prevState,
          totalPages: res.Data.length > 0 ? res.Data[0].TotalPages : 0,
          totalRecords: res.Data.length > 0 ? res.Data[0].TotalRows : 0,
          totalCount: res.Data.length >= 1 ? res.Data.length : 0,
        }));
      }
    );
  };

  const viewDepartment = (row) => {
    updateViewDepartmentStatus(row, true)
  }
  const updateViewDepartmentStatus = (row, flag) => {
    setViewDepartmentProps({
      canViewDepartment: flag === true ? flag : false,
      departmentProps: row,
    })
  }

  //Sorting column
  const sortingColumn = (page) => {
    if (page && page.sorted && page.sorted[0]) {
      refreshList(page);
    }
  };

  //Update popup state
  const updatePopupState = (flag, isRefreshList) => {
    setModelProps({
      isAdd: flag ? flag : false,
      isShowPopup: !modelProps.isShowPopup,
    });
    if (isRefreshList) {
      refreshList();
    }
  };

  const [viewDepartmentProps, setViewDepartmentProps] = useState({
    canViewDepartment: false,
    departmentProps: {},
  })

  //Add or Edit
  const addOrEditDepartment = () => {
    updatePopupState(true);
  };

  //Edit record
  const editDepartment = (row) => {
    setSelectedRow(row);
    updatePopupState(undefined, false);
  };

  //Delete record
  const deleteDepartment = (row) => {
    if (row.DepartmentID > 0) {
      APIRequest.getDeleteService(
        APIUrl.DELETE_DEPARTMENT + "/" + row.DepartmentID
      )
        .then((res) => {
          if (res.Data === true) {
            refreshList();
            showNotification("Department deleted successfully.", "success");
          }
        })
        .catch((err) => {
          showNotification("Something went wrong...", "error");
        });
    } else {
      showNotification("Please selected valid items to delete", "error");
    }
  };

  // Pagination call, this would call next or previous page
  const callNextOrPreviousPage = (pageNo) => {
    refreshList(pageNo);
  };

  //Call Export data
  const handleExportData = () => {
    refreshList(0, true)
  }

  // Grid columns
  const columns = [
    {
      Header: () => <div style={{ textAlign: "left" }}>S.No</div>,
      accessor: "SerialNo",
      sortable: false,
      resizable: false,
      className: "tableheader wordwrap",
      headerClassName: "BoldText ColoredText",

      Cell: (row) => {
        return (
          <div className="tabledata">
            {row.value}
          </div>
        );
      },
      width: Math.round(window.innerWidth * 0.04),
    },
    {
      Header: () => <div style={{ textAlign: "left" }}>Department</div>,
      resizable: false,
      accessor: "DepartmentName",
      style: { overflowWrap: "break-word" },
      Cell: (row) => {
        return (
          <div
            className="tabledata wordwrap"
            title={row.original.DepartmentName}
          >
            {tableColumnDataLimit(row.original.DepartmentName)}
          </div>
        );
      },
    },
    {
      Header: () => <div style={{ textAlign: "left" }}>Description</div>,
      resizable: false,
      accessor: "Description",
      style: { overflowWrap: "break-word" },
      Cell: (row) => {
        return (
          <div className="tabledata wordwrap" title={row.original.Description}>
            {tableColumnDataLimit(row.original.Description)}
          </div>
        );
      },
    },
    {
      Header: () => (
        <div className="align-middle">
          {"     "}Action
        </div>
      ),
      accessor: "Action",
      className: "tableheader wordwrap",
      sortable: false,
      resizable: false,
      width: Math.round(window.innerWidth * 0.07),
      filterable: false,
      Cell: (row) => {
        return (
          <div className="tabledata text-center">
            <FontAwesomeIcon
              title="Edit"
              className="mr-2 icon_edit"
              icon={faEdit}
              style={{ cursor: "pointer" }}
              onClick={() => editDepartment(row.original)}
            />
            <FontAwesomeIcon
              title="Edit"
              className="mr-2 icon_eye cursor-pointer"
              onClick={() => viewDepartment(row.original)}
              icon={faEye}
            />
            <FontAwesomeIcon
              title="Delete"
              className="mr-2 icon_Delete"
              icon={faTrashAlt}
              style={{ cursor: "pointer" }}
              onClick={() => deleteDepartment(row.original)}
            />
          </div>
        );
      },
    },
  ];
  return (
    <>
      <div>
        <div className="h-full singletab">
          <div className="games_layout form-group mt-3 row col-sm-12 pr-0">
            <div className="col-sm-3">Manage Department</div>
            <div className="col-sm-9 d-flex justify-content-end">
              <SearchDepartment search={search} />
            </div>
          </div>
        </div>
        <div className="row col-sm-12">
          {modelProps.isShowPopup ? (
            <AddDepartment
              updatePopupState={updatePopupState}
              modelProps={modelProps}
              selectedRow={selectedRow}
            />
          ) : null}
        </div>
        {viewDepartmentProps.canViewDepartment ? (
          <ViewDepartment
            viewProps={viewDepartmentProps.departmentProps}
            updateStatus={updateViewDepartmentStatus}
          />
        ) : null}
        <div className="row col-sm-12 d-flex justify-content-end">
          <Button className="btn_green" onClick={addOrEditDepartment}>
            Add Department
          </Button>
        </div>
        <div className="mr-3 ml-3">
          <ReactTable
            data={departmentData}
            columns={columns}
            showPaginationTop={false}
            showPaginationBottom={false}
            minRows={0}
            defaultPageSize={10}
            defaultPage={1}
            onFetchData={sortingColumn}
            ref={table}
            sortable={true}
            multiSort={true}
            manual
            className={"CosmicTable"}
            showPageSizeOptions={false}
            NoDataComponent={NoDataConst}
            manual
            defaultSorting={[
              {
                id: "id",
                desc: true,
              },
            ]}
            manualPagination={true}
          />
        </div>
        <div className="row col-sm-12 pr-0 ml-0 mt-2">
          <div className="col-sm-6 pr-0 pl-0 mt-1">
            <label>
              <b>Total Records: </b>
              {paginationProp.totalRecords > 10
                ? paginationProp.totalCount +
                " of " +
                paginationProp.totalRecords
                : paginationProp.totalRecords !== 0
                  ? paginationProp.totalRecords
                  : 0}
            </label>
          </div>
        </div>
        <div className="row col-sm-12 form-group">
          <div className="col-sm-4">
            <Button className="btn_green mr-2" onClick={handleExportData} >
              <FontAwesomeIcon title="Export Data" color={"#FFFFFF"} icon={faDownload} />
              Export Excel
            </Button>
          </div>
          {paginationProp.totalPages > 0 ? (
            <div className="row col-sm-8 d-flex justify-content-end pr-0">
              <Pagination
                totalPages={paginationProp.totalPages}
                totalRecords={paginationProp.totalRecords}
                paginationCall={callNextOrPreviousPage}
              ></Pagination>
            </div>
          ) : null}
        </div>
      </div>
      {exportExcelProps.isExportExcel ? (
        <ExportData
          data={exportExcelProps.exportData}
          label={exportLabel}
          filename="Department List"
        />
      ) : null}
    {/* </div> */}

     
    </>
  );
};
export default DepartmentList;
