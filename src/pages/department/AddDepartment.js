import React, { useEffect, useState } from "react";
import { Input } from "antd";
import { Button, Modal } from "react-bootstrap";
import {
  APIRequest,
  showNotification,
} from "../../components/api-manager/apiRequest";
import * as APIUrl from "../../components/api-manager/apiConstant";

const AddDepartment = (props) => {
  const [fields, setFields] = useState({
    department: "",
    description: "",
  });

  const [departmentId, setDepartmentId] = useState(0);

  const [errors, setErrors] = useState({});

  const onChange = (e) => {
    const { name, value } = e.target;

    //set selected field values
    setFields((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    //clear selected field error
    setErrors((prevState) => ({
      ...prevState,
      [name]: "",
    }));
  };

  //Lifecycle method will gets executed automatically when page gets load.
  useEffect(() => {
    //If it is Edit mode then below code will gets executed...
    if (!props.modelProps.isAdd) {
      const { DepartmentID, DepartmentName, Description } = props.selectedRow;
      setFields({
        department: DepartmentName ? DepartmentName : " ",
        description: Description ? Description : " ",
      });
      let id = DepartmentID ? DepartmentID : 0;
      setDepartmentId(id);
    }
  }, []);

  //Validate form fields
  const validateFields = () => {
    const { department, description } = fields;

    const error = {};
    let isErrorExist = false;
    if (department === "" || department === undefined || department === null) {
      isErrorExist = true;
      error["department"] = "Please enter Department Name";
    }
    if (
      description === "" ||
      description === undefined ||
      description === null
    ) {
      isErrorExist = true;
      error["description"] = "Please enter Description";
    }
    setErrors(error);

    return isErrorExist;
  };

  //Save
  const save = () => {
    let isError = validateFields();
    if (!isError) {
      let input = {
        DepartmentName: fields.department,
        Description: fields.description,
        IsEdit: props.modelProps.isAdd ? false : true,
        DepartmentID: departmentId ? departmentId : 0,
      };
      APIRequest.getPostService(APIUrl.SAVE_DEPARTMENT, input).then((res) => {
        if (res.Succeeded && res.Data === true) {
          props.updatePopupState(undefined, true);
          if (props.modelProps.isAdd)
            showNotification("Department added successfully.", "success");
          else showNotification("Department updated successfully.", "success");
        } else {
          showNotification("Something went wrong", "error");
        }
      });
    }
  };

  //Close popup
  const closePopup = () => {
    props.updatePopupState();
  };

  return (
    <>
      <Modal
        className="user-list-toggle-main1"
        size="lg"
        backdrop="static"
        show={true}
      >
        <Modal.Header className="pb-0 border-bottom-0 popup_header font-weight-bold">
          <p>{props.modelProps.isAdd ? "Add " : "Edit "} Department</p>
          <div className="col-md-1">
            <div className="d-flex justify-content-end">
              <button
                title="Close"
                type="button"
                className="close"
                data-dismiss="modal"
                area-aria-label="Close"
                onClick={closePopup}
              >
                <span style={{ color: "#c29a37" }} area-hidden="true">
                  &times;
                </span>
              </button>
            </div>
          </div>
        </Modal.Header>
        <Modal.Body>
          <div className="row col-sm-12 form-group">
            <div className="col-sm-6">
              <label>
                Departmant Name<span className="text-danger">*</span>
              </label>
              <Input
                type="text"
                title="department"
                name="department"
                size="large"
                autoComplete="off"
                value={fields.department}
                onChange={onChange}
                bordered={false}
                className="textfiled w-80"
                placeholder="Department Name"
              />
              <span className="text-danger">{errors.department}</span>
            </div>
            <div className="col-sm-6">
              <label>
                Description<span className="text-danger">*</span>
              </label>
              <Input
                type="text"
                title="description"
                name="description"
                size="large"
                autoComplete="off"
                value={fields.description}
                onChange={onChange}
                bordered={false}
                className="textfiled w-80"
                placeholder="Description"
              />
              <span className="text-danger">{errors.description}</span>
            </div>
          </div>
          <div className="row col-sm-12 mt-3 form-group">
            <div className="col-sm-12 d-flex justify-content-end ">
            {/* <Button className="btn_green mr-2" onClick={closePopup}>
                Close
              </Button> */}
              <Button className="btn_green" onClick={save}>
                Save
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default React.memo(AddDepartment);
