import React from "react"
import { Button, Modal, Col, Form } from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import { faEdit, faSave } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { APIRequest } from "../../components/api-manager/apiRequest"
import ReactTable from "react-table"
import "../../styles/CosmicTable.css"
import "../../App"
import { notification, Select } from "antd"
import "antd/dist/antd.css"
import { faPlusCircle, faTrashAlt, faCheck } from "@fortawesome/free-solid-svg-icons"
import { Checkbox } from 'antd';

const { Option } = Select
const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete ' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	} else if (flag === 2) {
		notification[type]({
			message: description,
			description: "",
		})
	}
}

const NoDataConst = (props) => <span className='table-nodata-style'>No records found. </span>

const openModal = (rowid, self, flg, id) => {
	self.setState({ isOpen: true, isDeleteId: id, isDeleteRowId: rowid, isDeleteflag: flg })
}

class OtherInfo extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			isEditable: true,
			ReligionID: 0,
			Religion: "",
			BloodGroupID: 0,
			BloodGroup: "",
			ReligionIDList: [],
			BloodGroupIDList: [],
			loading: false,
			rowcount: 0,
			selected: {},
			fields: [],
			errors: {
				AddanotherLanguage: "",
			},
			isOpen: false,
			isDelete: false,
			isDeleteId: 0,
			isDeleteflag: 0,
			addLanguagetxt: "Add Language",
			LanguageID: [],
			LanguageList: [],
			Write: false,
			Speak: false,
			Read: false,
		}
		this.fetch = this.fetch.bind(this)
		this.table = React.createRef()
		this.handleAddRow = this.handleAddRow.bind(this)
		this.deleteModal = this.deleteModal.bind(this)
	}

	async fetch(state) {
		let thisSelf = this
		APIRequest.getGetService(APIUrl.GET_OTHER_INFO)
			.then((response) => {
				if (response.Succeeded) {
					thisSelf.setState({
						fields: response.Data.Language != null ? response.Data.Language : [],
						ReligionID: response.Data.ReligionID != null ? response.Data.ReligionID : 0,
						Religion: response.Data.Religion != null ? response.Data.Religion : "",
						BloodGroupID: response.Data.BloodGroupID != null ? response.Data.BloodGroupID : 0,
						BloodGroup: response.Data.BloodGroup != null ? response.Data.BloodGroup : "",
						isEditable: false,
					})
				} else {
					thisSelf.setState({
						isEditable: true,
					})
				}
				thisSelf.setState({ loading: false, users: response.Data })
			})
			.catch((error) => {
				thisSelf.setState({ loading: false })
			})
	}

	editProfile = () => {
		this.setState({
			isEditable: true,
		})
	}

	// onSubmit = () => {
	// 	//VALIDATE
	// 	var errors = []

	// 	if (this.state.LanguageID === "") {
	// 		errors.push("LanguageID")
	// 	}

	// 	// if (this.state.Proficiency === "") {
	// 	// 	errors.push("Proficiency")
	// 	// }

	// 	if (this.state.ReligionID.length === 0) {
	// 		errors.push("ReligionID")
	// 	}
	// 	if (this.state.BloodGroupID.length === 0) {
	// 		errors.push("BloodGroupID")
	// 	}

	// 	this.setState({
	// 		errors: errors,
	// 	})

	// 	if (errors.length > 0) {
	// 		return false
	// 	} else {
	// 		this.inputData = {
	// 			LanguageID: this.state.LanguageID,
	// 			ReligionID: this.state.ReligionID,
	// 			BloodGroupID: this.state.BloodGroupID,
	// 		}
	// 	}

	// 	let thisSelf = this
	// 	APIRequest.getPostService(APIUrl.SAVE_OTHER_INFO, this.inputData)
	// 		.then((response) => {
	// 			if (response.Succeeded) {
	// 				thisSelf.setState({
	// 					isEditable: false,
	// 				})
	// 			} else {
	// 				thisSelf.setState({
	// 					isEditable: true,
	// 				})
	// 			}
	// 			thisSelf.setState({ loading: false, users: response.Data })
	// 		})
	// 		.catch((error) => {
	// 			thisSelf.setState({ loading: false })
	// 		})
	// }

	onChange = (e) => {
		this.setState({
			...this.state,
			[e.target.name]: e.target.value,
		})
	}
	deleteModal = async () => {
		let id = this.state.isDeleteId
		let Rowid = this.state.isDeleteRowId
		let fields = this.state.fields
		fields.splice(Rowid, 1)
		this.setState({ fields })
		this.closeModal()
		this.setState({ errors: [] })
		let rowId = this.state.selected
		if (rowId !== undefined) {
			delete rowId[id]
		}
		openNotificationWithIcon("success", "Record has been deleted", 1)
		if (Object.keys(this.state.selected).length === 0) {
			this.setState({ btnDelete: true, btnSend: true })
		}
		if (this.state.fields.length === 0) {
			this.setState({
				rowcount: 0,
				addUsertxt: "Add Language",
			})
		}
	}
	closeModal = () => {
		this.setState({ isOpen: false })
	}

	handleCheck = (cellInfo, event) => {
		let fields = [...this.state.fields]
		fields[cellInfo.index][cellInfo.column.id] = event.target.checked
		this.setState({ fields })
	}
	handleAddRow = (e) => {
		e.preventDefault()
		let count = this.state.rowcount
		let rowData = this.state.fields.length
		let fields = this.state.fields
		for (let i = 0; i < fields.length; i++) {
			//let data = this.state.fields[i]
		}
		if (rowData === 0) {
			count = 0
			this.setState({
				addLanguagetxt: "Add another Language",
			})
		}
		this.setState({
			rowcount: count + 1,
		})
		const values = [...this.state.fields]
		values.push({
			Write: false,
			Speak: false,
			Read: false,
			id: count,
		})
		this.setState({
			fields: values,
			errors: {
				AddanotherLanguage: "",
			},
		})
	}
	handleDropdownChange = (cellInfo, event) => {
		let fields = [...this.state.fields]
		fields[cellInfo.index][cellInfo.column.id] = event
		this.setState({ fields })
		//let data = [cellInfo.column.id].toString()
		//let id = [cellInfo.index].toString()
		//let value = event.toString()
	}

	handleInputChange = (cellInfo, event) => {
		let fields = [...this.state.fields]
		fields[cellInfo.index][cellInfo.column.id] = event.target.value
		this.setState({ fields })
		// let fullname = [cellInfo.column.id].toString()
		// let id = [cellInfo.index].toString()
		// if (fullname === "fullname" && event.target.value !== "") {
		// 	var myElementID = document.getElementById("Fullname_tbl" + id)
		// 	var element = document.getElementById("Fullname_tbl" + id)
		// }
	}

	handleBtnSendALL = (self) => {
		let selectdata = self.state.fields
		var InputData = []
		var obj = {}
		// let result = 0
		// let LanguageID = 0
		for (var i = 0; i < selectdata.length; i++) {
			let data = self.state.fields[i]
			// let id = data.id
			// let LanguageID = document.getElementById("Language_tbl" + i)
			// let Read = document.getElementById("Read_tbl" + i)
			// let Write = document.getElementById("Write_tbl" + i)
			// let Speak = document.getElementById("Speak_tbl" + i)

			obj["LanguageID"] = data.LanguageID
			obj["Read"] = data.Read
			obj["Write"] = data.Write
			obj["Speak"] = data.Speak
			obj["BloodGroupID"] = this.state.BloodGroupID
			obj["ReligionID"] = this.state.ReligionID

			InputData.push(obj)
			obj = {}
		}
		let thisSelf = this
		APIRequest.getPostService(APIUrl.ADD_USER_LANGUAGE, InputData)
			.then((response) => {
				if (response.Succeeded === true) {
					thisSelf.setState({
						isEditable: false,
					})
					this.fetch(this.state)
				} else {
					thisSelf.setState({
						isEditable: true,
					})
				}
				thisSelf.setState({ loading: false, users: response.Data })
			})
			.catch((error) => {})
	}
	renderEditableLanguage = (cellInfo) => {
		let cellValue = ""
		let cellValuetext = ""

		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
			cellValuetext = this.state.fields[cellInfo.index].Language
		}
		if (this.state.isEditable) {
			return (
				<div
					id={"Language_tbl" + cellInfo.index}
					className={"tbl_OtherInfo Language_tbl" + cellInfo.index}
				>
					<Select
						style={{ width: "100%" }}
						value={cellValue}
						size={"large"}
						showSearch
						name='LanguageID'
						bordered={false}
						placeholder='Select Language'
						optionFilterProp='children'
						defaultValue='Select'
						onChange={this.handleDropdownChange.bind(null, cellInfo)}
						filterSort={(optionA, optionB) =>
							optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
						}
					>
						{this.state.LanguageList.map((c) => (
							<Option value={c.OptionID}>{c.OptionData}</Option>
						))}
					</Select>
				</div>
			)
		} else {
			if (cellValuetext !== "") {
				return <span>{cellValuetext}</span>
			} else {
				return <span></span>
			}
		}
	}
	renderEditableReadCheckBox = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		if (this.state.isEditable) {
			return (
				<div id={"Read_tbl" + cellInfo.index} className={"tbl_OtherInfo Read_tbl" + cellInfo.index}>
					<Checkbox
						
						style={{ width: "100%" }}
						value={cellValue}
						size={"large"}
						name='Read'
						bordered={false}
						checked={cellValue}
						onChange={this.handleCheck.bind(null, cellInfo)}
						class="geekmark"
					></Checkbox>
				</div>
			)
		} else {
			if (cellValue) {
				return (
					<div className='default-iconcolor'>
						<FontAwesomeIcon title='Checked' icon={faCheck}></FontAwesomeIcon>
					</div>
				)
			} else {
				return <div></div>
			}
		}
	}

	renderEditableWriteCheckBox = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		if (this.state.isEditable) {
			return (
				<div
					id={"Write_tbl" + cellInfo.index}
					className={"tbl_OtherInfo Write_tbl" + cellInfo.index}
				>
					
					<Checkbox
						
						style={{ width: "100%" }}
						value={cellValue}
						size={"large"}
						name='Write'
						bordered={false}
						checked={cellValue}
						onChange={this.handleCheck.bind(null, cellInfo)}
						class="geekmark"
					></Checkbox>
					
				</div>
			)
		} else {
			if (cellValue) {
				return (
					<div className='default-iconcolor'>
						<FontAwesomeIcon title='Checked' icon={faCheck}></FontAwesomeIcon>
					</div>
				)
			} else {
				return <div></div>
			}
		}
	}
	renderEditableSpeakCheckBox = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		if (this.state.isEditable) {
			return (
				<div
					id={"Speak_tbl" + cellInfo.index}
					className={"tbl_OtherInfo Speak_tbl" + cellInfo.index}
				>
					
					<Checkbox
						
						style={{ width: "100%" }}
						value={cellValue}
						size={"large"}
						name='Speak'
						bordered={false}
						checked={cellValue}
						onChange={this.handleCheck.bind(null, cellInfo)}
						class="geekmark"
					></Checkbox>
				</div>
			)
		} else {
			if (cellValue) {
				return (
					<div className='default-iconcolor'>
						<FontAwesomeIcon title='Checked' icon={faCheck}></FontAwesomeIcon>
					</div>
				)
			} else {
				return <div></div>
			}
		}
	}
	handleTest = () => {
		alert("test")
	}
	componentDidMount() {
		let self = this
		this.fetch(this.state)
		APIRequest.getGetService(APIUrl.LANGUAGE_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						LanguageList: response.Data,
					})
					//response.Data
				}
			})
			.catch((error) => {})

		APIRequest.getGetService(APIUrl.RELIGION_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						ReligionIDList: response.Data,
					})
					//response.Data
				}
			})
			.catch((error) => {})

		APIRequest.getGetService(APIUrl.BLOOD_GROUP_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						BloodGroupIDList: response.Data,
					})
					//response.Data
				}
			})
			.catch((error) => {})
	}
	handlerLoadingStart = () => {
		this.setState({ loading: true })
	}
	// It is used to over all loader stop
	handlerLoadingStop = () => {
		this.setState({ loading: false })
	}
	render() {
		const columns = [
			{
				Header: () => <div className='otherInfo-lang-pad pl-2'>Language</div>,
				accessor: "LanguageID",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader other-info-tabledata",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableLanguage,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='text-center pl-2'>Read</div>,
				accessor: "Read",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableReadCheckBox,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='text-center pl-2'>Write</div>,
				accessor: "Write",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableWriteCheckBox,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='text-center pl-2'>Speak</div>,
				accessor: "Speak",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader",
				sortable: false,
				resizable: false,
				class:"main",
				Cell: this.renderEditableSpeakCheckBox,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className={this.state.isEditable ? "text-center" : "hidden"}></div>,
				accessor: "Action",
				className: "tableheader",
				resizable: false,
				sortable: false,
				filterable: false,
				minWidth: 30,
				width: 70,
				maxWidth: 80,
				id: "delete",
				//accessor: (str) => "delete",
				Cell: (row) => (
					<div className='mt-2'>
						{this.state.isEditable ? (
							<span
								className='icon_Delete '
								onClick={() => openModal(row.index, this, 3, row.original.id)} //{
							>
								<FontAwesomeIcon title='Delete' icon={faTrashAlt}></FontAwesomeIcon>
							</span>
						) : (
							<span></span>
						)}
					</div>
				),
			},
		]
		const { isEditable } = this.state
		return (
			<div className='margin-top-2'>
				<Form>
					<div className='row'>
						<Form.Group className='col-md-12' controlId='formPlaintextLanguageID'>
							<Form.Label className='default-textstyle'>Language</Form.Label>
							<div className='admin-report-table otherInfo-tbl-border'>
								<div className=' d-flex justify-content-start pt-1 '>
									<p className='tab_form_content'></p>
								</div>

								<ReactTable
									data={this.state.fields}
									columns={columns}
									showPaginationTop={false}
									showPaginationBottom={false}
									minRows={0}
									ref={this.table}
									sortable={false}
									multiSort={false}
									NoDataComponent={NoDataConst}
									manual
									style={{ overflow: "wrap" }}
									className='OtherInfoTable form-group'
									manualPagination={false}
								/>
								<div className='otherInfo-Add-btn'>
									{this.state.isEditable ? (
										<button
											onClick={this.handleAddRow}
											className='btn-default border-0 bg-white link'
										>
											{this.state.addLanguagetxt}{" "}
											<FontAwesomeIcon icon={faPlusCircle}></FontAwesomeIcon>
										</button>
									) : (
										<div></div>
									)}
								</div>
								{this.state.isOpen ? (
									<>
										<Modal
											size='sm'
											backdrop='static'
											show={this.state.isOpen}
											onHide={this.closeModal}
										>
											<div className='popup-content'>
												<div className='deletecircle'></div>
												<i className='fa fa-trash-o deleteicon'></i>
												<Modal.Body >
													<p className='pull-left' style={{ margin: "4rem 1rem" }}>
														{" "}
														<h5>You are about to delete an Language</h5>
														<h6 style={{ color: "darkgray" }}>
															This will delete your Language from the list
														</h6>
														<h6
															style={{
																color: "darkgray",
																marginRight: "7rem",
																marginBottom: "-3rem",
															}}
														>
															Are you sure?
														</h6>
													</p>
													<br />
													<div className='pull-right'>
														<Button className='btn_green' onClick={this.deleteModal}>
															Yes
														</Button>
														<Button className='btn_cancel mr-2' onClick={this.closeModal}>
															No
														</Button>
													</div>
												</Modal.Body>
											</div>
										</Modal>
									</>
								) : null}
							</div>
						</Form.Group>
					</div>
					<div className='row'>
						<Form.Group className='col-md-6' controlId='formPlaintextBloodGroupID'>
							<Form.Label className='default-textstyle' column>
								Blood Group
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										as='select'
										componentClass='select'
										placeholder='select'
										className='w-100 m-0'
										name='BloodGroupID'
										value={this.state.BloodGroupID}
										onChange={this.onChange}
									>
										<option value='0'>select</option>
										{this.state.BloodGroupIDList.map((c) => (
											<option value={c.OptionID}>{c.OptionData}</option>
										))}
									</Form.Control>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.BloodGroup}</span>
								</Col>
							)}
						</Form.Group>

						<Form.Group className='col-md-6' controlId='formPlaintextReligionID'>
							<Form.Label className='default-textstyle' column>
								Religion
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										as='select'
										componentClass='select'
										placeholder='select'
										className='w-100 m-0'
										name='ReligionID'
										value={this.state.ReligionID}
										onChange={this.onChange}
									>
										<option value='0'>select</option>
										{this.state.ReligionIDList.map((c) => (
											<option value={c.OptionID}>{c.OptionData}</option>
										))}
									</Form.Control>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.Religion}</span>
								</Col>
							)}
						</Form.Group>
					</div>
				</Form>
				<div className='edit-icon-postion'>
					{isEditable ? (
						<button
							disabled={this.state.btnDelete}
							className='btn btn-primary btn_green pull-right waves-effect waves-light btn-xs  mr-2 btn btn-primary transbox-body-content transbox-body-tab-button'
							bsStyle='danger'
							bsSize='small'
							onClick={() => this.handleBtnSendALL(this)}
						>
							<FontAwesomeIcon
								title='Delete'
								color={"#FFFFFF"}
								icon={faSave}
								className='mr-0'
							></FontAwesomeIcon>{" "}
							<span>Save</span>
						</button>
					) : (
						// <button className="pull-right" onClick={this.onSubmit}>Save</button>

						<button
							disabled={this.state.btnDelete}
							className='btn btn-primary btn_green pull-right waves-effect waves-light btn-xs  mr-2 btn btn-primary transbox-body-content transbox-body-tab-button'
							bsStyle='danger'
							bsSize='small'
							onClick={this.editProfile}
						>
							<FontAwesomeIcon
								title='Delete'
								color={"#FFFFFF"}
								icon={faEdit}
								className='mr-0'
							></FontAwesomeIcon>{" "}
							Profile
						</button>
					)}
				</div>
			</div>
		)
	}
}
export default OtherInfo
