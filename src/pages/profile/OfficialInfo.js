import React from "react"
import { Col, Form } from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { APIRequest } from "../../components/api-manager/apiRequest"

class OfficialInfo extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			CommitteeName: "",
			CouncilName: "",
			AssociationName: "",
			ClubName: "",
			CoachName: "",
			Sports: "",
			Role: "",
			Country: "",
		}

		this.fetch = this.fetch.bind(this)
	}
	async fetch(state) {
		const fetchURL = APIUrl.GET_OFFICIAL_INFO

		let thisSelf = this
		APIRequest.getGetService(fetchURL)
			.then((response) => {
				if (response.Succeeded) {
					thisSelf.setState({
						CommitteeName: response.Data.CommitteeName != null ? response.Data.CommitteeName : "",
						CouncilName: response.Data.CouncilName != null ? response.Data.CouncilName : "",
						AssociationName:
							response.Data.AssociationName != null ? response.Data.AssociationName : "",
						ClubName: response.Data.ClubName != null ? response.Data.ClubName : "",
						CoachName: response.Data.CoachName != null ? response.Data.CoachName : "",
						Sports: response.Data.Sports != null ? response.Data.Sports : "",
						Role: response.Data.Role != null ? response.Data.Role : "",
						Country: response.Data.Country != null ? response.Data.Country : "",
					})
				} else {
				}
			})
			.catch((error) => {
				thisSelf.setState({ loading: false })
			})
	}
	componentDidMount() {
		this.fetch(this.state)
	}
	render() {
		return (
			<div className='margin-top-2'>
				{this.state.CommitteeName !== "" &&
				this.state.CouncilName === "" &&
				this.state.AssociationName === "" &&
				this.state.ClubName === "" ? (
					<Form>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextCommitteeName'>
								<Form.Label className='default-textstyle' column>
									Committee Name
								</Form.Label>
								<Col className='info-input-padding m-0'>
									<span>{this.state.CommitteeName}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextCoachName'>
								<Form.Label className='default-textstyle' column>
									Coach Name
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.CoachName}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextSports'>
								<Form.Label className='default-textstyle' column>
									Sports
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Sports}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextRole'>
								<Form.Label className='default-textstyle' column>
									Role
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Role}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextCountry'>
								<Form.Label className='default-textstyle' column>
									Country
								</Form.Label>
								<Col className='info-input-padding m-0'>
									<span>{this.state.Country}</span>
								</Col>
							</Form.Group>
						</div>
					</Form>
				) : null}
				{this.state.CouncilName !== "" &&
				this.state.AssociationName === "" &&
				this.state.ClubName === "" ? (
					<Form>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextCommitteeName'>
								<Form.Label className='default-textstyle' column>
									Committee Name
								</Form.Label>
								<Col className='info-input-padding m-0'>
									<span>{this.state.CommitteeName}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextCouncilName'>
								<Form.Label className='default-textstyle' column>
									Council Name
								</Form.Label>
								<Col className='info-input-padding m-0'>
									<span>{this.state.CouncilName}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextCoachName'>
								<Form.Label className='default-textstyle' column>
									Coach Name
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.CoachName}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextSports'>
								<Form.Label className='default-textstyle' column>
									Sports
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Sports}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextRole'>
								<Form.Label className='default-textstyle' column>
									Role
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Role}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextCountry'>
								<Form.Label className='default-textstyle' column>
									Country
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Country}</span>
								</Col>
							</Form.Group>
						</div>
					</Form>
				) : null}

				{this.state.AssociationName !== "" && this.state.ClubName === "" ? (
					<Form>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextCommitteeName'>
								<Form.Label className='default-textstyle' column>
									Committee Name
								</Form.Label>
								<Col className='info-input-padding m-0'>
									<span>{this.state.CommitteeName}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextCouncilName'>
								<Form.Label className='default-textstyle' column>
									Council Name
								</Form.Label>
								<Col className='info-input-padding m-0'>
									<span>{this.state.CouncilName}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextAssociationName'>
								<Form.Label className='default-textstyle' column>
									Association Name
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.AssociationName}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextCoachName'>
								<Form.Label className='default-textstyle' column>
									Coach Name
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.CoachName}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextSports'>
								<Form.Label className='default-textstyle' column>
									Sports
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Sports}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextRole'>
								<Form.Label className='default-textstyle' column>
									Role
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Role}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextCountry'>
								<Form.Label className='default-textstyle' column>
									Country
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Country}</span>
								</Col>
							</Form.Group>
						</div>
					</Form>
				) : null}

				{this.state.ClubName !== "" ? (
					<Form>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextCommitteeName'>
								<Form.Label className='default-textstyle' column>
									Committee Name
								</Form.Label>
								<Col className='info-input-padding m-0'>
									<span>{this.state.CommitteeName}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextCouncilName'>
								<Form.Label className='default-textstyle' column>
									Council Name
								</Form.Label>
								<Col className='info-input-padding m-0'>
									<span>{this.state.CouncilName}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextAssociationName'>
								<Form.Label className='default-textstyle' column>
									Association Name
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.AssociationName}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextClubName'>
								<Form.Label className='default-textstyle' column>
									Club Name
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.ClubName}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextCoachName'>
								<Form.Label className='default-textstyle' column>
									Coach Name
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.CoachName}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextSports'>
								<Form.Label className='default-textstyle' column>
									Sports
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Sports}</span>
								</Col>
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextRole'>
								<Form.Label className='default-textstyle' column>
									Role
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Role}</span>
								</Col>
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextCountry'>
								<Form.Label className='default-textstyle' column>
									Country
								</Form.Label>

								<Col className='info-input-padding m-0'>
									<span>{this.state.Country}</span>
								</Col>
							</Form.Group>
						</div>
					</Form>
				) : null}
				{/* <div className='row'>
						<Form.Group className='col-md-6' controlId='formPlaintextClubAddress'>
							<Form.Label className='default-textstyle' column>
								Club Address
							</Form.Label>
							
								<Col className='info-input-padding m-0'>
									<span>#31, Method Street, Dubai, UAE.</span>
								</Col>
						</Form.Group>

						<Form.Group className='col-md-6' controlId='formPlaintextStreet'>
							<Form.Label className='default-textstyle' column>
								Street
							</Form.Label>
							
								<Col className='info-input-padding m-0'>
									<span>3rd Cross Street</span>
								</Col>
						</Form.Group>
					</div>
					<div className='row'>
						<Form.Group className='col-md-6' controlId='formPlaintextCity'>
							<Form.Label className='default-textstyle' column>
								City
							</Form.Label>
							
								<Col className='info-input-padding m-0'>
									<span>Rainbow</span>
								</Col>
						</Form.Group>

						<Form.Group className='col-md-6' controlId='formPlaintextZip'>
							<Form.Label className='default-textstyle' column>
								Zip
							</Form.Label>
							
								<Col className='info-input-padding m-0'>
									<span>05567</span>
								</Col>
						</Form.Group>
					</div>
					<div className='row'>
						<Form.Group className='col-md-6' controlId='formPlaintextCountryRegion'>
							<Form.Label className='default-textstyle' column>
								Country / region
							</Form.Label>
							
								<Col className='info-input-padding m-0'>
									<span>Dubai</span>
								</Col>
						</Form.Group>
					</div> */}
			</div>
		)
	}
}
export default OfficialInfo
