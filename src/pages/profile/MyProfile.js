import React from "react"
import { Spin } from "antd"
import { Tabs, Tab, Row, Col } from "react-bootstrap"
import PersonalInfo from "./PersonaInfo"
import ContactInfo from "./ContactInfo"
import OfficialInfo from "./OfficialInfo"
import OtherInfo from "./OtherInfo"
import PhotoUpload from "./PhotoUpload"
import "../../styles/menu.css"
import {
	faUser,
	faUserCircle,
	faUserFriends,
	faAddressBook,
} from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { APIRequest } from "../../components/api-manager/apiRequest"
import { connect } from "react-redux"

class viewProfile extends React.Component {
	constructor(props) {
		super(props)
		this.childRef = React.createRef()

		this.state = {
			FullName: "",
			Roles: "",
			EditContactInfo: false,
			EditOfficialInfo: false,
			EditOtherInfo: false,
			image: "",
			files: [],
			fileList: [],
			loading: true,
		}
		this.fetch = this.fetch.bind(this)
	}
	async fetch(state) {
		const fetchURL = APIUrl.GET_MY_PROFILE_INFO
		let thisSelf = this
		APIRequest.getGetService(fetchURL)
			.then((response) => {
				thisSelf.setState({
					loading: false,
				})
				if (response.Succeeded) {
					thisSelf.setState({
						...thisSelf.state,
						FullName: response.Data.FullName,
						Roles: response.Data.Roles,
					})
				} else {
				}
			})
			.catch((error) => {
				thisSelf.setState({ loading: false })
			})
	}
	handlePersonal = (IsfromPersonalSave) => {
		this.setState({ EditContactInfo: IsfromPersonalSave })
	}
	ChangedFullName = (ChangeFullName) => {
		this.setState({
			...this.state,
			FullName: ChangeFullName,
		})
	}

	handleContact = (IsfromContactSave) => {
		this.setState({
			EditOfficialInfo: IsfromContactSave,
			EditOtherInfo: IsfromContactSave,
		})
	}

	componentDidMount() {
		this.fetch(this.state)
	}
	render() {
		const defaultTab =
			!this.props.user.ProfileContactCompleted && this.props.user.ProfilePersonalCompleted
				? "ContactInfo"
				: "personalInfo"
		return (
			<div>
				<Spin size='large' spinning={this.state.loading} tip='Loading...'>
					<div className='profile-gradiant m-0 profile-height-assign'>
						<div className='row m-0 profile-height-assign profile-avatar-layout'>
							<div className='col-xl-2 col-md-4 col-xs-12 profile-header-container photo-section  p-0'>
								<PhotoUpload />
							</div>
							<div className='col-xl-4 col-md-2 col-xs-12' style={{ padding: "6% 0% 0% 0%" }}>
								<span
									className='user-profile-head-font profile-header-txtfont'
									title={this.state.FullName}
								>
									{this.state.FullName}
								</span>
								<br />
								<div className='user-profile-head-font wrap-80' title={this.state.Roles}>
									Roles - {this.state.Roles}
								</div>
							</div>
							<div className='col-xl-6 col-md-6 col-xs-12' style={{ padding: "6% 0% 0% 0%" }}></div>
						</div>
					</div>
					<Row gutter={[8, 8]} className='overflow-hidden sectoion-align'>
						<Col className='h-full p-0'>
							<Tabs
								className='profile-tab'
								defaultActiveKey={defaultTab}
								id='controlled-tab-example'
							>
								<Tab
									eventKey='personalInfo'
									title={
										<span className='tab-header-text'>
											<i className='tab-header-icon-font'>
												<FontAwesomeIcon icon={faUser} />
											</i>
											Personal Info
										</span>
									}
								>
									{
										<PersonalInfo
											onSelectPersonal={this.handlePersonal}
											onChangedFullName={this.ChangedFullName}
										/>
									}
								</Tab>
								{this.state.EditContactInfo ? (
									<Tab
										eventKey='ContactInfo'
										title={
											<span>
												<i className='tab-header-icon-font'>
													<FontAwesomeIcon icon={faAddressBook} />
												</i>
												Contact Info
											</span>
										}
									>
										{<ContactInfo onSelectContact={this.handleContact} />}
									</Tab>
								) : (
									<Tab
										disabled
										eventKey='ContactInfo'
										title={
											<span>
												<i className='tab-header-icon-font'>
													<FontAwesomeIcon icon={faAddressBook} />
												</i>
												Contact Info
											</span>
										}
									>
										{<ContactInfo />}
									</Tab>
								)}

								{this.state.EditOfficialInfo ? (
									<Tab
										eventKey='OfficialInfo'
										title={
											<span>
												<i className='tab-header-icon-font'>
													<FontAwesomeIcon icon={faUserFriends} />
												</i>
												Official Info
											</span>
										}
									>
										{<OfficialInfo />}
									</Tab>
								) : (
									<Tab
										disabled
										eventKey='OfficialInfo'
										title={
											<span>
												<i className='tab-header-icon-font'>
													<FontAwesomeIcon icon={faUserFriends} />
												</i>
												Official Info
											</span>
										}
									>
										{<OfficialInfo />}
									</Tab>
								)}

								{this.state.EditOtherInfo ? (
									<Tab
										eventKey='OtherInfo'
										title={
											<span>
												<i className='tab-header-icon-font'>
													<FontAwesomeIcon icon={faUserCircle} />
												</i>
												Other Info
											</span>
										}
									>
										{<OtherInfo />}
									</Tab>
								) : (
									<Tab
										disabled
										eventKey='OtherInfo'
										title={
											<span>
												<i className='tab-header-icon-font'>
													<FontAwesomeIcon icon={faUserCircle} />
												</i>
												Other Info
											</span>
										}
									>
										{<OtherInfo />}
									</Tab>
								)}
							</Tabs>
						</Col>
					</Row>
				</Spin>
			</div>
		)
	}
}
const mapPropsToState = (state) => ({
	user: state.userReducer,
})

export default connect(mapPropsToState, null)(viewProfile)
