import React from "react"
import { Col, Form } from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import { faEdit, faSave } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { APIRequest } from "../../components/api-manager/apiRequest"
import * as ReduxAction from "../../components/redux/actions/userAction"
import { connect } from "react-redux"


class ContactInfo extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			isEditable: false,
			BlockNo: "",
			FloorNo: "",
			Street: "",
			City: "",
			State: "",
			PostalCode: 0,
			CountryID: [],
			Country: "",
			RegionID: [],
			Region: "",
			Email: "",
			AltEmail: "",
			Mobile: "",
			AltMobile: "",
			Office: "",
			Home: "",
			OfficeBlockNo: "",
			OfficeFloorNo: "",
			OfficeStreet: "",
			OfficeCity: "",
			OfficeState: "",
			OfficePostalCode: 0,
			OfficeCountryID: [],
			OfficeCountry: "",
			OfficeRegionID: [],
			OfficeRegion: "",
			SameAsRegisteredAddress: false,
			OfficeSameAsRegisteredAddress: false,
			CorrespondingSameAsRegisteredAddress: false,
			CorrespondingBlockNo: "",
			CorrespondingFloorNo: "",
			CorrespondingStreet: "",
			CorrespondingCity: "",
			CorrespondingState: "",
			CorrespondingPostalCode: 0,
			CorrespondingCountryID: [],
			CorrespondingCountry: "",
			CorrespondingRegionID: [],
			CorrespondingRegion: "",
			CountryIDList: [],
			RegionIDList: [],
			errors: [],
			PrimarySubmit: true,
			IsContactInfoExist: 0,
			IsSaveContactInfo: false,
		}
		this.fetch = this.fetch.bind(this)
		this.handleOfficeChange = this.handleOfficeChange.bind(this)
		this.handleCorrespondingChange = this.handleCorrespondingChange.bind(this)
	}

	async fetch(state) {
		const fetchURL = APIUrl.GET_CONTACT_INFO
		let thisSelf = this
		APIRequest.getGetService(fetchURL)
			.then((response) => {
				if (response.Succeeded) {
					if (response.Data.length > 0) {
						for (let userObject of response.Data) {
							if (userObject.AddressTypeID === 1) {
								thisSelf.setState({
									BlockNo: userObject.BlockNo,
									FloorNo: userObject.FloorNo,
									Street: userObject.Street,
									City: userObject.City,
									State: userObject.State,
									PostalCode: userObject.PostalCode,
									CountryID: userObject.CountryID,
									Country: userObject.Country,
									RegionID: userObject.RegionID,
									Region: userObject.Region,
									Email: userObject.Email,
									AltEmail: userObject.AltEmail,
									Mobile: userObject.Mobile,
									AltMobile: userObject.AltMobile,
									Office: userObject.Office,
									Home: userObject.Home,
									SameAsRegisteredAddress: userObject.SameAsRegisteredAddress,
									IsContactInfoExist: userObject.IsEditContactInfo,
								})
								this.props.SetUserEmail(userObject.Email)
								if (
									this.state.BlockNo !== "" &&
									this.state.FloorNo !== "" &&
									this.state.Street !== "" &&
									this.state.City !== "" &&
									this.state.State !== "" &&
									this.state.PostalCode !== "" &&
									this.state.CountryID !== "0"
								) {
									this.setState({
										PrimarySubmit: false,
									})
								}
								if (userObject.IsEditContactInfo === 1) {
									this.setState({
										IsSaveContactInfo: true,
									})
								}
							} else {
							}
							if (userObject.AddressTypeID === 2) {
								thisSelf.setState({
									OfficeBlockNo: userObject.BlockNo,
									OfficeFloorNo: userObject.FloorNo,
									OfficeStreet: userObject.Street,
									OfficeCity: userObject.City,
									OfficeState: userObject.State,
									OfficePostalCode: userObject.PostalCode,
									OfficeCountryID: userObject.CountryID,
									OfficeCountry: userObject.Country,
									OfficeRegionID: userObject.RegionID,
									OfficeRegion: userObject.Region,
									OfficeSameAsRegisteredAddress: userObject.SameAsRegisteredAddress,
									IsContactInfoExist: userObject.IsEditContactInfo,
								})

								if (userObject.IsEditContactInfo === 1) {
									this.setState({
										IsSaveContactInfo: true,
									})
								}
							} else {
							}
							if (userObject.AddressTypeID === 3) {
								thisSelf.setState({
									CorrespondingBlockNo: userObject.BlockNo,
									CorrespondingFloorNo: userObject.FloorNo,
									CorrespondingStreet: userObject.Street,
									CorrespondingCity: userObject.City,
									CorrespondingState: userObject.State,
									CorrespondingPostalCode: userObject.PostalCode,
									CorrespondingCountryID: userObject.CountryID,
									CorrespondingCountry: userObject.Country,
									CorrespondingRegionID: userObject.RegionID,
									CorrespondingRegion: userObject.Region,
									CorrespondingSameAsRegisteredAddress: userObject.SameAsRegisteredAddress,
									IsContactInfoExist: userObject.IsEditContactInfo,
								})

								if (userObject.IsEditContactInfo === 1) {
									this.setState({
										IsSaveContactInfo: true,
									})
								}
							} else {
							}
						}
						this.props.onSelectContact(true)
					} else {
						this.props.onSelectContact(false)
						this.state.isEditable = true
					}
					this.props.SetContactCompleted(this.state.IsSaveContactInfo)
				}
			})

			.catch((error) => {
				thisSelf.setState({ loading: false })
			})
	}

	hasError(key) {
		return this.state.errors.indexOf(key) !== -1
	}

	editProfile = () => {
		this.setState({
			isEditable: true,
		})
	}

	onSubmit = () => {
		debugger
		//VALIDATE
		var errors = []
		if (this.state.BlockNo === "") {
			errors.push("BlockNo")
		}
		if (this.state.FloorNo === "") {
			errors.push("FloorNo")
		}
		if (this.state.Street === "") {
			errors.push("Street")
		}
		if (this.state.City === "") {
			errors.push("City")
		}
		if (this.state.State === "") {
			errors.push("State")
		}
		if (this.state.RegionID === "0") {
			errors.push("RegionID")
		}
		if (this.state.PostalCode === "") {
			errors.push("PostalCode")
		}
		if (this.state.CountryID === "0") {
			errors.push("CountryID")
		}
		if (this.state.Email === "") {
			errors.push("errEmail")
		} else {
			var Emailpattern = new RegExp(
				/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			)
			if (!Emailpattern.test(this.state.Email)) {
				errors.push("Email")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("Email")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
		if (this.state.AltEmail !== "") {
			var AltEmailpattern = new RegExp(
				/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			)
			if (!AltEmailpattern.test(this.state.AltEmail)) {
				errors.push("AltEmail")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("AltEmail")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
		if (this.state.Mobile === "") {
			errors.push("errMobile")
		} else {
			var Mobilepattern = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)

			if (!Mobilepattern.test(this.state.Mobile)) {
				errors.push("Mobile")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("Mobile")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
		if (this.state.AltMobile !== "") {
			var AltMobilepattern = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)

			if (!AltMobilepattern.test(this.state.AltMobile)) {
				errors.push("AltMobile")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("AltMobile")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
		if (this.state.Office !== "") {
			var OfficeMobilepattern = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)

			if (!OfficeMobilepattern.test(this.state.Office)) {
				errors.push("Office")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("Office")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
		// if (this.state.Home === "") {
		// 	errors.push("Home")
		// } else {
			if (this.state.Home !== "") {
			var HomeMobilepattern = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)

			if (!HomeMobilepattern.test(this.state.Home)) {
				errors.push("Home")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("Home")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
		
		if (!this.state.CorrespondingSameAsRegisteredAddress) {
			if (this.state.CorrespondingBlockNo === "") {
				errors.push("CorrespondingBlockNo")
			}
			if (this.state.CorrespondingFloorNo === "") {
				errors.push("CorrespondingFloorNo")
			}
			if (this.state.CorrespondingStreet === "") {
				errors.push("CorrespondingStreet")
			}
			if (this.state.CorrespondingCity === "") {
				errors.push("CorrespondingCity")
			}

			if (this.state.CorrespondingState === "") {
				errors.push("CorrespondingState")
			}
			if (this.state.CorrespondingPostalCode === "") {
				errors.push("CorrespondingPostalCode")
			}
			if (this.state.CorrespondingCountryID === "0") {
				errors.push("CorrespondingCountryID")
			}
			if (this.state.CorrespondingRegionID === "0") {
				errors.push("CorrespondingRegionID")
			}
		}

		this.setState({
			errors: errors,
		})

		if (errors.length > 0) {
			return false
		} else {
			var IsSaved = true
			this.props.onSelectContact(IsSaved)
			let OfficePostal =this.state.OfficePostalCode==="" ? 0 : this.state.OfficePostalCode
			this.inputData = [
				{
					AddressTypeID: 1,
					ContactTypeID: 1,
					BlockNo: this.state.BlockNo,
					FloorNo: this.state.FloorNo,
					Street: this.state.Street,
					City: this.state.City,
					State: this.state.State,
					PostalCode: this.state.PostalCode,
					CountryID: this.state.CountryID.toString(),
					RegionID: this.state.RegionID.toString(),
					Email: this.state.Email,
					AltEmail: this.state.AltEmail,
					Mobile: this.state.Mobile,
					AltMobile: this.state.AltMobile,
					Office: this.state.Office,
					Home: this.state.Home,
					SameAsRegisteredAddress: this.state.SameAsRegisteredAddress,
				},
				{
					AddressTypeID: 2,
					ContactTypeID: 0,
					BlockNo: this.state.OfficeSameAsRegisteredAddress
						? this.state.BlockNo
						: this.state.OfficeBlockNo,
					FloorNo: this.state.OfficeSameAsRegisteredAddress
						? this.state.FloorNo
						: this.state.OfficeFloorNo,
					Street: this.state.OfficeSameAsRegisteredAddress
						? this.state.Street
						: this.state.OfficeStreet,
					City: this.state.OfficeSameAsRegisteredAddress ? this.state.City : this.state.OfficeCity,
					State: this.state.OfficeSameAsRegisteredAddress
						? this.state.State
						: this.state.OfficeState,
					PostalCode: this.state.OfficeSameAsRegisteredAddress
						? this.state.PostalCode
						: OfficePostal,
					CountryID: this.state.OfficeSameAsRegisteredAddress
						? this.state.CountryID.toString()
						: this.state.OfficeCountryID.toString(),
					RegionID: this.state.OfficeSameAsRegisteredAddress
						? this.state.RegionID.toString()
						: this.state.OfficeRegionID.toString(),
					Email: "",
					AltEmail: "",
					Mobile: "",
					AltMobile: "",
					Office: "",
					Home: "",
					SameAsRegisteredAddress: this.state.OfficeSameAsRegisteredAddress,
				},
				{
					AddressTypeID: 3,
					ContactTypeID: 0,
					BlockNo: this.state.CorrespondingSameAsRegisteredAddress
						? this.state.BlockNo
						: this.state.CorrespondingBlockNo,
					FloorNo: this.state.CorrespondingSameAsRegisteredAddress
						? this.state.FloorNo
						: this.state.CorrespondingFloorNo,
					Street: this.state.CorrespondingSameAsRegisteredAddress
						? this.state.Street
						: this.state.CorrespondingStreet,
					City: this.state.CorrespondingSameAsRegisteredAddress
						? this.state.City
						: this.state.CorrespondingCity,
					State: this.state.CorrespondingSameAsRegisteredAddress
						? this.state.State
						: this.state.CorrespondingState,
					PostalCode: this.state.CorrespondingSameAsRegisteredAddress
						? this.state.PostalCode
						: this.state.CorrespondingPostalCode,
					CountryID: this.state.CorrespondingSameAsRegisteredAddress
						? this.state.CountryID.toString()
						: this.state.CorrespondingCountryID.toString(),
					RegionID: this.state.CorrespondingSameAsRegisteredAddress
						? this.state.RegionID.toString()
						: this.state.CorrespondingRegionID.toString(),
					Email: "",
					AltEmail: "",
					Mobile: "",
					AltMobile: "",
					Office: "",
					Home: "",
					SameAsRegisteredAddress: this.state.CorrespondingSameAsRegisteredAddress,
				},
			]
		}

		let thisSelf = this

		APIRequest.getPostService(APIUrl.SAVE_CONTACT_INFO, this.inputData)
			.then((response) => {
				debugger
				if (response.Succeeded) {
					thisSelf.setState({
						isEditable: false,
					})
					this.fetch(this.state)
				} else {
					thisSelf.setState({
						isEditable: true,
					})
				}
				this.fetch(this.state)
				thisSelf.setState({ loading: false, users: response.Data })
			})
			.catch((error) => {
				thisSelf.setState({ loading: false })
			})
	}

	onChange = (e) => {
		debugger
		var errors = []
		// if (this.state.BlockNo === "") {
		// 	errors.push("BlockNo")
		// }
		// if (this.state.FloorNo === "") {
		// 	errors.push("FloorNo")
		// }
		// if (this.state.Street === "") {
		// 	errors.push("Street")
		// }
		// if (this.state.City === "") {
		// 	errors.push("City")
		// }
		// if (this.state.State === "") {
		// 	errors.push("State")
		// }
		// if (this.state.PostalCode === "") {
		// 	errors.push("PostalCode")
		// }
		// if (this.state.CountryID === "0") {
		// 	errors.push("CountryID")
		// }
		// if (this.state.RegionID === "0") {
		// 	errors.push("RegionID")
		// }
		// if (this.state.Email === "") {
		// 	errors.push("errEmail")
		// } 
		
			var Emailpattern = new RegExp(
				/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			)
			if (!Emailpattern.test(this.state.Email)) {
				errors.push("Email")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("Email")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		
		// if (this.state.AltEmail !==/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i)
		// {
		// 	this.setState({
		// 				errors: errors,
		// 		 		})
		// }

		var Emailpattern = new RegExp(
			/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
		)
		if (!Emailpattern.test(this.state.AltEmail)) {
			errors.push("AltEmail")

			this.setState({
				errors: errors,
			})
		} else {
			const index = errors.indexOf("AltEmail")
			if (index > -1) {
				errors.splice(index, 1)
			}
			this.setState({
				errors: errors,
			})
		}

		if (this.state.Mobile === "") {
			errors.push("errMobile")
		} else {
			var Mobilepattern = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)

			if (!Mobilepattern.test(this.state.Mobile)) {
				errors.push("Mobile")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("Mobile")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
		// if (this.state.AltMobile !== "/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/") {
		// 	this.setState({
		// 				errors: errors,
		// 		 		})
		// }

		if (this.state.AltMobile === "") {
			errors.push("AltMobile")
		} else {
		var AltMobile =  new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)
		if (!AltMobile.test(this.state.AltMobile)){
			errors.push("AltMobile")
			this.setState({
						errors: errors,
				 		})
		}
		else {
			const index = errors.indexOf("AltMobile")
			if (index > -1) {
				errors.splice(index, 1)
			}
			this.setState({
				errors: errors,
			})
		}
	}

		
		var officeNo =  new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)
		if (!officeNo.test(this.state.Office)){
			errors.push("Office")
			this.setState({
						errors: errors,
				 		})
		}
		else {
			const index = errors.indexOf("Office")
			if (index > -1) {
				errors.splice(index, 1)
			}
			this.setState({
				errors: errors,
			})
		}
	
		// if (this.state.Home === "") {
		// 	errors.push("Home")
		// } else {
			var HomeMobilepattern2 = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)

			if (!HomeMobilepattern2.test(this.state.Home)) {
				errors.push("Home")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("Home")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		// }
		
		if (!this.state.CorrespondingSameAsRegisteredAddress) {
			// if (this.state.CorrespondingBlockNo === "") {
			// 	errors.push("CorrespondingBlockNo")
			// }
			// if (this.state.CorrespondingFloorNo === "") {
			// 	errors.push("CorrespondingFloorNo")
			// }
			// if (this.state.CorrespondingStreet === "") {
			// 	errors.push("CorrespondingStreet")
			// }
			// if (this.state.CorrespondingCity === "") {
			// 	errors.push("CorrespondingCity")
			// }

			// if (this.state.CorrespondingState === "") {
			// 	errors.push("CorrespondingState")
			// }
			// if (this.state.CorrespondingPostalCode === "") {
			// 	errors.push("CorrespondingPostalCode")
			// }
			// if (this.state.CorrespondingCountryID === "0") {
			// 	errors.push("CorrespondingCountryID")
			// }
			// if (this.state.CorrespondingRegionID === "0") {
			// 	errors.push("CorrespondingRegionID")
			// }
		}
		this.setState({
			...this.state,
			[e.target.name]: e.target.value,
			errors: errors,
		})
		if (
			this.state.BlockNo !== "" &&
			this.state.FloorNo !== "" &&
			this.state.Street !== "" &&
			this.state.City !== "" &&
			this.state.State !== "" &&
			this.state.PostalCode !== "" &&
			this.state.CountryID !== "0" 
		) {
			this.setState({
				PrimarySubmit: false,
			})
		}
	}

	validate = (e) => {
		var FieldsName = e.target.name
		var Email = e.target.value
		var errors = [...this.state.errors]
		if (Email !== "undefined") {
			var Emailpattern = new RegExp(
				/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			)
			if (!Emailpattern.test(Email)) {
				errors.push(FieldsName)

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf(FieldsName)
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
	}
	validateAlterEmail = (e) => {
		var FieldsName = e.target.name
		var Email = e.target.value
		var errors = [...this.state.errors]
		if (Email !== "undefined" && Email !== "") {
			var Emailpattern = new RegExp(
				/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			)
			if (!Emailpattern.test(Email)) {
				errors.push(FieldsName)

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf(FieldsName)
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
	}
	ValidMobile = (e) => {
		var FieldsName = e.target.name
		var Mobile = e.target.value
		var errors = [...this.state.errors]
		if (Mobile !== "undefined") {
			var Mobilepattern = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)
			if (!Mobilepattern.test(Mobile)) {
				errors.push(FieldsName)

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf(FieldsName)
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
	}
	ValidAlterMobile = (e) => {
		var FieldsName = e.target.name
		var AltMobile = e.target.value
		var errors = [...this.state.errors]
		if (AltMobile !== "undefined" && AltMobile !== "") {
			var Mobilepattern = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)
			if (!Mobilepattern.test(AltMobile)) {
				errors.push(FieldsName)

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf(FieldsName)
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
	}

	onBlur = (e) => {
		if (
			this.state.BlockNo !== "" &&
			this.state.FloorNo !== "" &&
			this.state.Street !== "" &&
			this.state.City !== "" &&
			this.state.State !== "" &&
			this.state.PostalCode !== "" &&
			this.state.CountryID !== "0"
		) {
			this.setState({
				PrimarySubmit: false,
			})
		}
	}

	handleOfficeChange(evt) {
		this.setState({
			...this.state,
			[evt.target.name]: evt.target.checked,
		})
		if (evt.target.checked) {
			this.setState({
				...this.state,
				OfficeBlockNo: this.state.BlockNo,
				OfficeFloorNo: this.state.FloorNo,
				OfficeStreet: this.state.Street,
				OfficeCity: this.state.City,
				OfficeState: this.state.State,
				OfficePostalCode: this.state.PostalCode,
				OfficeCountryID: this.state.CountryID,
				OfficeRegionID: this.state.RegionID,
				OfficeSameAsRegisteredAddress: true,
			})
		} else {
			this.setState({
				...this.state,
				OfficeBlockNo: this.state.OfficeBlockNo,
				OfficeFloorNo: this.state.OfficeFloorNo,
				OfficeStreet: this.state.OfficeStreet,
				OfficeCity: this.state.OfficeCity,
				OfficeState: this.state.OfficeState,
				OfficePostalCode: this.state.OfficePostalCode,
				OfficeCountryID: this.state.OfficeCountryID,
				OfficeRegionID: this.state.OfficeRegionID,
				OfficeSameAsRegisteredAddress: false,
			})
		}
	}
	handleCorrespondingChange(evt) {
		debugger
		this.setState({
			...this.state,
			[evt.target.name]: evt.target.checked,
		})
		if (evt.target.checked) {
			this.setState({
				...this.state,
				CorrespondingBlockNo: this.state.BlockNo,
				CorrespondingFloorNo: this.state.FloorNo,
				CorrespondingStreet: this.state.Street,
				CorrespondingCity: this.state.City,
				CorrespondingState: this.state.State,
				CorrespondingPostalCode: this.state.PostalCode,
				CorrespondingCountryID: this.state.CountryID,
				CorrespondingRegionID: this.state.RegionID,
				CorrespondingSameAsRegisteredAddress: true,
			})
		} else {
			this.setState({
				...this.state,
				CorrespondingBlockNo: this.state.CorrespondingBlockNo,
				CorrespondingFloorNo: this.state.CorrespondingFloorNo,
				CorrespondingStreet: this.state.CorrespondingStreet,
				CorrespondingCity: this.state.CorrespondingCity,
				CorrespondingState: this.state.CorrespondingState,
				CorrespondingPostalCode: this.state.CorrespondingPostalCode,
				CorrespondingCountryID: this.state.CorrespondingCountryID,
				CorrespondingRegionID: this.state.CorrespondingRegionID,
				CorrespondingSameAsRegisteredAddress: false,
			})
		}
	}
	componentDidMount() {
		this.fetch(this.state)
		let self = this
		APIRequest.getGetService(APIUrl.COUNTRY_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						CountryIDList: response.Data,
					})
					//response.Data
				}
			})
			.catch((error) => {})
		APIRequest.getGetService(APIUrl.REGION_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						RegionIDList: response.Data,
					})
					//response.Data
				}
			})
			.catch((error) => {})
	}
	render() {
		const { isEditable } = this.state
		return (
			<div className='margin-top-2'>
				<Form>
					<div className=''>
						<span className='default-subHeading-textstyle'>
							Communication Details
							<br />
							<br />
						</span>
						<div class="admin-report-table otherInfo-tbl-border rounded">
						<div className='row' >
							<Form.Group className='col-md-6' controlId='formPlaintextEmail'>
								<Form.Label className='default-textstyle' column>
									Email <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											className={this.hasError("errEmail") ? "is-invalid w-100 m-0" : "w-100 m-0"}
											type='text'
											placeholder='Email'
											name='Email'
											value={this.state.Email}
											onChange={this.onChange}
											onBlur={this.validate}
											required
											maxLength='100'
										/>
										<span className={this.hasError("errEmail") ? "inline-errormsg" : "hidden"}>
											Email is mandatory.
										</span>
										<span className={this.hasError("Email") ? "inline-errormsg" : "hidden"}>
											Please enter the valid Email.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.Email}</span>
									</Col>
								)}
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextAltEmail'>
								<Form.Label className='default-textstyle' column>
									Alternate Email
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											className='w-100 m-0'
											type='text'
											placeholder='Alternate Email'
											name='AltEmail'
											value={this.state.AltEmail}
											onChange={this.onChange}
											onBlur={this.validateAlterEmail}
											required
											maxLength='100'
										/>
										<span className={this.hasError("AltEmail")&& this.state.AltEmail !== "" ? "inline-errormsg" : "hidden"}>
											Please enter the valid Alternate Email.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.AltEmail}</span>
									</Col>
								)}
							 </Form.Group>
					{/*	</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextMobile'>
								<Form.Label className='default-textstyle' column>
									Mobile Number <span className='text-danger'>*</span>
								</Form.Label>
								
								{this.state.isEditable ? (
									<Col>
									<div>
									<input
								class="info-input-padding m-0 col"
											// className={this.hasError("errMobile") ? "is-invalid w-100 m-0" : "w-100 m-0"}
											type='text'
											// placeholder='Mobile'
											name='Mobile'
											value="+91"
											// onChange={this.onChange}
											// onBlur={this.ValidMobile}
											required
											maxLength='5'
										/>
										<Form.Control
											className={this.hasError("errMobile") ? "is-invalid w-100 m-0" : "w-100 m-0"}
											type='number'
											placeholder='Mobile'
											name='Mobile'
											value={this.state.Mobile}
											onChange={this.onChange}
											onBlur={this.ValidMobile}
											required
											maxLength='15'
										/>
										</div>
										<span className={this.hasError("errMobile") ? "inline-errormsg" : "hidden"}>
											Mobile is mandatory.
										</span>
										<span className={this.hasError("Mobile") ? "inline-errormsg" : "hidden"}>
											Please enter the valid Mobile number.
										</span>
									</Col>
								) : (
									
									<Col className='info-input-padding m-0'>
										<span>{this.state.Mobile!=="" ? +"91"+" "+this.state.Mobile  :null}</span> 
									</Col>
								)}
								
							</Form.Group> */}
							
							<Form.Group compact className='col-md-6' controlId='formPlaintextAltMobile'>
								<Form.Label className='default-textstyle' column>
									Mobile Number <span className='text-danger'>*</span>
										</Form.Label>
										{this.state.isEditable ? (
										<div classname="mobiborder">						
      										<input  id="input" style={{ width: '7%' }} name='Office' 
	  										class="info-input-padding"
											value="+91"
											required
											maxLength='5' />
      										<input  id="input" style={{ width: '41%' }} 
											defaultValue="26888888" 
											class="info-input-padding"
											type='number'
											placeholder='Mobile Number'
											name='Mobile'
											value={this.state.Mobile}
											onChange={this.onChange}
											onBlur={this.ValidAlterMobile}
											required
											maxLength='15' />
											<div>
											<span className={this.hasError("errMobile")&& this.state.errMobile !== "" ? "inline-errormsg" : "hidden"}>
											<span style={{marginLeft:"52%"}}>Mobile is mandatory.</span>
											</span>
											<span className={this.hasError("Mobile")&& this.state.Mobile !== "" ? "inline-errormsg" : "hidden"}>
											<span style={{marginLeft:"52%"}}>Please enter the valid Mobile Number.</span>
											</span>
											</div>
										</div>
									) : (
										<Col span={0} className='info-input-padding m-0'>
											<span>{this.state.Mobile!=="" ?<span>+{this.state.Mobile!=="" ? + 91+" "+" "+this.state.Mobile  :null}</span>:""}
											</span>
										</Col>
									)}
    						</Form.Group>
	 				 
							 {/* <Form.Group className='col-md-6' controlId='formPlaintextAltMobile'>
								<Form.Label className='default-textstyle' column>
									Alternate Mobile
								</Form.Label>
								
								{this.state.isEditable ? (
									<Col span={0}>
									<input style={{ width: '7%' }}
										class="info-input-padding"
											// className={this.hasError("errMobile") ? "is-invalid w-100 m-0" : "w-100 m-0"}
											type='text'
											// placeholder='Mobile'
											name='Mobile'
											value="+91"
											// onChange={this.onChange}
											// onBlur={this.ValidMobile}
											required
											maxLength='5'
										/>
										<Form.Control
											className='w-100 m-0'
											type='number'
											placeholder='Alternate Mobile'
											name='AltMobile'
											value={this.state.AltMobile}
											onChange={this.onChange}
											onBlur={this.ValidAlterMobile}
											required
											maxLength='15'
										/>
										
										
										<span className={this.hasError("AltMobile") ? "inline-errormsg" : "hidden"}>
											Please enter the valid Alternate Mobile.
										</span>
									</Col>
								) : (
									<Col span={0} className='info-input-padding m-0'>
										<span>{this.state.AltMobile!=="" ? +"91"+" "+this.state.AltMobile  :null}</span> 
									</Col>
								)}
							</Form.Group>  */}



<Form.Group compact className='col-md-6' controlId='formPlaintextAltMobile'>
<Form.Label className='default-textstyle' column>
									Alternate Mobile
								</Form.Label>
								{this.state.isEditable ? (
		<div classname="mobiborder">						
      <input style={{ width: '7%' }}  id="input" defaultValue="0571" name='Mobile' 
	  										class="info-input-padding"
											value="+91"
											required
											maxLength='5' />
      <input style={{ width: '41%' }}  id="input" 
	  										class="info-input-padding"
											type='number'
											placeholder='Alternate Mobile'
											name='AltMobile'
											value={this.state.AltMobile}
											onChange={this.onChange}
											onBlur={this.ValidAlterMobile}
											required
											maxLength='15' />
											<div>
											<span className={this.hasError("AltMobile")&& this.state.AltMobile !== "" ? "inline-errormsg" : "hidden"}>
											<span style={{marginLeft:"52%"}}>Please enter the valid Alternate Mobile.</span>
										</span>
										</div>
			</div>
			) : (
				<Col span={0} className='info-input-padding m-0'>
					<span>{this.state.AltMobile!=="" ?<span>+{this.state.AltMobile!=="" ? + 91+" "+" "+this.state.AltMobile  :null}</span>:""}</span>
					{/* <span>+{this.state.AltMobile!=="" ? + 91+" "+" "+this.state.AltMobile  :null}</span>  */}
				</Col>
			)}
    </Form.Group>

							
						</div>
						<div className='row'>
						<Form.Group compact className='col-md-6' controlId='formPlaintextAltMobile'>
<Form.Label className='default-textstyle' column>
								Home Number
								</Form.Label>
								{this.state.isEditable ? (
				<div classname="mobiborder">						
      									<input id="input"
										  style={{ width: '7%' }} 
										  name='Mobile' 
	  									  class="info-input-padding"
											value="+91"
											required
											maxLength='5' 
											/>
     									 <input  id="input" style={{ width: '41%' }} 
										  class="info-input-padding"
											type='number'
											placeholder='Home Number'
											name='Home'
											value={this.state.Home}
											onChange={this.onChange}
											onBlur={this.ValidAlterMobile}
											required
											maxLength='15' 
											/>
											<div>
											<span className={this.hasError("Home")&& this.state.Home !== "" ? "inline-errormsg" : "hidden"}>
											<span style={{marginLeft:"52%"}}>Please enter the valid Home Number.</span>
											
										</span>
										</div>
				</div>
				) : (
				<Col span={0} className='info-input-padding m-0'>
					<span>{this.state.Home!=="" ?<span>+{this.state.Home!=="" ? + 91+" "+" "+this.state.Home  :null}</span>:""}</span>
					{/* <span>+{this.state.Home!=="" ? + 91+" "+" "+this.state.Home  :null}</span>  */}
				</Col>
				)}
    </Form.Group>
							{/* <Form.Group className='col-md-6' controlId='formPlaintextHome'>
								<Form.Label className='default-textstyle' column>
									Home Number <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
									<span>
									<input
								class="info-input-padding m-0 col"
											// className={this.hasError("errMobile") ? "is-invalid w-100 m-0" : "w-100 m-0"}
											type='text'
											// placeholder='Mobile'
											name='Mobile'
											value="+91"
											// onChange={this.onChange}
											// onBlur={this.ValidMobile}
											required
											maxLength='5'
										/>
										</span>
										<span>
										<Form.Control
											className={this.hasError("errHome") ? "is-invalid w-100 m-0" : "w-100 m-0"}
											type='number'
											placeholder='Home Number'
											name='Home'
											value={this.state.Home}
											onChange={this.onChange}
											onBlur={this.ValidMobile}
											required
											maxLength='15'
										/>
										</span>
										<span
											className={
												this.hasError("Home") && this.state.Home !== ""
													? "inline-errormsg"
													: "hidden"
											}
										>
											Please enter the valid Home number.
										</span>
										<span className={this.hasError("errHome") ? "inline-errormsg" : "hidden"}>
											Home Number is mandatory.
										</span>
										<span className={this.hasError("Home") ? "inline-errormsg" : "hidden"}>
											Please enter the valid Mobile number.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.Home===""?"":+91}</span> <span>{this.state.Home}</span>
									</Col>
								)}
							</Form.Group> */}

						<Form.Group compact className='col-md-6' controlId='formPlaintextAltMobile'>
							<Form.Label className='default-textstyle' column>
								Office Number
								</Form.Label>
								{this.state.isEditable ? (
								<div   classname="mobiborder">		
								<input id="input"
										  style={{ width: '7%' }} 
										  name='Office' 
	  									  class="info-input-padding"
											value="+91"
											required
											maxLength='5' 
											/>
     									 <input  id="input" style={{ width: '41%' }} 
										  class="info-input-padding"
											type='number'
											placeholder='Office Number'
											name='Office'
											value={this.state.Office}
											onChange={this.onChange}
											onBlur={this.ValidAlterMobile}
											required
											maxLength='15' 
											/>				
      								<div>
											<span className={this.hasError("Office") && this.state.Office !== ""? "inline-errormsg" : "hidden"}>
											<span style={{marginLeft:"52%"}}>Please enter the valid Office Number.</span>
											</span>
											</div>
									</div>
								) : (
								<Col span={0} className='info-input-padding m-0'>
									<span>{this.state.Office!=="" ?<span>+{this.state.Office!=="" ? + 91+" "+" "+this.state.Office  :null}</span>:""}</span>
									{/* <span>+{this.state.Office!=="" ? + 91+" "+" "+this.state.Office  :null}</span>  */}
								</Col>
								)}
    					</Form.Group>

							{/* <Form.Group className='col-md-6' controlId='formPlaintextOffice'>
								<Form.Label className='default-textstyle' column>
									Office Number 
									<br></br>
									<br></br>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
									<input
								class="info-input-padding m-0 col"
											// className={this.hasError("errMobile") ? "is-invalid w-100 m-0" : "w-100 m-0"}
											type='text'
											// placeholder='Mobile'
											name='Mobile'
											value="+91"
											// onChange={this.onChange}
											// onBlur={this.ValidMobile}
											required
											maxLength='5'
										/>
										<Form.Control
											className={this.hasError("errOffice") ? "is-invalid w-100 m-0" : "w-100 m-0"}
											type='number'
											placeholder='Office Number'
											name='Office'
											value={this.state.Office}
											onChange={this.onChange}
											required
											maxLength='15'
										/>
										<span className={this.hasError("errOffice") ? "inline-errormsg" : "hidden"}>
											Office Number is mandatory.
										</span>
										<span className={this.hasError("Office") ? "inline-errormsg" : "hidden"}>
											Please enter the valid Office Number.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.Office===""?"":+91}</span> <span>{this.state.Office}</span>
									</Col>
								)}
							</Form.Group> */}
							</div>
						</div>
					</div>
					<div className='p-t-10'>
						<span className='default-subHeading-textstyle'>
							Registered Address
							<br />
							</span>
							<Form.Label className='default-textstyle' column>
										Address 
									</Form.Label>
									<Col  >
										<div >
											<span>{this.state.BlockNo!==""?this.state.BlockNo+", ":null} {this.state.FloorNo!==""?this.state.FloorNo+",":null} {this.state.Street!==""?this.state.Street+",":null} {this.state.City!==""?this.state.City+",":null} {this.state.State!==""?this.state.State+",":null} {this.state.Region!==""?this.state.Region+",":null} {this.state.Country!==""?this.state.Country+",":null} {this.state.PostalCode!==""?this.state.PostalCode+".":null}</span>
										</div>
										</Col>
									<br />
									<br></br>
									<br></br>
										
						<div class="admin-report-table otherInfo-tbl-border rounded">
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextBlockNo'>
								<Form.Label className='default-textstyle' column>
									Door Number <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											className={this.hasError("BlockNo") ? "w-100 is-invalid m-0" : "w-100 m-0"}
											type='text'
											placeholder='Door Number'
											name='BlockNo'
											value={this.state.BlockNo}
											onChange={this.onChange}
											onBlur={this.onBlur}
											maxLength='10'
										/>
										<span className={this.hasError("BlockNo") ? "inline-errormsg" : "hidden"}>
										Door Number is mandatory.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.BlockNo}</span>
									</Col>
								)}
							</Form.Group>

							<Form.Group className='col-md-6' controlId='formPlaintextFloorNo'>
								<Form.Label className='default-textstyle' column>
									Street/Area <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											className={this.hasError("FloorNo") ? "w-100 is-invalid m-0" : "w-100 m-0"}
											type='text'
											placeholder='Street/Area'
											name='FloorNo'
											value={this.state.FloorNo}
											onChange={this.onChange}
											onBlur={this.onBlur}
											maxLength='60'
										/>
										<span className={this.hasError("FloorNo") ? "inline-errormsg" : "hidden"}>
										Street/Area is mandatory.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.FloorNo}</span>
									</Col>
								)}
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextStreet'>
								<Form.Label className='default-textstyle' column>
									Village/Town <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											className={this.hasError("Street") ? "w-100 is-invalid m-0" : "w-100 m-0"}
											type='text'
											placeholder='Village/Town'
											name='Street'
											value={this.state.Street}
											onChange={this.onChange}
											onBlur={this.onBlur}
											maxLength='60'
										/>
										<span className={this.hasError("Street") ? "inline-errormsg" : "hidden"}>
										Village/Town is mandatory.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.Street}</span>
									</Col>
								)}
							</Form.Group>

							<Form.Group className='col-md-6' controlId='formPlaintextCity'>
								<Form.Label className='default-textstyle' column>
									City <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											className={this.hasError("City") ? "w-100 is-invalid m-0" : "w-100 m-0"}
											type='text'
											placeholder='City'
											name='City'
											value={this.state.City}
											onChange={this.onChange}
											onBlur={this.onBlur}
											maxLength='60'
										/>
										<span className={this.hasError("City") ? "inline-errormsg" : "hidden"}>
											City is mandatory.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.City}</span>
									</Col>
								)}
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextState'>
								<Form.Label className='default-textstyle' column>
									State <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											className={this.hasError("State") ? "w-100 is-invalid m-0" : "w-100 m-0"}
											type='text'
											placeholder='State'
											name='State'
											value={this.state.State}
											onChange={this.onChange}
											onBlur={this.onBlur}
											maxLength='60'
										/>
										<span className={this.hasError("State") ? "inline-errormsg" : "hidden"}>
											State is mandatory.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.State}</span>
									</Col>
								)}
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextCountryID'>

								
								<Form.Label className='default-textstyle' column>
									Country <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											className={this.hasError("CountryID") ? "w-100 is-invalid m-0" : "w-100 m-0"}
											as='select'
											componentClass='select'
											placeholder='select'
											name='CountryID'
											value={this.state.CountryID}
											onChange={this.onChange}
											onBlur={this.onBlur}
											maxLength='60'
										>
											<option value='0'>select</option>
											{this.state.CountryIDList.map((c) => (
												<option value={c.OptionID}>{c.OptionData}</option>
											))}
										</Form.Control>
										<span className={this.hasError("CountryID") ? "inline-errormsg" : "hidden"}>
											Country is mandatory.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.Country}</span>
									</Col>
								)}
							</Form.Group>
						</div>
						<div className='row'>
							<Form.Group className='col-md-6' controlId='formPlaintextRegionID'>
								<Form.Label className='default-textstyle' column>
									Region <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											as='select'
											componentClass='select'
											placeholder='select'
											className='w-100 m-0'
											name='RegionID'
											value={this.state.RegionID}
											onChange={this.onChange}
											onBlur={this.onBlur}
										>
											<option value='0'>select</option>
											{this.state.RegionIDList.map((c) => (
												<option value={c.OptionID}>{c.OptionData}</option>
											))}
										</Form.Control>
										<span
												className={
													this.hasError("RegionID") ? "inline-errormsg" : "hidden"
												}
											>
												Region is mandatory.
											</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.Region}</span>
									</Col>
								)}
							</Form.Group>
							<Form.Group className='col-md-6' controlId='formPlaintextPostalCode'>
								<Form.Label className='default-textstyle' column>
									Postal Code <span className='text-danger'>*</span>
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Control
											className={this.hasError("PostalCode") ? "w-100 is-invalid m-0" : "w-100 m-0"}
											type='number'
											placeholder='Postal Code'
											name='PostalCode'
											value={this.state.PostalCode}
											onChange={this.onChange}
											onBlur={(this.onBlur, this.ValidatePostal)}
											maxLength='10'
										/>
										<span className={this.hasError("PostalCode") ? "inline-errormsg" : "hidden"}>
											Postal Code is mandatory.
										</span>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.PostalCode}</span>
									</Col>
								)}
							</Form.Group>
							
						</div>
						</div>
					</div>
					<div className='p-t-10'>
						<span className='default-subHeading-textstyle'>
							Office Address
							<br />
							</span>
							
						<Form.Label className='default-textstyle' column>
									Same as residential address
								</Form.Label>
								
								{this.state.isEditable ? (
									<Col>
										<Form.Check
											// disabled={this.state.PrimarySubmit}
											type='checkbox'
											className='w-100 m-0'
											label=''
											name='OfficeSameAsRegisteredAddress'
											checked={this.state.OfficeSameAsRegisteredAddress}
											value={this.state.OfficeSameAsRegisteredAddress}
											onChange={this.handleOfficeChange}
											class="checkmark"
										/>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.OfficeSameAsRegisteredAddress ? "Yes" : "No"}</span>
									</Col>
								)}
						<br></br>
						<br></br>
						<Form.Label className='default-textstyle' column>
										Address 
									</Form.Label>
									<Col  >
										<div>
											<span>{this.state.OfficeBlockNo !== "" ? `${this.state.OfficeBlockNo},` : null} {this.state.OfficeFloorNo!=="" ? this.state.OfficeFloorNo +",":null} {this.state.OfficeStreet!=="" ? this.state.OfficeStreet+",":null } {this.state.OfficeCity!=="" ? this.state.OfficeCity+",":null} {this.state.OfficeState!=="" ? this.state.OfficeState+",":null}  {this.state.OfficeRegion!==""?this.state.OfficeRegion+",":null} {this.state.OfficeCountry!==""?this.state.OfficeCountry+",":null} {this.state.OfficePostalCode!==""?this.state.OfficePostalCode+'.':null}</span>
											</div>
										</Col>
									<br />
									<br></br>
									<br></br>
						<div className={this.state.OfficeSameAsRegisteredAddress ? "hidden" : ""}>
						<div class="admin-report-table otherInfo-tbl-border rounded">
						<div className='row'>
							<Form.Group
								className='col-md-6'
								controlId='formPlaintextOfficeSameAsRegisteredAddress'
							>
								
							</Form.Group>
							<Form.Group className='col-md-6'></Form.Group>
						</div>
								<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextOfficeBlockNo'>
									<Form.Label className='default-textstyle' column>
									Door Number
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("OfficeBlockNo") ? "w-100 is-invalid m-0" : "w-100 m-0"
												}
												type='text'
												placeholder='Door Number'
												name='OfficeBlockNo'
												value={this.state.OfficeBlockNo}
												onChange={this.onChange}
												maxLength='10'
											/>
											{/* <span
												className={this.hasError("OfficeBlockNo") ? "inline-errormsg" : "hidden"}
											>
												Block is mandatory.
											</span> */}
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.OfficeBlockNo}</span>
										</Col>
									)}
								</Form.Group>

								<Form.Group className='col-md-6' controlId='formPlaintextOfficeFloorNo'>
									<Form.Label className='default-textstyle' column>
										Street/Area
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("OfficeFloorNo") ? "w-100 is-invalid m-0" : "w-100 m-0"
												}
												type='text'
												placeholder='Street/Area'
												name='OfficeFloorNo'
												value={this.state.OfficeFloorNo}
												onChange={this.onChange}
												maxLength='60'
											/>
											{/* <span
												className={this.hasError("OfficeFloorNo") ? "inline-errormsg" : "hidden"}
											>
												Floor No is mandatory.
											</span> */}
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.OfficeFloorNo}</span>
										</Col>
									)}
								</Form.Group>
							</div>
							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextOfficeStreet'>
									<Form.Label className='default-textstyle' column>
										Village/Town
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("OfficeStreet") ? "w-100 is-invalid m-0" : "w-100 m-0"
												}
												type='text'
												placeholder='Village/Town'
												name='OfficeStreet'
												value={this.state.OfficeStreet}
												onChange={this.onChange}
												maxLength='60'
											/>
											{/* <span
												className={this.hasError("OfficeStreet") ? "inline-errormsg" : "hidden"}
											>
												Street is mandatory.
											</span> */}
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.OfficeStreet}</span>
										</Col>
									)}
								</Form.Group>

								<Form.Group className='col-md-6' controlId='formPlaintextOfficeCity'>
									<Form.Label className='default-textstyle' column>
										City 
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("OfficeCity") ? "w-100 is-invalid m-0" : "w-100 m-0"
												}
												type='text'
												placeholder='City'
												name='OfficeCity'
												value={this.state.OfficeCity}
												onChange={this.onChange}
												maxLength='60'
											/>
											{/* <span className={this.hasError("OfficeCity") ? "inline-errormsg" : "hidden"}>
												City is mandatory.
											</span> */}
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.OfficeCity}</span>
										</Col>
									)}
								</Form.Group>
							</div>
							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextOfficeState'>
									<Form.Label className='default-textstyle' column>
										State 
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("OfficeState") ? "w-100 is-invalid m-0" : "w-100 m-0"
												}
												type='text'
												placeholder='State'
												name='OfficeState'
												value={this.state.OfficeState}
												onChange={this.onChange}
												maxLength='60'
											/>
											{/* <span className={this.hasError("OfficeState") ? "inline-errormsg" : "hidden"}>
												State is mandatory.
											</span> */}
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.OfficeState}</span>
										</Col>
									)}
								</Form.Group>
								<Form.Group className='col-md-6' controlId='formPlaintextOfficeCountryID'>
									<Form.Label className='default-textstyle' column>
										Country 
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												as='select'
												componentClass='select'
												placeholder='select'
												className={
													this.hasError("OfficeCountryID") ? "is-invalid w-100 m-0" : "w-100 m-0"
												}
												name='OfficeCountryID'
												value={this.state.OfficeCountryID}
												onChange={this.onChange}
											>
												<option value='0'>select</option>
												{this.state.CountryIDList.map((c) => (
													<option value={c.OptionID}>{c.OptionData}</option>
												))}
											</Form.Control>
											{/* <span
												className={this.hasError("OfficeCountryID") ? "inline-errormsg" : "hidden"}
											>
												Country is mandatory.
											</span> */}
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.OfficeCountry}</span>
										</Col>
									)}
								</Form.Group>
							</div>
							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextOfficeRegionID'>
									<Form.Label className='default-textstyle' column>
										Region
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												as='select'
												componentClass='select'
												placeholder='select'
												className='w-100 m-0'
												name='OfficeRegionID'
												value={this.state.OfficeRegionID}
												onChange={this.onChange}
											>
												<option value='0'>select</option>
												{this.state.RegionIDList.map((c) => (
													<option value={c.OptionID}>{c.OptionData}</option>
												))}
											</Form.Control>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.OfficeRegion}</span>
										</Col>
									)}
								</Form.Group>
								<Form.Group className='col-md-6' controlId='formPlaintextOfficePostalCode'>
									<Form.Label className='default-textstyle' column>
										Postal Code 
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className="w-100 m-0"
												type='number'
												placeholder='Postal Code'
												name='OfficePostalCode'
												value={this.state.OfficePostalCode}
												onChange={this.onChange}
												maxLength='10'
											/>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.OfficePostalCode}</span>
										</Col>
									)}
								</Form.Group>
								
							</div>
						</div>
						</div>
					</div>
					<div className='p-t-10'>
						<span className='default-subHeading-textstyle'>
							Corresponding Address
							<br />
						</span>
						<Form.Label className='default-textstyle' column>
									Same as residential address
								</Form.Label>
								{this.state.isEditable ? (
									<Col>
										<Form.Check
											// disabled={this.state.PrimarySubmit}
											type='checkbox'
											className='w-100 m-0'
											label=''
											name='CorrespondingSameAsRegisteredAddress'
											checked={this.state.CorrespondingSameAsRegisteredAddress}
											value={this.state.CorrespondingSameAsRegisteredAddress}
											onChange={this.handleCorrespondingChange}
										/>
									</Col>
								) : (
									<Col className='info-input-padding m-0'>
										<span>{this.state.CorrespondingSameAsRegisteredAddress ? "Yes" : "No"}</span>
									</Col>
								)}
								<br />
							<br />
							<Form.Label className='default-textstyle' column>
										Address 
									</Form.Label>
									<Col  >
										<div >
											<span>{this.state.CorrespondingBlockNo!==""?this.state.CorrespondingBlockNo+',':null} {this.state.CorrespondingFloorNo!==""?this.state.CorrespondingFloorNo+',':null} {this.state.CorrespondingStreet!==""?this.state.CorrespondingStreet+',':null} {this.state.CorrespondingCity!==""?this.state.CorrespondingCity+',':null} {this.state.CorrespondingState!==""?this.state.CorrespondingState+',':null}  {this.state.CorrespondingRegion!==""?this.state.CorrespondingRegion+',':null} {this.state.CorrespondingCountry!==""?this.state.CorrespondingCountry+',':null}  {this.state.CorrespondingPostalCode!==""?this.state.CorrespondingPostalCode+'.':null}</span>
											</div>
										</Col>
									<br />
									<br></br>
									<br></br>
									<div className={this.state.CorrespondingSameAsRegisteredAddress ? "hidden" : ""}>
						<div class="admin-report-table otherInfo-tbl-border rounded">
						<div className='row'>
							<Form.Group
								className='col-md-6'
								controlId='formPlaintextCorrespondingSameAsRegisteredAddress'
							>
								
							</Form.Group>
							<Form.Group className='col-md-6'></Form.Group>
						</div>
						
							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextCorrespondingBlockNo'>
									<Form.Label className='default-textstyle' column>
										Door Number <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("CorrespondingBlockNo")
														? "w-100 is-invalid m-0"
														: "w-100 m-0"
												}
												type='text'
												placeholder='Door Number'
												name='CorrespondingBlockNo'
												value={this.state.CorrespondingBlockNo}
												onChange={this.onChange}
												maxLength='10'
											/>
											<span
												className={
													this.hasError("CorrespondingBlockNo") ? "inline-errormsg" : "hidden"
												}
											>
												Door Number  is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.CorrespondingBlockNo}</span>
										</Col>
									)}
								</Form.Group>

								<Form.Group className='col-md-6' controlId='formPlaintextCorrespondingFloorNo'>
									<Form.Label className='default-textstyle' column>
										Street/Area <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("CorrespondingFloorNo")
														? "w-100 is-invalid m-0"
														: "w-100 m-0"
												}
												type='text'
												placeholder='Street/Area'
												name='CorrespondingFloorNo'
												value={this.state.CorrespondingFloorNo}
												onChange={this.onChange}
												maxLength='60'
											/>
											<span
												className={
													this.hasError("CorrespondingFloorNo") ? "inline-errormsg" : "hidden"
												}
											>
												Street/Area is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.CorrespondingFloorNo}</span>
										</Col>
									)}
								</Form.Group>
							</div>
							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextCorrespondingStreet'>
									<Form.Label className='default-textstyle' column>
										Village/Town <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("CorrespondingStreet")
														? "w-100 is-invalid m-0"
														: "w-100 m-0"
												}
												type='text'
												placeholder='Village/Town'
												name='CorrespondingStreet'
												value={this.state.CorrespondingStreet}
												onChange={this.onChange}
												maxLength='60'
											/>
											<span
												className={
													this.hasError("CorrespondingStreet") ? "inline-errormsg" : "hidden"
												}
											>
												Village/Town is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.CorrespondingStreet}</span>
										</Col>
									)}
								</Form.Group>

								<Form.Group className='col-md-6' controlId='formPlaintextCorrespondingCity'>
									<Form.Label className='default-textstyle' column>
										City <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("CorrespondingCity") ? "w-100 is-invalid m-0" : "w-100 m-0"
												}
												type='text'
												placeholder='City'
												name='CorrespondingCity'
												value={this.state.CorrespondingCity}
												onChange={this.onChange}
												maxLength='60'
											/>
											<span
												className={
													this.hasError("CorrespondingCity") ? "inline-errormsg" : "hidden"
												}
											>
												City is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.CorrespondingCity}</span>
										</Col>
									)}
								</Form.Group>
							</div>
							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextCorrespondingState'>
									<Form.Label className='default-textstyle' column>
										State <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("CorrespondingState") ? "w-100 is-invalid m-0" : "w-100 m-0"
												}
												type='text'
												placeholder='State'
												name='CorrespondingState'
												value={this.state.CorrespondingState}
												onChange={this.onChange}
												maxLength='60'
											/>
											<span
												className={
													this.hasError("CorrespondingState") ? "inline-errormsg" : "hidden"
												}
											>
												State is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.CorrespondingState}</span>
										</Col>
									)}
								</Form.Group>
								<Form.Group className='col-md-6' controlId='formPlaintextCorrespondingCountryID'>
									<Form.Label className='default-textstyle' column>
										Country <span className='text-danger'>*</span>
									</Form.Label>
								{this.state.isEditable ? (
										<Col>
											<Form.Control
												as='select'
												componentClass='select'
												placeholder='select'
												className={
													this.hasError("CorrespondingCountryID")	? "is-invalid w-100 m-0": "w-100 m-0"
												}
												name='CorrespondingCountryID'
												value={this.state.CorrespondingCountryID}
												onChange={this.onChange}
											>
												<option value='0'>select</option>
												{this.state.CountryIDList.map((c) => (
													<option value={c.OptionID}>{c.OptionData}</option>
												))}
											</Form.Control>
											<span
												className={
													this.hasError("CorrespondingCountryID") ? "inline-errormsg" : "hidden"
												}
											>
												Country is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.CorrespondingCountry}</span>
										</Col>
									)}
								</Form.Group>
							</div>
							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextCorrespondingRegionID'>
									<Form.Label className='default-textstyle' column>
										Region <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												as='select'
												componentClass='select'
												placeholder='select'
												className={
													this.hasError("CorrespondingRegionID")	? "is-invalid w-100 m-0": "w-100 m-0"
												}
												name='CorrespondingRegionID'
												value={this.state.CorrespondingRegionID}
												onChange={this.onChange}
											>
												<option value='0'>select</option>
												{this.state.RegionIDList.map((c) => (
													<option value={c.OptionID}>{c.OptionData}</option>
												))}
											</Form.Control>
											<span
												className={
													this.hasError("CorrespondingRegionID") ? "inline-errormsg" : "hidden"
												}
											>
												Region is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.CorrespondingRegion}</span>
										</Col>
									)}
								</Form.Group>
								<Form.Group className='col-md-6' controlId='formPlaintextCorrespondingPostalCode'>
									<Form.Label className='default-textstyle' column>
										Postal Code <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("CorrespondingPostalCode")
														? "w-100 is-invalid m-0"
														: "w-100 m-0"
												}
												type='number'
												placeholder='Postal Code'
												name='CorrespondingPostalCode'
												value={this.state.CorrespondingPostalCode}
												onChange={this.onChange}
												maxLength='10'
											/>
											<span
												className={
													this.hasError("CorrespondingPostalCode") ? "inline-errormsg" : "hidden"
												}
											>
												Postal Code is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.CorrespondingPostalCode}</span>
										</Col>
									)}
								</Form.Group>
							</div>
						</div>
					</div>
					</div>
				</Form>
				<div className='edit-icon-postion'>
					{
						isEditable ? (
							<button
								// disabled={this.state.btnDelete}
								className='btn btn-primary btn_green pull-right waves-effect waves-light btn-xs  mr-2 btn btn-primary transbox-body-content'
								bsStyle='danger'
								bsSize='small'
								onClick={this.onSubmit}
							>
								<FontAwesomeIcon
									title='Delete'
									color={"#FFFFFF"}
									icon={faSave}
									className='mr-0'
								></FontAwesomeIcon>{" "}
								<span>Save</span>
							</button>
						) : (
							// <button className="pull-right" onClick={this.onSubmit}>Save</button>

							<button
								// disabled={this.state.btnDelete}
								className='btn btn-primary btn_green pull-right waves-effect waves-light btn-xs  mr-2 btn btn-primary transbox-body-content'
								bsStyle='danger'
								bsSize='small'
								onClick={this.editProfile}
							>
								<FontAwesomeIcon
									title='Delete'
									color={"#FFFFFF"}
									icon={faEdit}
									className='mr-0'
								></FontAwesomeIcon>{" "}
								Profile
							</button>
						)

						// <span className="pull-right" onClick={this.editProfile}>
						// 	<i className="m-0 default-iconcolor">
						// 		<FontAwesomeIcon icon={faEdit} />
						// 	</i>
						// </span>
					}
				</div>
			</div>
		)
	}
}
const mapPropsToDispatch = (dispatch) => ({
	SetUserEmail: (UserEmail) => {
		dispatch(ReduxAction.addEmail(UserEmail))
	},
	SetContactCompleted: (ContactCompleted) => {
		dispatch(ReduxAction.setContactCompleted(ContactCompleted))
	},
})

export default connect(null, mapPropsToDispatch)(ContactInfo)
