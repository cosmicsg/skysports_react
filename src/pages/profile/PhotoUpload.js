import React, { useState, useEffect } from "react"
import { faPlus } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import UserImage from "../../content/images/user.png"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { APIRequest } from "../../components/api-manager/apiRequest"
import * as ReduxAction from "../../components/redux/actions/userAction"
import { connect } from "react-redux"

function PhotoUpload(props) {
	const [UserProfilePhoto, setUserProfilePhoto] = useState("")

	const uploadedImage = React.useRef(null)
	const imageUploader = React.useRef(null)
	const URL = APIUrl.GET_PROFILE_PHOTO
	useEffect(() => {
		APIRequest.getGetService(URL)
			.then((response) => {
				if (response.Succeeded) {
					setUserProfilePhoto(response.Data.FileData)
				} else {
				}
			})
			.catch((error) => {})
	})
	const handleImageUpload = (e) => {
		let inputData = {}
		const [file] = e.target.files
		if (file) {
			const reader = new FileReader()
			const { current } = uploadedImage
			current.file = file
			reader.onload = (e) => {
				current.src = e.target.result

				inputData = {
					Filename: current.file.name,
					FileType: current.file.type,
					FileData: current.src,
				}
				props.SetProfilePhoto(current.src)
				APIRequest.getPostService(APIUrl.SAVE_PROFILE_PHOTO, inputData)
					.then((response) => {
						if (response.Succeeded) {
						} else {
						}
					})
					.catch((error) => {})
			}
			reader.readAsDataURL(file)
		}
	}
	return (
		<div
			style={{
				display: "flex",
				flexDirection: "column",
				alignItems: "center",
				justifyContent: "center",
			}}
			className='profile-header-img'
		>
			<input
				type='file'
				accept='image/*'
				onChange={handleImageUpload}
				ref={imageUploader}
				style={{
					display: "none",
				}}
			/>
			<div>
				<img
					ref={uploadedImage}
					className='img-circle default-bordercolor profileView'
					src={UserProfilePhoto != null ? UserProfilePhoto : UserImage}
					alt='Profile Photo'
				/>
			</div>
			<span
				className='label label-default rank-label bg-icon-white'
				onClick={() => imageUploader.current.click()}
			>
				<i className='m-0'>
					<FontAwesomeIcon icon={faPlus} />
				</i>
			</span>
		</div>
	)
}

const mapPropsToDispatch = (dispatch) => ({
	SetProfilePhoto: (ProfilePhoto) => {
		dispatch(ReduxAction.addUserProfilePhoto(ProfilePhoto))
	},
})

export default connect(null, mapPropsToDispatch)(PhotoUpload)
