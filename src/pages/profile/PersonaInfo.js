import React from "react"
import { Col, Form } from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import { faEdit, faSave } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { APIRequest } from "../../components/api-manager/apiRequest"
import moment from "moment"
import { DatePicker, Space } from "antd"
import * as ReduxAction from "../../components/redux/actions/userAction"
import { connect } from "react-redux"
import TextArea from "antd/lib/input/TextArea"


class PersonalInfo extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			isEditable: false,
			dropdownDefault: ["Select..."],
			IdentificationTypeID: [],
			Identificationtype: "",
			IdentificationNo: "",
			SalutationID: [],
			Salutation: "",
			FullName: "",
			AliasName: "",
			Comment:"",
			IsResidentialOther:false,
			DOB: "",
			Age: "",
			GenderID: [],
			Gender: "",
			ResidentialStatusID: [],
			ResidentialStatus: "",
			RaceID: [],
			Race: "",
			ParentSalutationID: [],
			ParentSalutation: "",
			ParentName: "",
			ParentEmail: "",
			ParentMobile: "",
			RelationshipID: [],
			Relationship: "",
			NationalityID: [],
			Nationality: "",
			SalutationIDList: [],
			GenderIDList: [],
			ResidentialStatusIDList: [],
			RaceIDList: [],
			RelationshipIDList: [],
			IdentificationTypeIDList: [],
			NationalityIDList: [],
			IsPersonalInfoExist: 0,
			errors: [],
			emailError: "",
		}
		this.fetch = this.fetch.bind(this)
		this.onSubmit = this.onSubmit.bind(this)
	}

	async fetch(state) {
		const fetchURL = APIUrl.GET_PERSONAL_INFO
		let thisSelf = this
		APIRequest.getGetService(fetchURL)
			.then((response) => {
				if (response.Succeeded) {
					thisSelf.setState({
						IdentificationTypeID: response.Data.IdentificationTypeID,
						Identificationtype: response.Data.IdentificationType,
						IdentificationNo: response.Data.IdentificationNo,
						SalutationID: response.Data.SalutationID,
						Salutation: response.Data.Salutation,
						FullName: response.Data.FullName,
						AliasName: response.Data.AliasName,
						Comment:response.Data.Comment,
						DOB: moment(new Date(response.Data.DOB)).format("DD MMM YYYY"),
						GenderID: response.Data.GenderID,
						Gender: response.Data.Gender,
						ResidentialStatusID: response.Data.ResidentialStatusID,
						ResidentialStatus: response.Data.ResidentialStatus,
						RaceID: response.Data.RaceID,
						Race: response.Data.Race,
						ParentSalutationID: response.Data.ParentSalutationID,
						ParentSalutation: response.Data.ParentSalutation,
						ParentName: response.Data.ParentName,
						ParentEmail: response.Data.ParentEmail,
						ParentMobile: response.Data.ParentMobile,
						RelationshipID: response.Data.RelationshipID,
						Relationship: response.Data.Relationship,
						NationalityID: response.Data.NationalityID,
						Nationality: response.Data.Nationality,
						IsPersonalInfoExist: response.Data.IsEditPersonalInfo,
						IsResidentialOther:response.Data.ResidentialStatusID === 5 ? true : false,
					})
					this.props.SetFullName(response.Data.FullName)
					this.getAge(this.state.DOB)

					if (this.state.IsPersonalInfoExist === 0) {
						this.props.onSelectPersonal(true)
					} else {
						this.props.SetPersonCompleted(true)
						this.props.onSelectPersonal(false)
						this.state.isEditable = true
					}
				} else {
				}
			})
			.catch((error) => {
				thisSelf.setState({ loading: false })
			})
	}

	hasError(key) {
		return this.state.errors.indexOf(key) !== -1
	}

	handleDatePickerChange = (event) => {
		debugger
		if (event !== null) {
			let dates = new Date(event)
			let Day = ("0" + dates.getDate()).slice(-2)
			let month = ("0" + (dates.getMonth() + 1)).slice(-2)
			let dataformet = month + "-" + Day + "-" + dates.getFullYear()
			this.getAge(dataformet)
			this.setState({ DOB: dataformet })
		} else {
			this.setState({ DOB: "aN-aN-NaN" })
		}
	}
	DynamicdisabledDate = (current) => {
		let date = this.state.isDiabledDate
		var dateconvert = new Date(date)
		var expiryDate = dateconvert.setDate(new Date(dateconvert).getDate() + 1)
		let dynamicexpiryDate = new Date(expiryDate)
		let Day = ("0" + dynamicexpiryDate.getDate()).slice(-2)
		let month = ("0" + (dynamicexpiryDate.getMonth() + 1)).slice(-2)
		let dataformet = dynamicexpiryDate.getFullYear() + "-" + month + "-" + Day
		const start = moment(dataformet, "YYYY-MM-DD")
		return current < start || current < moment()
		//return moment().add(-1, 'days') >= current
	}
	editProfile = () => {
		this.setState({
			isEditable: true,
		})
	}
	getAge = (dateString) => {
		var today = new Date()
		var birthDate = new Date(dateString)
		var age = today.getFullYear() - birthDate.getFullYear()
		var m = today.getMonth() - birthDate.getMonth()
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--
		}
		this.setState({ Age: age })
	}
	getCommand = (e) =>{
		var residentialStatusID=this.state.ResidentialStatusID
		if(residentialStatusID=== 5){
			this.setState({Comment:e.target.value})
		}
	}

	onSubmit = () => {
		//VALIDATE
		debugger
		var errors = []

		if (
			this.state.IdentificationTypeID === "" ||
			this.state.IdentificationTypeID === undefined ||
			this.state.IdentificationTypeID === null ||
			this.state.IdentificationTypeID.length === 0
		) {
			errors.push("IdentificationTypeID")
		}

		if (
			this.state.IdentificationNo === "" ||
			this.state.IdentificationNo === undefined ||
			this.state.IdentificationNo === null ||
			this.state.IdentificationNo.length === 0
		) {
			errors.push("IdentificationNo")
		}

		if (
			this.state.SalutationID === "" ||
			this.state.SalutationID === undefined ||
			this.state.SalutationID === null ||
			this.state.SalutationID.length === 0
		) {
			errors.push("SalutationID")
		}

		if (
			this.state.FullName === "" ||
			this.state.FullName === undefined ||
			this.state.FullName === null
		) {
			errors.push("FullName")
		}

		if (this.state.DOB === "" || this.state.DOB === undefined || this.state.DOB === null) {
			errors.push("DOB")
		}

		if (this.state.Age === "" || this.state.Age === undefined || this.state.Age === null) {
			errors.push("Age")
		}

		if (
			this.state.GenderID === "" ||
			this.state.GenderID === undefined ||
			this.state.GenderID === null ||
			this.state.GenderID.length === 0
		) {
			errors.push("GenderID")
		}

		if (
			this.state.NationalityID === "" ||
			this.state.NationalityID === undefined ||
			this.state.NationalityID === null ||
			this.state.NationalityID.length === 0
		) {
			errors.push("NationalityID")
		}

		if (
			this.state.ResidentialStatusID === "" ||
			this.state.ResidentialStatusID === undefined ||
			this.state.ResidentialStatusID === null ||
			this.state.ResidentialStatusID.length === 0
		) {
			errors.push("ResidentialStatusID")
		}
		if (this.state.Age < 18 && this.state.Age !== "") {
			if (
				this.state.ParentSalutationID === "" ||
				this.state.ParentSalutationID === undefined ||
				this.state.ParentSalutationID === null ||
				this.state.ParentSalutationID.length === 0
			) {
				errors.push("ParentSalutationID")
			}

			if (
				this.state.ParentName === "" ||
				this.state.ParentName === undefined ||
				this.state.ParentName === null
			) {
				errors.push("ParentName")
			}

			if (
				this.state.ParentEmail === "" ||
				this.state.ParentEmail === undefined ||
				this.state.ParentEmail === null
			) {
				errors.push("ParentEmail")
			} else {
				var Emailpattern = new RegExp(
					/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
				)
				if (!Emailpattern.test(this.state.ParentEmail)) {
					errors.push("InValidEmail")

					this.setState({
						errors: errors,
					})
				}
			}
			if (
				this.state.ParentMobile === "" ||
				this.state.ParentMobile === undefined ||
				this.state.ParentMobile === null
			) {
				errors.push("ParentMobile")
			} else {
				var Mobilepattern = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)

				if (!Mobilepattern.test(this.state.ParentMobile)) {
					errors.push("InValidMobile")

					this.setState({
						errors: errors,
					})
				}
			}

			if (
				this.state.RelationshipID === "" ||
				this.state.RelationshipID === undefined ||
				this.state.RelationshipID === null ||
				this.state.RelationshipID.length === 0
			) {
				errors.push("RelationshipID")
			}
		}
		this.setState({
			errors: errors,
		})

		if (errors.length > 0) {
			return false
		} else {
			var IsSaved = true
			this.props.onSelectPersonal(IsSaved)
			this.props.onChangedFullName(this.state.FullName)
			this.inputData = {
				IdentificationTypeID: this.state.IdentificationTypeID,
				IdentificationNo: this.state.IdentificationNo,
				SalutationID: this.state.SalutationID,
				AliasName: this.state.AliasName,
				Comment:this.state.Comment,
				FullName: this.state.FullName,
				DOB: this.state.DOB,
				GenderID: this.state.GenderID,
				NationalityID: this.state.NationalityID,
				ResidentialStatusID: this.state.ResidentialStatusID,
				RaceID: this.state.RaceID,
				ParentSalutationID: this.state.ParentSalutationID,
				ParentName: this.state.ParentName,
				ParentEmail: this.state.ParentEmail,
				ParentMobile: this.state.ParentMobile,
				RelationshipID: this.state.RelationshipID,
			}
		}

		this.props.SetFullName(this.state.FullName)
		let thisSelf = this
		APIRequest.getPostService(APIUrl.SAVE_PERSONAL_INFO, this.inputData)
			.then((response) => {
				if (response.Succeeded) {
					thisSelf.setState({
						...this.state,
						isEditable: false,
					})
					this.props.SetPersonCompleted(true)
					this.fetch(this.state)
				} else {
					thisSelf.setState({
						...this.state,
						isEditable: true,
					})
				}
				thisSelf.setState({ loading: false, users: response.Data })
			})
			.catch((error) => {
				thisSelf.setState({ loading: false })
			})
	}

	onChange = (e) => {
		debugger
		this.setState({
			...this.state,
			[e.target.name]: e.target.value,
		})
		if (e.target.name === "DOB") {
			this.getAge(e.target.value)
		}
		if(e.target.name === "ResidentialStatusID"){
			if(e.target.value=== "5"){
				this.setState({IsResidentialOther:true})
			}
			else{
				this.setState({IsResidentialOther:false})
			}
		}
	}
	validate = (e) => {
		var Email = e.target.value
		var errors = [...this.state.errors]
		if (Email !== "undefined") {
			var Emailpattern = new RegExp(
				/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
			)
			if (!Emailpattern.test(Email)) {
				errors.push("InValidEmail")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("InValidEmail")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
	}
	ValidMobile = (e) => {
		var Mobile = e.target.value
		var errors = [...this.state.errors]
		if (Mobile !== "undefined") {
			var Mobilepattern = new RegExp(/^\(?([0-9]{3})\)?[- ]?([0-9]{3})[- ]?([0-9]{4})$/)
			if (!Mobilepattern.test(Mobile)) {
				errors.push("InValidMobile")

				this.setState({
					errors: errors,
				})
			} else {
				const index = errors.indexOf("InValidMobile")
				if (index > -1) {
					errors.splice(index, 1)
				}
				this.setState({
					errors: errors,
				})
			}
		}
	}
	componentDidMount() {
		this.fetch(this.state)
		let self = this
		APIRequest.getGetService(APIUrl.NATIONALITY_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						NationalityIDList: response.Data,
					})
				}
			})
			.catch((error) => {})

		APIRequest.getGetService(APIUrl.IDENTIFICATION_TYPE_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						IdentificationTypeIDList: response.Data,
					})
				}
			})
			.catch((error) => {})

		APIRequest.getGetService(APIUrl.SALUTATION_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						SalutationIDList: response.Data,
					})
				}
			})
			.catch((error) => {})

		APIRequest.getGetService(APIUrl.GENDER_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						GenderIDList: response.Data,
					})
				}
			})
			.catch((error) => {})

		APIRequest.getGetService(APIUrl.RESIDENTIAL_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						ResidentialStatusIDList: response.Data,
					})
				}
			})
			.catch((error) => {})

		APIRequest.getGetService(APIUrl.RACE_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						RaceIDList: response.Data,
					})
				}
			})
			.catch((error) => {})

		

		APIRequest.getGetService(APIUrl.RELATIONSHIP_LIST)
			.then((response) => {
				if (response.Succeeded) {
					self.setState({
						RelationshipIDList: response.Data,
					})
				}
			})
			.catch((error) => {})
	}
	render() {
		const { isEditable } = this.state
		return (
			<div className='margin-top-2'>
				<Form>
					<div className='row'>
						<Form.Group className='col-md-6' controlId='formPlaintextIdentificationTypeID'>
							<Form.Label className='default-textstyle' column>
								Identification type <span className='text-danger'>*</span>
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										as='select'
										componentClass='select'
										placeholder='select'
										className={
											this.hasError("IdentificationTypeID") ? "w-100 is-invalid m-0" : "w-100 m-0"
										}
										name='IdentificationTypeID'
										value={this.state.IdentificationTypeID}
										onChange={this.onChange}
										required
									>
										<option value=''>select</option>
										{this.state.IdentificationTypeIDList.map((c) => (
											<option value={c.OptionID}>{c.OptionData}</option>
										))}
									</Form.Control>
									<span
										className={this.hasError("IdentificationTypeID") ? "inline-errormsg" : "hidden"}
									>
										Identification type is mandatory.
									</span>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.Identificationtype}</span>
								</Col>
							)}
						</Form.Group>

						<Form.Group className='col-md-6' controlId='formPlaintextIdentificationNo'>
							<Form.Label className='default-textstyle' column>
								Identification Number <span className='text-danger'>*</span>
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										className={
											this.hasError("IdentificationNo") ? "w-100 is-invalid m-0" : "w-100 m-0"
										}
										type='text'
										placeholder='Identification Number'
										name='IdentificationNo'
										value={this.state.IdentificationNo}
										onChange={this.onChange}
										maxLength='20'
										required
									/>
									<span
										className={this.hasError("IdentificationNo") ? "inline-errormsg" : "hidden"}
									>
										Identification Number is mandatory.
									</span>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.IdentificationNo}</span>
								</Col>
							)}
						</Form.Group>
					</div>
					<div className='row'>
						<Form.Group className='col-md-6' controlId='formPlaintextSalutationID'>
							<Form.Label className='default-textstyle' column>
								Salutation <span className='text-danger'>*</span>
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										as='select'
										componentClass='select'
										placeholder='select'
										className={this.hasError("SalutationID") ? "w-100 is-invalid m-0" : "w-100 m-0"}
										name='SalutationID'
										value={this.state.SalutationID}
										onChange={this.onChange}
										required
									>
										<option value=''>select</option>
										{this.state.SalutationIDList.map((c) => (
											<option value={c.OptionID}>{c.OptionData}</option>
										))}
									</Form.Control>
									<span className={this.hasError("SalutationID") ? "inline-errormsg" : "hidden"}>
										Salutation is mandatory.
									</span>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.Salutation}</span>
								</Col>
							)}
						</Form.Group>
						<Form.Group className='col-md-6' controlId='formPlaintextFullName'>
							<Form.Label className='default-textstyle' column>
								Full Name <span className='text-danger'>*</span>
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										className={this.hasError("FullName") ? "w-100 is-invalid m-0" : "w-100 m-0"}
										type='text'
										placeholder='Full Name'
										name='FullName'
										value={this.state.FullName}
										onChange={this.onChange}
										maxLength='35'
										required
									/>
									<span className={this.hasError("FullName") ? "inline-errormsg" : "hidden"}>
										Full Name is mandatory.
									</span>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.FullName}</span>
								</Col>
							)}
						</Form.Group>
					</div>
					<div className='row'>
					<Form.Group className='col-md-6' controlId='formPlaintextAliasName'>
							<Form.Label className='default-textstyle' column>
								Alias Name
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										className='w-100 m-0'
										type='text'
										placeholder='Alias Name'
										name='AliasName'
										value={this.state.AliasName}
										onChange={this.onChange}
										maxLength='35'
									/>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.AliasName}</span>
								</Col>
							)}
						</Form.Group>
						<Form.Group className='col-md-6' controlId='formPlaintextGenderID'>
							<Form.Label className='default-textstyle' column>
								Gender <span className='text-danger'>*</span>
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										as='select'
										componentClass='select'
										placeholder='select'
										className={this.hasError("GenderID") ? "is-invalid w-100 m-0" : "w-100 m-0"}
										name='GenderID'
										value={this.state.GenderID}
										onChange={this.onChange}
										required
									>
										<option value=''>select</option>
										{this.state.GenderIDList.map((c) => (
											<option value={c.OptionID}>{c.OptionData}</option>
										))}
									</Form.Control>
									<span className={this.hasError("GenderID") ? "inline-errormsg" : "hidden"}>
										Gender is mandatory.
									</span>
								</Col>
							) : (
								<Col className='info-input-padding'>
									<span>{this.state.Gender}</span>
								</Col>
							)}
						</Form.Group>
					</div>
					<div className='row'>
						
						<Form.Group className='col-md-6' controlId='formPlaintextDOB'>
							<Form.Label className='default-textstyle' column>
								DOB <span className='text-danger'>*</span>
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Space direction='vertical' size={1}>
										<DatePicker
											style={{ cursor: "pointer" }}
											className={
												this.hasError("DOB")
													? "myDatePicker datepickerfiled Personal-info is-invalid w-100 m-0 p-0"
													: "myDatePicker datepickerfiled Personal-info w-100 m-0 p-0"
											}
											size='large'
											format='DD MMM YYYY'
											bordered={false}
											value={moment(this.state.DOB)}
											showToday={true}
											allowClear={false}
											name='DOB'
											onChange={this.handleDatePickerChange}
											disabledDate={(current) => {
												return moment().add(0, "days") <= current
											}}
											placeholder='Select DOB'
										/>
										<span className={this.hasError("DOB") ? "inline-errormsg" : "hidden"}>
											DOB is mandatory.
										</span>
									</Space>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.DOB}</span>
								</Col>
							)}
						</Form.Group>
						<Form.Group className='col-md-6' controlId='formPlaintextAge'>
							<Form.Label className='default-textstyle' column>
								Age
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										className='w-100 m-0 disabled'
										placeholder='Age'
										name='Age'
										value={this.state.Age}
										id="Formage"
										// style= "padding-left: 15px;"
									/>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.Age}</span>
								</Col>
							)}
						</Form.Group>
						
					</div>
					<div className='row'>
						<Form.Group className='col-md-6' controlId='formPlaintextNationalityID'>
							<Form.Label className='default-textstyle' column>
								Nationality <span className='text-danger'>*</span>
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										as='select'
										componentClass='select'
										placeholder='select'
										className={
											this.hasError("NationalityID") ? "is-invalid w-100 m-0" : "w-100 m-0"
										}
										name='NationalityID'
										value={this.state.NationalityID}
										onChange={this.onChange}
									>
										<option value=''>select</option>
										{this.state.NationalityIDList.map((c) => (
											<option value={c.OptionID}>{c.OptionData}</option>
										))}
									</Form.Control>
									<span className={this.hasError("NationalityID") ? "inline-errormsg" : "hidden"}>
										Nationality is mandatory.
									</span>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.Nationality}</span>
								</Col>
							)}
						</Form.Group>

						<Form.Group className='col-md-6' controlId='formPlaintextRaceID'>
							<Form.Label className='default-textstyle' column>
								Race
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										as='select'
										componentClass='select'
										placeholder='select'
										className='w-100 m-0'
										name='RaceID'
										value={this.state.RaceID}
										onChange={this.onChange}
									>
										<option value='0'>select</option>
										{this.state.RaceIDList.map((c) => (
											<option value={c.OptionID}>{c.OptionData}</option>
										))}
									</Form.Control>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.Race}</span>
								</Col>
							)}
						</Form.Group>
					</div>

					<div className='row'>
						<Form.Group className='col-md-6' controlId='formPlaintextResidential'>
							<Form.Label className='default-textstyle' column>
								Residential Status <span className='text-danger'>*</span>
							</Form.Label>
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										as='select'
										componentClass='select'
										placeholder='select'
										className={
											this.hasError("ResidentialStatusID") ? "is-invalid w-100 m-0" : "w-100 m-0"
										}
										name='ResidentialStatusID'
										value={this.state.ResidentialStatusID}
										onChange={this.onChange}
										required
									>
										<option value=''>select</option>
										{this.state.ResidentialStatusIDList.map((c) => (
											<option value={c.OptionID}>{c.OptionData}</option>
										))}
									</Form.Control>
									<span
										className={this.hasError("ResidentialStatusID") ? "inline-errormsg" : "hidden"}
									>
										Residential Status is mandatory.
									</span>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.ResidentialStatus}</span>
								</Col>
							)}
						</Form.Group>
						<div className='col-md-6'>
					{this.state.IsResidentialOther ? 
					(
						<Form.Group  controlId='formPlaintextComment'>
						
							<Form.Label className='default-textstyle' column>
								Comment <span className='text-danger'>*</span>
							</Form.Label>
							
							{this.state.isEditable ? (
								<Col>
									<Form.Control
										className='w-100 m-0'
										type='text'
										placeholder=''
										name='Comment'
										value={this.state.Comment}
										onChange={this.onChange}
										maxLength='35'
									/>
								</Col>
							) : (
								<Col className='info-input-padding m-0'>
									<span>{this.state.Comment}</span>
								</Col>
							)}
							
						</Form.Group>)
					: null}
					</div>
					</div>
					{this.state.Age < 18 && this.state.Age !== "" ? (
						<div className='p-t-10'>
							<span className='default-subHeading-textstyle'>
								Parent / Guardian Info
								<br />
							</span>

							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextParentSalutationID'>
									<Form.Label className='default-textstyle' column>
										Salutation <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												as='select'
												componentClass='select'
												placeholder='select'
												className={
													this.hasError("ParentSalutationID") ? "is-invalid w-100 m-0" : "w-100 m-0"
												}
												name='ParentSalutationID'
												value={this.state.ParentSalutationID}
												onChange={this.onChange}
											>
												<option value=''>select</option>
												{this.state.SalutationIDList.map((c) => (
													<option value={c.OptionID}>{c.OptionData}</option>
												))}
											</Form.Control>
											<span
												className={
													this.hasError("ParentSalutationID") ? "inline-errormsg" : "hidden"
												}
											>
												Salutation is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.ParentSalutation}</span>
										</Col>
									)}
								</Form.Group>
								<Form.Group className='col-md-6' controlId='formPlaintextParentName'>
									<Form.Label className='default-textstyle' column>
										Name <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("ParentName") ? "is-invalid w-100 m-0" : "w-100 m-0"
												}
												type='text'
												placeholder='ParentName'
												name='ParentName'
												value={this.state.ParentName}
												onChange={this.onChange}
												maxLength='35'
											/>
											<span className={this.hasError("ParentName") ? "inline-errormsg" : "hidden"}>
												Name Status is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.ParentName}</span>
										</Col>
									)}
								</Form.Group>
							</div>
							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextParentEmail'>
									<Form.Label className='default-textstyle' column>
										Email <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("ParentEmail") ? "is-invalid w-100 m-0" : "w-100 m-0"
												}
												type='email'
												placeholder='Parent Email'
												name='ParentEmail'
												value={this.state.ParentEmail}
												onChange={this.onChange}
												onBlur={this.validate}
												maxLength='100'
											/>
											<span className={this.hasError("ParentEmail") ? "inline-errormsg" : "hidden"}>
												Email Status is mandatory.
											</span>

											<span
												className={this.hasError("InValidEmail") ? "inline-errormsg" : "hidden"}
											>
												Please enter the valid Email.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.ParentEmail}</span>
										</Col>
									)}
								</Form.Group>
								<Form.Group className='col-md-6' controlId='formPlaintextParentMobile'>
									<Form.Label className='default-textstyle' column>
										Mobile <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												className={
													this.hasError("ParentMobile") ? "is-invalid w-100 m-0" : "w-100 m-0"
												}
												type='number'
												placeholder='Parent Mobile'
												name='ParentMobile'
												value={this.state.ParentMobile}
												onChange={this.onChange}
												onBlur={this.ValidMobile}
												maxLength='15'
											/>
											<span
												className={this.hasError("ParentMobile") ? "inline-errormsg" : "hidden"}
											>
												Mobile is mandatory.
											</span>

											<span
												className={this.hasError("InValidMobile") ? "inline-errormsg" : "hidden"}
											>
												Please enter the valid Mobile number.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.ParentMobile}</span>
										</Col>
									)}
								</Form.Group>
							</div>
							<div className='row'>
								<Form.Group className='col-md-6' controlId='formPlaintextRelationshipID'>
									<Form.Label className='default-textstyle' column>
										Relationship <span className='text-danger'>*</span>
									</Form.Label>
									{this.state.isEditable ? (
										<Col>
											<Form.Control
												as='select'
												componentClass='select'
												placeholder='select'
												className={
													this.hasError("RelationshipID") ? "is-invalid w-100 m-0" : "w-100 m-0"
												}
												name='RelationshipID'
												value={this.state.RelationshipID}
												onChange={this.onChange}
											>
												<option value=''>select</option>
												{this.state.RelationshipIDList.map((c) => (
													<option value={c.OptionID}>{c.OptionData}</option>
												))}
											</Form.Control>
											<span
												className={this.hasError("RelationshipID") ? "inline-errormsg" : "hidden"}
											>
												Relationship is mandatory.
											</span>
										</Col>
									) : (
										<Col className='info-input-padding m-0'>
											<span>{this.state.Relationship}</span>
										</Col>
									)}
								</Form.Group>
							</div>
						</div>
					) : (
						<div></div>
					)}
				</Form>
				<div className='edit-icon-postion'>
					{
						isEditable ? (
							<button
								disabled={this.state.btnDelete}
								className='btn btn-primary btn_green pull-right waves-effect waves-light btn-xs  mr-2 btn btn-primary transbox-body-content'
								bsStyle='danger'
								bsSize='small'
								onClick={this.onSubmit}
							>
								<FontAwesomeIcon
									title='Delete'
									color={"#FFFFFF"}
									icon={faSave}
									className='mr-0'
								></FontAwesomeIcon>{" "}
								<span>Save</span>
							</button>
						) : (
							// <button className="pull-right" onClick={this.onSubmit}>Save</button>

							<button
								disabled={this.state.btnDelete}
								className='btn btn-primary btn_green pull-right waves-effect waves-light btn-xs  mr-2 btn btn-primary transbox-body-content'
								bsStyle='danger'
								bsSize='small'
								onClick={this.editProfile}
							>
								<FontAwesomeIcon
									title='Delete'
									color={"#FFFFFF"}
									icon={faEdit}
									className='mr-0'
								></FontAwesomeIcon>{" "}
								Profile
							</button>
						)

						// <span className="pull-right" onClick={this.editProfile}>
						// 	<i className="m-0 default-iconcolor">
						// 		<FontAwesomeIcon icon={faEdit} />
						// 	</i>
						// </span>
					}
				</div>
			</div>
		)
	}
}

const mapPropsToDispatch = (dispatch) => {
	return {
		SetFullName: (UserFullname) => {
			dispatch(ReduxAction.addFullName(UserFullname))
		},
		SetPersonCompleted: (PersonCompleted) => {
			dispatch(ReduxAction.setPersoanlCompleted(PersonCompleted))
		},
	}
}

export default connect(null, mapPropsToDispatch)(PersonalInfo)
