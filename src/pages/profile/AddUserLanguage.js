import React, { useState } from "react"
import ReactTable from "react-table"
import "../../styles/CosmicTable.css"
import { Button, Modal, Row, Col, Form } from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import "../../App"
import { Input, notification, Select, Checkbox } from "antd"
import "antd/dist/antd.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faPlusCircle, faTrashAlt } from "@fortawesome/free-solid-svg-icons"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { APIRequest } from "../../components/api-manager/apiRequest"

const { Option } = Select
const openNotificationWithIcon = (type, description, flag) => {
	if (flag === 1) {
		///////delete flag
		notification[type]({
			message: description,
			icon: <FontAwesomeIcon className='fa-xs icon_Delete ' icon={faTrashAlt}></FontAwesomeIcon>,
			description: "",
		})
	} else if (flag === 2) {
		notification[type]({
			message: description,
			description: "",
			//  duration:20
		})
	}
}

const NoDataConst = (props) => <span className='table-nodata-style'>No records found. </span>

const openModal = (rowid, self, flg, id) => {
	self.setState({ isOpen: true, isDeleteId: id, isDeleteRowId: rowid, isDeleteflag: flg })
}

class AddUserLanguage extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			rowcount: 0,
			selected: {},
			fields: [],
			errors: {
				AddanotherLanguage: "",
			},
			isOpen: false,
			isDelete: false,
			isDeleteId: 0,
			isDeleteflag: 0,
			addLanguagetxt: "Add Language",
			languageId: [],
			languageList: [],
			Write: false,
			Speak: false,
			Read: false,
		}
		this.table = React.createRef()
		this.handleAddRow = this.handleAddRow.bind(this)
		this.deleteModal = this.deleteModal.bind(this)
	}
	deleteModal = async () => {
		let id = this.state.isDeleteId
		let Rowid = this.state.isDeleteRowId
		let flag = this.state.isDeleteflag
		this.props.isLoadingStartFunc()
		let fields = this.state.fields
		fields.splice(Rowid, 1)
		this.setState({ fields })
		this.closeModal()
		this.setState({ errors: [], emailExiest: 0 })
		let rowId = this.state.selected
		if (rowId !== undefined) {
			delete rowId[id]
		}
		this.props.isLoadingStopFunc()
		openNotificationWithIcon("success", "Record has been deleted", 1)
		if (Object.keys(this.state.selected).length === 0) {
			this.setState({ btnDelete: true, btnSend: true })
		}
		if (this.state.fields.length === 0) {
			this.setState({
				rowcount: 0,
				addUsertxt: "Add Language",
			})
		}
	}
	closeModal = () => {
		this.setState({ isOpen: false })
	}

	handleCheck = (cellInfo, event) => {
		let fields = [...this.state.fields]
		fields[cellInfo.index][cellInfo.column.id] = event.target.value
		this.setState({ fields })
	}
	handleAddRow = (e) => {
		e.preventDefault()
		let count = this.state.rowcount
		let rowData = this.state.fields.length
		let fields = this.state.fields
		for (let i = 0; i < fields.length; i++) {
			let data = this.state.fields[i]
		}
		if (rowData === 0) {
			count = 0
			this.setState({
				addLanguagetxt: "Add another Language",
			})
		}
		this.setState({
			rowcount: count + 1,
		})
		const values = [...this.state.fields]
		values.push({
			Write: false,
			Speak: false,
			Read: false,
			id: count,
		})
		this.setState({
			fields: values,
			errors: {
				AddanotherLanguage: "",
			},
		})
	}
	handleDropdownChange = (cellInfo, event) => {
		let fields = [...this.state.fields]
		fields[cellInfo.index][cellInfo.column.id] = event
		this.setState({ fields })
		let data = [cellInfo.column.id].toString()
		let id = [cellInfo.index].toString()
		let value = event.toString()
	}
	handlefocusoutChange = (cellInfo, event) => {
		let data = [cellInfo.column.id].toString()
		let id = [cellInfo.index].toString()
		let language = document.getElementById("language_tbl" + id)
		let fields = [...this.state.fields]
		let value = fields[cellInfo.index][cellInfo.column.id].toString()
	}

	handleInputChange = (cellInfo, event) => {
		let fields = [...this.state.fields]
		fields[cellInfo.index][cellInfo.column.id] = event.target.value
		this.setState({ fields })
		let fullname = [cellInfo.column.id].toString()
		let id = [cellInfo.index].toString()
		if (fullname === "fullname" && event.target.value !== "") {
			var myElementID = document.getElementById("Fullname_tbl" + id)
			var element = document.getElementById("Fullname_tbl" + id)
		}
	}

	handleBtnSendALL = (self) => {
		let selectdata = self.state.fields
		var InputData = []
		var obj = {}
		let result = 0
		if (self.state.emailExiest === 0) {
			for (var i = 0; i < selectdata.length; i++) {
				let data = self.state.fields[i]
				let id = data.id
				let role = data.role
				let roledata = role.toString()
				let email = document.getElementById("Email_tbl" + i)
				let fullname = document.getElementById("Fullname_tbl" + i)
				let Roleid = document.getElementById("Role_tbl" + i)
				let country = document.getElementById("Country_tbl" + i)
				let council = document.getElementById("council_tbl" + i)
				let association = document.getElementById("association_tbl" + i)
				let club = document.getElementById("club_tbl" + i)
				let department = document.getElementById("Department_tbl" + i)
				let date = document.getElementById("Date_tbl" + i)
				if (data.fullname === undefined || data.fullname === "") {
					if (fullname !== null) {
						result = 1
						fullname.classList.add("mandatory_text")
					}
				}
				if (data.email === undefined || data.email === "") {
					if (email !== null) {
						result = 1
						email.classList.add("mandatory_text")
					}
				}
				if (data.role === undefined || roledata === "") {
					if (Roleid != null) {
						result = 1
						Roleid.classList.add("mandatory_text")
					}
				}
				if (
					(data.country === undefined || data.country.toString() === "") &&
					self.state.isCountryCloumn === true
				) {
					if (country !== null) {
						result = 1
						country.classList.add("mandatory_text")
					}
				}
				if (
					(data.council === undefined || data.council.toString() === "") &&
					self.state.iscouncilCloumn === true
				) {
					if (council !== null) {
						result = 1
						council.classList.add("mandatory_text")
					}
				}
				if (
					(data.association === undefined || data.association.toString() === "") &&
					self.state.isAssociationColumn === true
				) {
					if (association !== null) {
						result = 1
						association.classList.add("mandatory_text")
					}
				}
				if (
					(data.club === undefined || data.club.toString() === "") &&
					self.state.isClubColumn === true
				) {
					if (club !== null) {
						result = 1
						club.classList.add("mandatory_text")
					}
				}
				if (
					(data.department === undefined || data.department.toString() === "") &&
					self.state.isDepartmentCloumn === true
				) {
					if (department != null) {
						result = 1
						department.style.borderBottomColor = "red"
						// department.classList.add("mandatory_text");
					}
				}
				if (data.date === undefined || data.date === "") {
					if (date != null) {
						result = 1
						date.classList.add("mandatory_text")
					}
				}
				if (result === 1) {
				} else {
					obj["ClubID"] = data.club.toString() === "" ? "0" : data.club.toString()
					obj["Association"] = self.state.isAssociationUsertab
					obj["Committee"] = self.state.isCommitteetab
					obj["Council"] = self.state.isCounciltab
					obj["Club"] = self.state.isClubUsertab
					obj["Athlete"] = self.state.isAthleteUsertab
					obj["Coach"] = self.state.isCoachUsertab
					InputData.push(obj)
					obj = {}
				}
			}
			if (result === 0) {
				self.props.isLoadingStartFunc()
				APIRequest.getPostService(APIUrl.ADD_INVITE_USERS, InputData)
					.then((response) => {
						if (response.Succeeded === true) {
							self.setState({ fields: [], rowcount: 0, selected: [] })
							self.props.refreshInvitedListFunc()
							self.props.isLoadingStopFunc()
							openNotificationWithIcon("success", "Email notification sent success", 2)
						}
					})
					.catch((error) => {
						self.props.isLoadingStopFunc()
					})
			} else if (result === 1) {
				let errors = {}
				errors["Addanotheruser"] =
					"One or more mandatory fields have an error. Please check and try again"
				self.setState({
					errors: errors,
				})
			}
		} else if (self.state.emailExiest === 1) {
			let errors = {}
			errors["Addanotheruser"] = "Email is not valid "
			self.setState({
				errors: errors,
			})
		} else if (self.state.emailExiest === 2) {
			let errors = {}
			errors["Addanotheruser"] = "Email already exists "
			self.setState({
				errors: errors,
			})
		}
	}
	renderEditableLanguage = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"language_tbl" + cellInfo.index}
				className={"tbl_listfiled language_tbl" + cellInfo.index}
			>
				<Select
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					showSearch
					name='language'
					bordered={false}
					placeholder='Select Language'
					optionFilterProp='children'
					defaultValue='Select'
					onBlur={this.handlefocusoutChange.bind(null, cellInfo)}
					onChange={this.handleDropdownChange.bind(null, cellInfo)}
					filterSort={(optionA, optionB) =>
						optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
					}
				>
					{this.state.languageList.map((c) => (
						<Option value={c.OptionID}>{c.OptionData}</Option>
					))}
				</Select>
			</div>
		)
	}
	renderEditableReadCheckBox = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"language_tbl" + cellInfo.index}
				className={"tbl_listfiled language_tbl" + cellInfo.index}
			>
				<input
					type='checkbox'
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					name='Read'
					bordered={false}
					onChange={this.handleCheck.bind(null, cellInfo)}
				></input>
			</div>
		)
	}

	renderEditableWriteCheckBox = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"language_tbl" + cellInfo.index}
				className={"tbl_listfiled language_tbl" + cellInfo.index}
			>
				<input
					type='checkbox'
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					name='Write'
					bordered={false}
					onChange={this.handleCheck.bind(null, cellInfo)}
				></input>
			</div>
		)
	}
	renderEditableSpeakCheckBox = (cellInfo) => {
		let cellValue = ""
		if (this.state.fields.length > cellInfo.index) {
			cellValue = this.state.fields[cellInfo.index][cellInfo.column.id]
		}
		return (
			<div
				id={"language_tbl" + cellInfo.index}
				className={"tbl_listfiled language_tbl" + cellInfo.index}
			>
				<input
					type='checkbox'
					style={{ width: "100%" }}
					value={cellValue}
					size={"large"}
					name='Speak'
					bordered={false}
					onChange={this.handleCheck.bind(null, cellInfo)}
				></input>
			</div>
		)
	}
	handleTest = () => {
		alert("test")
	}
	async componentDidMount() {
		const languageList = await APIRequest.getGetService(APIUrl.LANGUAGE_LIST)
		this.setState({ languageList: languageList.Data })
	}
	render() {
		// table columns
		const columns = [
			{
				Header: () => <div className='vertical_center pl-2'>Language</div>,
				accessor: "fullname",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader ",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableLanguage,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='vertical_center pl-2'>Read</div>,
				accessor: "Read",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableReadCheckBox,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='vertical_center pl-2'>Write</div>,
				accessor: "Write",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader ",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableWriteCheckBox,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='vertical_center pl-2'>Speak</div>,
				accessor: "Speak",
				headerClassName: "BoldText ColoredText",
				align: "left",
				className: "tableheader ",
				sortable: false,
				resizable: false,
				Cell: this.renderEditableSpeakCheckBox,
				style: {
					textAlign: "center",
				},
			},
			{
				Header: () => <div className='vertical_center'>Action</div>,
				accessor: "Action",
				className: "tableheader",
				resizable: false,
				sortable: false,
				filterable: false,
				minWidth: 30,
				width: 70,
				maxWidth: 80,
				id: "delete",
				accessor: (str) => "delete",
				Cell: (row) => (
					<div className='mt-2'>
						<span
							className='icon_Delete '
							onClick={() => openModal(row.index, this, 3, row.original.id)} //{
						>
							<FontAwesomeIcon title='Delete' icon={faTrashAlt}></FontAwesomeIcon>
						</span>{" "}
					</div>
				),
			},
		]
		return (
			<div className='admin-report-table'>
				<div className=' d-flex justify-content-start pt-1 '>
					<p className='tab_form_content'></p>
				</div>

				<ReactTable
					data={this.state.fields}
					columns={columns}
					showPaginationTop={false}
					showPaginationBottom={false}
					minRows={0}
					ref={this.table}
					sortable={false}
					multiSort={false}
					NoDataComponent={NoDataConst}
					manual
					style={{ overflow: "wrap" }}
					className='CosmicTable InviteUser form-group'
					manualPagination={false}
				/>
				<div>
					<button onClick={this.handleAddRow} className='btn-default border-0 bg-white link'>
						{this.state.addLanguagetxt} <FontAwesomeIcon icon={faPlusCircle}></FontAwesomeIcon>
					</button>
				</div>
				{this.state.isOpen ? (
					<>
						<Modal size='sm' backdrop='static' show={this.state.isOpen} onHide={this.closeModal}>
							<div className='popup-content'>
								<div className='deletecircle'></div>
								<i className='fa fa-trash-o deleteicon'></i>
								<Modal.Body>
									<p className='pull-left' style={{ margin: "4rem 2rem" }}>
										{" "}
										<h5>You are about to delete an invite(s)</h5>
										<h6 style={{ color: "darkgray" }}>
											This will delete your invite(s) from the list
										</h6>
										<h6 style={{ color: "darkgray", marginRight: "7rem", marginBottom: "-3rem" }}>
											Are you sure?
										</h6>
									</p>
									<br />
									<div className='pull-right'>
										<Button className='btn_cancel mr-2' onClick={this.closeModal}>
											Cancel
										</Button>
										<Button className='btn_green' onClick={this.deleteModal}>
											Delete
										</Button>
									</div>
								</Modal.Body>
							</div>
						</Modal>
					</>
				) : null}
			</div>
		)
	}
}

export default AddUserLanguage
