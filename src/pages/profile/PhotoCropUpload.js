import React, { useState } from "react"
import { Upload } from "antd"
import ImgCrop from "antd-img-crop"
import { faPlus } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import UserImage from "../../content/images/hd.png"

export const PhotoCropUpload = () => {
	const [fileList, setFileList] = useState([])

	const onChange = ({ fileList: newFileList }) => {
		{
			fileList.length <= 1 && setFileList(newFileList)
		}
	}

	const onPreview = async (file) => {
		let src = file.url
		if (!src) {
			src = await new Promise((resolve) => {
				const reader = new FileReader()
				reader.readAsDataURL(file.originFileObj)
				reader.onload = () => resolve(reader.result)
			})
		}
		const image = new Image()
		image.src = src
		const imgWindow = window.open(src)
		imgWindow.document.write(image.outerHTML)
	}

	return (
		<div className='profile-header-img'>
			<ImgCrop rotate>
				<Upload
					action=''
					listType='picture-card'
					fileList={fileList}
					onPreview={onPreview}
					onChange={onChange}
				>
					{fileList.length < 1 && (
						<span className='label label-default rank-label bg-icon-white'>
							<i className='m-0'>
								{" "}
								<FontAwesomeIcon icon={faPlus} />{" "}
							</i>
						</span>
					)}
				</Upload>
			</ImgCrop>
		</div>
	)
}
