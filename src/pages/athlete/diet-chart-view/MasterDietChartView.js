import { forwardRef, useEffect, useState } from "react";
import { connect } from "react-redux";
import { Col, Row } from "react-bootstrap";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronDown,
  faChevronLeft,
  faChevronRight,
  faChevronUp,
} from "@fortawesome/free-solid-svg-icons";

import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";

import { Permissions as Permission } from "../../../permissions/AppPermissions";
import PermissionProvider from "../../../permissions/PermissionProvider";

export const MasterDietChartView = forwardRef((props, ref) => {

  // 
	//	Reading user's permissions from PermissionProvider component
	//
  let permissions = props.getAppPermissions();

  /// default current date get date
  const defualtDate = new Date().toString().split(" ");
  let defualtDates =
    defualtDate[2] + "-" + defualtDate[1] + "-" + defualtDate[3];

  const [inputData, setInputData] = useState({
    subHeader: defualtDates,
    listDate: [],
    isexpand: true,
    isexpandTxt: "Expand All",
    permissions: permissions,
  });

  /// Expand collapse change function
  useEffect(() => {
    if (inputData.isexpand === true) {
      handelExpandAll(false);
    }
    if (inputData.isexpand === false) {
      handelExpandAll(true);
    }
  }, [inputData.isexpand]);

  // useEffect(()=>{

  //
  // if(inputData.listDate.length > 0){
  //   let expand=  inputData.listDate.filter((x)=>x.isExpend=true)
  //   let closeall=  inputData.listDate.filter((x)=>x.isExpend=false)
  // if(expand.length===inputData.listDate.length){
  //     setInputData((prev)=>({...prev,isexpand:false}))
  // }
  // if(closeall.length===inputData.listDate.length){
  //     setInputData((prev)=>({...prev,isexpand:true}))
  // }
  // }
  // },[inputData.listDate])

  //// Previous Date Click function
  const getPreviousDate = () => {
    let datess = "";
    if (inputData.subHeader === "" || inputData.subHeader === undefined) {
      datess = moment(new Date(), "DD MMM YYYY");
    } else {
      datess = moment(inputData.subHeader, "DD MMM YYYY");
    }

    const currentDayInMilli = new Date(datess).getTime();
    const oneDay = 1000 * 60 * 60 * 24;
    const previousDayInMilli = currentDayInMilli - oneDay;
    const previousDates = new Date(previousDayInMilli).toString().split(" ");

    let previousDate =
      previousDates[2] + "-" + previousDates[1] + "-" + previousDates[3];
    setInputData((prev) => ({
      ...prev,
      subHeader: previousDate,
    }));
  };
  /// next date change function
  const getNextDate = () => {
    let datess = "";
    if (inputData.subHeader === "" || inputData.subHeader === undefined) {
      datess = moment(new Date(), "DD MMM YYYY");
    } else {
      datess = moment(inputData.subHeader, "DD MMM YYYY");
    }

    const currentDayInMilli = new Date(datess).getTime();
    const oneDay = 1000 * 60 * 60 * 24;
    const nextDayInMilli = currentDayInMilli + oneDay;
    const nextDates = new Date(nextDayInMilli).toString().split(" ");

    let nextDate = nextDates[2] + "-" + nextDates[1] + "-" + nextDates[3];
    setInputData((prev) => ({
      ...prev,
      subHeader: nextDate,
    }));
  };
  ///expand all and close all change function
  const handelExpandAll = (datas) => {
    let data = inputData.listDate;
    if (data.length !== 0) {
      for (let masterid = 0; masterid < data.length; masterid++) {
        let masterdata = data[masterid];
        var toggle = document.getElementById(
          "DietGroup" + masterdata.CategoryID
        );
        if (datas === true) {
          // if (masterdata.isExpend === true) {
          data[masterid].isExpend = true;
          toggle.classList.remove("hidden");
        } else {
          data[masterid].isExpend = false;
          toggle.classList.add("hidden");
        }
      }
      setInputData((prev) => ({ ...prev, listDate: data }));
    }
  };
  ///expand function and close function
  const handelExpand = (id) => {
    var toggle = document.getElementById("DietGroup" + id);

    let data = inputData.listDate;
    if (data.length !== 0) {
      for (let masterid = 0; masterid < data.length; masterid++) {
        let masterdata = data[masterid];
        if (id === masterdata.CategoryID) {
          if (masterdata.isExpend === false) {
            masterdata.isExpend = true;
            toggle.classList.remove("hidden");
          } else {
            masterdata.isExpend = false;
            toggle.classList.add("hidden");
          }
        }
      }
    }
    setInputData((prev) => ({
      ...prev,
      listDate: data,
    }));
  };
  /// screen onload functions
  useEffect(() => {
    var toggleWhite = document.getElementsByClassName("padding-body-3");
    var toggle = document.querySelectorAll(".ant-layout-has-sider");

    // if(toggleWhite.length >0){
    //     toggleWhite[0].classList.add("diet-chart-body")
    //     toggleWhite[0].classList.remove("bg-white")
    // }
    // if(toggle.length >0){

    //     toggle[0].classList.remove("background-body-color")
    //     toggle[0].classList.add("diet-chart-body")
    // }

    var defualtDate=moment(new Date()).format("DD MMM YYYY");
    APIRequest.getGetService(APIUrl.GET_DIET_CHART_VIEW_DATA+"/"+ defualtDate).then((res) => {
      if (res.Succeeded) {
        setInputData((prevState) => ({
          ...prevState,
          listDate: res.Data,
        }));
      }
    });
  }, []);

  useEffect(async()=>{

   async function fetchApi(){
    APIRequest.getGetService(APIUrl.GET_DIET_CHART_VIEW_DATA+"/"+ inputData.subHeader).then((response) => {
      if (response.Succeeded) {
        
        setInputData((prevState) => ({
          ...prevState,
          listDate: response.Data,
        }));
      }
    })
  }
  fetchApi();
  },[inputData.subHeader]);

  return (
    <div className="diet-chart-body">
      {inputData.permissions.includes(Permission.ATHLETE_DIET_CHART_VIEW) ? ( 
        <>
          <Row className="pl-5 form-group pt-3">
            <Col className="font-weight-bold">
              <span className="text-black-50"> The Diet Chart View</span>
            </Col>
          </Row>
          <Row className="pl-4 form-group  pt-4">
            <Col className="font-weight-bold" lg={8}>
              <span>
                {" "}
                <FontAwesomeIcon
                  onClick={() => getPreviousDate()}
                  style={{ cursor: "pointer" }}
                  icon={faChevronLeft}
                  className="sm arrow_Icons"
                />
              </span>{" "}
              <span className="m-3">
              {inputData.subHeader}{" "}
              </span>
              <span>
                {" "}
                <FontAwesomeIcon
                  onClick={() => getNextDate()}
                  style={{ cursor: "pointer" }}
                  className="arrow_Icons"
                  icon={faChevronRight}
                />
              </span>
            </Col>
            <Col lg={4} className="text-right diet-expand-btn-div">
              <button
                className="diet-expand-btn font-italic"
                onClick={() =>
                  setInputData((prev) => ({
                    ...prev,
                    isexpand: inputData.isexpand === true ? false : true,
                    isexpandTxt:
                      inputData.isexpandTxt === "Expand All"
                        ? "Close All"
                        : "Expand All",
                  }))
                }
              >
                {inputData.isexpandTxt}
              </button>
            </Col>
          </Row>

          <div>
            {inputData.listDate.map((x) => {
              let headerFlag = "";
              let hiddenflag="hidden";
              if (x.isExpend === true) {
                headerFlag = "diet-subheader-active";
                 hiddenflag="";
              }
              return (
                <>
                  <div className="diet-group-box">
                    <Row className="w-100">
                      <Col lg={11} sm={11} xs={11} className="mb-0">
                        <h4 className={headerFlag + " mb-0"}>{x.Category}</h4>
                      </Col>
                      <Col
                        lg={1}
                        sm={1}
                        xs={1}
                        className="text-right"
                        onClick={() => handelExpand(x.CategoryID)}
                      >
                        {" "}
                        {x.isExpend ? (
                          <FontAwesomeIcon
                            className=" Diet_Group diet-expand-icon"
                            icon={faChevronUp}
                          ></FontAwesomeIcon>
                        ) : (
                          <FontAwesomeIcon
                            className=" diet-expand-icon Diet_Group"
                            icon={faChevronDown}
                          ></FontAwesomeIcon>
                        )}
                      </Col>
                    </Row>
                    <div
                      className={' px-0 DietGroup '+hiddenflag}
                      id={"DietGroup" + x.CategoryID}
                    >
                      <Row>
                        <Col sm={12} md={6} lg={3}></Col>
                        <Col sm={6} md={6} lg={2} className="font-weight-bold">
                          <span>Calories</span>
                        </Col>
                        <Col sm={6} md={6} lg={2} className="font-weight-bold">
                          <span>Protein</span>
                        </Col>
                        <Col sm={6} md={6} lg={2} className="font-weight-bold">
                          <span>Fat</span>
                        </Col>
                        <Col sm={6} md={6} lg={2} className="font-weight-bold">
                          <span>Carbo Hydrate</span>
                        </Col>
                      </Row>
                      {x.FoodItem.map((y) => {
                        return (
                          <>
                            <Row className="form-group">
                              <Col sm={12} lg={3} md={6}>
                                <img
                                  src={y.FoodImageUrl}
                                  style={{ width: 40 }}
                                ></img>
                                <span className="">
                                  {"  "}
                                  {y.ItemName}
                                </span>
                              </Col>
                              <Col sm={6} lg={2} md={6}>
                                <span className="border">{y.Calories}</span>
                              </Col>
                              <Col sm={6} lg={2} md={6}>
                                <span className="border">{y.Protein}</span>
                              </Col>
                              <Col sm={6} lg={2} md={6}>
                                <span className="border">{y.Fat}</span>
                              </Col>
                              <Col sm={6} lg={2} md={6}>
                                <span className="border">{y.CarboHydrate}</span>
                              </Col>
                            </Row>
                          </>
                        );
                      })}
                    </div>
                  </div>{" "}
                </>
              );
            })}
          </div>
        </>
       ) : null} 
    </div>
  );
});

export default PermissionProvider(MasterDietChartView);
