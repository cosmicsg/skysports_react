

import { Chart } from "react-google-charts";
import { Col } from "react-bootstrap"
import "../../..//styles/grid.css"
import React, { useEffect, useState } from "react";
import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import PermissionProvider from "../../../permissions/PermissionProvider";
import { Permissions as Permission } from "../../../permissions/AppPermissions";
import {Select} from "antd"
import { CaretDownOutlined } from "@ant-design/icons";

const TraningPerformance = (props) => {


    let permissions = props.getAppPermissions();

    const [inputData, setInputtData] = useState({
        displayHeader_Date:"",
        toDate: "",
        fromDate:"",
        traningPerformance:[],
        weekData:[],
        weekNumber:[],
         permissions: permissions,
    });
  
    useEffect(() => {
APIRequest.getGetService(APIUrl.GET_TRAINING_PERFORMANCE_WEEK_DATA).then((res) => {

    
    setInputtData((prevState) => ({
        ...prevState,weekData:res.Data
            }));
})


    }, [])
  
    const onSearchChartData=()=>{
        
  /// Dashbord AthleteResult Data get API
                APIRequest.getGetService(APIUrl.GET_TRAINING_PERFORMANCE + "/"+ inputData.fromDate  +"/" + inputData.toDate 
                ).then((res) => {
                    let dataArray = [];
                 
                    if (res.Succeeded) {
        
                        if (res.Data.length > 0) {
                            let count=0;
                            res.Data.map((x) => {

                                let names= x.ProfileName.split(",,");
                                if(count==0){
                                dataArray.push(['x', x.ProfileName])
                                }
                                count=1;
                                // var result = x.Data.split(",").map(function (x) { 
                                    
                                //     return parseInt(x, 10); 
                                //   });
                                // let proformanceData=x.Data.split(",")
                                // dataArray.push([x.Date, parseFloat(x.Data)])
                                if(x.Data==null){
                                    x.Data=0;
                                }
                                dataArray.push([x.Date, parseFloat(x.Data)])
                            })
                        }
                      
                        setInputtData((prevState) => ({
                            ...prevState,
                            traningPerformance: dataArray
                        }))
                    }
                })
    }
            const onchangeWeek=(week, key)=>{


let data =inputData.weekData.filter((x)=> x.WeekNumber==week);

if(data.length>0){
    setInputtData((prev)=>({...prev,weekNumber:week,fromDate:data[0].StartDate,toDate:data[0].EndDate}))
}
else{
    setInputtData((prev)=>({...prev,weekNumber:week,}))
}
               

            }
    return (

        <>{inputData.permissions.includes(Permission.TRAINING_PERFORMANCE_VIEW)?<>
            <h2 className="ml-3 font-weight-bold pt-3 text-secondary">Training Performance </h2>
            <Col sm={12} lg={12} md={12} xl={12}>
                <div className='training-performance-box'>
                    <div className="form-group pt-3">
                        <div className="row text-center form-group">
                            <div className=" col-sm-0 col-xs-0 col-md-0 col-lg-3 col-xl-3 "> </div>
                            <div className=" col-sm-12 col-xs-12 col-md-6 col-lg-3 col-xl-3 ">
                                <div className="selectdiv form-group">
                                    <Select size="lg" 
                                    placeholder="Select Week"
                                    suffixIcon={<CaretDownOutlined style={{ fontSize: '18px', color: '#297b86' ,marginTop:"-3px"}}/>}
                                
                                 onChange={(val,key)=> onchangeWeek(val,key)}  className=" lg form-group form-control color-textfiled text-center  font-weight-bold" value={inputData.weekNumber}>
                                        
                                   
                                        {inputData.weekData.map((c) => (
                                    <option  value={c.WeekNumber}>{"Week"+c.WeekNumber}</option>
                                ))} 
                                    </Select>
                                </div>
                            </div>
                            <div className=" col-sm-12 col-xs-12 col-md-6 col-lg-3 col-xl-3">
                                <button onClick={onSearchChartData} className="btn_lg_green btn">Search</button>
                            </div>
                        </div>
                        {inputData.weekNumber !=0 ?(
                        <div className="row text-center">
                            <Col className="font-weight-bold form-group text-center">
                                <span>    {/*onClick={() => getWeekDate("-1")}> */}
                                    <FontAwesomeIcon
                                        style={{ cursor: "pointer" }}
                                        icon={faChevronLeft}
                                        className="sm arrow_Icons"
                                    />
                                </span>
                                {" " + inputData.fromDate + " "+inputData.toDate}
                                <span> {/*onClick={() => getWeekDate("+1")}*/}
                                    <FontAwesomeIcon
                                        style={{ cursor: "pointer" }}
                                        className="arrow_Icons"
                                        icon={faChevronRight}
                                    />
                                </span>
                            </Col>
                        </div>
                        ):null}
                    </div>
                    {inputData.traningPerformance !=0 ?(
                    <Chart
                        width={'100%'}
                        height={'400px'}
                        chartType="LineChart"
                        loader={<div>Loading Chart</div>}
                        // numberPrefix="$"
                        borderRadius="10"
                        data={inputData.traningPerformance}
                        options={{
                            colors: ['#0094C0', '#FD4952', '#90B3C7'],
                            legend: { position: "none" , maxLines: 3 },

                            hAxis: {
                                title: 'Dates',
                                minValue: 0,
                                format: 'hh:mm',
                                minorGridlines: {
                                    count: 0
                                },
                                minValue:0,
                                viewWindow: {
                                    min: 0
                                },
                                viewWindowMode: "explicit",
                                gridlines: {
                                    color: 'gray',
                                    count: 0,

                                }, textStyle: {
                                   
                                    color: 'gray',
                                    textalgin: "left",
                                    borderRadius: 10,
                                },
                            },

                            vAxis: {
                                minorGridlines: {
                                    count: 0
                                },
                                format: 'Hrs ',
                                title: 'Hours',
                                gridlines: {
                                    color: 'green',
                                    count: 0
                                },
                                minValue:0,
                               
                                textStyle: {
                                    color: 'gray',
                                    fontWeight: "bold",
                                    textalgin: "left",
                                    borderRadius: 10,
                                    backgroundColor: '#b7b1b121',
                                },

                            },
                           
                            chartArea: {
                                // "numberPrefix": "$",
                                backgroundColor: {
                                    gradient: {
                                        // Start color for gradient.
                                        color1: '#d79db8',
                                        // Finish color for gradient.
                                        color2: 'white',
                                        // Where on the boundary to start and
                                        // end the color1/color2 gradient,
                                        // relative to the upper left corner
                                        // of the boundary.
                                        x2: '5%', y2: '5%',
                                        x1: '0%', y1: '100%',

                                        // If true, the boundary for x1,
                                        // y1, x2, and y2 is the box. If
                                        // false, it's the entire chart.
                                        useObjectBoundingBoxUnits: true
                                    },
                                },
                            },
                          
                            series: {
                                0: { curveType: 'function', visibleInLegend : true,
                                pointShape: 'circle',
                                pointSize: 10, },
                            },
                        }}
                        rootProps={{ 'data-testid': '1' }}
                    />
                    ):null}
                </div>
            </Col>
        </>:null}
        </>

    )



}

export default PermissionProvider( TraningPerformance);