import {
  faChevronLeft,
  faChevronRight,
  faStopCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { forwardRef, useEffect, useState } from "react";
import { Col, Row, Button, Modal } from "react-bootstrap";
import { PlayCircleFilled } from "@ant-design/icons";
import moment from "moment";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import { Permissions as Permission } from "../../../permissions/AppPermissions";
import PermissionProvider from "../../../permissions/PermissionProvider";
import { withRouter } from "react-router";
import { connect } from "react-redux";

/// Monthly schedules
export const Month_Schedules = forwardRef((props, ref) => {

  //
  // Reading user's permissions from PermissionProvider component
  //
  let permissions = props.getAppPermissions();

  const [scheduleProgreamModel, setScheduleProgreamModel] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [schedulesDetials, setSchedulesDetials] = useState([]);

  ///default current date geted
  const [inputData, setInputData] = useState({
    subHeader: moment(new Date()).format("DD MMMM YYYY - dddd").toString(),
    header: moment(new Date()).format("MMM YYYY").toString(),
    months: moment(new Date()).format("MMM YYYY").toString(),
    date: moment(new Date()).format("DD-MMM-YYYY").toString(),
    pop: 0,
    flag: 0,
    uiFlag: 0,
    permissions: permissions,
  });
  const [masterDatas, setMasterData] = useState({
    ProgramID: 0,
  });

  /// program change function
  useEffect(() => {
    
    if (masterDatas.ProgramID !== undefined && masterDatas.ProgramID != null) {
      if (props != undefined && props != null) {
        props.changeProgramsFun(masterDatas.ProgramID);
      }
    }
  }, [masterDatas.ProgramID]);

  /// schedule details view pop up function
  const scheduleViewFunction = async (model) => {
    let date = "";
    if (model.Date != undefined && model.Date != null) {
      date = moment(new Date(model.Date)).format("DD MMM YYYY").toString();
    }
    let data = "";
    if (model != undefined && model != null) {
      let datas = Array.prototype.map
        .call(
          model.WorkOutModel,
          (s) => s.WorkOut + " " + s.Sets + " * " + s.Reps
        )
        .toString();
      data = datas;
    }
    model["Date"] = date;
    model["workoutDtl"] = data;
    setSchedulesDetials(model);
    setShowModal(true);
  };

  /// WorkoutAttendance start function
  const clickWorkoutStart = async (model) => {
    let InputData = {
      TrainingProgramID:
        model.ProgramID.toString() == "" ? "0" : model.ProgramID.toString(),
      TrainingScheduleID:
        model.ScheduleID.toString() == "" ? "0" : model.ScheduleID.toString(),
      ScheduleTimeID:
        model.ScheduleTimeID.toString() == ""
          ? "0"
          : model.ScheduleTimeID.toString(),
      WorkoutAttendanceID: 0, // model.WorkoutAttendanceID.toString()==""?"0":model.WorkoutAttendanceID.toString(),
      WorkoutDate: model.Date,
    };
    await APIRequest.getPostService(APIUrl.CREATE_WORKOUT_ATTENDANCE, InputData)
      .then((response) => {
        if (response.Data == true) {
          let date = moment(new Date(inputData.date))
            .format("DD-MMM-YYYY")
            .toString();
          let dddd = getDateChange(date);
        }
      })
      .catch((error) => {});
  };

  /// WorkoutAttendance Stop function
  const clickWorkoutStop = async (model) => {
    let data = await APIRequest.getGetService(
      APIUrl.UPDATE_WORKOUT_ATTENDANCE_TIME + "/" + model.WorkoutAttendanceID
    );

    if (data.Data == true) {
      let date = moment(new Date(inputData.date))
        .format("DD-MMM-YYYY")
        .toString();
      await getDateChange(date);
    }
  };
  /// date change to monthly schedule master data geted
  const getDateChange = async (date) => {
    date = moment(new Date(date)).format("DD MMM YYYY").toString();
    await APIRequest.getGetService(
      APIUrl.GET_DAY_SCHEDULE_MASTER_DATA + "/" + date
    )
      .then((response) => {
        let schedule_Day_Data = [];
        if (response.Data.ScheduleProgreamModel != null) {
          schedule_Day_Data = response.Data.ScheduleProgreamModel;
          if (schedule_Day_Data != null && schedule_Day_Data.length > 0) {
            let date = schedule_Day_Data[0].Date;
            setInputData((prev) => ({ ...prev, listDate: date }));
            setMasterData((prev) => ({
              ...prev,
              ProgramID: schedule_Day_Data[0].ProgramID,
            }));
          } else {
            setMasterData((prev) => ({ ...prev, ProgramID: 0 }));
          }
        } else {
          setMasterData((prev) => ({ ...prev, ProgramID: 0 }));
        }
        setScheduleProgreamModel(schedule_Day_Data);
      })
      .catch((error) => {});
  };
  //// 30 seconds once called (Month day schedule master data) for  current date only
  useEffect(() => {
    const timer = setInterval(() => {
      let currantDate = moment(new Date()).format("DD-MMM-YYYY").toString();
      let date = moment(new Date(inputData.date))
        .format("DD-MMM-YYYY")
        .toString();
      if (currantDate == date) {
        getDateChange(date);
      }
    }, 30000);
    // clearing interval
    return () => clearInterval(timer);
  });
  /// date change function
  useEffect(async () => {
    async function fetchMyAPI() {
      await getDateChange(inputData.date);
    }

    fetchMyAPI();
  }, [inputData.date]);

  //dymanic color change function
  const removeMonthDayColor = async () => {
    var toggles = document.getElementsByClassName("schedule_month_day_btn");
    let toggleData = toggles.length;
    for (var i = 0; i < toggleData; i++) {
      var myElementID = document.getElementById("schedule_month_day_btn" + i);

      if (myElementID !== null) {
        document
          .getElementById("schedule_month_day_btn" + i)
          .classList.remove("schedule-month-day-active-btn");
        document
          .getElementById("schedule_month_day_btn" + i)
          .classList.add("schedule-month-day-btn");
      }
    }
  };
  /// month day color change function
  const updateMonthDay = (index, subDate, flag, daylist, datess) => {
    let dates = moment(new Date(datess)).format("DD-MMM-YYYY").toString();

    if (flag === 1) {
      setInputData((prev) => ({
        ...prev,
        subHeader: subDate,
        uiFlag: 1,
        date: dates,
      }));
      setScheduleProgreamModel([]);
    }
    var toggles = document.getElementsByClassName("schedule_month_day_btn");
    let toggleData = toggles.length;
    for (var i = 0; i < toggleData; i++) {
      var myElementID = document.getElementById("schedule_month_day_btn" + i);
      if (i === index) {
        if (myElementID !== null) {
          document
            .getElementById("schedule_month_day_btn" + i)
            .classList.add("schedule-month-day-active-btn");
          document
            .getElementById("schedule_month_day_btn" + i)
            .classList.remove("schedule-month-day-btn");
        }
      } else {
        if (myElementID !== null) {
          document
            .getElementById("schedule_month_day_btn" + i)
            .classList.remove("schedule-month-day-active-btn");
          document
            .getElementById("schedule_month_day_btn" + i)
            .classList.add("schedule-month-day-btn");
        }
      }
    }
    daylist[index].flag = 1;
    setMonthDayData(daylist);
  };

  /// select month full days geted
  const monthFullDays = (val) => {
    let dates = moment(inputData.months)
      .format("MM,YYYY")
      .toString()
      .split(",");
    let monthIndex = dates[0] - 1; // 0..11 instead of 1..12
    let date = new Date(dates[1], monthIndex, 1);
    let result = [];
    let currentdates = moment(new Date()).format("DD-MM-YYYY").toString();
    let count = 0;
    let pop = 0;
    while (date.getMonth() == monthIndex) {
      var dateList = {
        header: moment(new Date(date)).format("DD MMM ddd").toString(),
        date: moment(new Date(date)).format("DD-MMM-YY").toString(),
        year: moment(new Date(date)).format("YYYY").toString(),
        month: moment(new Date(date)).format(" MMMM ").toString(),
        weekday: moment(new Date(date)).format(" dddd ").toString(),
        DateMonthDayDtl: moment(new Date(date))
          .format("DD MMMM YYYY - dddd")
          .toString(),
        DayMonthyear: moment(new Date(date)).format("DD-MMM-YYYY").toString(),
        flag: 1,
      };
      let currentdatess = moment(new Date(date))
        .format("DD-MM-YYYY")
        .toString();
      if (currentdates === currentdatess) {
        pop = count;
      }
      result.push(dateList);
      date.setDate(date.getDate() + 1);
      count += 1;
    }
    setMonthDayData(result);
    let flagPosision = 0;
    if (val == 0) {
      flagPosision = 1;
    }
    setInputData((prev) => ({
      ...prev,
      flag: flagPosision,
      pop: pop,
      uiFlag: 0,
    }));

    return result;
  };

  /// month change function
  useEffect(() => {
    monthFullDays(0);
  }, [inputData.months]);
  /// default called month full day geted function
  useEffect(() => {
    monthFullDays(0);
  }, []);

  /// month day color change function
  useEffect(() => {
    if (inputData.flag == 1) {
      updateMonthDay(inputData.pop, "", 0, monthDayData, "");
      setInputData((prev) => ({ ...prev, uiFlag: 1 }));
    }
  }, [inputData.flag]);

  const [monthDayData, setMonthDayData] = useState([]);

  //// month change function

  const monthChange = async (val) => {
    await removeMonthDayColor();
    let data = "";
    if (val === "-1") {
      data = moment(inputData.months)
        .subtract(1, "months")
        .format("MMM YYYY")
        .toString();
    }
    if (val === "+1") {
      data = moment(inputData.months)
        .add(1, "months")
        .format("MMM YYYY")
        .toString();
    }
    setInputData((prev) => ({
      ...prev,
      header: data,
      months: data,
    }));
  };

  return (
    <>
      <Row className="pl-5  pt-1">
        <Col className="col font-weight-bold">
          <span onClick={() => monthChange("-1")}>
            {" "}
            <FontAwesomeIcon
              style={{ cursor: "pointer" }}
              icon={faChevronLeft}
              className="sm arrow_Icons"
            />
            {"  "}
          </span>{" "}
          {"   " + inputData.header + "   "}{" "}
          <span onClick={() => monthChange("+1")}>
            {"  "}{" "}
            <FontAwesomeIcon
              style={{ cursor: "pointer" }}
              className="arrow_Icons"
              icon={faChevronRight}
            />
          </span>
        </Col>
      </Row>
      <Row className="col-sm-12 form-group pl-5 pr-0">
        {monthDayData.map((x, index) => {
          return (
            <>
              <Col
                sm="auto"
                xs="auto"
                md="auto"
                lg="auto"
                xl="auto"
                className="px-1"
              >
                <button
                  id={"schedule_month_day_btn" + index}
                  className="btn schedule_month_day_btn schedule-month-day-btn"
                  onClick={() => {
                    removeMonthDayColor();
                    updateMonthDay(
                      index,
                      x.DateMonthDayDtl,
                      1,
                      monthDayData,
                      x.DayMonthyear
                    );
                  }}
                >
                  {x.header}
                </button>
              </Col>
            </>
          );
        })}
      </Row>
      {inputData.uiFlag == 1 ? (
        <div>
          <Row className="pl-5 form-group pt-1">
            <Col className=" font-weight-bold">
              {" "}
              {" " + inputData.subHeader + " "}{" "}
            </Col>
          </Row>

          {scheduleProgreamModel.map((x) => {
            let flagclass = "";

            if (x.Flag == 2) {
              flagclass = "schedule-day-box-runing-color";
            } else if (x.Flag == 3) {
              flagclass = "schedule-day-box-stop-color";
            } else {
              flagclass = "schedule-day-box-default-color";
            }

            return (
              <>
                <Row
                  className="row form-group  "
                  onClick={() => scheduleViewFunction(x)}
                >
                  <Col md="1" sm="1" lg="1" xl="1"></Col>
                  <Col
                    md="10"
                    sm="10"
                    lg="10"
                    xl="10"
                    className={" mx-4 schedule-day-box " + flagclass}
                  >
                    <label className="p-2">
                      {x.Name} {x.Time}
                    </label>
                    <Row className="form-group">
                      {x.WorkOutModel.map((y) => {
                        return (
                          <Col sm="auto ">
                            <label className="schedule-day-subbox ">
                              {y.WorkOut} {y.Sets} * {y.Reps}{" "}
                            </label>
                          </Col>
                        );
                      })}
                    </Row>
                    <Row className="d-flex justify-content-center form-group">
                      {x.Flag == 1 &&
                      inputData.permissions.includes(Permission.M_S_START) ? (
                        <div className="col-sm-12 text-center form-group">
                          <div>
                            <PlayCircleFilled
                              onClick={() => clickWorkoutStart(x)}
                              className="schedule-click-icon"
                            />
                          </div>

                          <div className="col-sm-12 text-center">
                            <div className="schedule-click-text">
                              Click here{" "}
                            </div>
                          </div>
                        </div>
                      ) : x.Flag == 2 &&
                        inputData.permissions.includes(Permission.M_S_STOP) ? (
                        <div className="col-sm-12 text-center form-group">
                          <div>
                            <span className="schedule-running-icon">
                              <FontAwesomeIcon
                                onClick={() => clickWorkoutStop(x)}
                                icon={faStopCircle}
                                className="schedule-running-icon-prop"
                                size="lg"
                              ></FontAwesomeIcon>
                            </span>{" "}
                          </div>

                          <div className=" text-center">
                            <div className="schedule-running-text">
                              Stop Your Schedule{" "}
                            </div>
                          </div>
                        </div>
                      ) : x.Flag == 3 ? (
                        <label className="schedule-stop-text">
                          {x.TotalTime}
                        </label>
                      ) : null}
                    </Row>
                  </Col>
                  <Col md="1" sm="1" lg="1" xl="1"></Col>
                </Row>
              </>
            );
          })}
        </div>
      ) : null}

      {schedulesDetials != null ? (
        <Modal
          backdrop="static"
          size="md"
          //className='popupborder'
          keyboard={true}
          show={showModal}
          onHide={() => setShowModal(false)}
          aria-labelledby="contained-modal-title-vcenter"
          //centered
        >
          <Modal.Header className="">
            <div className="col-sm-12">
              <div className="row">
                <div className="col-sm-11">
                  <div className="d-flex justify-content-center">
                    <h5 className="font-weight-bold text-white mb-0">
                      {" "}
                      Schedule View
                    </h5>
                  </div>
                </div>
                <div className="col-sm-1">
                  <div className="d-flex justify-content-end">
                    <button
                      type="button"
                      className="close text-white"
                      onClick={() => setShowModal(false)}
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>{" "}
                  </div>
                </div>
              </div>
            </div>
          </Modal.Header>
          <Modal.Body className="">
            <div className="row ">
              <div className="col-sm-5">
                {" "}
                <label className="font-weight-bold popup_txt">
                  Program Name
                </label>{" "}
              </div>
              <div className="col-sm-7">
                {" "}
                <label>{schedulesDetials.ProgramName}</label>{" "}
              </div>
              <div className="col-sm-5">
                {" "}
                <label className="font-weight-bold popup_txt">
                  Schedule Name
                </label>{" "}
              </div>
              <div className="col-sm-7">
                {" "}
                <label>{schedulesDetials.Name}</label>{" "}
              </div>
              <div className="col-sm-5">
                {" "}
                <label className="font-weight-bold popup_txt">
                  Workout
                </label>{" "}
              </div>
              <div className="col-sm-7">
                {" "}
                <label>{schedulesDetials.workoutDtl}</label>
              </div>
              <div className="col-sm-5">
                {" "}
                <label className="font-weight-bold popup_txt">Date </label>{" "}
              </div>
              <div className="col-sm-7">
                {" "}
                <label>{schedulesDetials.Date}</label>{" "}
              </div>
              <div className="col-sm-5">
                {" "}
                <label className="font-weight-bold popup_txt">
                  Schedule Time
                </label>{" "}
              </div>
              <div className="col-sm-7">
                {" "}
                <label>{schedulesDetials.Time}</label>{" "}
              </div>
              <div className="col-sm-5">
                {" "}
                <label className="font-weight-bold popup_txt">
                  Start Time
                </label>{" "}
              </div>
              <div className="col-sm-7">
                {" "}
                <label>{schedulesDetials.StartTime}</label>{" "}
              </div>
              <div className="col-sm-5">
                {" "}
                <label className="font-weight-bold popup_txt">
                  End Time
                </label>{" "}
              </div>
              <div className="col-sm-7">
                {" "}
                <label>{schedulesDetials.EndTime}</label>{" "}
              </div>
              <div className="col-sm-5">
                {" "}
                <label className="font-weight-bold popup_txt">
                  Total Time
                </label>{" "}
              </div>
              <div className="col-sm-7">
                {" "}
                <label>{schedulesDetials.TotalTime}</label>{" "}
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer className="m-0 py-0 ">
            <div className="col-sm-12">
              <div className="row  d-flex justify-content-center">
                <Button
                  className="btn_green  btn"
                  onClick={() => (setShowModal(false), setSchedulesDetials([]))}
                >
                  Close
                </Button>
              </div>
            </div>
          </Modal.Footer>
        </Modal>
      ) : null}
    </>
  );
});

export default PermissionProvider(Month_Schedules);

// const mapPropsToState = (state) => ({
//   user: state.userReducer,
// });
// export default  connect(mapPropsToState, null)(withRouter(Month_Schedules));
