import { PlayCircleFilled } from "@ant-design/icons";
import {
  faChevronLeft,
  faChevronRight,
  faStopCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment";
import { forwardRef, useEffect, useState } from "react";
import { Col, Modal, Row, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import { Permissions as Permission } from "../../../permissions/AppPermissions";
import PermissionProvider from "../../../permissions/PermissionProvider";

export const DaySchedules = forwardRef((props, ref) => {

  //
  // Reading user's permissions from PermissionProvider component
  //
  let permissions = props.Props.getAppPermissions();

  const defualtDate = new Date().toString().split(" ");

  const [showModal, setShowModal] = useState(false);

  /// default curent date
  let defualtDates =
    defualtDate[2] + "-" + defualtDate[1] + "-" + defualtDate[3];

  const [scheduleProgreamModel, setScheduleProgreamModel] = useState([]); //temdata

  const [inputData, setInputData] = useState({
    subHeader: defualtDates,
    listDate: "",
    permissions: permissions,
  });

  const [schedulesDetials, setSchedulesDetials] = useState([]);

  /// Date change function
  useEffect(async () => {
    async function fetchMyAPI() {
      await getDateChange(inputData.subHeader);
    }

    fetchMyAPI();
  }, [inputData.subHeader]);

  //// Previous Date Click function
  const getPreviousDate = () => {
    let datess = "";
    if (inputData.subHeader === "" || inputData.subHeader === undefined) {
      datess = moment(new Date(), "DD MMM YYYY");
    } else {
      datess = moment(inputData.subHeader, "DD MMM YYYY");
    }

    const currentDayInMilli = new Date(datess).getTime();
    const oneDay = 1000 * 60 * 60 * 24;
    const previousDayInMilli = currentDayInMilli - oneDay;
    const previousDates = new Date(previousDayInMilli).toString().split(" ");

    let previousDate =
      previousDates[2] + "-" + previousDates[1] + "-" + previousDates[3];
    setScheduleProgreamModel([]);
    setInputData((prev) => ({
      ...prev,
      subHeader: previousDate,
    }));
  };
  const [masterDatas, setMasterData] = useState({
    ProgramID: 0,
  });

  //// program change function
  useEffect(() => {
    if (masterDatas.ProgramID !== undefined && masterDatas.ProgramID != null) {
      if (props != undefined && props != null) {
        props.changeProgramsFun(masterDatas.ProgramID);
      }
    }
  }, [masterDatas.ProgramID]);

  //// 30 seconds once called (day schedule master data) for  current date only
  useEffect(() => {
    const timer = setInterval(() => {
      let currantDate = moment(new Date()).format("DD-MMM-YYYY").toString();
      if (currantDate == inputData.subHeader) {
        getDateChange(inputData.subHeader);
      }
    }, 30000);
    // clearing interval
    return () => clearInterval(timer);
  });

  /// date change to day schedule master data geted
  const getDateChange = async (date) => {
    date = moment(new Date(date)).format("DD MMM YYYY").toString();
    await APIRequest.getGetService(
      APIUrl.GET_DAY_SCHEDULE_MASTER_DATA + "/" + date
    )
      .then((response) => {
        let schedule_Day_Data = [];
        if (response.Data.ScheduleProgreamModel != null) {
          schedule_Day_Data = response.Data.ScheduleProgreamModel;
          if (schedule_Day_Data != null && schedule_Day_Data.length > 0) {
            let date = schedule_Day_Data[0].Date;
            setInputData((prev) => ({ ...prev, listDate: date }));
            setMasterData((prev) => ({
              ...prev,
              ProgramID: schedule_Day_Data[0].ProgramID,
            }));
          } else {
            setMasterData((prev) => ({ ...prev, ProgramID: 0 }));
          }
        } else {
          setMasterData((prev) => ({ ...prev, ProgramID: 0 }));
        }
        setScheduleProgreamModel(schedule_Day_Data);
      })
      .catch((error) => {});
  };
  /// schedule details view pop up function
  const scheduleViewFunction = async (model) => {
    let date = "";
    if (model.Date != undefined && model.Date != null) {
      date = moment(new Date(model.Date)).format("DD MMM YYYY").toString();
    }
    let data = "";
    if (model != undefined && model != null) {
      let datas = Array.prototype.map
        .call(
          model.WorkOutModel,
          (s) => s.WorkOut + " " + s.Sets + " * " + s.Reps
        )
        .toString();
      data = datas;
    }
    model["Date"] = date;
    model["workoutDtl"] = data;
    setSchedulesDetials(model);
    setShowModal(true);
  };

  /// WorkoutAttendance start function
  const clickWorkoutStart = async (model) => {
    let InputData = {
      TrainingProgramID:
        model.ProgramID.toString() == "" ? "0" : model.ProgramID.toString(),
      TrainingScheduleID:
        model.ScheduleID.toString() == "" ? "0" : model.ScheduleID.toString(),
      ScheduleTimeID:
        model.ScheduleTimeID.toString() == ""
          ? "0"
          : model.ScheduleTimeID.toString(),
      WorkoutAttendanceID: 0, // model.WorkoutAttendanceID.toString()==""?"0":model.WorkoutAttendanceID.toString(),
      WorkoutDate: model.Date,
    };
    await APIRequest.getPostService(APIUrl.CREATE_WORKOUT_ATTENDANCE, InputData)
      .then((response) => {
        if (response.Data == true) {
          getDateChange(inputData.subHeader);
        }
      })
      .catch((error) => {});
  };
  /// WorkoutAttendance Stop function
  const clickWorkoutStop = async (model) => {
    let data = await APIRequest.getGetService(
      APIUrl.UPDATE_WORKOUT_ATTENDANCE_TIME + "/" + model.WorkoutAttendanceID
    );

    if (data.Data == true) {
      await getDateChange(inputData.subHeader);
    }
  };

  /// next date change function
  const getNextDate = () => {
    let datess = "";
    if (inputData.subHeader === "" || inputData.subHeader === undefined) {
      datess = moment(new Date(), "DD MMM YYYY");
    } else {
      datess = moment(inputData.subHeader, "DD MMM YYYY");
    }

    const currentDayInMilli = new Date(datess).getTime();
    const oneDay = 1000 * 60 * 60 * 24;
    const nextDayInMilli = currentDayInMilli + oneDay;
    const nextDates = new Date(nextDayInMilli).toString().split(" ");

    let nextDate = nextDates[2] + "-" + nextDates[1] + "-" + nextDates[3];
    setScheduleProgreamModel([]);
    setInputData((prev) => ({
      ...prev,
      subHeader: nextDate,
    }));
  };
  return (
    <>
      <Row className="pl-3 form-group  pt-1">
        <Col className="font-weight-bold">
          Day :
          <span>
            {" "}
            <FontAwesomeIcon
              onClick={() => getPreviousDate()}
              style={{ cursor: "pointer" }}
              icon={faChevronLeft}
              className="sm arrow_Icons"
            />
          </span>{" "}
          {inputData.subHeader}{" "}
          <span>
            {" "}
            <FontAwesomeIcon
              onClick={() => getNextDate()}
              style={{ cursor: "pointer" }}
              className="arrow_Icons"
              icon={faChevronRight}
            />
          </span>
        </Col>
      </Row>

      {scheduleProgreamModel.map((x) => {
        let flagclass = "";

        if (x.Flag == 2) {
          flagclass = "schedule-day-box-runing-color";
        } else if (x.Flag == 3) {
          flagclass = "schedule-day-box-stop-color";
        } else {
          flagclass = "schedule-day-box-default-color";
        }

        return (
          <>
            <Row
              className="row form-group  "
              onClick={() => scheduleViewFunction(x)}
            >
              <Col md="1" sm="1" lg="1" xl="1"></Col>
              <Col
                md="10"
                sm="10"
                lg="10"
                xl="10"
                className={" mx-4 schedule-day-box " + flagclass}
              >
                <label className="p-2">
                  {x.Name} {x.Time}
                </label>
                <Row className="form-group">
                  {x.WorkOutModel.map((y) => {
                    return (
                      <Col sm="auto">
                        <label className="schedule-day-subbox">
                          {y.WorkOut} {y.Sets} * {y.Reps}{" "}
                        </label>
                      </Col>
                    );
                  })}
                </Row>
                <Row className="d-flex justify-content-center form-group">
                  {x.Flag == 1 &&
                  inputData.permissions.includes(Permission.M_S_START) ? (
                    <div className="col-sm-12 text-center form-group">
                      <div>
                        <PlayCircleFilled
                          onClick={() => clickWorkoutStart(x)}
                          className="schedule-click-icon"
                        />
                      </div>

                      <div className="col-sm-12 text-center">
                        <div className="schedule-click-text">Click here </div>
                      </div>
                    </div>
                  ) : x.Flag == 2 &&
                    inputData.permissions.includes(Permission.M_S_STOP) ? (
                    <div className="col-sm-12 text-center form-group">
                      <div>
                        <span className="schedule-running-icon">
                          <FontAwesomeIcon
                            onClick={() => clickWorkoutStop(x)}
                            icon={faStopCircle}
                            className="schedule-running-icon-prop"
                            size="lg"
                          ></FontAwesomeIcon>
                        </span>{" "}
                      </div>

                      <div className=" text-center">
                        <div className="schedule-running-text">
                          Stop Your Schedule{" "}
                        </div>
                      </div>
                    </div>
                  ) : x.Flag == 3 ? (
                    <label className="schedule-stop-text">{x.TotalTime}</label>
                  ) : null}
                </Row>
              </Col>
              <Col md="1" sm="1" lg="1" xl="1"></Col>
            </Row>
          </>
        );
      })}

      <Modal
        backdrop="static"
        size="md"
        keyboard={true}
        show={showModal}
        onHide={() => setShowModal(false)}
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header className="">
          <div className="col-sm-12">
            <div className="row">
              <div className="col-sm-11">
                <div className="d-flex justify-content-center">
                  <h5 className="font-weight-bold text-white mb-0">
                    {" "}
                    Schedule View
                  </h5>
                </div>
              </div>
              <div className="col-sm-1">
                <div className="d-flex justify-content-end">
                  <button
                    type="button"
                    className="close text-white"
                    onClick={() => setShowModal(false)}
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>{" "}
                </div>
              </div>
            </div>
          </div>
        </Modal.Header>
        <Modal.Body className="">
          <div className="row ">
            <div className="col-sm-5">
              {" "}
              <label className="font-weight-bold popup_txt">
                Program Name
              </label>{" "}
            </div>
            <div className="col-sm-7">
              {" "}
              <label>{schedulesDetials.ProgramName}</label>{" "}
            </div>
            <div className="col-sm-5">
              {" "}
              <label className="font-weight-bold popup_txt">
                Schedule Name
              </label>{" "}
            </div>
            <div className="col-sm-7">
              {" "}
              <label>{schedulesDetials.Name}</label>{" "}
            </div>
            <div className="col-sm-5">
              {" "}
              <label className="font-weight-bold popup_txt">Workout</label>{" "}
            </div>
            <div className="col-sm-7">
              {" "}
              <label>{schedulesDetials.workoutDtl}</label>{" "}
            </div>
            <div className="col-sm-5">
              {" "}
              <label className="font-weight-bold popup_txt">Date </label>{" "}
            </div>
            <div className="col-sm-7">
              {" "}
              <label>{schedulesDetials.Date}</label>{" "}
            </div>
            <div className="col-sm-5">
              {" "}
              <label className="font-weight-bold popup_txt">
                Schedule Time
              </label>{" "}
            </div>
            <div className="col-sm-7">
              {" "}
              <label>{schedulesDetials.Time}</label>{" "}
            </div>
            <div className="col-sm-5">
              {" "}
              <label className="font-weight-bold popup_txt">
                Start Time
              </label>{" "}
            </div>
            <div className="col-sm-7">
              {" "}
              <label>{schedulesDetials.StartTime}</label>{" "}
            </div>
            <div className="col-sm-5">
              {" "}
              <label className="font-weight-bold popup_txt">
                End Time
              </label>{" "}
            </div>
            <div className="col-sm-7">
              {" "}
              <label>{schedulesDetials.EndTime}</label>{" "}
            </div>
            <div className="col-sm-5">
              {" "}
              <label className="font-weight-bold popup_txt">
                Total Time
              </label>{" "}
            </div>
            <div className="col-sm-7">
              {" "}
              <label>{schedulesDetials.TotalTime}</label>{" "}
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer className="m-0 py-0 ">
          <div className="col-sm-12">
            <div className="row  d-flex justify-content-center">
              <Button
                className="btn_green  btn"
                onClick={() => setShowModal(false)}
              >
                Close
              </Button>
            </div>
          </div>
        </Modal.Footer>
      </Modal>
    </>
  );
});

//export default PermissionProvider(DaySchedules);

const mapPropsToState = (state) => ({
  user: state.userReducer,
});
export default PermissionProvider(connect(
  mapPropsToState
)(withRouter(DaySchedules)));

