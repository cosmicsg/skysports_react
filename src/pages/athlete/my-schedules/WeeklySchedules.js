import { PlayCircleFilled } from "@ant-design/icons";
import {
  faChevronLeft,
  faChevronRight,
  faStopCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { forwardRef, useEffect, useRef, useState } from "react";
import { Col, Row, Modal, Button } from "react-bootstrap";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import moment from "moment";
import { Permissions as Permission } from "../../../permissions/AppPermissions";
import PermissionProvider from "../../../permissions/PermissionProvider";
import { withRouter } from "react-router";
import { connect } from "react-redux";

///weekly schedules
export const WeeklySchedules = forwardRef((props, ref) => {
  
  //
  // Reading user's permissions from PermissionProvider component
  //
  let permissions = props.Props.getAppPermissions();

  const [listData, setListData] = useState({
    week_contents: [],
  });
  const [showModal, setShowModal] = useState(false);
  const [schedulesDetials, setSchedulesDetials] = useState([]);
  const start_Week = moment(new Date())
    .add(1, "days")
    .startOf("isoWeek")
    .toDate()
    .toString()
    .split(" ");
  const end_Week = moment(new Date())
    .add(1, "days")
    .endOf("isoWeek")
    .toDate()
    .toString()
    .split(" ");
  const [inputData, setInputtData] = useState({
    displayHeader_Date:
      start_Week[2] +
      "-" +
      start_Week[1] +
      "-" +
      start_Week[3] +
      " To " +
      end_Week[2] +
      "-" +
      end_Week[1] +
      "-" +
      end_Week[3],
    toDate: end_Week[2] + "-" + end_Week[1] + "-" + end_Week[3],
    fromDate: start_Week[2] + "-" + start_Week[1] + "-" + start_Week[3],
    permissions: permissions,
  });
  // date change function
  useEffect(async () => {
    await getWeekDateChange();
  }, [inputData.toDate, inputData.fromDate]);

  /// week date change function
  const getWeekDateChange = async () => {
    await APIRequest.getGetService(
      APIUrl.GET_WEEK_SCHEDULE_MASTER_DATA +
        "/" +
        inputData.fromDate +
        "/" +
        inputData.toDate
    )
      .then((response) => {
        let schedule_Day_Data = [];
        if (response.Data != null) {
          schedule_Day_Data = response.Data;
          if (schedule_Day_Data != null && schedule_Day_Data.length > 0) {
            // setMasterData((prev)=>({...prev,ProgramID:schedule_Day_Data[0].ProgramID}))
            setMasterData((prev) => ({ ...prev, ProgramID: 0 }));
          }
        } else {
          setMasterData((prev) => ({ ...prev, ProgramID: 0 }));
        }
        setListData((prev) => ({ ...prev, week_contents: schedule_Day_Data }));
      })
      .catch((error) => {});
  };
  const [masterDatas, setMasterData] = useState({
    ProgramID: 0,
  });
  // week range change function and gentrate new week
  const getWeekDate = (val) => {
    let startOfWeek = "";
    let endOfWeek = "";
    let fromDate = "";
    let toDate = "";

    if (val == "+1") {
      startOfWeek = moment(inputData.toDate)
        .add(1, "days")
        .startOf("isoWeek")
        .toDate()
        .toString()
        .split(" ");
      endOfWeek = moment(inputData.toDate)
        .add(1, "days")
        .endOf("isoWeek")
        .toDate()
        .toString()
        .split(" ");
      toDate = endOfWeek[2] + "-" + endOfWeek[1] + "-" + endOfWeek[3];
      fromDate = startOfWeek[2] + "-" + startOfWeek[1] + "-" + startOfWeek[3];
    }
    if (val == "-1") {
      startOfWeek = moment(inputData.fromDate)
        .subtract(1, "days")
        .startOf("isoWeek")
        .toDate()
        .toString()
        .split(" ");
      endOfWeek = moment(inputData.fromDate)
        .subtract(1, "days")
        .endOf("isoWeek")
        .toDate()
        .toString()
        .split(" ");
      toDate = endOfWeek[2] + "-" + endOfWeek[1] + "-" + endOfWeek[3];
      fromDate = startOfWeek[2] + "-" + startOfWeek[1] + "-" + startOfWeek[3];
    }

    let rangeDates =
      startOfWeek[2] +
      " " +
      startOfWeek[1] +
      " " +
      startOfWeek[3] +
      " To " +
      endOfWeek[2] +
      " " +
      endOfWeek[1] +
      " " +
      endOfWeek[3];
    setInputtData((prev) => ({
      ...prev,
      displayHeader_Date: rangeDates,
      toDate: toDate,
      fromDate: fromDate,
    }));
  };

  /// WorkoutAttendance start function
  const clickWorkoutStart = async (model) => {
    let InputData = {
      TrainingProgramID:
        model.ProgramID.toString() == "" ? "0" : model.ProgramID.toString(),
      TrainingScheduleID:
        model.ScheduleID.toString() == "" ? "0" : model.ScheduleID.toString(),
      ScheduleTimeID:
        model.ScheduleTimeID.toString() == ""
          ? "0"
          : model.ScheduleTimeID.toString(),
      WorkoutAttendanceID: 0,
      WorkoutDate: model.Date,
    };
    await APIRequest.getPostService(APIUrl.CREATE_WORKOUT_ATTENDANCE, InputData)
      .then((response) => {
        if (response.Data == true) {
          getCurrentDateChange();
        }
      })
      .catch((error) => {});
  };
  /// WorkoutAttendance Stop function
  const clickWorkoutStop = async (model) => {
    let data = await APIRequest.getGetService(
      APIUrl.UPDATE_WORKOUT_ATTENDANCE_TIME + "/" + model.WorkoutAttendanceID
    );

    if (data.Data == true) {
      await getCurrentDateChange();
    }
  };
  /// Current date data list geted
  const getCurrentDateChange = async () => {
    let schedule_Day_Data = [];
    let date = moment(new Date()).format("DD MMM YYYY").toString();
    await APIRequest.getGetService(
      APIUrl.GET_DAY_SCHEDULE_MASTER_DATA + "/" + date
    )
      .then((response) => {
        if (response.Data != null) {
          schedule_Day_Data = response.Data;
          let weekdatas = listData.week_contents;
          let currantDate = moment(new Date()).format("DD-MM-YYYY").toString();

          let removeDatas = weekdatas.filter((x) => x.Date != currantDate);
          removeDatas.push(response.Data);
          setListData((prev) => ({ ...prev, week_contents: removeDatas }));
        } else {
        }

        return schedule_Day_Data;
      })
      .catch((error) => {
        return schedule_Day_Data;
      });
    return schedule_Day_Data;
  };

  const initalState = 0;
  const [count, setCount] = useState(initalState);
  const counterRef = useRef(initalState);

  useEffect(() => {
    counterRef.current = count;
  });
  // timer set every 30 sec called
  useEffect(() => {
    let timer = setInterval(() => {
      setCount(counterRef.current + 1);
    }, 30000);
    return () => clearInterval(timer);
  }, []);

  //// 30 seconds once called (Week schedule current day range master data) for  current date only

  useEffect(async () => {
    let currantDate = moment(new Date()).format("DD-MM-YYYY").toString();
    let daata = listData.week_contents.filter((x) => x.Date == currantDate);

    if (daata.length > 0) {
      await getCurrentDateChange();
    }
  }, [count]);

  /// schedule details view pop up function
  const scheduleViewFunction = async (model) => {
    let date = "";
    if (model.Date != undefined && model.Date != null) {
      date = moment(new Date(model.Date)).format("DD MMM YYYY").toString();
    }
    let data = "";
    if (model != undefined && model != null) {
      let datas = Array.prototype.map
        .call(
          model.WorkOutModel,
          (s) => s.WorkOut + " " + s.Sets + " * " + s.Reps
        )
        .toString();
      data = datas;
    }
    model["Date"] = date;
    model["workoutDtl"] = data;
    setSchedulesDetials(model);
    setShowModal(true);
  };
  return (
    <>
      <Row className="pl-3 form-group pt-1">
        <Col className="font-weight-bold">
          <span onClick={() => getWeekDate("-1")}>
            <FontAwesomeIcon
              style={{ cursor: "pointer" }}
              icon={faChevronLeft}
              className="sm arrow_Icons"
            />
          </span>
          {" " + inputData.displayHeader_Date + " "}
          <span onClick={() => getWeekDate("+1")}>
            <FontAwesomeIcon
              style={{ cursor: "pointer" }}
              className="arrow_Icons"
              icon={faChevronRight}
            />
          </span>
        </Col>
      </Row>
      {listData.week_contents.length != 0 ? (
        <>
          <Row className="mx-2 seven-cols">
            <Col
              sm={12}
              xs={12}
              xl={1}
              lg={1}
              md={4}
              className="px-2 form-group"
            >
              <div className=" d-flex justify-content-center form-group">
                <label className="schedule-week-header d-flex justify-content-center font-weight-bold">
                  Monday
                </label>
              </div>
              {listData.week_contents.map((z) => {
                return (
                  <div>
                    {z.Day == "Monday" ? (
                      <>
                        {z.ScheduleProgreamModel != null ? (
                          <>
                            {z.ScheduleProgreamModel.map((x) => {
                              let flagclass = "";
                              if (x.Flag == 2) {
                                flagclass = "schedule-day-box-runing-color";
                              } else if (x.Flag == 3) {
                                flagclass = "schedule-week-box-stop-color";
                              } else {
                                flagclass = "schedule-day-box-default-color";
                              }
                              return (
                                <div
                                  className={
                                    "schedule-week-box form-group " + flagclass
                                  }
                                  onClick={() => scheduleViewFunction(x)}
                                >
                                  <label className="p-2 d-flex justify-content-center font-weight-bold">
                                    {x.Name} {x.Time}
                                  </label>
                                  <div className="col-sm-12 form-group">
                                    {x.WorkOutModel.map((y) => {
                                      return (
                                        <div className=" mb-2 schedule-week-subbox">
                                          {y.WorkOut +
                                            " " +
                                            y.Sets +
                                            " * " +
                                            y.Reps}
                                        </div>
                                      );
                                    })}
                                  </div>
                                  <div className="">
                                    {x.Flag == 1 &&
                                    inputData.permissions.includes(
                                      Permission.M_S_START
                                    ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div>
                                          <PlayCircleFilled
                                            onClick={() => clickWorkoutStart(x)}
                                            className="schedule-click-icon"
                                          />
                                        </div>

                                        <div className="col-sm-12 text-center">
                                          <div className="schedule-click-text">
                                            Click here{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 2 &&
                                      inputData.permissions.includes(
                                        Permission.M_S_STOP
                                      ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <span className="schedule-running-icon">
                                          <FontAwesomeIcon
                                            onClick={() => clickWorkoutStop(x)}
                                            icon={faStopCircle}
                                            className="schedule-running-icon-prop"
                                            size="lg"
                                          ></FontAwesomeIcon>
                                        </span>

                                        <div className=" text-center">
                                          <div className="schedule-running-text">
                                            Stop Your Schedule{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 3 ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div className=" text-center">
                                          <label className="schedule-stop-text">
                                            {x.TotalTime}
                                          </label>
                                        </div>
                                      </div>
                                    ) : null}
                                  </div>
                                </div>
                              );
                            })}
                          </>
                        ) : null}
                      </>
                    ) : null}
                  </div>
                );
              })}
            </Col>
            <Col
              sm={12}
              xs={12}
              xl={1}
              lg={1}
              md={4}
              className="px-2 form-group"
            >
              <div className=" d-flex justify-content-center form-group">
                <label className="schedule-week-header d-flex justify-content-center font-weight-bold">
                  Tuesday
                </label>
              </div>
              {listData.week_contents.map((z) => {
                return (
                  <div>
                    {z.Day == "Tuesday" ? (
                      <>
                        {z.ScheduleProgreamModel != null ? (
                          <>
                            {z.ScheduleProgreamModel.map((x) => {
                              let flagclass = "";
                              if (x.Flag == 2) {
                                flagclass = "schedule-day-box-runing-color";
                              } else if (x.Flag == 3) {
                                flagclass = "schedule-week-box-stop-color";
                              } else {
                                flagclass = "schedule-day-box-default-color";
                              }
                              return (
                                <div
                                  className={
                                    "schedule-week-box form-group " + flagclass
                                  }
                                  onClick={() => scheduleViewFunction(x)}
                                >
                                  <label className="p-2 d-flex justify-content-center font-weight-bold">
                                    {x.Name} {x.Time}
                                  </label>
                                  <div className="col-sm-12 form-group ">
                                    {x.WorkOutModel.map((y) => {
                                      return (
                                        <div className=" mb-2 schedule-week-subbox">
                                          {y.WorkOut +
                                            " " +
                                            y.Sets +
                                            " * " +
                                            y.Reps}
                                        </div>
                                      );
                                    })}
                                  </div>
                                  <div className="">
                                    {x.Flag == 1 &&
                                    inputData.permissions.includes(
                                      Permission.M_S_START
                                    ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div>
                                          <PlayCircleFilled
                                            onClick={() => clickWorkoutStart(x)}
                                            className="schedule-click-icon"
                                          />
                                        </div>

                                        <div className="col-sm-12 text-center">
                                          <div className="schedule-click-text">
                                            Click here{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 2 &&
                                      inputData.permissions.includes(
                                        Permission.M_S_STOP
                                      ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <span className="schedule-running-icon">
                                          <FontAwesomeIcon
                                            onClick={() => clickWorkoutStop(x)}
                                            icon={faStopCircle}
                                            className="schedule-running-icon-prop"
                                            size="lg"
                                          ></FontAwesomeIcon>
                                        </span>

                                        <div className=" text-center">
                                          <div className="schedule-running-text">
                                            Stop Your Schedule{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 3 ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div className=" text-center">
                                          <label className="schedule-stop-text">
                                            {x.TotalTime}
                                          </label>
                                        </div>
                                      </div>
                                    ) : null}
                                  </div>
                                </div>
                              );
                            })}
                          </>
                        ) : null}
                      </>
                    ) : null}
                  </div>
                );
              })}
            </Col>
            <Col
              sm={12}
              xs={12}
              xl={1}
              lg={1}
              md={4}
              className="px-2 form-group"
            >
              {" "}
              {/* d-flex justify-content-center row */}
              <div className=" d-flex justify-content-center form-group">
                <label className="schedule-week-header d-flex justify-content-center font-weight-bold">
                  Wednesday
                </label>
              </div>
              {listData.week_contents.map((z) => {
                return (
                  <div>
                    {z.Day == "Wednesday" ? (
                      <>
                        {z.ScheduleProgreamModel != null ? (
                          <>
                            {z.ScheduleProgreamModel.map((x) => {
                              let flagclass = "";
                              if (x.Flag == 2) {
                                flagclass = "schedule-day-box-runing-color";
                              } else if (x.Flag == 3) {
                                flagclass = "schedule-week-box-stop-color";
                              } else {
                                flagclass = "schedule-day-box-default-color";
                              }
                              return (
                                <div
                                  className={
                                    "schedule-week-box form-group " + flagclass
                                  }
                                  onClick={() => scheduleViewFunction(x)}
                                >
                                  <label className="p-2 d-flex justify-content-center font-weight-bold">
                                    {x.Name} {x.Time}
                                  </label>
                                  <div className="col-sm-12 form-group">
                                    {x.WorkOutModel.map((y) => {
                                      return (
                                        <div className=" mb-2 schedule-week-subbox">
                                          {y.WorkOut +
                                            " " +
                                            y.Sets +
                                            " * " +
                                            y.Reps}
                                        </div>
                                      );
                                    })}
                                  </div>
                                  <div className="">
                                    {x.Flag == 1 &&
                                    inputData.permissions.includes(
                                      Permission.M_S_START
                                    ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div>
                                          <PlayCircleFilled
                                            onClick={() => clickWorkoutStart(x)}
                                            className="schedule-click-icon"
                                          />
                                        </div>

                                        <div className="col-sm-12 text-center">
                                          <div className="schedule-click-text">
                                            Click here{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 2 &&
                                      inputData.permissions.includes(
                                        Permission.M_S_STOP
                                      ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <span className="schedule-running-icon">
                                          <FontAwesomeIcon
                                            onClick={() => clickWorkoutStop(x)}
                                            icon={faStopCircle}
                                            className="schedule-running-icon-prop"
                                            size="lg"
                                          ></FontAwesomeIcon>
                                        </span>

                                        <div className=" text-center">
                                          <div className="schedule-running-text">
                                            Stop Your Schedule{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 3 ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div className=" text-center">
                                          <label className="schedule-stop-text">
                                            {x.TotalTime}
                                          </label>
                                        </div>
                                      </div>
                                    ) : null}
                                  </div>
                                </div>
                              );
                            })}
                          </>
                        ) : null}
                      </>
                    ) : null}
                  </div>
                );
              })}
            </Col>
            <Col
              sm={12}
              xs={12}
              xl={1}
              lg={1}
              md={4}
              className="px-2 form-group"
            >
              {" "}
              {/* d-flex justify-content-center row */}
              <div className=" d-flex justify-content-center form-group">
                <label className="schedule-week-header d-flex justify-content-center font-weight-bold">
                  Thursday
                </label>
              </div>
              {listData.week_contents.map((z) => {
                return (
                  <div>
                    {z.Day == "Thursday" ? (
                      <>
                        {z.ScheduleProgreamModel != null ? (
                          <>
                            {z.ScheduleProgreamModel.map((x) => {
                              let flagclass = "";
                              if (x.Flag == 2) {
                                flagclass = "schedule-day-box-runing-color";
                              } else if (x.Flag == 3) {
                                flagclass = "schedule-week-box-stop-color";
                              } else {
                                flagclass = "schedule-day-box-default-color";
                              }
                              return (
                                <div
                                  className={
                                    "schedule-week-box form-group " + flagclass
                                  }
                                  onClick={() => scheduleViewFunction(x)}
                                >
                                  <label className="p-2 d-flex justify-content-center font-weight-bold">
                                    {x.Name} {x.Time}
                                  </label>
                                  <div className="col-sm-12 form-group">
                                    {x.WorkOutModel.map((y) => {
                                      return (
                                        <div className=" mb-2 schedule-week-subbox">
                                          {y.WorkOut +
                                            " " +
                                            y.Sets +
                                            " * " +
                                            y.Reps}
                                        </div>
                                      );
                                    })}
                                  </div>
                                  <div className="">
                                    {x.Flag == 1 &&
                                    inputData.permissions.includes(
                                      Permission.M_S_START
                                    ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div>
                                          <PlayCircleFilled
                                            onClick={() => clickWorkoutStart(x)}
                                            className="schedule-click-icon"
                                          />
                                        </div>

                                        <div className="col-sm-12 text-center">
                                          <div className="schedule-click-text">
                                            Click here{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 2 &&
                                      inputData.permissions.includes(
                                        Permission.M_S_STOP
                                      ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <span className="schedule-running-icon">
                                          <FontAwesomeIcon
                                            onClick={() => clickWorkoutStop(x)}
                                            icon={faStopCircle}
                                            className="schedule-running-icon-prop"
                                            size="lg"
                                          ></FontAwesomeIcon>
                                        </span>

                                        <div className=" text-center">
                                          <div className="schedule-running-text">
                                            Stop Your Schedule{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 3 ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div className=" text-center">
                                          <label className="schedule-stop-text">
                                            {x.TotalTime}
                                          </label>
                                        </div>
                                      </div>
                                    ) : null}
                                  </div>
                                </div>
                              );
                            })}
                          </>
                        ) : null}
                      </>
                    ) : null}
                  </div>
                );
              })}
            </Col>
            <Col
              sm={12}
              xs={12}
              xl={1}
              lg={1}
              md={4}
              className="px-2 form-group"
            >
              {" "}
              {/* d-flex justify-content-center row */}
              <div className=" d-flex justify-content-center form-group">
                <label className="schedule-week-header d-flex justify-content-center font-weight-bold">
                  Friday
                </label>
              </div>
              {listData.week_contents.map((z) => {
                return (
                  <div>
                    {z.Day == "Friday" ? (
                      <>
                        {z.ScheduleProgreamModel != null ? (
                          <>
                            {z.ScheduleProgreamModel.map((x) => {
                              let flagclass = "";
                              if (x.Flag == 2) {
                                flagclass = "schedule-day-box-runing-color";
                              } else if (x.Flag == 3) {
                                flagclass = "schedule-week-box-stop-color";
                              } else {
                                flagclass = "schedule-day-box-default-color";
                              }
                              return (
                                <div
                                  className={
                                    "schedule-week-box form-group " + flagclass
                                  }
                                  onClick={() => scheduleViewFunction(x)}
                                >
                                  <label className="p-2 d-flex justify-content-center font-weight-bold">
                                    {x.Name} {x.Time}
                                  </label>
                                  <div className="col-sm-12 form-group">
                                    {x.WorkOutModel.map((y) => {
                                      return (
                                        <div className=" mb-2 schedule-week-subbox">
                                          {y.WorkOut +
                                            " " +
                                            y.Sets +
                                            " * " +
                                            y.Reps}
                                        </div>
                                      );
                                    })}
                                  </div>
                                  <div className="">
                                    {x.Flag == 1 &&
                                    inputData.permissions.includes(
                                      Permission.M_S_START
                                    ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div>
                                          <PlayCircleFilled
                                            onClick={() => clickWorkoutStart(x)}
                                            className="schedule-click-icon"
                                          />
                                        </div>

                                        <div className="col-sm-12 text-center">
                                          <div className="schedule-click-text">
                                            Click here{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 2 &&
                                      inputData.permissions.includes(
                                        Permission.M_S_STOP
                                      ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <span className="schedule-running-icon">
                                          <FontAwesomeIcon
                                            onClick={() => clickWorkoutStop(x)}
                                            icon={faStopCircle}
                                            className="schedule-running-icon-prop"
                                            size="lg"
                                          ></FontAwesomeIcon>
                                        </span>

                                        <div className=" text-center">
                                          <div className="schedule-running-text">
                                            Stop Your Schedule{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 3 ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div className=" text-center">
                                          <label className="schedule-stop-text">
                                            {x.TotalTime}
                                          </label>
                                        </div>
                                      </div>
                                    ) : null}
                                  </div>
                                </div>
                              );
                            })}
                          </>
                        ) : null}
                      </>
                    ) : null}
                  </div>
                );
              })}
            </Col>
            <Col
              sm={12}
              xs={12}
              xl={1}
              lg={1}
              md={4}
              className="px-2 form-group"
            >
              {" "}
              {/* d-flex justify-content-center row */}
              <div className=" d-flex justify-content-center form-group">
                <label className="schedule-week-header d-flex justify-content-center font-weight-bold">
                  Saturday
                </label>
              </div>
              {listData.week_contents.map((z) => {
                return (
                  <div>
                    {z.Day == "Saturday" ? (
                      <>
                        {z.ScheduleProgreamModel != null ? (
                          <>
                            {z.ScheduleProgreamModel.map((x) => {
                              let flagclass = "";
                              if (x.Flag == 2) {
                                flagclass = "schedule-day-box-runing-color";
                              } else if (x.Flag == 3) {
                                flagclass = "schedule-week-box-stop-color";
                              } else {
                                flagclass = "schedule-day-box-default-color";
                              }
                              return (
                                <div
                                  className={
                                    "schedule-week-box form-group " + flagclass
                                  }
                                  onClick={() => scheduleViewFunction(x)}
                                >
                                  <label className="p-2 d-flex justify-content-center font-weight-bold">
                                    {x.Name} {x.Time}
                                  </label>
                                  <div className="col-sm-12 form-group">
                                    {x.WorkOutModel.map((y) => {
                                      return (
                                        <div className=" mb-2 schedule-week-subbox">
                                          {y.WorkOut +
                                            " " +
                                            y.Sets +
                                            " * " +
                                            y.Reps}
                                        </div>
                                      );
                                    })}
                                  </div>
                                  <div className="">
                                    {x.Flag == 1 &&
                                    inputData.permissions.includes(
                                      Permission.M_S_START
                                    ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div>
                                          <PlayCircleFilled
                                            onClick={() => clickWorkoutStart(x)}
                                            className="schedule-click-icon"
                                          />
                                        </div>

                                        <div className="col-sm-12 text-center">
                                          <div className="schedule-click-text">
                                            Click here{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 2 &&
                                      inputData.permissions.includes(
                                        Permission.M_S_STOP
                                      ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <span className="schedule-running-icon">
                                          <FontAwesomeIcon
                                            onClick={() => clickWorkoutStop(x)}
                                            icon={faStopCircle}
                                            className="schedule-running-icon-prop"
                                            size="lg"
                                          ></FontAwesomeIcon>
                                        </span>
                                        <div className=" text-center">
                                          <div className="schedule-running-text">
                                            Stop Your Schedule{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 3 ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div className=" text-center">
                                          <label className="schedule-stop-text">
                                            {x.TotalTime}
                                          </label>
                                        </div>
                                      </div>
                                    ) : null}
                                  </div>
                                </div>
                              );
                            })}
                          </>
                        ) : null}
                      </>
                    ) : null}
                  </div>
                );
              })}
            </Col>
            <Col
              sm={12}
              xs={12}
              xl={1}
              lg={1}
              md={4}
              className="px-2 form-group"
            >
              {" "}
              {/* d-flex justify-content-center row */}
              <div className=" d-flex justify-content-center form-group">
                <label className="schedule-week-header d-flex justify-content-center font-weight-bold">
                  Sunday
                </label>
              </div>
              {listData.week_contents.map((z) => {
                return (
                  <div>
                    {z.Day == "Sunday" ? (
                      <>
                        {z.ScheduleProgreamModel != null ? (
                          <>
                            {z.ScheduleProgreamModel.map((x) => {
                              let flagclass = "";
                              if (x.Flag == 2) {
                                flagclass = "schedule-day-box-runing-color";
                              } else if (x.Flag == 3) {
                                flagclass = "schedule-week-box-stop-color";
                              } else {
                                flagclass = "schedule-day-box-default-color";
                              }
                              return (
                                <div
                                  className={
                                    "schedule-week-box form-group " + flagclass
                                  }
                                  onClick={() => scheduleViewFunction(x)}
                                >
                                  <label className="p-2 d-flex justify-content-center font-weight-bold">
                                    {x.Name} {x.Time}
                                  </label>
                                  <div className="col-sm-12 form-group">
                                    {x.WorkOutModel.map((y) => {
                                      return (
                                        <div className=" mb-2 schedule-week-subbox">
                                          {y.WorkOut +
                                            " " +
                                            y.Sets +
                                            " * " +
                                            y.Reps}
                                        </div>
                                      );
                                    })}
                                  </div>
                                  <div className="">
                                    {x.Flag == 1 &&
                                    inputData.permissions.includes(
                                      Permission.M_S_START
                                    ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div>
                                          <PlayCircleFilled
                                            onClick={() => clickWorkoutStart(x)}
                                            className="schedule-click-icon"
                                          />
                                        </div>

                                        <div className="col-sm-12 text-center">
                                          <div className="schedule-click-text">
                                            Click here{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 2 &&
                                      inputData.permissions.includes(
                                        Permission.M_S_STOP
                                      ) ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <span className="schedule-running-icon">
                                          <FontAwesomeIcon
                                            onClick={() => clickWorkoutStop(x)}
                                            icon={faStopCircle}
                                            className="schedule-running-icon-prop"
                                            size="lg"
                                          ></FontAwesomeIcon>
                                        </span>

                                        <div className=" text-center">
                                          <div className="schedule-running-text">
                                            Stop Your Schedule{" "}
                                          </div>
                                        </div>
                                      </div>
                                    ) : x.Flag == 3 ? (
                                      <div className="col-sm-12 text-center form-group">
                                        <div className=" text-center">
                                          <label className="schedule-stop-text">
                                            {x.TotalTime}
                                          </label>
                                        </div>
                                      </div>
                                    ) : null}
                                  </div>
                                </div>
                              );
                            })}
                          </>
                        ) : null}
                      </>
                    ) : null}
                  </div>
                );
              })}
            </Col>

            {schedulesDetials != null ? (
              <Modal
                backdrop="static"
                size="md"
                //className='popupborder'
                keyboard={true}
                show={showModal}
                onHide={() => setShowModal(false)}
                aria-labelledby="contained-modal-title-vcenter"
                //centered
              >
                <Modal.Header className="">
                  <div className="col-sm-12">
                    <div className="row">
                      <div className="col-sm-11">
                        <div className="d-flex justify-content-center">
                          <h5 className="font-weight-bold text-white mb-0">
                            {" "}
                            Schedule View
                          </h5>
                        </div>
                      </div>
                      <div className="col-sm-1">
                        <div className="d-flex justify-content-end">
                          <button
                            type="button"
                            className="close text-white"
                            onClick={() => setShowModal(false)}
                            data-dismiss="modal"
                            aria-label="Close"
                          >
                            <span aria-hidden="true">&times;</span>
                          </button>{" "}
                        </div>
                      </div>
                    </div>
                  </div>
                </Modal.Header>
                <Modal.Body className="">
                  <div className="row ">
                    <div className="col-sm-5">
                      {" "}
                      <label className="font-weight-bold popup_txt">
                        Program Name
                      </label>{" "}
                    </div>
                    <div className="col-sm-7">
                      {" "}
                      <label>{schedulesDetials.ProgramName}</label>{" "}
                    </div>
                    <div className="col-sm-5">
                      {" "}
                      <label className="font-weight-bold popup_txt">
                        Schedule Name
                      </label>{" "}
                    </div>
                    <div className="col-sm-7">
                      {" "}
                      <label>{schedulesDetials.Name}</label>{" "}
                    </div>
                    <div className="col-sm-5">
                      {" "}
                      <label className="font-weight-bold popup_txt">
                        Workout
                      </label>{" "}
                    </div>
                    <div className="col-sm-7">
                      {" "}
                      <label>{schedulesDetials.workoutDtl}</label>
                    </div>
                    <div className="col-sm-5">
                      {" "}
                      <label className="font-weight-bold popup_txt">
                        Date{" "}
                      </label>{" "}
                    </div>
                    <div className="col-sm-7">
                      {" "}
                      <label>{schedulesDetials.Date}</label>{" "}
                    </div>
                    <div className="col-sm-5">
                      {" "}
                      <label className="font-weight-bold popup_txt">
                        Schedule Time
                      </label>{" "}
                    </div>
                    <div className="col-sm-7">
                      {" "}
                      <label>{schedulesDetials.Time}</label>{" "}
                    </div>
                    <div className="col-sm-5">
                      {" "}
                      <label className="font-weight-bold popup_txt">
                        Start Time
                      </label>{" "}
                    </div>
                    <div className="col-sm-7">
                      {" "}
                      <label>{schedulesDetials.StartTime}</label>{" "}
                    </div>
                    <div className="col-sm-5">
                      {" "}
                      <label className="font-weight-bold popup_txt">
                        End Time
                      </label>{" "}
                    </div>
                    <div className="col-sm-7">
                      {" "}
                      <label>{schedulesDetials.EndTime}</label>{" "}
                    </div>
                    <div className="col-sm-5">
                      {" "}
                      <label className="font-weight-bold popup_txt">
                        Total Time
                      </label>{" "}
                    </div>
                    <div className="col-sm-7">
                      {" "}
                      <label>{schedulesDetials.TotalTime}</label>{" "}
                    </div>
                  </div>
                </Modal.Body>
                <Modal.Footer className="m-0 py-0 ">
                  <div className="col-sm-12">
                    <div className="row  d-flex justify-content-center">
                      <Button
                        className="btn_green  btn"
                        onClick={() => (
                          setShowModal(false), setSchedulesDetials([])
                        )}
                      >
                        Close
                      </Button>
                    </div>
                  </div>
                </Modal.Footer>
              </Modal>
            ) : null}
          </Row>
        </>
      ) : null}
    </>
  );
});
const mapPropsToState = (state) => ({
  user: state.userReducer,
});
export default PermissionProvider(connect(
  mapPropsToState
)(withRouter(WeeklySchedules)));
//export default PermissionProvider(WeeklySchedules);
