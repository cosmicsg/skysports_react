import { Divider, Spin } from "antd";
import {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import { Col, Container, Row, Button } from "react-bootstrap";
import { connect } from "react-redux";
import * as APIUrl from "../../../components/api-manager/apiConstant";
import { APIRequest } from "../../../components/api-manager/apiRequest";
import { WeeklySchedules } from "./WeeklySchedules";
import { DaySchedules } from "./DaySchedules";
import MonthlySchedules from "./MonthlySchedules";
import UserImage from "../../../content/images/UserImage.png"
import { Permissions as Permission } from "../../../permissions/AppPermissions";
import PermissionProvider from "../../../permissions/PermissionProvider";
import { withRouter } from "react-router";
/// Master Schedules
export const MasterSchedule = forwardRef((props, ref) => {
  let permissions = props.getAppPermissions();

  const [masterData, setMaserData] = useState({
    programid: 0,
    fullname: props.user.Name,
    coachName: "",
    programName: "",
    role: "",
    imageData: "",
    athleteData: "",
    gole: "",
    flag: "",
    permissions: permissions,
  });
  const day_ScheduleRef = useRef(null);
  const week_ScheduleRef = useRef(null);
  const month_ScheduleRef = useRef(null);

  // header change function
  const changePrograms = (programid) => {
    if (
      programid !== null &&
      programid !== undefined &&
      masterData.programid !== programid
    ) {
      const fetchURL = APIUrl.GET_PROGRAM_MASTER_DATA + "/" + programid;
      APIRequest.getGetService(fetchURL)
        .then((response) => {
          if (response.Succeeded) {
            setMaserData((prev) => ({
              ...prev,
              programid: response.Data.ProgramID,
              imageData: response.Data.CoachImageData,
              coachName: response.Data.CoachName,
              fullname: response.Data.UserName,
              athleteData: response.Data.UserRoleName,
              role: response.Data.CoachRoleName,
              gole: response.Data.FocusGoal,
              programName: response.Data.ProgramName,
            }));
          }
        })
        .catch((error) => {});
    }
  };
  ///schedule type change function
  const scheduleOnClick = async (val) => {
    setMaserData((prev) => ({
      ...prev,
      flag: val,
    }));
  };
  //// default onload function
  useEffect(async () => {
    let shedules = "";
    if (masterData.permissions.includes(Permission.M_S_MONTHLY_SCHEDULES)) {
      shedules = "Month";
    }
    if (masterData.permissions.includes(Permission.M_S_WEEKLY_SCHEDULES)) {
      shedules = "Week";
    }
    if (masterData.permissions.includes(Permission.M_S_DAILY_SCHEDULES)) {
      shedules = "Day";
    }
    await scheduleOnClick(shedules);
    //const fetchURL = APIUrl.GET_PROFILE_PHOTO
    // APIRequest.getGetService(fetchURL)
    //     .then((response) => {
    //         if (response.Succeeded) {
    //             setMaserData((prev) => ({
    //                 ...prev,
    //                 imageData: response.Data.FileData,
    //             }))
    //         }
    //     })
    //     .catch((error) => { })
  }, []);
  return (
    <>
      <Container fluid className="p-0">
        <Spin spinning={false}>
          {masterData.permissions.includes(Permission.M_S_VIEW) ? (
            <>
              <div className="p-1">
                <div className="schedule-baner">
                  <Row>
                    <Col sm={12} xs={12} md={4} lg={3} xl={3}>
                      <div className="form-group">
                        <label className="pt-4 pl-4 m-0 text-white schedule-heder truncate " title={masterData.fullname}>
                          {" "}
                          {masterData.fullname}
                        </label>
                        <div className="pl-4 text-white schedule-sub-heder truncate" title={masterData.athleteData}>
                          {" "}
                          {masterData.athleteData}
                        </div>
                      </div>
                    </Col>
                    <Col
                      sm={12}
                      xs={12}
                      md={4}
                      lg={6}
                      xl={6}
                      className=" schedule-baner-img"
                    >
                      {""}
                      <div className="row schedule-baner-img"></div>
                    </Col>
                    <Col sm={12} xs={12} md={4} lg={3} xl={3}>
                      <div className="form-group">
                        <h4 className="p-2 mb-0 text-white truncate" title={masterData.programName}>
                          {" "}
                          {masterData.programName}
                        </h4>
                        <div className="pl-2 text-white">
                          {" "}
                          Goal-{" "}
                          <span className="text-warning truncate" title={masterData.gole} >
                            {masterData.gole}
                          </span>
                        </div>
                      </div>
                      <Col xl={12} sm={12} md={12} lg={12} xl={12} className="form-group p-0">
                        <div className="schedule-baner-box-item mr-3">
                          <Col xl={7}  lg={7} md={6} sm={6} xs={6} className="pull-left p-0">
                            <h4 className="p-2 text-left mb-0 text-white truncate" title={masterData.coachName}>
                              {" "}
                              {masterData.coachName}
                            </h4>
                            <p className="pl-2 text-left truncate" title={masterData.role}> {masterData.role}</p>
                          </Col>
                          <Col xl={5}  lg={5} md={6} sm={6} xs={6} className="pull-right">
                            <img
                              className="photo photo-userprofile bg-white"
                              style={{
                                width: "50px",
                                height: "50px",
                                textAlign: "center",
                                margin: "10px",
                              }}
                              src={
                                masterData.imageData == ""
                                  ? UserImage
                                  : masterData.imageData
                              }
                            ></img>
                          </Col>
                        </div>
                      </Col>
                    </Col>
                  </Row>
                </div>
              </div>
              <Row className="py-2 text-center">
                {masterData.permissions.includes(
                  Permission.M_S_DAILY_SCHEDULES
                ) ? (
                  <>
                    {/* <Col className="col-sm-1"></Col> */}
                    <Col xl={4} lg={4} md={6} sm={12} xs={12}>
                      {masterData.flag == "Day" ? (
                        <Button
                          onClick={() => scheduleOnClick("Day")}
                          className="schedule-header-Active-btn schedule-header-btn w-75 mx-4"
                        >
                          Daily Schedules
                        </Button>
                      ) : (
                        <Button
                          onClick={() => scheduleOnClick("Day")}
                          className=" schedule-header-btn w-75 mx-4"
                        >
                          Daily Schedules
                        </Button>
                      )}{" "}
                    </Col>
                  </>
                ) : null}
                {masterData.permissions.includes(
                  Permission.M_S_WEEKLY_SCHEDULES
                ) ? (
                  <>
                    {/* <Col className="col-sm-1"></Col> */}
                    <Col xl={4} lg={4} md={6} sm={12} xs={12}>
                      {masterData.flag == "Week" ? (
                        <Button
                          onClick={() => scheduleOnClick("Week")}
                          className="schedule-header-Active-btn  w-75 schedule-header-btn  mx-4"
                        >
                          Weekly Schedules
                        </Button>
                      ) : (
                        <Button
                          onClick={() => scheduleOnClick("Week")}
                          className="  schedule-header-btn w-75 mx-4"
                        >
                          Weekly Schedules
                        </Button>
                      )}
                    </Col>
                  </>
                ) : null}
                {masterData.permissions.includes(
                  Permission.M_S_MONTHLY_SCHEDULES
                ) ? (
                  <>
                    {/* <Col className="col-sm-1"></Col> */}
                    <Col xl={4} lg={4} md={6} sm={12} xs={12}>
                      {masterData.flag == "Month" ? (
                        <Button
                          onClick={() => scheduleOnClick("Month")}
                          className=" schedule-header-Active-btn schedule-header-btn w-75 mx-4"
                        >
                          Monthly Schedules
                        </Button>
                      ) : (
                        <Button
                          onClick={() => scheduleOnClick("Month")}
                          className=" schedule-header-btn w-75 mx-4"
                        >
                          Monthly Schedules
                        </Button>
                      )}{" "}
                    </Col>
                  </>
                ) : null}
                {/* <Col className="col-sm-1"></Col> */}
              </Row>
              {masterData.flag == "Day" &&
              masterData.permissions.includes(
                Permission.M_S_DAILY_SCHEDULES
              ) ? (
                <DaySchedules
                  Props={props}
                  ref={day_ScheduleRef}
                  changeProgramsFun={changePrograms}
                ></DaySchedules>
              ) : masterData.flag == "Week" &&
                masterData.permissions.includes(
                  Permission.M_S_WEEKLY_SCHEDULES
                ) ? (
                <WeeklySchedules
                  Props={props}
                  changeProgramsFun={changePrograms}
                  ref={week_ScheduleRef}
                >
                  {" "}
                </WeeklySchedules>
              ) : masterData.flag == "Month" &&
                masterData.permissions.includes(
                  Permission.M_S_MONTHLY_SCHEDULES
                ) ? (
                <MonthlySchedules
                  Props={props}
                  changeProgramsFun={changePrograms}
                  ref={month_ScheduleRef}
                >
                  {" "}
                </MonthlySchedules>
              ) : null}
            </>
          ) : null}
        </Spin>
      </Container>
    </>
  );
});
const mapPropsToState = (state) => ({
  user: state.userReducer,
});

//export default PermissionProvider(MasterSchedule);

export default PermissionProvider(connect(
  mapPropsToState
)(withRouter(MasterSchedule)));
