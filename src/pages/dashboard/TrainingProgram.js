import React, { useState, useEffect } from "react"
import { APIRequest } from "../../components/api-manager/apiRequest"
import * as APIUrl from "../../components/api-manager/apiConstant"
import moment from "moment"
import { Empty } from "antd"
import DashboardBackground from "../../content/images/DashboardBackground.png"

const TrainingProgram = () => {
	const [trainingProgramData, setTrainingProgramData] = useState([])

	useEffect(() => {
		APIRequest.getGetService(APIUrl.GET_DASHBOARD_TRAINING_PROGRAM_LIST).then((res) => {
			if (res.Succeeded) {
				setTrainingProgramData(res.Data)
			}
		})
	}, [])

	return (
		<>
		<div className={trainingProgramData.length > 0 ? null : "Dashboard-Background"}>
			<div className='training-program-container' style={{ width: "95%" }}>
				<div>
					<label class='training-program-text-line'></label>
					<label className='training-program-text'>Training program</label>
				</div>
				{trainingProgramData.length > 0 ? (
					<div className='training-program-scroll-section'>
						{trainingProgramData.map((item, index) => (
							<div className="row" key={index}>
								<div className="col-5 text-truncate w-100" title={item.ProgramName}>
									{item.ProgramName}
									</div>
									<div className='col-3 pull-right Training-date'>
										{moment(new Date(item.StartDate)).format("DD MMM YYYY").toString()}
									</div>
									<div
										className='col-1 pull-right p-0'
										// style={{ width: "5%", padding: "0px 10px 0px 10px", color: "black" }}
									>
										<hr className='training-program-line' />
									</div>
									<div className='col-3 pull-right Training-date'>
										{moment(new Date(item.EndDate)).format("DD MMM YYYY").toString()}
									</div>
									<hr className='training-program-line' />
								</div>
							
						))}
					</div>
				) : (
					<Empty className='training-program-scroll-section'
					description={
						<span>No Training Program</span>
					}
					></Empty>
				)}
			</div>
		</div>
		</>
	)
}

export default TrainingProgram
