import React, { useState, useEffect } from "react"
import { APIRequest, showNotification } from "../../components/api-manager/apiRequest"
import * as APIUrl from "../../components/api-manager/apiConstant"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEye } from "@fortawesome/free-solid-svg-icons"
import { Modal, Button } from "react-bootstrap"
import { Input, Empty } from "antd"

const { TextArea } = Input

const TasksList = () => {
	const [tasksData, setTasksData] = useState([])
	const [isAccept] = useState([])
	const [isAcceptpopup, setAcceptpopup] = useState(false)
	const [isRejectpopup, setRejectpopup] = useState(false)
	const [isRejectRemarks, setRejectRemarks] = useState("")
	const [errorProps, setErrorProps] = useState({})

	const openViewAlldataModal = (data) => {
		setAcceptpopup({ isAcceptpopup: true, isAccept: data })
		isAccept.TaskID = data.TaskID
		isAccept.TaskTypeId = data.TaskTypeId
		isAccept.ToInvitedUserId = data.ToInvitedUserId
		isAccept.TaskSourceId = data.TaskSourceId
		isAccept.TaskSourceTable = data.TaskSourceTable
		isAccept.TaskDescription = data.TaskDescription
		isAccept.TaskDescriptionTag = data.TaskDescriptionTag
	}

	//handling onchange reject
	const onChange = (e) => {
		let gameName = e.target.value
		let isError = {}
		let flag = false
		if (gameName === "" || gameName === undefined || gameName === null) {
			flag = true
			isError["RejectRemarks"] = "Please enter Remarks"
		}
		if (!flag) {
			setRejectRemarks(gameName)
		} else {
			setErrorProps(isError)
		}
	}

	const viewAlldatapopupClose = () => {
		setAcceptpopup(false)
	}
	const handleRejectInvite = (data) => {
		setRejectpopup({ isRejectpopup: true, isAccept: data })
		isAccept.TaskID = data.TaskID
		isAccept.TaskTypeId = data.TaskTypeId
		isAccept.ToInvitedUserId = data.ToInvitedUserId
		isAccept.TaskSourceId = data.TaskSourceId
		isAccept.TaskSourceTable = data.TaskSourceTable
		isAccept.TaskDescription = data.TaskDescription
		isAccept.TaskDescriptionTag = data.TaskDescriptionTag
	}

	const handleCloseReject = () => {
		setRejectpopup(false)
	}
	const handleSaveReject = async (data) => {
		let Input = {
			TaskID: data.TaskID,
			TaskTypeId: data.TaskTypeId,
			ToInvitedUserId: data.ToInvitedUserId,
			TaskSourceId: data.TaskSourceId,
			TaskSourceTable: data.TaskSourceTable,
			RejectRemarks: isRejectRemarks,
		}
		await APIRequest.getPostService(APIUrl.ROUTE_REJECT_GAMES_INVITATION, Input)
			.then((response) => {
				if (response.Succeeded === true) {
					showNotification("Invitation Rejected.", "success")
					APIRequest.getGetService(APIUrl.GET_USRE_TASKS_LIST).then((res) => {
						if (res.Succeeded) {
							setTasksData(res.Data)
						}
					})
				} else {
					showNotification("Something went wrong.", "error")
				}
				handleCloseReject()
				viewAlldatapopupClose()
			})
			.catch((error) => {})
	}
	const handleAcceptInvite = async (data) => {
		let Input = {
			TaskID: data.TaskID,
			TaskTypeId: data.TaskTypeId,
			ToInvitedUserId: data.ToInvitedUserId,
			TaskSourceId: data.TaskSourceId,
			TaskSourceTable: data.TaskSourceTable,
			RejectRemarks: "",
		}
		await APIRequest.getPostService(APIUrl.ROUTE_ACCEPT_GAMES_INVITATION, Input)
			.then((response) => {
				if (response.Succeeded === true) {
					showNotification("Invitation accepted successfully.", "success")
					APIRequest.getGetService(APIUrl.GET_USRE_TASKS_LIST).then((res) => {
						if (res.Succeeded) {
							setTasksData(res.Data)
						}
					})
				} else {
					showNotification("Something went wrong.", "error")
				}
				viewAlldatapopupClose()
			})
			.catch((error) => {})
	}

	useEffect(() => {
		APIRequest.getGetService(APIUrl.GET_USRE_TASKS_LIST).then((res) => {
			if (res.Succeeded) {
				setTasksData(res.Data)
			}
		})
	}, [])
	return (
		<>
		<div className={tasksData.length > 0 ? null : "Dashboard-Background"}>
			<div className='user-tasks-container' style={{ width: "95%" }}>
				<div>
					<label className='task-text-line'></label>
					<label className='task-text'>Tasks</label>
				</div>
				{tasksData.length > 0 ? (
					<div className='user-tasks-scroll-section'>
						{tasksData.map((item, index) => (
							<tr key={index}>
								<td style={{ width: "30%" }}>
									<div className='row'>
								<div className='col-md-10'>{item.TaskDescription}</div>
								    <div className='col-md-2 pull-right'>
										<FontAwesomeIcon
											title='View'
											className='mr-3 icon_blue_color'
											icon={faEye}
											onClick={() => openViewAlldataModal(item)}
											style={{ cursor: "pointer" }}
										/>
									</div>
									</div>
									<hr className='user-tasks-line' />
								</td>
							</tr>
						))}
					</div>
				) : (
					<Empty className='user-tasks-scroll-section'
					description={
						<span>No Tasks</span>
					}
					></Empty>
				)}
			</div>
			<div>
				<Modal
					backdrop='static'
					size='md'
					keyboard={true}
					show={isAcceptpopup}
					onHide={viewAlldatapopupClose}
					aria-labelledby='contained-modal-title-vcenter'
				>
					<Modal.Header className=''>
						<div className='col-sm-12'>
							<div className='row'>
								<div className='col-sm-11'>
									<div className='d-flex justify-content-center'>
										<h5 className='font-weight-bold text-white mb-0'>{"Accept Invite"}</h5>
									</div>
								</div>
								<div className='col-sm-1'>
									<div className='d-flex justify-content-end'>
										<button
											type='button'
											className='close text-white'
											onClick={viewAlldatapopupClose}
											data-dismiss='modal'
											aria-label='Close'
										>
											<span aria-hidden='true'>&times;</span>
										</button>{" "}
									</div>
								</div>
							</div>
						</div>
					</Modal.Header>
					<Modal.Body className=''>
						<div className='col-sm-12'>
							<div className='row pt-10'>
								<div className='col-sm-1'>{""}</div>
								<div className='col-sm-10'>
									<div className='form-group row'>{isAccept.TaskDescription}</div>
								</div>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer className='m-0 py-0 '>
						<div className='col-sm-12'>
							<div className='row  d-flex justify-content-center'>
								<Button className='btn_green mr-2' onClick={() => handleAcceptInvite(isAccept)}>
									Accept
								</Button>
								<Button
									className='cancel mr-2 btn_green'
									onClick={() => handleRejectInvite(isAccept)}
								>
									Reject
								</Button>
							</div>
						</div>
					</Modal.Footer>
				</Modal>
			</div>
			<div>
				<Modal
					backdrop='static'
					size='md'
					keyboard={true}
					show={isRejectpopup}
					onHide={handleCloseReject}
					aria-labelledby='contained-modal-title-vcenter'
				>
					<Modal.Header className=''>
						<div className='col-sm-12'>
							<div className='row'>
								<div className='col-sm-11'>
									<div className='d-flex justify-content-center'>
										<h5 className='font-weight-bold text-white mb-0'>{"Reject Invite"}</h5>
									</div>
								</div>
								<div className='col-sm-1'>
									<div className='d-flex justify-content-end'>
										<button
											type='button'
											className='close text-white'
											onClick={handleCloseReject}
											data-dismiss='modal'
											aria-label='Close'
										>
											<span aria-hidden='true'>&times;</span>
										</button>{" "}
									</div>
								</div>
							</div>
						</div>
					</Modal.Header>
					<Modal.Body className=''>
						<div className='row col-sm-12 form-group'>
							<div className='col-sm-12'>
								<label>
									Remarks<span className='text-danger'>*</span>
								</label>
								<TextArea
									type='text'
									name='rejectRemarks'
									size='large'
									autoComplete='off'
									max={200}
									value={isRejectRemarks}
									onChange={onChange}
									bordered={false}
									className='textfiled w-80'
									placeholder='Enter Remarks'
								></TextArea>
								<span className='text-danger'>{errorProps.RejectRemarks}</span>
							</div>
						</div>
					</Modal.Body>
					<Modal.Footer className='m-0 py-0 '>
						<div className='col-sm-12'>
							<div className='row  d-flex justify-content-center'>
								<Button className='btn_green mr-2' onClick={() => handleSaveReject(isAccept)}>
									Save
								</Button>
								<Button className='cancel mr-2 btn_green' onClick={handleCloseReject}>
									Close
								</Button>
							</div>
						</div>
					</Modal.Footer>
				</Modal>
			</div>
			</div>
		</>
	)
}

export default TasksList
