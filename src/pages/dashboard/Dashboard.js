import React from "react"
import "../../styles/menu.css"
import UpcomingGames from "./UpcomingGames"
import AthleteResult from "./AthleteResult"
import TasksList from "./TasksList"
import TrainingProgram from "./TrainingProgram"

class dashBoard extends React.Component {
	render() {
		return (
			<div className='mt-4' style={{ paddingLeft: "15px"}}>
				<div className='row col-md-12 ' style={{ paddingLeft: "3%", paddingRight: "0%" }}>
					<div className='col-md-6' style={{ padding: "10px" }}>
						<TasksList />
					</div>
					<div className='col-md-6 form-group' style={{ padding: "10px" }}>
						<UpcomingGames />
					</div>

					<div className='col-md-6' style={{ padding: "10px" }}>
						<AthleteResult />
					</div>
					<div className='col-md-6' style={{ padding: "10px" }}>
						<TrainingProgram />
					</div>
				</div>
			</div>
		)
	}
}
export default dashBoard
