import React, { useState, useEffect } from "react"
import { APIRequest } from "../../components/api-manager/apiRequest"
import * as APIUrl from "../../components/api-manager/apiConstant"
import moment from "moment"
import { Empty } from "antd"

const UpcomingGames = () => {
	const [gamesData, setGamesData] = useState([])

	useEffect(() => {
		APIRequest.getGetService(APIUrl.GET_UPCOMING_GAMES_LIST).then((res) => {
			if (res.Succeeded) {
				setGamesData(res.Data)
			}
		})
	}, [])

	return (
		<>
			<div className={gamesData.length > 0 ? null : "Dashboard-Background"} >
				<div className='upcoming-game-container'  style={{ width: "95%" }}>
					<div>
						<label class='games-text-line'></label>
						<label className='games-text'>Upcoming Games</label>
					</div>
					{gamesData.length > 0 ? (
						<div className='upcoming-games-scroll-section'>
							{gamesData.map((item, index) => (
								<div className='row' key={index} >
									<div className="col-5 text-truncate w-100" title={item.Name}>
										{item.Name}
									</div>
									<div className='col-3 pull-right upcoming-date'>
										{moment(new Date(item.StartDate)).format("DD MMM YYYY").toString()}
									</div>
									<div
										className='col-1 pull-right p-0'
									// style={{ width: "20%", padding: "0px 10px 0px 10px", color: "black" }}
									>
										<hr className='upcoming-date-line' />
									</div>
									<div className='col-3 pull-right upcoming-date'>
										{moment(new Date(item.EndDate)).format("DD MMM YYYY").toString()}
									</div>
									<br></br>
									<br></br>
									{/* <hr className='upcoming-date-line' /> */}
								</div>
							))}
						</div>
					) : (
						<Empty className='upcoming-games-scroll-section'
							description={
								<span>No Upcoming Games</span>
							}
						></Empty>
					)}
				</div>
			</div>
		</>
	)
}

export default UpcomingGames
