import { Empty, Tooltip } from "antd";
import { Select, } from "antd"
import React, { useEffect, useState } from "react";
import Chart from "react-google-charts";
import * as APIUrl from "../../components/api-manager/apiConstant"
import { APIRequest } from "../../components/api-manager/apiRequest"
import PermissionProvider from "../../permissions/PermissionProvider";

const AthleteResult = (props) => {

    const [datas, setDatas] = useState({
        sportid: [],
        listData: [],
        listSport: [],
        groupWidth: "30%"

    })
    const { Option } = Select

    /// Bar chart Tooltip function
    const generateTooltipContent = (Athlete, Position, Score, sport) => {
        return "<Tooltip style={{color='red'}}><div id='tooltip-inner-container'  className='athlete-dashbord-tooltip pt-2'><h2 className='athlete-dashbord-tooltip'>Athlete : " + Athlete + " Position : " + Position + " Score : " + Score + " Sport :" + sport +
            "<h2></div></Tooltip>"
    }
    // onload function 
    useEffect(() => {

        /// sports list geted API
        APIRequest.getGetService(APIUrl.GET_SPORT_LIST).then((res) => {
            if (res.Succeeded) {

                let sportID = 0;
                if (res.Data.length > 0) {
                    sportID = res.Data[0].OptionID;

                }
                setDatas((prevState) => ({
                    ...prevState,
                    listSport: res.Data, sportid: sportID
                }))
            }
        })

        /// Dashbord AthleteResult Data get API
        APIRequest.getGetService(APIUrl.DASHBOARD_ATHLETE_TRACK_RESULT + "/" + datas.sportid).then((res) => {
            let dataArray = [];
            dataArray.push(['TT', 'Position', { role: 'tooltip', type: 'string', p: { html: true } }])
            if (res.Succeeded) {

                if (res.Data.length > 0) {
                    res.Data.map((x) => {

                        dataArray.push([x.ProfileName, x.Positions, generateTooltipContent(x.ProfileName, x.Positions, x.Score, x.sportName)])
                    })
                }
                let groupwidth = "10%"
                if (res.Data.length > 3) {
                    groupwidth = "30%"
                }
                setDatas((prevState) => ({
                    ...prevState,
                    listData: dataArray, groupWidth: groupwidth
                }))
            }
        })


    }, [])

    //// Sport onchange function
    useEffect(() => {

        /// Sport change based  Dashbord AthleteResult Data get API
        APIRequest.getGetService(APIUrl.DASHBOARD_ATHLETE_TRACK_RESULT + "/" + datas.sportid).then((res) => {
            let dataArray = [];
            dataArray.push(['TT', 'Position', { role: 'tooltip', type: 'string', p: { html: true } }])
            if (res.Succeeded) {

                if (res.Data.length > 0) {
                    res.Data.map((x) => {

                        dataArray.push([x.ProfileName, x.Positions, generateTooltipContent(x.ProfileName, x.Positions, x.Score, x.sportName)])
                    })
                }
                let groupwidth = "10%"
                if (res.Data.length > 3) {
                    groupwidth = "30%"
                }
                setDatas((prevState) => ({
                    ...prevState,
                    listData: dataArray, groupWidth: groupwidth
                }))
            }
        })
    }, [datas.sportid])

    /// Data set function
    const handleSportnchange = async (val, data) => {

        if (val !== undefined) {
            setDatas((Prev) => ({
                ...Prev, sportid: val
            }))
        }

    }
    return (
        <>
        <div className={datas.listData.length > 1 ? null : "Dashboard-Background"}>
            <div className="athlete-dashboard-container"  style={{ width: "95%" }}>
                <div className="row">
                    <div className="col-sm-6">
                        <label className="athlete-dashbord-header-left-border"> </label>
                        <label className="athlete-dashboard-header">Athlete Result </label> </div>
                    <br></br>
                    <div className="col-sm-6">
                        {props.permissionState.UserType != "Athlete" ? (
                            <Select className='textfiled pl-0'
                            getPopupContainer={trigger=> trigger.parentNode}
                                bordered={false}
                                showSearch={true}
                                showArrow
                                //allowClear
                                // style={{ width: "100%" }}
                                placeholder='Select Sport'
                                name='sport'
                                size={"large"}
                                value={datas.sportid}
                                onChange={(val, key) => handleSportnchange(val, key)}
                                optionFilterProp='children'
                                filterSort={(optionA, optionB) =>
                                    optionA.children
                                        .toLowerCase()
                                        .localeCompare(optionB.children.toLowerCase())
                                }
                            >
                                {datas.listSport.map((c) => (
                                    <Option value={c.OptionID}>{c.OptionData}</Option>
                                ))}
                            </Select>
                        ) : null}</div>
                </div>
                {datas.listData.length > 1 ? <>
                    <Chart
                        width={'100%'}
                        chartType="BarChart"
                        loader={<div>Loading Chart</div>}

                        data={datas.listData}

                        options={{
                            title: 'Top Athlete Results',
                            borderColor: '#EBBA95',
                            borderRadius: 20,
                            borderWidth: 2,
                            chartArea: { width: '50%' },
                            legend: { position: 'none' },
                            width: "100%",
                            height: 180,//180
                            bar: { groupWidth: datas.groupWidth, cornerRadius: 10 },
                            colors: ['#e300ff'],///"linear-gradient(to top, red, orange)",
                            tooltip: { isHtml: true, trigger: "visible" },
                            borderRadius: 10,
                            backgroundColor: "#f3f3f3",
                            isStacked: true,
                            animation: {
                                duration: 800,
                                easing: 'linear',
                                startup: true,
                            },
                            //   series: { 0: { color: "orange", pointSize: 5, lineWidth: 4 }}, 
                            hAxis: {
                                title: 'Position',
                                titleTextStyle: { color: 'blue' },
                                minValue: 0,
                                position: "left",
                                maxValue: 1,
                                gridlines: { color: "green" },
                                // textPosition: 'out'
                                textStyle: {
                                    // fontSize: 10,
                                    // fontStyle: "Arial",
                                    // marginTop: '10',
                                    color: 'green',
                                    // textalgin: "left",
                                    bordered: "5px solid red",

                                },
                            },
                            vAxis: {
                                title: 'Athlete',
                                textPosition: "left",
                                titleTextStyle: { color: 'blue' },
                                gridlines: { color: "blue" },
                                textStyle: {
                                    fontSize: 10,
                                    fontStyle: "Arial",
                                    marginTop: '10',
                                    color: 'red',
                                    textalgin: "left",
                                    borderRadius: 10,

                                },
                                //  textPosition: 'in'
                            },
                            "displayExactValues": true,
                            // chartArea: {left: "100",top: 10 }
                        }}
                        // For tests
                        rootProps={{ 'data-testid': '2' }}
                    />
                </> : <Empty className="pt-5"
                    description={
                        <span>No Athlete Results</span>
                    }
                ></Empty>}
            </div>
            </div>
        </>
    )
}

export default PermissionProvider(AthleteResult);