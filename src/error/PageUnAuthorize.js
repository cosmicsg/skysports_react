// it is used to show page not authorized

import React, {useEffect} from 'react'
import '../App'

function PageUnAuthorize() {

    //it is life cycle method of reactjs.
    useEffect(()=>{
        document.title = "401 - Unauthorized Access";
    });
    return (
        <div className="error-page-content">
            <h1> 401 - Unauthorized: Access denied due to invalid credentials</h1>
        </div>
    )
}

export default PageUnAuthorize
