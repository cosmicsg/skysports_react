import React from "react";
import "../styles/grid.css";

const LinkExpired = () => {
  const showErrorPage = () => {
    let message =
      '<div><h1 className="link-expired-page">Sorry, the link has expired.</h1><span style={{fontSize:"30px"}} className="link-expired-page">Click <Link to={ROUTES.LOGIN}>here</Link>to go back to the login page</span> </div>';
    document.write(message);
  };
  return (
    <>
      <div className="link-expired-page">
        <h1>Sorry, the link has expired.</h1>
      </div>
    </>
  );
};

export default LinkExpired;
