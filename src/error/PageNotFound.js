//it is used to show page not found

import React,{useEffect} from 'react'
import '../App'
function PageNotFound() {
    
    //it is life cycle method of reactjs.
    useEffect(()=>{
        document.title = "404 - Page Not Found"
    });
    return (
        //<div style={{textAlign: "center"}}>
        <div className="error-page-content">
            <h1> 404 - Error Page</h1>
            <h2>The page you requested isn't available.</h2>
        </div>
    )
}

export default PageNotFound
