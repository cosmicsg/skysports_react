import React from "react";

/*
      GlobalErrorHandle which is used to handle global level or application level handles an error 
      only if the error occurred in react lifecycle method 
*/
class GlobalErrorHandler extends React.PureComponent {

  //
  // constructor for state declaration and initialization
  //
  constructor(props) {
    
    super(props);
    this.state = {
      hasError: false,
    };
  }

  //
  // Handle errors of an application
  //
  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  //
  // Capture error logs
  //
  componentDidCatch(error, errorInfo) {
    //If you need to log error then this is the place to capture the error logs.
  }

  //
  // render which will return an elements()
  //
  render() {
    //if hasError flag is true then below fallback UI gets rendered
    if (this.state.hasError) return <div>Something went wrong</div>;
    return this.props.children;
  }
}

export default GlobalErrorHandler;
