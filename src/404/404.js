import React from "react"
import { connect } from "react-redux"
// import Lottie from "react-lottie"
// import animationData from "../assets/json/404.json"

class FourOhFour extends React.Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render(props) {
		// const defaultOptions = {
		// 	loop: true,
		// 	autoplay: true,
		// 	animationData: animationData,
		// 	rendererSettings: {
		// 		preserveAspectRatio: "xMidYMid slice",
		// 	},
		// }
		return (
			<div>
				<div className='' style={{ margin: "40px" }}>
					<h1 className='page-not-found'>Page Not Found</h1>
					<div style={{ marginLeft: "55px" }}>
						<h5>We couldn't find what you looking for.</h5>
						<h5>Please contact the support Admin.</h5>
					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		user: state.userReducer,
	}
}

export default connect(mapStateToProps, null)(FourOhFour)
//export default withRouter(FourOhFour)
