import React from 'react'
import ReactExport from 'react-export-excel';

//export components
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

//Export data func
function ExportData(props) {
    return (
        <div>
                <ExcelFile filename = {props.filename} hideElement>
                    <ExcelSheet data = { props.data ? props.data : [] } name = {props.filename}>
                        { props.label.map((label, key)=>{
                            return(
                                    <ExcelColumn key={key} label = {label.columnName} value= {label.value} />
                            )
                        }) }
                    </ExcelSheet>
                </ExcelFile>
        </div>
    )
}

export default ExportData
