import {connect} from 'react-redux'
import PropTypes from 'prop-types';

//
// This component can be used to validate user's permission, but now this component has not been used in any where else. 
// For dummy purpose it has been created. 
// 
//
const Can=({children})=>{
    const userPermissions = ["ADMIN"]//user.userPermissions;
    const permissions = ["ADMIN"]
    let match = false;
    const PermissionArr = Array.isArray(permissions) ? permissions : [permissions];
    if(PermissionArr.length === 0){
        match = true;
    }else{
        match = PermissionArr.some((p)=> userPermissions.includes(p));
    }
    if(match){
        return (
            <>{children}</>
        )
    }else{
        return null;
    }
}

//
// properties type checking
//
Can.propTypes = {
    permissions: PropTypes.string.isRequired,
    userPermissions: PropTypes.array.isRequired
};

//
// read state from redux store through reducer.
//
const mapStateToProps = (state) => {
    return {
        user: state.userReducer,
    };
};

export default connect(mapStateToProps)(Can);


