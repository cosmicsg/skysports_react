import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faArrowLeft,
  faStepBackward,
  faStepForward,
} from "@fortawesome/free-solid-svg-icons";
import "../App";

class Pagination extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      todos: [
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
        "o",
        "p",
        "q",
        "r",
        "s",
        "t",
        "u",
        "v",
        "w",
        "x",
        "y",
        "z",
      ],
      currentPage: 1,
      todosPerPage: 10, ////page data show
      upperPageBound: 3, /// page length .... set
      lowerPageBound: 0, ////// page ..... show number set
      isPrevBtnActive: "disabled",
      isFirstpage: "disabled",
      isLastpage: "",
      isNextBtnActive: "",
      pageBound: 3, ///3dot center degit set
      totalpage: this.props.totalPages,
      totalRecord: this.props.totalRecords,
      pages: false,
      updateComponent: true,
    };
    this.handleClick = this.handleClick.bind(this);
    this.btnDecrementClick = this.btnDecrementClick.bind(this);
    this.btnIncrementClick = this.btnIncrementClick.bind(this);
    this.btnNextClick = this.btnNextClick.bind(this);
    this.btnPrevClick = this.btnPrevClick.bind(this);
    this.btnFirstClick = this.btnFirstClick.bind(this);
    this.btnLastClick = this.btnLastClick.bind(this);
    // this.componentDidMount = this.componentDidMount.bind(this);
    this.setPrevAndNextBtnClass = this.setPrevAndNextBtnClass.bind(this);
  }
  componentDidUpdate() {
    let toggle = document.querySelectorAll("ul li");
    if (this.state.currentPage !== 0) {
      for (var i = 0; i < toggle.length; i++) {
        //let value = toggle[i]
        toggle[i].classList.remove("active");
        var myElementID = document.getElementById(this.state.currentPage);
        if (myElementID !== null) {
          document
            .getElementById(this.state.currentPage)
            .classList.add("active");
        }
      }
    }
  }
  handleClick(event) {
    let listid = Number(event.target.id);
    this.setState({
      currentPage: listid,
    });
    var toggle = document.querySelectorAll(".pagination li ");
    for (var i = 0, length = toggle.length; i < length; i++) {
      toggle[i].classList.remove("active");
      document.getElementById(listid).classList.add("active");
    }
    this.setPrevAndNextBtnClass(listid);
  }

  setPrevAndNextBtnClass(listid) {
    let totalPage = Math.ceil(this.state.totalRecord / this.state.todosPerPage);
    this.setState({ isNextBtnActive: "disabled" });
    this.setState({ isPrevBtnActive: "disabled" });
    this.setState({ isFirstpage: "disabled" });
    this.setState({ isLastpage: "disabled", totalpage: totalPage });
    if (totalPage === listid && totalPage > 1) {
      this.setState({ isPrevBtnActive: "" });
      this.setState({ isFirstpage: "" });
    }
     else if (totalPage === listid) {
      this.setState({ isNextBtnActive: "disabled" });
      this.setState({ isLastpage: "disabled" });
    }
    else if (listid === 1 && totalPage > 1) {
      this.setState({ isNextBtnActive: "" });
      this.setState({ isLastpage: "" });
    } else if (totalPage > 1) {
      this.setState({ isNextBtnActive: "" });
      this.setState({ isPrevBtnActive: "" });
      this.setState({ isFirstpage: "" });
      this.setState({ isLastpage: "" });
    }
    this.props.paginationCall(listid);
  }
  btnIncrementClick() {
    this.setState({
      upperPageBound: this.state.upperPageBound + this.state.pageBound,
    });
    this.setState({
      lowerPageBound: this.state.lowerPageBound + this.state.pageBound,
    });
    let listid = this.state.upperPageBound + 1;
    this.setState({ currentPage: listid });
    this.setPrevAndNextBtnClass(listid);
  }
  btnDecrementClick() {
    this.setState({
      upperPageBound: this.state.upperPageBound - this.state.pageBound,
    });
    this.setState({
      lowerPageBound: this.state.lowerPageBound - this.state.pageBound,
    });
    let listid = this.state.upperPageBound - this.state.pageBound;
    this.setState({ currentPage: listid });
    this.setPrevAndNextBtnClass(listid);
  }
  btnPrevClick() {
    if ((this.state.currentPage - 1) % this.state.pageBound === 0) {
      this.setState({
        upperPageBound: this.state.upperPageBound - this.state.pageBound,
      });
      this.setState({
        lowerPageBound: this.state.lowerPageBound - this.state.pageBound,
      });
    }
    let listid = this.state.currentPage - 1;
    this.setState({ currentPage: listid });
    this.setPrevAndNextBtnClass(listid);
  }
  btnNextClick() {
    if (this.state.currentPage + 1 > this.state.upperPageBound) {
      this.setState({
        upperPageBound: this.state.upperPageBound + this.state.pageBound,
      });
      this.setState({
        lowerPageBound: this.state.lowerPageBound + this.state.pageBound,
      });
    }
    let listid = this.state.currentPage + 1;
    this.setState({ currentPage: listid });
    this.setPrevAndNextBtnClass(listid);
  }
  btnFirstClick() {
    // if((1) %this.state.pageBound === 0 ){
    this.setState({ upperPageBound: this.state.pageBound });
    this.setState({ lowerPageBound: 0 });
    //}
    let listid = 1;
    this.setState({ currentPage: listid });
    this.setPrevAndNextBtnClass(listid);
  }
  btnLastClick() {
    // if((1) %this.state.pageBound === 0 ){
    let totalPage = Math.ceil(this.state.totalRecord / this.state.todosPerPage);
    this.setState({ upperPageBound: totalPage });
    this.setState({ lowerPageBound: totalPage - this.state.pageBound });
    //}
    let listid = totalPage;
    this.setState({ currentPage: listid });
    this.setPrevAndNextBtnClass(listid);
  }
  static getDerivedStateFromProps(props) {
    return {
      totalRecord: props.totalRecords,
    };
  }
  render() {
    const {
      currentPage,
      todosPerPage,
      upperPageBound,
      lowerPageBound,
      isPrevBtnActive,
      isNextBtnActive,
      isLastpage,
      isFirstpage,
      totalpage,
      totalRecord,
      pages,
    } = this.state;
    // Logic for displaying current todos
    //const indexOfLastTodo = currentPage * todosPerPage
    //const indexOfFirstTodo = indexOfLastTodo - todosPerPage
    //const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);

    // const renderTodos = currentTodos.map((todo, index) => {
    //
    //   return <li key={index}>{todo}</li>;
    // });

    // Logic for displaying page numbers
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(totalRecord / todosPerPage); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map((number) => {
      if (number === 1 && currentPage === 1) {

        return (
          <li key={number} className="active" value={number} id={number}>
            <a
              href="#"
              id={number}
              onClick={this.handleClick}
              className="default-color"
            >
              {number}
            </a>
          </li>
        );
      } else if (number < upperPageBound + 1 && number > lowerPageBound) {
        // this.setState({
        //   isLastpage: "",
        //   isNextBtnActive: "",
        // })
        return (
          <li key={number} value={number} id={number}>
            <a
              href="#"
              id={number}
              onClick={this.handleClick}
              className="default-color"
            >
              {number}
            </a>
          </li>
        );
      }
    });
    let pageIncrementBtn = null;
    if (pageNumbers.length > upperPageBound) {

      pageIncrementBtn = (
        <li className="">
          <a
            href="#"
            onClick={this.btnIncrementClick}
            className="default-color"
          >
            {" "}
            &hellip;{" "}
          </a>
        </li>
      );
    }
    let pageDecrementBtn = null;
    if (lowerPageBound >= 1) {
      pageDecrementBtn = (
        <li className="">
          <a
            href="#"
            onClick={this.btnDecrementClick}
            className="default-color"
          >
            {" "}
            &hellip;{" "}
          </a>
        </li>
      );
    }
    let renderPrevBtn = null;
    if (isPrevBtnActive === "disabled") {
      renderPrevBtn = (
        <li className={isPrevBtnActive}>
          <span id="btnPrev" className="default-color pagination-icon-border">
            {" "}
            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>{" "}
          </span>
        </li>
      );
    } else {
      renderPrevBtn = (
        <li className={isPrevBtnActive}>
          <a
            href="#"
            id="btnPrev"
            onClick={this.btnPrevClick}
            className="default-color pagination-icon-border"
          >
            {" "}
            <FontAwesomeIcon icon={faArrowLeft}></FontAwesomeIcon>{" "}
          </a>
        </li>
      );
    }
    let renderNextBtn = null;
    let totalPage = this.props.totalPages
    if (totalPage === 1) {
         this.setState({isNextBtnActive: "disabled"})
    }
    if (isNextBtnActive === "disabled") {
      renderNextBtn = (
        <li className={isNextBtnActive}>
          <span id="btnNext" className="default-color pagination-icon-border">
            {" "}
            <FontAwesomeIcon icon={faArrowRight}></FontAwesomeIcon>{" "}
          </span>
        </li>
      );
    } else {
      renderNextBtn = (
        <li className={isNextBtnActive}>
          <a
            href="#"
            id="btnNext"
            onClick={this.btnNextClick}
            className="default-color pagination-icon-border"
          >
            {" "}
            <FontAwesomeIcon icon={faArrowRight}></FontAwesomeIcon>{" "}
          </a>
        </li>
      );
    }
    let renderFirstBtn = null;
    if (isFirstpage === "disabled") {
      renderFirstBtn = (
        <li className={isFirstpage}>
          <span id="btnFirst" className="default-color pagination-icon-border">
            {" "}
            <FontAwesomeIcon icon={faStepBackward}></FontAwesomeIcon>{" "}
          </span>
        </li>
      );
    } else {
      renderFirstBtn = (
        <li className={isFirstpage}>
          <a
            href="#"
            id="btnFirst"
            onClick={this.btnFirstClick}
            className="default-color pagination-icon-border"
          >
            {" "}
            <FontAwesomeIcon icon={faStepBackward}></FontAwesomeIcon>{" "}
          </a>
        </li>
      );
    }
    let renderLastBtn = null;
    if (totalPage === 1) {
         this.setState({isLastpage: "disabled"})
       }
    if (isLastpage === "disabled") {
      renderLastBtn = (
        <li className={isLastpage}>
          <span id="btnNext" className="default-color pagination-icon-border">
            {" "}
            <FontAwesomeIcon icon={faStepForward}></FontAwesomeIcon>{" "}
          </span>
        </li>
      );
    } else {
      renderLastBtn = (
        <li className={isLastpage}>
          <a
            href="#"
            id="btnNext"
            onClick={this.btnLastClick}
            className="default-color pagination-icon-border"
          >
            {" "}
            <FontAwesomeIcon icon={faStepForward}></FontAwesomeIcon>{" "}
          </a>
        </li>
      );
    }
    let renderpages = null;

    if (pages === true) {
      renderpages = (
        <li className="">
          <a href="#" className="default-color">
            {" "}
            Page {currentPage} / {totalpage}{" "}
          </a>
        </li>
      );
    }
    return (
      <div>
        {/* <ul>
          {renderTodos}
        </ul> */}
        <ul className="pagination">
          {renderpages}
          {renderFirstBtn}
          {renderPrevBtn}
          {pageDecrementBtn}
          {renderPageNumbers}
          {pageIncrementBtn}
          {renderNextBtn}
          {renderLastBtn}
        </ul>
      </div>
    );
  }
}

export default Pagination;
